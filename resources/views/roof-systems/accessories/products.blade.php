@extends('layouts.frontend')

@section('title', 'Light Commercial Vehicles - Roof Bars - Models')

@section('heading')
	Light Commercial Vehicles - Roof Bars - Models
@stop

@section('meta_description')
	Witter Light Commercial Vehicles - Roof Bars - Models
@stop

@section('content')


<div class="container product-listing">
	<div class="row">

		<div class="col-md-12">

            <p class="pre-title">We have the following roof bars to fit your</p>

            <h2 class="nomargin">{{ $manufacturer->manufacturerName }} - {{ $model->modelName }}</h2>

            <div class="button-area">
                <a href="{{ route('roof-systems.accessories') }}" class="btn btn-primary">Not your Vehicle?</a>
            </div>
        </div> <!-- .col-md-6 -->

    </div> <!-- .row -->

    <div class="row">
        <div class="col-md-12">

            @if(!empty($products))
                <div class="item-list">
                    @foreach($products as $product)

                        @include('partials.analytics.json_product_details', [
                            'product' => $product,
                            'list_name' => 'Category Results',
                            'category' => 'Roof Systems/Accessories',
                            'position' => $loop->iteration
                        ])

                        <div class="product-listing-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="{{ route('roof-systems.accessories.view', encode_url($product->partNo)) }}" class="product-link" data-product-id="{{ $product->partNo }}"><img src="{{ image_load($product->imageUrl) }}" alt=""></a>
                                </div> <!-- .col-md-3 -->

                                <div class="col-md-6">
                                    <a href="{{ route('roof-systems.accessories.view', encode_url($product->partNo)) }}" class="product-link" data-product-id="{{ $product->partNo }}"><h3>{{ $product->title }}</h3></a>

                                    <p>{{ $product->details }}</p>
                                    <div class="price">

                                        @php
                                            $save = empty($product->retailPrice) ? 0 : $product->retailPrice - $product->price;
                                        @endphp

                                        @if($save > 0)

                                            <span class="retail-price">RRP &pound;{{ price($product->retailPrice) }}</span>

                                            <h3 class="price">Buy now for &pound;{!! price($product->price) !!}</h3>

                                            <span class="retail-price">Save &pound;{{ price($save) }}</span>
                                        @else
                                            Buy now for
                                            <span>&pound;{{ price($product->price) }} Inc. VAT</span>
                                        @endif

                                    </div> <!-- .price -->
                                </div> <!-- .col-md-6 -->
                                <div class="col-md-3">
                                    <p>
                                        <a href="{{ route('roof-systems.accessories.view', encode_url($product->partNo)) }}" class="btn btn-secondary btn-block product-link" data-product-id="{{ $product->partNo }}">More<br>Details</a>
                                    </p>
                                </div> <!-- .col-md-3 -->
                            </div> <!-- .row -->
                        </div> <!-- .towbar-listing -->
                    @endforeach
                </div> <!-- .item-list -->
            @else
                <p>Sorry, we don't have any roof bars that are compatible with your vehicle.</p>
            @endif

        </div> <!-- .col-md-12 -->
    </div> <!-- .row -->
</div> <!-- .container -->
@stop

@section('scripts')
	<script>
        @include('partials.analytics.list', [
            'products' => $products,
            'page' => 1,
            'per_page' => 999,
            'list_name' => 'Category Results',
            'category' => 'Roof Systems/Accessories',
        ])
	</script>
@stop
