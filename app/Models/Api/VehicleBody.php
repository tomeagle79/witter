<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class VehicleBody extends ApiModel
{
	const URI = "bodies";
	
	/**
	 * Get all models based on model ID
	 *
	 * @param Int
	 * @return Object
	 */
	static public function get_all_from_model_id($model_id)
	{
		$url = config('witter.api_base') . 'models/'. $model_id .'/bodies';
		$obj = self::get_request($url);
		
		return $obj;
	}

	/**
	 * Get a single body
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_single_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'/slug?' . http_build_query($args);
		$obj = self::get_request($url);
		
		return $obj;
	}
	
	/**
	 * Get a single manufacturer by id 
	 *
	 * @params string
	 * @return Object
	 */
	static public function get_by_id(string $id)
	{
		$url = config('witter.api_base') . self::URI . '/' . $id;
		$obj = self::get_request($url);

		return $obj;
	}
}