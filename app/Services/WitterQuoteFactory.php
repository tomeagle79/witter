<?php

namespace App\Services;

use App\Contracts\TenantLocaleContext;
use App\Models\Api\WitterQuote;
use Log;

class WitterQuoteFactory
{
    private $tenant;

    public function __construct(TenantLocaleContext $tenant)
    {
        // Inject the TenantLocaleContext class
        $this->tenant = $tenant;
    }

    /**
     * Create the quote.
     *
     * @param  obj $quote
     * @return obj WitterQuote
     */
    public function create($quote)
    {
        // Get the data from the quote object
        $cart = json_decode($quote->quote_data, true);
        $customer_data = json_decode($quote->customer_data, true);

        // Build the product data arrays.
        $accessories_arr = [];
        $accessories_fitted_arr = [];
        $reg_and_colour_arr = [];
        $mobileFitCost = 0;
        $towbar_arr = [];
        $isMobile = 0;
        $bookingTime = null;

        if (!empty($cart['contents'])) {
            foreach ($cart['contents'] as $accessory) {
                // If the item is to be fitted then send appointment details.
                if ($accessory['type'] == 'accessory-fitted') {
                    $accessories_fitted_arr[] = [
                        'partNo' => $accessory['partNo'],
                        'Quantity' => 1,
                        'TimeslotId' => $accessory['appointmentId'],
                        'AppointmentDate' => $accessory['appointmentDate'],
                        // 'removalOption' => !is_null($accessory['removal_option']) && $accessory['removal_option'] === "true" ? 'Y' : 'N',
                        // 'removalCost' => !is_null($accessory['removal_cost']) ? $accessory['removal_cost'] : 0,
                        'IncludeRemoval' => !is_null($accessory['removal_option']) && $accessory['removal_option'] === "true" ? 1 : 0,
                    ];

                    // Get the appointment time
                    $bookingTime = $accessory['appointmentTime'];

                    if (!empty($accessory['fitterFee'])) {
                        $mobileFitCost = $mobileFitCost + $accessory['fitterFee'];
                    }

                    // Add true value to the isMobile flag.
                    if ($accessory['fitterType'] == 'mobile') {
                        $isMobile = 1;
                    }

                } else {
                    // Non fitted items
                    $accessories_arr[] = [
                        'partNo' => $accessory['partNo'],
                        'quantity' => 1,
                    ];
                }

                /*
            // As we are looping items in the cart. Check if any have had vehicle details added.
            if (!empty($checkout['sectionVehicle'][$accessory['cart_item_id']])) {
            $reg_and_colour_arr[] = [
            'partNo' => $accessory['partNo'],
            'RegistrationNo' => $checkout['sectionVehicle'][$accessory['cart_item_id']]['registration_no'],
            'VehicleColour' => $checkout['sectionVehicle'][$accessory['cart_item_id']]['vehicle_colour'],
            ];
            }
             */
            }
        }

        // Towbar details
        if (!empty($cart['towbar'])) {
            if ($cart['towbar']['fitterType'] == 'self_fitted') {
                $accessories_arr[] = [
                    'AdditionalInfo' => $cart['towbar']['variantTowbarId'],
                    'partNo' => $cart['towbar']['partNo'],
                    'quantity' => 1,
                ];

                // If the towball_option cost money then add it to the array
                if (!empty($cart['towbar']['towball_option_price'])) {
                    $accessories_arr[] = [
                        'partNo' => $cart['towbar']['towball_option'],
                        'quantity' => 1,
                    ];
                }

                if (!empty($cart['towbar']['electric_option'])) {
                    $accessories_arr[] = [
                        'partNo' => $cart['towbar']['electric_option'],
                        'quantity' => 1,
                    ];
                }
            } else {
                // Normal fitted towbar
                $towbar_arr = [
                    'vtId' => $cart['towbar']['variantTowbarId'],
                    'timeslotId' => $cart['towbar']['appointmentId'],
                    'appointmentDate' => $cart['towbar']['appointmentDate'],
                    'towballCode' => $cart['towbar']['towball_option'],
                    'electricKitCode' => $cart['towbar']['electric_option'],
                    'recodeRequested' => $cart['towbar']['softwareUpgrade'],
                ];

                if (!empty($cart['towbar']['fitterFee'])) {
                    $mobileFitCost = $mobileFitCost + $cart['towbar']['fitterFee'];
                }

                // Get the appointment time
                $bookingTime = $cart['towbar']['appointmentTime'];

                // Add true value to the isMobile flag.
                if ($cart['towbar']['fitterType'] == 'mobile') {
                    $isMobile = 1;
                }
            }
        }

        // Promo code
        $promo_code = '';
        if (!empty($cart['promotion'])) {
            if (!empty($cart['promotion']['promotionCode'])) {
                $promo_code = $cart['promotion']['promotionCode'];
            }
        }

        // Check if we are redirecting users from this site.
        if (empty($this->tenant->getWebsite()->redirect_url)) {

            $quote_link = route('quote.retrieve', $quote->quote_link_ref);
        } else {
            // Build the link users need to retrieve their quote
            $quote_link = $this->tenant->getWebsite()->redirect_url . 'my-quote/' . $quote->quote_link_ref;
        }

        // Build the quote data
        $quote_data = [
            'quoteID' => $quote->quote_ref,
            'quoteLink' => $quote_link,
            'customerFirstName' => $customer_data['first_name'],
            'customerSurname' => $customer_data['surname'],
            'customerTelephone' => $customer_data['telephone'],
            'customerEmail' => $customer_data['email'],
            'partnerID' => $customer_data['email'],
            'orderItems' => $accessories_arr,
            'webfitDetails' => $towbar_arr,
            'promotionCode' => $promo_code,
            'totalCost' => $cart['grand_total'],
            'AccessoryFittings' => $accessories_fitted_arr,
            'MobileFitCost' => $mobileFitCost,
            'IsMobile' => $isMobile,
            'BookingTime' => $bookingTime,
            'RegistrationNo' => $quote->registration_no
        ];

        // Create the WitterQuote with the quote_data array.
        $witter_quote = WitterQuote::create_quote($quote_data);

        if (empty($witter_quote->success)) {
            Log::error('Error :: Witter Quote Failed.');
            Log::error('Error :: Witter Quote = ' . json_encode($quote_data));
        }

        return $witter_quote;
    }
}
