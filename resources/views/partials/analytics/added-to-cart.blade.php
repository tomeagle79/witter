// 
gtag('event', 'add_to_cart', {
  "items": [{
        'name': '{{ $product['name'] }}',
        'id': '{{ $product['id'] }}',
        'price': '{{ $product['price'] }}',
        'quantity': '{{ $product['quantity'] }}',
        'brand': '{{ $product['brand']}}',
        'category': '{{ $product['category'] }}'
    }]
});