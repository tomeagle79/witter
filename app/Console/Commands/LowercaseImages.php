<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Image;
use App\Models\BlogPost;
use Storage;

class LowercaseImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get the image object
        $images = Image::get();

        // Loop images and force name to lowercase
        foreach($images as $image) {

            // current file
            $original_file = base_path('public_html' . $image->path . $image->filename);

            // new file
            $new_file = strtolower($original_file);

            if(file_exists($original_file)) {

                rename($original_file, $new_file);

                // Update the filename in the images table.
                $image->filename = strtolower($image->filename);
                $image->save();

            } else {

                $this->info('File does not exist.');
            }

            $this->info($original_file . ' -> ' . $new_file);
            $this->info($image->filename . ' Image id =' . $image->id);
        }
    }
}
