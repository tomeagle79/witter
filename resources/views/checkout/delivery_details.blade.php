@extends('layouts.frontend')

@section('title', 'Checkout - Step 2, Delivery Details')

@section('body-class', 'body_bg_checkout')

@section('heading')
	Checkout - Step 2, Delivery Details
@stop

@section('meta_description')
	Checkout
@stop

@section('content')

<div class="container checkout-process">

    {!! Form::open(['route' => 'checkout.section2_submit', 'class' => 'form-horizontal', 'id' => 'checkout-form']) !!}
    <div class="row">
        <div class="col-xs-12">
            @if(!empty($fitting_type) && $fitting_type == 'mobile')
                <h1>Fitting Address</h1>
            @else
                <h1>Delivery Details</h1>
            @endif
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->

	<div class="row">

        <div class="col-xs-12  col-md-9">

            <div class="section">

                @if(!empty($fitting_type) && $fitting_type == 'standard')

                    <div class="delivery-notice">
                        @if($fitting_type == 'mobile')
                            <i class="icon-mobile-fitter"></i>
                        @else
                            <i class="icon-garage-fitting"></i>
                        @endif
                        <span>The following item(s) will be delivered to your fitter:</span>
                    </div><!-- /.delivery-notice -->

                    <div class="mobile-box">
                        @if(!empty($cart['towbar']))
                            @include('cart.partials.cart_item', ['edit' => false, 'item' => $cart['towbar']])
                            @if(!empty($cart['contents']))
                                <hr>
                            @endif
                        @endif

                        @if(!empty($cart['contents']))
                            @foreach($cart['contents'] as $item )
                                @include('cart.partials.cart_item', ['edit' => false, 'item' => $item])
                                @if(!$loop->last)
                                    <hr>
                                @endif
                            @endforeach
                        @endif
                    </div><!-- /.mobile-box -->

                @else
                    <div class="form-group">
                        <div class="col-xs-12 ">
                            <div class="checkbox">
                                @if(!empty($fitting_type) && $fitting_type == 'mobile')

                                    Because you selected a mobile fitter your fitting address is where the fitting will take place.<br><br>

                                    You must use the same postcode as the one entered when choosing your product.
                                @else
                                    <label class="checkbox-lg">The delivery address is the same as my <br>billing address.
                                            {!! Form::checkbox('copy_from',
                                            true,
                                            false,
                                            ['id'=>'copy_from']) !!}
                                        <span class="checkmark"></span>
                                    </label>
                                @endif
                            </div> <!-- .checkbox -->
                        </div> <!-- .col -->
                    </div>
                    <hr>
                    <div class="form-group has-feedback">
                        <label for="telephone" class="col-xs-12">Telephone *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('delivery_telephone',
                                (empty($checkout['section2']['delivery_telephone']) ? null : $checkout['section2']['delivery_telephone']),
                                ['required', 'data-required-error'=>'Please enter your delivery telephone number.', 'class'=>'form-control', 'id'=>'delivery_telephone',  'placeholder' => 'Telephone *']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <hr>

                    <div class="form-group has-feedback">
                        <label for="delivery_postcode" class="col-xs-12">Postcode *</label>
                        <div class="col-sm-6 col-xs-12">
                            <div class="input-group">

                                @if(!empty($customer_postcode))
                                    {!! Form::text('delivery_postcode',
                                        $customer_postcode,
                                        ['required', 'data-required-error'=>'Please enter your delivery address postcode.', 'class'=>'form-control',  'id'=>'delivery_postcode', 'placeholder' => 'Postcode *',  'readonly']) !!}
                                @else
                                    {!! Form::text('delivery_postcode',
                                        (empty($checkout['section2']['delivery_postcode']) ? null : $checkout['section2']['delivery_postcode']),
                                        ['required', 'data-required-error'=>'Please enter your delivery address postcode.', 'class'=>'form-control',  'id'=>'delivery_postcode', 'placeholder' => 'Postcode *']) !!}
                                @endif

                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="address-lookup" type="button">Address Lookup</button>
                                </span>
                            </div> <!-- .input-group -->

                            <div class="alert alert-help alert-help-warning" id="postcode-alert" style="display:none;"><p></p></div>
                            <div class="alert alert-help alert-help-info" id="loading" style="display:none;"><p>Loading...</p></div>

                            <div class="address-lookup-wrapper">
                                <div class="dropdown">
                                    {!! Form::select('address_dropdown',
                                        [],
                                        null,
                                        ['class' => 'form-control', 'id' => 'address_dropdown']) !!}
                                </div><!-- /.dropdown -->
                            </div><!-- /.address-lookup-wrapper -->
                            <span class="form-control-feedback form-control-feedback-dropdown" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div>

                    <div class="form-group has-feedback">
                        <label for="delivery_address1" class="col-xs-12">Address 1 *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('delivery_address1',
                                (empty($checkout['section2']['delivery_address1']) ? null : $checkout['section2']['delivery_address1']),
                                ['required', 'data-required-error'=>'Please enter the first line of your delivery address.',  'id'=>'delivery_address1', 'class'=>'form-control']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->


                    <div class="form-group has-feedback">
                        <label for="delivery_address2" class="col-xs-12">Address 2</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('delivery_address2',
                                (empty($checkout['section2']['delivery_address2']) ? null : $checkout['section2']['delivery_address2']),
                                ['class'=>'form-control', 'id'=>'delivery_address2']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="delivery_towncity" class="col-xs-12">Town / City *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('delivery_towncity',
                                (empty($checkout['section2']['delivery_towncity']) ? null : $checkout['section2']['delivery_towncity']),
                                ['required', 'data-required-error'=>'Please enter your delivery address town or city.',  'id'=>'delivery_towncity', 'class'=>'form-control']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="delivery_county" class="col-xs-12">County *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('delivery_county',
                                (empty($checkout['section2']['delivery_county']) ? null : $checkout['section2']['delivery_county']),
                                ['required', 'data-required-error'=>'Please enter your delivery address county.', 'id'=>'delivery_county', 'class'=>'form-control']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="delivery_country" class="col-xs-12">Country *</label>
                        <div class="col-sm-6 col-xs-12">
                            <div class="dropdown">
                                {!! Form::select('delivery_country', [
                                        'United Kingdom' => 'United Kingdom
                                    '], 'United Kingdom', [
                                        'required',
                                        'data-required-error'=>'Please select your delivery address country.',
                                        'id' => 'delivery_country',
                                        'class' => 'form-control',
                                    ]) !!}
                            </div><!-- /.dropdown -->
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group" id="no-delivery-msg">
                    <p class="col-xs-10">Unfortunately, we can only deliver to the UK at the moment. However please do get in <a href="tel:{{ config('witter.contact.telephone') }}">contact</a> with us to discuss other options.</p>
                    </div>

                @endif {{-- If fitting_type. If item fitted --}}

                <br>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-6">
                            <button type="submit" class="btn btn-tertiary btn-large pull-right">Step 3 - Summary &amp; Payment</button>
                    </div><!-- /.col-xs-12 -->

                    <div class="hidden-xs hidden-sm col-sm-6">
                            <a href="{{ route('checkout') }}" type="submit" class="btn btn-standard btn-standard-backward pull-right">Back</a>
                    </div><!-- /.col-xs-12 -->

                </div><!-- /.form-group -->
            </div> <!-- .section -->

		</div> <!-- .col -->
        <div class="hidden-xs hidden-sm col-md-3">

            @include('checkout.partials.sidebar-checkout')

        </div> <!-- .col-md-3 -->
    </div>
    {!! Form::close() !!}
</div>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){

            // Add this to our scroll to values
            var $header_height = $('header').outerHeight() + 5;

            $('#checkout-form').validator({
                feedback: {
                    success: 'icon-simple-tick',
                    error: 'icon-alert'
                },
                disable: false
            });

            // Add to the scroll to value of validation
            $.fn.validator.Constructor.FOCUS_OFFSET = $header_height;

            $(document).on('submit', '#checkout-form', function() {
                @include('partials.analytics.checkout-progress', [
                    'cart' => $cart
                ])

                @include('partials.analytics.checkout-progress-step-2')
            });

            var section1_address1  = '{{ (empty($checkout['section1']['billing_address1']) ? '' : $checkout['section1']['billing_address1']) }}';
            var section1_address2  = '{{ (empty($checkout['section1']['billing_address2']) ? '' : $checkout['section1']['billing_address2']) }}';
            var section1_towncity  = '{{ (empty($checkout['section1']['billing_towncity']) ? '' : $checkout['section1']['billing_towncity']) }}';
            var section1_county    = '{{ (empty($checkout['section1']['billing_county'])   ? '' : $checkout['section1']['billing_county']) }}';
            var section1_country   = '{{ (empty($checkout['section1']['billing_country'])  ? '' : $checkout['section1']['billing_country']) }}';
            var section1_postcode  = '{{ (empty($checkout['section1']['billing_postcode']) ? '' : $checkout['section1']['billing_postcode']) }}';
            var section1_telephone = '{{ (empty($checkout['section1']['telephone'])        ? '' : $checkout['section1']['telephone']) }}';

            // Copy from
            $('#copy_from').on('click', function(){
                if($(this).is(':checked')) {
                    $('#delivery_postcode').val(section1_postcode).trigger('input');
                    $('#delivery_address1').val(section1_address1).trigger('input');
                    $('#delivery_address2').val(section1_address2).trigger('input');
                    $('#delivery_towncity').val(section1_towncity).trigger('input');
                    $('#delivery_county').val(section1_county).trigger('input');
                    $('#delivery_country').val(section1_country).trigger('input');
                    $('#delivery_telephone').val(section1_telephone).trigger('input');
                }
            });

            // Address lookup button
            $('#address-lookup').on('click', function(){
                $('#postcode-alert').hide();
                $('#loading').hide();
                $('.address-lookup-wrapper').hide();

                var $postcode = $(this).parent().prev();
                if($postcode.val().length == 0) {
                    $('#postcode-alert p').html('Please enter a postcode');
                    $('#postcode-alert').fadeIn('slow');
                } else {
                    $('#loading').fadeIn('slow');

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('postcode.lookup')}}",
                        data: {
                            'postcode': $postcode.val()
                        },
                        success: function(result){
                            $('#loading').fadeOut('slow');

                            if(result.success == false) {
                                $('#postcode-alert p').html(result.errorMessage);
                                $('#postcode-alert').fadeIn('slow');
                            } else {
                                // Success!
                                var $options = $('#address_dropdown');
                                $options.html('<option>Please select</option>');
                                $.each(result.addressList, function(){
                                    $options.append($("<option />").val(this.key).text(this.value));
                                });
                                $('.address-lookup-wrapper').fadeIn('slow');
                            }
                        },
                        error: function(){
                            $('#postcode-alert p').html('Sorry, something went wrong');
                            $('#postcode-alert').fadeIn('slow');
                        }
                    });
                }
            });

            // Address lookup dropdown selection
            $(document).on('change', '#address_dropdown', function() {

                if($(this).val().length !== 0) {
                    // Look up the full address
                    var key = $(this).val();

                    $('.address-lookup-wrapper').fadeOut('slow');

                    $('#loading').fadeIn('slow');

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('postcode.get_addresses')}}",
                        data: {
                            'key': key
                        },
                        success: function(result){
                            $('#loading').fadeOut('slow');

                            if(result.success == false) {
                                $('#postcode-alert p').html(result.errorMessage);
                                $('#postcode-alert').fadeIn('slow');
                            } else {
                                // Success!
                                $('#delivery_address1').val( (result.organisationName.length == 0 ? '' : result.organisationName + ', ') + result.line1 ).trigger('input');
                                $('#delivery_address2').val( result.line2 ).trigger('input');
                                $('#delivery_towncity').val( result.postTown ).trigger('input');
                                $('#delivery_county').val( result.county ).trigger('input');
                            }
                        },
                        error: function(){
                            $('#postcode-alert p').html('Sorry, something went wrong');
                            $('#postcode-alert').fadeIn('slow');
                        }
                    });

                }

            });

        });
    </script>
@stop
