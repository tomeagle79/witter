@extends('layouts.backend')

@section('actions')
	<a href="{!! URL::backend('help_articles/create') !!}" class="pull-right btn btn-success">Add Help &amp; Advice Article</a>
@stop

@section('title')
	Help &amp; Advice Articles
@stop

@section('scripts')
<script>
$(document).ready(function(){
	$('.datatable').DataTable( {
        "ajax": "{!! URL::backend('help_articles/index_datatables') !!}",
        "processing": true,
        "serverSide": true,
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            {"searchable": false, "orderable": false},
            {"searchable": false, "orderable": false},
        ]
    } );
});
</script>
@stop

@section('content')

<table class="table table-hover table-striped table-bordered datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Featured</th>
			<th>Published</th>
			<th>Created</th>
			<th>Updated</th>
			<th width="50"></th>
			<th width="50"></th>
		</tr>
	</thead>
	<tbody></tbody>
</table>

@stop


