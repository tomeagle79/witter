gtag('event', 'view_item', {
    'items': [{

        @if($category == 'Towbars')
            'name': '{{ str_replace("'", '', $product->title) }}',
            'id': '{{ $product->towbarPartNo ?? '' }}',
            'price': '{{ $product->price }}',
            @if(!empty($product->brandName))
            'brand': '{{ $product->brandName }}',
            @endif
            @if(!empty($category))
            'category': '{{ $category }}'
            @endif
        @endif

        @if($category == 'Accessories')
            'name': '{{ str_replace("'", '', $product->title) }}',
            'id': '{{ $product->partNo }}',
            'price': '{{ $product->displayPrice }}',
            @if(!empty($product->brandName))
            'brand': '{{ $product->brandName }}',
            @endif
            @if(!empty($category))
            'category': '{{ $category }}'
            @endif
        @endif

        @php
            $arr = [
                'Roof Systems/Accessories',
                'Roof Systems/By Manufacturer',
                'Roof Systems/Roof Bars',
                'Roof Systems/Roof Racks',
                'Electrical Kits',
                'Bike Racks'
            ];
        @endphp

        @if(in_array($category, $arr))
            'name': '{{ str_replace("'", '', $product->title) }}',
            'id': '{{ $product->partNo }}',
            'price': '{{ $product->price }}',
            @if(!empty($product->brandName))
            'brand': '{{ $product->brandName }}',
            @endif
            @if(!empty($category))
            'category': '{{ $category }}'
            @endif
        @endif
    }]
});