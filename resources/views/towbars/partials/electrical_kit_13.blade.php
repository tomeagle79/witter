
    <h4>13-pin Vehicle Specific Electrics</h4>

    <p>
        You will need a 13-pin Vehicle Specific Electrics kit for towing a caravan because it provides all of the legal lighting requirements for towing, as well as powering the electrics inside your caravan. Most caravans, manufactured after January 2009, will have 13-pin electrics and will also connect to your reverse lights whilst providing a full power supply inside your caravan.
    </p>

    <h4>What can a 13-pin electric socket power?</h4>

    <div class="powered">
        <ul class="row towbar-includes">
            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Left indicator</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Right indicator</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Taillights</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Fog lights</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Brake lights</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Reverse lights</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Caravan electrics (inside)</div>
            </li> <!-- col-sm-6 -->
        </ul>
    </div><!-- /.powered -->

    <div class="alert alert-danger">
        <p>
            {{-- *Our dedicated wiring kits synchronise perfectly with your car’s electrical system and, depending on how sophisticated your car is, turn signal failure could also be activated by your vehicle’s warning system. You might need a software update to activate additional benefits and safety features. You may need to pay an additional fee for the software update. --}}
            *Our dedicated wiring kits synchronise perfectly with your car’s electrical system and, depending on how sophisticated your car is, turn signal failure could also be activated by your vehicle’s warning system. You might need a software update to activate additional benefits and safety features. You can book the software upgrade in the next step.
        </p>
    </div><!-- /.alert -->
