<?php

return [

    'api_key' => env('MAILCHIMP_API_KEY', false),

    // General test MailChimp list id.
    'list_id' => env('MAILCHIMP_LIST_ID', 'e41914ad75'),
    // Mailchimp lists
    'contact_list' => env('MAILCHIMP_CONTACT_LIST_ID', '0e5ef9e6e7'),
    'market_research_list' => env('MAILCHIMP_MARKET_RESEARCH_LIST_ID', '6c7af52bdc'),
];
