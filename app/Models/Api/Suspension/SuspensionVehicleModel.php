<?php

namespace App\Models\Api\Suspension;

use App\Models\Api\ApiModel;

class SuspensionVehicleModel extends ApiModel
{
    /**
     * Get all models based on manufacturer ID
     *
     * @param Int
     * @return Object
     */
    public static function get_all_from_manufacturer_id($manufacturer_id)
    {
        $url = config('witter.api_base') . 'suspensions/manufacturers/' . $manufacturer_id . '/models';
        $obj = self::get_request($url);

        return $obj;
    }
}
