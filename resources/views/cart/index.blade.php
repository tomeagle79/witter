@extends('layouts.frontend')

@section('title')
	Cart
@stop

@section('heading')
	Your Basket
@stop

@section('body-class', 'body_bg_checkout')

@section('meta_description')
	Cart and basket pages
@stop

@section('content')

<div class="container checkout-process cart">
    <div class="row">
        <div class="col-xs-12 col-md-9">
            @if(!empty($cart['towbar']))
                <div class="section">
                    @include('cart.partials.cart_item', ['edit' => true, 'item' => $cart['towbar']])
                </div><!-- /.section -->
            @endif

            @if(!empty($cart['contents']))
                @foreach($cart['contents'] as $item)
                    <div class="section">
                        @include('cart.partials.cart_item', ['edit' => true, 'item' => $item])
                    </div><!-- /.section -->
                @endforeach
            @endif
        </div><!-- /.col-xs-12 -->

        <div class="col-xs-12  col-md-3">

            @include('cart.partials.cart_sidebar')

        </div> <!-- .col-md-3 -->
    </div><!-- /.row -->

</div> <!-- .container -->


<div class="modal fade find-your-fitter" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="loading-modal-body"></div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
                <h4 class="modal-title">Update fitting date and time</h4>
            </div> <!-- .modal-header -->
            <div class="modal-body">
            </div> <!-- .modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@include('partials.products_list', ['list_title' => 'Customers also bought...', 'list_products' => $alsobought])

@stop

@section('scripts')

@if(!empty($cart['quote_ref']))
    <script type="text/javascript" src="{{ empty($assets['assets/js/cart_appointments.js']) ? URL::asset('assets/js/cart_appointments.js') : URL::asset($assets['assets/js/cart_appointments.js']) }}"></script>

@endif

<script type="text/javascript">
    @if(session('product_added'))
        @include('partials.analytics.added-to-cart', [
            'product' => session('product_added')
        ])
    @endif

    $(document).ready(function() {
        $('#begin_checkout').on('click', function(e) {

            @if(!empty($cart['webfit']) && !empty($cart['quote_ref']) && !empty($cart['new_appointment_required']))

                if($('#valid_basket').val() == 'false') {
                    e.preventDefault();
                    $('#appointment_not_changed').fadeIn('slow');
                }
            @endif

            @include('partials.analytics.begin-checkout', [
                'cart' => $cart
            ])
        });

        @if(!empty($allow_date_change))

            var fitters_url = "{{ route('appointments.cart_ajax_list_fitters') }}";

            var accessory_fitters_url = "{{ route('appointments.cart_ajax_list_accessory_fitters') }}";

            var appointments_url = "{{ route('appointments.cart_ajax_list_dates') }}";

            // 'new_appointment' click to display list of appointments for selected fitter.
            $('#new_appointment').on('click', function() {

                var $element = $(this);

                callAppointments(appointments_url, $element);

                $('.find-your-fitter').modal('show');
            });

            // Load the list of fitters in the modal based on the 'customer_postcode' value
            $('#modal').on('click', '#list_fitters', function () {

                var postcode = $('input[name=customer_postcode]').val();

                // Depending on the fitting appointment type find fitters
                if($('#appointment_type').val() == 'towbar'){

                    findFitters(fitters_url, postcode);
                }else{
                    findAccessoryFitters(accessory_fitters_url, postcode);
                }
            });

            // Load the list of fitters based on the 'postcode_update' value
            $('#modal').on('click', '#update', function() {

                var postcode = $('#postcode_update').val();

                // Depending on the fitting appointment type find fitters
                if($('#appointment_type').val() == 'towbar'){

                    findFitters(fitters_url, postcode);
                }else{
                    findAccessoryFitters(accessory_fitters_url, postcode);
                }
            });

        @endif

    });
</script>

@stop