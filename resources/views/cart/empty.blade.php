@extends('layouts.frontend')

@section('title')
	Cart
@stop

@section('heading')
	Your Basket
@stop

@section('body-class', 'body_bg_checkout')

@section('meta_description')
	Cart and basket pages
@stop

@section('content')

	<div class="container checkout-process cart">
		<div class="row">

			<div class="col-md-12">
				<p>Your cart is empty, why not take a look at our <a href="{{ route('towbars') }}">towbars</a>?
			</div> <!-- col-md-12 -->

		</div> <!-- .row -->
	</div> <!-- .container -->

@include('partials.products_list', ['list_title' => 'Popular products...', 'list_products' => $alsobought])

@stop
