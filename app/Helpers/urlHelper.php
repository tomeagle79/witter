<?php 

function encode_url ($url) {
    // Some URLs have slashes in, so we can't URL encode due to inconsistencies with 
    // Apache.  Apache doesn't needs the AllowEncodedSlashes directive On, but this 
    // cannot be set at .htaccess level.  Instead we'll replace the / with something else 
    // and then url encode as normal. 

    $new_url = str_replace( '/', '|||', $url );
   // $new_url = urlencode($new_url);

    return $new_url;
}

function decode_url ($url) {
    // Opposite of above

    //$new_url = urldecode($url);
    $new_url = str_replace( '|||', '/', $url );

    return $new_url;
}