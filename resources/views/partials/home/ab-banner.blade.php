

<div class="blue-banner hidden-lg hidden-md hidden-sm">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <img src="/assets/img/icons/box.svg" alt="">
                Free UK Delivery
            </div>
            <div class="col-xs-6">
                <img src="/assets/img/icons/spanner.svg" alt="">
                Nationwide Fitting
            </div>
        </div>
    </div>
</div>

<div id="home-banner" class="ab-test" style="background-image: url(/assets/img/home/witter-hp.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 title-part">
                <h2 class="title">ALL-IN-ONE Towbar Fitting</h2>
                <p class="lead">Towbar + Electric Kit + Fitting</p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 search-part">
                <div class="tabs">
                    <div class="tab active" id="reg" data-type="reg">
                        <p>Find by vehicle registration</p>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </div>
                    <div class="tab" id="make" data-type="make">
                        <p>Find by make and model</p>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="slide by-reg" id="regSlide">
                    <div class="row">
                        <div class="bg">
                            <p class="lead">Let's find <br>your vehicle</p>

                            {!! Form::open(['route' => 'search.numberplate', 'class' => 'form numberplate-search float-right', 'method' => 'get', 'onsubmit' => "
                                gtag('event', 'Success', {
                                    event_category: 'Reg lookup',
                                    event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                                });
                            "]) !!}
                                {!! Form::text('search', null, ['required', 'class' => 'form-control', 'placeholder' => 'Registration Number']) !!}
                                <button type="submit"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="slide by-make" id="makeSlide">
                    <div class="row">
                        <div class="col-xs-2">
                            <p>Let's find <br>your vehicle</p>
                        </div>
                        <div class="col-xs-1"></div>
                        {!! Form::open(['url' => '/', 'id' => 'search-dropdown-form', 'class' => 'search-dropdown-form', 'onsubmit' => "
                            gtag('event', 'Success', {
                                event_category: 'Model select',
                                event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                            });
                        "]) !!}
                            <div class="bg">
                                <select name="manufacturer" id="manufacturer" class="manufacturer" data-style="btn-select-box">
                                    <option>MAKE</option>
                                    @foreach($manufacturers_dropdown as $mdrop)
                                        <option value="{{ $mdrop->manufacturerId }}" data-slug="{{ $mdrop->slug }}">{{ $mdrop->manufacturerName }}</option>
                                    @endforeach
                                </select>

                                {{ Form::select('models', [null => 'Model'], null, ['id' => 'model', 'class'=>'model', 'data-style' => 'btn-select-box']) }}
                                {{ Form::select('bodies', [null => 'Body'], null, ['id' => 'body', 'class'=>'body', 'data-style' => 'btn-select-box']) }}
                                {{ Form::select('registrations', [null => 'Registration'], null, ['id' => 'registration', 'class'=>'registration', 'data-style' => 'btn-select-box']) }}

                                {{ Form::hidden('manufacturerSlug', '', ['id' => 'manufacturerSlug', 'class'=>'manufacturerSlug']) }}
                                {{ Form::hidden('modelSlug', '', ['id' => 'modelSlug', 'class'=>'modelSlug']) }}
                                {{ Form::hidden('bodySlug', '', ['id' => 'bodySlug', 'class'=>'bodySlug']) }}
                                {{ Form::hidden('registrationId', '', ['id' => 'registrationId', 'class'=>'registrationId']) }}

                                <button type="submit" onclick="
                                    gtag('event', 'Success', {
                                        event_category: 'Model select',
                                        event_label: '{{ $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] }}'
                                    });
                                "><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mobile-inner">
    <div class="row hidden-lg hidden-md hidden-sm">
        <div class="col-xs-12">
            <h3>Let’s find your vehicle</h3>
        </div>
    </div>
    <div class="search-part search-part-mobile">
        <div class="tabs clearfix">
            <div class="tab active" id="reg" data-type="reg">
                <p>Reg Number</p>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </div>
            <div class="tab" id="make" data-type="make">
                <p>Make and model</p>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </div>
        </div>
        <div class="slide by-reg" id="regSlide">
            <div class="row">
                {!! Form::open(['route' => 'search.numberplate', 'class' => 'form numberplate-search float-right', 'method' => 'get', 'onsubmit' => "
                    gtag('event', 'Success', {
                        event_category: 'Reg lookup',
                        event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                    });
                "]) !!}
                    <div class="col-xs-9">
                        {!! Form::text('search', null, ['required', 'class' => 'form-control', 'placeholder' => 'Registration Number']) !!}
                    </div>
                    <div class="col-xs-3">
                        <button type="submit"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="slide by-make" id="makeSlide">
            <div class="row">
                {!! Form::open(['url' => '/', 'id' => 'search-dropdown-form', 'class'=>'search-dropdown-form', 'onsubmit' => "
                    gtag('event', 'Success', {
                        event_category: 'Model select',
                        event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                    });
                "]) !!}
                    <select name="manufacturer" id="manufacturer" class="manufacturer" data-style="btn-select-box">
                        <option>MAKE</option>
                        @foreach($manufacturers_dropdown as $mdrop)
                            <option value="{{ $mdrop->manufacturerId }}" data-slug="{{ $mdrop->slug }}">{{ $mdrop->manufacturerName }}</option>
                        @endforeach
                    </select>

                    {{ Form::select('models', [null => 'Model'], null, ['id' => 'model', 'class'=>'model', 'data-style' => 'btn-select-box']) }}
                    {{ Form::select('bodies', [null => 'Body'], null, ['id' => 'body', 'class'=>'body', 'data-style' => 'btn-select-box']) }}
                    {{ Form::select('registrations', [null => 'Registration'], null, ['id' => 'registration', 'class'=>'registration', 'data-style' => 'btn-select-box']) }}

                    {{ Form::hidden('manufacturerSlug', '', ['id' => 'manufacturerSlug', 'class'=>'manufacturerSlug']) }}
                    {{ Form::hidden('modelSlug', '', ['id' => 'modelSlug', 'class'=>'modelSlug']) }}
                    {{ Form::hidden('bodySlug', '', ['id' => 'bodySlug', 'class'=>'bodySlug']) }}
                    {{ Form::hidden('registrationId', '', ['id' => 'registrationId', 'class'=>'registrationId']) }}

                    <button type="submit" class="btn btn-green">Find my vehicle</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<div class="banner-carousel ab hidden-xs">
    <div class="container-fluid blue-transparent-banner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="blue-transparent-banner-flex">
                        <div>
                            <p><i class="icon-white-block"></i> Free UK Delivery</p>
                        </div>
                        <div>
                            <p><i class="icon-mobile-fitter"></i> Nationwide <span class="hidden-xs hidden-sm hidden-md">Mobile</span> Fitting</p>
                        </div>
                        <div class="hidden-xs">
                            <p><i class="icon-trusted"></i> Trusted Since 1950</p>
                        </div>
                        <div class="hidden-xs">
                            <a href="{!! route('blog_dynamic_route', 'what-is-webfit') !!}" title="WEBFIT IS HERE">
                                <p><i class="icon-webfit"></i> <span>WEBFIT IS HERE, CLICK <br>TO FIND OUT MORE</span></p>
                            </a>
                        </div>
                    </div><!-- /.blue-transparent-banner-flex -->
                </div><!-- /.col-xs-12 -->
            </div><!-- /.row -->
        </div>
    </div>
</div>



@section('scripts')
@parent
<script>
    $(document).ready(function() {
        // Send the full url of where our vehicle ajax endpoints are
        setUpVehicleDropdowns( "{{ URL('vehicles') }}" );

        // Send the starting/first slug to build the url and redirect the user.
        setUpVehicleRedirect("/");

        /*
        * Dynamic Dropdowns call a url to get the next dropdowns list of options
        *
        */
        $.fn.dynamicDropdowns = function( $url ) {

            $selectbox = $(this);

            $selectbox.find("option:gt(0)").remove();

            $.getJSON( $url, function( data ) {
                $.each( data, function( key, val ) {

                    $selectbox.append("<option value='" + val.index + "' data-slug='" + val.slug + "'>" + val.name + "</option>");
                });
            }).always(function() {
            });
        };

        /*
        * Clear the dropdowns.
        * So if the user selects back there will be no issues with empty or untidy dropdowns.
        *
        */
        function clearDropdownSelection(select){
            var selectedValue = select.find('option[selected]').val();

            if ( selectedValue ) {
                select.val( selectedValue );
            } else {
                select.prop( 'selectedIndex', 0 );
            }
        }

        /*
        * Set up the dynamic vehicle dropdowns on the page
        *
        */
        function setUpVehicleDropdowns(vehicle_url_call){
            // When manufacturer dropdown changed update the other dropdowns
            if ( $( '.manufacturer' ).length ) {

                // If the manufacturer dropdown has no data then call it via ajax
                if( $(".manufacturer option").length <= 1){

                    $(".manufacturer").dynamicDropdowns( vehicle_url_call + "/ajax_manufacturers" );
                }

                $( '.manufacturer' ).change(function() {

                    // Check if its not the first default empty option.
                    if($( this ).prop('selectedIndex') == 0){

                        // Clear the model dropdowns if no data to call.
                        $( '.model' ).find('option:gt(0)').remove();

                        $('.manufacturerSlug').val("");
                    } else {
                        // Call the data.
                        $( '.model' ).dynamicDropdowns( vehicle_url_call + '/ajax_models/' + $(this).val());

                        $( '.manufacturerSlug' ).val($('option:selected', this).attr('data-slug'));
                    }

                    // Clear the dropdowns
                    $( '.body' ).find('option:gt(0)').remove();
                    $( '.registration' ).find('option:gt(0)').remove();

                    // Clear the slug values
                    $('.modelSlug').val("");
                    $('.bodySlug').val("");
                    $('.registrationId').val("");
                });

                clearDropdownSelection( $( '.manufacturer' ) );
            }

            // When model dropdown changed update the next bodies dropdown
            if ( $( '.model' ).length ) {
                $('.model').change(function() {

                    // Check if its not the first default empty option.
                    if($( this ).prop('selectedIndex') == 0){

                        // Clear the model dropdowns if no data to call.
                        $( '.body' ).find('option:gt(0)').remove();

                        $('.modelSlug').val("");
                    } else {
                        // Call the data.
                        $('.body').dynamicDropdowns( vehicle_url_call + '/ajax_bodies/' + $(this).val());

                        $('.modelSlug').val($('option:selected', this).attr('data-slug'));
                    }

                    // Clear any dropdowns
                    $('.registration').find('option:gt(0)').remove();

                    // Clear the slug values
                    $('.bodySlug').val("");
                    $('.registrationId').val("");
                });

                clearDropdownSelection( $( '.model' ) );
            }

            // When body dropdown changed update the next registrations dropdown
            $('.body').change(function() {

                // Check if its not the first default empty option.
                if($( this ).prop('selectedIndex') == 0){

                    // Clear the model dropdowns if no data to call.
                    $( '.registration' ).find('option:gt(0)').remove();
                    $('.bodySlug').val("");

                } else {
                    // Call the data.
                    $('.registration').dynamicDropdowns( vehicle_url_call + '/ajax_registrations/' + $(this).val());

                    $('.bodySlug').val($('option:selected', this).attr('data-slug'));
                }

                // Clear the slug values
                $('.registrationId').val("");
            });

            // When registration dropdown selected record the slug value.
            $('.registration').change(function() {

                // Check if its not the first default empty option.
                if($( this ).prop('selectedIndex') != 0){

                    $('.registrationId').val($('option:selected', this).attr('data-slug'));
                } else {
                    $('.registrationId').val("");
                }
            });

            clearDropdownSelection( $( '.body' ) );
            clearDropdownSelection( $( '.registration' ) );
        }

        /*
        * Set up the dynamic vehicle dropdowns form so when submitted call the correct functions
        *
        */
        function setUpVehicleRedirect(slug, validate){
            if(typeof validate === 'undefined'){
                validate = false;
            }

            $('.search-dropdown-form').on('submit', function(e){
                e.preventDefault();

                // Get the dropdown values.
                var manufacturer = $('.manufacturerSlug').val();
                var model = $('.modelSlug').val();
                var body = $('.bodySlug').val();
                var registration = $('.registrationId').val();

                // Only validate the last page before the car types
                if(validate){
                if(!dropdownValidate(body, registration)){
                        return false;
                    }
                }

                // Build a url from the slug values and redirect the user
                dropdownRedirects(slug, manufacturer, model, body, registration);
            });
        }

        /*
        * Validate that the required vehicle dropdowns have been selected.
        *
        */
        function dropdownValidate(body, registration){
            // If body not set then request it from the user
            if(!body.length) {
                $( '#dropdownError' ).html('<p class="alert alert-danger" role="alert">Please select a body from above.</p>');
                $( '#dropdownError' ).fadeIn('slow');
                return false;x
            }

            // If body not set then request it from the user
            if(!registration.length) {
                $( '#dropdownError' ).html('<p class="alert alert-danger" role="alert">Please select a registration from above.</p>');
                $( '#dropdownError' ).fadeIn('slow');
                return false;
            }

            return true;
        }

        /*
        * When dynamicDropdowns have been submitted build the url they are being redirected to
        * and redirect them to that url.
        */
        function dropdownRedirects(url, manufacturer, model, body, registration){
            // Set the default url starting slug as "towbars"
            url = '/towbars/';

            if(manufacturer.length) {
                url = url + manufacturer + '/';

                if(model.length) {
                    url = url + model + '/';

                    if(body.length) {

                        url = url + body + '/';

                        if(registration.length) {
                            $( '#dropdownError' ).fadeOut('slow');

                            url = url + registration + '/';
                        }
                    }
                }
            }

            window.location = url;
        }

        var reg = $(".tabs").find('[data-type=reg]');
        var make = $(".tabs").find('[data-type=make]');
        $('.tab').on('click', function() {
            var typeClicked = $(this).data('type');
            $('.tab').removeClass('active');
            $('.slide').hide();

            if(typeClicked == 'reg') {
                $('.by-reg').fadeIn('fast');
                reg.addClass('active');
            } else {
                $('.by-make').fadeIn('fast');
                make.addClass('active');
            }
        });
    });
</script>
@stop
