
<div class="container">
	<div class="row">
		<div class="col-sm-4 col-md-3 hidden-xs">

		    @include('partials.sidebar-faqs-menu')

			@include('partials.sidebar-search')

			@include('partials.sidebar-findlink-secondary')

		</div> <!-- .col-md-3 -->

		<div class="col-sm-8 col-md-9">

				<h2 class="border-bottom title-text">Recent Articles</h2>

				<div class="row blog-articles">
		    	@foreach($blog_posts as $blog)


					<div class="col-sm-4 col-md-4 blog-article">

						<article>

							@if(!is_null($blog->image))
								<img src="{{URL('imagecache', ['template' => 'small-thumbnail', 'filename' => $blog->image->filename])}}" alt="{!! $blog->title !!}" />
							@endif

							<a href="{!! route('blog_dynamic_route', $blog->url_slug) !!}" class="blog-title"><h3 class="blog-list-header">{!! $blog->title !!}</h3></a>

							<div class="blog-copy">
								{!! substr($blog->content, 0, 150) !!}...

							</div> <!-- .blog-copy -->
						</article>
					</div> <!-- .blog-article -->

				@endforeach

				</div> <!-- .row -->

				<h2 class="border-bottom title-text">Frequently asked questions</h2>

				@include('faqs.partials.list-faqs')

		</div> <!-- .col-md-9 -->

		<div class="col-sm-4 col-md-3 hidden-lg hidden-md hidden-sm">

		    @include('partials.sidebar-faqs-menu')

			@include('partials.sidebar-search')

			@include('partials.sidebar-findlink-secondary')

		</div> <!-- .col-md-3 -->
	</div> <!-- .row -->
</div> <!-- .container -->
