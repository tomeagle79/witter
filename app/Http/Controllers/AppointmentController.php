<?php

namespace App\Http\Controllers;

use App\Models\Api\Appointment;
use Illuminate\Http\Request;

class AppointmentController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_list_fitters($dates_route, Request $request)
    {
        $args = [
            'postcode' => $request->input('postcode'),
            'vtId' => $request->input('variantTowbarId'),
            'electric' => $request->input('electric_option'),
            'nAppointments' => 5,
            'requiresCoding' => $request->input('requires_coding') === 'true' ? 'Y' : 'N',
        ];

        $fitting_type = $request->input('fitting_type');

        if (!empty($fitting_type)) {
            if ($fitting_type == 'mobile') {
                $args['mobileOnly'] = 'y';
            } elseif ($fitting_type == 'workshop') {
                $args['dealerOnly'] = 'y';
            }
        }

        $fitters = Appointment::get_fitters_where($args);
        $fitters->partNo = $request->input('partNo');
        $fitters->variantTowbarId = $request->input('variantTowbarId');
        $fitters->vehicleDesc = $request->input('vehicleDesc');
        $fitters->electric = $request->input('electric_option');
        $fitters->fitterType = $request->input('fitterType');

        $dates_route = route($dates_route);

        if ($fitters->success == true) {
            $view = view('appointments.ajax_list_fitters', ['fitters' => $fitters, 'dates_route' => $dates_route])->render();
            $fitters->modal = $view;
        }

        return response()->json($fitters);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_list_dates($view_template, Request $request)
    {
        if ($request->input('variantTowbarId')) {
            $args = [
                'vtId' => $request->input('variantTowbarId'),
            ];
        }

        if (!empty($request->input('electric'))) {
            $args['electric'] = $request->input('electric');
        }

        // If the fitter is a mobile fitter add flag
        if (!empty($request->input('fitterType'))) {
            if ($request->input('fitterType') == 'mobile') {
                $args['isMobile'] = "Y";
            } else {
                $args['isMobile'] = "N";
            }
        }

        $appointments = Appointment::get_dates_where($request->input('partnerId'), $args);
        $appointments->address = $request->input('address');
        $appointments->partnerId = $request->input('partnerId');
        $appointments->partNo = $request->input('partNo');
        $appointments->variantTowbarId = $request->input('variantTowbarId');
        $appointments->vehicleDesc = $request->input('vehicleDesc');
        $appointments->electric = $request->input('electric');
        $appointments->fitterType = $request->input('fitterType');

        if ($appointments->success == true) {
            // JSON of all data in proper format
            $data = [];

            foreach ($appointments->appointments as $app) {

                $data[$app->appointmentDate][$app->appointmentTime] = [
                    'appointmentTime' => $app->appointmentTime,
                    'appointmentId' => $app->appointmentId,
                ];
                $times[$app->appointmentId] = $app->appointmentTime;
            }

            // We now take the existing appointment times and add the full list of hours for the working day.
            // We find the appointment id for the new time-slot by comparing the middle point of the nearest appointment
            // To rollback skip this foreach.
            foreach ($data as $date_key => $date) {

                // Building an array of time-slots through the day.
                $new_times = [];

                // Loop the appointments array
                foreach (config('settings.appointments_arr') as $time_slot) {

                    // Add the time to the new_times array
                    if (array_key_exists($time_slot, $date)) {

                        $new_times[$time_slot] = $date[$time_slot];
                    } else {
                        // Create a new time if does not exist.
                        $closest_id = null;
                        $closest = null;

                        $time_slot_numeric = (int) preg_replace('/[^0-9]/', '', $time_slot);

                        // Use the date keys as the value we check.
                        foreach ($date as $key => $value) {

                            // The existing timeslot we wish to compare plus 1.5 hour.
                            $key_numeric = (int) preg_replace('/[^0-9]/', '', $key) + 150;

                            // Compare if the key is closer to the time slot than the already set closest.
                            if ($closest === null || abs($time_slot_numeric - $closest) > abs($time_slot_numeric - $key_numeric)) {

                                $closest = $key_numeric;
                                $closest_id = $value['appointmentId'];
                            }
                        }

                        // Add the new time slot.
                        $new_times[$time_slot] = [
                            'appointmentTime' => $time_slot,
                            'appointmentId' => $closest_id,
                        ];
                    }
                }

                // Add the new times to the array date.
                $data[$date_key] = $new_times;
                ksort($data[$date_key]);
            }

            $appointments->data = $data;

            // JSON of all enabled dates
            $enabled_dates = [];
            foreach ($appointments->appointments as $app) {
                $enabled_dates[] = $app->appointmentDate;
            }
            $appointments->enabled_dates = $enabled_dates;

            if (!empty($view_template) && $view_template == 'cart') {

                $view = view('appointments.cart.ajax_list_dates', ['appointments' => $appointments])->render();
            } else {

                $view = view('appointments.ajax_list_dates', ['appointments' => $appointments])->render();
            }

            $appointments->modal = $view;
        }

        return response()->json($appointments);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_list_accessory_fitters($dates_route, Request $request)
    {
        $args = [
            'postcode' => $request->input('postcode'),
            'partNo' => $request->input('partNo'),
            'nAppointments' => 5,
        ];

        $fitting_type = $request->input('fitting_type');

        if (!empty($fitting_type)) {
            if ($fitting_type == 'mobile') {
                $args['mobileOnly'] = 'y';
            } elseif ($fitting_type == 'workshop') {
                $args['dealerOnly'] = 'y';
            }
        }

        $fitters = Appointment::get_fitters_where($args);
        $fitters->partNo = $request->input('partNo');
        $fitters->variantTowbarId = $request->input('variantTowbarId');
        $fitters->vehicleDesc = $request->input('vehicleDesc');
        $fitters->fitterType = $request->input('fitterType');

        $dates_route = route($dates_route);

        if ($fitters->success == true) {
            $view = view('appointments.ajax_list_accessory_fitters', ['fitters' => $fitters, 'dates_route' => $dates_route])->render();
            $fitters->modal = $view;
        }

        return response()->json($fitters);
    }

}
