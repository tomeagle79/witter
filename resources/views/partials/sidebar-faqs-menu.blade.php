
<h2 class="border-bottom title-text">Categories</h2>

<div class="categories-panel panel-group">
    <div class="panel">

		@foreach($help_categories as $help_category)
		    <div class="panel-heading">
		        <h4 class="panel-title">
		        	<a data-toggle="collapse" class="collapsed" aria-expanded="false" href="#category{!! $help_category->id !!}" >{!! $help_category->title !!}</a>
		        </h4>
		    </div>

		    <div id="category{!! $help_category->id !!}" class="panel-collapse collapse">
		    	<ul class="list-group">
			    	@foreach($help_category->help_articles as $article)

			    		<li class="list-group-item">
			    			<a href="{!! route('help_article_dynamic_route', $article->url_slug) !!}">{{ $article->title }}</a>
			    		</li>

			        @endforeach
		        </ul>
		    </div>
		@endforeach
	    <div class="panel-heading">
	        <h4 class="panel-title">
	        	<a data-toggle="collapse" aria-expanded="false" href="#category_faqs" >FAQs</a>
	        </h4>
	    </div>
	    <div id="category_faqs" class="panel-collapse ">
	    	<ul class="list-group">
		    	@foreach($categories as $category)
		    		<li class="list-group-item {{ empty($category['active'])? '' : 'active' }}">
		    			<a href="{!! route('faq_dynamic_route', $category['url_slug']) !!}">{{ $category['title'] }}</a>
		    		</li>
		        @endforeach
	        </ul>
	    </div>
	</div>
</div>