<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaypalPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paypal_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paypal_payment_id')->nullable();
            $table->string('intent')->nullable();
            $table->string('state')->nullable();
            $table->string('cart')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payer_status')->nullable();
            $table->string('payer_email')->nullable();
            $table->string('payer_first_name')->nullable();
            $table->string('payer_last_name')->nullable();
            $table->string('payer_id')->nullable();
            $table->string('payer_country_code')->nullable();
            $table->string('total')->nullable();
            $table->string('currency')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypal_payments');
    }
}
