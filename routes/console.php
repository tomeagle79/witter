<?php

use Illuminate\Foundation\Inspiring;

use App\Models\TwitterCache;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

// Refresh Twitter cache
Artisan::command('twitter:refresh', function() {
    
    $latest_cache = TwitterCache::orderBy('id', 'desc')->first();
    $latest_tweet = Twitter::getUserTimeline(['user_id' => '197080341', 'count' => 1, 'format' => 'object']);

    if(empty($latest_tweet)) {
        $this->comment('There was a problem getting the latest Tweet.');
    } else {
        if(empty($latest_cache) || ($latest_cache->twitter_id != $latest_tweet[0]->id) ) {
            $new_tweet = new TwitterCache;
            $new_tweet->twitter_created_at = $latest_tweet[0]->created_at;
            $new_tweet->twitter_id = $latest_tweet[0]->id;
            $new_tweet->tweet = $latest_tweet[0]->text;
            $new_tweet->save();

            $this->comment('New tweet retrieved (ID: ' . $latest_tweet[0]->id . ')');
        } else {
            $this->comment('Tweet ID ' . $latest_cache->twitter_id . ' is already the latest.');
        }
    }
    
})->describe('Update the Twitter cache');