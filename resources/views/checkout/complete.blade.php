@extends('layouts.frontend')

@section('title', 'Order successful')

@section('body-class', 'body_bg_checkout')

@section('heading')
	Checkout - Order successful
@stop

@section('meta_description')
	Checkout - Order successful
@stop

@section('content')

<div class="container checkout-process">
    <div class="row">
        <div class="col-xs-12">
            <h1>Order successful…</h1>
            <h3 class="tag-line">
                    @if(empty($checkout['section1']['first_name']))
                        Thanks for your order
                    @else
                        Thanks for your order, {{ $checkout['section1']['first_name'] }}
                    @endif
            </h3><!-- /.tag-line -->
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->

    <div class="row">

        <div class="col-xs-12 col-md-9">

            <div class="section">
                <h2 class="border_bottom">Order {{ '#' .$payment['order_id'] }} Summary</h2>

                <div class="delivery-notice">

                    @if(empty($fitting_type) || (!empty($fitting_type) && $fitting_type == 'self_fitted'))
                        <i class="icon-van"></i><span>The following item(s) will be delivered to your address</span>
                    @else
                        @if($fitting_type == 'mobile')
                            <i class="icon-mobile-fitter"></i>
                        @else
                            <i class="icon-garage-fitting"></i>
                        @endif
                        <span>The following item(s) will be delivered to your fitter:</span>
                    @endif
                </div><!-- /.delivery-notice -->

                <div class="mobile-box">
                    @if(!empty($cart['towbar']))
                        @include('cart.partials.cart_item', ['edit' => false, 'item' => $cart['towbar']])
                        @if(!empty($cart['contents']))
                            <hr>
                        @endif
                    @endif

                    @if(!empty($cart['contents']))
                        @foreach($cart['contents'] as $item )
                            @include('cart.partials.cart_item', ['edit' => false, 'item' => $item])
                            @if(!$loop->last)
                                <hr>
                            @endif
                        @endforeach
                    @endif
                </div><!-- /.mobile-box -->

                @include('checkout.partials.checkout_subtotal')

            </div> <!-- .section -->

            @if(empty($fitting_type) || (!empty($fitting_type) && $fitting_type == 'self_fitted'))
                <div class="section delivery-summary">
                    <h2 class="border_bottom">Delivery Summary</h2>
                    <div class="mobile-box">
                        <h3>{{ $checkout['section1']['title'] }} {{ $checkout['section1']['first_name'] }} {{ $checkout['section1']['surname'] }}</h3>

                        <p>{{  $checkout['section2']['delivery_address1'] }},
                            @if(!empty($checkout['section2']['delivery_address2'] )) {{ $checkout['section2']['delivery_address2'] }}, @endif
                            {{  $checkout['section2']['delivery_towncity'] }},
                            {{  $checkout['section2']['delivery_county'] }},
                            {{  $checkout['section2']['delivery_country'] }},
                            {{  strtoupper($checkout['section2']['delivery_postcode']) }}
                        </p>

                        <p>Telephone: {{  $checkout['section2']['delivery_telephone'] }}</p>
                    </div><!-- /.mobile-box -->
                </div> <!-- .section -->
            @endif


		</div> <!-- .col -->
        <div class="hidden-xs hidden-sm col-md-3">

            @include('checkout.partials.sidebar-checkout')

        </div> <!-- .col-md-3 -->
	</div>
</div>
@stop

@section('scripts')

    @if($run_tracking == 'true')
        @if(!empty($payment))
            <script type="text/javascript">
                /* <![CDATA[ */
                    // Check if analytics has already been fired
                    $.ajax({
                        url: '{{ route('checkout.has_analytics_fired', $payment["order_id"]) }}',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: "GET",
                    }).done(function(data) {
                        if(data.shouldFire == true) {
                            @include('partials.analytics.checkout-purchase', [
                                'cart' => $cart,
                                'payment' => $payment,
                                'revenue_amt' => $revenue_amt
                            ])
                        }
                    });

                    // Google Code for Witter eShop Conversion Page
                    var google_conversion_id = 942583751;
                    var google_conversion_language = "en";
                    var google_conversion_format = "3";
                    var google_conversion_color = "ffffff";
                    var google_conversion_label = "GzwOCKnJswUQx9-6wQM";
                    var google_conversion_value = {{ price($payment["amount"]) }};
                    var google_conversion_currency = "GBP";
                    var google_remarketing_only = false;

                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/942583751/?value={{ price($payment['amount']) }}&currency_code=GBP&label=GzwOCKnJswUQx9-6wQM&guid=ON&script=0"/>
                </div>
            </noscript>
        @endif
    @endif

@stop
