<?php

/**
 * Builds a cart variable from the session
 */
function build_cart()
{
    $cart = Session::get('cart', []);

    if (empty($cart)) {
        return $cart;
    }

    if (empty($cart['towbar']) && empty($cart['contents'])) {
        return $cart;
    }

    $total_price = 0;

    // Set the main regAndColourRequired flag as false
    $cart['regAndColourRequired'] = false;
    $i = 0;
    foreach ($cart['contents'] as $item) {
        // Accessory
        if ($item['type'] == 'accessory') {
            // Grab full item details
            $cart['contents'][$i]['details'] = \App\Models\Api\Accessory::get_single_where(['partNo' => $item['partNo']]);

            // Check if the accessory requires the car veg and colour.
            $cart['regAndColourRequired'] = ($cart['contents'][$i]['details']->regAndColourRequired == true) ? true : $cart['regAndColourRequired'];

            $total_price = $total_price + $cart['contents'][$i]['details']->price;
        }

        // Accessory fitted
        if ($item['type'] == 'accessory-fitted') {
            // Grab full item details
            $cart['contents'][$i]['details'] = \App\Models\Api\Accessory::get_single_where(['partNo' => $item['partNo']]);

            // Check if the accessory requires the car veg and colour.
            $cart['regAndColourRequired'] = ($cart['contents'][$i]['details']->regAndColourRequired == true) ? true : $cart['regAndColourRequired'];

            // Calcualte actual cost here so not in views.
            $cart['contents'][$i]['totalCost'] = $cart['contents'][$i]['details']->price;

            // Fitting price.
            $cart['contents'][$i]['totalCost'] += $cart['contents'][$i]['details']->fittingPrice;

            // Mobile fitting fee.
            if (!empty($item['fitterFee'])) {
                $cart['contents'][$i]['totalCost'] += $item['fitterFee'];
            }

            // Add the removal cost.
            if ($cart['contents'][$i]['removal_option'] === 'true') {
                $cart['contents'][$i]['totalCost'] += $cart['contents'][$i]['removal_cost'];
            }

            // Add our total cost to the total price.
            $total_price += $cart['contents'][$i]['totalCost'];
        }

        // Electrical kit
        if ($item['type'] == 'electrical-kit') {
            // Grab full item details
            $cart['contents'][$i]['details'] = \App\Models\Api\Electric::get_single_where(['partNo' => $item['partNo']]);

            $total_price = $total_price + $cart['contents'][$i]['details']->price;
        }

        $i++;
    }

    // Towbar
    if (!empty($cart['towbar'])) {
        // Grab full item details
        $cart['towbar']['details'] = \App\Models\Api\Towbar::get_single_where(['vehicleTowbarSlug' => $cart['towbar']['slug']]);

        // Check the fitter type. Self fitted towbars calculated differently
        if ($cart['towbar']['fitterType'] == \App\Models\Api\Towbar::FITTING_TYPE_SELF) {
            // Use the towbarOnlyPrice as a base to add to.
            $price = $cart['towbar']['details']->towbarOnlyPrice;

            // If the user has selected a different towball option use the priceAdjustment from the selected towball
            foreach ($cart['towbar']['details']->towballOptions as $to) {
                if ($to->partNo == $cart['towbar']['towball_option']) {
                    $price = $price + $to->towballOnlyPrice;
                    $cart['towbar']['towball_option_title'] = $to->title;
                    $cart['towbar']['towball_option_price'] = $to->towballOnlyPrice;
                }
            }

            // Get the kitOnlyPrice from any selected electrical kits
            foreach ($cart['towbar']['details']->electricOptions as $eo) {
                if ($eo->partNo == $cart['towbar']['electric_option']) {
                    // kitOnlyPrice when self fitted / purchased without webfit
                    $price = $price + $eo->kitOnlyPrice;
                    $cart['towbar']['electric_option_price'] = $eo->kitOnlyPrice;
                    $cart['towbar']['electric_option_title'] = $eo->title;
                    $cart['towbar']['electric_option_kit_identifier'] = $eo->kitIdentifier;
                }
            }
        } else {
            $price = $cart['towbar']['details']->price;

            foreach ($cart['towbar']['details']->electricOptions as $eo) {
                if ($eo->partNo == $cart['towbar']['electric_option']) {
                    $price = $price + $eo->priceAdjustment;
                    $cart['towbar']['electric_option_title'] = $eo->title;
                    $cart['towbar']['electric_option_price'] = $eo->priceAdjustment;
                    $cart['towbar']['electric_option_kit_identifier'] = $eo->kitIdentifier;
                }
            }

            foreach ($cart['towbar']['details']->towballOptions as $to) {
                if ($to->partNo == $cart['towbar']['towball_option']) {
                    $price = $price + $to->priceAdjustment;
                    $cart['towbar']['towball_option_title'] = $to->title;
                    $cart['towbar']['towball_option_price'] = $to->priceAdjustment;
                }
            }

            if (!empty($cart['towbar']['fitterFee'])) {
                $price = $price + $cart['towbar']['fitterFee'];
            }
        }

        // Add on coding fee.
        if ($cart['towbar']['softwareUpgrade'] === true) {
            $price += $cart['towbar']['softwareUpgradePrice'];
        }

        // Save our  price as final_price
        $cart['towbar']['final_price'] = $price;

        $total_price = $total_price + $cart['towbar']['final_price'];

        $i++;
    }

    $cart['sub_total'] = $total_price;

    // Recalculate grand total if we have a promo
    if (!empty($cart['promotion'])) {
        $cart['grand_total'] = calculate_promo($cart['sub_total'], $cart['promotion']['discountValue']);
    } else {
        $cart['grand_total'] = $cart['sub_total'];
    }

    return $cart;
}

/**
 * We should always have a cart variable, this creates it
 */
function create_cart()
{
    $cart = Session::get('cart', []);
    if (empty($cart)) {
        $cart = [
            'contents' => [],
            'towbar' => [],
            'sub_total' => 0.00,
            'grand_total' => 0.00,
        ];

        Session::put('cart', $cart);
    }
    return $cart;
}

/**
 * Calculate promo
 */
function calculate_promo($subtotal, $promo_amount)
{
    $grand_total = $subtotal - (float) $promo_amount; // promo_amount is a positive figure
    return $grand_total;
}

/**
 * Apply promo
 */
function apply_promo_to_cart($promo_code)
{
    $cart = Session::get('cart', []);

    if (empty($cart)) {
        return redirect('/cart');
    }

    $accessories = [];
    $towbar = [];
    $accessories_fitted = [];
    $mobileFitCost = 0;

    foreach ($cart['contents'] as $item) {
        // It's an accessory or electrical kit - Witter treat these the same
        if ($item['type'] == 'accessory' || $item['type'] == 'electrical-kit') {
            $accessories[] = [
                'PartNo' => $item['partNo'],
                'Quantity' => 1,
            ];
        }

        // Accessory fitted array
        if ($item['type'] == 'accessory-fitted') {
            // Build array for promo endpoint.
            $accessories_fitted[] = [
                'partNo' => $item['partNo'],
                'Quantity' => 1,
                'TimeslotId' => $item['appointmentId'],
                'AppointmentDate' => $item['appointmentDate'],
            ];

            $mobileFitCost = $item['fitterFee'];
        }
    }

    // Towbar/webfit
    if (!empty($cart['towbar'])) {
        // Check the fitter type. Self fitted towbars calculated differently
        if ($cart['towbar']['fitterType'] == \App\Models\Api\Towbar::FITTING_TYPE_SELF) {
            $accessories[] = [
                'AdditionalInfo' => $cart['towbar']['variantTowbarId'],
                'PartNo' => $cart['towbar']['partNo'],
                'Quantity' => 1,
            ];

            // If the towball_option cost money then add it to the array
            if (!empty($cart['towbar']['towball_option_price'])) {
                $accessories_arr[] = [
                    'partNo' => $cart['towbar']['towball_option'],
                    'quantity' => 1,
                ];
            }

            if (!empty($cart['towbar']['electric_option'])) {
                $accessories[] = [
                    'PartNo' => $cart['towbar']['electric_option'],
                    'Quantity' => 1,
                ];
            }
        } else {
            $towbar = [
                'VtId' => $cart['towbar']['variantTowbarId'],
                'TowballCode' => $cart['towbar']['towball_option'],
                'ElectricKitCode' => $cart['towbar']['electric_option'],
                'TimeslotId' => $cart['towbar']['appointmentId'],
                'AppointmentDate' => $cart['towbar']['appointmentDate'],
            ];

            $mobileFitCost = $cart['towbar']['fitterFee'];
        }
    }

    // Request promo
    $data = [
        'PromotionCode' => $promo_code,
        'OrderItems' => $accessories,
        'WebfitDetails' => $towbar,
        'AccessoryFittings' => $accessories_fitted,
        'MobileFitCost' => $mobileFitCost,
    ];

    $promotion = \App\Models\Api\Promotion::apply_promotion($data);

    if ($promotion->isValid == false) {
        if (!empty($cart['promotion'])) {
            unset($cart['promotion']);
        }

        $cart['grand_total'] = $cart['sub_total'];

        Session::put('cart', $cart);

        return ['error' => $promotion->notValidUserMessage];
    } else {
        // Rebuild the cart
        $cart = build_cart($cart);

        // We have a valid promo, add it to the cart.
        // Note that this will overwrite the existing promo if there is one, which is intended.
        $cart['promotion'] = (array) $promotion;

        $cart['grand_total'] = calculate_promo($cart['sub_total'], $cart['promotion']['discountValue']);

        Session::put('cart', $cart);

        return ['success' => 'A promotion has been added to your cart'];
    }
}
