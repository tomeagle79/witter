@extends('layouts.frontend')

@section('title', $manufacturer->manufacturerName .' Air Suspension Kit | '. $manufacturer->manufacturerName)

@section('heading')
Air Suspension Kits for {{ $manufacturer->manufacturerName }}
@stop

@section('meta_description')
	Buy an {{ $manufacturer->manufacturerName }} Air Suspension Kit direct from Witter Towbars.
@stop

@section('content')

<div class="container">
	<div class="row">

        <div class="col-xs-12 col-md-9 towbars-listing">

            <h2>{{ $manufacturer->manufacturerName }}</h2>

            @include('partials.search-dropdowns-suspensions', ['redirect_slug' => 'air-suspension-kits',
                'form_button_text' => 'Find your suspension kit',
                'display_models' => true,
                'display_manufacturers' => false,
                'vehicle_dropdown_url' => route('suspensions')])
            {{--
            @if(!empty($manufacturer->manufacturerDescription))
                <hr>

                <div class="description">
                    <p>{!! nl2br($manufacturer->manufacturerDescription) !!}</p>
                </div> <!-- .description -->
            @endif
            --}}
		</div> <!-- .col-md-6 -->

        <div class="hidden-xs hidden-sm col-md-3">

            @if(!empty($manufacturer->logoUrl))
                <div class="top-img-wrapper">
                    <img src="{{ $manufacturer->logoUrl }}" alt="{{ $manufacturer->manufacturerName }}" class="top-img">
                    <p class="text-center">Images are representative only</p>
                </div> <!-- .wrapper -->
            @endif

        </div><!-- /.col-md-3 -->
	</div>
    <div class="row">
        <div class="col-xs-12 towbars-listing">
            <hr >
            @if(!empty($models))

                <h2 class="title-margin-bottom">Find an Air Suspension Kit for {{ $manufacturer->manufacturerName }} models</h2>
                <div class="row item-list flex-items">
                    @foreach ($models as $key => $model)

                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                                <div class="item">
                                    <div class="item-body">
                                        <a href="{{ route('suspensions.models.vehicle_list', [$manufacturer->slug, $model->slug]) }}">
                                            <img src="{{ image_load($model->photoUrl) }}" title="Air Suspension Kit for {{ $model->modelName }}" alt="Air Suspension Kit for {{ $model->modelName }}">
                                            <h3>{{ $manufacturer->manufacturerName }} {{ $model->modelName }} Air Suspension Kit</h3>
                                        </a>
                                    </div><!-- /.item-body -->
                                </div> <!-- .model -->
                            </div>

                    @endforeach
                </div><!-- .row -->
            @endif
        </div><!-- /.col-md-6 -->
    </div><!-- /.row -->
</div>
@stop
