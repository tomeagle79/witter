/**
 * Toggle secondary menu on mobile
 */

$(document).ready(function () {

    console.log('toggle.js dom ready');

    $('.menu-icon').on('click', function () {
        console.log('menu clicked');
        $('body').toggleClass('nav-shown');
    });
});

// Function to clear the body past the header
var breakPoint = 992;
function clearHeader() {
    var headerHeight = $('header').innerHeight();
    var windowWidth = $(window).outerWidth();
    if (windowWidth < breakPoint) {
        $('body').css('padding-top', headerHeight);
    } else {
        $('body').css('padding-top', '0');

    }
}

$(window).on('load resize orientationchange', clearHeader)

// Toggle subnav menus
function toggleThis() {
    $(this).parents().children('.subnav').slideToggle();
    $(this).toggleClass('open');
}

$('.has-subnav span').on('click', toggleThis);