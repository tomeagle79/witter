@extends('layouts.frontend')

@section('title', $accessory->title)

@section('heading')
    {{ $accessory->title }}
@stop

@section('meta_description')
    {{ $accessory->title }}
@stop

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css">
@stop

@section('body-class', 'body_towbar_listing')

@section('content')

<div class="container towbars-product-view">

    <div class="row">

        <div class="col-md-9" itemscope itemtype="http://schema.org/Product">
            <div class="towbars-product-listing">

                <div class="row headlines-container">

                    <div class="col-md-4 hidden-xs hidden-sm">
                        @if(!empty($accessory->imageUrl))
                            <img src="{{ image_load($accessory->imageUrl) }}" alt="" itemprop="image">
                        @endif
                    </div> <!-- .col-md-4 -->

                    <div class="col-md-8">
                        <div class="product-headlines">
                            <h2 itemprop="name">{{ $accessory->title }}</h2>

                            <div class="hidden-lg hidden-md towbar-image">
                                @if(!empty($accessory->imageUrl))
                                    <img src="{{ image_load($accessory->imageUrl) }}" alt="" itemprop="image" class="hidden-lg hidden-md">
                                @endif
                            </div> <!-- .col-md-4 -->

                            <div class="description hidden-xs hidden-sm" itemprop="description">
                                <p>
                                    {!! $accessory->formatted_details !!}
                                </p>
                                <hr><br>

                                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#more-details" aria-expanded="false" aria-controls="more-details">
                                    Read More
                                </a>

                                <p class="collapse" id="more-details">
                                    <br>
                                    {!! $accessory->formatted_more_details !!}
                                </p>
                            </div> <!-- .description -->

                            <ul class="row towbar-includes">
                                @if(towbar_in_stock($accessory) && ($accessory->stockLevel > 5 || $accessory->stockLevel == 0))
                                    <li class="col-sm-6 col-xs-12 stock in-stock">
                                        <div class="include-item">In Stock</div>
                                    </li> <!-- col-sm-6 -->
                                @elseif(towbar_in_stock($accessory) && $accessory->stockLevel <= 5)
                                    <li class="col-sm-6 col-xs-12 stock low-stock">
                                        <div class="include-item">Hurry, only {{ $accessory->stockLevel }} left!</div>
                                    </li> <!-- col-sm-6 -->
                                @else
                                    <li class="col-sm-6 col-xs-12 stock out-of-stock">
                                        <div class="include-item">Out of Stock</div>
                                    </li> <!-- col-sm-6 -->
                                @endif

                                <li class="col-sm-6 col-xs-12 tick-li">
                                    <div class="include-item">Free UK Delivery</div>
                                </li> <!-- col-sm-6 -->
                                @if($accessory->isFittable)
                                    <li class="col-xs-12 tick-li">
                                        <div class="include-item">Workshop Fitting Available</div>
                                    </li> <!-- .col-md-4 col-sm-6 -->
                                @endif
                            </ul> <!-- .row -->
                            <div class="row mobile-pricing hidden-lg hidden-md hidden-sm">
                                <div class="col-xs-12">
                                    <p>Starting at <span>&pound;{{ price($accessory->price)}}</span></p>
                                    <a href="#order-form" class="btn btn-block btn-primary smoothscroll">Configure now</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <a href="#tech" class="view-features smoothscroll">View More Features</a>
                                </div>
                                <div class="col-md-6 col-sm-6 hidden-xs hidden-sm text-right">

                                    @if(strtolower($accessory->brandName) == 'witter')
                                        <img src="/assets/img/towbars/witter-logo.png" alt="{{ $accessory->brandName }}" width="70">
                                    @elseif(strtolower($accessory->brandName) == 'westfalia')
                                        <img src="/assets/img/towbars/westfalia-logo.png" alt="{{ $accessory->brandName }}" width="150">
                                    @endif

                                </div>
                            </div>
                        </div> <!-- .product-headlines -->
                    </div> <!-- .col-md-8 -->

                    <div class="col-xs-12 hidden-md hidden-lg">
                        <div class="product-headlines">
                            <div class="description" itemprop="description">
                                <p>
                                    {!! $accessory->formatted_details !!}
                                </p>
                            </div> <!-- .description -->
                        </div> <!-- .product-headlines -->
                    </div> <!-- .col-md-8 -->

                </div> <!-- .row -->

                @if(!towbar_in_stock($accessory) && !$accessory->isMto)

                    <div class="process">
                        <div class="bordered">
                            <h3>Out of Stock</h3>

                            <p>
                                Sorry, but this product is currently out of stock.
                            </p>
                            <p>
                                We can alert you when this product comes back in stock, just your
                                details below.
                            </p>
                            <p>
                                <strong>All fields are mandatory</strong>
                            </p>

                            <form action="{{ route('stock_notifications') }}" method="post" id="oos" onsubmit="
                                gtag('event', 'Out of stock', {
                                    event_category: 'Form Submission',
                                    event_label: '{{ $accessory->title }}'
                                });
                            ">
                                {{ csrf_field() }}

                                <input type="hidden" name="towbar" value="{{ $accessory->partNo }}">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" class="form-control" placeholder="First Name" required="required">
                                    </div> <!-- .col-sm-12 -->
                                </div> <!-- .row -->

                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" name="surname" id="surname" value="{{ old('surname') }}" class="form-control" placeholder="Surname" required="required">
                                    </div> <!-- .col-sm-12 -->
                                </div> <!-- .row -->

                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control" placeholder="Email" required="required">
                                    </div> <!-- .col-sm-12 -->
                                </div> <!-- .row -->

                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" name="mobile" id="mobile" value="{{ old('mobile') }}" class="form-control" placeholder="Mobile Number" required="required">
                                    </div> <!-- .col-sm-12 -->
                                </div> <!-- .row -->

                                <div class="row">
                                    <div class="col-lg-8 col-md-7 col-sm-6">
                                        <input type="text" name="postcode" id="postcode" value="{{ old('postcode') }}" class="form-control" placeholder="Postcode" required="required">
                                    </div> <!-- .col-md-9 -->
                                    <div class="col-lg-4 col-md-5 col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block" id="btn-oos">Receive Updates</button>
                                    </div> <!-- .col-md-3 -->
                                </div> <!-- .row -->
                            </form>
                        </div> <!-- .bordered -->
                    </div> <!-- .process -->

                @elseif($accessory->isFittable)
                {{-- Check if the caravan mover is fittable --}}
                    <form
                        action="{{ route('add_to_cart') }}"
                        method="post" id="order-form"
                        data-start-price="{{ price($accessory->displayPrice) }}"
                        data-price="{{ price($accessory->price) }}"
                        data-fit-only="{{ $accessory->fitOnly ? 'true' : 'false' }}"
                        data-removable="{{ $accessory->isRemovable ? 'true' : 'false' }}"
                    >

                        {{ csrf_field() }}

                        <div class="panel-group">
                            <div class="panel-towbar-options" id="removal-options">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#removal">1. <span>Existing Caravan Mover</span></a>
                                    </h4>

                                    <div class="panel-title-icons">
                                        <div class="unselected">
                                            <a data-toggle="collapse" href="#removal" class="unselected-plus"><i class="icon-plus-circle"></i></a>
                                        </div><!-- /.unselected -->

                                        <div class="selected">
                                            <a data-toggle="collapse" href="#removal" class="selected-pen"><i class="icon-pen"></i></a>
                                            <a data-toggle="collapse" href="#removal" class="selected-tick"><i class="icon-green-tick"></i></a>
                                        </div><!-- /.selected -->
                                    </div><!-- /.panel-title-icons -->
                                </div>
                                <div id="removal" class="panel-collapse {{ $accessory->isRemovable ? 'in' : 'collapse' }}">
                                    <div class="panel-body">

                                        <div class="row row-error no-removal-option" style="display: none;">
                                            <div class="col-xs-12">
                                                <div class="alert alert-danger">
                                                    <p>You must select a removal option before proceeding.</p>
                                                </div>
                                            </div>
                                        </div>

                                        @if($accessory->isRemovable)
                                        <p>Do you have a caravan mover you would like to replace? We can offer this removal service as part of the fitting for an additional fee of &pound;{{ $accessory->removalPrice }}.</p>

                                        <ul class="towbar-option-list">
                                            <li class="towbar-option">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h5>I need to remove my existing Caravan Mover</h5>
                                                    </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->

                                                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                                                        <input id="removal_option_true" type="radio" name="removal_option" value="true" data-name="Remove my current Caravan Mover" data-price="{{ $accessory->removalPrice }}">
                                                        <label for="removal_option_true"><span class="btn btn-grey"><span class="sr-only">Select</span></span></label>
                                                    </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->
                                                </div>
                                            </li>

                                            <li class="towbar-option">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <h5>I don’t need a removal service</h5>
                                                        </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->

                                                        <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                                                            <input id="removal_option_false" type="radio" name="removal_option" value="false" data-name="I don't need a removal service" data-price="0">
                                                            <label for="removal_option_false"><span class="btn btn-grey"><span class="sr-only">Select</span></span></label>
                                                        </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->
                                                    </div>
                                                </li>
                                        </ul><!-- /.towbar-option -->
                                        @else
                                        <p>Sorry, we do not currently offer a removal service with this caravan mover.</p>
                                        @endif

                                    </div><!-- /.panel-body -->
                                </div><!-- /.panel-collapse -->
                            </div><!-- /.panel-towbar-options -->

                            <div class="panel-towbar-options" id="fitting-options">
                                <div class="panel-heading">

                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#towbar-fitting">2. <span>Book Your Fitting</span></a>
                                        <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                        title="
                                            <h4>Book your fitting?</h4>
                                            <hr />
                                            <p>To book your caravan mover fitting, enter your postcode to find your nearest fitting centre or select a mobile fitter who can come to you. When you book your caravan mover fitting through our website, we will guarantee the labour if you have any problems after the caravan mover has been fitted. If you only purchase the caravan mover from us and take it to your own fitter, the labour will not be covered by the guarantee. To book a fitter, just select a date and time that is best for you and we will take care of the rest.</p>
                                        "></i>
                                    </h4>

                                    <div class="panel-title-icons">

                                        <div class="unselected">

                                            <a data-toggle="collapse" href="#towbar-fitting" class="unselected-plus"><i class="icon-plus-circle"></i></a>
                                        </div><!-- /.unselected -->

                                        <div class="selected">
                                            <a data-toggle="collapse" href="#towbar-fitting" class="selected-pen"><i class="icon-pen"></i></a>
                                            <a data-toggle="collapse" href="#towbar-fitting" class="selected-tick"><i class="icon-green-tick"></i></a>
                                        </div><!-- /.selected -->

                                    </div><!-- /.panel-title-icons -->
                                </div>

                                <div id="towbar-fitting" class="panel-collapse {{ $accessory->isRemovable ? 'collapse' : 'in' }}">

                                    <div class="panel-body">

                                        <div class="row row-error no-fitting-option" style="display: none;">
                                            <div class="col-xs-12">
                                                <div class="alert alert-danger">
                                                    <p>You must select a fitting option before proceeding.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="type_of_fitting">
                                            <div class="panel-heading-fitting panel-heading-fitting-garage" role="tab" id="workshop-fitting-tab">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="fitting-title fitting-title-garage">

                                                            <h4 class="panel-title">
                                                                I would like to take my vehicle to a workshop
                                                                <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                                                title="
                                                                    <h4>Fitting Centre</h4>
                                                                    <hr />
                                                                    <p>We have a network of approved fitters who can install your caravan mover, and if you book the fitting through our website then the labour will be covered as part of your guarantee. Just enter your postcode to find your nearest fitting centre. Book your caravan mover fitting for a date and time to suit you and arrive at the fitting centre in plenty of time for your scheduled appointment. Your caravan mover fitting will take around 3-4 hours.</p>
                                                                "></i>
                                                            </h4>

                                                        </div><!-- /.fitting-title-mobile -->
                                                    </div><!-- /.col-md-9 -->

                                                    <div class="col-md-3 text-right title-buttons">

                                                    <input id="fitting_option_workshop" type="radio" name="type" value="accessory-fitted" data-name="Workshop fitting" data-price="{{ $accessory->fittingPrice }}">
                                                        <label for="fitting_option_workshop"><span class="btn btn-grey select-fitter-option" data-type="workshop" data-for="fitting_option_workshop"><span class="sr-only">Select</span></span></label>
                                                    </div><!-- /.col-md-3 -->

                                                    <div class="col-xs-12">
                                                        <div class="selected-fitter" id="selected-fitter"></div>
                                                    </div><!-- /.col-xs-12 -->

                                                </div><!-- /.row -->
                                            </div>

                                            <div class="panel-heading-fitting panel-heading-fitting-mobile" role="tab" id="mobile-fitting-tab">

                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="fitting-title fitting-title-mobile">
                                                            <h4 class="panel-title">
                                                                I would like a mobile fitter to come to me
                                                                <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                                                title="
                                                                    <h4>Mobile Fitter</h4>
                                                                    <hr />
                                                                    <p>Choosing a mobile fitter couldn’t be simpler or more convenient. We have already sourced reliable fitters, so you don’t have to, plus the labour will be guaranteed when you book your towbar fitting through us. Fitters can come to your home or work address, so simply enter the postcode for where you would like the fitting to take place. Just make sure that your chosen location has safe and secure off road parking for your car and the fitter’s vehicle. The fitter will need full access to your car and installation takes around 3-4 hours.</p>
                                                                "></i>
                                                            </h4>
                                                        </div><!-- /.fitting-title-mobile -->
                                                    </div><!-- /.col-md-9 -->

                                                    <div class="col-md-3 text-right title-buttons">
                                                        <input id="fitting_option_mobile" type="radio" name="type" value="accessory-fitted" data-name="Mobile fitting" data-price="{{ $accessory->fittingPrice + 25 }}">
                                                        <label for="fitting_option_mobile"><span class="btn btn-grey select-fitter-option" data-type="mobile" data-for="fitting_option_mobile"><span class="sr-only">Select</span></span></label>
                                                    </div><!-- /.col-md-3 -->

                                                    <div class="col-xs-12">
                                                        <div class="selected-fitter" id="selected-fitter-mobile"></div>
                                                    </div><!-- /.col-xs-12 -->
                                                </div><!-- /.row -->

                                            </div>

                                            <div class="panel-heading-fitting" role="tab" id="self-fitting-tab">

                                                <div class="row row-error self-fitting-removal-error" style="display: none;">
                                                    <div class="col-xs-12">
                                                        <div class="alert alert-danger">
                                                            <p>You cannot use your own fitter if you'd like us to remove your current caravan mover.</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row row-error fit-only-error" style="display: none;">
                                                    <div class="col-xs-12">
                                                        <div class="alert alert-danger">
                                                            <p>This product is fitted only, you can only select workshop fitting.</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="fitting-title fitting-title-self">
                                                            <h4 class="panel-title">

                                                                I have my own fitter
                                                                <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                                                title="
                                                                    <h4>I have my own fitter</h4>
                                                                    <hr />
                                                                    <p>Finding your own fitter can be a risk. If a fitter has not been approved by us, and something goes wrong, our guarantee does not cover the labour. Our guarantee, however, will still cover the parts you bought from us.</p>
                                                                "></i>
                                                            </h4>
                                                        </div><!-- /.fitting-title-self -->

                                                    </div><!-- /.col-md-9 -->

                                                    <div class="col-md-3 text-right title-buttons">
                                                        <input id="fitting_option_self_fitted" type="radio" name="type" value="accessory" data-price="0" data-name="I have my own fitter">
                                                        <label for="fitting_option_self_fitted"><span class="btn btn-grey select-fitter-option"  data-type="self_fitted" data-for="fitting_option_self_fitted"><span class="sr-only">Select</span></span></label>
                                                    </div><!-- /.col-md-3 -->
                                                </div><!-- /.row -->
                                            </div>
                                        </div>
                                    </div><!-- /.panel-body -->
                                </div><!-- /.panel-collapse -->
                            </div><!-- /.panel-towbar-options -->

                            <div class="price-bar">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <p class="type">Your customised price:</p>
                                    </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->
                                    <div class="col-md-4 col-sm-4 col-xs-12 price-wrapper">
                                        &pound;<span class="final-price">0</span><span class="vat">Inc. VAT</span>
                                    </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="btn-table">
                                            <button type="submit" class="btn btn-primary btn-block order-btn submit-towbar-form">Add to Basket</button>
                                        </div> <!-- .btn-table -->
                                    </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->
                                </div> <!-- .row -->
                            </div> <!-- .price-bar -->
                        </div><!-- /.panel-group -->

                        <input type="hidden" name="price" value="{{ $accessory->price }}">
                        <input type="hidden" name="fittingPrice" value="{{ $accessory->fittingPrice }}">
                        <input type="hidden" name="removal_option_cost" value="{{ $accessory->removalPrice }}">
                        <input type="hidden" name="slug" id="fld_slug" value="{{ $accessory->partNo }}">
                        <input type="hidden" name="registrationDate" id="fld_registration_date" value="{{ $registration_date ?? '' }}">
                        <input type="hidden" name="fitterFee" id="fld_fitterFee" value="0">

                        @if(empty($appointment_exists))
                            <input type="hidden" name="partNo" id="fld_partNo" value="{{ $accessory->partNo }}">
                            <input type="hidden" name="appointmentDate" id="fld_appointmentDate" value="">
                            <input type="hidden" name="appointmentTime" id="fld_appointmentTime" value="">
                            <input type="hidden" name="appointmentId" id="fld_appointmentId" value="">
                            <input type="hidden" name="partnerId" id="fld_partnerId" value="">
                            <input type="hidden" name="address" id="fld_address" value="">
                            <input type="hidden" name="fitterType" id="fld_fitterType" value="">
                            <input type="hidden" name="postcode" id="fld_postcode" value="">
                        @else
                            <input type="hidden" name="partNo" id="fld_partNo" value="{{ $accessory->partNo }}">
                            <input type="hidden" name="appointmentDate" id="fld_appointmentDate" value="{{ $appointment_exists['appointmentDate'] }}">
                            <input type="hidden" name="appointmentTime" id="fld_appointmentTime" value="{{ $appointment_exists['appointmentTime'] }}">
                            <input type="hidden" name="appointmentId" id="fld_appointmentId" value="{{ $appointment_exists['appointmentId'] }}">
                            <input type="hidden" name="partnerId" id="fld_partnerId" value="{{ $appointment_exists['partnerId'] }}">
                            <input type="hidden" name="address" id="fld_address" value="{{ $appointment_exists['address'] }}">
                            <input type="hidden" name="fitterType" id="fld_fitterType" value="{{ $appointment_exists['fitterType'] }}">
                            <input type="hidden" name="postcode" id="fld_postcode" value="{{ $appointment_exists['customer_postcode'] }}">
                        @endif
                    </form>
                @else
                {{-- None fitted caravan movers. --}}
                <div class="panel-group">
                    <div class="price-bar">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <p class="type">Your customised price:</p>
                            </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->
                            <div class="col-md-4 col-sm-4 col-xs-12 price-wrapper">
                                &pound;<span class="final-price">{{ price($accessory->displayPrice) }}</span><span class="vat">Inc. VAT</span>
                            </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="btn-table">
                                    <a href="{{ route('add_to_cart', [
                                        'type' => 'accessory',
                                        'partNo' => $accessory->partNo
                                    ]) }}" class="btn btn-primary btn-block order-btn submit-towbar-form">Add to Cart</a>
                                </div> <!-- .btn-table -->
                            </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->
                        </div> <!-- .row -->
                    </div> <!-- .price-bar -->
                </div><!-- /.panel-group -->
                @endif
            </div><!-- /.towbars-product-listing -->

        </div> <!-- .col-md-6 -->

        <div class="col-md-3 hidden-xs hidden-sm">
            @if($accessory->isFittable)
                @include('partials.sidebar-webfit')
            @endif
            <div class="all-in-one">
                <div id="customised-section" style="display: none;">
                    <h4>You have currently customised your caravan mover as follows:</h4>

                    <ul class="all_in_one_items">
                    </ul>
                </div>

                <p class="price">&pound;<span class="final-price">{{ price($accessory->displayPrice) }}</span> <span class="inc">Inc. VAT</span></p>

                @if($accessory->isFittable)
                    <button type="submit" class="btn btn-primary btn-block order-btn submit-towbar-form">Add to Basket</button>
                @else
                    <a href="{{ route('add_to_cart', [
                        'type' => 'accessory',
                        'partNo' => $accessory->partNo
                    ]) }}" class="btn btn-primary btn-block order-btn submit-towbar-form">Add to Cart</a>
                @endif
            </div>

            @if(isset($accessory->videoId) && $accessory->videoId !== "")
            <div class="youtube-video">
                <h4>Watch the video</h4>
                <a data-fancybox target="_blank" href="https://www.youtube.com/embed/{{ $accessory->videoId }}" class="clearfix">
                    <div class="play-wrapper">
                        <img src="https://img.youtube.com/vi/{{ $accessory->videoId }}/mqdefault.jpg" alt="Watch the video">
                    </div>
                </a>
            </div>
            @endif
        </div> <!-- .col-md-3 -->
    </div> <!-- .row -->

    <div class="row details_tech" id="tech">
        @if(!empty($accessory->productDetails))
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="panel-group" id="techAccordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-product-info">
                        <div class="panel-heading active">
                            <a data-toggle="collapse" data-parent="#techAccordion" href="#techDetails"><h4 class="panel-title">Technical Details</h4></a>
                        </div><!-- /.panel-heading -->
                        <div id="techDetails" class="panel-collapse collapse in">
                            <div class="product-info-rows">
                                @foreach($accessory->productDetails as $attribute)
                                    <div class="row">
                                        <div class="col-xs-6">
                                            {{ $attribute->prefix }}
                                        </div><!-- /.col-xs-6 -->

                                        <div class="col-xs-6 text-right">
                                            {{ $attribute->text }}
                                        </div><!-- /.col-xs-6 -->
                                    </div><!-- /.row -->
                                @endforeach
                            </div><!-- /.info-list -->
                        </div><!-- /.panel-collapse -->
                    </div><!-- /.panel -->
                </div>
            </div>
        @endif
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="panel-group" id="infoAccordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-product-info">
                    <div class="panel-heading active">
                        <a data-toggle="collapse" data-parent="#infoAccordion" href="#delivery"><h4 class="panel-title">Delivery</h4></a>
                    </div><!-- /.panel-heading -->
                    <div id="delivery" class="panel-collapse collapse in">
                        <div class="product-info-rows">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>
                                        When you choose to get your caravan mover fitted at one of our approved fitting centres or with one of our mobile fitters, your caravan mover will be delivered to your fitter ready for installation.
                                    </p>
                                    <p>
                                        However, if you want to choose your own fitter, your caravan mover will be delivered to your chosen address on the next working day you place your order before 2:30pm.
                                    </p>
                                </div><!-- /.col-xs-6 -->
                            </div><!-- /.row -->
                        </div><!-- /.info-list -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->
                @if($accessory->isFittable)
                <div class="panel panel-product-info">
                    <div class="panel-heading active">
                        <a data-toggle="collapse" data-parent="#infoAccordion" href="#fitting"><h4 class="panel-title">Fitting</h4></a>
                    </div><!-- /.panel-heading -->
                    <div id="fitting" class="panel-collapse collapse">
                        <div class="product-info-rows">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>
                                        It is essential that you should get your caravan mover fitted by a professional fitter to protect your vehicle from unnecessary damage and to protect your guarantee. We have saved you the trouble of finding a fitter, so all you need to do is enter your postcode and we will find your nearest fitting centre or mobile fitter.
                                    </p>
                                </div><!-- /.col-xs-6 -->
                            </div><!-- /.row -->
                        </div><!-- /.info-list -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->
                @endif
            </div>
        </div>
    </div>

</div> <!-- .container -->

<div class="modal fade find-your-fitter" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="loading-modal-body"></div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
                <h4 class="modal-title">Select a fitting location</h4>
            </div> <!-- .modal-header -->
            <div class="modal-body">
                <div class="fitters">
                    <div class="form">
                        <label>Your Postcode</label>
                        <input type="text" name="postcode" id="postcode_update" value="" class="form-control">
                        <button type="button" id="update" class="btn btn-primary btn-primary-double-line">Find<br>Fitters</button>
                    </div> <!-- .form -->
                </div>
            </div> <!-- .modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js"></script>

    <script>
        @include('partials.analytics.product-view', [
            'product' => $accessory,
            'category' => 'Towbars'
        ])

        @if($accessory->isFittable)


        /**
         * calculatePrice
         * Depending on users selections update the price.
         */
        function calculatePrice() {
            var total = 0;

            if ($('.panel-towbar-options input[data-price]:checked').length > 1) {
                total += parseFloat($('#order-form').data('price'));
            } else {
                total += parseFloat($('#order-form').data('start-price'));
            }

            $('.panel-towbar-options input[data-price]:checked').each(function() {
                total += parseFloat($(this).data('price'));
            });

            // Display the total on the page.
            total = total.toFixed(2);

            // Re enter the comma to display to the user
            var formatted_total =  Number(total).toLocaleString('en', { minimumFractionDigits: 2});

            $('.final-price').html(formatted_total);
        }

        function loadStep(step) {
            $step = $(step);

            // Make sure the step exists.
            if($step.length < 1) {
                return false;
            }

            // Collapse existing open steps.
            $('.panel-towbar-options .panel-collapse').collapse('hide');
            $step.find('.panel-collapse').collapse('show');
        }

        function buildModalContent() {
            $('#modal .modal-body').html(
                '<div class="fitters">'+
                    '<div class="form">'+
                        '<label>Your Postcode</label>'+
                        '<input type="text" name="postcode" id="postcode_update" value="" class="form-control">'+
                        '<button type="button" id="update" class="btn btn-primary btn-primary-double-line">Find<br>Fitters</button>'+
                    '</div> <!-- .form -->'+
                '</div>');

            $('#modal .modal-title').html( 'Book your fitting' );

            return true;
        }

        $(document).ready(function() {

            /**
             * Initial vars.
             */
            var partNo = '{{ $accessory->partNo }}';

            // Catch enter press
            $('#postcode').on('keypress', function(e) {
                var code = e.keyCode || e.which;
                if(code == 13) { //Enter keycode
                    $('#update').click();
                    return false;
                }
            });

            $('body').on('click', '#update', function() {
                var postcode = $('#postcode_update').val();

                $('#no-postcode, #no-kit, #loading, #error').hide();

                $("#modal .loading-modal-body").show();

                if(!postcode.length) {
                    $('#no-postcode').fadeIn('slow');
                } else {
                    $('#loading').fadeIn('slow');

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('appointments.ajax_list_accessory_fitters')}}",
                        data: {
                            'postcode': postcode,
                            'partNo': partNo,
                            'fitting_type': $('input[name="type"]:checked').prop('id') == 'fitting_option_mobile' ? 'mobile' : 'workshop',
                            'caravanMover': true,
                        },
                        success: function(result){
                            $('#loading').fadeOut('slow');
                            $("#modal .loading-modal-body").fadeOut('slow');

                            if(result.success == false) {
                                $('#error p').html(result.errorMessage);
                                $('#error').fadeIn('slow');
                            } else {
                                // Fitting process has begun so readonly everything - client request
                                // $('input[type=radio]:not(:checked)').prop('disabled', true);
                                $('#reload').fadeIn('slow');

                                $('#fld_postcode').val(postcode);
                                $('#modal .modal-content .modal-body').html(result.modal);
                                $('#modal').modal('show');

                                // If the yes radio button has not been check then select it.
                                if(!$('#fitting_yes').is(':checked')) {
                                    $("#fitting_yes").prop("checked", true);
                                }

                                calculatePrice();
                            }
                        },
                        error: function(){
                            alert('Sorry, something went wrong');
                        }
                    });
                }
            });

            /**
             * Functions that need running when a towbar-option is changed.
             */
            $('.panel-towbar-options input').on('change', function(e) {
                var $input = $(this);
                var name = $input.attr('name');

                // Hide present errors.
                $('.row-error').hide();

                if (name === 'removal_option') {
                    if ($input.val() === 'true') {
                        // Make sure self fitting isn't already checked.
                        if ($('#fitting_option_self_fitted').is(':checked')) {
                            $('#fitting_option_self_fitted').prop('checked', false);
                        }
                    }

                    loadStep('#fitting-options');
                } else if (name === 'type') {
                    // Workshop.
                    if ($input.val() === 'accessory-fitted') {
                        buildModalContent();
                        $('#modal').modal('show');
                    }

                    // Self fitting.
                    if ($input.val() === 'accessory') {
                        // Throw error if removal option is checked.
                        if($('input[name="removal_option"]:checked').val() === 'true') {
                            $('.self-fitting-removal-error').show();

                            $input.prop('checked', false);
                        }

                        // Throw error if product is self fit only.
                        if ($('#order-form').data('fit-only') === 'true') {
                            $('.fit-only-error').show();

                            $input.prop('checked', false);
                        }
                    }
                }

                updatePanelsByRadioName($input.attr('name'));
                calculatePrice();
            });

            /**
             * Functions to run on DOM ready.
             */
            calculatePrice();

            /**
             * Check all is good before submitting the order form.
             */
            $('.order-btn').on('click', function(e) {
                e.preventDefault();

                // Hide present errors.
                $('.row-error').hide();

                // Make sure step 1 is complete.
                if ($('#order-form').data('removable') === true && $('input[name="removal_option"]:checked').length < 1) {
                    $('.no-removal-option').show();

                    $([document.documentElement, document.body]).animate({
                        scrollTop: $('.no-removal-option').offset().top
                    }, 500);

                    return false;
                }

                // Make sure step 2 is complete.
                if ($('input[name="type"]:checked').length < 1) {
                    $('.no-fitting-option').show();

                    $([document.documentElement, document.body]).animate({
                        scrollTop: $('.no-fitting-option').offset().top
                    }, 500);

                    return false;
                }

                // If we've got this far we should be fine to submit the form.
                $('.order-btn').unbind('click');
                $('.order-btn').first().trigger('click');
            });
        }); // Document ready.


        @endif
    </script>
@stop
