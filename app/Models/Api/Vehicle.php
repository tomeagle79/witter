<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class Vehicle extends ApiModel
{
	const URI = "vehicles";

	/**
	 * Get a vehicle list
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'?' . http_build_query($args);
		$obj = self::get_request($url);
		
		return $obj;
	}

	/**
	 * Get a single manufacturer
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_single_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'/slug?' . http_build_query($args);
		$obj = self::get_request($url);
		
		return $obj;
	}
}
