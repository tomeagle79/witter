<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpCategory extends Model
{
    use SoftDeletes;

    /**
     * Relationship to help_articles
     */
    public function help_articles()
    {
        return $this->belongsToMany('App\Models\HelpArticle', 'help_category_help_articles')
            ->where('help_articles.published', '=', 1);
    }
}
