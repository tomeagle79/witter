<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="@yield('meta_description', '')">

        @if(!empty($noindex_nofollow_page))

            <meta name="robots" content="noindex, nofollow">
        @elseif(!empty($noindex_follow_page))

            <meta name="robots" content="noindex,follow" />
        @elseif(!empty($noindex_page))

            <meta name="robots" content="noindex" />
        @endif

        @if(!empty($canonical))
            <link rel="canonical" href="{{ $canonical }}" />
        @else
            <link rel="canonical" href="{{ url()->current() }}" />
        @endif

        <title>@yield('title') | Witter Towbars</title>

        <link href="{{ empty($assets['assets/css/app.css']) ? URL::asset('assets/css/app.css') : URL::asset($assets['assets/css/app.css']) }}" rel="stylesheet" type="text/css" >
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600" rel="stylesheet">

        @yield('styles')

        <!--[if lte IE 9]>
            <link href="{{ empty($assets['assets/css/ie/app_1.css']) ? URL::asset('assets/css/ie/app_1.css') : URL::asset($assets['assets/css/ie/app_1.css']) }}" rel="stylesheet" type="text/css">
            <link href="{{ empty($assets['assets/css/ie/app_2.css']) ? URL::asset('assets/css/ie/app_2.css') : URL::asset($assets['assets/css/ie/app_2.css']) }}" rel="stylesheet" type="text/css">
            <link href="{{ empty($assets['assets/css/ie/app_3.css']) ? URL::asset('assets/css/ie/app_3.css') : URL::asset($assets['assets/css/ie/app_3.css']) }}" rel="stylesheet" type="text/css">
        <![endif]-->

        <script type="text/javascript">
            (function(a,e,c,f,g,h,b,d){var k=

            {ak:"942583751",cl:"BoyhCKuFwYMBEMffusED",autoreplace:"01244 284 555"}
            ;a[c]=a[c]||function()

            {(a[c].q=a[c].q||[]).push(arguments)}
            ;a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e)

            {a[c](2,b,k,d,null,new Date,e)}
            ;a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
        </script>

        @yield('scripts-head')

        @yield('JSON-LD')

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('settings.google_analytics_code') }}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '{{ config('settings.google_analytics_code') }}');
        </script>
        <script>
        </script>


        <script src='//cdn.zarget.com/177021/462309.js'></script>

        @if(config('app.url') !== 'http://witter-staging.rnmtest.co.uk')
        <!-- PayPal BEGIN -->
        <script>
            ;(function(a,t,o,m,s){a[m]=a[m]||[];a[m].push({t:new Date().getTime(),event:'snippetRun'});var f=t.getElementsByTagName(o)[0],e=t.createElement(o),d=m!=='paypalDDL'?'&m='+m:'';e.async=!0;e.src='https://www.paypal.com/tagmanager/pptm.js?id='+s+d;f.parentNode.insertBefore(e,f);})(window,document,'script','paypalDDL','8e9847a1-6e51-4e24-9e80-dd436d03a495');
        </script>
        <!-- PayPal END -->
        @endif

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '846331282381098');
        fbq('track', 'PageView');
        </script>
        <noscript>
        <img height="1" width="1"
        src="https://www.facebook.com/tr?id=846331282381098&ev=PageView
        &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

    </head>
    <body class="@yield('body-class')">
        @includeIf('theme::partials.consumer-banner')

        @if(empty($checkout_layout))
            @include('includes.header')
        @else
            @include('includes.checkout_header')
        @endif

        <main>
            @if(isset($breadcrumbs) || isset($include_main_banner))

                <div class="container-fluid points">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                                <p><i class="icon-white-block"></i> Free UK Delivery</p>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                                <p><i class="icon-mobile-fitter"></i> Nationwide Mobile Fitting</p>
                            </div>
                            <div class="col-lg-3 col-md-4 hidden-xs hidden-sm">
                                <p><i class="icon-trusted"></i> Trusted Since 1950</p>
                            </div>
                            <div class="col-lg-3 hidden-xs hidden-sm hidden-md">
                                <a href="{!! route('blog_dynamic_route', 'what-is-webfit') !!}" title="WEBFIT IS HERE">
                                    <p><i class="icon-webfit"></i> <span>WEBFIT IS HERE, CLICK <br>TO FIND OUT MORE</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                @if(isset($breadcrumbs))
                    <div class="container-fluid page-title">
                        <div class="container">
                            <div class="row">
                                @if(isset($reg_search))
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                @else
                                    <div class="col-xs-12">
                                @endif
                                    @if(!empty($heading_h2))
                                        <h2>@yield('heading')</h2>
                                        {!! $breadcrumbs !!}
                                    @else
                                        <h1>@yield('heading')</h1>
                                        {!! $breadcrumbs !!}
                                    @endif
                                </div>
                                @if(isset($reg_search))
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        @include('partials.numberplate-search')
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @else
                    {{-- When no breadcrumb just display the page title --}}
                    <div class="container-fluid page-title">
                        <div class="container">
                                <div class="row">
                                    <div class="col-xs-12">
                                        @if(!empty($heading_h2))
                                            <h2>@yield('heading')</h2>
                                        @else
                                            <h1>@yield('heading')</h1>
                                        @endif
                                    </div><!-- /.col-xs-12 -->
                                </div><!-- /.row -->
                        </div><!-- /.container -->
                    </div><!-- /.container-fluid -->
                @endif
            @endif

            @if(session('message'))
                <div class="container">
                    <div class="alert alert-info fadein" role="alert">
                        <p>{{ session('message') }}</p>
                    </div>
                </div>
            @endif
            @if(session('error'))
                <div class="container">
                    <div class="alert alert-danger fadein" role="alert">
                        <p>{{ session('error') }}</p>
                    </div>
                </div>
            @endif
            @if(session('success'))
                <div class="container">
                    <div class="alert alert-success fadein" role="alert">
                        <p>{!! session('success') !!}</p>
                    </div>
                </div>
            @endif

            @if(isset($errors))
                @if (count($errors) > 0)
                    <div class="container">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            @endif

            @yield('content')
        </main>

        @include('includes.footer')

        <script type="text/javascript" src="{{ empty($assets['assets/js/app.js']) ? URL::asset('assets/css/app.js') : URL::asset($assets['assets/js/app.js']) }}"></script>
        <script>
            $(document).ready(function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('[data-toggle="tooltip"]').tooltip();

                $('.cookie-message button').on('click', function() {
                    $(this).parents('.cookie-message').fadeOut('slow');

                    if (($(window).width() > 768) && ($(window).height() > 740)) {
                        // Move the home page main banner down again.
                        $(".blue-transparent-banner").animate({'padding-bottom' : '15px'}, "slow");
                        $(".carousel-controllers").animate({'margin-bottom' : 0}, "slow");
                    }

                    Cookies.set('cookie-message', 'dismissed');
                });


                $(document).on('click', '.product-link', function(e) {
                    var prodid = $(this).attr('data-product-id');

                    gtag('event', 'select_content', {
                        "content_type": "product",
                        "items": [
                            {
                                "id": window.product_json[prodid].id,
                                "name": window.product_json[prodid].name,
                                "brand": (window.product_json[prodid].brand.length ? window.product_json[prodid].brand : ''),
                                "category": (window.product_json[prodid].category.length ? window.product_json[prodid].category : ''),
                                "price": window.product_json[prodid].price,
                                "list_name": window.product_json[prodid].list_name,
                                "list_position": window.product_json[prodid].list_position
                            }
                        ]
                    });
                });
            });

        </script>

        @if(!empty($koop_script))
            <!-- Koop script -->
            <script src="{{ $koop_script }}" async defer></script>
        @endif

        @if(session('ga_event'))
            <script type="text/javascript">
                gtag('event', '{{ session('ga_event')['event_action'] ?? '' }}', {
                    event_category: '{{ session('ga_event')['event_category'] ?? '' }}',
                    event_label: '{{ session('ga_event')['event_label'] ?? '' }}',
                    value: {{ session('ga_event')['value'] ?? '' }}
                });
            </script>
        @endif
        @yield('scripts')
        <script>
          window.intercomSettings = {
            app_id: "hhtyo763"
          };
        </script>
        <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/hhtyo763';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
    </body>
</html>
