<?php

namespace App\Traits;

trait Published
{
    /**
     * Scope for published object
     *
     * @return $query
     */
    public function scopePublished($query)
    {
        return $query->where('published', '=', 1);
    }
}
