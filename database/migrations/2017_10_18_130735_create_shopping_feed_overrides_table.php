<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingFeedOverridesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_feed_overrides', function (Blueprint $table) {
            $table->increments('id');
            $table->string('part_no')->comment('Product to override');
            $table->string('product_title')->nullable();
            $table->text('product_description')->nullable();
            $table->string('product_image')->nullable();
            $table->text('google_category')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_feed_overrides');
    }
}
