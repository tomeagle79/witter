<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCategoryBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_category_blog_posts', function (Blueprint $table) {
            $table->integer('blog_post_id')->unsigned();
            $table->integer('blog_category_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('blog_category_blog_posts', function($table) {
            $table->foreign('blog_post_id')->references('id')->on('blog_posts')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('blog_category_id')->references('id')->on('blog_categories')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_category_blog_posts', function (Blueprint $table) {
            Schema::dropIfExists('blog_category_blog_posts');
        });
    }
}
