@extends('layouts.backend')

@section('title')
	Edit FAQ Category
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/faq_categories/partials/_form', ['submit_text' => 'Edit FAQ Category', 'type' => 'edit'])
	</form>

@stop
