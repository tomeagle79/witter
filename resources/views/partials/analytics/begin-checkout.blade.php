
gtag('event', 'begin_checkout', {
    "items": [
        @if(!empty($cart['towbar']))
            {
                'name': '{{ str_replace("'", '', $cart['towbar']['details']->title) }}',
                'id': '{{ $cart['towbar']['partNo'] }}',
                'price': '{{ price($cart['towbar']['final_price']) }}',
                'quantity': '1',
                'category': 'Towbars',
                'variant': '{{ ($cart['towbar']['fitterType'] ? $cart['towbar']['fitterType'] : 'garage_fitted') }}',
            }@if(!empty($cart['contents'])), @endif
        @endif
        
        @if(!empty($cart['contents']))
            @foreach($cart['contents'] as $item)
                {
                    'name': '{{ str_replace("'", '', $item['details']->title) }}',
                    'id': '{{ $item['details']->partNo }}',
                    'price': '{{ $item['details']->price }}',
                    'quantity': '1',
                    'brand': '{{ $item['details']->brandName ?? '' }}',
                    @if(!empty($item['details']->partClassId) && $item['details']->partClassId == 3)
                        'category': 'Bike Racks'
                    @else
                        @if($item['type'] == 'accessory')
                            'category': 'Accessories'
                        @elseif($item['type'] == 'electrical-kit')
                            'category': 'Electrical Kits'
                        @else
                            'category': 'Accessories'
                        @endif
                    @endif
                }@if(!$loop->last), @endif
            @endforeach
        @endif
    ],
    "coupon": "{{ $cart['promotion']['promotionCode'] ?? '' }}"
});
