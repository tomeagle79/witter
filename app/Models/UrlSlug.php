<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UrlSlug extends Model
{
	use SoftDeletes;

	/*
	* Get the url slug page
	*/
	public function page()
	{
	    return $this->hasOne('App\Page');
	}
}

