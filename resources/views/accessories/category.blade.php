@extends('layouts.frontend')

@section('title', $category->title)

@section('heading')
	{{ $category->title }}
@stop

@section('meta_description')
	{{ $category->metaDescription }}
@stop

@section('content')
	<div class="container witter-listing">
		<div class="row">

			<div class="col-sm-8 col-md-9 col-md-push-3 accessory-listing">

				<h2 class="border-bottom no-top-margin">{{ $category->title }}</h2>

				<div class="description">
					<p>{{ nl2br($category->description) }}</p>
				</div>

				<div class="row item-list flex-items">

                    @foreach($subcategories as $sub_category)

                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="item text-center">
                                <div class="item-body">

                                    @if($sub_category->categoryId == 68)
                                        <a href="{{ route('trackers.index') }}"><img src="{{ image_load($sub_category->imageUrl) }}" alt="{{ $sub_category->categoryName }}" /></a>

                                        <a href="{{ route('trackers.index') }}"><h3 class="list-title">{!! $sub_category->categoryName !!}</h3></a>
                                    @else
                                        <a href="{!! route('accessories.subcategory', [$category->slug, $sub_category->slug]) !!}" ><img src="{{ image_load($sub_category->imageUrl) }}" alt="{{ $sub_category->categoryName }}" /></a>

                                        <a href="{!! route('accessories.subcategory', [$category->slug, $sub_category->slug]) !!}" ><h3 class="list-title">{!! $sub_category->categoryName !!}</h3></a>
                                    @endif


                                </div><!-- /.item-body -->
                            </div>
                        </div>

                    @endforeach

				</div> <!-- .row -->

			</div> <!-- .col-md-9 -->
			<div class="col-sm-4 col-md-3 col-md-pull-9">

				@include('partials.sidebar-partclass-menu')

				@include('partials.sidebar-search')

				@include('partials.sidebar-findlink-secondary')

			</div> <!-- .col-md-3 -->
		</div> <!-- .row -->
	</div> <!-- .container -->
@stop

