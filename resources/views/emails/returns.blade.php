@extends('layouts.email')

@section('content')

    You received a message from Witter:
    <p>
        Order number: {{ $order_number }}
    </p>
    <p>
        First Name: {{ $first_name }}
    </p>
    <p>
        Surname: {{ $surname }}
    </p>
    <p>
        Company Name: {{ $company_name }}
    </p>
    <p>
        Postcode: {{ $postcode }}
    </p>
    <p>
        Address 1: {{ $address1 }}
    </p>
    <p>
        Address 2: {{ $address2 }}
    </p>
    <p>
        Address 3: {{ $address3 }}
    </p>
    <p>
        Town/City: {{ $towncity }}
    </p>
    <p>
        Telephone: {{ $telephone }}
    </p>
    <p>
        Fax: {{ $fax }}
    </p>
    <p>
        Email: {{ $email }}
    </p>

    <hr>

    <p>
        Date Purchased: {{ $date_purchased_dd }}/{{ $date_purchased_mm }}/{{ $date_purchased_yyyy }}
    </p>

    <hr>

    <p>
        Product Serial Number: {{ $product_serial_no }}
    </p>
    <p>
        Product Part No: {{ $product_part_no }}
    </p>
    <p>
        Product Name: {{ $product_name }}
    </p>
    <p>
        Return Reason: {{ $return_reason }}
    </p>

@stop
