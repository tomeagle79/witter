<?php

namespace App\Http\Controllers;

use App\Models\Api\Accessory;
use App\Models\Api\Suspension\SuspensionAccessory;
use App\Models\Api\Suspension\SuspensionVehicle;
use App\Models\Api\Suspension\SuspensionVehicleManufacturer;
use App\Models\Api\Suspension\SuspensionVehicleModel;
use App\Models\Api\VehicleManufacturer;
use App\Models\Api\VehicleModel;
use Session;

class SuspensionController extends FrontendController
{
    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Air Suspension', route('suspensions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['vehicle_dropdown'] = $vehicle_dropdown = Session::get('suspension_vehicle_dropdown', []);

        // Get all the manufacturers
        $this->data['manufacturers'] = $this->data['manufacturers_dropdown'] = $data = SuspensionVehicleManufacturer::get_all();

        // Get all the manufacturer's models if the manufacturer_id is set
        if (isset($vehicle_dropdown['manufacturer_id'])) {
            $this->data['models_dropdown'] = SuspensionVehicleModel::get_all_from_manufacturer_id($vehicle_dropdown['manufacturer_id']);
        }

        // Get all the model's bodies if the model_id is set
        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['vehicles_dropdown'] = SuspensionVehicle::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        return view('suspensions.index', $this->data);
    }

    /**
     * Display a listing models.
     *
     * @return \Illuminate\Http\Response
     */
    public function model_list($manufacturer_slug)
    {
        $this->data['manufacturer_slug'] = $manufacturer_slug;

        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        if (empty($manufacturer)) {
            return redirect()->route('suspensions')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['models'] = $models = SuspensionVehicleModel::get_all_from_manufacturer_id($manufacturer->manufacturerId);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('suspensions.model_list', $manufacturer_slug));

        // Build the vehicle_dropdown session array
        $this->data['vehicle_dropdown'] = $vehicle_dropdown = build_suspension_dropdown($manufacturer->manufacturerId);

        // If the manufacturer_id set then get the models for the dropdown
        if (isset($vehicle_dropdown['manufacturer_id'])) {
            $this->data['models_dropdown'] = SuspensionVehicleModel::get_all_from_manufacturer_id($vehicle_dropdown['manufacturer_id']);
        }

        // If the model_id set then get the bodies for the body dropdown
        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['vehicles_dropdown'] = SuspensionVehicle::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        return view('suspensions.models', $this->data);
    }

    /**
     * Display a list of vehicles
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicle_list($manufacturer_slug, $model_slug)
    {
        $this->data['manufacturer_slug'] = $manufacturer_slug;
        $this->data['model_slug'] = $model_slug;

        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);
        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('suspensions.model_list', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('suspensions.models.vehicle_list', [$manufacturer_slug, $model_slug]));

        $this->data['vehicles_dropdown'] = $this->data['vehicles'] = SuspensionVehicle::get_all_from_model_id($model->modelId);

        // Build the vehicle_dropdown session array
        $vehicle_dropdown = build_suspension_dropdown($manufacturer->manufacturerId, $model->modelId);

        // If the manufacturer_id set then get the models for the dropdown
        if (isset($vehicle_dropdown['manufacturer_id'])) {
            $this->data['models_dropdown'] = SuspensionVehicleModel::get_all_from_manufacturer_id($vehicle_dropdown['manufacturer_id']);
        }

        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['vehicles_dropdown'] = SuspensionVehicle::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        return view('suspensions.vehicles', $this->data);
    }

    /**
     * Display a list of product
     *
     * @return \Illuminate\Http\Response
     */
    public function product_list($manufacturer_slug, $model_slug, $vehicle_slug)
    {

        $this->data['manufacturer'] = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);
        $this->data['model'] = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        $vehicle_dropdown = build_suspension_dropdown($this->data['manufacturer']->manufacturerId, $this->data['model']->modelId, $vehicle_slug);

        $this->data['products'] = collect(SuspensionAccessory::get_by_slug_where(['vehicleSlug' => $vehicle_slug]));

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('suspensions', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('suspensions.models.vehicle_list', [$manufacturer_slug, $model_slug]));
        $this->data['breadcrumbs']->addCrumb('Products', route('suspensions.models.vehicle_list', [$manufacturer_slug, $model_slug, $vehicle_slug]));

        // Set the breadcrumb in a flash session so can be used on the Towbar purchase page
        request()->session()->flash('breadcrumbs', $this->data['breadcrumbs']);

        return view('suspensions.products', $this->data);
    }

    /**
     * Display a product
     *
     * @return \Illuminate\Http\Response
     */
    public function view($accessory_slug)
    {
        // Get accessory details
        $this->data['accessory'] = Accessory::get_single_by_slug($accessory_slug);

        // If coming from the vehicles pages use the breadcrumb in the session
        $breadcrumbs = request()->session()->get('breadcrumbs');

        if (!empty($breadcrumbs)) {
            $this->data['breadcrumbs'] = $breadcrumbs;
        }

        $this->data['breadcrumbs']->addCrumb($this->data['accessory']->title);

        return view('suspensions.view', $this->data);
    }
}
