<?php

namespace App\Models\Api;

use App\Models\Api\ApiModel;

class PartClass extends ApiModel
{
    const URI = "partclasses";

    /**
     * Get all part classes
     *
     * @return Object
     */
    public static function get_all()
    {
        $url = config('witter.api_base') . self::URI;
        $obj = self::get_request($url);

        return $obj;
    }

    /**
     * Get subcategories based off partclass slug
     */
    public static function get_subcategories_by_slug($category_slug)
    {
        $url = config('witter.api_base') . 'partclasses/slug/partcategories?classSlug=' . urlencode($category_slug);
        $obj = self::get_request($url);

        return $obj;
    }

    /**
     * Get category based off partclass slug
     */
    public static function get_category_by_slug($category_slug)
    {
        $url = config('witter.api_base') . 'partclasses/slug?classSlug=' . urlencode($category_slug);
        $obj = self::get_request($url);

        if (is_null($obj)) {
            return abort(404);
        }

        return $obj;
    }

    /**
     * Get sub-category based off category slug
     */
    public static function get_subcategory_by_slug($category_slug, $subcategory_slug)
    {
        $url = config('witter.api_base') . 'partcategories/slug?classSlug=' . urlencode($category_slug) . '&categorySlug=' . urlencode($subcategory_slug);
        $obj = self::get_request($url);

        return $obj;
    }

    /**
     * Get details of apartclass based off ID
     */
    public static function get_partclass_by_id($partClassId)
    {
        $url = config('witter.api_base') . self::URI . '/' . $partClassId;
        $obj = self::get_request($url);

        return $obj;
    }

    /**
     * Get partclass CATEGORIES based off ID
     */
    public static function get_category_by_category_id($category_id)
    {
        $url = config('witter.api_base') . 'partcategories/' . $category_id;
        $obj = self::get_request($url);

        return $obj;
    }

    /**
     * Get partclass CATEGORIES based off ID
     */
    public static function get_categories_by_partclass_id($partClassId)
    {
        $url = config('witter.api_base') . 'partclasses/' . $partClassId . '/partcategories';
        $obj = self::get_request($url);

        return $obj;
    }

}