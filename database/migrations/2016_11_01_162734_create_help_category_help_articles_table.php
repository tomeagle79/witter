<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpCategoryHelpArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_category_help_articles', function (Blueprint $table) {
            $table->integer('help_article_id')->unsigned();
            $table->integer('help_category_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('help_category_help_articles', function($table) {
            $table->foreign('help_article_id')->references('id')->on('help_articles')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('help_category_id')->references('id')->on('help_categories')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_category_help_articles');
    }
}
