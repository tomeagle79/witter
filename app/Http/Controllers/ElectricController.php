<?php

namespace App\Http\Controllers;

use App\Models\Api\Electric;
use App\Models\Api\Vehicle;
use App\Models\Api\VehicleBody;
use App\Models\Api\VehicleManufacturer;
use App\Models\Api\VehicleModel;
use App\Models\Api\VehicleRegistration;
use Session;

class ElectricController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Witter Electrical Kits', route('electrics'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['vehicle_dropdown'] = $vehicle_dropdown = Session::get('vehicle_dropdown', []);

        // Get all the manufacturers
        $this->data['manufacturers'] = $this->data['manufacturers_dropdown'] = $data = VehicleManufacturer::get_all();

        // Get all the manufacturer's models if the manufacturer_id is set
        if (isset($vehicle_dropdown['manufacturer_id'])) {
            $this->data['models_dropdown'] = VehicleModel::get_all_from_manufacturer_id($vehicle_dropdown['manufacturer_id']);
        }

        // Get all the model's bodies if the model_id is set
        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['bodies_dropdown'] = VehicleBody::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        // Get all the body registrations if the body_id is set
        if (isset($vehicle_dropdown['body_id'])) {
            $this->data['registrations_dropdown'] = VehicleRegistration::get_all_from_body_id($vehicle_dropdown['body_id']);
        }

        return view('electrics.index', $this->data);
    }

    /**
     * Display a listing models.
     *
     * @return \Illuminate\Http\Response
     */
    public function model_list($manufacturer_slug)
    {
        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        if (empty($manufacturer)) {
            return redirect()->route('electrics')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['models'] = VehicleModel::get_all_from_manufacturer_id($manufacturer->manufacturerId);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('electrical-kits.model_list', $manufacturer_slug));

        // Build the vehicle_dropdown session array
        $this->data['vehicle_dropdown'] = $vehicle_dropdown = build_dropdown($manufacturer->manufacturerId);

        // If the manufacturer_id set then get the models for the dropdown
        if (isset($vehicle_dropdown['manufacturer_id'])) {
            $this->data['models_dropdown'] = VehicleModel::get_all_from_manufacturer_id($vehicle_dropdown['manufacturer_id']);
        }

        // If the model_id set then get the bodies for the body dropdown
        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['bodies_dropdown'] = VehicleBody::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        // If the body_id set then get the registrations for the dropdown
        if (isset($vehicle_dropdown['body_id'])) {
            $this->data['registrations_dropdown'] = VehicleRegistration::get_all_from_body_id($vehicle_dropdown['body_id']);
        }

        return view('electrics.models', $this->data);
    }

    /**
     * Display a listing body types.
     *
     * @return \Illuminate\Http\Response
     */
    public function body_list($manufacturer_slug, $model_slug)
    {
        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        // First check if the manufacturer exists
        if (empty($manufacturer)) {
            return redirect()->route('electrics')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        // Check if the model exists
        if (empty($model)) {
            return redirect()->route('electrical-kits.model_list', $manufacturer_slug)->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $this->data['bodies'] = VehicleBody::get_all_from_model_id($model->modelId);

        // Set up the breadcrumbs
        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('electrical-kits.model_list', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('electrical-kits.model_list.body_list', [$manufacturer_slug, $model_slug]));

        // Build the vehicle_dropdown session array
        $this->data['vehicle_dropdown'] = $vehicle_dropdown = build_dropdown($manufacturer->manufacturerId, $model->modelId);

        // If the model_id set then get the bodies for the body dropdown
        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['bodies_dropdown'] = VehicleBody::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        // If the body_id set then get the registrations for the dropdown
        if (isset($vehicle_dropdown['body_id'])) {
            $this->data['registrations_dropdown'] = VehicleRegistration::get_all_from_body_id($vehicle_dropdown['body_id']);
        }

        $this->data['model_slug'] = $model_slug;

        $this->data['noindex_follow_page'] = 'true';

        return view('electrics.bodies', $this->data);
    }

    /**
     * List of the vehicle bodies.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registration_list($manufacturer_slug, $model_slug, $body_slug)
    {

        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        // First check if the manufacturer exists
        if (empty($manufacturer)) {
            return redirect()->route('electrical-kits')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        // Check if the model exists
        if (empty($model)) {
            return redirect()->route('electrical-kits.model_list', $manufacturer_slug)->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $this->data['body'] = $body = VehicleBody::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug, 'bodySlug' => $body_slug]);

        // Check if the model exists
        if (empty($body)) {
            return redirect()->route('electrical-kits.model_list.body_list', [$manufacturer_slug, $model_slug])->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $vehicle_dropdown = build_dropdown($body->manufacturerId, $body->modelId, $body->bodyId);

        $this->data['bodies'] = VehicleBody::get_all_from_model_id($model->modelId);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('electrical-kits.model_list', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('electrical-kits.model_list.body_list', [$manufacturer_slug, $model_slug]));

        // Build the vehicle_dropdown session array
        $vehicle_dropdown = build_dropdown($manufacturer->manufacturerId, $model->modelId);

        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['bodies_dropdown'] = VehicleBody::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        if (isset($vehicle_dropdown['body_id'])) {
            $this->data['registrations_dropdown'] = VehicleRegistration::get_all_from_body_id($vehicle_dropdown['body_id']);
        }

        $this->data['breadcrumbs']->addCrumb($this->data['body']->bodyName, route('electrical-kits.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug]));

        $this->data['vehicle_dropdown'] = $vehicle_dropdown;

        $this->data['model_slug'] = $model_slug;

        $this->data['noindex_follow_page'] = 'true';

        return view('electrics.bodies', $this->data);
    }

    /**
     * Display a listing vehicles.
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicle_list($manufacturer_slug, $model_slug, $body_slug, $registration_id)
    {

        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        // First check if the manufacturer exists
        if (empty($manufacturer)) {
            return redirect()->route('electrical-kits')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        // Check if the model exists
        if (empty($model)) {
            return redirect()->route('electrical-kits.model_list', $manufacturer_slug)->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $this->data['body'] = $body = VehicleBody::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug, 'bodySlug' => $body_slug]);

        // Check if the model exists
        if (empty($body)) {
            return redirect()->route('electrical-kits.model_list.body_list', [$manufacturer_slug, $model_slug])->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $this->data['vehicles'] = $vehicles = Vehicle::get_where(['bodyId' => $body->bodyId, 'registrationId' => $registration_id]);

        if (empty($vehicles)) {
            return redirect()->route('electrical-kits.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug])->with('message', 'Sorry, we could not find that year. Please select a year from below.');
        }

        // Check if there is only one vehicle if so redirect to towbars.
        if (count($this->data['vehicles']) == 1) {
            return redirect()->to(route('electrics.by_selection', [$manufacturer_slug, $model_slug, $body_slug, $registration_id, $this->data['vehicles'][0]->slug]));
        }

        $this->data['registration_id'] = $registration_id;

        // Set the dropdown values as the user has navigated to this car type.
        $vehicle_dropdown = build_dropdown($manufacturer->manufacturerId, $model->modelId, $body->bodyId, $registration_id);

        // If there are no vehicles to display get the reg details for our message and form.
        if (empty($this->data['vehicles'])) {
            $registrations = VehicleRegistration::get_all_from_body_id($body->bodyId);

            foreach ($registrations as $value) {
                if ($value->registrationId == $registration_id) {
                    $this->data['registration'] = $value;
                    break;
                }
            }

            $this->data['back_link'] = route('electrical-kits.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug]);
        }

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('electrical-kits.model_list', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('electrical-kits.model_list.body_list', [$manufacturer_slug, $model_slug]));
        $this->data['breadcrumbs']->addCrumb($this->data['body']->bodyName, route('electrical-kits.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug]));
        $this->data['breadcrumbs']->addCrumb('Registration', route('electrical-kits.model_list.body_list.registration_list.vehicle_list', [$manufacturer_slug, $model_slug, $body_slug, $registration_id]));

        // No-index, follow meta.
        $this->data['noindex_follow_page'] = true;

        return view('electrics.vehicles', $this->data);
    }

    /**
     * List towbars
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicle_kits($manufacturer_slug, $model_slug, $body_slug, $registration_id, $vehicle_slug)
    {
        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        // First check if the manufacturer exists
        if (empty($manufacturer)) {
            return redirect()->route('electrical-kits')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        // Check if the model exists
        if (empty($model)) {
            return redirect()->route('electrical-kits.model_list', $manufacturer_slug)->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $this->data['body'] = $body = VehicleBody::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug, 'bodySlug' => $body_slug]);

        // Check if the model exists
        if (empty($body)) {
            return redirect()->route('electrical-kits.model_list.body_list', [$manufacturer_slug, $model_slug])->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        // Get a list of the available kits using this vehicle slug
        $this->data['kits'] = Electric::get_where(['vehicleSlug' => $vehicle_slug]);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('electrical-kits.model_list', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('electrical-kits.model_list.body_list', [$manufacturer_slug, $model_slug]));
        $this->data['breadcrumbs']->addCrumb($this->data['body']->bodyName, route('electrical-kits.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug]));
        $this->data['breadcrumbs']->addCrumb('Registration', route('electrical-kits.model_list.body_list.registration_list.vehicle_list', [$manufacturer_slug, $model_slug, $body_slug, $registration_id]));
        $this->data['breadcrumbs']->addCrumb('Vehicle', route('electrics.by_selection', [$manufacturer_slug, $model_slug, $body_slug, $registration_id, $vehicle_slug]));

        // No-index, follow meta.
        $this->data['noindex_follow_page'] = true;

        return view('electrics.kits_list', $this->data);
    }

    /**
     * List towbars
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicle_kits_by_reg($vehicle_id, $registration_date)
    {
        // Get a list of the available towbars using this vehicle slug
        $this->data['kits'] = $towbars = Electric::get_kits_by_id($vehicle_id, $registration_date);
        $this->data['registration_date'] = $registration_date;

        // No-index, follow meta.
        $this->data['noindex_follow_page'] = true;

        return view('electrics.kits_list', $this->data);
    }

    /**
     * View individual electrical kit
     *
     * @return \Illuminate\Http\Response
     */
    public function view($partNo)
    {
        // Get the towbar
        $this->data['kit'] = $kit = Electric::get_single_where(['partNo' => decode_url($partNo)]);

        // If an empty data set returned form the API the redirect the user to the electrical kits page.
        if (empty($kit->partNo)) {
            return redirect(route('electrics'), 301);
        }

        return view('electrics.view', $this->data);
    }
}
