<?php

return [

    // Set up the domains types for basic multi tenant config
    'domains' => [
        /*
        Fitters config.
         */
        'fitters.witter-towbars.co.uk' => [
            'type' => 'trade',
            'theme' => 'trade',
            'redirect_url' => 'https://www.witter-towbars.co.uk/',
        ],
        'www.localfitter.witter-towbars.co.uk' => [
            'type' => 'trade',
            'theme' => 'trade',
            'redirect_url' => 'http://www.local.witter-towbars.co.uk/',
        ],
        'witter-fitter-staging.rnmtest.co.uk' => [
            'type' => 'trade',
            'theme' => 'trade',
            'redirect_url' => 'http://witter-staging.rnmtest.co.uk/',
        ],
        /*
        Default config.
         */
        'www.witter-towbars.co.uk' => [
            'type' => 'default',
        ],
        'www.local.witter-towbars.co.uk' => [
            'type' => 'default',
        ],
        'witter-staging.rnmtest.co.uk' => [
            'type' => 'default',
        ],
        'witter-staging2.rnmtest.co.uk' => [
            'type' => 'default',
        ],
    ],
];
