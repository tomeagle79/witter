
@extends('layouts.frontend')

@section('title', 'Contact Us')

@section('heading')
	Contact Us
@stop

@section('JSON-LD')
<script type='application/ld+json'>
{
  "@context": "http://www.schema.org",
  "@type": "AutoPartsStore",
  "name": "Witter Towbars",
  "url": "https://www.witter-towbars.co.uk/",
  "logo": "https://www.witter-towbars.co.uk/assets/img/logo-white-curve.png",
  "image": "https://www.witter-towbars.co.uk/imagecache/original/UnCP32Hqek1q6PDEWOB1CCNqTo4niFOSgV8bPy0B.jpg",
  "description": "Towbar fitting by Witter Towbars. Our all-in-one price includes towbar, electric kit and fitting. Buy your towbar and arrange a fitting online today.",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "6-11 Drome Road, Deeside Industrial Park",
    "addressLocality": "Deeside",
    "addressRegion": "Flintshire",
    "postalCode": "CH5 2NY",
    "addressCountry": "UK"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": "53.228828",
    "longitude": "-2.997269"
  },
  "hasMap": "https://www.google.co.uk/maps/place/Witter+Towbars/@53.2288694,-2.9995062,17z/data=!3m1!4b1!4m5!3m4!1s0x487adb9c245e5fdd:0x9fbeb592ca60235c!8m2!3d53.2288662!4d-2.9973122",
  "openingHours": "Mo, Tu, We, Th, Fr 08:30-17:30",
  "contactPoint": {
    "@type": "ContactPoint",
    "contactType": "customer service",
    "telephone": "+44(0)1244 284555"
  },
  "telephone": "+44(0)1244 284555"
}
 </script>
@stop

@section('content')

<div class="container contact">

	<div class="row">
		<div id="map" class="col-md-12 contact-map"></div>
	</div>


	<div class="row">
		<div class="address col-md-3">
			<h2 class="border-bottom">Find Us</h2>
			<p>
				6-11 Drome Road<br>
				Deeside Industrial Park<br>
				Deeside<br>
				CH5 2NY
			</p><br>
			<p>
				Call us on<br>
				<span class="color-secondary">01244 284555</span><br><br>
				Send us an email<br>
				<span class="color-secondary"><a href="mailto:ecommerce@witter-towbars.co.uk" class="email-font">ecommerce@witter-towbars.co.uk</a></span>
			</p>
		</div>

		<div class="col-md-4 col-md-offset-1 text-center get-in-touch">
			<h2 class="border-bottom">Get in Touch</h2>

			<ul>
			    @foreach($errors->all() as $error)
			        <li>{{ $error }}</li>
			    @endforeach
			</ul>

			@if(Session::has('message'))
			    <div class="alert alert-info">
			      {{Session::get('message')}}
			    </div>
			@endif

			{!! Form::open(array('route' => 'contact.submit', 'class' => 'form', 'id' => 'contact_form')) !!}

				{{--
				<div class="form-group">
				    {!! Form::label('enquiry_type', 'Enquiry Type', array('class' => 'sr-only')) !!}

				    {!! Form::select('enquiry_type', ['default' => 'Enquiry Type', 'General' => 'General', 'Question' => 'Question'], null, ['class' => 'form-control']) !!}
				</div>
				--}}

				<div class="form-group">
				    {!! Form::label('name', 'Name', array('class' => 'sr-only')) !!}

				    {!! Form::text('name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
				</div>

				<div class="form-group">

			    	{!! Form::label('email', 'Email Address', array('class' => 'sr-only')) !!}

			    	{!! Form::text('email', null,
				    	array('required', 'class'=>'form-control', 'placeholder'=>'Email Address')) !!}
				</div>

				<div class="form-group">

			    	{!! Form::label('phone', 'Telephone', array('class' => 'sr-only')) !!}

			    	{!! Form::text('phone', null,
				    	array('required', 'class'=>'form-control', 'placeholder'=>'Telephone')) !!}
				</div>

				<div class="form-group">

			    	{!! Form::label('postcode', 'Postcode', array('class' => 'sr-only')) !!}

			    	{!! Form::text('postcode', null,
				    	array('class'=>'form-control', 'placeholder'=>'Postcode')) !!}
				</div>

				<div class="form-group">

				    {!! Form::label('enquiry', 'Enquiry', array('class' => 'sr-only')) !!}

				    {!! Form::textarea('enquiry', null,
				        array('required', 'class'=>'form-control', 'placeholder'=>'Enquiry')) !!}
				</div>

				<div class="form-group">

				    <button type="submit" class="btn btn-primary btn-primary-large ">Submit Enquiry </button>

				</div>
			{!! Form::close() !!}
		</div>

		<div class="col-md-3 col-md-offset-1 text-right opening-times">
			<h2 class="border-bottom">Opening Times</h2>

			<table class="table table-condensed table-openinghours">
				<tbody>
					@foreach($openingHours as $openingtime)
					<tr @if(strtoupper($openingtime['day']) == strtoupper(date('l'))) class="today"@endif>
						<td class="text-left">{{ $openingtime['day'] }}</td>
						<td class="text-right">{{ $openingtime['times'] }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>
	</div>
</div>

@stop

@section('scripts')

    <script type="text/javascript">
		function initMap() {
			var witter = {lat: 53.228828, lng:  -2.997269};
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 12,
				center: witter
			});

			var marker = new google.maps.Marker({
			  	position: witter,
			  	map: map,
			 	title: ''
			});
		}

		$(document).ready(function(){
		    $("form#contact_form").submit(function() {
		        $('button.btn').attr('disabled', 'disabled');
		        return true;
		    });
		});

    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-VmDhNlxlKQoxC7F7YXoQvmS2APd6Qw0&callback=initMap">
    </script>
@stop


