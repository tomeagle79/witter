<?php

namespace App\Services;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\Item;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Api\Details;
use PayPal\Api\PaymentExecution;
use Log;
use App\Models\PaypalPayment;

class PayPal
{
    private $apiContext;
    private $payer;
    private $redirectUrls;
    private $payment;

    public function __construct()
    {
        /**
         * https://developer.paypal.com/webapps/developer/applications/myapps
         */
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                config('paypal.credentials.client_id'),
                config('paypal.credentials.client_secret')
            )
        );

        /**
         * Payment.
         */
        $this->payment = new Payment();

        /**
         * Payer.
         */
        $this->payer = new Payer();
        $this->payer->setPaymentMethod('paypal');

        /**
         * Redirect URLs.
         */
        $this->redirectUrls = new RedirectUrls();
        $this->redirectUrls->setReturnUrl(route('checkout.section3'))
            ->setCancelUrl(route('checkout.section3'));

        /**
         * Set up the PayPal apiContext config
         */
        $paypal_context = [];

        /**
         * Enable logging if log_api_calls is true.
         */
        if(config('paypal.log_api_calls')) {

            $paypal_context = [
                'log.LogEnabled' => true,
                'log.FileName' => storage_path('logs/paypal-'.date('Y-m-d').'.log'),
                'log.LogLevel' => 'DEBUG',
            ];
        }

        // Check if we are using the paypal sandbox if not set the mode to LIVE
        if(empty(config('paypal.paypal_sandbox'))) {

            $paypal_context['mode'] = 'live';
        }

        $this->apiContext->setConfig($paypal_context);

    }

    public function create_payment()
    {
        $cart = build_cart();

        /**
         * Amount.
         */
        $amount = new Amount();
        $amount->setTotal($cart['grand_total']);
        $amount->setCurrency('GBP');

        /**
         * Transaction.
         */
        $transaction = new Transaction();
        $transaction->setAmount($amount);

        /**
         * Payment.
         */
        $this->payment
            ->setIntent('sale')
            ->setPayer($this->payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($this->redirectUrls);

        /**
         * Experience Profile.
         */
        if(config('paypal.experience_profile_id')) {
            $this->payment->setExperienceProfileId(config('paypal.experience_profile_id'));
        }

        /**
         * Attempt to call the PayPal API.
         */
        try {

            $this->payment->create($this->apiContext);

            return response($this->payment, 200)
                ->header('Content-Type', 'application/json');
        }
        catch (PayPalConnectionException $ex) {
            /**
             * There was an error - debug.
             */
            Log::error(json_encode($ex->getData()));
            Log::error($ex->getMessage());

            $arr = [];
            $arr['success'] = false;
            $arr['error'] = 'There was an issue when trying to contact PayPal. Please try again in a few minutes, or pay with another method such as credit or debit card.';

            return response()->json($arr);
        }
    }

    public function charge_payment($paymentId, $payerID, $total)
    {
        /**
         * Payment log.
         */
        $payment_log = new PaypalPayment();
        $payment_log->paypal_payment_id = $paymentId;
        $payment_log->payer_id = $payerID;
        $payment_log->save();

        /**
         * Payment.
         */
        $payment = Payment::get($paymentId, $this->apiContext);

        /**
         * Details.
         */
        $details = new Details();
        $details->setSubtotal($total);

        /**
         * Amount.
         */
        $amount = new Amount();
        $amount->setCurrency('GBP');
        $amount->setTotal($total);
        $amount->setDetails($details);

        /**
         * Transaction.
         */
        $transaction = new Transaction();
        $transaction->setAmount($amount);

        /**
         * Execution.
         */
        $execution = new PaymentExecution();
        $execution->setPayerId($payerID);
        $execution->addTransaction($transaction);

        try {
            // Execute the PayPal Payment
            // $this->apiContext->addRequestHeader('PayPal-Mock-Response', '{"mock_application_codes":"NEED_CREDIT_CARD_OR_BANK_ACCOUNT"}');

            $result = $payment->execute($execution, $this->apiContext);

            // Just check it the item exists before we save it to the log don't want this to cause the payment to fail.
            $payment_log->intent = empty($result->intent)? null : $result->intent;
            $payment_log->state = empty($result->state)? null : $result->state;
            $payment_log->cart = empty($result->cart)? null : $result->cart;
            $payment_log->payment_method = empty($result->payer->payment_method)? null : $result->payer->payment_method;
            $payment_log->payer_status = empty($result->payer->status)? null : $result->payer->statu;
            $payment_log->payer_email = empty($result->payer->payer_info->email)? null : $result->payer->payer_info->email;
            $payment_log->payer_first_name = empty($result->payer->payer_info->first_name)? null : $result->payer->payer_info->first_name;
            $payment_log->payer_last_name = empty($result->payer->payer_info->last_name)? null : $result->payer->payer_info->last_name;
            $payment_log->payer_id = empty($result->payer->payer_info->payer_id)? null : $result->payer->payer_info->payer_id;
            $payment_log->payer_country_code = empty($result->payer->payer_info->country_code)? null : $result->payer->payer_info->country_code;
            $payment_log->total = empty($result->transactions[0]->amount->total)? null : $result->transactions[0]->amount->total;
            $payment_log->currency = empty($result->transactions[0]->amount->currency)? null : $result->transactions[0]->amount->currency;
            $payment_log->save();

            return $result;

        } catch (\Exception $ex) {

            // Get the sanitised_errors array
            $error_arr = config('paypal.sanitised_errors');

            // Ultra save check if getData is not empty
            if(method_exists($ex, 'getData') && !empty($ex->getData())){

                $data = json_decode($ex->getData());

                // Log the errors and return a error array.
                Log::error("Executed Payment Failed $paymentId =" . $paymentId . "$payerID =" . $payerID);
                Log::error($ex->getData());

                // Check if error name in error array
                if(array_key_exists($data->name, $error_arr) && !empty($error_arr[$data->name])) {

                    $error_message = $error_arr[$data->name];
                }else{

                    $error_message = $error_arr['GENERAL'];
                }
            }else{

                // Log the errors and return a error array.
                Log::error("Executed Payment Failed $paymentId =" . $paymentId . "$payerID =" . $payerID);
                Log::error($ex->getMessage());

                $error_message = $error_arr['GENERAL'];
            }

            $arr = [];
            $arr['success'] = false;
            $arr['error'] = $error_message;

            return $arr;
        }
    }
}
