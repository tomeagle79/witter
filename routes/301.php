<?php

// Home
Route::get('/index.php', function(){
    return redirect('/', 301);
});
Route::get('/index', function(){
    return redirect('/', 301);
});
Route::get('/default.aspx', function(){
    return redirect('/', 301);
});
Route::get('/default.asp', function(){
    return redirect('/', 301);
});
Route::get('/,', function(){
    return redirect('/', 301);
});

// Bike Carriers
Route::get('/cycle-carriers', function(){
    return redirect('/bike-racks', 301);
});
Route::get('/accessories/cycle-carriers', function(){
    return redirect('/bike-racks', 301);
});

Route::get('/accessories/cycle-carriers/cycle-carrier--2-bikes', function(){
    return redirect('/bike-racks', 301);
});
Route::get('/accessories/cycle-carriers/cycle-carrier-3-bikes', function(){
    return redirect('/bike-racks', 301);
});
Route::get('/accessories/cycle-carriers/cycle-carrier-4-bikes', function(){
    return redirect('/bike-racks', 301);
});
Route::get('/accessories/cycle-carriers/cycle-carrier-tow-and-carry', function(){
    return redirect('/bike-racks', 301);
});
Route::get('/accessories/cycle-carriers/cycle-carrier-rear-mounted', function(){
    return redirect('/bike-racks', 301);
});

Route::get('/accessory/zx202', function(){
    return redirect('/bike-racks/bolt-on-towball-mounted-2-bike-carrier', 301);
});
Route::get('/accessory/zx302', function(){
    return redirect('/bike-racks/clamp-on-towball-mounted-2-bike-carrier', 301);
});
Route::get('/accessory/zx502', function(){
    return redirect('/bike-racks/clamp-on-towball-mounted-tilting-2-bike-carrier', 301);
});
Route::get('//accessory/zxe502', function(){
    return redirect('/bike-racks/towball-mounted-tilting-2-electric-bike-carrier', 301);
});
Route::get('/accessory/350030600001', function(){
    return redirect('/bike-racks/westfalia-towball-mounted-tilting-2-bike-carrier', 301);
});
Route::get('/accessory/350053600001', function(){
    return redirect('/bike-racks/bikelander-classic-towball-mounted-tilting-2-bike-carrier', 301);
});
Route::get('/accessory/350050600001', function(){
    return redirect('/bike-racks/bikelander-led-towball-mounted-tilting-2-bike-carrier', 301);
});
Route::get('/accessory/350030600001-kit', function(){
    return redirect('/bike-racks/bc-60-towball-mounted-tilting-2-bike-carrier', 301);
});
Route::get('/accessory/350050600001-kit', function(){
    return redirect('/bike-racks/bikelander-towball-mounted-tilting-2-bike-carrier-box-platform', 301);
});
Route::get('/accessory/350046600001', function(){
    return redirect('/bike-racks/c1-aygo-108-bike-carrier-support-system', 301);
});
Route::get('/accessory/350047600001', function(){
    return redirect('/bike-racks/c1-aygo-108-bike-carrier-support-system', 301);
});

Route::get('/accessory/zx203', function() {
    return redirect('/bike-racks/bolt-on-towball-mounted-3-bike-carrier', 301);
});
Route::get('/accessory/zx303', function() {
    return redirect('/bike-racks/clamp-on-towball-mounted-3-bike-carrier', 301);
});
Route::get('/accessory/zx503', function() {
    return redirect('/bike-racks/towball-mounted-tilting-3-bike-carrier', 301);
});
Route::get('//accessory/zbc2035', function() {
    return redirect('/bike-racks/towball-mounted-3-bike-carrier', 301);
});
Route::get('//accessory/zbc2085', function() {
    return redirect('/bike-racks/rear-mounted-3-bike-carrier-cradle-straps', 301);
});

Route::get('/accessory/zx204', function() {
    return redirect('/bike-racks/bolt-on-towball-mounted-4-bike-carrier', 301);
});
Route::get('/accessory/zx304', function() {
    return redirect('/bike-racks/clamp-on-towball-mounted-4-bike-carrier', 301);
});
Route::get('/accessory/zx504', function() {
    return redirect('/bike-racks/towball-mounted-tilting-4-bike-carrier', 301);
});

Route::get('/accessory/zx88', function() {
    return redirect('/bike-racks/flange-towbar-mounted-3-4-bike-carrier', 301);
});
Route::get('/accessory/zx89', function() {
    return redirect('/bike-racks/clamp-flange-towbar-mounted-3-bike-carrier', 301);
});
Route::get('/accessory/zx79', function() {
    return redirect('/bike-racks/clamp-flange-towbar-mount-3-bike-carrier', 301);
});
Route::get('/accessory/zx99', function() {
    return redirect('/bike-racks/flange-towbar-mounted-3-bike-carrier-spare-wheel-110mm', 301);
});
Route::get('/accessory/zx98', function() {
    return redirect('/bike-racks/flange-towbar-mounted-3-4-bike-carrier-spare-wheel-110mm', 301);
});
Route::get('/accessory/zx109', function() {
    return redirect('/bike-racks/flange-towbar-mounted-3-bike-carrier-spare-wheel-187mm', 301);
});
Route::get('/accessory/zx108', function() {
    return redirect('/bike-racks/flange-towbar-mounted-3-4-bike-carrier-spare-wheel-187mm', 301);
});

Route::get('/accessory/zrb1070', function() {
    return redirect('/bike-racks/mway-harrier-roof-bar-mount-single-bike', 301);
});

// Towbars
Route::get('/towbars/findtowbar1.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/whybuy.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/ukreg.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/faq1.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/faq2.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/faq3.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/faq4.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/faq5.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/faq6.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/faq7.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/faq8.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/faq9.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/auto', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/ajax_bodies', function(){
    return redirect('/towbars', 301);
});
Route::get('/towbars/vauxhall/opel', function(){
    return redirect('/towbars/vauxhall', 301);
});
Route::get('/towbars/land-rover/range-rover', function(){
    return redirect('/towbars/land-rover-range-rover', 301);
});
Route::get('/towbars/towbars/ldv-/-leyland/daf', function(){
    return redirect('/towbars', 301);
});
Route::get('/productinformation.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/product_review.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/product_review2.php', function(){
    return redirect('/towbars', 301);
});
Route::get('/product_review2.php', function(){
    return redirect('/towbars', 301);
});

Route::get('/how-to-guide-cycle-carriers', function(){
 return redirect('/how-to-guide-bike-racks', 301);
});


Route::get('/towbars/towbars.php', function(){ // Handle /towbars.php and various query strings
    $make = Input::get('make');

    if(empty($make)) {
        return redirect('/towbars', 301);
    } else {
        // Typical URI is ?make=Alfa+Romeo-towbars&id=2
        $make_arr = explode('-', $make);
        if(empty($make_arr)) {
            return redirect('/towbars', 301);
        } else {
            // Replace + and spaces to dashes and lowercase
            $make_uri = str_replace('+', '-', $make_arr[0]);
            $make_uri = str_replace(' ', '-', $make_uri);
            $make_uri = strtolower($make_uri);

            return redirect('/towbars/'. $make_uri, 301);
        }
    }
});

Route::get('/towbars/towbar2.php', function(){ // Handle /towbars.php and various query strings
    $make = Input::get('make');

    if(empty($make)) {
        return redirect('/towbars', 301);
    } else {
        // Typical URI is ?make=Alfa+Romeo-towbars&id=2
        $make_arr = explode('-', $make);
        if(empty($make_arr)) {
            return redirect('/towbars', 301);
        } else {
            // Replace + and spaces to dashes and lowercase
            $make_uri = str_replace('+', '-', $make_arr[0]);
            $make_uri = str_replace(' ', '-', $make_uri);
            $make_uri = strtolower($make_uri);

            return redirect('/towbars/'. $make_uri, 301);
        }
    }
});

Route::get('/towbars/towbar.php', function(){ // Handle /towbars.php and various query strings
    $make = Input::get('make');

    if(empty($make)) {
        return redirect('/towbars', 301);
    } else {
        // Typical URI is ?make=Alfa+Romeo-towbars&id=2
        $make_arr = explode('-', $make);
        if(empty($make_arr)) {
            return redirect('/towbars', 301);
        } else {
            // Replace + and spaces to dashes and lowercase
            $make_uri = str_replace('+', '-', $make_arr[0]);
            $make_uri = str_replace(' ', '-', $make_uri);
            $make_uri = strtolower($make_uri);

            return redirect('/towbars/'. $make_uri, 301);
        }
    }
});
Route::get('/towbars/selectvehicle.php', function(){ // Handle /towbars.php and various query strings
    $make = Input::get('make');

    if(empty($make)) {
        return redirect('/towbars', 301);
    } else {
        // Typical URI is ?make=Alfa+Romeo-towbars&id=2
        $make_arr = explode('-', $make);
        if(empty($make_arr)) {
            return redirect('/towbars', 301);
        } else {
            // Replace + and spaces to dashes and lowercase
            $make_uri = str_replace('+', '-', $make_arr[0]);
            $make_uri = str_replace(' ', '-', $make_uri);
            $make_uri = strtolower($make_uri);

            return redirect('/towbars/'. $make_uri, 301);
        }
    }
});

Route::get('/towbars/modelyear.php', function(){ // Handle /towbars.php and various query strings
    $make = Input::get('make');

    if(empty($make)) {
        return redirect('/towbars', 301);
    } else {
        // Typical URI is ?make=Alfa+Romeo-towbars&id=2
        $make_arr = explode('-', $make);
        if(empty($make_arr)) {
            return redirect('/towbars', 301);
        } else {
            // Replace + and spaces to dashes and lowercase
            $make_uri = str_replace('+', '-', $make_arr[0]);
            $make_uri = str_replace(' ', '-', $make_uri);
            $make_uri = strtolower($make_uri);

            return redirect('/towbars/'. $make_uri, 301);
        }
    }
});

// Accessories
Route::get('/towbar_acc/index.php', function(){
    return redirect('/accessories', 301);
});
Route::get('/towbar_acc', function(){
    return redirect('/accessories', 301);
});
Route::get('/towbar_acc/couplings.php', function(){
    return redirect('/accessories/couplings', 301);
});
Route::get('/towbar_acc/couplingacc.php', function(){
    return redirect('/accessories/couplings', 301);
});
Route::get('/towbar_acc/bumpershields.php', function(){
    return redirect('/accessories/covers-shields', 301);
});
Route::get('/towbar_acc/elecuniversal.php', function(){
    return redirect('/accessories/electric-kits-standard', 301);
});
Route::get('/towbar_acc/elecdedicated1.php', function(){
    return redirect('/accessories/electric-kits-standard/universal-electric-kits', 301);
});
Route::get('/towbar_acc/towsteps.php', function(){
    return redirect('/accessories/towsteps', 301);
});
Route::get('/towbar_acc/elecaccessories.php', function(){
    return redirect('/accessories/electrical-accessories', 301);
});
Route::get('/electrics/towbar.aspx', function(){
    return redirect('/electrical-kits', 301);
});
Route::get('/cycle_carriers/findcyclecarrier1.php', function(){
    return redirect('/accessories/cycle-carriers', 301);
});
Route::get('/cycle_carriers/cyclecarrieracc.php', function(){
    return redirect('/accessories/cycle-accessories', 301);
});
Route::get('/cycle_carriers/cyclecarrierspares.php', function(){
    return redirect('/accessories/cycle-carriers', 301);
});
Route::get('/cycle_carriers/mountingguidelines.php', function(){
    return redirect('/accessories/cycle-carriers', 301);
});
Route::get('/cycle_carriers/findcyclecarrier1.php', function(){
    $category_id = Input::get('partCategoryId');

    switch($category_id) {
        case 3:
            return redirect('/accessories/cycle-carriers/cycle-carrier--2-bikes', 301);
        case 38:
            return redirect('/accessories/cycle-carriers/cycle-carrier-3-bikes', 301);
        case 35:
            return redirect('/accessories/cycle-carriers/cycle-carrier-4-bikes', 301);
        case 4:
            return redirect('/accessories/cycle-carriers/cycle-carrier-tow-and-carry', 301);
        default:
            return redirect('/accessories/cycle-carriers', 301);
    }
});
Route::get('/towbar_acc/towsteps.php', function(){
    return redirect('/accessories/towsteps', 301);
});
Route::get('/towbar_acc/witter-towsteps.php', function(){
    $id = Input::get('id');

    switch($id) {
        case 18:
            return redirect('/accessories/towsteps/single-sided-step', 301);
        case 12:
            return redirect('/accessories/towsteps/double-sided-step', 301);
        case 40:
            return redirect('/accessories/towsteps/full-width-step', 301);
        case 41:
            return redirect('/accessories/towsteps/towbar-steps', 301);
        case 42:
            return redirect('/accessories/towsteps/platform-steps', 301);
        default:
            return redirect('/accessories/towsteps', 301);
    }
});

// Roof Systems
Route::get('/roof_racks/index.php', function(){
    return redirect('/roof-systems', 301);
});
Route::get('/roof_racks', function(){
    return redirect('/roof-systems', 301);
});
Route::get('/roof-systems/by-manufacturer/vauxhall/opel', function(){
    return redirect('/roof-systems/by-manufacturer/vauxhall', 301);
});
Route::get('/roof-systems/by-manufacturer/land-rover/range-rover', function(){
    return redirect('/roof-systems/by-manufacturer/land-rover-range-rover', 301);
});

Route::get('/roof_racks/roof-racks.php', function(){
    return redirect('/roof-systems/roof-decks', 301);
});
Route::get('/roof_racks/roof-system.php', function(){
    return redirect('/roof-systems/roof-decks', 301);
});
Route::get('/roof_racks/roof-system-model.php', function(){
    return redirect('/roof-systems/roof-decks', 301);
});
Route::get('/buy-roof-system.php', function(){
    return redirect('/roof-systems/roof-decks', 301);
});
Route::get('/roof_racks/roof-accessories-products.php', function(){
    return redirect('/roof-systems/roof-decks', 301);
});
Route::get('/roof_racks/roof-bars.php', function(){
    return redirect('/roof-systems/roof-bars', 301);
});
Route::get('/roof_racks/roof-accessories.php', function(){
    return redirect('/roof-systems/accessories', 301);
});
Route::get('/roof_racks/roof-system-make.php', function(){
    $make = Input::get('make');

    if(empty($make)) {
        return redirect('/roof-systems', 301);
    } else {
        // Typical URI is ?make=Alfa+Romeo-towbars&id=2
        $make_arr = explode('-', $make);
        if(empty($make_arr)) {
            return redirect('/roof-systems', 301);
        } else {
            // Replace + and spaces to dashes and lowercase
            $make_uri = str_replace('+', '-', $make_arr[0]);
            $make_uri = str_replace(' ', '-', $make_uri);
            $make_uri = strtolower($make_uri);

            return redirect('/roof-systems/by-manufacturer/'. $make_uri, 301);
        }
    }
});

// Help & advice
Route::get('/towbars/whichtowbar.php', function(){
    return redirect('/help-advice/which-towbar', 301);
});
Route::get('/towbars/whywitter.php', function(){
    return redirect('/help-advice/why-a-witter-towbar', 301);
});
Route::get('/towbars/weights.php', function(){
    return redirect('/help-advice/weights-and-measures', 301);
});
Route::get('/towbars/rightcar.php', function(){
    return redirect('/help-advice/buying-the-right-car-for-towing', 301);
});
Route::get('/towbars/towsafe.php', function(){
    return redirect('/help-advice/why-a-witter-towbar', 301);
});
Route::get('/cycle_carriers/whichcyclecarrier.php', function(){
    return redirect('/help-advice/which-bike-carrier', 301);
});
Route::get('/quality_standards/ec_qanda.aspx', function(){
    return redirect('/faqs/ec-type-approval-faqs', 301);
});
Route::get('/quality_standards/ec_qanda.aspx/{unknown}', function(){
    return redirect('/faqs/ec-type-approval-faqs', 301);
});
// Pages
Route::get('/towbars/towsafe.php', function(){
    return redirect('/what-can-i-tow', 301);
});
Route::get('/about/towsafe.php', function(){
    return redirect('/what-can-i-tow', 301);
});
Route::get('/about/aboutwitter.php', function(){
    return redirect('/about', 301);
});
Route::get('/about/history.php', function(){
    return redirect('/about', 301);
});
Route::get('/about/engineering.php', function(){
    return redirect('/about', 301);
});
Route::get('/careers/becomewitterfitter.php', function(){
    return redirect('/about', 301);
});
Route::get('/about/stockists.php', function(){
    return redirect('/', 301);
});
Route::get('/about/contact.php', function(){
    return redirect('/contact-us', 301);
});
Route::get('/about/meetteam.php', function(){
    return redirect('/', 301);
});
Route::get('/cust/towbarreg.php', function(){
    return redirect('/product-registration/towbar', 301);
});
Route::get('/terms/warranty.php', function(){
    return redirect('/warranty-information', 301);
});
Route::get('/warranty/zq1', function(){
    return redirect('/product-registration/towbar', 301);
});
Route::get('/careers/careers.php', function(){
    return redirect('/about', 301);
});
Route::get('/cust/sitemap.php', function(){
    return redirect('/', 301);
});
Route::get('/terms/termsofuse.php', function(){
    return redirect('/terms-of-use', 301);
});
Route::get('/cust/termsconditions.php', function(){
    return redirect('/terms-and-conditions', 301);
});
Route::get('/terms/privacypolicy.php', function(){
    return redirect('/privacy', 301);
});
Route::get('/terms/towsafe.php', function(){
    return redirect('/help-advice/what-can-my-car-tow', 301);
});

// Trade zone
Route::get('/pricelists/towbars.aspx', function(){
    return redirect('http://tz.witter-towbars.co.uk/Login.aspx?ReturnURL=%2fPriceLists.aspx', 301);
});

// French site
Route::get('/fr/towbars/towbar.php', function(){
    return redirect('http://www.witter-towbars.fr/towbars/findTowbar1.php', 301);
});
Route::get('/fr', function(){
    return redirect('http://www.witter-towbars.fr/', 301);
});
Route::get('/fr/{unknown}', function(){
    return redirect('http://www.witter-towbars.fr/', 301);
});

// Cart
Route::get('/cart/shoppingcart.php', function(){
    return redirect('/cart', 301);
});
Route::get('/cart/cartsection1.php', function(){
    return redirect('/cart', 301);
});
Route::get('/Cart/addtocartboltkit.aspx', function(){
    return redirect('/cart', 301);
});

// News/Blog
Route::get('/news/news.php', function(){
    return redirect('/blog', 301);
});
Route::get('/videos/default.aspx', function(){
    return redirect('/blog', 301);
});
Route::get('/latestnews/', function(){
    $p = Input::get('p');

    switch($p) {
        case 1279:
            return redirect('/blog/the-uks-most-dangerous-road-trip', 301);
        case 1236:
            return redirect('/blog/the-most-instagrammed-locations-across-the-uk', 301);
        default:
            return redirect('/blog', 301);
    }
});

/*
    General redirects by folder.

    Relies on the folder name changing from one site to the other.
*/

// news folder
// e.g. /news/latestNews6.php
Route::get('/news/{unknown}', function(){
    return redirect('/blog', 301);
});

// cycle_carriers folder
// e.g. /cycle_carriers/cycle_carrier_ZX108.aspx
Route::get('/cycle_carriers/{unknown}', function(){
    return redirect('/accessories/cycle-carriers', 301);
});