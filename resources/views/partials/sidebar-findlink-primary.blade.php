
<div class="sidebar-findlink sidebar-findlink-primary hidden-sm hidden-xs">

    <a href="/what-can-i-tow"><h2 class="findlink-title">Find out <span class="slim block">what your car can tow safely</span></h2></a>

    <a href="/what-can-i-tow" class="sidebar-car-link"><span class="sr-only">Click here to find out what your car can pull</span></a>

</div>
