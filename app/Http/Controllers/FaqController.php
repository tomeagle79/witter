<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\HelpCategory;

class FaqController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('FAQs', route('faqs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['help_categories'] = HelpCategory::get();

        $categories = FaqCategory::get()->toArray();

        $this->data['categories'] = $this->buildCategories($categories);

        // Get and paginate blog posts
        $this->data['faqs'] = Faq::published()->orderBy('created_at', 'desc')->paginate(config('settings.pagination.faqs'));

        return view('faqs.index', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewCategory($url, $id)
    {
        $this->data['help_categories'] = HelpCategory::get();

        $this->data['faq_category'] = FaqCategory::find($id);

        $this->data['faqs'] = $this->data['faq_category']->faqs;

        $faq_categories = FaqCategory::get()->toArray();

        $this->data['categories'] = $this->buildCategories($faq_categories, $id);

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb($this->data['faq_category']->title);

        return view('faqs.index', $this->data);
    }

    /**
     * buildCategories is a helper method at the moment
     * simply turns our tree of FAQs into a nice multidimensional array
     *
     * @return array()
     */
    private function buildCategories($array, $active_id = null, $parent_id = null)
    {
        $result = [];

        foreach ($array as $row) {

            if ($row['parent_id'] == $parent_id) {
                $children = $this->buildCategories($array, $active_id, $row['id']);
                if ($children) {
                    $row['children'] = $children;
                }
                if (isset($active_id) && $row['id'] === $active_id) {

                    $row['active'] = true;
                }

                $result[] = $row;
            }
        }

        return $result;
    }
}
