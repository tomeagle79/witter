

@if(!empty($list_products))

<div class="container-fluid cart-upsells popular-products">

    <div class="container" data-scroll="0">
            <div class="row content">
                <div class="col-xs-12">
                    <h2>{{ $list_title }}</h2>
                </div>
            </div>
            <div class="row item-list is-flex slick-carousel" id="prod_results">

                @foreach($list_products as $product)
                {{-- Limit popular products feed to 4 items --}}
                @if($loop->iteration > 4)
                    @break
                @endif

                <div class="col-xs-12 col-sm-6 col-md-3 col-centered">
                    <div class="item">

                        @if(!empty($cart['webfit'] ) && !empty($product->webfitDiscountPercent))
                            <div class="item-header">
                                <a href="{!! buildLinkToAccessoryView($product) !!}">
                                    {{ $product->webfitDiscountPercent }}% Webfit discount available
                                </a>
                            </div><!-- /.item-header -->
                        @endif

                        <a class="image-link" href="{!! buildLinkToAccessoryView($product) !!}">
                            <img src="{{ image_load($product->imageUrl) }}" alt="" />
                        </a>
                        <a href="{!! buildLinkToAccessoryView($product) !!}">
                            <h3 class="list-title">{!! $product->title !!}</h3>
                        </a>
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="price">&pound;{{ price($product->displayPrice) }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <p class="price-rrp">@if($product->retailPrice > $product->price)RRP &pound;{{ price($product->retailPrice) }}@endif</p>
                            </div>
                        </div>

                        @if($product->isSellable && $product->inStock)
                        <div class="row stock">
                            <div class="col-xs-12">
                                <i class="icon-check"></i>
                                <p>In Stock</p>
                            </div>
                        </div>
                        @else
                        <div class="row stock">
                            <div class="col-xs-12">
                                <i class="icon-cancel-1"></i>
                                <p>Out of stock</p>
                            </div>
                        </div>
                        @endif @if($product->isSellable && $product->inStock)
                        <div class="row">
                            <div class="col-xs-7">
                                <a href="{!! buildLinkToAccessoryView($product) !!}" class="btn btn-primary">View Now</a>
                            </div>
                            {{-- .col-xs-7 --}}
                            <div class="col-xs-5">
                                @if(empty($product->fitOnly))
                                    <a href="{{ route('add_to_cart', [
                                                    'type' => 'accessory',
                                                    'partNo' => $product->partNo
                                                ]) }}" class="btn btn-primary btn-green-plus">
                                        <img src="/assets/img/basket.svg" height="20" width="20" alt="Add to basket">
                                    </a>
                                @endif
                            </div>
                        </div>
                        {{-- .row --}}
                        @else
                        <div class="row">
                            <div class="col-xs-12">
                                <a diabled class="btn btn-primary btn-oos-model">Notify me when stock arrives</a>
                            </div>
                        </div>
                        {{-- .row --}}
                        @endif @if($product->inStock == false)
                        <div class="out-of-stock-modal">
                            <div class="close-modal">
                                <i class="icon-cancel-1"></i>
                            </div>
                            <img src="{{URL('assets/img/icons', ['filename' => 'basket.svg'])}}" alt="">
                            <p>We’re sorry we have no stock</p>
                            <h5>We’d like to let you know when it’s back…</h5>
                            <p>Fill in your email address here and as soon as it comes back into stock we’ll send you an update:</p>

                            {!! Form::open(['route' => 'product.email_notify']) !!} {{ Form::email('email', null, ['id' => 'email', 'placeholder' => 'Email Address']) }} {{ Form::hidden('product_id', $product->partNo, ['id' => 'product_id']) }} {{ Form::hidden('product_name', $product->title, ['id' => 'product_name']) }} {{ Form::hidden('product_url', buildLinkToAccessoryView($product), ['id' => 'product_url']) }} {{ Form::hidden('checkbox_for_subscription', false, ['id' => 'checkbox_for_subscription']) }}

                            <button type="submit" class="btn btn-primary btn-green-basic">Notify Me</button>
                            <input type="checkbox" name="placeholder_for_checkbox" id="placeholder_for_checkbox">
                            <label for="placeholder_for_checkbox">Would you also like to subscribe to news and offers from Witter?</label>
                            {!! Form::close() !!}
                        </div>
                        @endif
                    </div>
                </div>
                @endforeach

            </div>
            {{-- .row --}}
        </div>

</div><!-- /.container-fluid -->

@endif
