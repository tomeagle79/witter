@extends('layouts.frontend')

@section('title', 'Light Commercial Vehicles - Models - Vehicles')

@section('heading')
	Light Commercial Vehicles - Models - Vehicles
@stop

@section('meta_description')
	Witter Light Commercial Vehicles - Models - Vehicles
@stop

@section('content')

    @if(!empty($vehicles))

    <div class="container towbars-listing">

    	<div class="row">
    		<div class="col-md-12">

    			@if( !empty($model->photoUrl) )
                    <div class="top-img-wrapper">
    					<img src="{{ $model->photoUrl }}" alt="" class="top-img">
                        <p class="text-center">Images are representative only</p>
                    </div> <!-- .wrapper -->
    			@endif
    			<h2>{{ $manufacturer->manufacturerName }}</h2>
    			<h3>{{ $model->modelName }}</h3>

    			<div class="item-list">
                    <?php
                    // Count
                    $count = count($vehicles);
                    ?>
    				@foreach ($vehicles as $key => $vehicle)

    					<?php $key++ ?>

    					@if($key%4 === 1)
    						<div class="row">
    					@endif

    					<div class="col-md-3">
                            <div class="item">
                                <div class="manufacturer-logo">
                                    <a href="{{ route('roof-systems.models.vehicles.products', [$manufacturer_slug, $model_slug, $vehicle->slug]) }}">
                                        <img src="{{ image_load($vehicle->photoUrl) }}" alt="Roof Bars for {{ $vehicle->vehicleName }}" >
                                    </a>
                                     <a href="{{ route('roof-systems.models.vehicles.products', [$manufacturer_slug, $model_slug, $vehicle->slug]) }}">
                                        <h3>{{ $vehicle->vehicleName }}</h3>
                                        <h4>
                                            @if(!empty($vehicle->fromMonth))
                                                {{ date('M', mktime(0, 0, 0, $vehicle->fromMonth, 1, $vehicle->fromYear)) }}
                                            @endif
                                            {{ $vehicle->fromYear }}
                                            -
    										@if(empty($vehicle->toYear))
    											Onwards
    										@else
    											@if(!empty($vehicle->toMonth))
    												{{ date('M', mktime(0, 0, 0, $vehicle->toMonth, 1, $vehicle->toMonth)) }}
    											@endif
    											{{ $vehicle->toYear }}
    										@endif
                                        </h4>
                                    </a>
                                </div>
                            </div> <!-- .item -->
    					</div>

    					@if($key%4 === 0 && $key > 0 || $key == $count)
    						</div><!-- .row -->
    					@endif
    				@endforeach

    			</div> <!-- .item-list -->

    		</div>
    	</div>
    </div>

    @else
        <div class="container towbars-product-listing">
            <div class="row">

                <div class="col-md-9">

                    <h2 class="nomargin">{{ $manufacturer->manufacturerName }} {{ $model->modelName }} </h2>

                    <div class="row contact">

                        <div class="col-xs-12">
                            <p>Please get in contact with our Witter Towbar experts who will be able to advise you on the best Roof System for your {{ $manufacturer->manufacturerName }} {{ $model->modelName }}.</p>
                        </div>

                    <div class="col-md-4 get-in-touch">
                        <h2 class="border-bottom">Contact us</h2>
                        <p>
                            Call us on<br>
                            <a href="tel:+4401244284555"><span class="color-secondary">01244 284555</span></a><br><br>
                            Send us an email<br>
                            <span class="color-secondary"><a href="mailto:ecommerce@witter-towbars.co.uk" class="email-font">ecommerce@witter-towbars.co.uk</a></span>
                        </p>
                    </div>

                    <div class="col-md-7 get-in-touch">
                        <h2 class="border-bottom">Email us</h2>

                        {!! Form::open(array('route' => 'contact.submit_product_enquiry', 'class' => 'form', 'id' => 'contact_form')) !!}

                            <input type="hidden" name="enquiry_regarding" value="Roof System for {{ $manufacturer->manufacturerName }} {{ $model->modelName }}">
                            <input type="hidden" name="website_url" value="{{ url()->current() }}">

                            <div class="form-group">

                                {!! Form::label('name', 'Name', array('class' => 'sr-only')) !!}

                                {!! Form::text('name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                            </div>

                            <div class="form-group">

                                {!! Form::label('email', 'Email Address', array('class' => 'sr-only')) !!}

                                {!! Form::text('email', null,
                                    array('required', 'class'=>'form-control', 'placeholder'=>'Email Address')) !!}
                            </div>

                            <div class="form-group">

                                {!! Form::label('phone', 'Telephone', array('class' => 'sr-only')) !!}

                                {!! Form::text('phone', null,
                                    array('required', 'class'=>'form-control', 'placeholder'=>'Telephone')) !!}
                            </div>

                            <div class="form-group">

                                {!! Form::label('postcode', 'Postcode', array('class' => 'sr-only')) !!}

                                {!! Form::text('postcode', null,
                                    array('class'=>'form-control', 'placeholder'=>'Postcode')) !!}
                            </div>

                            <div class="form-group">

                                {!! Form::label('enquiry', 'Enquiry', array('class' => 'sr-only')) !!}

                                {!! Form::textarea('enquiry', "Please contact me with information on a Roof System for a $manufacturer->manufacturerName $model->modelName"  ,
                                    array('required', 'class'=>'form-control', 'placeholder'=>'Enquiry')) !!}
                            </div>

                            <div class="form-group">

                                <button type="submit" class="btn btn-primary btn-primary-large ">Submit Enquiry </button>

                            </div>
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div> <!-- .col-md-9 -->

                <div class="col-md-3">
                    <div class="top-img-wrapper">
                        <img src="<?php echo $manufacturer->logoUrl; ?>" alt="<?php echo $manufacturer->manufacturerName; ?>" class="top-img"/>
                    </div> <!-- .wrapper -->

                </div> <!-- .col-md-6 -->

            </div> <!-- .row -->

        </div>
    @endif

@stop

@section('scripts')

    @if(empty($vehicles))
        <script type="text/javascript">

            $(document).ready(function(){
                $("form#contact_form").submit(function() {
                    $('button.btn').attr('disabled', 'disabled');
                    return true;
                });
            });

        </script>
    @endif

@stop
