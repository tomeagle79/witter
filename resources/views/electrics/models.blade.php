@extends('layouts.frontend')

@section('title', $manufacturer->manufacturerName .' Electrical Kits')

@section('heading')
	Electrical kits for {{ $manufacturer->manufacturerName }}
@stop

@section('meta_description')
	Buy an {{ $manufacturer->manufacturerName }} electrical kit direct from Witter Towbars. Simply search by your car's registration number and buy your {{ $manufacturer->manufacturerName }} electrical kit online.
@stop

@section('content')

<div class="container">
	<div class="row">

        <div class="col-md-6 col-md-push-3 towbars-listing">

            <h2>{{ $manufacturer->manufacturerName }}</h2>

            <div class="item-list">
                @include('partials.search-dropdowns-child', ['redirect_slug' => 'electrical-kits',
                    'form_button_text' => 'Find your kit',
                    'display_models' => true])
            </div> <!-- .item-list -->

            <div class="description">
                <p>{!! nl2br($manufacturer->manufacturerDescription) !!}</p>
            </div> <!-- .description -->
        </div> <!-- .col-md-6 -->

        <div class="col-md-3 col-md-pull-6">

            @include('partials.electrics.sidebar-search')

            @include('partials.sidebar-info-menu')

        </div> <!-- .col-md-3 -->

        <div class="col-md-3">

            @if( !empty($manufacturer->logoUrl) )
                <div class="top-img-wrapper">
                    <img src="{{ $manufacturer->logoUrl }}" alt="{{ $manufacturer->manufacturerName }}" class="top-img">
                    <p class="text-center">Images are representative only</p>
                </div> <!-- .wrapper -->
            @endif

        </div><!-- /.col-md-3 -->

    </div><!-- .row -->

    <div class="row">
        <div class="col-md-9 col-md-push-3 towbars-listing">
            <hr >
            @if(count($models) > 0)

                <h2 class="title-margin-bottom">Find an Electrical Kit for {{ $manufacturer->manufacturerName }} models</h2>

                <div class="row lazy-load-images item-list flex-items">
                    @foreach ($models as $key => $model)
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="item">
                                <div class="item-body">
                                    <a href="{{ route('electrical-kits.model_list.body_list', [$manufacturer->slug, $model->slug]) }}">
                                        <img src="{{ image_load($model->photoUrl) }}" title="Towbars for {{ $model->modelName }}" alt="Towbar Electrical Kits for {{ $model->modelName }}">
                                        <h3>{{ $manufacturer->manufacturerName }} {{ $model->modelName }} Towbar Electrical Kits</h3>
                                    </a>
                                </div><!-- /.item-body -->
                            </div> <!-- .model -->
                        </div>
                    @endforeach
                </div><!-- .row -->
            @endif

        </div><!-- /.col-md-6 -->

    </div><!-- /.row -->

</div><!-- .container -->
@stop
