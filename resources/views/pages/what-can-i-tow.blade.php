@extends('layouts.frontend')

@section('title', 'What can I tow?')

@section('meta_description')
    Need help choosing the right towbar for your vehicle? Read our easy to follow towbar guide. All you need to know about buying and fitting a towbar.
@stop

@section('body-class', 'complete-guide what-can-i-tow')

@section('heading')
    WHAT CAN I TOW?
@stop
 
@section('content')
    <a name="#top" id="top"></a>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <p>Making sure you can legally tow your caravan is vital. Failure to correctly match your car and caravan weights could lead to an expensive fine, points on your licence and could potentially endanger your life and the lives of others around you.</p>
                <p>To help you tow as safely as possible, we’ve put together a little advice on towing capacity. Covering everything you need to know about what you can tow, this guide includes practical advice and tips for finding your car’s towing capacity.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-sm-push-3 col-md-push-3 page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="small" id="what-can-i-tow-weight">What can my car tow (weight)?</h2>
                        <hr>
                        <p>Every car has a maximum towing capacity. The maximum towing capacity is set by the vehicle manufacturer and can be found by reading the vehicle identification plate (VIN plate).</p>
                        <p>Located under the car bonnet or on a door pillar, the VIN plate has four lines. The VIN plate will look like this:</p>
                        
                        <div class="vin-plate">
                            <div class="plate-row">
                                <p class="text">1. Gross Vehicle Weight</p>
                                <span class="value">3000kg</span>
                            </div>
                            <div class="plate-row">
                                <p class="text">2. Gross Train Weight</p>
                                <span class="value">5000kg</span>
                            </div>
                            <div class="plate-row">
                                <p class="text">3. Maximum Front Axle Load</p>
                                <span class="value">1500kg</span>
                            </div>
                            <div class="plate-row">
                                <p class="text">4. Maximum Rear Axle Load</p>
                                <span class="value">1600kg</span>
                            </div>
                        </div>

                        <p>1. The 1st line is the gross vehicle weight (GVW). This is the total maximum weight of the vehicle. Do not load the car over this weight.</p>
                        <p>2. The 2nd line is the gross train weight (GTW). This is the total maximum weight of the vehicle plus caravan plus load. Do not load the car and trailer/caravan over this weight.</p>
                        <p>3. The 3rd line is the maximum front axle load (MFAL). This is the maximum distributed weight that may be supported by the front axle of the vehicle.</p>
                        <p>4. The 4th line is the maximum rear axle load (MRAL). This is the maximum distributed weight that may be supported by the rear axle of the vehicle.</p>

                        <h3>Calculate your maximum towing weight</h3>
                        <p>To calculate the maximum weight your car can tow, all you need to do is subtract the gross vehicle weight (GVW) from the gross train weight (GTW).</p>
                        <img src="/assets/img/guides/towing/chart.png" class="chart">
                        <p>This is the absolute maximum weight your vehicle can tow. Towing above this weight will endanger your life and the lives of others around you and could lead to an expensive fine and points on your licence.</p>

                        <hr>
                        <h2 class="small" id="what-can-i-tow-size">What can my car tow (width &amp; length)?</h2>
                        <hr>

                        <p>While it’s common knowledge that all cars have a maximum towing capacity that needs to be legally adhered to, the lesser known fact about towing is that your vehicle needs to legally adhere to width and length capacities.</p>
                        <p>The two rules that apply to all cars regardless of power and size are:</p>
                        <p>-  The maximum towing width is 2.55 metres</p>
                        <p>-  The maximum towing length is 7 metres</p>
                        <p>You will need to access a vehicle manual and calculate the overall width and length of the car and caravan/trailer together. Alternatively, you can perform a double check using a tape measure.</p>
                        
                        <hr>
                        <h2 class="small" id="what-can-i-tow-match">What can my car tow (Tow match service)?</h2>
                        <hr>

                        <p>If you can’t find your VIN plate or you want to make doubly sure that your car and caravan/trailer is legally compliant, then you will need to use a dedicated tow match service.</p>
                        <p>With access to over 1 billion VIN plates, a tow match service can calculate your towing capacity in just a few clicks. </p>
                        <p>To help make it easy, the Witter team recently partnered with Towsafe to bring you cheap and easy access to the Towsafe match service.</p>
                        <p>Giving you access to the weight and towing capacity of your car, all you need to get started is your vehicle registration number.</p>
                        <a href="#what-can-i-tow-weight" class="btn btn-primary margin-b smoothscroll">Find my towing capacity</a>
                        <p>Remember, if your outfit is incorrectly matched you may be stopped, fined £1000 and have 3 points added to your licence. Be Safe. Be Legal.</p>
                        
                        <hr>
                        <h2 class="small" id="can-i-tow-caravan">Can I tow a caravan?</h2>
                        <hr>

                        <p>The legality surrounding towing a caravan is quite confusing, so it’s important that you read this section carefully. There are different rules depending on when you passed your driving test. These are:</p>
                        <h3>Licences issued from 1 January 1997</h3>
                        <p>If you passed your car driving test on or after 1 January 1997 you can:</p>
                        <p>-  Drive a car or van up to 3,500kg maximum authorised mass (MAM).</p>
                        <p>-  Tow a trailer over 750kg MAM as long as the combined weight (MAM) of the trailer and towing vehicle is no more than 3,500kg.</p>
                        <p>-  MAM stands for ‘maximum authorised mass’. This is the complete weight of the car, trailer (caravan) and the trailer contents combined.</p>
                        <p>In most cases, it is unlikely that your caravan and car’s maximum authorised mass is less than 3500kg. If this is the case, you will need to pass the car and trailer driving test before towing a caravan.</p>
                   
                        <h3>Licences issued before 1 January 1997</h3>
                        <p>If you passed your car test before 1 January 1997 you can:</p>
                        <p>-  Drive a vehicle and trailer combination of up to 8,250kg MAM. View your driving licence information to check.</p>
                        <p>-  Drive a minibus with a trailer over 750kg MAM.</p>
                        <p>If you want to drive anything heavier than 8,250kg, then you will need to complete the additional car and trailer driving test. </p>

                        <h3>So can I tow a caravan?</h3>
                        <p>-  If you passed your car driving test on or after 1 January 1997, and you want to tow a caravan over the 3,500kg maximum authorised mass, then you will need to take a car and trailer driving test.</p>
                        <p>-  If you passed your car test before 1 January 1997, you can tow a caravan up to an 8,250kg maximum authorised mass.</p>

                        <hr>
                        <h2 class="small" id="legality">What is the legality of towing a caravan?</h2>
                        <hr>

                        <p>With the stated licencing and weight rules of towing a caravan, you will also find that there are a variety of other laws that you must adhere to when towing a caravan. These include:</p>
                        <p>-  All towbars must meet type approved EU regulations.</p>
                        <p>-  Do not exceed the 50mph speed limit on a single carriageway.</p>
                        <p>-  Do not exceed the 60mph speed limit on dual carriageways.</p>
                        <p>-  Never carry passengers in a caravan while driving.</p>
                        <p>-  Your number plate and registration number must be clearly displayed at the rear of your caravan.</p>
                        <p>-  Your rear lights must be working and visible. Double check all your vehicle lights before driving.</p>
                        <p>-  Any caravan over 750kg must have a working brake system.</p>

                        <div class="ideal-towbar clearfix">
                            <p>Ready to find your ideal caravan mover?</p>
                            <a href="/will-my-caravan-mover-fit" class="btn btn-primary">Let's get started</a>
                        </div>
                        
                    </div>
                </div>    
            </div>
            
            <div class="col-xs-12 col-sm-3 col-md-3 col-sm-pull-9 col-md-pull-9 sidebar-complete-guide">
                <div class="sidebar">
                    <div class="sidebar-heading">This guide will look at:</div>
                    <ul>
                        <a class="smoothscroll" href="#what-can-i-tow-weight"><li>What can my car tow (weight)?</li></a>
                        <a class="smoothscroll" href="#what-can-i-tow-size"><li>What can my car tow (width & length)?</li></a>
                        <a class="smoothscroll" href="#what-can-i-tow-match"><li>What can my car tow (tow match service?)</li></a>
                        <a class="smoothscroll" href="#can-i-tow-caravan"><li>Can I tow a caravan?</li></a>
                        <a class="smoothscroll" href="#legality"><li>What is the legality of towing a caravan?</li></a>
                    </ul>
                </div>
            
                <a href="#top" class="btn btn-primary back-to-top hidden-xs smoothscroll" >Back to top</a>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script type="text/javascript">
		$(document).ready(function() {
            var $root = $('html, body');

            $('a.smoothscroll').click(function () {

                // Just open the panel if data-open-panel is set
                if( $( this )[0].hasAttribute( 'data-open-panel' ) ) {

                    $( '#' + $.attr( this, 'data-open-panel' ) ).collapse('show');
                }

                $root.animate({

                    scrollTop: $( $.attr(this, 'href') ).offset().top
                }, 500);

                return true;
            });
		});
	</script>
@stop
