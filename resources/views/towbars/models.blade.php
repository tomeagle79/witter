@extends('layouts.frontend')

@section('title', $manufacturer->manufacturerName .' Towbar Fitting | '. $manufacturer->manufacturerName .' Tow Bar')

@section('heading')
	Towbars for {{ $manufacturer->manufacturerName }}
@stop

@section('meta_description')
	Buy an {{ $manufacturer->manufacturerName }} towbar direct from Witter Towbars. Simply search by your car's registration number and book your {{ $manufacturer->manufacturerName }} towbar fitting online.
@stop

@section('content')

<div class="container">
	<div class="row">

        <div class="col-md-6 col-md-push-3 towbars-listing">

            <h2>{{ $manufacturer->manufacturerName }}</h2>

            <div class="item-list">
                @include('partials.search-dropdowns-child', ['redirect_slug' => 'towbars',
                    'form_button_text' => 'Find your towbar',
                    'display_models' => true])
            </div> <!-- .item-list -->

            @if(!empty($manufacturer->manufacturerDescription))
                <hr>

                <div class="description">
                    <p>{!! nl2br($manufacturer->manufacturerDescription) !!}</p>
                </div> <!-- .description -->
            @endif
		</div> <!-- .col-md-6 -->

        <div class="col-md-3 col-md-pull-6">

            @include('partials.sidebar-search')

            @include('partials.sidebar-info-menu')

        </div> <!-- .col-md-3 -->

        <div class="col-md-3">

            @if(!empty($manufacturer->logoUrl))
                <div class="top-img-wrapper">
                    <img src="{{ $manufacturer->logoUrl }}" alt="{{ $manufacturer->manufacturerName }}" class="top-img">
                    <p class="text-center">Images are representative only</p>
                </div> <!-- .wrapper -->
            @endif

        </div><!-- /.col-md-3 -->
	</div>
    <div class="row">
        <div class="col-md-9 col-md-push-3 towbars-listing">
            <hr >
            @if(count($models) > 0)

                <h2 class="title-margin-bottom">Find a Towbar for {{ $manufacturer->manufacturerName }} models</h2>

                <div class="row item-list flex-items">
                    @foreach ($models as $key => $model)
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="item">
                                <div class="item-body">
                                    <a href="{{ route('towbars.model_list.body_list', [$manufacturer->slug, $model->slug]) }}">
                                        <img src="{{ image_load($model->photoUrl) }}" title="{{ $model->modelName }}" alt="{{ $model->modelName }}">
                                        <h3>{{ $manufacturer->manufacturerName }} {{ $model->modelName }}</h3>
                                    </a>
                                </div><!-- /.item-body -->
                            </div> <!-- .model -->
                        </div>
                    @endforeach
                </div><!-- .row -->
            @endif

        </div><!-- /.col-md-6 -->

    </div><!-- /.row -->


</div>
@stop
