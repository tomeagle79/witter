<div class="manufacturer-dropdowns">

    <p>Please select your vehicle</p>

    @if($display_models)
    {!! Form::open(['route' => [$redirect_slug . '.model_list_submit', $manufacturer->slug], 'id' => 'search-dropdown-form', 'onsubmit' => "
        gtag('event', 'Success', {
            event_category: 'Model select',
            event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
        });
    "]) !!}
    @else
    {!! Form::open(['route' => [$redirect_slug . '.body_registration_submit', $manufacturer->slug, $model_slug], 'id' => 'search-dropdown-form', 'onsubmit' => "
        gtag('event', 'Success', {
            event_category: 'Model select',
            event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
        });
    "]) !!}
    @endif
        @if($display_models)
            <div class="form-group">
                <label for="model" class="sr-only">Choose Model</label>
                <div class="car-dropdowns-select">

                    <select name="model" id="model" class="" data-style="btn-select-box">
                        <option>CHOOSE MODEL</option>

                        @if(!empty($models_dropdown))
                            @foreach($models_dropdown as $mdrop)
                                <option value="{{ $mdrop->modelId }}" data-slug="{{ $mdrop->slug }}"
                                    @if(isset($vehicle_dropdown['model_id']) && $vehicle_dropdown['model_id'] == $mdrop->modelId)
                                        selected="selected"
                                        @php $model_slug = $mdrop->slug @endphp
                                    @endif >{{ $mdrop->modelName }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        @endif

        <div class="form-group">
            <label for="body" class="sr-only">Choose Body Type</label>
            <div class="car-dropdowns-select">
                <select name="body" id="body" class="" data-style="btn-select-box">
                    <option>CHOOSE BODY</option>
                    @if(!empty($bodies_dropdown))
                        @foreach($bodies_dropdown as $mdrop)
                            <option value="{{ $mdrop->bodyId }}" data-slug="{{ $mdrop->slug }}"
                                @if(isset($vehicle_dropdown['body_id']) && $vehicle_dropdown['body_id'] == $mdrop->bodyId)
                                    selected="selected"
                                    @php $body_slug = $mdrop->slug @endphp
                                @endif >{{ $mdrop->bodyName }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="registration" class="sr-only">Choose  </label>
            <div class="car-dropdowns-select">
                <select name="registration" id="registration" class="" data-style="btn-select-box">
                    <option>CHOOSE REGISTRATION</option>
                    @if(!empty($registrations_dropdown))
                        @foreach($registrations_dropdown as $rdrop)
                            <option value="{{ $rdrop->registrationId }}" data-slug="{{ $rdrop->registrationId }}"
                                @if(isset($vehicle_dropdown['registration_id']) && $vehicle_dropdown['registration_id'] == $rdrop->registrationId)
                                    selected="selected"
                                    @php $registration_slug = $rdrop->registrationId @endphp
                                @endif >{{ $rdrop->registrationText }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        {{ Form::hidden('manufacturerSlug', $manufacturer->slug, ['id' => 'manufacturerSlug']) }}
        @if($display_models)
            {{ Form::hidden('modelSlug', empty($model_slug) ? '' : $model_slug, ['id' => 'modelSlug']) }}
        @else
            {{ Form::hidden('modelSlug', $model->slug, ['id' => 'modelSlug']) }}
        @endif
        {{ Form::hidden('bodySlug', empty($body_slug) ? '' : $body_slug, ['id' => 'bodySlug']) }}
        {{ Form::hidden('registrationId', empty($registration_slug) ? '' : $registration_slug, ['id' => 'registrationId']) }}

        <div class="row">

            <div class="col-xs-12">
                <div id="dropdownError"></div><!-- /#dropdownError -->
            </div><!-- /.col-xs-12 -->
            <div class="col-xs-9 col-sm-6">
                <button type="submit" class="btn btn-primary">{{ $form_button_text }}</button>
            </div><!-- /.col-sm-6 -->
        </div><!-- /.row -->

    {!! Form::close() !!}
    <div class="car-dropdowns-loading"></div>

</div>

@section('scripts')
    @parent
    <script type="text/javascript">

        $(function() {

            var vehicle_url_call = "{{ URL('vehicles') }}";

            setUpVehicleDropdowns(vehicle_url_call);

            @if(empty($display_models))

                var validate = true;
            @else
                var validate = false;
            @endif

            // Send the starting slug to build the url and redirect the user.
            setUpVehicleRedirect("/{{ $redirect_slug }}/", validate);

        });

    </script>
@stop
