@extends('layouts.frontend')

@section('title')
	Error
@stop

@section('heading')
	Error
@stop

@section('meta_description')
	Error
@stop

@section('content')

	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<article>
					
					<p>
                        <strong>Sorry</strong>
                        it looks like something went wrong with that request.  The problem
                        has been automatically reported to our technical teams.
                    </p>

                    <p>
                        Please try again, and if the problem persists please contact us.
                    </p>
					
				</article>
			</div> <!-- col-md-12 -->

		</div> <!-- .row -->
	</div> <!-- .container -->
@stop