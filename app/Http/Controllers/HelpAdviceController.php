<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\HelpArticle;
use App\Models\HelpCategory;

class HelpAdviceController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Help &amp; Advice', route('help_articles'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['help_categories'] = HelpCategory::get();

        $faq_categories = FaqCategory::get()->toArray();

        $this->data['categories'] = $this->buildCategories($faq_categories);

        $this->data['faqs'] = Faq::published()->orderBy('created_at', 'desc')->paginate(config('settings.pagination.faqs'));

        $this->data['blog_posts'] = BlogPost::published()->orderBy('created_at', 'desc')->take(3)->get();

        return view('help_articles.index', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($url_slug, $id)
    {

        $this->data['help_categories'] = HelpCategory::get();
        $faq_categories = FaqCategory::get()->toArray();
        $this->data['categories'] = $this->buildCategories($faq_categories);

        $this->data['article'] = $article = HelpArticle::published()->find($id);

        if (empty($article)) {
            return redirect('help_articles');
        }

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb($article->title);

        // If we have a template for this content type then use it.
        if (!empty($article->template_name)) {

            return view('help_articles.' . $article->template_name, $this->data);
        }

        return view('help_articles.post', $this->data);
    }

    private function buildCategories($array, $parent_id = null)
    {
        $result = [];

        foreach ($array as $row) {
            if ($row['parent_id'] == $parent_id) {
                $children = $this->buildCategories($array, $row['id']);
                if ($children) {
                    $row['children'] = $children;
                }
                $result[] = $row;
            }
        }
        return $result;
    }
}
