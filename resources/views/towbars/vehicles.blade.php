@extends('layouts.frontend')

@section('title', $manufacturer->manufacturerName .' '. $model->modelName .' '. $body->bodyName .' towbars')

@section('heading')
	Towbars for {{ $manufacturer->manufacturerName }} {{ $model->modelName }} {{ $body->bodyName }}
@stop

@section('meta_description')Towbars for {{ $manufacturer->manufacturerName }} {{ $model->modelName }} {{ $body->bodyName }}, Please select your vehicle.@stop

@section('content')

    @if(!empty($vehicles))

    <div class="container">
    	<div class="row">
            <div class="col-md-9 col-md-push-3 towbars-listing">

                <h2>{{ $manufacturer->manufacturerName }} {{ $model->modelName }} {{ $body->bodyName }}</h2>
                <p class="select-vehicle">Please select your vehicle below</p>

                    <div class="row item-list flex-items">

                        @if(!empty($vehicles))
                            @foreach ($vehicles as $key => $vehicle)

                                <div class="col-sm-6 col-md-4">
                                    <div class="item">

                                        @if(!empty($vehicle->cannotTow))
                                            <div class="item-header">
                                                <a href="{{ route('towbars.by_selection', [$manufacturer->slug, $model->slug, $body->slug, $registration_id, $vehicle->slug]) }}">
                                                    Your vehicle does not have a towing capacity.
                                                    Find out more.
                                                </a>
                                            </div><!-- /.item-header -->
                                        @elseif(!empty($vehicle->towbarInDevelopment))
                                            <div class="item-header">
                                                <a href="{{ route('towbars.by_selection', [$manufacturer->slug, $model->slug, $body->slug, $registration_id, $vehicle->slug]) }}">
                                                    Towbar in Development. <br>
                                                    Contact us for more Details.
                                                </a>
                                            </div><!-- /.item-header -->

                                        @elseif(empty($vehicle->towbarsAvailable))
                                            <div class="item-header">
                                                <a href="{{ route('towbars.by_selection', [$manufacturer->slug, $model->slug, $body->slug, $registration_id, $vehicle->slug]) }}">
                                                    There is no towbar available. <br>
                                                    Contact us for more info.
                                                </a>
                                            </div><!-- /.item-header -->
                                        @endif

                                        <div class="item-body">
                                            @if(!empty($vehicle->vehiclePictureUrl))
                                                <a href="{{ route('towbars.by_selection', [$manufacturer->slug, $model->slug, $body->slug, $registration_id, $vehicle->slug]) }}">
                                                    <img src="{{ image_load($vehicle->vehiclePictureUrl) }}" alt="Towbars for {{ $vehicle->description }}">
                                                </a>
                                            @else

                                                <a href="{{ route('towbars.by_selection', [$manufacturer->slug, $model->slug, $body->slug, $registration_id, $vehicle->slug]) }}">
                                                    <img src="{{ image_load($manufacturer->logoUrl) }}" alt="Towbars for {{ $vehicle->description }}" class="manufacturer-logo" >
                                                </a>

                                            @endif
                                            <a href="{{ route('towbars.by_selection', [$manufacturer->slug, $model->slug, $body->slug, $registration_id, $vehicle->slug]) }}">
                                                <h3>
                                                    {{ $vehicle->description }}<br>
                                                    {{ $vehicle->period }}
                                                </h3>
                                            </a>
                                        </div><!-- /.item-body -->
                                    </div> <!-- .body -->
                                </div>

                            @endforeach
                        @endif
                    </div> <!-- .bodies -->

            </div>

            <div class="col-md-3 col-md-pull-9">
                @include('partials.sidebar-search')

                @include('partials.sidebar-info-menu')

            </div> <!-- .col-md-3 -->

    	</div>
    </div>
    @else

        <div class="container towbars-product-listing">
            <div class="row">

                <div class="col-md-9">

                    <h2 class="nomargin">{{ $manufacturer->manufacturerName }} {{ $model->modelName }} {{ $body->bodyName }} ( reg {{ $registration->registrationText }} ) </h2>

                    <div class="button-area">
                        <a href="{{ $back_link }}" class="btn btn-primary">Not your registration?</a>
                    </div>

                    <div class="row contact">

                        <div class="col-xs-12">
                            <p>Please get in contact with our Witter Towbar experts who will be able to advise you on the best Towbar for your {{ $manufacturer->manufacturerName }} {{ $model->modelName }} {{ $body->bodyName }} ( reg {{ $registration->registrationText }} ).</p>
                        </div>

                    <div class="col-md-4 get-in-touch">
                        <h2 class="border-bottom">Contact us</h2>
                        <p>
                            Call us on<br>
                            <a href="tel:+4401244284555"><span class="color-secondary">01244 284555</span></a><br><br>
                            Send us an email<br>
                            <span class="color-secondary"><a href="mailto:ecommerce@witter-towbars.co.uk" class="email-font">ecommerce@witter-towbars.co.uk</a></span>
                        </p>
                    </div>

                    <div class="col-md-7 get-in-touch">
                        <h2 class="border-bottom">Email us</h2>

                        {!! Form::open(array('route' => 'contact.submit_product_enquiry', 'class' => 'form', 'id' => 'contact_form')) !!}

                            <input type="hidden" name="enquiry_regarding" value="Towbar for {{ $manufacturer->manufacturerName }} {{ $model->modelName }} {{ $body->bodyName }} ( reg {{ $registration->registrationText }} ) ">
                            <input type="hidden" name="website_url" value="{{ url()->current() }}">

                            <div class="form-group">

                                {!! Form::label('name', 'Name', array('class' => 'sr-only')) !!}

                                {!! Form::text('name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                            </div>

                            <div class="form-group">

                                {!! Form::label('email', 'Email Address', array('class' => 'sr-only')) !!}

                                {!! Form::text('email', null,
                                    array('required', 'class'=>'form-control', 'placeholder'=>'Email Address')) !!}
                            </div>

                            <div class="form-group">

                                {!! Form::label('phone', 'Telephone', array('class' => 'sr-only')) !!}

                                {!! Form::text('phone', null,
                                    array('required', 'class'=>'form-control', 'placeholder'=>'Telephone')) !!}
                            </div>

                            <div class="form-group">

                                {!! Form::label('postcode', 'Postcode', array('class' => 'sr-only')) !!}

                                {!! Form::text('postcode', null,
                                    array('class'=>'form-control', 'placeholder'=>'Postcode')) !!}
                            </div>

                            <div class="form-group">

                                {!! Form::label('enquiry', 'Enquiry', array('class' => 'sr-only')) !!}

                                {!! Form::textarea('enquiry', "Please contact me with information on a Towbar for a $manufacturer->manufacturerName $model->modelName $body->bodyName ( reg $registration->registrationText ) "  ,
                                    array('required', 'class'=>'form-control', 'placeholder'=>'Enquiry')) !!}
                            </div>

                            <div class="form-group">

                                <button type="submit" class="btn btn-primary btn-primary-large ">Submit Enquiry </button>

                            </div>
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div> <!-- .col-md-9 -->

                <div class="col-md-3">
                    <div class="top-img-wrapper">
                        <img src="<?php echo $manufacturer->logoUrl; ?>" alt="<?php echo $manufacturer->manufacturerName; ?>" class="top-img"/>
                    </div> <!-- .wrapper -->

                </div> <!-- .col-md-6 -->

            </div> <!-- .row -->

        </div>
    </div>
    @endif


@stop

@section('scripts')

    @if(empty($vehicles))
        <script type="text/javascript">

            $(document).ready(function(){
                $("form#contact_form").submit(function() {
                    $('button.btn').attr('disabled', 'disabled');
                    return true;
                });
            });

        </script>
    @endif

@stop

