@extends('layouts.frontend')

@section('title', 'Land Rover Towing Guide')

@section('meta_description')
    Land Rover was originally an all-purpose off-road vehicle and is now commonly known as a luxury car brand. Land Rover produces 4 wheel drive cars, which are commonly used as ...
@stop

@section('body-class', 'complete-guide land-rover-towing-guide')

@section('heading')
    Land Rover Towing Guide
@stop

@section('content')
    <a name="#top" id="top"></a>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <p><strong>Land Rover was originally an all-purpose off-road vehicle and is now commonly known as a luxury car brand. Land Rover produces 4 wheel drive cars, which are commonly used as military vehicles, so they are robust and great on off-road terrain which makes them strong towing cars.</strong></p>
                <p>Often named as some of the best towing cars, Land Rovers are ideal for towing boats, trailers, caravans and horse boxes. As Land Rover has developed over the years, it now has a sophisticated infotainment system, Advanced Tow Assist and a 360o camera which makes towing even easier.</p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <img src="img/complete-guide/Land-rover-mountains.jpg" alt="Land Rover Towbars" >
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 sidebar-complete-guide">
                <div class="sidebar">
                    <div class="sidebar-heading">This guide will look at:</div>
                    <ul>
                        <a class="smoothscroll" href="#how-much-weight-land"><li>How much weight can a Land Rover tow?</li></a>
                        <a class="smoothscroll" href="#how-much-weight-range"><li>How much weight can Range Rover tow?</li></a>
                        <a class="smoothscroll" href="#preparing"><li>Preparing your Land Rover for towing.</li></a>
                        <a class="smoothscroll" href="#towing-tips"><li>Towing top tips.</li></a>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-sm-9 col-md-9 page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="small" id="how-much-weight-land">How much weight can a Land Rover tow?</h2>
                        <hr>
                        <p>No matter what car you own, the legal weight that you are allowed to tow will depend on when you passed your driving test. You also need to be aware of the gross train weight, which is the combined weight of the vehicle, trailer and load, and you cannot exceed this.</p>
                        <p>All Land Rovers are ideal for towing and we have listed the maximum towing weight for each Land Rover and Range Rover model. However, the maximum towing weight for each model will also vary, depending on the following factors:</p>

                        <ul>
                            <li><strong>Model and year the car was made</strong></li>
                            <li><strong>Hybrid or PHEV</strong></li>
                            <li><strong>Engine size</strong></li>
                            <li><strong>Petrol or diesel</strong></li>
                            <li><strong>Wheelbase</strong></li>
                            <li><strong>Manual or automatic</strong></li>
                            <li><strong>Two-wheel drive or four-wheel drive</strong></li>
                            <li><strong>Convertible</strong></li>
                            <li><strong>Special Vehicle Operations</strong></li>
                        </ul>

                        <p>If you have not yet bought your Land Rover or you already own one and have not towed with it before, you should check the exact towing capacity for your Land Rover’s specifications. As rough guidance, the maximum towing capacity for each model is listed below.</p>

                        <h3>Towing Capacities</h3>

                        <div class="row capacities">
                            <div class="col-xs-6">
                                <div class="capacity model">
                                    <p>Land Rover<br>Discovery</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="capacity weight">
                                    <p>3,500kg</p>
                                </div>
                            </div>
                        </div>

                        <div class="row capacities">
                            <div class="col-xs-6">
                                <div class="capacity model">
                                    <p>Land Rover<br>Freelander</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="capacity weight">
                                    <p>2,000kg</p>
                                </div>
                            </div>
                        </div>

                        <div class="row capacities">
                            <div class="col-xs-6">
                                <div class="capacity model">
                                    <p>Land Rover<br>Defender</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="capacity weight">
                                    <p>3,500kg</p>
                                </div>
                            </div>
                        </div>

                        <div class="row capacities">
                            <div class="col-xs-6">
                                <div class="capacity model">
                                    <p>Land Rover<br>Discovery S</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="capacity weight">
                                    <p>3,500kg</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-6">
                                    <a href="/towbars/land-rover-range-rover" class="btn btn-primary btn-tertiary btn-block ">Find Your Land Rover Towbar</a>
                            </div><!-- /.col-xs-12 -->
                        </div><!-- /.row -->

                        <hr>
                        <h2 class="small" id="how-much-weight-range">How much weight can a Range Rover tow?</h2>
                        <hr>

                        <p>Just like Land Rover models, the same rules apply when towing with a Range Rover. The maximum weight you can tow will depend on a number of factors but the maximum towing capacity for each model can be found below.</p>

                        <h3>Towing Capacities</h3>

                        <div class="row capacities">
                            <div class="col-xs-6">
                                <div class="capacity model">
                                    <p>Range<br>Rover</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="capacity weight">
                                    <p>3,500kg</p>
                                </div>
                            </div>
                        </div>
                        <div class="row capacities">
                            <div class="col-xs-6">
                                <div class="capacity model">
                                    <p>Range Rover<br>Evoque</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="capacity weight">
                                    <p>2,000kg</p>
                                </div>
                            </div>
                        </div>

                        <div class="row capacities">
                            <div class="col-xs-6">
                                <div class="capacity model">
                                    <p>Range Rover<br>Sport</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="capacity weight">
                                    <p>3,500kg</p>
                                </div>
                            </div>
                        </div>

                        <div class="row capacities">
                            <div class="col-xs-6">
                                <div class="capacity model">
                                    <p>Range Rover<br>Velar</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="capacity weight">
                                    <p>2,400kg</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-6">
                                    <a href="/towbars/land-rover-range-rover" class="btn btn-primary btn-tertiary btn-block ">Find Your Range Rover Towbar</a>
                            </div><!-- /.col-xs-12 -->
                        </div><!-- /.row -->

                        <hr>
                        <h2 class="small" id="preparing">Preparing your Land Rover for towing</h2>
                        <hr>

                        <p>As Land Rover has become better known as a premium brand, they have developed sophisticated computer systems to help make towing even easier.</p>
                        <p>When you have your towbar fitted, this should be done by a professional fitter. We would always recommend that you have vehicle-specific electrics installed alongside your towbar. This way, your trailer, caravan or horsebox can sync with your Land Rover’s enhanced technical features which can help with towing.</p>
                        <p>Some of Land Rover’s advanced features include:</p>

                        <hr>

                        <h3>Hitch Assist</h3>
                        <p>Hitch assist helps you to hitch the trailer onto your towbar. Use the camera to select Hitch Assist and get the towbar as close to the trailer as possible to hitch it up with ease.</p>
                        <p>Although Tow Assist makes it easier to hitch your trailer or caravan, it is your responsibility to make sure it is connected properly and it doesn’t exceed the gross train weight.</p>

                        <hr>

                        <h3></h3>
                        <p>Fit a tracking target sticker to whatever you are towing and follow the configuration settings on the touch screen in your Land Rover. This is activated with your rearview camera and when your car detects the electrical plug has been correctly attached to the towing socket. Follow the setup instructions on the touch screen to use advanced towing features.</p>

                        <p>You will need the following information:</p>
                        <p><strong>Trailer dimensions</strong></p>
                        <p><strong>Number of axels</strong></p>
                        <p><strong>Camera preference</strong></p>

                        <hr>

                        <h3>Advanced Tow Assist</h3>
                        <p>If you activate Advanced Tow Assist, you can use steering control and terrain response which is useful when driving off-road.</p>

                        <hr>

                        <h3>Trailer Stability Assist</h3>
                        <p>Trailer stability assist can detect when your trailer is starting to sway. It gradually reduces the speed of your car by applying the breaks and reducing the engine power to regain stability.</p>

                        <hr>

                        <h3>Trailer stability assist can detect when your trailer is starting to sway. It gradually reduces the speed of your car by applying the breaks and reducing the engine power to regain stability.</h3>
                        <p>Measure the weight of your load before you start to drive away. This is a great feature if you think you might be close to the maximum capacity.</p>

                        <hr>

                        <p>Not all of these towing features come as standard on your Land Rover, but if you will be towing frequently across tough terrain, it may be worth having them installed. Even if you don’t choose advanced towing features, the Land Rover is still a sensible match for towing a caravan, trailer or horse box.</p>

                        <hr>
                        <h2 class="small" id="towing-tips">Towing Tips</h2>
                        <hr>

                        <p>Regardless of what vehicle you are towing with, you should bear these tips in mind when you are towing:</p>

                        <ul>
                            <li><strong>Maximum speed limit while towing is 60mph on the motorway and 50mph on A roads</strong></li>
                            <li><strong>Maximum towing capacity for an unbraked trailer is 750kg</strong></li>
                            <li><strong>Maximum towing capacity for a braked trailer is 3500kg, but this depends on your vehicle and when you passed your driving test.</strong></li>
                            <li><strong>Gross train weight is the weight of the vehicle, trailer and load combined</strong></li>
                            <li><strong>You may need to take additional tests to tow a trailer or caravan</strong></li>
                        </ul>

                        <p>By now, you’ve probably come to the conclusion that Land Rovers make great towing cars. To find your ideal towbar, use our simple towbar finder tool and select the towbar you want, your towbar electrics kit and have your towbar installed by one of our approved Witter Fitters.</p>

                        <div class="ideal-towbar ideal-towbar--blue clearfix">
                            <p>Ready to find your ideal Towbar?</p>
                            <a href="/towbars/land-rover-range-rover" class="btn btn-primary btn-tertiary">Find Your <span class="hidden-xs">Land or Range Rover</span> Towbar</a>
                        </div><!-- /.ideal-towbar -->

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <a href="#top" class="btn btn-primary back-to-top smoothscroll" >Back to top</a>
                            </div><!-- /.col-xs-12 -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
		$(document).ready(function() {
            var $root = $('html, body');

            $('a.smoothscroll').click(function () {

                // Just open the panel if data-open-panel is set
                if( $( this )[0].hasAttribute( 'data-open-panel' ) ) {

                    $( '#' + $.attr( this, 'data-open-panel' ) ).collapse('show');
                }

                $root.animate({

                    scrollTop: $( $.attr(this, 'href') ).offset().top
                }, 500);

                return true;
            });
		});
	</script>
@stop
