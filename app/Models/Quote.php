<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quote_ref', 'quote_link_ref', 'quote_data', 'customer_data', 'registration_no'
    ];
}
