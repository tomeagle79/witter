<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Api\PartClass;
use App\Models\Api\Accessory;

class CaravanMoversController extends FrontendController
{
    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Caravan Movers', route('caravan_movers'));
    }

    /**
     * Display a listing of all accessory categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['accessories'] = Accessory::get_accessories_by_slug('caravan-movers', 'caravan-movers');

        return view('caravan-movers.index', $this->data);
    }


    /**
     * Display the accessory.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($accessory_slug)
    {
        $this->data['accessory'] = Accessory::get_single_by_slug($accessory_slug);
        $this->data['category'] = PartClass::get_partclass_by_id($this->data['accessory']->partClassId);
        $this->data['subcategories'] = PartClass::get_categories_by_partclass_id($this->data['accessory']->partClassId);
        $this->data['subcategory'] = PartClass::get_category_by_category_id($this->data['accessory']->categoryId);

        $this->data['breadcrumbs']
            ->addCrumb(
                $this->data['accessory']->title,
                route('accessories.multi', [
                    encode_url($this->data['accessory']->partNo)
                ])
            );

        // If the accessory is "fittable" then we need to check if an appointment already exists.
        if ($this->data['accessory']->isFittable) {
            // Check if we have 'accessory-fitted' items in the cart
            $cart = build_cart();

            if (isset($cart['contents'])) {
                foreach ($cart['contents'] as $item) {
                    // If we do have an 'accessory-fitted' item then add it to the 'appointment-exists' value.
                    if ($item['type'] == 'accessory-fitted') {
                        $this->data['appointment_exists'] = $item;
                    }
                }
            }
        }

        // Create a cleaner version of the details and moreDetails text.
        $details = trim(str_replace("\n", "<br>", $this->data['accessory']->details));
        $more_details = trim(str_replace("\n", "<br>", $this->data['accessory']->moreDetails));

        $this->data['accessory']->formatted_details = $details;
        $this->data['accessory']->formatted_more_details = $more_details;

        return view('caravan-movers.view', $this->data);
    }
}
