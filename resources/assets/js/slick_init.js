
$(window).on('load resize orientationchange', function () {
  $('.slick-carousel').each(function () {
    var $carousel = $(this);
    /* Initializes a slick carousel only on mobile screens */
    // slick on mobile
    if ($(window).width() > 768) {
      if ($carousel.hasClass('slick-initialized')) {
        $carousel.slick('unslick');
      }
    }
    else {
      if (!$carousel.hasClass('slick-initialized')) {
        $carousel.slick({
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          slidesToShow: 1,
          mobileFirst: true,
          arrows: true,
          dots: true,
          centerMode: true,
          centerPadding: '45px',
          prevArrow: '<span class="slick-prev slick-arrow"></span>',
          nextArrow: '<span class="slick-next slick-arrow"></span>',
        });
      }
    }
  });
  var slickImageHeight = $('#prod_results .slick-current .image-link').outerHeight();
  $('.slick-arrow').height(slickImageHeight);
});

