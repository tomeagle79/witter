@extends('layouts.frontend', ['heading_h2' => true])

@section('title')
	{!! !empty($article->meta_title) ? $article->meta_title : $article->title !!}
@stop

@section('heading')
	HELP &amp; ADVICE
@stop


@section('meta_description')
	{!! $article->meta_description !!}
@stop

@section('content')

	<div class="container">
		<div class="row">

			<div class="col-sm-8 col-md-9">
				<article>

					@if(!is_null($article->image))

						<img src="{{URL('imagecache', ['template' => 'original', 'filename' => $article->image->filename])}}" alt="{!! $article->title !!}" />

					@endif

					<h1>{!! $article->title !!}</h1>

					<div class="blog-copy">
						{!! $article->content !!}
					</div>

				</article>
			</div> <!-- col-md-9 -->

			<div class="col-sm-4 col-md-3">

				@include('partials.sidebar-search')

				@include('partials.sidebar-findlink-secondary')

			</div> <!-- .col-md-3 -->

		</div> <!-- .row -->
	</div> <!-- .container -->
@stop
