<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\TowbarRegistrationRequest;
use App\Http\Requests\CycleCarrierRegistrationRequest;
use App\Models\Api\ProductRegistration;

use Mail;

class ProductRegistrationController extends FrontendController
{
    /**
     * Product Registration options
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['breadcrumbs']->addCrumb('Product Registration');
        return view('product_registration.index', $this->data);
    }

    /**
     * Product Registration for Towbar
     *
     * @return \Illuminate\Http\Response
     */
    public function towbar()
    {
        $this->data['breadcrumbs']->addCrumb('Product Registration')->addCrumb('Towbar');
        return view('product_registration.towbar', $this->data);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function towbar_process(TowbarRegistrationRequest $request)
    {

        $registration = ProductRegistration::towbar([

            "CustomerFirstName" => $request->first_name,
            "CustomerSurname" => $request->surname,
            "CustomerCompanyName" => $request->company_name,
            "CustomerPostcode" => $request->postcode,
            "CustomerAddressLine1" => $request->address1,
            "CustomerAddressLine2" => $request->address2,
            "CustomerAddressLine3" => $request->address3,
            "CustomerTownCity" => $request->towncity,
            "CustomerTelephone" => $request->telephone,
            "CustomerFax" => $request->fax,
            "CustomerEmailAddress" => $request->email,
            "FitDateDay" => $request->date_fitted_dd,
            "FitDateMonth" => $request->date_fitted_mm,
            "FitDateYear" => $request->date_fitted_yyyy,
            "FitterCompanyName" => $request->supplier_name,
            "FitterPostcode" => $request->supplier_postcode,
            "FitterAddress1" => $request->supplier_address1,
            "FitterAddress2" => $request->supplier_address2,
            "FitterAddress3" => $request->supplier_address3,
            "FitterTownCity" => $request->supplier_towncity,
            "FitterTelephone" => $request->supplier_telephone,
            "VehicleRegistration" => $request->vehicle_reg,
            "TowbarSerialNo" => $request->towbar_serial_no,
            "TowbarPartNo" => $request->towbar_part_no,
            "NeckSerialNo" => $request->neck_serial_no
        ]);

        if($registration->success == true) {
            Mail::send('emails.towbar_registration', (array)$request->all(), function($message)
            {
                $message->from(config('witter.noreply_email'));
                $message->to(config('witter.towbar_registration_email'), 'Admin')->subject('Witter Towbar Registration');
            });

            return redirect(route('product_registration.towbar'))->with('success', 'Thank you.  We will be in touch about your registration.');
        }

        return redirect()->back()->withErrors(['Sorry we have not been able to process your request. Please check your entries and try again. '])->withInput();
    }

    /**
     * Product Registration for Towbar
     *
     * @return \Illuminate\Http\Response
     */
    public function cycle_carrier()
    {
        $this->data['breadcrumbs']->addCrumb('Product Registration')->addCrumb('Cycle Carrier');
        return view('product_registration.cycle_carrier', $this->data);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function cycle_carrier_process(CycleCarrierRegistrationRequest $request)
    {

        $registration = ProductRegistration::cycle_carrier([

            "CustomerFirstName" => $request->first_name,
            "CustomerSurname" => $request->surname,
            "CustomerCompanyName" => $request->company_name,
            "CustomerPostcode" => $request->postcode,
            "CustomerAddressLine1" => $request->address1,
            "CustomerAddressLine2" => $request->address2,
            "CustomerAddressLine3" => $request->address3,
            "CustomerTownCity" => $request->towncity,
            "CustomerTelephone" => $request->telephone,
            "CustomerFax" => $request->fax,
            "CustomerEmailAddress" => $request->email,
            "FitDateDay" => $request->date_fitted_dd,
            "FitDateMonth" => $request->date_fitted_mm,
            "FitDateYear" => $request->date_fitted_yyyy,
            "VehicleRegistration" => $request->vehicle_reg,
            "SerialNo" => $request->cycle_carrier_serial_no,
            "PartNo" => $request->cycle_carrier_part_no
        ]);

        if($registration->success == true) {

            Mail::send('emails.cycle_carrier_registration', (array)$request->all(), function($message)
            {
                $message->from(config('witter.noreply_email'));
                $message->to(config('witter.cycle_carrier_registration_email'), 'Admin')->subject('Witter Cycle Carrier Registration');
            });

            return redirect(route('product_registration.cycle_carrier'))->with('success', 'Thank you.  We will be in touch about your registration.');
        }

        return redirect()->back()->withErrors(['Sorry we have not been able to process your request. Please check your entries and try again. '])->withInput();
    }
}
