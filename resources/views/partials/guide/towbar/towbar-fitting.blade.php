<div class="row">
    <div class="col-xs-12">
        <p>Once you've figured out which towbar and which electric kit you want from the selection available for your vehicle, you're ready to purchase a towbar. Our website offers you a simple process for getting your order started with the towbar and fitting all-in-one package. Find your towbar with our number plate tool or select from our <a href="/towbars">drop-down menu here.</a></p>

        <h2 class="small" id="cost-to-fit-towbar">How much does it cost to fit a towbar?</h2>
        <p>The great thing about buying a towbar with Witter is our huge network of approved fitters. With over 60 years of experience going into engineering our towbars, we only work with the very best to fit them. That’s why we include the cost of fitting a towbar within the price of buying the actual towbar. Not only does that ensure you have a legally fitted towbar, but it also allows you to relax knowing that your towbar is fitted by an experienced, verified professional.</p>

        <h2 class="small" id="fit-a-towbar-myself">Can I fit a towbar myself?</h2>
        <p>Of course, you could fit a towbar yourself. But why would you? Towing is a serious business that, when done incorrectly, can endanger your life and the lives of others around you. That’s why we offer an all-in-one fitting service. We have spent many years designing quality, safe towbars. We want to ensure the people who buy our towbars use them in the safest manner possible. That’s why we encourage you to buy a towbar from us and enjoy peace of mind with our free fitting service carried out by experienced professionals.</p>

        <h2 class="small" id="towbar-affect-insurance">Can fitting a towbar affect my insurance?</h2>
        <p>In insurance terms, <a href="/service-centres">fitting a towbar</a> is classified as a car modification. Therefore, it is imperative that you notify your insurance provider about this change to your vehicle. Car modifications can affect how your insurance premium is calculated and, if you don’t tell your insurance provider, they may use the modification to invalidate your policy in the event of an insurance claim. Always notify your insurance provider when fitting a towbar.</p>
    </div>
</div>
