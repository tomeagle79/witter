
<div class="sidebar-menu text-left">

    <ul>
    @foreach($partClasses as $class)
    @if($class->partClassId !== 25)
			<li>
				<a href="{!! route('accessories.multi', $class->slug) !!}" @if(!empty($class) && !empty($category) && $class->partClassId == $category->partClassId) class="active" @endif >{!! $class->title !!}</a>

				@if(!empty($subcategories))
					@if($class->partClassId == $subcategories[0]->partClassId)
						<ul>
							@foreach($subcategories as $subcat)
								<li>
									<a href="{!! route('accessories.subcategory', [$class->slug, $subcat->slug]) !!}" @if(!empty($subcategory) && $subcategory->categoryId == $subcat->categoryId) class="active" @endif >{!! $subcat->categoryName !!}</a>
								</li>
							@endforeach
						</ul>
					@endif
				@endif
      </li>
      @endif
		@endforeach
	</ul>

</div>
