<?php
namespace App\Facades;

class Request extends \Illuminate\Support\Facades\Request {
    public static function isadmin($path) {
        return self::is(config('settings.admin.slug').'/'.$path);
    }
    public static function islogin() {
        return self::is(config('settings.admin.loginurl'));
    }
}
