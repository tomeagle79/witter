@extends('layouts.backend')

@section('title')
	Create FAQ  
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/faqs/partials/_form', ['submit_text' => 'Create FAQ', 'type' => 'add'])
	</form>

@stop

