@extends('layouts.frontend')

@section('title', 'Checkout - Step 1, Your Details')

@section('body-class', 'body_bg_checkout')

@section('heading')
	Checkout
@stop

@section('meta_description')
	Checkout
@stop

@section('content')

<div class="container checkout-process">
    {!! Form::open(['route' => 'checkout.section1_submit', 'class' => 'form-horizontal', 'id' => 'checkout-form']) !!}
    <div class="row">
        <div class="col-xs-12">
            <h1>Your Details</h1>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->

    <div class="row">

        <div class="col-xs-12  col-md-9">

            <div class="section">
                    <h2 class="border_bottom">Personal Details</h2>
                    <div class="form-group has-feedback">
                        <label for="title" class="col-xs-12">Title *</label>
                        <div class="col-sm-6 col-xs-12">
                            <div class="dropdown">
                                {!! Form::select('title',
                                array_merge([null => 'Title*'], array_merge(['' => 'Title *'],array_combine(config('witter.salutations'), config('witter.salutations')))),
                                (empty($checkout['section1']['title']) ? null : $checkout['section1']['title']),
                                ['required', 'data-required-error'=>'Please select your title.', 'class' => 'form-control']) !!}
                            </div><!-- /.dropdown -->
                            <span class="form-control-feedback form-control-feedback-dropdown" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="first_name" class="col-xs-12">First Name *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('first_name',
                                (empty($checkout['section1']['first_name']) ? null : $checkout['section1']['first_name']),
                                ['required', 'data-required-error'=>'Please enter your first name.', 'class'=>'form-control', 'placeholder' => 'First Name *']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="surname" class="col-xs-12">Surname *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('surname',
                                (empty($checkout['section1']['surname']) ? null : $checkout['section1']['surname']),
                                ['required', 'data-required-error'=>'Please enter your surname.', 'class'=>'form-control', 'placeholder' => 'Surname *']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="telephone" class="col-xs-12">Telephone *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('telephone',
                                (empty($checkout['section1']['telephone']) ? null : $checkout['section1']['telephone']),
                                ['required', 'data-required-error'=>'Please enter your telephone number.',  'class'=>'form-control', 'placeholder' => 'Telephone *']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="mobile" class="col-xs-12">Mobile *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('mobile',
                                (empty($checkout['section1']['mobile']) ? null : $checkout['section1']['mobile']),
                                ['required', 'data-required-error'=>'Please enter your mobile number.', 'class'=>'form-control', 'placeholder' => 'Mobile *']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="email" class="col-xs-12">Email *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::email('email',
                                (empty($checkout['section1']['email']) ? null : $checkout['section1']['email']),
                                ['required', 'data-required-error'=>'Please enter your email address.', 'class'=>'form-control', 'placeholder' => 'Email *']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <h2 class="border_bottom">Billing Address</h2>

                    <div class="form-group has-feedback">
                        <label for="billing_postcode" class="col-xs-12">Postcode *</label>
                        <div class="col-sm-6 col-xs-12">
                            <div class="input-group">
                                {!! Form::text('billing_postcode',
                                    (empty($checkout['section1']['billing_postcode']) ? null : $checkout['section1']['billing_postcode']),
                                    ['required', 'data-required-error'=>'Please enter your billing address postcode.', 'class'=>'form-control', 'placeholder' => 'Postcode *']) !!}
                                <span class="input-group-btn">
                                    <button class="btn btn-default " id="address-lookup" type="button">Address Lookup</button>
                                </span>
                            </div> <!-- .input-group -->

                            <div class="alert alert-help alert-help-warning" id="postcode-alert" style="display:none;"><p></p></div>
                            <div class="alert alert-help alert-help-info" id="loading" style="display:none;"><p>Loading...</p></div>

                            <div class="address-lookup-wrapper">
                                <div class="dropdown">
                                    {!! Form::select('address_dropdown',
                                        [],
                                        null,
                                        ['class' => 'form-control', 'id' => 'address_dropdown']) !!}
                                </div><!-- /.dropdown -->
                            </div><!-- /.address-lookup-wrapper -->
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div>

                    <div class="form-group has-feedback">
                        <label for="billing_address1" class="col-xs-12">Address 1 *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('billing_address1',
                                (empty($checkout['section1']['billing_address1']) ? null : $checkout['section1']['billing_address1']),
                                ['required', 'data-required-error'=>'Please enter the first line of your billing address.', 'id'=>'billing_address1', 'class'=>'form-control', 'placeholder' => 'Address 1 *']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="billing_address2" class="col-xs-12">Address 2</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('billing_address2',
                                (empty($checkout['section1']['billing_address2']) ? null : $checkout['section1']['billing_address2']),
                                ['class'=>'form-control', 'id'=>'billing_address2', 'placeholder' => 'Address 2']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="billing_towncity" class="col-xs-12">Town / City*</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('billing_towncity',
                                (empty($checkout['section1']['billing_towncity']) ? null : $checkout['section1']['billing_towncity']),
                                ['required', 'data-required-error'=>'Please enter your billing address town or city.',  'id'=>'billing_towncity', 'class'=>'form-control', 'placeholder' => 'Town / City *']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="billing_county" class="col-xs-12">County *</label>
                        <div class="col-sm-6 col-xs-12">
                            {!! Form::text('billing_county',
                                (empty($checkout['section1']['billing_county']) ? null : $checkout['section1']['billing_county']),
                                ['required', 'data-required-error'=>'Please enter your billing address county.', 'id'=>'billing_county', 'class'=>'form-control', 'data-error'=>'Please enter a county for your billing address', 'placeholder' => 'County *']) !!}
                            <span class="form-control-feedback" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    <div class="form-group has-feedback">
                        <label for="billing_country" class="col-xs-12">Country *</label>
                        <div class="col-sm-6 col-xs-12">
                                <div class="dropdown">
                                    {!! Form::select('billing_country', array_merge(['' => 'Country *'],
                                        array_combine(config('witter.countries'), config('witter.countries'))),
                                        (empty($checkout['section1']['billing_country']) ? null : $checkout['section1']['billing_country']),
                                        ['required', 'data-required-error'=>'Please select your billing address country.',  'id'=>'billing_country', 'class'=>'form-control']) !!}
                                </div><!-- /.dropdown -->
                            <span class="form-control-feedback form-control-feedback-dropdown" aria-hidden="true"></span>
                        </div> <!-- .col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.form-group -->

                    @if(!empty($cart['regAndColourRequired']))

                        <div class="col-xs-12 checkbox-card">
                                <h2 class="border_bottom">Vehicle Details</h2>

                                @foreach($cart['contents'] as $item)

                                    @if($item['details']->regAndColourRequired == true)

                                        <input type="hidden" name="{{ 'vehicle[' . $item['cart_item_id'] .  '][partNo]' }}" value="{{ $item['partNo'] }}">

                                        <p><strong>{{ $item['details']->title }}</strong> will be fitted to the following vehicle</p>

                                        <div class="form-group has-feedback">
                                            <label for="email" class="col-xs-12">Registration number *</label>
                                            <div class="col-sm-6 col-xs-12">
                                                {!! Form::text(("vehicle[" . $item['cart_item_id'] . "][registration_no]"),
                                                    (empty($checkout['sectionVehicle'][$item['cart_item_id']]['registration_no']) ? null : $checkout['sectionVehicle'][$item['cart_item_id']]['registration_no']),
                                                    ['required', 'data-required-error'=>'Please eneter your vehicle\'s registration number.', 'class'=>'form-control']) !!}
                                                <span class="form-control-feedback" aria-hidden="true"></span>
                                            </div> <!-- .col -->
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div><!-- /.col-md-6 -->
                                        </div><!-- /.form-group -->

                                        <div class="form-group has-feedback">
                                            <label for="email" class="col-xs-12">Vehicle colour *</label>
                                            <div class="col-sm-6 col-xs-12">
                                                {!! Form::text( 'vehicle[' . $item['cart_item_id'] .  '][vehicle_colour]',
                                                    (empty($checkout['sectionVehicle'][$item['cart_item_id']]['vehicle_colour']) ? null : $checkout['sectionVehicle'][$item['cart_item_id']]['vehicle_colour']),
                                                    ['required', 'data-required-error'=>'Please eneter your vehicle\'s colour.',  'class'=>'form-control']) !!}
                                                <span class="form-control-feedback" aria-hidden="true"></span>
                                            </div> <!-- .col -->
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div><!-- /.col-md-6 -->
                                        </div><!-- /.form-group -->

                                    @endif
                                @endforeach
                        </div><!-- /.col-xs-12 -->
                    @endif

                    <hr>

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-5">
                            @if(!empty($fitting_type) && $fitting_type == 'mobile')
                                <button type="submit" class="btn btn-tertiary btn-large pull-right">Step 2 - Fitting Address</button>
                            @else
                                <button type="submit" class="btn btn-tertiary btn-large pull-right">Step 2 - Delivery Details</button>
                            @endif
                        </div><!-- /.col-xs-12 -->

                        <div class="hidden-xs hidden-sm col-sm-7">
                                <a href="{{ route('cart') }}" type="submit" class="btn btn-standard btn-standard-backward pull-right">Back</a>
                        </div><!-- /.col-xs-12 -->

                    </div><!-- /.form-group -->
            </div> <!-- .section -->

		</div> <!-- .col -->
        <div class="hidden-xs hidden-sm col-md-3">

            @include('checkout.partials.sidebar-checkout')

        </div> <!-- .col-md-3 -->
    </div>
    {!! Form::close() !!}
</div> <!-- .container-->
@stop

@section('scripts')
    <script>
        $(document).ready(function(){

            // Add this to our scroll to values
            var $header_height = $('header').outerHeight() + 5;

            $('#checkout-form').validator({
                feedback: {
                    success: 'icon-simple-tick',
                    error: 'icon-alert'
                },
                disable: false
            });

            // Add to the scroll to value of validation
            $.fn.validator.Constructor.FOCUS_OFFSET = $header_height;

            $(document).on('submit', '#checkout-form', function() {
                @include('partials.analytics.checkout-progress', [
                    'cart' => $cart
                ])
                @include('partials.analytics.checkout-progress-step-1')
            });

            // Address lookup button
            $('#address-lookup').on('click', function(){
                $('#postcode-alert').hide();
                $('#loading').hide();
                $('.address-lookup-wrapper').hide();

                var $postcode = $(this).parent().prev();
                if($postcode.val().length == 0) {
                    $('#postcode-alert p').html('Please enter a postcode');
                    $('#postcode-alert').fadeIn('slow');
                } else {
                    $('#loading').fadeIn('slow');

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('postcode.lookup')}}",
                        data: {
                            'postcode': $postcode.val()
                        },
                        success: function(result){
                            $('#loading').fadeOut('slow');

                            if(result.success == false) {
                                $('#postcode-alert p').html(result.errorMessage);
                                $('#postcode-alert').fadeIn('slow');
                            } else {
                                // Success!
                                var $options = $('#address_dropdown');
                                $options.html('<option>Please select</option>');
                                $.each(result.addressList, function(){
                                    $options.append($("<option />").val(this.key).text(this.value));
                                });
                                $('.address-lookup-wrapper').fadeIn('slow');
                            }
                        },
                        error: function(){
                            $('#postcode-alert p').html('Sorry, something went wrong');
                            $('#postcode-alert').fadeIn('slow');
                        }
                    });
                }
            });

            // Address lookup dropdown selection
            $(document).on('change', '#address_dropdown', function() {

                if($(this).val().length !== 0) {
                    // Look up the full address
                    var key = $(this).val();

                    $('.address-lookup-wrapper').fadeOut('slow');
                    $('#loading').fadeIn('slow');

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('postcode.get_addresses')}}",
                        data: {
                            'key': key
                        },
                        success: function(result){
                            $('#loading').fadeOut('slow');

                            if(result.success == false) {
                                $('#postcode-alert p').html(result.errorMessage);
                                $('#postcode-alert').fadeIn('slow');
                            } else {
                                // Success!
                                $('#billing_address1').val( (result.organisationName.length == 0 ? '' : result.organisationName + ', ') + result.line1 ).trigger('input');
                                $('#billing_address2').val( result.line2 ).trigger('input');
                                $('#billing_towncity').val( result.postTown ).trigger('input');
                                $('#billing_county').val( result.county ).trigger('input');
                                $('#billing_country').val('United Kingdom').trigger('input');
                            }
                        },
                        error: function(){
                            $('#postcode-alert p').html('Sorry, something went wrong');
                            $('#postcode-alert').fadeIn('slow');
                        }
                    });
                }
            });
        });
    </script>
@stop
