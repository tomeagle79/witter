@extends('layouts.frontend')

@section('title', 'Checkout - Step 3, Summary and Payment')

@section('body-class', 'body_bg_checkout')

@section('heading')
	Checkout
@stop

@section('meta_description')
	Checkout
@stop

@section('content')

<div class="container checkout-process">

    <div class="row">
        <div class="col-xs-12">
            <h1>Summary and Payment</h1>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->

    <div class="row">
        <div class="col-xs-12 col-md-9">

            <div class="section">

                <h2 class="border_bottom">Order Summary</h2>

                <div class="delivery-notice">

                    @if(empty($fitting_type) || (!empty($fitting_type) && $fitting_type == 'self_fitted'))
                        <i class="icon-van"></i><span>The following item(s) will be delivered to your address</span>
                    @else
                        @if($fitting_type == 'mobile')
                            <i class="icon-mobile-fitter"></i>
                        @else
                            <i class="icon-garage-fitting"></i>
                        @endif
                        <span>The following item(s) will be delivered to your fitter:</span>
                    @endif
                </div><!-- /.delivery-notice -->

                <div class="mobile-box">
                    @if(!empty($cart['towbar']))
                        @include('cart.partials.cart_item', ['edit' => false, 'item' => $cart['towbar']])
                        @if(!empty($cart['contents']))
                            <hr>
                        @endif
                    @endif

                    @if(!empty($cart['contents']))
                        @foreach($cart['contents'] as $item )

                            @include('cart.partials.cart_item', ['edit' => false, 'item' => $item])
                            @if(!$loop->last)
                                <hr>
                            @endif
                        @endforeach
                    @endif
                </div><!-- /.mobile-box -->

                @include('checkout.partials.checkout_subtotal')

            </div><!-- /.section -->

            @if(empty($fitting_type) || (!empty($fitting_type) && $fitting_type == 'self_fitted'))
                <div class="section delivery-summary">
                    <h2 class="border_bottom">Delivery Summary</h2>
                    <div class="mobile-box">
                        <h3>{{ $checkout['section1']['title'] }} {{ $checkout['section1']['first_name'] }} {{ $checkout['section1']['surname'] }}</h3>

                        <p>{{  $checkout['section2']['delivery_address1'] }},
                            @if(!empty($checkout['section2']['delivery_address2'] )) {{ $checkout['section2']['delivery_address2'] }}, @endif
                            {{  $checkout['section2']['delivery_towncity'] }},
                            {{  $checkout['section2']['delivery_county'] }},
                            {{  $checkout['section2']['delivery_country'] }},
                            {{  strtoupper($checkout['section2']['delivery_postcode']) }}
                        </p>

                        <p>Telephone: {{  $checkout['section2']['delivery_telephone'] }}</p>
                    </div><!-- /.mobile-box -->
                </div> <!-- .section -->
            @endif

            <div class="section section-payment-options">

                <h2 class="border_bottom space-above">Payment Options</h2>

                <div class="row payment-options ">

                    <div class="col-xs-12">
                        <div class="mobile-box">
                            <div class="row">
                                <div class="col-xs-12">
                                    <label class="radio-lg" for="payment_option_card"><span>Pay by card</span>
                                        <input type="radio" name="payment_option" value="card" id="payment_option_card">
                                        <span class="checkmark"></span>
                                    </label>

                                    <div class="payment-stripe-form" style="display:none;">
                                        <form action="{{ route('checkout.charge') }}" method="post" id="payment-form">
                                            <div class="form-row">

                                                <div id="card-element">
                                                    <!-- a Stripe Element will be inserted here. -->
                                                </div>

                                                <!-- Used to display form errors -->
                                                <div id="card-errors"></div>
                                            </div>
                                        </form>

                                        <div id="loading" class="alert alert-help alert-help-info" style="display:none;"><p>Loading...</p></div>
                                        <div id="stripe-error" class="alert alert-help alert-help-warning" style="display:none;"><p></p></div>
                                        <p class="help-block">All payments are secure and all major credit and debit cards are accepted.</p>
                                        <div class="clearfix"></div>
                                    </div><!-- /.payment-stripe -->
                                    <hr>
                                </div><!-- /.col-xs-12 -->

                                <div class="col-xs-12">
                                    <label class="radio-lg"  for="payment_option_paypal"><span>Pay with PayPal</span><img src="/assets/img/pp-logo-100px.png" border="0" alt="PayPal Logo" class="pull-right">
                                        <input type="radio" name="payment_option" value="paypal"  id="payment_option_paypal">
                                        <span class="checkmark"></span>
                                    </label>
                                </div><!-- /.col-xs-12 -->
                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-xs-12">
                                        <div id="payment_not_selected" class="alert alert-help alert-help-payment" style="display: none;"><p>Please select your preferred payment method.</p></div>
                                </div><!-- /.col-xs-12 -->
                            </div><!-- /.row -->

                        </div><!-- /.mobile-box -->
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.row -->
            </div> <!-- .section -->

            <div class="section agreements">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="border_bottom space-above">Confirm and Place Order</h2>
                    </div><!-- /.col-xs-12 -->

                    <div class="col-md-4 col-xs-12">
                        <label class="checkbox-lg">Get Special Offers, Promotions And Competiton Details Via Email.
                            <input type="checkbox" name="agreed_contact" id="agreed_contact" value="1" >
                            <span class="checkmark"></span><!-- /.checkmark -->
                        </label><!-- /.checkbox-lg -->
                    </div><!-- /.col-md-4 -->

                    <div class="col-md-4 col-xs-12">
                        <label class="checkbox-lg">Help Us To Improve Our Products And Services Via Market Research.
                            <input type="checkbox" name="agreed_market_research" id="agreed_market_research" value="1" >
                            <span class="checkmark"></span><!-- /.checkmark -->
                        </label><!-- /.checkbox-lg -->
                    </div><!-- /.col-md-4 -->

                    <div class="col-md-4 col-xs-12">
                        <label class="checkbox-lg">I agree to Witter's <a href="/terms-and-conditions" title="View Terms and Conditions" target="_blank">Terms and Conditions and Privacy Policy</a> *
                            <input type="checkbox" name="agreed_terms" id="agreed_terms" value="1" required>
                            <span class="checkmark"></span><!-- /.checkmark -->
                        </label><!-- /.checkbox-lg -->
                    </div><!-- /.col-md-4 -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-xs-12">
                            <div id="terms_missing" class="alert alert-help alert-help-terms" style="display: none;"><p>Please agree to the Witter Terms and Conditions before making your purchase.</p></div>
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.row -->

                <div class="row payment_buttons">

                    <div class="col-xs-12  col-sm-5">

                        <button type="submit" class="btn btn-tertiary btn-large pull-right make-payment">Buy now</button>

                        <div id="paypal-button-1" style="display:none;" class="paypal-button"></div>

                    </div><!-- /.col-xs-12 -->

                    <div class="hidden-xs hidden-sm col-sm-7">
                            <a href="{{ route('checkout.section2') }}" type="submit" class="btn btn-standard btn-standard-backward pull-right">Back</a>
                    </div><!-- /.col-xs-12 -->

                    <div class="col-xs-12">
                            <div id="paypal-error" class="alert alert-help alert-help-warning" style="display:none;"><p></p></div>
                    </div><!-- /.col-xs-12 -->

                </div><!-- /.row -->

                @if($phone_payment_option)
                    <hr>
                    <div class="row">
                        <div class="col-xs-12">
                            <label>
                                    Pay by Phone
                            </label>
                            <p>
                                If you would prefer to provide your credit/debit card details over the phone, click the button below.
                            </p>
                            <p class="help-block">
                                (Mon - Fri: 08.30 - 16.00)
                            </p>
                            <p>
                                <button type="button" class="btn btn-primary" id="btn-pay-by-phone">Pay By Phone</button>
                            </p>
                        </div><!-- /.col-xs-12 -->
                    </div><!-- /.row -->
                @endif
            </div><!-- /.section -->

		</div> <!-- .col -->
        <div class="hidden-xs hidden-sm col-md-3">

            @include('checkout.partials.sidebar-checkout')

        </div> <!-- .col-md-3 -->
	</div>
</div>

@if($phone_payment_option)

    <div class="modal fade" tabindex="-1" role="dialog" id="pay-by-phone-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Pay By Phone</h4>
                </div> <!-- .modal-header -->
                <div class="modal-body">
                    <p class="lead">Call Witter Towbars on 01244 284500 and quote the Reference Number below:</p>

                    <p>Reference code: <strong id="ref">ABC123</strong></p>

                    <p class="text-block">Lines are open Mon-Thu 9am - 5pm and on Friday 9am - 3.30pm</p>

                    <p class="text-block">Please note: The reference code is only live for a couple of hours so please ring as soon as possible</p>

                    <hr>

                    <p>
                        When you call you will be given a Confirmation Number to enter into the box below.
                    </p>
                    <div class="row">
                        <div class="col-md-7 col-sm-6">
                            <input type="text" name="confirmation_code" id="fld-confirmation" value="" class="form-control tall-input">
                        </div> <!-- .col-md-9 -->
                        <div class="col-md-5 col-sm-6">
                            <button type="button" class="btn btn-primary btn-block" id="btn-confirmation">Continue Order</button>
                        </div> <!-- .col-md-3 -->
                    </div> <!-- .row -->

                    <div class="alert alert-help alert-help-info" role="alert" id="ppp-loading" style="display: none;">
                        <p>Loading...</p>
                    </div>

                    <div class="alert alert-help alert-help-info" role="alert" id="ppp-alert" style="display: none;">
                        <p></p>
                    </div>

                </div> <!-- .modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    @endif

@stop

@section('scripts')

<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script>
    var postcode  = '{{ (empty($checkout['section1']['billing_postcode']) ? '' : $checkout['section1']['billing_postcode']) }}';
    var address1  = '{{ (empty($checkout['section1']['billing_address1']) ? '' : $checkout['section1']['billing_address1']) }}';
    var address2  = '{{ (empty($checkout['section1']['billing_address2']) ? '' : $checkout['section1']['billing_address2']) }}';
    var towncity  = '{{ (empty($checkout['section1']['billing_towncity']) ? '' : $checkout['section1']['billing_towncity']) }}';
    var county    = '{{ (empty($checkout['section1']['billing_county'])   ? '' : $checkout['section1']['billing_county']) }}';
    var country   = '{{ (empty($checkout['section1']['billing_country'])  ? '' : $checkout['section1']['billing_country']) }}';
    var telephone = '{{ (empty($checkout['section1']['telephone'])        ? '' : $checkout['section1']['telephone']) }}';

    var first_name = '{{ (empty($checkout['section1']['first_name']) ? null : $checkout['section1']['first_name']) }}';
    var surname = '{{ (empty($checkout['section1']['surname']) ? null : $checkout['section1']['surname']) }}';
    var full_name = (first_name.length > 0 ? first_name + ' ' : '') + surname;

    // Create a Stripe client
    var stripe = Stripe('{{ config('settings.stripe_publishable') }}');

    // Add this to our scroll to values
    var $header_height = $('header').outerHeight() + 5;

    // Create an instance of Elements
    var elements = stripe.elements();

    function gdpr_check(obj) {

        $(".agreements input[type=checkbox]:checked").each(function(){

            obj[$(this).attr('name')] = 1;
        });

        return obj;
    }

    function setUpPaymentButtons() {

        var payment_type = $('input[name=payment_option]:checked').val();

        if (payment_type == 'paypal') {
            $('.make-payment').hide();
            $('.payment-stripe-form').fadeOut('slow');

            $('.paypal-button').show();
            $('#payment_not_selected').fadeOut('slow');

        } else if (payment_type == 'card') {
            $('.paypal-button').hide();
            $('#paypal-error').hide();

            $('.make-payment').show();
            $('.payment-stripe-form').fadeIn('slow');
            $('#payment_not_selected').fadeOut('slow');
        }
    }


    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#636B6E',
            lineHeight: '24px',
            fontFamily: '"Work Sans", Helvetica Neue, Arial',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#636B6E'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>
    card.mount('#card-element');

    $(document).ready(function(){

        // On page load display the correct payment buttons.
        setUpPaymentButtons();

        $(document).on('payment-made', function(e, gateway) {
            @include('partials.analytics.checkout-progress', [
                'cart' => $cart
            ])

            @include('partials.analytics.checkout-progress-step-3')
        });

        $('#agreed_terms').change(function() {
            if($(this).is(":checked")) {
                $('#terms_missing').fadeOut('slow');
            }else{

                $('#terms_missing').fadeIn('slow');
            }
        });

        // Handle payment
        $('.make-payment').on('click', function(e){

            e.preventDefault();

            // If no payment type selected show the card message.
            if( ! $('input:radio[name=payment_option]').is(':checked')){

                $('#payment_not_selected').fadeIn('slow');

                $('html, body').animate({
                    scrollTop: $(".section-payment-options").offset().top-$header_height
                }, 1000);

                return false;
            }

            // Hide the payment selection warning.
            $('#payment_not_selected').hide();

            // Check that the Witter terms and conditions have been selected.
            if($('#agreed_terms').is(":not(:checked)")){

                $('#terms_missing').fadeIn('slow');

                $('html, body').animate({
                    scrollTop: $(".agreements").offset().top-$header_height
                }, 1000);

                return false;
            }

            // Prepare stripe.
            $('.make-payment').addClass('disabled');
            $('.make-payment').attr("disabled", true);
            $('#loading').fadeIn('slow');
            $('#stripe-error').hide();

            stripe.createToken(card, {
                'name' : full_name,
                'address_line1' : address1,
                'address_line2' : address2,
                'address_city' : towncity,
                'address_state' : county,
                'address_zip' : postcode,
                'address_country' : country,
            }).then(function(result) {
                // handle result.error or result.token
                if(result.error) {

                    $('.make-payment').removeClass('disabled');
                    $('.make-payment').attr("disabled", false);
                    $('#loading').fadeOut('slow', function(){
                        $('#stripe-error p').html(result.error.message);
                        $('#stripe-error').fadeIn('slow');
                    });

                    $('html, body').animate({
                        scrollTop: $(".section-payment-options").offset().top-$header_height
                    }, 1000);
                }

                if(result.token) {
                    // Fire off the token etc
                    // Data to post
                    var charge_data = result.token;

                    charge_data = gdpr_check(charge_data);

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('checkout.charge') }}',
                        data: charge_data,
                        success: function(data) {
                            if(data.success == true) {
                                // Listened for by gtag code
                                $(document).trigger('payment-made', ['Stripe']);

                                setTimeout(function(){
                                    location.href = '{{ route('checkout.complete') }}';
                                }, 1000);
                            } else {
                                $('#loading').fadeOut('slow', function() {
                                    $('#stripe-error p').html(data.error);
                                    $('#stripe-error').fadeIn('slow');
                                });

                                $('html, body').animate({
                                    scrollTop: $(".section-payment-options").offset().top-$header_height
                                }, 1000);
                            }
                        },
                        error: function() {
                            $('#loading').fadeOut('slow', function() {
                                $('#stripe-error p').html('There was a problem taking your payment.  Please get in touch with us on <a href="tel:+441244284555">01244 284555</a> to complete your order.');
                                $('#stripe-error').fadeIn('slow');

                            });

                            $('html, body').animate({
                                scrollTop: $(".section-payment-options").offset().top-$header_height
                            }, 1000);
                        }
                    });
                }
            });
        });
    });

    // Depending on the payment option.
    $('input[type=radio][name=payment_option]').change(function() {

        setUpPaymentButtons();
    });

    var CREATE_PAYMENT_URL  = "{{ route('paypal-payment') }}";
    var EXECUTE_PAYMENT_URL = "{{ route('checkout.charge') }}";


    function isValid() {
        return document.querySelector('#agreed_terms').checked;
    }

    function onChangeCheckbox(handler) {
        document.querySelector('#agreed_terms').addEventListener('change', handler);
    }

    function toggleValidationMessage() {
        if(isValid()){
            $('#terms_missing').fadeOut('slow');
        } else {
            $('#terms_missing').fadeIn('slow');

            $('html, body').animate({
                scrollTop: $(".agreements").offset().top-$header_height
            }, 1000);
        }
    }

    function toggleButton(actions) {
        return isValid() ? actions.enable() : actions.disable();
    }

    ['#paypal-button-1', '#paypal-button-2', '#paypal-button-3'].forEach(function(selector) {
        paypal.Button.render({
            env: '{{ empty(config('paypal.paypal_sandbox')) ? 'production' : 'sandbox' }}', // Or 'sandbox', 'production'
            commit: true, // Show a 'Pay Now' button
            style: {
                label: 'pay',
                color: 'gold',
                size: 'medium',
                shape: 'rect',   // pill | rect
                tagline: 'false',
            },

            validate: function(actions) {
                toggleButton(actions);

                onChangeCheckbox(function() {
                    toggleButton(actions);
                });
            },

            onClick: function() {
                toggleValidationMessage();
            },

            payment: function(data, actions) {
                // Clear any old errors.
                $('#paypal-error').hide();

                // Call our paypal end-point
                return paypal.request.get(CREATE_PAYMENT_URL).then(function(data){
                    return data.id;
                });
            },

            onAuthorize: function(data, actions) {

                // Data to post
                var charge_data = {
                    paymentID: data.paymentID,
                    payerID:   data.payerID,
                };

                charge_data = gdpr_check(charge_data);

                return paypal.request.post(EXECUTE_PAYMENT_URL, charge_data).then(function(data) {

                    // The payment is complete!
                    // You can now show a confirmation message to the customer
                    if(data.success == true) {
                        // Listened for by gtag code
                        $(document).trigger('payment-made', ['PayPal']);

                        setTimeout(function(){
                            location.href = '{{ route('checkout.complete') }}';
                        }, 1000);
                    } else {
                        $('#loading').fadeOut('slow', function() {
                            $('#paypal-error p').html(data.error);
                            $('#paypal-error').fadeIn('slow');
                        });
                    }

                }).catch(function(err) {

                });
            },

            onCancel: function(data, actions) {
                /*
                * Buyer cancelled the payment
                */
                $('#paypal-error p').html('The <b>PayPal</b> payment was cancelled. Please try again, or get in touch with us on <a href="tel:+441244284555">01244 284555</a> to complete your order.');
                $('#paypal-error').fadeIn('slow');
            },

            onError: function(err) {
                /*
                * There was an error.
                */
                var message_html = '';
                // message_html += '<b>PayPal Message:</b> ' + err.message + '<br><br>';
                message_html += 'There was a problem taking payment via PayPal. Please try again, or try payment using a credit or debit card. <br> You can get in touch with us on <a href="tel:+441244284555">01244 284555</a> to complete your order.';

                $('#paypal-error p').html(message_html);
                $('#paypal-error').fadeIn('slow');
            }

        }, selector);
    });


        @if($phone_payment_option)
            // Handle pay by phone - request code and open modal
            $('#btn-pay-by-phone').on('click', function() {

                if($('#agreed_terms').is(":not(:checked)")) {
                    $('#terms_missing').fadeIn('slow');

                    return false;
                }else{

                    $('#terms_missing').fadeOut('slow');
                }

                $('#btn-pay-by-phone').addClass('disabled');
                $('#btn-pay-by-phone').attr("disabled", true);

                $.ajax({
                    type: 'POST',
                    url: '{{ route('checkout.phone_prepare') }}',
                    success: function(data) {

                        $('#btn-pay-by-phone').removeClass('disabled');
                        $('#btn-pay-by-phone').attr("disabled", false);

                        if(data.success == true) {
                            $('#pay-by-phone-modal .modal-body #ref').html(data.request_code);
                            $('#pay-by-phone-modal').modal('show');
                            $('#fld-confirmation').focus();
                        } else {
                            alert(data.error);
                        }
                    },
                    error: function() {
                        $('#btn-pay-by-phone').removeClass('disabled');
                        $('#btn-pay-by-phone').attr("disabled", false);

                        alert('Sorry, there was a problem.  Please try again.');
                    }
                });

            // Handle pay by phone - validate request code
            $(document).on('click', '#btn-confirmation', function() {
                $('#ppp-loading').show();
                $('#ppp-alert').hide();

                $('#btn-confirmation').addClass('disabled');
                $('#btn-confirmation').attr("disabled", true);

                telephone_data = {
                    'requestCode' : $('#pay-by-phone-modal .modal-body #ref').html(),
                    'responseCode' : $('#fld-confirmation').val()
                };
                telephone_data = gdpr_check(telephone_data);

                $.ajax({
                    type: 'POST',
                    url: '{{ route('checkout.phone_request') }}',
                    data: telephone_data,
                    success: function(data) {
                        $('#btn-confirmation').removeClass('disabled');
                        $('#btn-confirmation').attr("disabled", false);
                        if(data.success == true) {
                            // Listened for by gtag code
                            $(document).trigger('payment-made', ['Telephone']);

                            setTimeout(function(){
                                location.href = '{{ route('checkout.complete') }}';
                            }, 1000);
                        } else {
                            $('#ppp-loading').fadeOut('slow', function() {
                                $('#ppp-alert p').html(data.error);
                                $('#ppp-alert').fadeIn('slow');
                            });
                        }
                    },
                    error: function() {
                        $('#ppp-loading').fadeOut('slow', function() {
                            $('#ppp-alert p').html('There was a problem authorising your code, please try again.');
                            $('#ppp-alert').fadeIn('slow');
                        });

                        $('#btn-confirmation').removeClass('disabled');
                        $('#btn-confirmation').attr("disabled", false);
                    }
                });
            });


         });
    @endif

</script>
@stop
