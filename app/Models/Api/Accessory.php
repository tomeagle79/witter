<?php

namespace App\Models\Api;

use App\Models\Api\ApiModel;

class Accessory extends ApiModel
{
    const URI = "accessories";

    /**
     * Get accessories based off category and sub-category
     */
    public static function get_accessories_by_slug($category_slug, $subcategory_slug)
    {
        $url = config('witter.api_base') . 'partcategories/slug/accessories?classSlug=' . $category_slug . '&categorySlug=' . $subcategory_slug;
        $obj = self::get_request($url, true); // Second param forces isSellable logic

        return $obj;
    }

    /**
     * Get accessories based off category and sub-category
     */
    public static function get_accessories_by_class_slug($category_slug, $subcategory_slug = null)
    {

        if ($subcategory_slug == null || is_null($subcategory_slug) || $subcategory_slug == '') {
            $url = config('witter.api_base') . 'partclasses/slug/accessories?classSlug=' . $category_slug;
        } else {
            $url = config('witter.api_base') . 'partclasses/slug/accessories?classSlug=' . $category_slug . '&categorySlug=' . $subcategory_slug;
        }

        $obj = self::get_request($url, true); // Second param forces isSellable logic

        return $obj;
    }

    /**
     * Get a single accessory
     *
     * @params Array
     * @return Object
     */
    public static function get_single_where(array $args)
    {
        $url = config('witter.api_base') . self::URI . '?' . http_build_query($args);

        // Second param forces isSellable logic
        // We set it to false as items go in and out of stock and the product view page needs to handle this.
        $obj = self::get_request($url, false);

        return $obj;
    }

    /**
     * list of popular products for homepage list
     *
     * @params Array
     * @return Object
     */
    public static function get_popular_products()
    {
        $url = config('witter.api_base') . self::URI . '/mostpopular';
        // Second param forces isSellable logic
        // We set it to false as items go in and out of stock and the product view page needs to handle this.
        $obj = self::get_request($url, false);

        return $obj;
    }

    /**
     * list of also bought products for cart page list
     *
     * @return Object
     */
    public static function get_also_bought()
    {
        $url = config('witter.api_base') . self::URI . '/alsobought';

        $obj = self::get_request($url, false);

        return $obj;
    }

    /**
     * Get a single accessory
     *
     * @params Array
     * @return Object
     */
    public static function get_single_by_slug($slug)
    {
        $url = config('witter.api_base') . self::URI . '/slug?' . http_build_query(['partSlug' => $slug]);
        // Second param forces isSellable logic
        // We set it to false as items go in and out of stock and the product view page needs to handle this.
        $obj = self::get_request($url, false);

        return $obj;
    }

    /**
     * Get a filtered collection of accessories
     *
     * @param string $category_slug
     * @param string $subcategory_slug
     * @param array $filter
     * @return Object collection
     */
    public static function get_filtered_accessories_by_slug($category_slug, $subcategory_slug, array $filter)
    {
        // Get the data form the API.
        $collection = collect(self::get_accessories_by_slug($category_slug, $subcategory_slug));

        // Filter the brand
        if (isset($filter['brand'])) {

            $collection = $collection->where('brandName', ucfirst($filter['brand']));
        }

        // Filter the price range
        if (isset($filter['price'])) {

            // If the price contains a grater than character.
            if (strpos($filter['price'], '>')) {

                // Strip back to the number
                $price = preg_replace('/[^0-9,.]/', '', $filter['price']);

                $collection = $collection->where('displayPrice', '>=', (int) $price);
            }

            // If the price contains a less than character.
            if (strpos($filter['price'], '<')) {

                // Strip back to the number
                $price = preg_replace('/[^0-9,.]/', '', $filter['price']);

                $collection = $collection->where('displayPrice', '<=', (int) $price);
            }

            // If the price contains a dash
            if (strpos($filter['price'], '-')) {

                // Price split the filter values.
                $price = explode('-', $filter['price']);

                // Strip back to the number
                $price_from = preg_replace('/[^0-9,.]/', '', $price[0]);

                $price_to = preg_replace('/[^0-9,.]/', '', $price[1]);

                $collection = $collection->where('displayPrice', '>=', (int) $price_from)
                    ->where('displayPrice', '<=', (int) $price_to);
            }
        }

        return $collection;
    }
}
