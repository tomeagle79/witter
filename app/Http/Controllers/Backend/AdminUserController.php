<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Datatables;
use App\Http\Requests\Backend\UserStoreRequest; 
use Hash;
use URL;
use Auth; 

class AdminUserController extends Controller
{

    /**
     * Instantiate a new instance.
    */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('backend.admin_users.index');
    }

    /**
     * Return the datatable for users
     *
     * @return Datatables
     */
    public function indexDatatable()
    {
        $users = User::select(array('users.id', 'users.name', 'users.email', 'users.created_at', 'users.updated_at'))->get();

        return Datatables::of($users)
            ->addColumn('edit', '<a href="{{ URL::backend(\'admin_users/edit/\'. $id) }}" class="btn btn-success">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'admin_users/delete/\'. $id) }}" class="btn btn-danger" data-confirm="Are you sure you want to delete this user?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('backend.admin_users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Backend\UserStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $user = new User();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect(URL::backend('admin_users'))->with('message', '<div class="alert alert-success"><strong>Success!</strong> User created.</div>');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('backend.admin_users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Backend\UserStoreRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->email = $request->email;
        $user->name = $request->name;
        if(!is_null($request->password)){
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return redirect(URL::backend('admin_users'))->with('message', '<div class="alert alert-success"><strong>Success!</strong> User updated.</div>');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect(URL::backend('admin_users'))->with('message', '<div class="alert alert-success"><strong>Success!</strong> User deleted.</div>');
    }
}
