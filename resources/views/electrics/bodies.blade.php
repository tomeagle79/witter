@extends('layouts.frontend')

@section('title', $manufacturer->manufacturerName .' '. $model->modelName .' Electrical Kits')

@section('heading')
	Electrical Kits for {{ $manufacturer->manufacturerName }} {{ $model->modelName }} @if(!empty($body)) {{ $body->bodyName }} @endif
@stop

@section('meta_description')Electrical Kits for {{ $manufacturer->manufacturerName }} {{ $model->modelName }}, Please select your vehicle.@stop

@section('content')

<div class="container">

    <div class="row">

        <div class="col-md-9 col-md-push-3">

            <div class="row">
                <div class="col-md-9 towbars-listing">

                    <h2>{{ $manufacturer->manufacturerName }} {{ $model->modelName }} @if(!empty($body)) {{ $body->bodyName }} @endif</h2>

                    <div class="item-list">
                        @include('partials.search-dropdowns-child', ['redirect_slug' => 'electrical-kits',
                            'form_button_text' => 'Find your kit',
                            'display_models' => false])
                    </div> <!-- .item-list -->

                </div> <!-- .col-md-9 -->

                <div class="col-md-3">

                    @if(!empty($model->photoUrl))
                        <div class="top-img-wrapper">
                            <img src="{{ $model->photoUrl }}" alt="{{ $model->modelName }}" class="top-img">
                            <p class="text-center">Images are representative only</p>
                        </div> <!-- .wrapper -->

                    @else
                        @if( !empty($manufacturer->logoUrl) )
                            <div class="top-img-wrapper">
                                <img src="{{ $manufacturer->logoUrl }}" alt="{{ $manufacturer->manufacturerName }}" class="top-img">
                                <p class="text-center">Images are representative only</p>
                            </div> <!-- .wrapper -->
                        @endif
                    @endif


                </div><!-- /.col-md-3 -->

            </div><!-- /.row -->

            @if(empty($body))
                <div class="row">
                    <div class="col-md-12 towbars-listing">
                        <hr >
                        @if(count($bodies) > 0)
                            <h2 class="title-margin-bottom">Find an Electrical Kit for {{ $manufacturer->manufacturerName }} {{ $model->modelName }}</h2>

                            <div class="row item-list flex-items">
                                @foreach ($bodies as $key => $body_item)
                                    <div class="col-xs-12 col-md-4">
                                        <div class="item">
                                            <div class="item-body">
                                                <a href="{{ route('electrical-kits.model_list.body_list.registration_list', [$manufacturer->slug, $model->slug, $body_item->slug]) }}" @if(!empty($body) && $body_item->slug == $body->slug) class="active" @endif>
                                                    <img src="{{ image_load($body_item->photoUrl) }}" alt="Towbar Electrical Kits for {{ $manufacturer->manufacturerName }} {{ $model->modelName }} {{ $body_item->bodyName }}" >
                                                    <h3>
                                                        {{ $manufacturer->manufacturerName }} {{ $model->modelName }}<br>
                                                        <strong>{{ $body_item->bodyName }}</strong>
                                                    </h3>
                                                </a>
                                            </div><!-- /.item-body -->
                                        </div> <!-- .item -->
                                    </div><!-- /.col-md-4 -->
                                @endforeach
                            </div> <!-- .row -->
                        @endif

                    </div><!-- /.col-md-6 -->

                </div><!-- /.row -->
            @endif

        </div> <!-- .col-md-9 -->

        <div class="col-md-3 col-md-pull-9">
            @include('partials.sidebar-search')

            @include('partials.sidebar-info-menu')

        </div> <!-- .col-md-3 -->

    </div><!-- /.row -->
</div><!-- /.container -->
@stop
