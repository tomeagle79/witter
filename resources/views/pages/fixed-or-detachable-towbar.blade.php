@extends('layouts.frontend')

@section('title', 'Fixed or Detachable Towbar Guide')

@section('meta_description')
Choosing the right towbar for your car can be tricky, especially if you have never had one fitted before. If you are stuck between...
@stop

@section('body-class', 'complete-guide land-rover-towing-guide')

@section('heading')
    Fixed or Detachable Towbar Guide
@stop

@section('content')
    <a name="#top" id="top"></a>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-sm-push-3 col-md-push-3 page-content">

                <h2 class="small" id="main">Fixed or Detachable Towbar</h2>
                <hr>
                <p>Choosing the right towbar for your car can be tricky, especially if you have never had one fitted before. If you are stuck between a fixed or detachable towbar, you are probably wondering ‘which one do I need? ‘what neck type should I choose?’ and ‘how does a detachable towbar work?’</p>
                <p>To help you choose the right towbar, we will explore the features and differences between a fixed and detachable system so you can buy the right type for your car. Our guide will look at:</p>

                <ul>
                    <li><a class="smoothscroll" href="#explained"><strong>Types of towbar</strong></a></li>
                    <li><a class="smoothscroll" href="#neck-types"><strong>Towbar neck types</strong></a></li>
                    <li><a class="smoothscroll" href="#how-fixed-towbar-work"><strong>How do fixed and detachable towbars work?</strong></a></li>
                    <li><a class="smoothscroll" href="#what-towbar"><strong>Which towbar do I need?</strong></a></li>
                </ul>

                <p>First, you need to consider what you will be using your towbar for. Do you want to use a towbar bike rack? Are you towing regularly? Does your vehicle have parking sensors? How you use your towbar will determine which one is best for you and will help you to choose between a fixed or detachable towbar. Luckily, there are plenty of types to choose from and each towbar has its own advantages and disadvantages.</p>
                <p>There are two common towbar types and two common neck types. You might be stuck between a detachable swan neck or a fixed flange, or vice versa, so we have broken down the towbar type and neck type to help you choose.</p>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3 col-sm-pull-9 col-md-pull-9 sidebar-complete-guide">
                <div class="sidebar">
                    <div class="sidebar-heading">This guide will look at:</div>
                    <ul>
                        <a class="smoothscroll" href="#main"><li>Fixed or Detachable</li></a>
                        <a class="smoothscroll" href="#explained"><li>Towbar types explained</li></a>
                        <a class="smoothscroll" href="#neck-types"><li>Towbar Neck Types</li></a>
                        <a class="smoothscroll" href="#what-towbar"><li>What towbar should I buy?</li></a>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-xs-offset-0 col-sm-9 col-md-9 col-sm-offset-3 page-content">

                <hr>
                <h2 class="small" id="explained">Towbar types explained</h2>
                <hr>

                <h3>What is a fixed towbar?</h3>
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <p>Fixed towbars are permanently attached to your vehicle, giving you the flexibility to tow whenever you need to. A fixed towbar is often a cheaper option as it has a simpler design and requires little or no maintenance.</p>
                        <p>If you have a commercial vehicle or you tow frequently, then a fixed towbar is probably the perfect choice for you. However, you could bang your legs on it when accessing the boot and if you have parking sensors, these can sometimes be triggered if your vehicle thinks there is something in the way of the car.</p>
                    </div><!-- /.col-xs-12 col-md-8 -->
                    <div class="col-xs-12 col-md-4">
                        <img src="/img/complete-guide/fixed_flange_grey.jpg" alt="Fixed flange towbar">
                    </div><!-- /.col-xs-12 col-md-4 -->
                </div><!-- /.row -->

                <hr>
                <h3>What is a detachable towbar?</h3>
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <p>A detachable towbar is just as safe and sturdy as a fixed towbar, with the added benefit of being able to remove the neck. Some detachable towbars allow you to tow and carry bikes at the same time and when you are not towing, it becomes almost invisible once removed.</p>
                        <p>A detachable towbar doesn’t affect your car’s appearance and is easy to use, remove and store. Detachable towbars have a safety lock so the neck cannot be removed. You may also be able to get a foldaway plate for your electrical socket so there is no evidence of a towbar when you are not towing.</p>
                    </div><!-- /.col-xs-12 col-md-8 -->
                    <div class="col-xs-12 col-md-4">
                        <img src="/img/complete-guide/flange_neck_grey.jpg" alt="Flange neck detachable towbar" >
                    </div><!-- /.col-xs-12 col-md-4 -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-6 m-20px-top">
                            <a href="/towbars" class="btn btn-primary btn-tertiary btn-block ">Find Your Towbar</a>
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.row -->

                <hr>
                <h2 class="small" id="neck-types">Towbar Neck Types</h2>

                <hr>
                <h3>Flange Towbar</h3>
                <div class="row">
                    <div class="col-xs-12 col-md-8">

                        <p>A flange towbar sits under your bumper and the towball is bolted on to the faceplate with either 2 or 4 bolts. This is usually the cheapest type of towbar and is generally the most common in the UK. The great thing about a flange towbar is that different types of towballs can be bolted on, which is great if you need an AL-KO towball or a ball & pin coupling.</p>
                        <p>Flange towbars are versatile as they are compatible with many accessories, which can be fitted between the faceplate of the towbar and the flange ball. These could include stabilisers, tow steps, bumper protectors or bike racks which allow you to tow and carry bikes at the same time. However, flange towbars are more likely to trigger parking sensors due to the bulkier design, which also makes them less visually appealing.</p>
                    </div><!-- /.col-xs-12 col-md-8 -->
                    <div class="col-xs-12 col-md-4">
                        <img src="/img/complete-guide/fixed_flange_grey.jpg" alt="Fixed flange towbar">
                    </div><!-- /.col-xs-12 col-md-4 -->
                </div><!-- /.row -->

                <hr>
                <h3>Swan Neck Towbar</h3>
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <p>A swan neck towbar has a narrow neck and sleek design as the neck of the towbar flows straight into the towball. This makes the swan neck more visually appealing than other types of towbars when it is fitted to your car. The narrow neck means it is less likely to trigger parking sensors.</p>
                        <p>However, the swan neck towbar does have some limitations. Although you can use a swan neck towbar to carry a bike rack, you will not be able to tow and carry bikes at the same time. It is also the least flexible type of towbar because no additional accessories can be used.</p>
                    </div><!-- /.col-xs-12 col-md-8 -->
                    <div class="col-xs-12 col-md-4">
                        <img src="/img/complete-guide/swan_neck_grey.jpg" alt="Swan neck detachable towbar" >
                    </div><!-- /.col-xs-12 col-md-4 -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-6 m-20px-top">
                            <a href="/towbars" class="btn btn-primary btn-tertiary btn-block ">Find Your Towbar</a>
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.row -->

                <hr>
                <h3>Which Towbar?</h3>
                <p>When you have decided which towbar neck type would be best for you, then you can choose whether you would prefer a detachable or a fixed towbar. However, your towbar options will depend on the make, age and model of your vehicle. The best way to see which towbars you can choose from is by entering your vehicle registration number into our search tool. That way, we will only show you towbars which are compatible with your vehicle.</p>

                <hr>

                <h3>What type of towbar do I need?</h3>
                <p>If you want to tow and carry bikes at the same time, you will need a fixed or detachable flange towbar. For regular towing, a fixed flange or swan neck towbar would be most suitable as it is always available. If you want a sleeker towbar that you can remove when you are not using it, then a detachable swan neck towbar will be best. However, the best towbar for you will ultimately depend on what is compatible with your vehicle.</p>

                <hr>

                <h3>How towbars work</h3>
                <p>Once you have determined what towbar and neck type you need, you should also know how it works. Fixed and detachable towbars work in different ways and, while you don’t need to be a towbar expert, you should know some basics so that you can tow safely.</p>

                <hr>

                <h3 id="how-fixed-towbar-work">How does a fixed towbar work?</h3>
                <p>A fixed towbar is simple. It is permanently fixed to your vehicle so you are always ready to tow. You also don’t need to worry about attaching it correctly or storing it when you don’t want to use it. Once a fixed towbar is fitted onto your car you don’t have to do anything to it, but you shouldn’t forget it is there, especially when parking or accessing the boot.</p>

                <hr>

                <h3>How does a detachable towbar work?</h3>
                <p>Detachable towbars lock into place so they are safe and secured to your car. When you remove a detachable towbar, there is a release mechanism. You also need to unlock it, as it cannot be removed without the key. The towbar electrics remain attached to the car and you may be able to get an electrics holder which folds away when the towbar has been removed. Once you have removed your towbar, you can store it in the boot of your car to prevent bumping your legs when you access the boot.</p>

                <hr>
                <h2 class="small" id="what-towbar">What towbar should I buy?</h2>
                <hr>

                <p>Some towbars are better suited to towbar cycle carriers, whilst others work better if your car has reversing sensors and you don’t want to trigger them. The best towbar for your car will also depend on what car you have and which towbars are compatible.</p>
                <p>If you don’t want to trigger parking sensors when you are not towing, you will be better with a detachable towbar. If you want to tow and carry bikes at the same time, you will need a detachable flange towbar to attach the bike rack to the faceplate.</p>
                <p>The best way to find the right towbar is to enter your vehicle registration number and see what towbars are compatible. Remember, depending on the style of towbar you choose, the price of the towbar can vary. You should always get your towbar fitted by a professional and don’t forget to purchase a towbar electrics kit.</p>

                <div class="ideal-towbar ideal-towbar--blue clearfix">
                    <p>Ready to find your ideal Towbar?</p>
                    <a href="/towbars" class="btn btn-primary btn-tertiary">Find Your Towbar</a>
                </div><!-- /.ideal-towbar -->

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <a href="#top" class="btn btn-primary back-to-top smoothscroll" >Back to top</a>
                    </div><!-- /.col-xs-12 -->
                </div>

            </div>


        </div>
    </div>
@stop


@section('scripts')
    <script type="text/javascript">
		$(document).ready(function() {
            var $root = $('html, body');

            $('a.smoothscroll').click(function () {

                // Just open the panel if data-open-panel is set
                if( $( this )[0].hasAttribute( 'data-open-panel' ) ) {

                    $( '#' + $.attr( this, 'data-open-panel' ) ).collapse('show');
                }

                $root.animate({

                    scrollTop: $( $.attr(this, 'href') ).offset().top
                }, 500);

                return true;
            });
		});
	</script>
@stop
