var $ = jQuery.noConflict();

/**
 * updatePanels When user makes a choice on the towbars purchase page
 *
 * Find the selected radio button and update the parents.
 */
function updatePanelsByRadioName (radio_name) {
	var radio_button = $('input[type=radio][name=' + radio_name + ']:checked');

	// Get the panel parent element
	var panel_selected = radio_button.closest('.panel-towbar-options');
	var panel_name = radio_button.data('name');
	var id_clicked = radio_button.attr('name');

	// console.log(panel_selected, panel_name, id_clicked);

	if (!panel_selected || !panel_name || !id_clicked) {
		$('.all_in_one_items').find('.' + radio_name).remove();
		return false;
	}

	// Add styles to the panel heading.
	panel_selected.addClass('selected');
	panel_selected.find('.panel-heading h4 span').text(panel_name);

	if ($('.all_in_one_items').find('.' + id_clicked)['length'] > 0) {
		$('.all_in_one_items').find('.' + id_clicked).remove();
		$('.all_in_one_items').append('<li class="' + id_clicked + '">' + panel_name + '</li>');
	} else {
		$('.all_in_one_items').append('<li class="' + id_clicked + '">' + panel_name + '</li>');
	}

	// Shall we hide or show the section?
	if ($('.all_in_one_items li').length > 0) {
		$('#customised-section').show();
	} else {
		$('#customised-section').hide();
	}

	return true;
}


function codingSetup (canCode, dealerCoding) {

	var $softwareUpgradeTab = $('#software-upgrade-tab');

	if (canCode) {

		// Check if coding can only be done by a dealer.
		if(dealerCoding) {

			// Disable the options.
			$softwareUpgradeTab.find('ul.towbar-option-list').hide();
			$softwareUpgradeTab.find('.can-code').hide();
			$softwareUpgradeTab.find('.cannot-code').hide();
			$softwareUpgradeTab.find('.dealer-code').show();

			// Set the false input as checked.
			$('input[name="software_upgrade"][value="true"]').attr('checked', false);
			$('input[name="software_upgrade"][value="false"]').attr('checked', true);

		} else {
			// Make sure options are not disabled.
			$softwareUpgradeTab.find('ul.towbar-option-list').show();
			$softwareUpgradeTab.find('.can-code').show();
			$softwareUpgradeTab.find('.cannot-code').hide();
			$softwareUpgradeTab.find('.dealer-code').hide();
		}

	} else {
		// Disable the options.
		$softwareUpgradeTab.find('ul.towbar-option-list').hide();
		$softwareUpgradeTab.find('.can-code').hide();
		$softwareUpgradeTab.find('.cannot-code').show();
		$softwareUpgradeTab.find('.dealer-code').hide();

		// Set the false input as checked.
		$('input[name="software_upgrade"][value="true"]').attr('checked', false);
		$('input[name="software_upgrade"][value="false"]').attr('checked', true);
	}
}


$(document).ready(function ($) {
	$('.smoothscroll').click(function () {
		// Just open the panel if data-open-panel is set
		if ($(this)[0].hasAttribute('data-open-panel')) {
			$('#' + $.attr(this, 'data-open-panel')).collapse('show');
		}

		var href = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(href).offset().top
		}, 1000);
	});

	/*

    We are using a really basic image lazy loader
    On page load, load the images in
    */
	var allimages = $('.lazy-load-images').find('img');
	for (var i = 0; i < allimages.length; i++) {
		if (allimages[i].getAttribute('data-src')) {
			allimages[i].setAttribute('src', allimages[i].getAttribute('data-src'));
		}
	}

	/*

    On the home page we use a simple read more to give more content.
    */

	$('.read-extra-content').on('click', function (e) {
		e.preventDefault();

		if ($('.extra-content').hasClass('open')) {
			$('.extra-content').slideUp(300)
				.removeClass('open');
		} else {
			$('.extra-content').slideDown(300)
				.addClass('open');
		}
	});

	/*
     * On click move down to products list
     */
	$('.view-matching-products').on('click', function (e) {
		// Scroll to div
		$('html, body').animate({
			scrollTop: ($('.item-list').offset().top - 70)
		}, 1000);
	});

	/**
     * Number plate exit button.
     *
     */
	$('.numberplate-form').on('click', '.btn-edit', function (e) {
		// Get the form the edit link sits in
		var form = $(this).parents('form:first');

		// Get the text field to enable and focus on.
		var input_text = form.find('input[type="text"]');

		input_text.prop('disabled', false).focus();
	});

	/**
     * Number plate exit button.
     *
     */
	$('.numberplate-form').on('click', '.btn-edit', function (e) {
		// Get the form the edit link sits in
		var form = $(this).parents('form:first');

		// Get the text field to enable and focus on.
		var input_text = form.find('input[type="text"]');

		input_text.prop('disabled', false).focus();
	});

	/**
     * Towbar product page selecting options.
     *
     */
	$('.panel-towbar-options input[type="radio"]').on('click', function (e) {
		// Check if the user is clicking the 'no_electrical_kit' radio button.
		if ($(this).attr('id') == 'no_electrical_kit') {
			e.preventDefault();

			$('#fitting-option-warning').show();

			return true;
		}

		if ($(this).is(':checked')) {
			// Get the panel parent element
			var panel_selected = $(this).closest('.panel-towbar-options');
			var panel_name = $(this).data('name');
			var id_clicked = $(this).attr('name');

			// Add styles to the panel heading.
			panel_selected.addClass('selected');
			panel_selected.find('.panel-heading h4 span').text(panel_name);

			if ($('.all_in_one_items').find('.' + id_clicked).length > 0) {
				$('.all_in_one_items').find('.' + id_clicked).remove();
				$('.all_in_one_items').append('<li class="' + id_clicked + '">' + panel_name + '</li>');
			} else {
				$('.all_in_one_items').append('<li class="' + id_clicked + '">' + panel_name + '</li>');
			}

			if (id_clicked === 'towball_option') {
				$('#towball').collapse('hide');
				$('#electric-kit').collapse('show');
				$('#software-upgrade-tab').collapse('hide');
			} else if (id_clicked === 'electric_option') {
				$('#towball').collapse('hide');
				$('#electric-kit').collapse('hide');

				var canCode = $(this).data('can_code');
				var dealerCoding = $(this).data('dealer_coding');

				// If can code then show teh software options
				if (canCode) {

					$('#software-upgrade-tab').collapse('show');
					$('#towbar-fitting').collapse('hide');
				} else {
				// If can't code then just show the fittings
					$('#software-upgrade-tab').collapse('hide');
					$('#towbar-fitting').collapse('show');
				}

				// Check the options and change the software update based on the coding
				if (!canCode) {
					// Tick the software update panel.
					$('#software-upgrade').addClass('selected');
				}

				// If set. Change the title of the software section.
				if (typeof $(this).data('software_title') !== 'undefined') {
					var software_title = $(this).data('software_title');

					$('#software-upgrade .panel-heading h4 span').text(software_title);
				}

				// If the user does not want an electrical kit we can't fit the towbar.
				if ($(this).attr('id') === 'no_electrical_kit') {
					$('.panel-heading-fitting-mobile').hide();
					$('.panel-heading-fitting-garage').hide();
				} else {
					$('.panel-heading-fitting-mobile').show();
					$('.panel-heading-fitting-garage').show();

					codingSetup(canCode, dealerCoding);
				}
			} else if (id_clicked === 'software_upgrade') {
				$('#towball').collapse('hide');
				$('#electric-kit').collapse('hide');
				$('#software-upgrade-tab').collapse('hide');
				$('#towbar-fitting').collapse('show');

				// Hide error messages.
				$('#own-fitter-software-upgrade-error').hide();

				// Uncheck self fitting option
				$('input[name="fitting_option"]:checked').prop('checked', false);
			}
		}
	});

	/**
     * Bootstrap collapse functionality.
     */
	$('.panel-collapse').on('show.bs.collapse', function () {
		$(this).siblings('.panel-heading').addClass('active');
	});

	$('.panel-collapse').on('hide.bs.collapse', function () {
		$(this).siblings('.panel-heading').removeClass('active');
	});

	/**
     * Checkout process functionality for Divido.
     */
	if ($('#finance_id').length > 0) {
		$('#divido-options .panel-collapse').on('show.bs.collapse', function () {
			var finance_id = $(this).attr('id').substring(4);
			$('#finance_id').val(finance_id);
		});
	}
});
