<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class Postcode extends ApiModel
{
	const URI = "postcodes";

	/**
	 * Get a list of addresses
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function lookup($postcode)
	{
		$url = config('witter.api_base') . self::URI .'?postcode=' . urlencode($postcode);
		$obj = self::get_request($url);
		
		return $obj;
	}

	/**
	 * Get a separated list of address elements for an address key
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_addresses($key)
	{
		$url = config('witter.api_base') . self::URI .'/address?key=' . urlencode($key);
		$obj = self::get_request($url);
		
		return $obj;
	}
}
