<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\HelpCategoryStoreRequest;
use App\Models\HelpCategory;
use Auth;
use Datatables;
use Hash;
use View;
use DB;
use URL;


class HelpCategoryController extends Controller
{
    /**
     * Instantiate a new instance.
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.help_categories.index');
    }

    /**
     * Return the datatable for help_categories
     *
     * @return Datatables
     */
    public function indexDatatable()
    {
        $help_categories = HelpCategory::select(['id', 'title', 'url_slug', 'created_at', 'updated_at'])->get();

        return Datatables::of($help_categories)
            ->addColumn('edit', '<a href="{{ URL::backend(\'help_categories/edit/\'. $id) }}" class="btn green">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'help_categories/delete/\'. $id) }}" class="btn red" data-confirm="Are you sure you want to delete this blog category?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.help_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  HelpCategoryStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HelpCategoryStoreRequest $request)
    {
        // save the blog_category
        $blog_category = new HelpCategory();
        $blog_category->title = $request->title;
        $blog_category->url_slug = str_slug($request->url_slug,'-');
        $blog_category->save();

        return redirect(URL::backend('help_categories'))->with('success', '<strong>Success!</strong> Blog category created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['blog_category'] = HelpCategory::find($id);

        return view('backend.help_categories.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  HelpCategoryStoreRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HelpCategoryStoreRequest $request, $id)
    {
        // save the blog_category
        $blog_category = HelpCategory::find($id);
        $blog_category->title = $request->title;
        $blog_category->url_slug = str_slug($request->url_slug,'-');
        $blog_category->save();

        return redirect(URL::backend('help_categories'))->with('success', '<strong>Success!</strong> Blog category updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog_category = HelpCategory::find($id);
        $blog_category->delete();

        return redirect(URL::backend('help_categories'))->with('message', '<strong>Success!</strong> Blog category deleted.');
    }

    /**
     * Restore the specified resource
     */
    public function restore($id)
    {
        $blog_category = HelpCategory::withTrashed()->find($id);
        $blog_category->restore();

        return redirect(URL::backend('help_categories'))->with('success', '<div class="alert alert-success"><strong>Success!</strong> Blog category restored.</div>');
    }
}
