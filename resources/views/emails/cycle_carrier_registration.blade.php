@extends('layouts.email')

@section('content')

    You received a message from Witter:

    <p>
        First Name: {{ $first_name }}
    </p>
    <p>
        Surname: {{ $surname }}
    </p>
    <p>
        Company Name: {{ $company_name }}
    </p>
    <p>
        Postcode: {{ $postcode }}
    </p>
    <p>
        Address 1: {{ $address1 }}
    </p>
    <p>
        Address 2: {{ $address2 }}
    </p>
    <p>
        Address 3: {{ $address3 }}
    </p>
    <p>
        Town/City: {{ $towncity }}
    </p>
    <p>
        Telephone: {{ $telephone }}
    </p>
    <p>
        Fax: {{ $fax }}
    </p>
    <p>
        Email: {{ $email }}
    </p>

    <hr>

    <p>
        Date Fitted: {{ $date_fitted_dd }}/{{ $date_fitted_mm }}/{{ $date_fitted_yyyy }}
    </p>

    <hr>

    <p>
        Vehicle Reg: {{ $vehicle_reg }}
    </p>
    <p>
        Cycle Carrier Serial No: {{ $cycle_carrier_serial_no }}
    </p>
    <p>
        Cycle Carrier Part No: {{ $cycle_carrier_part_no }}
    </p>

@stop