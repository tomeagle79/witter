<?php

namespace App\Http\Controllers;

use App\Services\GoogleGeocoder;
use Illuminate\Http\Request;
use App\Models\Api\ServiceCentre;
use App\Http\Requests\CentresPostcodeSearchRequest;

class ServiceCentreController extends FrontendController
{

    private $geocoder = null;

    /**
     * Any code that should be run on every front end page
     */
    public function __construct(GoogleGeocoder $geocoder)
    {
        parent::__construct();

        $this->geocoder = $geocoder;

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Service centres', route('service-centres'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CentresPostcodeSearchRequest $request)
    {
        $postcode = $request->postcode;

        if (empty($postcode)) {
            $center_collection = collect(ServiceCentre::get_approved());

            // Limit to the first 6 centers
            $this->data['approved_centers_list'] = $center_collection->splice(0, 6);
        } else {
            $this->data['breadcrumbs']->addCrumb('Search by Postcode', route('service-centres'));
            $response = ServiceCentre::get_where(['postcode' => $postcode]);
            $this->data['centres'] = collect($response->centres);

            if (!empty($this->data['centres'])) {
                $request->session()->put('centre_postcode_search', $postcode);
            }

            $this->data['nearest_centre'] = $this->data['centres']->shift();
            $this->data['geocode'] = $this->geocoder->geocode_centre($this->data['nearest_centre']);
            $this->data['approved_centers'] = $this->data['centres']->where('isApprovedCentre', true)->all();
            $this->data['centres'] = $this->data['centres']->where('isApprovedCentre', false)->all();
            $this->data['postcode'] = $postcode;
        }

        $this->data['region_arr'] = config('settings.fitting_centre.regions');

        return view('service-centres.index', $this->data);
    }

    /**
     * Display a listing of the regional resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function region_index($slug)
    {

        $this->data['breadcrumbs']->addCrumb('Service Centres', route('service-centres'));
        $this->data['regional_centres'] = collect(ServiceCentre::get_by_region_slug($slug));
        $this->data['region_arr'] = config('settings.fitting_centre.regions');
        $this->data['region_names'] = config('settings.fitting_centre.region_names');
        $this->data['slug_key'] = array_search($slug, $this->data['region_arr']);

        $this->data['current_region'] = config('settings.fitting_centre.regions_by_slug.' . $slug);

        return view('service-centres.region_index', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

        $this->data['centre'] = $centre = ServiceCentre::get_by_slug($slug);
        $this->data['geocode'] = $this->geocoder->geocode_centre($centre);

        // If the user has completed a postcode search find more local centres by postcode.
        if (session()->has('centre_postcode_search')) {
            $response = ServiceCentre::get_where(['postcode' => session()->get('centre_postcode_search')]);
            $centres = collect($response->centres);
        } else {
            // Get the nearby centres by searching the main centres postcode.
            if (!empty($centre->postcode)) {
                $response = ServiceCentre::get_where(['postcode' => $centre->postcode]);
                $centres = collect($response->centres);
            }
        }

        // Check if the collection is empty
        if (empty($centres)) {
            $this->data['centres'] = [];
        } else {
            $centre_arry = $centres->where('serviceCentreId', '!=', $centre->serviceCentreId)->all();
            // We just need 2 near by centers for the template
            $this->data['centres'] = array_slice($centre_arry, 0, 2);
        }

        $this->data['region_arr'] = config('settings.fitting_centre.regions');
        $this->data['breadcrumbs']->addCrumb($centre->centreName, route('service-centres.show', $slug));

        // Group the opening hours in an array of days.
        $opening_hours = [];

        if (!empty($centre->openingHours)) {
            foreach ($centre->openingHours as $day => $hours) {
                $hours = explode('-', $hours);

                if (is_array($hours) && count($hours) > 1) {
                    $from_time = date('H:i', strtotime(trim($hours[0])));
                    $to_time = date('H:i', strtotime(trim($hours[1])));
                    $opening_hours[substr($day, 0, 2)] = $from_time . '-' . $to_time;
                }
            }
        }

        $ld_json_hours = '';

        if (!empty($opening_hours)) {
            foreach ($opening_hours as $day => $hours) {
                if (empty($previous_hours)) {
                    $ld_json_hours .= $day . ' ' . $hours . ',';
                }
            }
        }

        $this->data['ld_json_hours'] = $ld_json_hours;

        return view('service-centres.show', $this->data);
    }
}
