@extends('layouts.frontend')

@section('title', 'Checkout - Step 1, Your Details')

@section('body-class', 'body_bg_checkout')

@section('heading')
    Share quote
@stop

@section('meta_description')
	Share quote
@stop

@section('content')

<div class="container checkout-process">
    {!! Form::open(['route' => 'quote.store', 'class' => 'form-horizontal', 'id' => 'checkout-form']) !!}
    <div class="row">
        <div class="col-xs-12">
            <h1>Share quote</h1>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->

    <div class="row">

        <div class="col-xs-12  col-md-9">

            <div class="section">
                <h2 class="border_bottom">Customer Details</h2>
                <div class="form-group has-feedback">
                    <label for="title" class="col-xs-12">Title *</label>
                    <div class="col-sm-6 col-xs-12">
                        <div class="dropdown">
                            {!! Form::select('title',
                            array_merge([null => 'Title*'], array_merge(['' => 'Title *'],array_combine(config('witter.salutations'), config('witter.salutations')))),
                            (empty($checkout['section1']['title']) ? null : $checkout['section1']['title']),
                            ['required', 'data-required-error'=>'Please select your title.', 'class' => 'form-control']) !!}
                        </div><!-- /.dropdown -->
                        <span class="form-control-feedback form-control-feedback-dropdown" aria-hidden="true"></span>
                    </div> <!-- .col -->
                    <div class="col-sm-6 col-xs-12">
                        <div class="help-block with-errors"></div>
                    </div><!-- /.col-md-6 -->
                </div><!-- /.form-group -->

                <div class="form-group has-feedback">
                    <label for="first_name" class="col-xs-12">First Name *</label>
                    <div class="col-sm-6 col-xs-12">
                        {!! Form::text('first_name',
                            (empty($checkout['section1']['first_name']) ? null : $checkout['section1']['first_name']),
                            ['required', 'data-required-error'=>'Please enter your first name.', 'class'=>'form-control', 'placeholder' => 'First Name *']) !!}
                        <span class="form-control-feedback" aria-hidden="true"></span>
                    </div> <!-- .col -->
                    <div class="col-sm-6 col-xs-12">
                        <div class="help-block with-errors"></div>
                    </div><!-- /.col-md-6 -->
                </div><!-- /.form-group -->

                <div class="form-group has-feedback">
                    <label for="surname" class="col-xs-12">Surname *</label>
                    <div class="col-sm-6 col-xs-12">
                        {!! Form::text('surname',
                            (empty($checkout['section1']['surname']) ? null : $checkout['section1']['surname']),
                            ['required', 'data-required-error'=>'Please enter your surname.', 'class'=>'form-control', 'placeholder' => 'Surname *']) !!}
                        <span class="form-control-feedback" aria-hidden="true"></span>
                    </div> <!-- .col -->
                    <div class="col-sm-6 col-xs-12">
                        <div class="help-block with-errors"></div>
                    </div><!-- /.col-md-6 -->
                </div><!-- /.form-group -->

                <div class="form-group has-feedback">
                    <label for="telephone" class="col-xs-12">Telephone/ Mobile *</label>
                    <div class="col-sm-6 col-xs-12">
                        {!! Form::text('telephone',
                            (empty($checkout['section1']['telephone']) ? null : $checkout['section1']['telephone']),
                            ['required', 'data-required-error'=>'Please enter your telephone number.',  'class'=>'form-control', 'placeholder' => 'Telephone *']) !!}
                        <span class="form-control-feedback" aria-hidden="true"></span>
                    </div> <!-- .col -->
                    <div class="col-sm-6 col-xs-12">
                        <div class="help-block with-errors"></div>
                    </div><!-- /.col-md-6 -->
                </div><!-- /.form-group -->

                <div class="form-group has-feedback">
                    <label for="email" class="col-xs-12">Email *</label>
                    <div class="col-sm-6 col-xs-12">
                        {!! Form::email('email',
                            (empty($checkout['section1']['email']) ? null : $checkout['section1']['email']),
                            ['required', 'data-required-error'=>'Please enter your email address.', 'class'=>'form-control', 'placeholder' => 'Email *']) !!}
                        <span class="form-control-feedback" aria-hidden="true"></span>
                    </div> <!-- .col -->
                    <div class="col-sm-6 col-xs-12">
                        <div class="help-block with-errors"></div>
                    </div><!-- /.col-md-6 -->
                </div><!-- /.form-group -->

                <hr>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-5">
                        <button type="submit" class="btn btn-tertiary btn-large pull-right">Submit and Share Quote</button>
                    </div><!-- /.col-xs-12 -->

                    <div class="hidden-xs hidden-sm col-sm-7">
                            <a href="{{ route('cart') }}" type="submit" class="btn btn-standard btn-standard-backward pull-right">Back</a>
                    </div><!-- /.col-xs-12 -->

                </div><!-- /.form-group -->
            </div> <!-- .section -->

		</div> <!-- .col -->
        <div class="hidden-xs hidden-sm col-md-3">

            @include('checkout.partials.sidebar-checkout')

        </div> <!-- .col-md-3 -->
    </div>
    {!! Form::close() !!}
</div> <!-- .container-->
@stop

@section('scripts')
    <script>
        $(document).ready(function(){

            // Add this to our scroll to values
            var $header_height = $('header').outerHeight() + 5;

            $('#checkout-form').validator({
                feedback: {
                    success: 'icon-simple-tick',
                    error: 'icon-alert'
                },
                disable: false
            });

            // Add to the scroll to value of validation
            $.fn.validator.Constructor.FOCUS_OFFSET = $header_height;

            {{--
            $(document).on('submit', '#checkout-form', function() {
                @include('partials.analytics.checkout-progress', [
                    'cart' => $cart
                ])
                @include('partials.analytics.checkout-progress-step-1')
            });
            --}}

        });
    </script>
@stop
