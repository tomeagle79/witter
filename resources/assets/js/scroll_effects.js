// https://daneden.github.io/animate.css/
//PARALLAX
var $document = jQuery(document);
var vp_height = jQuery(window).height();

//Lazy Loader
var lazyloaders = jQuery('.animated');
var lazyloaderslength = lazyloaders.length;


var revealpoint = Math.round(vp_height * 0.1);
var scrolled_distance = 0;

//STICKY NAV AND PARRALAX HEADER
var sticknav = function () {

    var bottom_of_window = jQuery(window).scrollTop() + (jQuery(window).height());
    if (lazyloaderslength >= 1) {
        lazyloaders.each(function (i) {
            var top_of_object = jQuery(this).offset().top;
            if (bottom_of_window > (top_of_object + revealpoint)) {
                jQuery(this).addClass(jQuery(this).data('animation'));
            } else {
                jQuery(this).removeClass(jQuery(this).data('animation'));
            }
        });
    }

    if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true;
    }
}
jQuery(window).on('scroll', function () {
    requestAnimationFrame(sticknav)
    // setTimeout(
    //   function () {
    //   }
    //   , 2000);
});

window.requestAnimationFrame(sticknav);

// Move blog arrows to keep centred on image
function moveBlogArrows() {
    var $blogImageHeight = $('#blog-carousel .item.active .blog-image').outerHeight();
    var $controllers = $('#blog-carousel .carousel-control');
    if ($(window).width() < 992) {
        // console.log($blogImageHeight)
        $controllers.css('top', $blogImageHeight / 2+'px');
    } else {
        $controllers.css('top', '50%');
    }
}

$(window).on('load resize orientationchange', moveBlogArrows);


