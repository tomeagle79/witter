<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class Towbar extends ApiModel
{
	const URI = "towbars";
	const FITTING_TYPE_SELF = "self_fitted";
	const FITTING_TYPE_MOBILE = "mobile";
	// const FITTING_TYPE_GARAGE = "garage";

	/**
	 * Get a vehicle list
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_where_by_slug(array $args)
	{
		$url = config('witter.api_base') . 'vehicles/slug/towbars?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a vehicle list
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_towbars_by_id($vehicle_id)
	{
		$url = config('witter.api_base') . 'vehicles/'. $vehicle_id .'/towbars';
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a vehicle list
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_towbars_by_id_and_date($vehicle_id, $registration_date)
	{
		$url = config('witter.api_base') . 'vehicles/'. $vehicle_id .'/towbars?registrationDate='.$registration_date;
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a single towbar
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_single_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'/slug?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}


}
