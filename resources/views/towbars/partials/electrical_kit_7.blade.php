
    <h4>7-pin Vehicle Specific Single Electrics</h4>

    <p>
        If you are towing a basic trailer, bike rack** or have a lighting board attached to whatever you are towing, a 7-pin electrics kit will support all of your basic needs. A 7-pin Vehicle Specific Single Electrics kit provides all of the basic legal lighting requirements for towing and synchronises perfectly with your car.
    </p>

    <h4>What can a 7-pin electric socket power?</h4>

    <div class="powered">
        <ul class="row towbar-includes">
            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Left indicator</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Right indicator</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Taillights</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Fog lights</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Brake lights</div>
            </li> <!-- col-sm-6 -->

        </ul>
    </div><!-- /.powered -->

    <div class="alert alert-danger">
        <p>
            {{--*Because our electric kits are specific to your vehicle, you might need a software update to activate additional benefits and safety features which you may need to pay more for.--}}
            *Because our electric kits are specific to your vehicle, you might need a software update to activate additional benefits and safety features which you can choose to book in the next step.
            <br>
            **Some bike racks require a 13-pin plug so bear this in mind when choosing your electrical kit.
        </p>
    </div><!-- /.alert -->
