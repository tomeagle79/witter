@extends('layouts.backend')

@section('title')
	Edit Blog Category
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/blog_categories/partials/_form', ['submit_text' => 'Edit Blog Category', 'type' => 'edit'])
	</form>

@stop
