@extends('layouts.frontend')

@section('title', 'Towbar Fitting ' . $current_region['name'] . ' | Professional Fitters' )

@section('heading')
	Service centres
@stop

@section('meta_description')
	{{ $current_region['meta_description'] }}
@stop

@section('content')

<div class="container service-centres">

	<div class="row">

		<div class="col-sm-7 centres-form">

			@if(empty($postcode))
				<h2>Search by postcode</h2>
			@else
				<h2>Search results for '{{ strtoupper($postcode) }}'</h2>
			@endif

			@include('service-centres.partials.postcode-form')

			@if(empty($regional_centres))
				<p>With over 450 service centres across the UK, including 'Witter Approved' service centres, you can be sure that you will receive the highest level of service no matter where you are located.</p>
			@endif

			@if(!empty($regional_centres))

				<h2>{{ $region_names[$slug_key] }}</h2>

				@foreach($regional_centres->chunk(2) as $centres_chunk)
					<div class="row">
				        @foreach ($centres_chunk as $regional_centre)
				        	<div class="col-sm-6">
				        		@include('service-centres.partials.regional-centre-card-basic', ['centre' => $regional_centre])
				        	</div><!-- /.col-sm-6 -->

				        @endforeach
					</div><!-- /.row -->
				@endforeach
			@endif

		</div><!-- /.col-sm-7 -->

		<div class="col-sm-5">

			<h2>Search by region</h2>

			@include('service-centres.partials.map-card-clear')

		</div><!-- /.col-sm-5 -->

	</div><!-- /.row -->

</div><!-- /.container -->


@stop



