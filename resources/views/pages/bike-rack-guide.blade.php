@extends('layouts.frontend')

@section('title', 'How to Choose a Bike Rack for Your Car')

@section('meta_description')
    Need help choosing the right bike rack for your vehicle? Read our easy to follow bike rack guide. All you need to know about buying and fitting a bike rack.
@stop

@section('body-class', 'complete-guide')

@section('heading')
    HOW TO CHOOSE A BIKE RACK FOR YOUR CAR
@stop

@section('content')
    <a name="#top" id="top"></a>
    <div class="container">
        <div class="row hidden-xs">
            <div class="col-xs-12">
                <p>If you’re new to the world of <a href="/bike-racks">bike racks</a> then choosing the right rack for your vehicle can be a bit of a daunting experience.</p>
                <p>Do you go for a 2, 3 or 4 bike mount? What’s the difference between a towbar bike rack and a towball bike rack? And are bike racks really that safe? All questions that will no doubt overwhelm a bike rack beginner. </p>
                <p>That's why we’ve decided to put together this simple, step by step guide to understanding the different types of bike racks on offer. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-sm-push-3 col-md-push-3 page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading active" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <div class="pill-title">Bike Rack Overview</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        @include('partials.guide.bike-rack.bikerack-overview')
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="collapsed" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <div class="pill-title" id="best-rack">Which is the Best Bike Rack?</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12" id="best-rack">
                                              <p>The best rack for your car will depend on how often you intend to carry bikes.</p>
                                              <p>If you’re only planning to use the bike rack on the odd occasion, then a rear mounted bike rack will be the best option. </p>
                                              <p>If you’re planning to use the bike rack quite frequently, over a summer holiday for example, then a <a href="/accessory/350036600001">towball mounted bike rack with secure locking</a> and easy access to the car boot is the option for you</p>
                                              <p>If you want a bike rack that can easily be attached and removed at a moment’s notice, then a towbar mounted bike rack will be the option for you.</p>
                                              <p>If you don’t want a bike rack blocking visibility at the rear of your vehicle, then a roof-mounted bike rack will be the option for you.</p>

                                            </div>

                                            <div class="col-xs-12">
                                                <table class="table table-bordered table-responsive table-hover">
                                                    <thead>
                                                        <tr>
                                                            <td></td>
                                                            <td>Ease of use</td>
                                                            <td>Access to the boot</td>
                                                            <td>Secure locking</td>
                                                            <td>Carry and Tow</td>
                                                            <td>Affordable</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Towball Bike Rack</td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Towbar Bike Rack</td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Rear Mounted Bike Rack</td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Roof Mounted Bike Rack</td>
                                                            <td></td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td align="center"><img src="/assets/img/tick.png" width="20" alt=""></td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="collapsed" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <div class="pill-title" id="safe-rack">Are Bike Racks Safe?</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                              <p>All bike racks purchased from respectable manufacturers and fitted by a professional will be perfectly safe for British roads.</p>
                                              <p>Bike racks are regularly tested to the extreme on a test track, so you don’t need to worry about dawdling along at 70mph on the motorway. </p>
                                              <p>The only obvious danger from a bike rack is lack of visibility from the rear window. Options like the rear mounted bike rack can make it particularly difficult to see out of the back window and can decrease your awareness of the road. </p>
                                              <p>Options like the towball mounted bike rack and the roof bike rack allow full visibility out of the rear window. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="collapsed" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <div class="pill-title" id="which-rack">Which racks will fit my vehicle?</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                              <p>For the towball, towbar and roof mounted bike racks, you do not have to worry about purchasing vehicle specific models. Each bike rack is designed and developed to fit securely to every towbar or set of roof bars.</p>
                                              <p>The rear-mounted roof racks are a little different. Due to the varying shape and sizes of vehicles, you will need to purchase a vehicle specific model for your car. This is due to the general design and strap sizes that will be developed exclusively for your car. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFive">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="collapsed" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            <div class="pill-title" id="can-i-fit">Can I Fit a Bike Rack Myself?</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <p>Yes, it could not be easier!</p>
                                                <p>All bike racks developed and sold by Witter are designed to be simple and easy to install and remove from your vehicle.</p>
                                                <p>Following four simple steps, each of our bike racks takes no longer than 10 minutes to fit to your vehicle. </p>
                                                <p>Don’t believe us? Watch our 6-minute installation video below. Here we show you how to install a <a href="/bike-racks/bikelander-classic-towball-mounted-tilting-2-bike-carrier">towball mounted bike carrier with tilting functionality</a>.</p>
                                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/51KaZlLPCD0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingSix">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="collapsed" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            <div class="pill-title" id="what-else">What Else Should I Consider Before Buying a Bike Rack?</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <p>Now that you understand the types of roof racks available and the advantages and disadvantages of each, you’re almost ready to buy a bike rack.</p>
                                                <p>Before you do, make sure you consider the following:</p>

                                                <p><strong>Carrying Capacity</strong></p>
                                                <p>Our three and <a href="/bike-racks">four bike carriers</a> are designed to carry a maximum load of 65 kg, which corresponds to 3 to 4 adult bikes. </p>
                                                <p>However, it is essential that the combined trailer nose weight and weight of the cycles does not exceed the maximum towball nose load, as specified by the vehicle manufacturer. </p>
                                                <p>Exceeding this load invalidates the warranty of the bike carrier, vehicle and the towbar. Any "clip" on accessories, e.g. pumps, bottles, bags etc. should be removed to reduce weight.</p>
                                                <p>Find your <a href="/what-can-i-tow">vehicle’s towball nose load here. </a></p>

                                                <p><strong>Warranty Against Theft</strong></p>
                                                <p>The locks on Witter Cycle Carriers are designed to deter vandalism and theft. Bike carriers and property should be removed from the vehicle when it is unattended for any extended period.</p>

                                                <p><strong>Security</strong></p>
                                                <p>Towball bike racks and roof bike racks can be securely locked to the towball with a <a href="/accessory/ZXL%7C%7C%7C1">bike carrier locking key.</a></p>
                                                <p>These bike racks have lockable arms that attach to the frame of the bike. In some cases you will also have the option to upgrade their support arms to lockable handles, to secure bikes to the carrier.</p>

                                                <p><strong>Lighting Board and Number Plates</strong></p>
                                                <p>When the bikes or bike carrier obscure the vehicle’s rear lights or registration number, it is a legal requirement to duplicate these functions at the rear of the load. </p>
                                                <p>All of our towball style bike racks will block/obscure the number plate, that's why each carrier comes with a <a href="/accessory/ZX59">lighting board</a> that allows for a number plate to be attached, so you are always adhering to the legal requirements.</p>
                                                <p>Some models will have a unique 'Park Mode' that allows it to be legally driven in the folded up position. In 'Park Mode', the lighting board will flip down to show the number plate and lights.</p>
                                                <p>All of our towball bike racks come complete with a 7-pin and 13-pin electrical plug to accommodate both 7-pin and 13-pin sockets with no adapter needed.</p>
                                                <p><strong>Bike Storage</strong></p>
                                                <p>All of our towball bike racks should not be stored outside in damp conditions for extended periods of time. Their ability to compactly fold up allows them to be easily stored indoors. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingSeven">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="collapsed" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                            <div class="pill-title" id="insurance">Can Fitting a Bike Rack Affect my Insurance?</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <p>It mostly depends on the insurance provider but, yes, fitting a bike rack can potentially be classified as a car modification. Therefore, it is essential that you notify your insurance provider about this change to your vehicle.  </p>
                                                <p>Car modifications can affect how your insurance premium is calculated and, if you don’t tell your insurance provider, they may use the modification to invalidate your policy in the event of an insurance claim. </p>
                                                <p>Always notify your insurance provider when making modifications to your car.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            @include('partials.sidebar-complete-guide-bike')
        </div>
    </div>
@stop


@section('scripts')
    <script type="text/javascript">
		$(document).ready(function() {
            var $root = $('html, body');

            $('a.smoothscroll').click(function () {

                // Just open the panel if data-open-panel is set
                if( $( this )[0].hasAttribute( 'data-open-panel' ) ) {

                    $( '#' + $.attr( this, 'data-open-panel' ) ).collapse('show');
                }

                $root.animate({

                    scrollTop: $( $.attr(this, 'href') ).offset().top
                }, 500);

                return true;
            });
		});
	</script>
@stop
