<p>When is best for you?</p>

<div class="appointments">
    <form class="form-horizontal" action="" method="post" id="book-form">
        <div class="strip">

            <div class="row">
                <div class="col-md-6">
                    <div id="datepicker"></div>

                </div> <!-- .col-md-6 -->
                <div class="col-md-6">
                    <div class="bordered">

                            <input type="hidden" name="appointment_date" id="appointment_date" value="">
                            <div class="form-group">
                                <label for="" class="col-sm-12 control-label">Fitter's address</label>
                                <div class="col-sm-12">
                                    <p class="form-control-static">

                                        {{ $appointments->address }}

                                        @if($appointments->fitterType == 'mobile')
                                            <span class="control-label">Mobile Fitter (They come to you, with an additional charge of &pound;25)</span>
                                        @endif
                                    </p>
                                </div>
                            </div> <!-- .form-group -->

                            <div class="form-group">
                                <label for="" class="col-sm-12 control-label">Selected date</label>
                                <div class="col-sm-12">
                                    <p class="form-control-static" id="date-text">

                                    </p>
                                </div>
                            </div> <!-- .form-group -->

                            <div class="form-group">
                                <label for="" class="col-sm-12 control-label">Select a time</label>
                                <div class="col-sm-12">
                                    <select name="appointment_id" class="form-control" id="appointment_id">
                                        <option>Select a time</option>
                                    </select>
                                </div>
                            </div> <!-- .form-group -->

                    </div> <!-- .bordered -->
                </div> <!-- .col-md-6 -->
            </div> <!-- .row -->
        </div><!-- /.strip -->
        <div class="row">
            <div class="col-md-6">

                <p class="tel">Got any questions? Just give us a call <a href="tel:+441244284555">01244 284 555</a></p>

            </div> <!-- .col-md-6 -->

            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-primary-large">Book Fitting</button>
                    </div><!-- /.col-md-12 -->
                </div> <!-- .form-group -->
            </div><!-- /.col-md-6 -->
        </div> <!-- .row -->
    </form>
</div> <!-- .appointments -->

<script>
    $(function () {
        var appointments = {!! json_encode($appointments->data) !!};

        var enabled_dates = [
            @foreach ( $appointments->enabled_dates as $key => $value )
                moment('{{ $value }}'),
            @endforeach
        ]

        var partner = {!! json_encode($appointments->partner) !!};
        var partnerId = '{{ $appointments->partnerId }}';
        var address = '{{ $appointments->address }}';
        var partNo = '{{ $appointments->partNo }}';
        var variantTowbarId = '{{ $appointments->variantTowbarId }}';
        var vehicleDesc = '{{ $appointments->vehicleDesc }}';
        var fitterType = '{{ $appointments->fitterType }}';
        var electric = '{{ $appointments->electric }}';

        $('#datepicker').datetimepicker({
            inline: true,
            sideBySide: true,
            format: 'DD/MM/YYYY',
            enabledDates: enabled_dates
        });

        $("#datepicker").on("dp.change", function (e) {
            $('#date-text').html(e.date.format('Do MMMM YYYY'));
            $('#appointment_date').val(e.date.format('YYYYMMDD'));

            var dt = e.date.format('YYYYMMDD');

            $('#appointment_id').html();
            var options = '';
            $.each(appointments[dt], function(i,v) {
                options = options + '<option value="' + appointments[dt][i].appointmentId + '">' + appointments[dt][i].appointmentTime + '</option>' + "\n";
            });
            $('#appointment_id').html(options);
        });

        // Appointment chosen
        $(document).on('submit', '#book-form', function(e){
            e.preventDefault();

            var appointmentId = $('#appointment_id').val();
            var appointmentTime = $('#appointment_id option:selected').html();
            var appointmentDateFriendly = $('#date-text').html();
            var appointmentDate = $('#appointment_date').val();

            if(appointmentId.length == 0 || appointmentTime.length == 0 || appointmentDateFriendly.length == 0 || appointmentDate.length == 0) {
                alert('Please select a date and time before continuing.');
                return false;
            }
            // Put the location copy together, the location address or a message about mobile fitter.
            if(fitterType == 'mobile') {

                address_message = 'Mobile fitter, they come to you';

            } else {

                address_message = address;
            }

            {{-- /* Check if we are getting an appointment for a towbar fitting*/ --}}
            @if(empty($appointments->variantTowbarId) && is_null(request()->caravanMover) || !is_null(request()->caravanMover) && request()->caravanMover === true)

                var html = '';
                html += '<table class="appointment">' + "\n";
                html += '   <tr>' + "\n";
                html += '       <td><strong>Location:</strong></td>' + "\n";
                html += '       <td>' + address_message + '</td>' + "\n";
                html += '   </tr>' + "\n";
                html += '   <tr>' + "\n";
                html += '       <td><strong>Fitting Date:</strong></td>' + "\n";
                html += '       <td>' + appointmentDateFriendly + '</td>' + "\n";
                html += '   </tr>' + "\n";
                html += '   <tr>' + "\n";
                html += '       <td><strong>Fitting Time:</strong></td>' + "\n";
                html += '       <td>' + appointmentTime + '</td>' + "\n";
                html += '   </tr>' + "\n";
                html += '</table>' + "\n";

                $('#selected-fitter').html(html);
                $('#selected-fitter').attr('data-date', appointmentDate);

            @else

                var html = '';
                html += '<div class="row">' + "\n";
                html += '   <div class="col-md-2 col-sm-2 col-xs-2">' + "\n";
                html += '       <i class="icon-green-tick"></i>' + "\n";
                html += '   </div>' + "\n";
                html += '   <div class="col-md-10 col-sm-10 col-xs-10">' + "\n";
                html += '       <p><strong>' + address_message + '</strong> - ' + appointmentDateFriendly + ' @ ' + appointmentTime + '</p>' + "\n";

                html += '   </div>' + "\n";
                html += '</div>' + "\n";

                if(fitterType == 'mobile') {
                    $('#selected-fitter-mobile').css('display', 'block');
                    $('#selected-fitter').css('display', 'none');
                    $('#selected-fitter-mobile').html(html);
                    $('#selected-fitter-mobile').attr('data-date', appointmentDate);
                    $('#fitting_option_mobile').prop("checked", true);
                } else {
                    $('#selected-fitter').css('display', 'block');
                    $('#selected-fitter-mobile').css('display', 'none');
                    $('#selected-fitter').html(html);
                    $('#selected-fitter').attr('data-date', appointmentDate);
                    $('#fitting_option_workshop').prop("checked", true);
                }

            @endif

            $('#modal').modal('hide');

            $('.process .final.greyed').removeClass('greyed');

            // Set the various order fields
            $('#fld_partNo').val(partNo);
            $('#fld_variantTowbarId').val(variantTowbarId);
            $('#fld_vehicleDesc').val(vehicleDesc);
            $('#fld_appointmentTime').val(appointmentTime);
            $('#fld_appointmentId').val(appointmentId);
            $('#fld_appointmentDate').val(appointmentDate);
            $('#fld_partnerId').val(partnerId);
            $('#fld_fitterType').val(fitterType);
            $('#fld_fitterFee').val('{{ $appointments->mobileFitCost }}');
            $('#fld_address').val(address);

            if(fitterType == 'mobile') {
                $('p.type').html("Your all-in-one mobile fitted price:");
            } else {
                $('p.type').html("Your all-in-one price:");
            }

            @if(!empty($appointments->variantTowbarId))
                updatePanelsByRadioName('fitting_option');
            @endif

            calculatePrice();
        });

    });
</script>
