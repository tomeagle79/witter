
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-3">
				<h2>OUR PRODUCTS</h2>

				<ul class="list-unstyled">
					<li><a href="{{ route('towbars') }}">Towbars</a></li>
					<li><a href="{{ route('accessories') }}">Accessories</a></li>
					<li><a href="{{ route('electrics') }}">Electrical Kits</a></li>
					<li><a href="{{ route('bike-racks') }}">Bike Racks</a></li>
					<li><a href="{{ route('roof-systems') }}">Roof Systems</a></li>
				</ul>
			</div>

			<div class="col-xs-6 col-sm-3">
				<h2>USEFUL LINKS</h2>

				<ul class="list-unstyled">
					<li><a href="{{ route('product_registration') }}">Register My Product</a></li>
					<li><a href="{{ route('returns') }}">Product Returns</a></li>
					<li><a href="/warranty-information">Warranty Information</a></li>
					<li><a href="/what-can-i-tow">What Can I Tow?</a></li>
					<li><a href="/about">The WItter Difference</a></li>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-3 third-column">
				<h2>SOCIAL</h2>

				<p>“@php
					// Get latest tweet
					$latest_tweet = \App\Models\TwitterCache::orderBy('id', 'desc')->first();
					$latest_tweet_text = !empty($latest_tweet) ? Twitter::linkify($latest_tweet->tweet) : '';
					echo $latest_tweet_text;
				@endphp"</p>
				<div class="links">
					<a href="https://www.facebook.com/WitterTowbar"><i class="icon-facebook" aria-hidden="true"></i></a>
					<a href="https://twitter.com/witter_towbars"><i class="icon-twitter" aria-hidden="true"></i></a>
				</div> <!-- /.links -->
			</div>
			<div class="col-xs-6 col-sm-3">
				<h2>SITE LINKS</h2>

				<ul class="list-unstyled">
					<li><a href="/blog">Witter News</a></li>
					<li><a href="/about">About</a></li>
					<li><a href="/privacy">Privacy</a></li>
					<li><a href="/careers">Careers</a></li>
					<li><a href="/terms-of-use">Terms of Use</a></li>
					<li><a href="/terms-and-conditions">Terms and Conditions</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

<div class="cookie-message">
	<div class="container">
		<p>
			This website uses cookies. Using this website means you are okay with this
			but you can find out more and learn how to manage your cookie choices
			<a href="/cookie-policy">here</a>.

			<button type="button" class="btn btn-default btn-xs">OK, got it.</button>
		</p>
	</div>
</div>
