

<div class="towbar-horizontal-search hidden-xs hidden-sm hidden-md">

    <div class="row">

        <div class="numberplate-search-wrapper">

            {!! Form::open(['route' => 'search.numberplate', 'class' => 'form numberplate-search float-right', 'method' => 'get', 'onsubmit' => "
                gtag('event', 'Success', {
                    event_category: 'Reg lookup',
                    event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                });            
            "]) !!}

            <div class="bg">
                <div class="lead">
                    <p>Let’s find <br> your vehicle</p>
                </div><!-- /.lead -->


                <div class="form-group">
                     {!! Form::text('search', null, ['required', 'class' => 'form-control', 'placeholder' => 'Reg Number']) !!}
                </div><!-- /.form-group -->

                <div class="form-group">
                    <button type="submit"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                </div><!-- /.form-group -->
            </div><!-- /.bg -->

            {!! Form::close() !!}

        </div><!-- /.numberplate-search -->

        <div class="or">
            <p>or</p>
        </div><!-- /.or -->

        <div class="search-dropdown-wrapper">
            {!! Form::open(['url' => '/', 'id' => 'search-dropdown-form', 'class' => 'search-dropdown-form', 'onsubmit' => "
                gtag('event', 'Success', {
                    event_category: 'Model select',
                    event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                });
            "]) !!}
                <div class="bg">

                    <div class="form-group">
                        <select name="manufacturer" id="manufacturer" class="manufacturer" data-style="btn-select-box">
                            <option>Make</option>
                            @foreach($manufacturers_dropdown as $mdrop)
                                <option value="{{ $mdrop->manufacturerId }}" data-slug="{{ $mdrop->slug }}">{{ $mdrop->manufacturerName }}</option>
                            @endforeach
                        </select>
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        {{ Form::select('models', [null => 'Model'], null, ['id' => 'model', 'class'=>'model', 'data-style' => 'btn-select-box']) }}
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        {{ Form::select('bodies', [null => 'Body'], null, ['id' => 'body', 'class'=>'body', 'data-style' => 'btn-select-box']) }}
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        {{ Form::select('registrations', [null => 'Year'], null, ['id' => 'registration', 'class'=>'registration', 'data-style' => 'btn-select-box']) }}

                        {{ Form::hidden('manufacturerSlug', '', ['id' => 'manufacturerSlug', 'class'=>'manufacturerSlug']) }}
                        {{ Form::hidden('modelSlug', '', ['id' => 'modelSlug', 'class'=>'modelSlug']) }}
                        {{ Form::hidden('bodySlug', '', ['id' => 'bodySlug', 'class'=>'bodySlug']) }}
                        {{ Form::hidden('registrationId', '', ['id' => 'registrationId', 'class'=>'registrationId']) }}
                    </div>

                    <div class="form-group">
                        <button type="submit" ><span class="sr-only">Search</span></button>
                    </div>
                </div><!-- /.bg -->

            {!! Form::close() !!}

        </div><!-- /.search-dropdown-wrapper -->

    </div><!-- /.row -->

</div><!-- /.towbar-horizontal-search -->


<div class="towbar-horizontal-search hidden-lg">

    <div class="numberplate-search-wrapper">

        <div class="row">
            <div class="col-xs-12">

                <div class="bg">

                    <div class="row">
                        <div class="col-sm-7 col-md-8">
                            <div class="lead">
                                <strong>Let’s find your vehicle</strong>
                                So we can show you the best products for you
                            </div><!-- /.lead -->
                        </div><!-- /.col-md-8 -->

                        <div class="col-sm-5 col-md-4">
                            {!! Form::open(['route' => 'search.numberplate', 'class' => 'form numberplate-search float-right', 'method' => 'get', 'onsubmit' => "
                                gtag('event', 'Success', {
                                    event_category: 'Reg lookup',
                                    event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                                });
                            "]) !!}

                                <div class="minus-button">
                                     {!! Form::text('search', null, ['required', 'class' => 'form-control', 'placeholder' => 'Reg Number']) !!}
                                </div><!-- /.form-group -->

                                <div class="button-wrapper">
                                    <button type="submit"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                                </div><!-- /.form-group -->

                            {!! Form::close() !!}
                        </div><!-- /.col-md-4 -->
                    </div><!-- /.row -->
                </div><!-- /.bg -->

            </div><!-- /.col-xs-12 -->

        </div><!-- /.row -->

        <div class="row">

            <div class="col-xs-12">

                <p class="splitter">or</p>

            </div><!-- /.col-xs-12 -->
        </div><!-- /.row -->

        <div class="row">

            <div class="col-xs-12">
                {!! Form::open(['url' => '/', 'id' => 'search-dropdown-form', 'class' => 'search-dropdown-form', 'onsubmit' => "
                    gtag('event', 'Success', {
                        event_category: 'Model select',
                        event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                    });
                "]) !!}
                <div class="bg">

                    <div class="row">
                        <div class="col-sm-3 nopadding-right-sm">

                            <select name="manufacturer" id="manufacturer" class="manufacturer" data-style="btn-select-box">
                                <option>Make</option>
                                @foreach($manufacturers_dropdown as $mdrop)
                                    <option value="{{ $mdrop->manufacturerId }}" data-slug="{{ $mdrop->slug }}">{{ $mdrop->manufacturerName }}</option>
                                @endforeach
                            </select>

                        </div><!-- /.col-sm-3 -->


                        <div class="col-sm-3 nopadding-right-sm">
                                {{ Form::select('models', [null => 'Model'], null, ['id' => 'model', 'class'=>'model', 'data-style' => 'btn-select-box']) }}

                        </div><!-- /.col-sm-3 -->

                         <div class="col-sm-3 nopadding-right-sm">
                                {{ Form::select('bodies', [null => 'Body'], null, ['id' => 'body', 'class'=>'body', 'data-style' => 'btn-select-box']) }}

                        </div><!-- /.col-sm-3 -->

                         <div class="col-sm-3">
                            <div class="minus-button">
                                {{ Form::select('registrations', [null => 'Year'], null, ['id' => 'registration', 'class'=>'registration', 'data-style' => 'btn-select-box']) }}

                                {{ Form::hidden('manufacturerSlug', '', ['id' => 'manufacturerSlug', 'class'=>'manufacturerSlug']) }}
                                {{ Form::hidden('modelSlug', '', ['id' => 'modelSlug', 'class'=>'modelSlug']) }}
                                {{ Form::hidden('bodySlug', '', ['id' => 'bodySlug', 'class'=>'bodySlug']) }}
                                {{ Form::hidden('registrationId', '', ['id' => 'registrationId', 'class'=>'registrationId']) }}

                            </div><!-- /.registrations-wrapper -->

                            <div class="button-wrapper">
                                <button type="submit" ><span class="sr-only">Search</span></button>
                            </div><!-- /.button-wrapperp -->
                        </div><!-- /.col-sm-3 -->

                    </div><!-- /.row -->
                </div><!-- /.bg -->

                {!! Form::close() !!}
            </div><!-- /.col-xs-12 -->
        </div><!-- /.row -->


    </div><!-- /.numberplate-search -->


</div><!-- /.towbar-horizontal-search -->
