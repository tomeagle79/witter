<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CartTotalItems
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!empty($request->route()->getName()) && ($request->route()->getName() == "checkout.complete" || $request->route()->getName() == "quote.show")) {

            return $next($request);
        }

        // From the session get the total number of items in the cart.
        if (!empty($cart = Session::get('cart', []))) {

            $count = 0;

            // Loop through the contents and add how many we have.
            if (!empty($cart['contents'])) {
                foreach ($cart['contents'] as $item) {
                    $count++;
                }
            }

            // We can only have one towbar so if this is not empty add an item top the cart.
            if (!empty($cart['towbar'])) {
                $count++;
            }

            $request->attributes->add(['cart_total_items' => $count]);
        }

        return $next($request);
    }
}
