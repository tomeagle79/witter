<?php

namespace App\Console\Commands;

use App\Models\Api\VehicleManufacturer;
use App\Models\Page;
use Illuminate\Console\Command;
use Psr\Http\Message\UriInterface;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\SitemapIndex;
use Spatie\Sitemap\Tags\Url;

class BuildSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to build the XML sitemap. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // List the file names we will generate here
        $file_names = ['pages' => 'sitemaps/pages-sitemap.xml',
            'bike' => 'sitemaps/bike-racks-sitemap.xml',
            'accessories' => 'sitemaps/accessories-sitemap.xml',
            'blog' => 'sitemaps/blog-sitemap.xml',
            'help' => 'sitemaps/help-advice-sitemap.xml',
            'caravan' => 'sitemaps/caravan-movers-sitemap.xml',
        ];

        /* Build the sub sitemaps
         */
        $this->generateMainPageSitemap($file_names['pages']);

        $this->generateCaravanMoversSitemap($file_names['caravan']);

        $this->generateBikeRacksSitemap($file_names['bike']);

        $this->generateAccessoriesSitemap($file_names['accessories']);

        $this->generateBlogSitemap($file_names['blog']);

        $this->generateHelpSitemap($file_names['help']);

        // Add Towbars array of files
        $towbars_arr = $this->generateTowbarSitemap();

        if (is_array($towbars_arr)) {

            $file_names = array_merge($file_names, $towbars_arr);
        }

        // add electrical-kits array of files
        $electricals_arr = $this->generateElectricalKitSitemap();

        if (is_array($electricals_arr)) {

            $file_names = array_merge($file_names, $electricals_arr);
        }

        /* Create a sitemap index file with all the above files.
         */
        $main_sitemap = SitemapIndex::create();

        foreach ($file_names as $xml_file) {
            // Check if the xml file exists
            if (file_exists(public_path($xml_file))) {
                $main_sitemap->add(url($xml_file));
            }
        }

        $main_sitemap->writeToFile(public_path('sitemap.xml'));

        $this->info('Main index sitemap done!');

        $this->info('All done!');

    }

    /*
    Generate sitemap Main pages
     */
    public function generateMainPageSitemap($filename)
    {
        // Main pages
        $this->info('Build the main site pages.');
        $pages_sitemap = Sitemap::create()

            ->add(Url::create(url('/'))->setPriority(1.0))
            ->add(Url::create(url('/towbars'))->setPriority(1.0))
            ->add(Url::create(url('/caravan-movers'))->setPriority(1.0))
            ->add(Url::create(url('/accessories'))->setPriority(1.0))
            ->add(Url::create(url('/bike-racks'))->setPriority(1.0))
            ->add(Url::create(url('/electrical-kits'))->setPriority(1.0))
            ->add(Url::create(url('/roof-systems'))->setPriority(1.0))
            ->add(Url::create(url('/help-advice'))->setPriority(0.9))
            ->add(Url::create(url('/blog'))->setPriority(0.9))
            ->add(Url::create(url('/contact-us'))->setPriority(0.9));

        // Get the content pages from the DB.
        $pages = Page::published()->with('url_slug')->get();

        foreach ($pages as $page) {

            $pages_sitemap->add(Url::create(url($page->url_slug->url_slug))->setPriority(0.9));
        }

        $pages_sitemap->writeToFile(public_path($filename));

        return true;
    }

    /*
    Generate sitemap for blog pages
     */
    public function generateBlogSitemap($filename)
    {
        // Blog pages
        $this->info('Build the blog sitemap.');
        SitemapGenerator::create(url('/blog'))
            ->shouldCrawl(function (UriInterface $url) {

                // Check the path starts with needle
                $needle = '/blog';
                $length = strlen($needle);

                // Ensure that any url is not a paginated page
                if (strpos($url->getQuery(), 'page') !== false) {

                    return false;
                }

                return (substr($url->getPath(), 0, $length) === $needle);

            })->writeToFile(public_path($filename));

        return true;
    }

    /*
    Generate Help & FAQ pages
     */
    public function generateHelpSitemap($filename)
    {
        // Help & advice and faq pages
        $this->info('Build the help-advice and faq.');
        SitemapGenerator::create(url('/help-advice'))
            ->shouldCrawl(function (UriInterface $url) {

                // Check the path starts with needle "/help-advice"
                $needle = '/help-advice';
                $length = strlen($needle);
                $inAccessoriesFolder = (substr($url->getPath(), 0, $length) === $needle);
                // Check the path starts with needle "/faqs"
                $needle = '/faqs';
                $length = strlen($needle);
                $inVehicleTrackersFolder = (substr($url->getPath(), 0, $length) === $needle);

                // if any return true
                return ($inAccessoriesFolder || $inVehicleTrackersFolder);

            })->writeToFile(public_path($filename));

        return true;
    }

    /*
    Generate carriers pages sitemap
     */
    public function generateBikeRacksSitemap($filename)
    {
        // Cycle carriers pages
        $this->info('Build the cycle-carriers file.' . date('H:i d/m/Y'));
        SitemapGenerator::create(url('/bike-racks'))
            ->shouldCrawl(function (UriInterface $url) {

                // Check the path starts with needle
                $needle = '/bike-racks';
                $length = strlen($needle);
                return (substr($url->getPath(), 0, $length) === $needle);

            })->writeToFile(public_path($filename));

        return true;
    }

    /*
    Generate Caravan Movers
     */
    public function generateCaravanMoversSitemap($filename)
    {
        // Caravan moverspages
        $this->info('Build the caravan-movers file.' . date('H:i d/m/Y'));
        SitemapGenerator::create(url('/caravan-movers'))
            ->shouldCrawl(function (UriInterface $url) {

                // Check the path starts with needle
                $needle = '/caravan-movers';
                $length = strlen($needle);
                return (substr($url->getPath(), 0, $length) === $needle);

            })->writeToFile(public_path($filename));

        return true;
    }

    /*
    Generate accessories and vehicle tracker pages sitemap
     */
    public function generateAccessoriesSitemap($filename)
    {
        // accessories and vehicle trackers
        $this->info('Build the accessories file.' . date('H:i d/m/Y'));

        SitemapGenerator::create(url('/accessories'))
            ->shouldCrawl(function (UriInterface $url) {

                // Check the path starts with needle "/accessories"
                $needle = '/accessories';
                $length = strlen($needle);
                $inAccessoriesFolder = (substr($url->getPath(), 0, $length) === $needle);
                // Check the path starts with needle "/vehicle-trackers"
                $needle = '/vehicle-trackers';
                $length = strlen($needle);
                $inVehicleTrackersFolder = (substr($url->getPath(), 0, $length) === $needle);

                // If the url has queries then ignore
                if($url->getQuery()) {

                    $inVehicleTrackersFolder = false;
                }

                // if any return true
                return ($inAccessoriesFolder || $inVehicleTrackersFolder);

            })->writeToFile(public_path($filename));

        return true;
    }

    /*
    Generate sitemap for towbars
     */
    public function generateTowbarSitemap()
    {
        $towbars_arr = [];

        $this->info('Start the towbars scan.');

        // We split the scan by manufacturer
        $manufacturers = VehicleManufacturer::get_all();

        foreach ($manufacturers as $manufacturer) {

            $slug = $manufacturer->slug;

            $this->info('Scan ' . $slug . ' ' . date('H:i d/m/Y'));

            SitemapGenerator::create(url('/towbars/' . $slug))
                ->shouldCrawl(function (UriInterface $url) use ($slug) {

                    $this->info($slug);

                    // Check the path starts with needle "/towbars"
                    $needle = '/towbars/' . $slug;
                    $this->info($url->getPath());
                    $length = strlen($needle);
                    if (substr($url->getPath(), 0, $length) === $needle) {
                        $this->info($url->getPath());
                        return true;
                    }

                    return false;

                })->writeToFile(public_path('sitemaps/towbars-' . $slug . '.xml'));

            $towbars_arr[] = 'sitemaps/towbars-' . $slug . '.xml';

            $this->info('towbars-' . $slug . '.xml done! ' . date('H:i d/m/Y'));
        }

        $this->info('Generate sitemap index for towbars.');

        return $towbars_arr;
    }

    /*
    Generate sitemap for electrical kits.
     */
    public function generateElectricalKitSitemap()
    {
        $electricals_arr = [];

        $this->info('Start the electrical kits scan.');

        // We split the scan by manufacturer
        $manufacturers = VehicleManufacturer::get_all();

        foreach ($manufacturers as $manufacturer) {

            $slug = $manufacturer->slug;

            $this->info('Scan ' . $slug . ' ' . date('H:i d/m/Y'));

            SitemapGenerator::create(url('/electrical-kits/' . $slug))
                ->shouldCrawl(function (UriInterface $url) use ($slug) {

                    $this->info($slug);

                    // Check the path starts with needle "/electrical-kits"
                    $needle = '/electrical-kits/' . $slug;

                    $length = strlen($needle);
                    if (substr($url->getPath(), 0, $length) === $needle) {
                        $this->info($url->getPath());
                        return true;
                    }

                    return false;

                })->writeToFile(public_path('sitemaps/electrical-kits-' . $slug . '.xml'));

            $electricals_arr[] = 'sitemaps/electrical-kits-' . $slug . '.xml';

            $this->info('sitemaps/electrical-kits-' . $slug . '.xml done! ' . date('H:i d/m/Y'));
        }

        return $electricals_arr;
    }
}
