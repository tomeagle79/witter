@extends('layouts.frontend', ['heading_h2' => true])

@section('title')
	{{ !empty($page->meta_title) ? $page->meta_title : $page->title }}
@stop

@section('heading')
	{{ $page->title }}
@stop

@section('meta_description')
	{!! $page->meta_description !!}
@stop

@section('content')

	<div class="container">
		<div class="row">

			<div class="col-sm-8 col-md-9">
				<article>

					@if(!is_null($page->image))

						<img src="{{URL('imagecache', ['template' => 'original', 'filename' => $page->image->filename])}}" alt="{!! $page->title !!}" />

					@endif

					<h1>{!! $page->title !!}</h1>

					<div class="blog-copy">
						{!! $page->content !!}
					</div>

				</article>
			</div> <!-- col-md-9 -->

			<div class="col-sm-4 col-md-3">

				@include('partials.sidebar-search')

				@include('partials.sidebar-findlink-secondary')

			</div> <!-- .col-md-3 -->

		</div> <!-- .row -->
	</div> <!-- .container -->
@stop
