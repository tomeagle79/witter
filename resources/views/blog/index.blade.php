@extends('layouts.frontend')

@section('title')
	Blog
@stop

@section('heading')
	Witter Blog
@stop

@section('meta_description')
	The Witter Towbars blog, news, articles and inspiration
@stop

@section('content')
	@include('blog.loop', ['blog_posts' => $blog_posts])
@stop