<?php

namespace App\Models\Api\Suspension;

use App\Models\Api\ApiModel;

class SuspensionVehicle extends ApiModel
{
    /**
     * Get all models based on model ID
     *
     * @param Int
     * @return Object
     */
    public static function get_all_from_model_id($model_id)
    {
        $url = config('witter.api_base') . 'suspensions/models/' . $model_id . '/vehicles';
        $obj = self::get_request($url);

        $obj = collect($obj)->sortByDesc(function ($item, $key) {
            return preg_replace('/\D/', '', ($item->fromYear . $item->vehicleId));
        })->values()->all();

        // Loop the returned data
        foreach ($obj as $key => $value) {

            // Build a full name for the body type from the parts of the returned object.
            $obj[$key]->full_name = '';

            $obj[$key]->full_name .= empty($value->chassisDesignation) ? '' : 'Chassis ' . $value->chassisDesignation . ', ';

            if (empty($value->toYear)) {

                $obj[$key]->full_name .= $value->fromYear . ' - Onwards';
            } else {

                $obj[$key]->full_name .= 'From ' . $value->fromYear . ' to ' . $value->toYear;
            }

            if (!empty($value->notes)) {

                $obj[$key]->full_name .= ' - ' . $value->notes;
            }
        }

        return $obj;
    }

    /**
     * Get all models based on model slug
     *
     * @param Int
     * @return Object
     */
    public static function get_all_by_slug($args)
    {
        $url = config('witter.api_base') . 'suspensions/models/slug/vehicles?' . http_build_query($args);
        $obj = self::get_request($url);

        return $obj;
    }
}
