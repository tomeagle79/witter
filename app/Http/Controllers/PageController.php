<?php

namespace App\Http\Controllers;

use App\Models\Career;
use App\Models\Page;

class PageController extends FrontendController
{
    /**
     * Show the page
     *
     * @param  string $slug
     * @param  id     $slug_id
     * @return Response
     */
    public function view($slug, $slug_id)
    {
        $page = Page::published()->where('url_slug_id', $slug_id)->first();

        if (empty($page)) {
            abort(404);
        } else {

            $this->data['page'] = $page;

            $this->data['breadcrumbs']->addCrumb($page->title);

            return view('pages.view', $this->data);
        }
    }

    /**
     * Show the about page
     *
     * @return Response
     */
    public function about()
    {
        // Get all categories
        $this->data['careers'] = Career::published()->get();

        $this->data['breadcrumbs']->addCrumb('About');

        return view('pages.about', $this->data);
    }

    /**
     * Show the towbar guide page
     *
     * @return Response
     */
    public function towbar_guide()
    {
        $this->data['breadcrumbs']->addCrumb('Towbar Guide');

        return view('pages.towbar-guide', $this->data);
    }

    /**
     * Show the towbar guide page
     *
     * @return Response
     */
    public function what_can_i_tow()
    {
        $this->data['breadcrumbs']->addCrumb('Help & Advice');
        $this->data['breadcrumbs']->addCrumb('What can I tow?');

        return view('pages.what-can-i-tow', $this->data);
    }

    /**
     * Show the towbar guide page
     *
     * @return Response
     */
    public function will_my_caravan_mover_fit()
    {
        $this->data['breadcrumbs']->addCrumb('Help & Advice');
        $this->data['breadcrumbs']->addCrumb('Will my caravan mover fit?');

        return view('pages.will-my-caravan-mover-fit', $this->data);
    }

    /**
     * Show the bike rack guide page
     *
     * @return Response
     */
    public function bike_rack_guide()
    {
        $this->data['breadcrumbs']->addCrumb('Bike Rack Guide');

        return view('pages.bike-rack-guide', $this->data);
    }

    /**
     * Show the land rover guide page
     *
     * @return Response
     */
    public function land_rover_towing_guide()
    {
        $this->data['breadcrumbs']->addCrumb('Help & Advice');
        $this->data['breadcrumbs']->addCrumb('Land Rover Towing Guide');

        return view('pages.land-rover-towing-guide', $this->data);
    }

    /**
     * Show the land rover guide page
     *
     * @return Response
     */
    public function fixed_or_detachable_towbar()
    {
        $this->data['breadcrumbs']->addCrumb('Help & Advice');
        $this->data['breadcrumbs']->addCrumb('Fixed or Detachable Towbar');

        return view('pages.fixed-or-detachable-towbar', $this->data);
    }

    /**
     * This is the handler defined in routes for admin.404, it will only be called when admin urls 404
     * - frontend 404s are served by Laravel's standard 404 behaviour, directly access views/errors/404
     */
    public function view404()
    {
        // provide minimum of data to show 404
        $this->data['canonical_url'] = route('home');
        $this->data['navbar_categories'] = [];
        $this->data['show_breadcrumbs'] = false;
        $this->data['show_heading'] = true;
        $this->data['basket_items'] = 0;
        return view('errors/404', $this->data);
    }

}
