<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Login | {{ config('settings.site.name') }}</title>
    <link href="{{ asset('assets/css/login.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script>

    </script>
</head>
<body class="login">
    <div class="logo">
        <img src="{{ asset('assets/img/logo.png') }}" alt="{{ config('settings.site.name') }}" />
    </div> <!-- .logo -->

    <div class="content">
        @yield('content')
    </div> <!-- .content -->

    <div class="copyright">&copy; <?php echo date('Y'); ?> {{ config('settings.site.name') }}.  Created by <a href="{{ config('settings.developer.url') }}" target="_blank" title="{{ config('settings.developer.name') }}">{{ config('settings.developer.name') }}</a>.</div>

    <script src="{{ asset('assets/js/login.js') }}"></script>
</body>
</html>
