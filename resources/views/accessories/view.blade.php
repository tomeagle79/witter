@extends('layouts.frontend')

@section('title', $accessory->title)

@section('heading')
    {{ $accessory->title }}
@stop

@section('meta_description')
    Witter Towbars, {{ $accessory->title }}
@stop

@section('content')
	<div class="container witter-product view-accessory" itemscope itemtype="http://schema.org/Product">
		<div class="row">

			<div class="ol-sm-8 col-md-9 col-md-push-3 accessory-listing">

				<h2 class="no-top-margin" itemprop="name">{{ $accessory->title }}</h2>

				<div class="row">
                    <div class="col-md-4">
                        <div class="bordered img-center">
                            <img src="{{ image_load($accessory->imageUrl) }}" alt="{{ $accessory->title }}" itemprop="image">
                        </div> <!-- .bordered -->
                    </div>
                    <div class="col-md-8">
                        <div class="details" itemprop="description">{!! nl2br($accessory->details) !!}</div> <!-- .details -->

                        <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

                            @php
                                $save = empty($accessory->retailPrice) ? 0 : $accessory->retailPrice - $accessory->price;
                            @endphp

                            @if($save > 0)

                                <div class="retail-price">RRP &pound;{{ price($accessory->retailPrice) }}</div>

                                Buy now for &pound;<span itemprop="price">{{ price($accessory->price) }}</span>

                                <div class="retail-price">Save &pound;{{ price($save) }}</div>

                            @else

                                &pound;<span itemprop="price">{{ price($accessory->price) }}</span>

                            @endif

                            <meta itemprop="priceCurrency" content="GBP" />
                            <meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
                            @if($accessory->isSellable && $accessory->inStock)
                                <link itemprop="availability" href="http://schema.org/InStock"/>
                            @else
                                <link itemprop="availability" href="http://schema.org/OutOfStock"/>
                            @endif
                        </div> <!-- .price -->

                        @if($accessory->isSellable && $accessory->inStock)
                            <a href="{{ route('add_to_cart', [
                                'type' => 'accessory',
                                'partNo' => $accessory->partNo
                            ]) }}" class="btn btn-primary">Add to Cart</a>
                        @else
                            <h4>Out of stock</h4>
                            <p>This item is currently out of stock</p>
                        @endif
                    </div> <!-- .col-md-9 -->
                </div> <!-- .row -->

                @if($accessory->isFittable)

                <div class="row view-accessory-fittings">
                    <div class="col-md-8 col-md-offset-4">
                        <div class="process">
                            <form action="{{ route('add_to_cart') }}" method="post" id="order-form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="bordered fitting" id="book-scrollto">

                                    <h3>Book your fitting for an extra £{{ price($accessory->fittingPrice) }}</h3>

                                    <div class="find-fitter-form">
                                        @if(empty($appointment_exists))

                                            <p class="uppercase">
                                                Enter your postcode to find your nearest fitter, and view available booking dates.
                                            </p> <!-- .uppercase -->

                                            <div class="alert alert-danger" role="alert" id="no-postcode" style="display: none;">
                                                <p>Please enter a postcode</p>
                                            </div>

                                            <div class="alert alert-danger" role="alert" id="error" style="display: none;">
                                                <p></p>
                                            </div>

                                            <div class="alert alert-info" role="alert" id="loading" style="display: none;">
                                                <p>Loading...</p>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-8 col-md-7 col-sm-6">
                                                    <input type="text" name="postcode" id="postcode" value="" class="form-control" placeholder="Postcode">
                                                </div> <!-- .col-md-9 -->
                                                <div class="col-lg-4 col-md-5 col-sm-6">
                                                    <button type="button" class="btn btn-primary btn-block btn-secondary-double-line" id="find-fitter">Find Your<br>Fitter</button>
                                                </div> <!-- .col-md-3 -->
                                            </div> <!-- .row -->

                                            <div class="alert alert-info" role="alert" id="loading" style="display: none;">
                                                <p>Loading...</p>
                                            </div>

                                            <div class="alert alert-info" role="alert" id="reload" style="display: none;">
                                                <p>
                                                    If you selected the wrong dates <a href="">start the process again</a>.
                                                </p>
                                            </div>

                                        @else
                                            <p class="uppercase">
                                                We simply add to the fitting to your existing appointment
                                            </p> <!-- .uppercase -->

                                            <small>
                                                Fitting at <strong>{{ $appointment_exists['address'] }}</strong><br>
                                                Fitting on <strong>{{ date('jS F', strtotime($appointment_exists['appointmentDate'])) }}</strong> at <strong>{{ $appointment_exists['appointmentTime'] }}</strong>
                                            </small>
                                        @endif


                                        <div id="selected-fitter"></div>
                                    </div> <!-- .find-fitter-form -->
                                </div> <!-- .bordered -->

                                <div class="bordered final @if(empty($appointment_exists)) greyed @endif">
                                    <h3>Your All-In-One Price</h3>
                                    <p>Includes </p>
                                    <ul>
                                        <li>{{ $accessory->title }}</li>
                                        <li>Vehicle fitting</li>
                                    </ul>

                                    <div class="terms">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="terms" value="1" required>
                                                I agree to Witter's <a href="/terms-and-conditions" target="_blank">Terms &amp; Conditions</a>
                                            </label>
                                        </div> <!-- .checkbox -->
                                    </div> <!-- .terms -->
                                    <div class="price-bar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="price">&pound;<span></span></span>
                                            </div> <!-- .col-md-6 -->
                                            <div class="col-md-6">
                                                <div class="btn-table">

                                                    <button type="submit" class="btn btn-secondary btn-block btn-lg" id="order-btn">Buy Now</button>
                                                </div> <!-- .btn-table -->
                                            </div> <!-- .col-md-6 -->
                                        </div> <!-- .row -->
                                    </div> <!-- .price-bar -->
                                </div> <!-- .bordered .final .greyed -->

                                <input type="hidden" name="price" value="{{ $accessory->price }}">
                                <input type="hidden" name="fittingPrice" value="{{ $accessory->fittingPrice }}">
                                <input type="hidden" name="type" id="fld_type" value="accessory-fitted">

                                <input type="hidden" name="slug" id="fld_slug" value="{{ $accessory->partNo }}">
                                <input type="hidden" name="registrationDate" id="fld_registration_date" value="{{ $registration_date ?? '' }}">

                                @if(empty($appointment_exists))
                                    <input type="hidden" name="partNo" id="fld_partNo" value="">
                                    <input type="hidden" name="appointmentDate" id="fld_appointmentDate" value="">
                                    <input type="hidden" name="appointmentTime" id="fld_appointmentTime" value="">
                                    <input type="hidden" name="appointmentId" id="fld_appointmentId" value="">
                                    <input type="hidden" name="partnerId" id="fld_partnerId" value="">
                                    <input type="hidden" name="address" id="fld_address" value="">
                                    <input type="hidden" name="fitterType" id="fld_fitterType" value="">
                                @else
                                    <input type="hidden" name="partNo" id="fld_partNo" value="{{ $accessory->partNo }}">
                                    <input type="hidden" name="appointmentDate" id="fld_appointmentDate" value="{{ $appointment_exists['appointmentDate'] }}">
                                    <input type="hidden" name="appointmentTime" id="fld_appointmentTime" value="{{ $appointment_exists['appointmentTime'] }}">
                                    <input type="hidden" name="appointmentId" id="fld_appointmentId" value="{{ $appointment_exists['appointmentId'] }}">
                                    <input type="hidden" name="partnerId" id="fld_partnerId" value="{{ $appointment_exists['partnerId'] }}">
                                    <input type="hidden" name="address" id="fld_address" value="{{ $appointment_exists['address'] }}">
                                    <input type="hidden" name="fitterType" id="fld_fitterType" value="{{ $appointment_exists['fitterType'] }}">
                                    <input type="hidden" name="postcode" id="fld_postcode" value="{{ $appointment_exists['customer_postcode'] }}">

                                @endif

                                <!-- book fitting -->
                            </form>
                        </div> <!-- .process -->
                    </div> <!-- .col-md-9 -->
                </div> <!-- .row -->
                @endif

                <h3>Additional Information</h3>

                <div class="bordered">
                    {!! nl2br($accessory->moreDetails) !!}
                </div> <!-- .bordered -->

			</div> <!-- .col-md-9 -->
			<div class="col-sm-4 col-md-3 col-md-pull-9">

				@include('partials.sidebar-partclass-menu')

			</div> <!-- .col-md-3 -->
		</div> <!-- .row -->
	</div> <!-- .container -->

@if($accessory->isFittable)
    <div class="modal fade find-your-fitter" tabindex="-1" role="dialog" id="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
                    <h4 class="modal-title">Select a fitting location</h4>
                </div> <!-- .modal-header -->
                <div class="modal-body">

                </div> <!-- .modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endif

@stop


@section('scripts')
    <script>

        @include('partials.analytics.product-view', [
            'product' => $accessory,
            'category' => 'Accessories'
        ])

        @if($accessory->isFittable)
            $(document).ready(function(){
                var partNo = '{{ $accessory->partNo }}';


                calculatePrice();

                // Catch enter press
                $('#postcode').on('keypress', function(e) {
                    var code = e.keyCode || e.which;
                    if(code == 13) { //Enter keycode
                        $('#find-fitter').click();
                        return false;
                    }
                });

                $('.process').on('click', 'label input[type=radio]', function(){
                    $('.process label input[type=radio]').each(function(){
                        if($(this).is(':checked')) {
                            $(this).parents('label').addClass('active');
                        } else {
                            $(this).parents('label').removeClass('active');
                        }
                    });

                    // Calculate total price
                    calculatePrice();
                });

                $('#find-fitter').on('click', function() {
                    var postcode = $('#postcode').val();


                    $('#no-postcode, #no-kit, #loading, #error').hide();

                    if(!postcode.length) {
                        $('#no-postcode').fadeIn('slow');
                    } else {
                        $('#loading').fadeIn('slow');

                        $.ajax({
                            type: 'POST',
                            url: "{{ route('appointments.ajax_list_accessory_fitters')}}",
                            data: {
                                'postcode': postcode,
                                'partNo': partNo,

                            },
                            success: function(result){
                                $('#loading').fadeOut('slow');

                                if(result.success == false) {
                                    $('#error p').html(result.errorMessage);
                                    $('#error').fadeIn('slow');
                                } else {
                                    // Fitting process has begun so readonly everything - client request
                                    $('input[type=radio]:not(:checked)').prop('disabled', true);
                                    $('#reload').fadeIn('slow');

                                    $('#modal .modal-content .modal-body').html(result.modal);
                                    $('#modal').modal('show');
                                }
                            },
                            error: function(){
                                alert('Sorry, something went wrong');
                            }
                        });
                    }
                });
            });

            function calculatePrice()
            {

                var base = parseFloat({{ $accessory->price }});

                var fitting = parseFloat({{ $accessory->fittingPrice }});

                var total = base + fitting;

                total = total.toFixed(2);

                $('.final .price-bar .price span').html(total);
            }
        @endif
    </script>
@stop

