<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
 */

// Include 301 redirects
include '301.php';

Route::group(['middleware' => ['reckless']], function () {
    Route::get('/migrate', ['uses' => 'RecklessController@migrate']);
    Route::get('/clear-cache', ['uses' => 'RecklessController@clear_cache']);
    Route::get('/tweets', ['uses' => 'RecklessController@tweets']);
    Route::get('/test', ['uses' => 'TestController@test']);
});

// Routes for trade.
Route::group(['middleware' => ['web', 'frontend', 'cart_total_items'], 'namespace' => 'Trade'], function () {

    Route::get('/share-quote', ['uses' => 'QuoteController@create', 'as' => 'quote.create']);

    Route::post('/share-quote', ['uses' => 'QuoteController@store', 'as' => 'quote.store']);

    Route::get('/quote-sent', ['uses' => 'QuoteController@show', 'as' => 'quote.show']);
});

Route::group(['middleware' => ['web', 'frontend', 'cart_total_items']], function () {

    // Retrieve fitters quote.
    Route::get('/my-quote/{quote_ref}', ['uses' => 'Trade\\QuoteController@retrieve', 'as' => 'quote.retrieve']);

    // PayPal routes.
    Route::any('paypal-payment', ['uses' => 'CheckoutController@create_payment_paypal', 'as' => 'paypal-payment']);

    // Pay by Phone routes
    Route::post('checkout/phone-prepare', ['uses' => 'CheckoutController@phone_prepare', 'as' => 'checkout.phone_prepare']);
    Route::post('checkout/phone-request', ['uses' => 'CheckoutController@phone_request', 'as' => 'checkout.phone_request']);

    // Login Routes...
    Route::get('/' . config('settings.admin.loginurl'), ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('/' . config('settings.admin.loginurl'), ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
    Route::post('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

    // Password Reset Routes...
    Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);

    Route::get('/', ['uses' => 'WelcomeController@index', 'as' => 'home']);
    Route::get('/index2', ['uses' => 'WelcomeController@indexTwo', 'as' => 'home2']);

    Route::get('contact-us', ['uses' => 'ContactController@index', 'as' => 'contact']);
    Route::post('contact-us/submit', ['uses' => 'ContactController@submit', 'as' => 'contact.submit']);

    Route::post('contact-us/product-enquiry', ['uses' => 'ContactController@product_enquiry', 'as' => 'contact.submit_product_enquiry']);

    // handler for the About page.
    Route::any('about', ['uses' => 'PageController@about', 'as' => 'about']);

    // handler for the Guide pages.
    Route::any('how-to-guide-towbars', ['uses' => 'PageController@towbar_guide', 'as' => 'towbar_guide']);
    Route::any('how-to-guide-bike-racks', ['uses' => 'PageController@bike_rack_guide', 'as' => 'bike_rack_guide']);
    Route::any('what-can-i-tow', ['uses' => 'PageController@what_can_i_tow', 'as' => 'what_can_i_tow']);
    Route::any('will-my-caravan-mover-fit', ['uses' => 'PageController@will_my_caravan_mover_fit', 'as' => 'will_my_caravan_mover_fit']);
    Route::any('land-rover-towing-guide', ['uses' => 'PageController@land_rover_towing_guide', 'as' => 'land_rover_towing_guide']);
    Route::any('fixed-or-detachable-towbar', ['uses' => 'PageController@fixed_or_detachable_towbar', 'as' => 'fixed_or_detachable_towbar']);

    Route::get('service-centres', ['uses' => 'ServiceCentreController@index', 'as' => 'service-centres']);
    Route::get('service-centres/{slug}', ['as' => 'service-centres.show', function ($slug) {

        if (in_array($slug, config('settings.fitting_centre.regions'))) {
            return app()->make('App\Http\Controllers\ServiceCentreController')->callAction('region_index', ['slug' => $slug]);
        } else {
            return app()->make('App\Http\Controllers\ServiceCentreController')->callAction('show', ['slug' => $slug]);
        }
    }]);

    /* Suspensions */
    // ajax calls for suspensions
    Route::get('air-suspension-kits/ajax_manufacturers', ['uses' => 'SuspensionVehicleAjaxController@ajax_manufacturers', 'as' => 'suspensions.ajax_manufacturers']);
    Route::get('air-suspension-kits/ajax_models/{id}', ['uses' => 'SuspensionVehicleAjaxController@ajax_models', 'as' => 'suspensions.ajax_models']);
    Route::get('air-suspension-kits/ajax_bodies/{id}', ['uses' => 'SuspensionVehicleAjaxController@ajax_bodies', 'as' => 'suspensions.ajax_bodies']);

    // Display single suspension kit
    Route::any('air-suspension-kits/product/{partNo}', ['uses' => 'SuspensionController@view', 'as' => 'suspensions.view']);

    // handler for suspensions
    Route::get('air-suspension-kits', ['uses' => 'SuspensionController@index', 'as' => 'suspensions']);
    Route::get('air-suspension-kits/{manufacturer_slug}', ['uses' => 'SuspensionController@model_list', 'as' => 'suspensions.model_list']);
    Route::get('air-suspension-kits/{manufacturer_slug}/{model_slug}', ['uses' => 'SuspensionController@vehicle_list', 'as' => 'suspensions.models.vehicle_list']);
    Route::get('air-suspension-kits/{manufacturer_slug}/{model_slug}/{vehicle_slug}', ['uses' => 'SuspensionController@product_list', 'as' => 'suspensions.models.vehicles.products']);

    // When users post the dropdown forms redirect them
    Route::post('air-suspension-kits', ['uses' => 'SuspensionVehicleAjaxController@index_submit', 'as' => 'suspensions.index_submit']);
    Route::post('air-suspension-kits/{manufacturer_slug}', ['uses' => 'SuspensionVehicleAjaxController@model_list_submit', 'as' => 'suspensions.model_list_submit']);
    Route::post('air-suspension-kits/{manufacturer_slug}/{model_slug}', ['uses' => 'SuspensionVehicleAjaxController@body_registration_submit', 'as' => 'suspensions.body_registration_submit']);

    /* Towbars */
    // Number plate search
    Route::get('/registration-lookup/number-plate', ['uses' => 'RegistrationLookupController@towbar_search', 'as' => 'search.numberplate']);

    // handler for towbars
    Route::get('towbars', ['uses' => 'TowbarController@index', 'as' => 'towbars']);

    // New design for the towbars index page.
    Route::get('towbars2', ['uses' => 'TowbarController@index2', 'as' => 'towbars2']);

    // When users post the dropdown forms redirect them
    Route::post('towbars', ['uses' => 'VehicleAjaxController@index_submit', 'as' => 'towbars.index_submit']);
    Route::post('towbars/{manufacturer_slug}', ['uses' => 'VehicleAjaxController@model_list_submit', 'as' => 'towbars.model_list_submit']);
    Route::post('towbars/{manufacturer_slug}/{model_slug}', ['uses' => 'VehicleAjaxController@body_registration_submit', 'as' => 'towbars.body_registration_submit']);

    // ajax calls
    Route::get('vehicles/ajax_manufacturers', ['uses' => 'VehicleAjaxController@ajax_manufacturers', 'as' => 'vehicles.ajax_manufacturers']);
    Route::get('vehicles/ajax_models/{id}', ['uses' => 'VehicleAjaxController@ajax_models', 'as' => 'vehicles.ajax_models']);
    Route::get('vehicles/ajax_bodies/{id}', ['uses' => 'VehicleAjaxController@ajax_bodies', 'as' => 'vehicles.ajax_bodies']);
    Route::get('vehicles/ajax_registrations/{id}', ['uses' => 'VehicleAjaxController@ajax_registrations', 'as' => 'vehicles.ajax_registrations']);

    // Fnd by manufacturer, then model, then body type, then varient
    Route::get('towbars/{manufacturer_slug}', ['uses' => 'TowbarController@model_list', 'as' => 'towbars.model_list']);
    Route::get('towbars/{manufacturer_slug}/{model_slug}', ['uses' => 'TowbarController@body_list', 'as' => 'towbars.model_list.body_list']);
    Route::get('towbars/{manufacturer_slug}/{model_slug}/{body_slug}', ['uses' => 'TowbarController@registration_list', 'as' => 'towbars.model_list.body_list.registration_list']);
    Route::get('towbars/{manufacturer_slug}/{model_slug}/{body_slug}/{registration_slug}', ['uses' => 'TowbarController@vehicle_list', 'as' => 'towbars.model_list.body_list.registration_list.vehicle_list']);

    // Towbars for a given vehicle ID
    Route::get('towbars/{manufacturer_slug}/{model_slug}/{body_slug}/{registration_slug}/{vehicle_slug}', ['uses' => 'TowbarController@vehicle_towbars', 'as' => 'towbars.by_selection']);
    Route::get('towbars-by-reg/{vehicle_id}/{registration_date?}', ['uses' => 'TowbarController@vehicle_towbars_by_reg', 'as' => 'towbars.vehicle_towbars_by_reg']);

    // View towbar
    Route::get('towbar/view/{towbar_slug}/{registration_date?}', ['uses' => 'TowbarController@view', 'as' => 'towbars.view']);

    // Stock notifications, currently only for towbars
    Route::post('stock-notifications', ['uses' => 'StockNotificationController@add_notification', 'as' => 'stock_notifications']);

    /* Electrical kits */
    Route::get('/e-registration-lookup/number-plate', ['uses' => 'RegistrationLookupController@electrics_search', 'as' => 'electrics.search.numberplate']);

    Route::get('electrical-kits', ['uses' => 'ElectricController@index', 'as' => 'electrics']);

    // When users post the dropdown forms redirect them
    Route::post('electrical-kits', ['uses' => 'VehicleAjaxController@index_submit', 'as' => 'electrical-kits.index_submit']);
    Route::post('electrical-kits/{manufacturer_slug}', ['uses' => 'VehicleAjaxController@model_list_submit', 'as' => 'electrical-kits.model_list_submit']);
    Route::post('electrical-kits/{manufacturer_slug}/{model_slug}', ['uses' => 'VehicleAjaxController@body_registration_submit', 'as' => 'electrical-kits.body_registration_submit']);

    // Fnd by manufacturer, then model, then body type, then varient
    Route::get('electrical-kits/{manufacturer_slug}', ['uses' => 'ElectricController@model_list', 'as' => 'electrical-kits.model_list']);
    Route::get('electrical-kits/{manufacturer_slug}/{model_slug}', ['uses' => 'ElectricController@body_list', 'as' => 'electrical-kits.model_list.body_list']);
    Route::get('electrical-kits/{manufacturer_slug}/{model_slug}/{body_slug}', ['uses' => 'ElectricController@registration_list', 'as' => 'electrical-kits.model_list.body_list.registration_list']);
    Route::get('electrical-kits/{manufacturer_slug}/{model_slug}/{body_slug}/{registration_slug}', ['uses' => 'ElectricController@vehicle_list', 'as' => 'electrical-kits.model_list.body_list.registration_list.vehicle_list']);

    // Electrical kits for a given vehicle ID
    Route::get('electrical-kits/{manufacturer_slug}/{model_slug}/{body_slug}/{registration_slug}/{vehicle_slug}', ['uses' => 'ElectricController@vehicle_kits', 'as' => 'electrics.by_selection']);
    Route::get('electrical-kits-by-reg/{vehicle_id}/{registration_date?}', ['uses' => 'ElectricController@vehicle_kits_by_reg', 'as' => 'electrics.vehicle_towbars_by_reg']);

    // View kit
    Route::get('electrical-kit/view/{partNo}', ['uses' => 'ElectricController@view', 'as' => 'electrics.view']);

    /* Roof Systems */
    Route::any('roof-systems', ['uses' => 'RoofSystemsController@index', 'as' => 'roof-systems']);

    Route::any('roof-systems/roof-bars', ['uses' => 'Roofsystems\RoofBarsController@index', 'as' => 'roof-systems.roof-bars']);
    Route::any('roof-systems/roof-bars/{manufacturer_slug}', ['uses' => 'Roofsystems\RoofBarsController@models', 'as' => 'roof-systems.roof-bars.models']);
    Route::any('roof-systems/roof-bars/{manufacturer_slug}/{model_slug}', ['uses' => 'Roofsystems\RoofBarsController@vehicles', 'as' => 'roof-systems.roof-bars.models.vehicles']);
    Route::any('roof-systems/roof-bars/{manufacturer_slug}/{model_slug}/{vehicle_slug}', ['uses' => 'Roofsystems\RoofBarsController@products', 'as' => 'roof-systems.roof-bars.models.vehicles.products']);
    Route::any('roof-systems/roof-bars-view/{partNo}', ['uses' => 'Roofsystems\RoofBarsController@view', 'as' => 'roof-systems.roof-bars.view']);

    Route::any('roof-systems/roof-decks', ['uses' => 'Roofsystems\RoofDecksController@index', 'as' => 'roof-systems.roof-decks']);
    Route::any('roof-systems/roof-decks/{manufacturer_slug}', ['uses' => 'Roofsystems\RoofDecksController@models', 'as' => 'roof-systems.roof-decks.models']);
    Route::any('roof-systems/roof-decks/{manufacturer_slug}/{model_slug}', ['uses' => 'Roofsystems\RoofDecksController@vehicles', 'as' => 'roof-systems.roof-decks.models.vehicles']);
    Route::any('roof-systems/roof-decks/{manufacturer_slug}/{model_slug}/{vehicle_slug}', ['uses' => 'Roofsystems\RoofDecksController@products', 'as' => 'roof-systems.roof-decks.models.vehicles.products']);
    Route::any('roof-systems/roof-decks-view/{partNo}', ['uses' => 'Roofsystems\RoofDecksController@view', 'as' => 'roof-systems.roof-decks.view']);

    //Route::any('roof-systems/roof-accessories', ['uses' => 'RoofSystemsController@accessories', 'as' => 'roof-systems.accessories']);
    //Route::any('roof-systems/roof-accessories/{slug}', ['uses' => 'RoofSystemsController@accessories_category', 'as' => 'roof-systems.accessories.multi']);
    Route::any('roof-systems/accessories', ['uses' => 'Roofsystems\RoofAccessoriesController@index', 'as' => 'roof-systems.accessories']);
    Route::any('roof-systems/accessories/{manufacturer_slug}', ['uses' => 'Roofsystems\RoofAccessoriesController@models', 'as' => 'roof-systems.accessories.models']);
    Route::any('roof-systems/accessories/{manufacturer_slug}/{model_slug}', ['uses' => 'Roofsystems\RoofAccessoriesController@vehicles', 'as' => 'roof-systems.accessories.models.vehicles']);
    Route::any('roof-systems/accessories/{manufacturer_slug}/{model_slug}/{vehicle_slug}', ['uses' => 'Roofsystems\RoofAccessoriesController@products', 'as' => 'roof-systems.accessories.models.vehicles.products']);
    Route::any('roof-systems/accessory-view/{partNo}', ['uses' => 'Roofsystems\RoofAccessoriesController@view', 'as' => 'roof-systems.accessories.view']);

    Route::any('roof-systems/by-manufacturer/{manufacturer_slug}', ['uses' => 'RoofSystemsController@models', 'as' => 'roof-systems.models']);
    Route::any('roof-systems/by-manufacturer/{manufacturer_slug}/{model_slug}', ['uses' => 'RoofSystemsController@vehicles', 'as' => 'roof-systems.models.vehicles']);
    Route::any('roof-systems/by-manufacturer/{manufacturer_slug}/{model_slug}/{vehicle_slug}', ['uses' => 'RoofSystemsController@products', 'as' => 'roof-systems.models.vehicles.products']);

    /** Accessories **/
    // handler for accessories page
    Route::any('accessories', ['uses' => 'AccessoryController@index', 'as' => 'accessories']);
    # Route::any('accessories/{category_slug}', ['uses' => 'AccessoryController@category', 'as' => 'accessories.multi']);
    Route::any('accessories/{category_slug}/{subcategory_slug}', ['uses' => 'AccessoryController@subcategory', 'as' => 'accessories.subcategory']);
    # Route::any('accessories/{accessory_slug}', ['uses' => 'AccessoryController@view', 'as' => 'accessories.multi']);

    Route::any('accessories/{slug}', ['as' => 'accessories.multi', function ($slug) {
        // Check if it's a valid part
        $accessory = \App\Models\Api\Accessory::get_single_by_slug($slug);

        if (!empty($accessory->partNo)) {

            $app = app();

            return $app->make('App\Http\Controllers\AccessoryController')->callAction('view', ['accessory_slug' => $slug]);

        } else {

            $app = app();
            return $app->make('App\Http\Controllers\AccessoryController')->callAction('category', ['category_slug' => $slug]);

        }
    }]);

    // Direct view for caravon movers
    Route::any('caravan-movers', ['uses' => 'CaravanMoversController@index', 'as' => 'caravan_movers']);
    Route::any('caravan-movers/{part_number}', ['uses' => 'CaravanMoversController@view', 'as' => 'caravan_movers.view']);

    // Bike carriers handler
    Route::any('bike-racks', ['uses' => 'BikeCarrierController@index', 'as' => 'bike-racks']);
    Route::any('bike-racks/product_notify', ['uses' => 'BikeCarrierController@email_notify', 'as' => 'product.email_notify']);
    Route::any('bike-racks/{part_number}', ['uses' => 'BikeCarrierController@view', 'as' => 'bike_rack.view']);

    // Fitters and appointments
    Route::post('appointments/ajax-list-fitters', ['uses' => 'AppointmentController@ajax_list_fitters', 'as' => 'appointments.ajax_list_fitters'])->defaults('dates_route', 'appointments.ajax_list_dates');
    Route::post('cart/appointments/ajax-list-fitters', ['uses' => 'AppointmentController@ajax_list_fitters', 'as' => 'appointments.cart_ajax_list_fitters'])->defaults('dates_route', 'appointments.cart_ajax_list_dates');

    Route::post('appointments/ajax-list-dates', ['uses' => 'AppointmentController@ajax_list_dates', 'as' => 'appointments.ajax_list_dates'])->defaults('view_template', 'default');
    Route::post('cart/appointments/ajax-list-dates', ['uses' => 'AppointmentController@ajax_list_dates', 'as' => 'appointments.cart_ajax_list_dates'])->defaults('view_template', 'cart');

    // Fitters and appointments
    Route::post('appointments/ajax-list-accessory-fitters', ['uses' => 'AppointmentController@ajax_list_accessory_fitters', 'as' => 'appointments.ajax_list_accessory_fitters'])->defaults('dates_route', 'appointments.ajax_list_dates');

    Route::post('cart/appointments/ajax-list-accessory-fitters', ['uses' => 'AppointmentController@ajax_list_accessory_fitters', 'as' => 'appointments.cart_ajax_list_accessory_fitters'])->defaults('dates_route', 'appointments.cart_ajax_list_dates');

    // Add to cart
    Route::any('add_to_cart', ['uses' => 'CartController@add', 'as' => 'add_to_cart']);
    Route::get('cart', ['uses' => 'CartController@cart', 'as' => 'cart']);
    Route::get('cart/remove', ['uses' => 'CartController@remove', 'as' => 'cart.remove']);
    Route::get('cart/remove_towbar', ['uses' => 'CartController@remove_towbar', 'as' => 'cart.remove_towbar']);
    Route::post('cart/apply-promo', ['uses' => 'CartController@apply_promo', 'as' => 'cart.apply_promo']);
    Route::get('cart/remove-promo', ['uses' => 'CartController@remove_promo', 'as' => 'cart.remove_promo']);
    Route::post('cart/adjust-appointment', ['uses' => 'CartController@adjustAppointment', 'as' => 'cart.adjust_appointment']);

    // Checkout
    Route::get('checkout', ['uses' => 'CheckoutController@cardholder_details', 'as' => 'checkout']);
    Route::post('checkout/section1_submit', ['uses' => 'CheckoutController@section1_submit', 'as' => 'checkout.section1_submit']);

    Route::get('checkout/section2', ['uses' => 'CheckoutController@delivery_details', 'as' => 'checkout.section2']);
    Route::post('checkout/section2_submit', ['uses' => 'CheckoutController@section2_submit', 'as' => 'checkout.section2_submit']);

    // Vehicle details recored for trackers etc.
    Route::get('checkout/vehicle_details', ['uses' => 'CheckoutController@vehicleDetails', 'as' => 'checkout.sectionVehicle']);
    Route::post('checkout/vehicle_details', ['uses' => 'CheckoutController@vehicleDetailsSubmit', 'as' => 'checkout.sectionVehicleSubmit']);

    Route::get('checkout/section3', ['uses' => 'CheckoutController@payment_options', 'as' => 'checkout.section3']);
    Route::post('checkout/section3_submit', ['uses' => 'CheckoutController@section3_submit', 'as' => 'checkout.section3_submit']);

    Route::post('checkout/charge', ['uses' => 'CheckoutController@charge', 'as' => 'checkout.charge']);

    Route::any('checkout/complete', ['uses' => 'CheckoutController@complete', 'as' => 'checkout.complete']);
    Route::any('checkout/has-analytics-fired/{order_id}', ['uses' => 'CheckoutController@has_analytics_fired', 'as' => 'checkout.has_analytics_fired']);

    // Postcode lookup
    Route::post('postcode/lookup', ['uses' => 'PostcodeController@lookup', 'as' => 'postcode.lookup']);
    Route::post('postcode/addresses', ['uses' => 'PostcodeController@get_addresses', 'as' => 'postcode.get_addresses']);

    // Product Registration
    Route::get('product-registration', ['uses' => 'ProductRegistrationController@index', 'as' => 'product_registration']);
    Route::get('product-registration/towbar', ['uses' => 'ProductRegistrationController@towbar', 'as' => 'product_registration.towbar']);
    Route::post('product-registration/towbar', ['uses' => 'ProductRegistrationController@towbar_process', 'as' => 'product_registration.towbar.process']);
    Route::get('product-registration/cycle-carrier', ['uses' => 'ProductRegistrationController@cycle_carrier', 'as' => 'product_registration.cycle_carrier']);
    Route::post('product-registration/cycle-carrier', ['uses' => 'ProductRegistrationController@cycle_carrier_process', 'as' => 'product_registration.cycle_carrier.process']);

    // Product Returns
    Route::get('product-returns', ['uses' => 'ProductReturnsController@returns', 'as' => 'returns']);
    Route::post('product-returns', ['uses' => 'ProductReturnsController@returns_process', 'as' => 'returns.process']);

    // Tracker pages
    Route::get('vehicle-trackers', ['uses' => 'TrackerController@index', 'as' => 'trackers.index']);

    /** Frontend that use slug model **/

    // handler for Help Articles and Help Categories
    Route::any('help-advice', ['uses' => 'HelpAdviceController@index', 'as' => 'help_articles']);
    Route::any('help-advice/{slug}', ['as' => 'help_article_dynamic_route', function ($slug) {
        // Check if it's a Help Article
        $url = \App\Models\HelpArticle::where('url_slug', $slug)->first();
        if (!is_null($url)) {
            $app = app();
            return $app->make('App\Http\Controllers\HelpAdviceController')->callAction('show', ['url_slug' => $url->url_slug, 'id' => $url->id]);
        } else {
            // Check if it's a category
            $url = \App\Models\HelpCategory::where('url_slug', $slug)->first();

            if (!is_null($url)) {
                $app = app();
                return $app->make('App\Http\Controllers\HelpAdviceController')->callAction('viewCategory', ['url_slug' => $url->url_slug, 'id' => $url->id]);
            } else {
                abort(404);
            }
        }
    }]);

    // handler for FAQs and FAQ categories
    Route::any('faqs', ['uses' => 'FaqController@index', 'as' => 'faqs']);
    Route::any('faqs/{slug}', ['as' => 'faq_dynamic_route', function ($slug) {
        // Check if it's a faq psot
        $url = \App\Models\Faq::where('url_slug', $slug)->first();
        if (!is_null($url)) {
            $app = app();
            return $app->make('App\Http\Controllers\FaqController')->callAction('viewPost', ['url_slug' => $url->url_slug, 'id' => $url->id]);
        } else {
            // Check if it's a category
            $url = \App\Models\FaqCategory::where('url_slug', $slug)->first();

            if (!is_null($url)) {
                $app = app();
                return $app->make('App\Http\Controllers\FaqController')->callAction('viewCategory', ['url_slug' => $url->url_slug, 'id' => $url->id]);
            } else {
                abort(404);
            }
        }
    }]);

    // Careers
    Route::any('careers', function () {
        return redirect('about');
    });
    Route::any('careers/{slug}', ['as' => 'careers_dynamic_route', function ($slug) {
        // Check if it's a Help Article
        $url = \App\Models\Career::where('url_slug', $slug)->first();
        if (!is_null($url)) {
            $app = app();
            return $app->make('App\Http\Controllers\CareerController')->callAction('show', ['url_slug' => $url->url_slug, 'id' => $url->id]);
        } else {
            abort(404);
        }
    }]);

    // handler for blogs and blog categories
    Route::any('blog', ['as' => 'blog', 'uses' => 'BlogController@index']);
    Route::any('blog/{slug}', ['as' => 'blog_dynamic_route', function ($slug) {
        // Check if it's a blog psot
        $url = \App\Models\BlogPost::where('url_slug', $slug)->first();
        if (!is_null($url)) {
            $app = app();
            return $app->make('App\Http\Controllers\BlogController')->callAction('viewPost', ['url_slug' => $url->url_slug, 'id' => $url->id]);
        } else {
            // Check if it's a category
            $url = \App\Models\BlogCategory::where('url_slug', $slug)->first();

            if (!is_null($url)) {
                $app = app();
                return $app->make('App\Http\Controllers\BlogController')->callAction('viewCategory', ['url_slug' => $url->url_slug, 'id' => $url->id]);
            } else {
                abort(404);
            }
        }
    }]);

}); // Frontend

/** BACKEND **/
Route::group(['as' => 'backend', 'middleware' => ['auth'], 'namespace' => 'Backend', 'prefix' => config('settings.admin.slug')], function () {
    Route::get('/', function () {
        return Redirect::backend('dashboard');
    });

    Route::get('/dashboard', ['uses' => 'DashboardController@index']);

    // Admin users
    Route::get('/admin_users', ['uses' => 'AdminUserController@index', 'as' => 'admin.users']);
    Route::get('/admin_users/index_datatables', 'AdminUserController@indexDatatable');
    Route::get('/admin_users/edit/{id}', ['uses' => 'AdminUserController@edit', 'as' => 'admin.users.edit']);
    Route::post('/admin_users/edit/{id}', 'AdminUserController@update');
    Route::get('/admin_users/create', ['uses' => 'AdminUserController@create', 'as' => 'admin.users.add']);
    Route::post('/admin_users/create', 'AdminUserController@store');
    Route::get('/admin_users/delete/{id}', ['uses' => 'AdminUserController@destroy', 'as' => 'admin.users.delete']);

    // Blog Post edit
    Route::get('/blog_posts', ['uses' => 'BlogPostController@index', 'as' => 'admin.blog_posts']);
    Route::get('/blog_posts/index_datatables', 'BlogPostController@indexDatatable');
    Route::get('/blog_posts/edit/{id}', ['uses' => 'BlogPostController@edit', 'as' => 'admin.blog_posts.edit']);
    Route::post('/blog_posts/edit/{id}', 'BlogPostController@update');
    Route::get('/blog_posts/create', ['uses' => 'BlogPostController@create', 'as' => 'admin.blog_posts.add']);
    Route::post('/blog_posts/create', 'BlogPostController@store');
    Route::get('/blog_posts/delete/{id}', ['uses' => 'BlogPostController@destroy', 'as' => 'admin.blog_posts.delete']);
    Route::get('/blog_posts/restore/{id}', ['uses' => 'BlogPostController@restore', 'as' => 'admin.blog_posts.restore']);

    // Blog Categories edit
    Route::get('/blog_categories', ['uses' => 'BlogCategoryController@index', 'as' => 'admin.blog_categories']);
    Route::get('/blog_categories/index_datatables', 'BlogCategoryController@indexDatatable');
    Route::get('/blog_categories/edit/{id}', ['uses' => 'BlogCategoryController@edit', 'as' => 'admin.blog_categories.edit']);
    Route::post('/blog_categories/edit/{id}', 'BlogCategoryController@update');
    Route::get('/blog_categories/create', ['uses' => 'BlogCategoryController@create', 'as' => 'admin.blog_categories.add']);
    Route::post('/blog_categories/create', 'BlogCategoryController@store');
    Route::get('/blog_categories/delete/{id}', ['uses' => 'BlogCategoryController@destroy', 'as' => 'admin.blog_categories.delete']);
    Route::get('/blog_categories/restore/{id}', ['uses' => 'BlogCategoryController@restore', 'as' => 'admin.blog_categories.restore']);

    // redactor
    Route::get('/redactor/image_upload', 'RedactorController@image_upload');
    Route::post('/redactor/image_upload', 'RedactorController@image_upload');
    Route::post('/redactor/file_upload', 'RedactorController@file_upload');
    Route::get('/redactor/get_image_json', 'RedactorController@get_image_json');

    // FAQ edit
    Route::get('/faqs', ['uses' => 'FaqController@index', 'as' => 'admin.faqs']);
    Route::get('/faqs/index_datatables', 'FaqController@indexDatatable');
    Route::get('/faqs/edit/{id}', ['uses' => 'FaqController@edit', 'as' => 'admin.faqs.edit']);
    Route::post('/faqs/edit/{id}', 'FaqController@update');
    Route::get('/faqs/create', ['uses' => 'FaqController@create', 'as' => 'admin.faqs.add']);
    Route::post('/faqs/create', 'FaqController@store');
    Route::get('/faqs/delete/{id}', ['uses' => 'FaqController@destroy', 'as' => 'admin.faqs.delete']);
    Route::get('/faqs/restore/{id}', ['uses' => 'FaqController@restore', 'as' => 'admin.faqs.restore']);

    // FAQ Categories edit
    Route::get('/faq_categories', ['uses' => 'FaqCategoryController@index', 'as' => 'admin.faq_categories']);
    Route::get('/faq_categories/index_datatables', 'FaqCategoryController@indexDatatable');
    Route::get('/faq_categories/edit/{id}', ['uses' => 'FaqCategoryController@edit', 'as' => 'admin.faq_categories.edit']);
    Route::post('/faq_categories/edit/{id}', 'FaqCategoryController@update');
    Route::get('/faq_categories/create', ['uses' => 'FaqCategoryController@create', 'as' => 'admin.faq_categories.add']);
    Route::post('/faq_categories/create', 'FaqCategoryController@store');
    Route::get('/faq_categories/delete/{id}', ['uses' => 'FaqCategoryController@destroy', 'as' => 'admin.faq_categories.delete']);
    Route::get('/faq_categories/restore/{id}', ['uses' => 'FaqCategoryController@restore', 'as' => 'admin.faq_categories.restore']);

    // Pages edit
    Route::get('/pages', ['uses' => 'PageController@index', 'as' => 'admin.pages']);
    Route::get('/pages/index_datatables', 'PageController@indexDatatable');
    Route::get('/pages/edit/{id}', ['uses' => 'PageController@edit', 'as' => 'admin.pages.edit']);
    Route::post('/pages/edit/{id}', 'PageController@update');
    Route::get('/pages/create', ['uses' => 'PageController@create', 'as' => 'admin.pages.add']);
    Route::post('/pages/create', 'PageController@store');
    Route::get('/pages/delete/{id}', ['uses' => 'PageController@destroy', 'as' => 'admin.pages.delete']);
    Route::get('/pages/restore/{id}', ['uses' => 'PageController@restore', 'as' => 'admin.pages.restore']);

    // Banners edit
    Route::get('/banners', ['uses' => 'BannerController@index', 'as' => 'admin.banners']);
    Route::get('/banners/index_datatables', 'BannerController@indexDatatable');
    Route::get('/banners/edit/{id}', ['uses' => 'BannerController@edit', 'as' => 'admin.banners.edit']);
    Route::post('/banners/edit/{id}', 'BannerController@update');
    Route::get('/banners/create', ['uses' => 'BannerController@create', 'as' => 'admin.banners.add']);
    Route::post('/banners/create', 'BannerController@store');
    Route::get('/banners/delete/{id}', ['uses' => 'BannerController@destroy', 'as' => 'admin.banners.delete']);
    Route::get('/banners/restore/{id}', ['uses' => 'BannerController@restore', 'as' => 'admin.banners.restore']);

    // Careers
    Route::get('/careers', ['uses' => 'CareerController@index', 'as' => 'admin.careers']);
    Route::get('/careers/index_datatables', 'CareerController@indexDatatable');
    Route::get('/careers/edit/{id}', ['uses' => 'CareerController@edit', 'as' => 'admin.careers.edit']);
    Route::post('/careers/edit/{id}', 'CareerController@update');
    Route::get('/careers/create', ['uses' => 'CareerController@create', 'as' => 'admin.careers.add']);
    Route::post('/careers/create', 'CareerController@store');
    Route::get('/careers/delete/{id}', ['uses' => 'CareerController@destroy', 'as' => 'admin.careers.delete']);
    Route::get('/careers/restore/{id}', ['uses' => 'CareerController@restore', 'as' => 'admin.careers.restore']);

    // Help Post edit
    Route::get('/help_articles', ['uses' => 'HelpArticleController@index', 'as' => 'admin.help_articles']);
    Route::get('/help_articles/index_datatables', 'HelpArticleController@indexDatatable');
    Route::get('/help_articles/edit/{id}', ['uses' => 'HelpArticleController@edit', 'as' => 'admin.help_articles.edit']);
    Route::post('/help_articles/edit/{id}', 'HelpArticleController@update');
    Route::get('/help_articles/create', ['uses' => 'HelpArticleController@create', 'as' => 'admin.help_articles.add']);
    Route::post('/help_articles/create', 'HelpArticleController@store');
    Route::get('/help_articles/delete/{id}', ['uses' => 'HelpArticleController@destroy', 'as' => 'admin.help_articles.delete']);
    Route::get('/help_articles/restore/{id}', ['uses' => 'HelpArticleController@restore', 'as' => 'admin.help_articles.restore']);

    // Help Categories edit
    Route::get('/help_categories', ['uses' => 'HelpCategoryController@index', 'as' => 'admin.help_categories']);
    Route::get('/help_categories/index_datatables', 'HelpCategoryController@indexDatatable');
    Route::get('/help_categories/edit/{id}', ['uses' => 'HelpCategoryController@edit', 'as' => 'admin.help_categories.edit']);
    Route::post('/help_categories/edit/{id}', 'HelpCategoryController@update');
    Route::get('/help_categories/create', ['uses' => 'HelpCategoryController@create', 'as' => 'admin.help_categories.add']);
    Route::post('/help_categories/create', 'HelpCategoryController@store');
    Route::get('/help_categories/delete/{id}', ['uses' => 'HelpCategoryController@destroy', 'as' => 'admin.help_categories.delete']);
    Route::get('/help_categories/restore/{id}', ['uses' => 'HelpCategoryController@restore', 'as' => 'admin.help_categories.restore']);

});

// handler for pages, products and categories.  This must be last in this file as it is lowest priority
Route::any('{slug}', ['as' => 'dynamic_route', function ($slug) {
    $url = \App\Models\UrlSlug::where('url_slug', $slug)->first();
    if (!is_null($url) && !empty($url->url_type)) {
        $app = app();

        switch ($url->url_type) {
            case 'product':
                $controller = $app->make('App\Http\Controllers\ProductsController');
                break;
            case 'category':
                $controller = $app->make('App\Http\Controllers\CategoriesController');
                break;
            case 'page':
                $controller = $app->make('App\Http\Controllers\PageController');
                break;
        }
        return $controller->callAction('view', ['url_slug' => $url->url_slug, 'url_slug_id' => $url->id]);
    } else {
        abort(404);
    }
}]);
