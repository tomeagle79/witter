
<div class="hidden-sm hidden-xs">
    <a href="{{ route('help_articles') }}" class="no-hover">
        <div class="sidebar-findlink sidebar-findlink-secondary">

            <a href="/what-can-i-tow"><h2 class="findlink-title">Find out <span class="slim block">what your car can tow safely</span></h2></a>

            <div class="sidebar-car-link"><span class="sr-only">Click here to find out what your car can tow</span></div>

        </div>
    </a>
</div>
