
<div class="checkout-widget checkout-widget-grey">

    <form action="{{ route('cart.apply_promo') }}" method="post" class="form-inline" id="promo_form">
        {{ csrf_field() }}
        <label  for="discount_code">I have a discount code...</label>
        <div class="row">
            <div class="col-xs-12">
                <input type="text" class="form-control" id="discount_code" name="discount_code" placeholder="Code..." >
                <button type="submit" class="btn btn-primary btn-special"></button>
            </div><!-- /.col-xs-12 -->
        </div><!-- /.row -->
    </form><!-- /#promo_form -->
</div><!-- /.checkout-widget -->

<div class="checkout-widget">
    @if(!empty($cart['promotion']))
        <div class="promos">
            <div class="row">
                <div class="col-xs-5">
                    <p>Subtotal:</p>
                </div><!-- /.col-sm-5 -->
                <div class="col-sm-7 sub-total-value">
                    &pound;{{ price($cart['sub_total']) }}
                </div><!-- /.col-sm-7 -->
            </div><!-- /.row -->
            <hr>
            <div class="row">
                @if($cart['promotion']['promotionCode'] != config('witter.webfit_discount_code'))
                    <div class="col-xs-12">

                    </div><!-- /.col-xs-12 -->
                @endif
                <div class="col-xs-4">
                    <p>Discount:</p>
                </div><!-- /.col-sm-5 -->
                <div class="col-xs-8 sub-total-value">
                    &pound;{{ price($cart['promotion']['discountValue']) }}
                    @if($cart['promotion']['promotionCode'] != config('witter.webfit_discount_code'))
                        <a href="{{ route('cart.remove_promo') }}" class="pull-right"><i class="icon-cross"></i></a>
                    @endif
                </div><!-- /.col-sm-7 -->
                <div class="col-xs-12">
                    {{-- <p>Code: {{ strtoupper($cart['promotion']['promotionCode']) }}</p> --}}
                    <p class="description">
                        {{ $cart['promotion']['promotionDetails'] }}
                    </p><!-- /.description -->
                </div><!-- /.col-xs-12 -->
            </div><!-- /.row -->
            <hr>
        </div><!-- /.promos -->

    @endif
    <p class="grand-total @if(!empty($cart['promotion'])) discount-applied @endif">Total price:</p><!-- /.grand-total -->

    <div class="price-wrapper">
            <p> &pound;<span class="final-price">{{ price($cart['grand_total']) }}</span><span class="vat">Inc. VAT</span></p>
    </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->

    @if(!empty($cart['quote_ref']) && !empty($cart['new_appointment_required']))
        <input type="hidden" id="valid_basket" value="false">
    @else
        <input type="hidden" id="valid_basket" value="true">
    @endif

    @includeIf('theme::cart.partials.buttons')

</div><!-- /.checkout-widget -->

<ul class="checkout-cards">
    <li>
        <img src="/assets/img/cards/visa.png" alt="Pay with Visa at Witter Towbars" width="60px">
    </li>
    <li>
        <img src="/assets/img/cards/mastercard.png" alt="Pay with Mastercard at Witter Towbars" width="60px">
    </li>
    <li>
        <img src="/assets/img/cards/american-express.png" alt="Pay with American Express at Witter Towbars" width="60px">
    </li>
    <li>
        <img src="/assets/img/cards/paypal.png" alt="Pay with PayPal at Witter Towbars" width="60px">
    </li>
</ul><!-- /.cards -->

<div class="checkout-widget checkout-widget-no-bg ">
    @if(empty($cart['towbar']))
    <a href="{{ route('accessories') }}" class="btn btn-standard btn-large bnt-continue-shopping">Continue Shopping</a>
    @else
    <a href="{{ route('accessories.multi', 'cycle-carriers') }}" class="btn btn-standard btn-large bnt-continue-shopping">Continue Shopping</a>
    @endif
</div><!-- /.checkout-widget-no-bg -->

