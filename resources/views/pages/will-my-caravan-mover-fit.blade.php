@extends('layouts.frontend')

@section('title', 'Will my caravan mover fit?')

@section('meta_description')
    Need help choosing the right caravan mover for your caravan? Read our easy to follow caravan mover guide. All you need to know about buying a caravan mover.
@stop

@section('body-class', 'complete-guide will-my-mover-fit')

@section('heading')
    Will my caravan mover fit?
@stop

@section('content')
    <a name="#top" id="top"></a>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <p>Touring caravans come in a variety of shapes and sizes. If you are buying a caravan mover for the first time, one of your main concerns might be, 'will it fit?'</p>
                <p>Because of the wide range of chassis, wheel arrangements and caravan models, there is no 'one size fits all' when it comes to caravan motor movers. But the great thing is, this doesn't mean there isn't a mover for you - it just means you may need to purchase some extra parts to make it fit correctly.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-sm-push-3 col-md-push-3 page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="small" id="manual-vs-powered">Manual caravan mover vs. powered caravan mover</h2>
                        <hr>
                        <p>First of all, you need to determine which caravan mover is best for you. All of our caravan movers are operated by a remote controll. Before you can move your caravan remotely, you need to engage the motor powered roller with the wheels of your caravan.</p>
                        <p>With a powered caravn mover, all you need to do is press a button and the roller will move automatically towards the wheel of the caravan. However, with a manual motor mover, you will need to insert a bar into the mover and turn it, using your own strength, to move the roller towards the wheel. Manual movers can be cheaper, but if you are older or don't have much upper body strength, a powered caravan mover is probably more suitable.</p>

                        <hr>
                        <h2 class="small" id="will-it-fit">Will my caravan mover fit?</h2>
                        <hr>

                        <div class="row">
                            <div class="col-sm-12 col-md-8">
                                <p>Once you have determined whether you would prefer a manual or powered caravan mover, you need to know if it will fit. Our movers can fit onto a wide range of caravan models with L and U profiled chassis. Even if you have a different chassis, our caravan movers may still be compatible, the chassis on the caravan must meet specific ground clearances.</p>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <img src="/assets/img/guides/mover/profiles.jpg" class="additional-part">
                            </div>
                        </div>

                        <img src="/assets/img/guides/mover/profiles-graph.jpg" class="chart">

                        <hr>
                        <h2 class="small" id="additioal-parts">Additional parts to fit your caravan mover</h2>
                        <hr>

                        <p>You may need to purchase some extra parts, (outlined below), to meet the required clearance and ensure the motor mover sits in the correct position on the chassis.</p>

                        <hr>
                        <div class="row">
                            <div class="col-sm-12 col-md-9">
                                <h3>Narrow Gauge Chassis Adapters</h3>
                                <p>If you have an AL-KO Vario III/AV Chassis with a frame less than 2.8mm thick then you must install these plates. If this is the case, your motor mover should be installed behind the axle as the plates must be fitted into the pre-drilled holes that are already on the chassis.</p>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <img src="/assets/img/guides/mover/narrow-guage.jpg" class="additional-part">
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-sm-12 col-md-9">
                                <h3>16mm Spacers - 1 Pair</h3>
                                <p>If the frame height of your chassis is between 140mm and 185mm, you will need to install spacers to achieve the correct frame height. However, you can not install any more than 3 sets of spacers to achieve the correct height of 185mm. If you install spacers, you will also require a set of extended clamp bolts which should be installed simultaneously.</p>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <img src="/assets/img/guides/mover/spacers.jpg" class="additional-part">
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-sm-12 col-md-9">
                                <h3>Set of 8 M10 x 100 Bolts</h3>
                                <p>You should fit the extended clamp bolts in conjunction with the 16mm spacers.</p>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <img src="/assets/img/guides/mover/bolts.jpg" class="additional-part">
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-sm-12 col-md-9">
                                <h3>Low Profile Chassis Adapter Plates</h3>
                                <p>For a chassis frame height below 140mm, you need to install adapter plates in order to achieve the correct height of 185mm. In order to install chassis adapters, you may need to drill into your chassis.</p>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <img src="/assets/img/guides/mover/low-profile.jpg" class="additional-part">
                            </div>
                        </div>

                        <hr>
                        <h2 class="small" id="how-to-fit">How to fit a caravan mover</h2>
                        <hr>

                        <p>We would always recommend having your caravan mover fitted by a professional. Not only can you be sure that it has been fitted correctly, but it will save time and allow you to use your caravan mover with confidence. We have an extensive network of professional fitters and you can book a fitting directly through us and even have an existing motor mover removed.</p>
                        <p>If you do choose to fit a caravan mover yourself, as well as making any necessary modifications, this is taken at your own risk. Depending on which country you live in, you may need to have the installation checked by a professional fitter.</p>

                        <hr>
                        <h2 class="small" id="which-mover">Which caravan mover should I buy?</h2>
                        <hr>

                        <p>Although it is possible to buy a caravan mover second hand, you cannot be sure if it has been well maintained so your safety could be compromised.</p>
                        <p>When you buy a new motor mover, you can be sure that your motor mover is in excellent working condition. If you have the mover from brand new, you will also be able to maintain it to your own high standard and endeavour to keep it in good working condition for many years to come.</p>
                        <p>All of our caravan movers are compatible with single or twin axle caravans. However, some motor movers are only compatible with twin OR single axle caravans, so it is important that you purchase the correct mover for your caravan. Depending on how heavy your caravan is, you may need to purchase more than one mover to cope with the weight of your caravan.</p>

                        <hr>
                        <h2 class="small" id="upgrading">Upgrading the leisure battery</h2>
                        <hr>

                        <p>Your motor mover is powered by the leisure battery on your caravan, which also powers the appliances inside. Therefore, you need to make sure that your leisure battery is well maintained, otherwise your motor mover could be less effective. It may also be necessary to upgrade the leisure battery and the minimum size of your leisure battery should not be below 85AH.</p>

                        <hr>
                        <h2 class="small" id="how-to-use">How to use a caravan mover</h2>
                        <hr>

                        <p>Once you have had your caravan mover fitted by a professional, with any additional parts, it is very easy to use. All you need to do is engage the roller with the tyres of your caravan, either automatically or manually, and your caravan mover is ready to manoeuver with a remote control.</p>
                        <p>Most remote controls are wireless so you can stand in any position around your caravan when you are moving it into place. You should make sure the area is clear of obstacles and there are no people in the way when you begin to move your caravan with the remote. It may also be useful to have another person stood on the other side of your caravan if you are unable to see.</p>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3 col-sm-pull-9 col-md-pull-9 sidebar-complete-guide">
                <div class="sidebar">
                    <div class="sidebar-heading">This guide will look at:</div>
                    <ul>
                        <a class="smoothscroll" href="#manual-vs-powered"><li>Manual caravan mover vs. powered caravan mover</li></a>
                        <a class="smoothscroll" href="#will-it-fit"><li>Will my caravan mover fit? </li></a>
                        <a class="smoothscroll" href="#additioal-parts"><li>Additional parts to fit your caravan mover</li></a>
                        <a class="smoothscroll" href="#how-to-fit"><li>How to fit a caravan mover</li></a>
                        <a class="smoothscroll" href="#which-mover"><li>Which caravan mover should I buy?</li></a>
                        <a class="smoothscroll" href="#upgrading"><li>Upgrading the leisure battery</li></a>
                        <a class="smoothscroll" href="#how-to-use"><li>How to use a caravan mover</li></a>
                    </ul>
                </div>

                <a href="#top" class="btn btn-primary back-to-top hidden-xs smoothscroll" >Back to top</a>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script type="text/javascript">
		$(document).ready(function() {
            var $root = $('html, body');

            $('a.smoothscroll').click(function () {

                // Just open the panel if data-open-panel is set
                if( $( this )[0].hasAttribute( 'data-open-panel' ) ) {

                    $( '#' + $.attr( this, 'data-open-panel' ) ).collapse('show');
                }

                $root.animate({

                    scrollTop: $( $.attr(this, 'href') ).offset().top
                }, 500);

                return true;
            });
		});
	</script>
@stop
