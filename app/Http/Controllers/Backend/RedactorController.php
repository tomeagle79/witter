<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ImageUploadRequest;
use App\Http\Requests\Admin\FileUploadRequest;

use Illuminate\Http\Request;
use View;
use Session;
use Input;
use Response;
use Auth;

class RedactorController extends Controller {

    /**
     * Instantiate a new instance.
    */
	public function __construct()
	{
		$this->data['message'] = Session::get('message');

		$this->data['admin_user'] = Auth::user();
	}

	function image_upload(Request $request)
	{
		$file = $request->file('file');

		$filename = strtolower(str_random(40)).'.'.$file->getClientOriginalExtension();

		$file->move(public_path().'/uploads/cms/images/', $filename);

		$json = array(
			'filelink' => url("/uploads/cms/images/".$filename)
		);

		return Response::json($json);
	}

	function file_upload(Request $request)
	{
		$file = $request->file('file');

		$filename = strtolower(str_random(40)).'.'.$file->getClientOriginalExtension();

		$file->move(public_path().'/uploads/cms/files/', $filename);

		$json = array(
			'filelink' => url("/uploads/cms/files/".$filename),
			'filename' => $filename
		);

		return Response::json($json);
	}

	public function get_image_json()
	{
		$files = array();
		if ($handle = opendir(public_path().'/uploads/cms/images/')) {

			while (false !== ($entry = readdir($handle))) {
				$files[] = $entry;
			}

			closedir($handle);
		}

		unset($files[0]);
		unset($files[1]);

		$json = array();

		foreach($files as $file)
		{
			$json[] = array(
				'thumb' => url('uploads/cms/images/'.$file),
				'image' => url('uploads/cms/images/'.$file),
				'title' => $file,
				'folder' => 'uploads'
			);
		}

		return Response::json($json);
	}

}
