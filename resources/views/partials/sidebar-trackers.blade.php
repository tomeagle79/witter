<div class="sidebar-grey">

	<h2>TRACKER OVERVIEW</h2>

    @if(!empty($brand_filters))

        <h3>BRAND</h3>
        <ul>
            @foreach($brand_filters as $key => $value)

                <li>
                    @if(isset($filters['price']))
                        <a href="{{ route('trackers.index', ['brand' => $key, 'price' => $filters['price']]) }}"
                    @else
                        <a href="{{ route('trackers.index', ['brand' => $key]) }}"
                    @endif
                    @if(isset($filters['brand']) && $filters['brand'] == $key)
                        class="active"
                    @endif
                        >
                        {{ $value }}
                    </a>
                </li>
            @endforeach
        </ul>
    @endif

    @if(!empty($price_filters))

        <h3>PRICE</h3>
        <ul>
            @foreach($price_filters as $key => $value)

                <li>

                    @if(isset($filters['brand']))
                        <a href="{{ route('trackers.index', ['brand' => $filters['brand'], 'price' => $key]) }}"
                    @else
                        <a href="{{ route('trackers.index', ['price' => $key]) }}"
                    @endif
                    @if(isset($filters['price']) && $filters['price'] == $key)
                        class="active"
                    @endif
                        >
                            {!! $value !!}
                        </a>
                </li>
            @endforeach
        </ul>
    @endif

</div>

