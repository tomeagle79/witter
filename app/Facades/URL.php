<?php
namespace App\Facades;

class URL extends \Illuminate\Support\Facades\URL {
    public static function backend($path) {
        return self::to(config('settings.admin.slug').'/'.$path);
    }
    
    public static function login() {
        return self::to(config('settings.admin.loginurl'));
    }
}