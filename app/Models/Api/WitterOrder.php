<?php

namespace App\Models\Api;

use App\Models\Api\ApiModel;

class WitterOrder extends ApiModel
{
    const URI = "orders";

    /**
     * Create an order
     *
     * @params Array
     * @return Object
     */
    public static function create_order($order)
    {
        $url = config('witter.api_base') . self::URI . '/ordermade';

        // 21/4/17 request from Pete Sanders (client) to change isPhoneOrder to an integer instead
        // of a boolean.  i.e. true = 1, false = 0.
        if (isset($order['isPhoneOrder'])) {
            if ($order['isPhoneOrder'] === true) {
                $order['isPhoneOrder'] = 1;
            } else if ($order['isPhoneOrder'] === false) {
                $order['isPhoneOrder'] = 0;
            }
        }

        $obj = self::post_request($url, $order);

        return $obj;
    }

    /**
     * Let Witter know an order's been paid for (or not).
     *
     * @return Object
     */
    public static function order_paid($witter_order_id, $payment_ref, $paid, $divido)
    {
        $url = config('witter.api_base') . self::URI . '/orderpayment';

        $data = [
            'OrderId' => $witter_order_id,
            'PaymentReference' => $payment_ref,
            'IsPaid' => $paid,
            'IsFromDivido' => $divido,
        ];

        $obj = self::post_request($url, $data);

        return $obj;
    }

    /**
     * Create a new preparation phone request
     *
     * @params Array
     * @return Object
     */
    public static function phone_request_prepare($args)
    {
        $url = config('witter.api_base') . self::URI . '/phonerequestprepare?' . http_build_query($args);
        $obj = self::get_request($url);

        return $obj;
    }

    /**
     * Create a phone order
     *
     * @params Array
     * @return Object
     */
    public static function phone_request($args)
    {
        if (env('TEST_PHONE_ORDERS') == 'true') {
            $url = config('witter.api_base') . self::URI . '/phonerequesttest?' . http_build_query($args);
        } else {
            $url = config('witter.api_base') . self::URI . '/phonerequestcheck?' . http_build_query($args);
        }
        // Change to go live
        $obj = self::get_request($url);

        return $obj;
    }

}
