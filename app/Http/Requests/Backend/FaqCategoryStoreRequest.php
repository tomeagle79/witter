<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class FaqCategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:255',
            'url_slug' => ['required', 'max:255', 'regex:/^[a-z0-9-_]+$/'],
        ];


        $id = $this->route('id');

        if($id){
            $rules['url_slug'][] = 'unique:faq_categories,url_slug,'.$id.',id,deleted_at,NULL';
        }
        else {
            $rules['url_slug'][] = 'unique:faq_categories,url_slug,NULL,id,deleted_at,NULL';
        }

        return $rules;
    }
}
