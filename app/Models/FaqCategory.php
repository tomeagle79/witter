<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    /**
     * Relationship to faqs
     */
    public function faqs()
    {
        return $this->hasMany('App\Models\Faq', 'category_id')
            ->where('faqs.published', '=', 1);
    }

    /**
     * Relationship to parent
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\FaqCategory', 'parent_id');
    }

    /**
     * Relationship to children
     */
    public function children()
    {
        return $this->hasMany('App\Models\FaqCategory', 'parent_id');
    }
}
