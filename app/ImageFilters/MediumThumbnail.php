<?php

namespace App\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class MediumThumbnail implements FilterInterface
{

    public function applyFilter(Image $image)
    {
        return $image->resize(380, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }
}