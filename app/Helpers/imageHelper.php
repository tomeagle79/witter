<?php 

function image_load ($url) {
    if(empty($url)) {
        return '/assets/img/no-image.jpg';
    } else {
        return $url;
    }
}