@extends('layouts.frontend')
@section('title', 'Towbar Fitting | UK Tow Bar Fitters')
@section('meta_description') Towbar fitting by Witter Towbars. Our all-in-one price includes towbar, electric kit and fitting. Buy your towbar and arrange a fitting online today.
@stop


@section('JSON-LD') {{--
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" /> --}}
<script type='application/ld+json'>
    {
        "@context": "http://www.schema.org",
        "@type": "Organization",
        "name": "Witter Towbars",
        "url": "https://www.witter-towbars.co.uk/",
        "logo": "https://www.witter-towbars.co.uk/assets/img/logo-white-curve.png",
        "image": "https://www.witter-towbars.co.uk/imagecache/original/UnCP32Hqek1q6PDEWOB1CCNqTo4niFOSgV8bPy0B.jpg",
        "description": "Towbar fitting by Witter Towbars. Our all-in-one price includes towbar, electric kit and fitting. Buy your towbar and arrange a fitting online today.",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "6-11 Drome Road, Deeside Industrial Park",
            "addressLocality": "Deeside",
            "addressRegion": "Flintshire",
            "postalCode": "CH5 2NY",
            "addressCountry": "UK"
        },
        "contactPoint": {
            "@type": "ContactPoint",
            "contactType": "customer service",
            "telephone": "+44(0)1244 284555"
        },
        "telephone": "+44(0)1244 284555"
    }
</script>

@stop
@section('content')
@if($bannerType == 'AB')
@include('partials.home.ab-banner')
@else @if(count($banners) > 0)

@section('body-class', 'home')

<div id="home-carousel" class="carousel slide banner-carousel" data-ride="carousel">

    {{-- Wrapper for slides --}}
    <div class="carousel-inner" role="listbox">

        @foreach($banners as $banner)

        <div class="item" @if(!empty($banner->image->filename)) style="background-image: url({{URL('imagecache', ['template' => 'original', 'filename' => $banner->image->filename])}} ) ;" @endif >

            <div class="item-shadow"></div>
            {{-- .item-shadow --}}

            <div class="container">
                <div class="row">

                    @if(!empty($banner->manufacturer_of_week))
                    <div class="highlight-block">
                        <strong>MANUFACTURER</strong> OF THE WEEK
                    </div>
                    {{-- .discount --}}
                    @endif @if(!empty($banner->vehicle_of_week))
                    <div class="highlight-block">
                        <strong>VEHICLE</strong> OF THE WEEK
                    </div>
                    {{-- .discount --}}
                    @endif

                    <div class="col-xs-12 col-sm-7 high-zindex">

                        <h2>
                            <a href="{!! $banner->link !!}" class="item-link @if( !empty($banner->drop_shadow) ) drop-shadow @endif" @if( !empty($banner->main_color) ) style="color: {{ $banner->main_color }};" @endif >{!! $banner->title !!}</a>
                        </h2>

                        @if(!empty($banner->sub_title))
                        <p class="">
                            <a href="{!! $banner->link !!}" class="item-link @if( !empty($banner->drop_shadow) ) drop-shadow @endif" @if( !empty($banner->sub_color) ) style="color: {{ $banner->sub_color }};" @endif >{!! $banner->sub_title !!}</a>
                        </p>
                        @endif @if(!empty($banner->discount_code_check))
                        <div class="discount hidden-x">
                            <div class="discount-box">{!! $banner->discount_code !!}</div>
                            Simply use this code on checkout.
                        </div>
                        @endif @if(!empty($banner->button_text))
                        <div class="bnt-wrapper">
                            <a class="btn btn-primary" href="{!! $banner->link !!}">{{ $banner->button_text }}</a>
                        </div>
                        {{-- .discount --}}
                        @endif

                    </div>
                </div>
            </div>

        </div>
        @endforeach @if(!empty($banners))
        <div class="carousel-controllers">
            {{-- Indicators --}}
            <a class="prev" href="#home-carousel" role="button" data-slide="prev"></a>

            <ul class="carousel-indicators">

                @foreach ($banners as $banner)
                <li data-target="#home-carousel" data-slide-to="{{ $loop->index }}" @if($loop->index == 0) class="active" @endif>
                </li>
                @endforeach
            </ul>

            <a class="next" href="#home-carousel" role="button" data-slide="next"></a>

        </div>
        {{-- .carousel-controllers --}}
        @endif

    </div>

    <div class="container-fluid blue-transparent-banner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="blue-transparent-banner-flex">
                        <div>
                            <p><i class="icon-white-block"></i> Free UK Delivery</p>
                        </div>
                        <div>
                            <p><i class="icon-mobile-fitter"></i> Nationwide <span class="hidden-xs hidden-sm hidden-md">Mobile</span> Fitting</p>
                        </div>
                        <div class="hidden-xs">
                            <p><i class="icon-trusted"></i> Trusted Since 1950</p>
                        </div>
                        <div class="hidden-xs">
                            <a href="{!! route('blog_dynamic_route', 'what-is-webfit') !!}" title="WEBFIT IS HERE">
                                <p><i class="icon-webfit"></i> <span>WEBFIT IS HERE, CLICK <br>TO FIND OUT MORE</span></p>
                            </a>
                        </div>
                    </div>{{-- .blue-transparent-banner-flex --}}
                </div>
                {{-- .col-xs-12 --}}
            </div>
            {{-- .row --}}
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="search-wrapper">
        <div class="container">
            <div class="towbar-search-wrapper">
                @include('partials.search-towbar-search-box')
            </div>
            {{-- .towbar-search-wrapper --}}
        </div>
    </div>
</div>
@endif {{-- if (count($banners) > 0) --}} @endif {{-- if AB --}}
<div class="homepage-links">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <a href="/towbars" class="">
                    <div class="inner bg-arrow-round" style="background-image: url('/assets/img/home-links/towbar-caravan.jpg')">
                        <h4>Towbars</h4>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-md-4">
                <a href="/bike-racks" class="">
                    <div class="inner bg-arrow-round" style="background-image: url('/assets/img/home-links/cycle-carrier.jpg')">
                        <h4>Bike Racks</h4>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-md-4">
                <a href="/accessories" class="">
                    <div class="inner bg-arrow-round" style="background-image: url('/assets/img/home-links/van-tarmac.jpg')">
                        <h4>Accessories</h4>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="homepage-guide-links">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <a href="/how-to-guide-towbars" class="">
                    <div class="inner bg-arrow-round " style="background-image: url('/assets/img/home-links/towbar-swan.jpg')">
                        <div class="text">
                            <h4>Which towbar is right for you?</h4>
                            <p>Read our helpful guide here...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-md-4">
                <a href="/how-to-guide-cycle-carriers" class="">
                    <div class="inner bg-arrow-round " style="background-image: url('/assets/img/home-links/towbar-swan.jpg')">
                        <div class="text">
                            <h4>Unsure which bike rack?</h4>
                            <p>Read our helpful guide here...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-md-4">
                <a href="/help-advice/mobile-towbar-fitting-guide" class="">
                    <div class="inner bg-arrow-round " style="background-image: url('/assets/img/home-links/towbar-swan.jpg')">
                        <div class="text">
                            <h4>Fitting at your home or work</h4>
                            <p>Read our helpful guide here...</p>
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </div>
</div>

@include('partials.products_list', ['list_title' => 'Popular products...', 'list_products' => $popular_products])

<div class="approved-fitters animated" data-animation="revealed">
    <div class="container-fluid ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Nationwide workshops of approved fitters</h2>
                </div>
                <div class="col-md-6">
                    <p>With over 450 service centres across the UK, you can be sure that you will receive the highest level of service no matter where you are located.</p>
                </div>

            </div>
            <div class="row hidden-xs hidden-sm">
                <div class="col-xs-12">
                    <ul>
                        <li>
                            <a href="/service-centres/scotland">Scotland</a>
                        </li>
                        <li>
                            <a href="/service-centres/northern-ireland">Northern Ireland</a>
                        </li>
                        <li>
                            <a href="/service-centres/north-west">North West</a>
                        </li>
                        <li>
                            <a href="/service-centres/north-east">North East</a>
                        </li>
                        <li>
                            <a href="/service-centres/yorkshire-and-the-humber">Yorkshire and the Humber</a>
                        </li>
                        <li>
                            <a href="/service-centres/wales">Wales</a>
                        </li>
                        <li>
                            <a href="/service-centres/west-midlands">West Midlands</a>
                        </li>
                        <li>
                            <a href="/service-centres/east-midlands">East Midlands</a>
                        </li>
                        <li>
                            <a href="/service-centres/east-of-england">East of England</a>
                        </li>
                        <li>
                            <a href="/service-centres/greater-london">Greater London</a>
                        </li>
                        <li>
                            <a href="/service-centres/south-west">South West</a>
                        </li>
                        <li>
                            <a href="/service-centres/south-east">South East</a>
                        </li>

                    </ul>

                </div>
            </div>
            <div class="row service-centres">
                <div class="col-xs-12 col-md-6">
                    <div class="centres-form-container">

                        <p>Search over 450 locations, simply enter your postcode</p>
                        <form method="GET" action="/service-centres" accept-charset="UTF-8" class="" id="service_centres_form">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <input class="form-control" placeholder="Search by city, town or postcode" name="postcode" type="text">
                                </div>
                                {{-- .col-sm-7 --}}

                                <div class="col-xs-12 col-md-6">
                                    <button type="submit" class="btn btn-primary btn-primary-large ">Search</button>
                                </div>
                            </div>
                            {{-- .col-sm-7 --}}

                        </form>


                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<div class="clearfix"></div>

<div class="blog-banner">
    <div id="blog-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
        {{-- Wrapper for slides --}}
        <div class="container">
            <div class="carousel-inner">
                @foreach($blog_posts as $post)
                <div class="item {{$loop->first ? 'active' : ''}}">
                    <div class="row row-flex">
                        <div class="col col-xs-12 col-md-5">
                            <div class="blog-content">
                                <h2 class="blog-title">{{ $post->title }}</h2>
                                <div class="blog-copy">
                                    <p>{{ str_limit(strip_tags($post->content), 205, '...') }}</p>
                                </div>
                                <a href="{{ route('blog_dynamic_route', $post->url_slug) }}" class="btn btn-grey">Read More</a>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-6 col-md-offset-1">
                            <div class="blog-image animated" data-animation="fadeIn">
                                @if(!is_null($post->image))
                                <img src="{{URL('imagecache', ['template' => 'large', 'filename' => $post->image->filename])}}" alt="" />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        {{-- Left and right controls --}}
        <a class="left carousel-control" href="#blog-carousel" data-slide="prev">
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#blog-carousel" data-slide="next">
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

{{-- top 10 --}}
@if(!empty($top_ten))
<div class="container-fluid top-10-banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <img src="/assets/img/witter-logo-shadow.png" alt="Witter Towbars logo" class="witter-logo" />
                <h2>Top 10 Vehicle Towbars</h2>
            </div>
            {{-- .col-xs-12 --}}
        </div>
        {{-- .row --}}

        <div class="row list-teasers">
            @foreach($top_ten as $vehicle)
            @if($loop->iteration == 1 || $loop->iteration == 6)
            <div class="col-xs-6 col-md-2 col-md-offset-1 top-10-item"> @else <div class="col-xs-6 col-md-2 top-10-item">
                    @endif
                    @if(!empty($vehicle->bodies) && count($vehicle->bodies) == 1)

                    <a href="{{ route('towbars.model_list.body_list.registration_list', [$vehicle->manufacturerSlug, $vehicle->modelSlug, $vehicle->bodies[0]->slug]) }}" class="top-10-link" data-manufacturer_slug="{{ $vehicle->manufacturerSlug }}" data-model_id="{{ $vehicle->modelId }}" data-model_slug="{{ $vehicle->modelSlug }}" data-body_id="{{ $vehicle->bodies[0]->bodyId }}" data-body_slug="{{ $vehicle->bodies[0]->slug }}" data-manufacturer_name="{{ explode(" / ", $vehicle->manufacturerName)[0] }}" data-model_name="{{ $vehicle->modelName }}" data-top-ten-id="{{ $loop->iteration }}">
                        @else

                        <a href="{{ route('towbars.model_list.body_list', [$vehicle->manufacturerSlug, $vehicle->modelSlug]) }}" class="top-10-link" data-manufacturer_slug="{{ $vehicle->manufacturerSlug }}" data-model_id="{{ $vehicle->modelId }}" data-model_slug="{{ $vehicle->modelSlug }}" data-manufacturer_name="{{ explode(" / ", $vehicle->manufacturerName)[0] }}" data-model_name="{{ $vehicle->modelName }}" data-top-ten-id="{{ $loop->iteration }}">
                            @endif
                            <div class="info">
                                <span>Select</span>
                                <div class="number">
                                    {{ $loop->iteration }}
                                </div>
                                {{-- .number --}}
                                <div class="arrow"></div>
                            </div>

                            <img src="/assets/img/cars/{{ $vehicle->manufacturerSlug . '-' . $vehicle->modelSlug }}.png" class="top-10-img" alt="{{ explode(" / ", $vehicle->manufacturerName)[0] }} {{ $vehicle->modelName }} Towbars">
                            <h3 class="model-title">{{ explode("/", $vehicle->manufacturerName)[0] }}
                                <span>{{ $vehicle->modelName }}</span>
                            </h3>
                        </a>

                </div> {{-- end col --}}
                @endforeach
            </div>
            <div class="row hidden-md hidden-lg">
                <div class="col-xs-12" style="text-align: center">
                    <img class="animated" src="/assets/img/icons/swipe.svg" alt="Swipe icon" data-animation="swipe" />
                </div>
            </div>
        </div>

        {{-- .container --}}
    </div>
    {{-- .container-fluid --}}
    {{-- end top 10 --}}
</div>
@endif

<div class="container general-banner">
    <div class="row is-flex">
        <div class="col-xs-12 col-md-6">
            <h2>An introduction to Witter...</h2>
        </div>
        <div class="col-xs-12 col-md-6 col-btn">
            <a href="/about" class="btn btn-primary btn-primary-slim">Read more</a>
        </div>

        <div class="col-xs-12 col-p">
            <p>Since it was founded over half a century ago, Witter Towbars has consistently set the standard for
                <strong>quality, reliability and value for money</strong>. Witter Towbars are designed, developed and tested to Regulation 55 at our centre of engineering excellence in the UK. Our fully qualified engineers utilise the latest 3D CAD systems to design towbars that fit all the vehicle manufacturers' specified attachment points, ensuring the widest variety of towbars possible. Once designed, our towbars go through vigorous testing carried out in our in house servo hydraulic test rigs. The testing process is independently witnessed and authorised by the UK Vehicle Certification Agency (VCA). Only when the VCA are satisfied that we have met all of the criteria of the EC regulations, will they then grant us EC Type Approval for that particular towbar. Therefore we can give every Witter Towbar our life time guarantee.</p>
        </div>


    </div>
</div>
<div class="container general-banner towbar-manufacturers">
    <div class="row">
        <div class="col-md-12">
            <h2>Towbars by Manufacturer</h2>
        </div>
        <div class="col-xs-12 col-p">
            <p>Choose your vehicle manufacturer to find out which towbars are suitable. Here are some of the car manufacturers we make towbars to fit:</p>
        </div>
    </div>
    <div class="row row-flex">
        <div class="col-xs-3 col-md-2 col-md-offset-1 col-lg-1 col-lg-offset-0 ta-c">
            <a href="/towbars/alfa-romeo" class="angular-blocks">
                <img src="/assets/img/manufacturers/alfa-romeo.png" alt="Alfa Romeo">
            </a>
        </div>
        <div class="col-xs-3 col-md-2 col-lg-1 ta-c">
            <a href="/towbars/jeep" class="angular-blocks">
                <img src="/assets/img/manufacturers/jeep.png" alt="Jeep">
            </a>
        </div>
        <div class="col-xs-3 col-md-2 col-lg-1 ta-c">
            <a href="/towbars/land-rover-range-rover" class="angular-blocks">
                <img src="/assets/img/manufacturers/land-rover.png" alt="Land Rover">
            </a>
        </div>
        <div class="col-xs-3 col-md-2 col-lg-1 ta-c">
            <a href="/towbars/honda" class="angular-blocks">
                <img src="/assets/img/manufacturers/honda.png" alt="Honda">
            </a>
        </div>
        <div class="col-xs-3 col-md-2 col-lg-1 ta-c">
            <a href="/towbars/bmw" class="angular-blocks">
                <img src="/assets/img/manufacturers/bmw.png" alt="BMW">
            </a>
        </div>
        <div class="col-xs-3 col-md-2  col-md-offset-1 col-lg-1 col-lg-offset-0 ta-c">
            <a href="/towbars/citroen" class="angular-blocks">
                <img src="/assets/img/manufacturers/citroen.png" alt="Citroen">
            </a>
        </div>
        <div class="col-xs-3 col-md-2 col-lg-1 ta-c">
            <a href="/towbars/ford" class="angular-blocks">
                <img src="/assets/img/manufacturers/ford.png" alt="Ford">
            </a>
        </div>
        <div class="col-xs-3 col-md-2 col-lg-1 ta-c">
            <a href="/towbars/fiat" class="angular-blocks">
                <img src="/assets/img/manufacturers/fiat.png" alt="fiat">
            </a>
        </div>
        <div class="col-xs-3 col-md-2 col-lg-1 hidden-xs hidden-sm ta-c">
            <a href="/towbars/toyota" class="angular-blocks">
                <img src="/assets/img/manufacturers/toyota.png" alt="toyota">
            </a>
        </div>
        <div class="col-xs-3 col-md-2 col-lg-1 hidden-xs hidden-sm ta-c">
            <a href="/towbars/volkswagen" class="angular-blocks">
                <img src="/assets/img/manufacturers/vw.png" alt="Volkswagen">
            </a>
        </div>
        <div class="col-xs-12 col-lg-2 view-all">
            <a href="/towbars" class="btn btn-primary btn-primary-slim">View All</a>

        </div>
    </div>
</div>

<div class="modal fade modal-vehicle-search" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </span>
                </button>
            </div>
            {{-- .modal-header --}}
            <div class="modal-body">

                <div class="card-search">
                    <div class="card-body">

                        @include('partials.search-dropdowns-modal')

                    </div>
                    {{-- .card-search --}}

                </div>
                {{-- .modal-body --}}
            </div>
            {{-- .modal-content --}}
        </div>
        {{-- .modal-dialog --}}
    </div>
    {{-- .modal --}}

    @stop
    @section('scripts')


    <script type="text/javascript">
        // top-10 carousel
        $(document).ready(function () {
            $(window).on('load resize orientationchange', function () {
                var $carousel = $('.list-teasers');
                if ($(window).outerWidth() >= 992) {
                    if ($carousel.hasClass('slick-initialized')) {
                        $carousel.slick('unslick');
                    }
                } else {
                    if (!$carousel.hasClass('slick-initialized')) {
                        $carousel.slick({
                            infinte: true,
                            arrows: false,
                            dots: false,
                            slidesToShow: 1,
                            centerMode: true,
                            centerPadding: '150px',
                            mobileFirst: true,
                            responsive: [{
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 3,
                                }
                            }]
                        });
                    }
                } // else
            }) // end window load
        }) // end doc ready


        $(document).ready(function () {

            // Set first carousel to '.active'
            $('#home-carousel .carousel-inner .item:first-child').addClass('active');

            $('#manufacturer_dropdown').on('submit', function (e) {
                e.preventDefault();

                var slug = $(this).find('select option:selected').attr('data-slug');

                if (slug.length == 0) {
                    return null;
                } else {
                    location.href = '/towbars/' + slug;
                }
            });

            // Set up the top 10 links
            $(".top-10-link").on('click', function (e) {

                e.preventDefault();

                $('#modal #search-dropdown-modal input[name=modelSlug]').val($(this).attr("data-model_slug"));

                $('#modal #search-dropdown-modal input[name=manufacturerSlug]').val($(this).attr("data-manufacturer_slug"));

                $('#modal #search-dropdown-modal input[name=topTenId]').val($(this).attr("data-top-ten-id"));

                $('#modal .vehicle-plate-search input[name=topTenId]').val($(this).attr("data-top-ten-id"));



                var html = '<p>Find Towbars for <span>' + $(this).attr("data-manufacturer_name") + ' <span>' + $(this).attr("data-model_name") + '</span></span></p>';

                $('#modal #search-dropdown-modal .vehicle-search-message').html(html);


                $("div.demo-container").html();

                var attr = $(this).attr('data-body_id');

                // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
                if (typeof attr !== typeof undefined && attr !== false) {
                    // Element has this attribute
                    $('#modal #search-dropdown-modal .modal-body-dropdown').hide();

                    $('#modal #search-dropdown-modal .modal-registration-dropdown').addClass('col-sm-9');

                    $('#modal #search-dropdown-modal .modal-registration-dropdown').addClass();

                    $('#modal #search-dropdown-modal input[name=bodySlug]').val($(this).attr("data-body_slug"));

                    $('#modal #search-dropdown-modal .registration').dynamicDropdowns('/vehicles/ajax_registrations/' + $(this).attr("data-body_id"));

                } else {

                    $('#modal #search-dropdown-modal .modal-body-dropdown').show();

                    $('#modal #search-dropdown-modal .modal-registration-dropdown').removeClass('col-sm-9');

                    // $('#modal #search-dropdown-modal .modal-body-dropdown')addClass( 'col-sm-8' );

                    $('#modal #search-dropdown-modal .body').dynamicDropdowns('/vehicles/ajax_bodies/' + $(this).attr("data-model_id"));
                }

                $('#modal').modal('show');

            });
        });
    </script>
    @stop