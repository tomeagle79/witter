<?php

namespace App\Models;

use App\Traits\Published;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Career extends Model
{
    use SoftDeletes;

    use Published;

}
