<?php

namespace App\Models\Api\Commercial;

use  App\Models\Api\ApiModel;

class CommercialVehicleModel extends ApiModel
{
	const URI = "commercials/models";

	/**
	 * Get all models based on manufacturer ID
	 *
	 * @param Int
	 * @return Object
	 */
	static public function get_where(array $args)
	{
		$url = config('witter.api_base') . 'commercials/manufacturers/slug/models?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get all models based on type and manufacturer ID
	 *
	 * @param Int
	 * @return Object
	 */
	static public function get_by_type(array $args)
	{
		$url = config('witter.api_base') . 'commercials/types/slug/manufacturers/slug/models?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}
}
