<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Api\PartClass;
use App\Models\Api\Accessory;

class TrackerController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
         parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Vehicle trackers', route('trackers.index'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Set our slugs to call the API.
        $category_slug = "trackers-pdcs-alarms";
        $subcategory_slug = "tracker";

        $this->data['category'] = PartClass::get_category_by_slug($category_slug);
        $this->data['subcategory'] = PartClass::get_subcategory_by_slug($category_slug, $subcategory_slug);

        // Get the filter parameters from the request
        $filters = $request->all();

        // Get trackers
        $this->data['trackers'] = Accessory::get_filtered_accessories_by_slug($category_slug, $subcategory_slug, $filters);

        $this->data['filters'] = $filters;

        $this->data['price_filters'] = ['0-100' => '&pound0  - &pound100',
            '100-300' => '&pound100  - &pound300',
            '300-500' => '&pound300  - &pound500',
            '500>' => '&gt; &pound500'];

        $this->data['brand_filters'] = ['bulldog' => 'Bulldog', 'scorpion' => 'Scorpion'];

        return view('trackers.index', $this->data);
    }

}
