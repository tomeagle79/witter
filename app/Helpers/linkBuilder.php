<?php 

// Route the popular products 
function buildLinkToAccessoryView($product) {
  $partClassSlug = $product->partClassSlug;
  // echo $partClassSlug;
  switch ($partClassSlug) {
    case "cycle-carriers":
      return route('bike_rack.view', [encode_url($product->slug)]);
      break;
    case "caravan-movers":
      return route('caravan_movers.view', [encode_url($product->slug)]);
      break;
    // case "trackers-pdcs-alarms":
    //   return route('bike_rack.view', [encode_url($product->slug)]);
    //   break;
    case "Couplings":
      return route('accessories.multi', [encode_url($product->slug)]);
      break;
    default:
      return route('accessories.multi', [encode_url($product->slug)]);
  }
}