@extends('layouts.frontend')

@section('title', $manufacturer->manufacturerName .' '. $model->modelName .' Air Suspension Kits')

@section('heading')
Air Suspension Kits for {{ $manufacturer->manufacturerName }} {{ $model->modelName }}
@stop

@section('meta_description')
	Witter Towbars | Air Suspension Kits
@stop

@section('body-class', 'body_towbar_listing')

@section('content')

<div class="container">
    <div class="towbars-product-listing ">


        @if(!empty($products->count()))

            <div class="row">

        		<div class="col-xs-12 col-sm-9">

                    <div class="row">

                        <div class="col-xs-12 numberplate">

                            @if(!empty($numberplate))

                                @include('partials.numberplate-search')

                            @endif

                        </div><!-- /.col-xs-12 -->

                        <div class="col-xs-12 towbars-listing-title">

                            <p class="pre-title">We have the following Air Suspension Kits to fit your</p>

                            <h2> {{ $manufacturer->manufacturerName }} {{ $model->modelName }}</h2>

                        </div><!-- /.col-xs-12 col-xs-push-12 col-sm-12 col-xs-push-0 -->

                    </div><!-- /.row -->
                </div> <!-- .col-sm-9 -->

                <div class="col-sm-3">
                    <div class="top-img-wrapper">
                        @if(!empty($manufacturer))
                            <img src="<?php echo $manufacturer->logoUrl; ?>" alt="{{ $manufacturer->manufacturerName }} {{ $model->modelName }}" class="top-img"/>
                        @endif
                    </div> <!-- .wrapper -->
                </div> <!-- .col-sm-3 -->

            </div> <!-- .row -->

            <div class="row">
                <div class="col-md-12 item-list">

                    @foreach($products as $product)

                        @include('partials.analytics.json_product_details', [
                            'product' => $product,
                            'list_name' => 'Category Results',
                            'category' => 'Roof Systems/Roof Racks',
                            'position' => $loop->iteration
                        ])

                        <div class="towbar-listing">

                            <div class="row towbar-listing-content">

                                <div class="col-md-3 towbar-image">
                                    <a href="{{ route('suspensions.view', encode_url($product->slug)) }}" class="product-link ribbon-conatiner" data-product-id="{{ $product->partNo }}"><img src="{{ image_load($product->imageUrl) }}" alt="">
										@if($product->isFittable)
											@include('partials.ribbon.webfit')
                                        @endif
                                    </a>
                                </div> <!-- .col-md-3 -->
                                <div class="col-md-6">

                                    <h3><a href="{{ route('suspensions.view', encode_url($product->slug)) }}" class="product-link" data-product-id="{{ $product->partNo }}">{{ $product->title }}</a></h3>

                                    <div class="hidden-xs">{!! $product->details !!}</div>

                                    <ul class="row towbar-includes">
                                        @if(towbar_in_stock($product) && ($product->stockLevel > 5 || $product->stockLevel == 0))

                                            <li class="col-sm-6 col-xs-12 stock in-stock">
                                                <div class="include-item">In Stock</div>
                                            </li> <!-- col-sm-6 -->
                                        @elseif(towbar_in_stock($product) && $product->stockLevel <= 5)
                                            <li class="col-sm-6 col-xs-12 stock low-stock">
                                                <div class="include-item">Hurry, only {{ $product->stockLevel }} left!</div>
                                            </li> <!-- col-sm-6 -->
                                        @else
                                            <li class="col-sm-6 col-xs-12 stock out-of-stock">
                                                <div class="include-item">Out of Stock</div>
                                            </li> <!-- col-sm-6 -->
                                        @endif

                                        <li class="col-sm-6 col-xs-12 tick-li">
                                            <div class="include-item">Free UK Delivery</div>
                                        </li> <!-- col-sm-6 -->

                                        <li class="col-xs-12 tick-li">
                                            <div class="include-item">Mobile and Workshop Fitting Available</div>
                                        </li> <!-- .col-md-4 col-sm-6 -->
                                    </ul> <!-- .row -->

                                @if(!empty($product->attributes))
                                    <div class="benefits-toggle hidden-xs hidden-sm">
                                        <a href="#{{ $product->partNo }}" data-toggle="collapse" class="benefits-toggle-link collapsed">View all benefits</a>
                                    </div> <!-- .benefits-toggle -->
                                @endif

                                </div> <!-- .col-md-6 -->

                                <div class="col-xs-12 col-md-3 text-right">

                                    <div class="row price-wrapper">
                                        <div class="col-xs-push-6 col-xs-6 col-md-12 col-md-push-0 towbar-brand-logo">

                                            @if(strtolower($product->brandName) == 'witter')
                                                <img src="/assets/img/towbars/witter-logo.png" alt="{{ $towbar->brandName }}" width="80">
                                            @elseif(strtolower($product->brandName) == 'westfalia')
                                                <img src="/assets/img/towbars/westfalia-logo.png" alt="{{ $towbar->brandName }}" width="160">
                                            @endif

                                        </div><!-- /.col-md-12 -->

                                        <div class="col-xs-pull-6 col-xs-6 col-md-12 col-md-pull-0">

                                            <div class="price">
                                                Fully fitted price from
                                                <span>&pound;{{ price($product->displayPrice) }}</span>
                                            </div> <!-- .price -->
                                        </div><!-- /.col-md-12 -->

                                    </div><!-- /.row -->

                                    <div class="row">
                                        <div class="col-xs-12 btn-links">

                                            @if(towbar_in_stock($product))
                                                <a href="{{ route('suspensions.view', encode_url($product->slug)) }}"  class="btn btn-primary btn-block product-link" data-product-id="{{ $product->partNo }}">View Details</a>
                                            @else
                                                <a href="{{ route('suspensions.view', encode_url($product->slug)) }}"  class="btn btn-primary btn-oos-model">Notify me when in stock</a>
                                            @endif

                                        </div><!-- /.col-xs-12 -->

                                    </div><!-- /.row -->

                                </div> <!-- .col-md-3 -->
                            </div> <!-- .row -->

                            <div class="row collapse" id="{{ $product->partNo }}">

                                <div class="col-xs-12 col-md-9 col-md-offset-3">

                                    <div class="benefits hidden-xs hidden-sm">

                                        @foreach($product->attributes as $benefit)

                                            @if($loop->iteration%2 === 1)
                                                <ul class="row towbar-includes">
                                            @endif

                                                <li class="col-xs-12 col-md-6 tick-li">{{ $benefit }}</li>

                                            @if($loop->iteration%2 === 0 || $loop->last)
                                                </ul>
                                            @endif

                                        @endforeach

                                    </div><!-- /.benefits -->
                                </div><!-- /.col-md-9 -->

                            </div><!-- /.row -->

                        </div> <!-- .towbar-listing -->
                    @endforeach

                    <hr>

                </div> <!-- .col-md-12 -->
            </div> <!-- .row -->

            <div class="row">
                {{-- $products->links() --}}
            </div>

        @else

            <div class="row ">

                <div class="col-md-8">

                    <h2 class="nomargin">{{ $manufacturer->manufacturerName }} {{ $model->modelName }}</h2>

                    <div class="button-area">
                        <a href="{{ route('suspensions') }}" class="btn btn-primary">Not your Vehicle?</a>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <p>Please get in contact with our Witter Towbar experts who will be able to advise you on the best Air Suspension Kit for your {{ $manufacturer->manufacturerName }} {{ $model->modelName }}.</p>
                        </div>
                    </div>

                </div> <!-- .col-md-8 -->

                <div class="col-md-4">
                    <div class="top-img-wrapper">
                            @if(!empty($manufacturer))
                            <img src="<?php echo $manufacturer->logoUrl; ?>" alt="{{ $manufacturer->manufacturerName }} {{ $model->modelName }}" class="top-img"/>
                        @endif
                    </div> <!-- .wrapper -->
                </div> <!-- .col-md-6 -->
            </div> <!-- .row -->

            <div class="row contact">

                <div class="col-md-3 get-in-touch">
                    <h2 class="border-bottom">Contact us</h2>
                    <p>
                        Call us on<br>
                        <a href="tel:+4401244284555"><span class="color-secondary">01244 284555</span></a><br><br>
                        Send us an email<br>
                        <span class="color-secondary"><a href="mailto:ecommerce@witter-towbars.co.uk" class="email-font">ecommerce@witter-towbars.co.uk</a></span>
                    </p>
                </div>

                <div class="col-md-6 get-in-touch">
                    <h2 class="border-bottom">Email us</h2>

                    {!! Form::open(array('route' => 'contact.submit_product_enquiry', 'class' => 'form', 'id' => 'contact_form', 'onsubmit' => "
                                gtag('event', 'Out of stock', {
                                    event_category: 'Form Submission',
                                    event_label: '".  $manufacturer->manufacturerName . ' ' . $model->modelName ."'
                                });
                            ")) !!}

                        <input type="hidden" name="enquiry_regarding" value="Air Suspension Kit for {{ $manufacturer->manufacturerName }} {{ $model->modelName }}">
                        <input type="hidden" name="website_url" value="{{ url()->current() }}?nofire=true">

                        <div class="form-group">

                            {!! Form::label('name', 'Name', array('class' => 'sr-only')) !!}

                            {!! Form::text('name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                        </div>

                        <div class="form-group">

                            {!! Form::label('email', 'Email Address', array('class' => 'sr-only')) !!}

                            {!! Form::text('email', null,
                                array('required', 'class'=>'form-control', 'placeholder'=>'Email Address')) !!}
                        </div>

                        <div class="form-group">

                            {!! Form::label('phone', 'Telephone', array('class' => 'sr-only')) !!}

                            {!! Form::text('phone', null,
                                array('required', 'class'=>'form-control', 'placeholder'=>'Telephone')) !!}
                        </div>

                        <div class="form-group">

                            {!! Form::label('postcode', 'Postcode', array('class' => 'sr-only')) !!}

                            {!! Form::text('postcode', null,
                                array('class'=>'form-control', 'placeholder'=>'Postcode')) !!}
                        </div>

                        <div class="form-group">

                            {!! Form::label('enquiry', 'Enquiry', array('class' => 'sr-only')) !!}

                            {!! Form::textarea('enquiry', null,
                                array('required', 'class'=>'form-control', 'placeholder'=>'Enquiry')) !!}
                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary">Submit Enquiry </button>

                        </div>
                    {!! Form::close() !!}
                </div>

            </div>

        @endif
    </div><!-- /.card-clear -->
</div> <!-- .container -->
@stop

@section('scripts')



@stop
