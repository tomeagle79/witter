<div class="form-group required">
	<label for="faq" class="col-md-2 control-label">Question</label>
	<div class="col-md-4">
		{!! Form::text('faq', ( ($type == 'edit') ? $faq->faq : null), ['class' => 'form-control title-for-slug', 'id' => 'faq', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group">
	<label for="published" class="col-sm-3 col-md-2 control-label">Published</label>
	<div class="col-sm-9 col-md-6">
		{!! Form::checkbox('published', true, isset($faq) ? $faq->published : true, ['class' => '']) !!}
		<p class="help-block">Display on website?</p>
	</div>
</div>

<div class="form-group required">
	<label for="url_slug" class="col-md-2 control-label">URL Slug</label>
	<div class="col-md-4">
		{!! Form::text('url_slug', ( ($type == 'edit') ? $faq->url_slug : null), ['class' => 'form-control slug-generated', 'id' => 'slug']) !!}
	</div>
</div>

@if($type=='edit' && !is_null($faq->image))
	<div class="form-group">
		<label for="logo" class="control-label col-xs-12 col-sm-3 col-md-2 col-lg-2">Current Image</label>
		<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">

			<a href="{{URL('imagecache', ['template' => 'original', 'filename' => $faq->image->filename])}}" class="fancybox image">
				<img src="{{URL('imagecache', ['template' => 'small', 'filename' => $faq->image->filename])}}" alt="" />
			</a>
		</div>
	</div>
@endif

<div class="form-group required">
	<label for="answer" class="control-label col-xs-12 col-sm-3 col-md-2 col-lg-2">Answer</label>
	<div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
		{!! Form::textarea('answer', ( ($type == 'edit') ? $faq->answer : null), ['class' => 'form-control wysiwyg']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="category_id" class="col-md-2 control-label">Category</label>
	<div class="col-md-4">
		{{ Form::select('category_id', $categories, $category_id , ['placeholder' => 'Pick a category...']) }}
	</div>
</div>

<div class="form-group">
	<div class="col-md-offset-2 col-md-4">
		<button type="submit" class="btn btn green">{{ $submit_text }}</button>
	</div>
</div>

@section('scripts')
	@parent
	<script type="text/javascript" src="{!! URL::asset('assets/js/redactor.js') !!}"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('textarea.wysiwyg').redactor({
				minHeight: 300,
				plugins: ['table', 'video', 'imagemanager'],
				buttonSource: true,
				imageUpload: '{!! URL::to('wtadmin/redactor/image_upload') !!}?_token=' + '{{ csrf_token() }}',
				imageManagerJson: '{!! URL::to('wtadmin/redactor/get_image_json') !!}',
			});
		});

	</script>
@overwrite
