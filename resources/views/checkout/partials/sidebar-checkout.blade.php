<div class="checkout-widget">
    @if(!empty($cart['promotion']))
        <div class="promos">
            <div class="row">
                    <div class="col-xs-5">
                        <p>Subtotal:</p>
                    </div><!-- /.col-sm-5 -->
                    <div class="col-sm-7 sub-total-value">
                        &pound;{{ price($cart['sub_total']) }}
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.row -->
                <hr>
                <div class="row">
                    <div class="col-sm-5">
                        <p>Discount:</p>
                    </div><!-- /.col-sm-5 -->
                    <div class="col-sm-7 sub-total-value">
                        &pound;{{ price($cart['promotion']['discountValue']) }}
                    </div><!-- /.col-sm-7 -->
                    <div class="col-xs-12">
                        <p class="description">
                            {{ $cart['promotion']['promotionDetails'] }}
                        </p><!-- /.description -->
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.row -->
                <hr>
        </div><!-- /.promos -->

    @endif
    <p class="grand-total @if(!empty($cart['promotion'])) discount-applied @endif">Total price:</p><!-- /.grand-total -->

    <div class="price-wrapper {{ (!empty($checkout_step) && $checkout_step == 4) ? 'checkout-complete' : '' }}">
            <p> &pound;<span class="final-price">{{ price($cart['grand_total']) }}</span><span class="vat">Inc. VAT</span></p>
    </div> <!-- .price-wrapper -->

    @if(!empty($checkout_step) && $checkout_step != 4)

        @if($checkout_step == 3)

            <button type="submit" class="btn btn-tertiary btn-large  make-payment" >Buy now</button>

            <div class="paypal-button" style="display:none;" id="paypal-button-2"></div>

        @else
            <button type="submit" class="btn btn-tertiary btn-large">
                    {{ empty($checkout_step) ? 'Next' : 'Step '.  ($checkout_step+1) }}
            </button>
        @endif
    @endif
</div>

