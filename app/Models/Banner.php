<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
	use SoftDeletes;

    /**
     * Relationship to image
     */
    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    /*
    * Only show active banners
    */
    public function scopeActive($query)
    {
        $query->where('banners.active', 1);
    }
}
