<div class="alert alert-danger" role="alert" id="error" style="display: none;">
    <p></p>
</div>

<div class="fitters">

    <div class="form">
        <label>Your Postcode</label>
        <input type="text" name="postcode" id="postcode_update" value="{{ $fitters->postcode }}" class="form-control">
        <button type="button" id="update" class="btn btn-primary btn-primary-double-line">Update<br>Postcode</button>
    </div> <!-- .form -->

    <div class="table-responsive hidden-xs">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th width="50">Type</th>
                    <th class="hide-small-td">Address</th>
                    <th width="100" >City/Postcode</th>
                    <th class="hide-small-td">Distance</th>
                    <th class="hide-small-td">Earliest Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($fitters->fitters as $fitter)
                    <tr class="{{ ($fitter->isMobile ? 'mobile-row' : 'standard-row' ) }}">
                        <td>
                            @if($fitter->isMobile)
                                <i class="icon-mobile-fitter"></i>
                            @else
                                <i class="icon-garage-fitting"></i>
                            @endif
                        </td>
                        <td class="hide-small-td">{{ $fitter->address }}</td>
                        <td>{{ $fitter->city }} {{ $fitter->postcode }}</td>
                        <td class="hide-small-td">{{ $fitter->distance }}</td>
                        <td class="hide-small-td">{{ str_replace('-', ' ', $fitter->earliestAppointmentDate) }}</td>
                        <td><a href="javascript:void(0);" class="btn btn-secondary btn-block book btn-secondary-double-line"
                            data-address="{{ $fitter->address }} {{ $fitter->city }} {{ $fitter->postcode }}"
                            data-postcode="{{ $fitters->postcode }}"
                            data-partNo="{{ $fitters->partNo }}"
                            data-variantTowbarId="{{ $fitters->variantTowbarId }}"
                            data-vehicleDesc="{{ $fitters->vehicleDesc }}"
                            data-fitter="{{ $fitter->partnerId }}"
                            data-fitterType="{{ ($fitter->isMobile ? 'mobile' : 'standard' ) }}"
                            data-electric="">Book<br>Fitting</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>

    <div class="mobile-fittings hidden-lg hidden-md hidden-sm">
        <table class="table table-bordered table-hover">
            <tbody>
                @foreach($fitters->fitters as $fitter)
                    <div class="fitter">
                        @if($fitter->isMobile)
                            <i class="icon-mobile-fitter"></i>
                        @else
                            <i class="icon-garage-fitting"></i>
                        @endif

                        <p class="name">{{ $fitter->address }} <br />{{ $fitter->city }} {{ $fitter->postcode }}</p>
                        <hr />
                        <div class="date-distance clearfix">
                            <p><span>Distance:</span> <br />{{ $fitter->distance }}</p>
                            <p><span>Earliest Date:</span> <br />{{ str_replace('-', ' ', $fitter->earliestAppointmentDate) }}</p>
                        </div> <!-- /.date-distance -->

                        <p><a href="javascript:void(0);" class="btn btn-secondary btn-block book"
                            data-address="{{ $fitter->address }} {{ $fitter->city }} {{ $fitter->postcode }}"
                            data-postcode="{{ $fitters->postcode }}"
                            data-partNo="{{ $fitters->partNo }}"
                            data-variantTowbarId="{{ $fitters->variantTowbarId }}"
                            data-vehicleDesc="{{ $fitters->vehicleDesc }}"
                            data-fitter="{{ $fitter->partnerId }}"
                            data-fitterType="{{ ($fitter->isMobile ? 'mobile' : 'standard' ) }}"
                            data-electric="">Book<br>Fitting</a></p>
                    </div>
                @endforeach
            </tbody>
        </table>
    </div> <!-- /.mobile-fittings hidden-lg hidden-md hidden-sm -->

    <div class="row">
        <div class="legend">
            <div class="col-sm-4 col-xs-6">
                <div class="key">
                    <i class="icon-garage-fitting"></i>
                    <p>Fitting Centre<br><span>(You go to them)</span><p>
                </div> <!-- .key -->
            </div> <!-- .col-md-3 -->

            <div class="col-sm-4 col-xs-6">
                <div class="key">
                    <i class="icon-mobile-fitter"></i>
                    <p>Mobile Fitter<br><span>(They come to you, with an additional charge of £25)</span><p>
                </div> <!-- .key -->
            </div> <!-- .col-md-3 -->

            <div class="col-sm-4 col-xs-12">
                <p class="text-right"><strong>N.B. All distances are 'as the crow flies'</p>
            </div> <!-- .col-md-6 -->
        </div> <!-- .legend -->
    </div> <!-- .row -->

</div> <!-- .fitters -->

<script>
    $(document).ready(function(){
        $(document).on('click', '.book', function(){
            $('.fitters').fadeOut('slow');

            $.ajax({
                type: 'POST',
                url: "{{ $dates_route }}",
                data: {
                    'address': $(this).attr('data-address'),
                    'postcode': $(this).attr('data-postcode'),
                    'partNo': $(this).attr('data-partNo'),
                    'partnerId': $(this).attr('data-fitter'),
                    'fitterType': $(this).attr('data-fitterType'),
                    'caravanMover': {{ request()->caravanMover ?? 'false' }},
                },
                success: function(result){
                    if(result.success == false) {
                        $('#error p').html(result.errorMessage);
                        $('#error').fadeIn('slow');
                    } else {
                        $('#modal .modal-content .modal-body').html(result.modal);
                    }
                },
                error: function(){
                    alert('Sorry, something went wrong');
                }
            });
        });

        // Update postcode click
        $('#update').on('click', function(){
            $('#postcode').val($('#postcode_update').val());

            $('#modal').modal('hide');
            $('#find-fitter').click(); // reinitialize everything
        });
    });
</script>
