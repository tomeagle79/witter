@extends('layouts.backend')

@section('actions')
	<a href="{!! URL::backend('admin_users/create') !!}" class="pull-right btn btn-success">Add User</a>
@stop

@section('title')
	Admin Users
@stop

@section('scripts')
<script>
$(document).ready(function(){
	$('.datatable').DataTable( {
        "ajax": "{!! URL::backend('admin_users/index_datatables') !!}",
        "processing": true,
        "serverSide": true,
        "columns": [
            null,
            null,
            null,
            null,
            null,
            {"searchable": false, "orderable": false},
            {"searchable": false, "orderable": false},
        ]
    } );
});
</script>
@stop

@section('content')
<table class="table table-hover table-striped table-bordered datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Email</th>
			<th>Created</th>
			<th>Updated</th>
			<th width="50"></th>
			<th width="50"></th>
		</tr>
	</thead>
	<tbody></tbody>
</table>

@stop
