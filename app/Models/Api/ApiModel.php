<?php

namespace App\Models\Api;

use App\Models\Error;
use Illuminate\Support\Facades\Log;
use Mail;
use Request;

class ApiModel
{
    /**
     * Method to GET.  Abstracted so we can handle and catch errors easily
     */
    public static function get_request($url, $protectIsSellable = false)
    {
        try {
            $download = file_get_contents($url);
        } catch (\Exception $e) {

            // Add error to error log
            $error = new Error;
            $error->user_agent = \Request::header('User-Agent');
            $error->ip_address = \Request::ip();
            $error->url = url()->full();
            $error->api_url = $url;
            $error->error_text = $e->getMessage();

            $error->save();

            // Email devs
            Mail::send('emails.api_error', ['error' => $error], function ($m) {
                $m->from('no-reply@witter-towbars.co.uk', 'Witter App');

                $m->to(config('witter.developer_email'), 'Reckless')
                    ->bcc(config('settings.developer.dev_email', 'Reckless'))
                    ->subject('Witter API Error');
            });

            abort(500);

        }
        $obj = json_decode($download);

        // Log all the API calls
        if (empty($obj)) {
            Log::debug("Empty data set returned \n\r" . $url . " link " . url()->full());

            // Just save the headers of the empty calls as we are logging too much logs become useless.
            Log::info(json_encode($download, JSON_PRETTY_PRINT) . "\n\r headers" . json_encode($http_response_header, JSON_PRETTY_PRINT));
        } else {
            Log::info("Full data set returned \n\r" . $url . " link " . url()->full());
        }

        if ($protectIsSellable) {
            // It's a single object
            if (isset($obj->isSellable)) {
                if ($obj->isSellable == true) {
                    return $obj;
                } else {
                    return null;
                }
            }

            // It's a collection
            $new_obj = [];
            foreach ($obj as $object) {
                if ($object->isSellable == true) {
                    $new_obj[] = $object;
                }
            }

            return $new_obj;
        }

        return $obj;
    }

    /**
     * Method to POST.  Abstracted so we can handle and catch errors easily
     */
    public static function post_request($url, $data)
    {
        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data),
            ],
        ];
        $context = stream_context_create($options);

        try {
            $result = file_get_contents($url, false, $context);
        } catch (\Exception $e) {

            // Add error to error log
            $error = new Error;
            $error->user_agent = \Request::header('User-Agent');
            $error->ip_address = \Request::ip();
            $error->url = url()->full();
            $error->error_text = $e->getMessage();

            $error->save();

            // Email devs
            Mail::send('emails.api_error', ['error' => $error], function ($m) {
                $m->from('no-reply@witter-towbars.co.uk', 'Witter App');

                $m->to(config('witter.developer_email'), 'Reckless')
                    ->bcc(config('settings.developer.dev_email', 'Reckless'))
                    ->subject('Witter API Error');

            });

            abort(500);

        }

        if ($result === false) {
            Log::error('ApiModel - $result false, request failed');
        }

        $obj = json_decode($result);

        return $obj;
    }
}
