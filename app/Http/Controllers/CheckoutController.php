<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckoutSection1Request;
use App\Http\Requests\CheckoutSection2Request;
use App\Http\Requests\CheckoutVehicleDetailsRequest;
use App\Models\Api\Appointment;
use App\Models\Api\Towbar;
use App\Models\Api\WitterOrder;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\StripePayment;
use App\Services\MailChimp;
use App\Services\PayPal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Mail;
use Session;
use \Stripe\Stripe;

class CheckoutController extends Controller
{
    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        $this->middleware('has_cart');

        // Set checkout_layout as true
        $this->data['checkout_layout'] = true;

    }

    /**
     * Start checkout - Section 1
     *
     * @return Response
     */
    public function cardholder_details()
    {
        $this->data['checkout_step'] = 1;

        $this->data['checkout'] = Session::get('checkout');

        $this->data['cart'] = build_cart();

        // If new_appointment_required is true redirect user to cart
        if(!empty($this->data['cart']['new_appointment_required'])) {
            return redirect(route('cart'))->with('error', 'Please select a new fiting appointment.');
        }

        // Check the fitting type - needed for the delivery address / location
        $this->data['fitting_type'] = $this->getFittingType();

        return view('checkout.cardholder_details', $this->data);
    }

    /**
     * Prcoess Section 1
     *
     * @return Response
     */
    public function section1_submit(CheckoutSection1Request $request)
    {
        // Build a session
        $checkout = Session::get('checkout');

        $checkout['section1'] = [
            'title' => $request->title,
            'first_name' => $request->first_name,
            'surname' => $request->surname,
            'telephone' => $request->telephone,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'billing_postcode' => $request->billing_postcode,
            'billing_address1' => $request->billing_address1,
            'billing_address2' => $request->billing_address2,
            'billing_towncity' => $request->billing_towncity,
            'billing_county' => $request->billing_county,
            'billing_country' => $request->billing_country,
        ];

        // If the user submits vehicle details then save to the checkout session.
        if(!empty($request->vehicle)) {
            $checkout['sectionVehicle'] = $request->vehicle;
        }

        Session::put('checkout', $checkout);

        return redirect(route('checkout.section2'));
    }

    /**
     * Delivery details
     *
     * @return Response
     */
    public function delivery_details()
    {
        $this->data['checkout_step'] = 2;

        $this->data['cart'] = build_cart();

        // If new_appointment_required is true redirect user to cart
        if(!empty($this->data['cart']['new_appointment_required'])) {
            return redirect(route('cart'))->with('error', 'Please select a new fiting appointment.');
        }

        // Check the fitting type - needed for the delivery address / location
        $this->data['fitting_type'] = $this->getFittingType();

        $this->data['checkout'] = Session::get('checkout');

        // Set the default 'customer_postcode' as null.
        $this->data['customer_postcode'] = null;

        // If the user has chosen mobile fitting then get the postcode they have arealy entered.
        if($this->data['fitting_type'] == 'mobile'){

            // Check if the users postcode is in the towbar details.
            if(!empty($this->data['cart']['towbar']['customer_postcode'])) {

                $this->data['customer_postcode'] = strtoupper($this->data['cart']['towbar']['customer_postcode']);
            }else{
                // Loop the items the user has in the cart
                if(!empty($this->data['cart']['contents'])) {
                    foreach ($this->data['cart']['contents'] as $accessory) {

                        // If the item is a 'accessory-fitted'
                        if($accessory['type'] == 'accessory-fitted'){

                            // if the user has added their postcode alreay then use it.
                            if(!empty($accessory['customer_postcode'])) {

                                $this->data['customer_postcode'] = strtoupper($accessory['customer_postcode']);
                            }
                        }
                    }
                }
            }
        }

        // If the existing delivery address has a different postcode to our customer_postcode then remove the address from the session.
        if(!empty($this->data['checkout']['section2']['delivery_postcode']) && !empty($this->data['customer_postcode'])){

            if($this->data['checkout']['section2']['delivery_postcode'] != $this->data['customer_postcode']){
                unset($this->data['checkout']['section2']['delivery_address1']);
                unset($this->data['checkout']['section2']['delivery_address2']);
                unset($this->data['checkout']['section2']['delivery_towncity']);
                unset($this->data['checkout']['section2']['delivery_county']);
            }
        }

        return view('checkout.delivery_details', $this->data);
    }

    /**
     * Process Section 2
     *
     * @return Response
     */
    public function section2_submit(CheckoutSection2Request $request)
    {
        // Build a session
        $checkout = Session::get('checkout');

        // Get the cart see if we need to send user to sectionVehicle page
        $cart = build_cart();

        $checkout['section2'] = [
            'delivery_telephone' => $request->delivery_telephone,
            'delivery_postcode' => $request->delivery_postcode,
            'delivery_address1' => $request->delivery_address1,
            'delivery_address2' => $request->delivery_address2,
            'delivery_towncity' => $request->delivery_towncity,
            'delivery_county' => $request->delivery_county,
            'delivery_country' => $request->delivery_country,
        ];

        Session::put('checkout', $checkout);

        return redirect(route('checkout.section3'));
    }

    /**
     * Payment options
     *
     * @return Response
     */
    public function payment_options(Request $request)
    {
        $this->data['checkout_step'] = 3;

        $this->data['cart'] = build_cart();

        if(!empty($this->data['cart']['new_appointment_required'])) {
            return redirect(route('cart'))->with('error', 'Please select a new fiting appointment.');
        }

        $this->data['checkout'] = Session::get('checkout');

        // Allow telephone
        if (in_array($request->ip(), config('settings.enable_phone_payments_by_ip'))) {
            $this->data['phone_payment_option'] = true;
        } else {
            $this->data['phone_payment_option'] = false;
        }

        // Check the fitting type - needed for the delivery address / location
        $this->data['fitting_type'] = $this->getFittingType();

        return view('checkout.payment_options', $this->data);
    }

    /**
     * Create payment request for PayPal.
     */
    public function create_payment_paypal(PayPal $paypal)
    {
        /**
         * Set the address.
         */
        //$paypal->shipping_address();

        return $paypal->create_payment();
    }

    /**
     * Charge the customer, return json
     *
     * @return Response
     */
    public function charge(Request $request)
    {
        $cart = build_cart();
        $checkout = Session::get('checkout');

        $arr = []; // This will be sent to the user as json

        if (empty($cart) || empty($checkout)) {
            $arr['success'] = false;
            $arr['error'] = 'Sorry, there was a problem.  Your session may have timed out.  Please try again and if the problem persists please visit your cart again.';
            return response()->json($arr);
        }

        if (empty($cart['contents']) && empty($cart['towbar'])) {
            $arr['success'] = false;
            $arr['error'] = 'Sorry, there was a problem.  Your session may have timed out.  Please try again and if the problem persists please visit your cart again.';
            return response()->json($arr);
        }

        // id comes from Stripe. paymentID comes from PayPal.  Without it there is an issue
        if (empty($request->id) && empty($request->paymentID)) {
            $arr['success'] = false;
            $arr['error'] = 'Sorry, there was a problem.  Please try again and if the problem please contact us.';
            return response()->json($arr);
        }

        // Need to check if the fitter is mobile or not and pass it beck to the API.
        if (!empty($cart['towbar']['fitterType']) && $cart['towbar']['fitterType'] == 'mobile') {
            $is_mobile = "Y";
        } else {
            $is_mobile = "N";
        }

        if (!empty($cart['towbar']) && $cart['towbar']['fitterType'] != 'self_fitted') {
            // Create a provisional booking
            $booking = $this->provisional_appointment($cart, $checkout, $is_mobile, $accessory = false);

            if (!$booking) {
                $arr['success'] = false;
                $arr['error'] = 'Sorry, but your time slot has been taken by another customer.  Please return to <a href="' . route('towbars') . '">towbars</a> and try another time slot.';
                return response()->json($arr);
            }
        }

        // Create a provisional booking for a fitted accessory
        if (!empty($cart['contents'])) {
            foreach ($cart['contents'] as $item) {
                if ($item['type'] == 'accessory-fitted') {
                    // Need to check if the fitter is mobile or not and pass it beck to the API.
                    if (!empty($item['fitterType']) && $item['fitterType'] == 'mobile') {
                        $is_mobile = "Y";
                    } else {
                        $is_mobile = "N";
                    }

                    $accessory_booking = $this->provisional_appointment($cart, $checkout, $is_mobile, $accessory = true, $item);

                    if (!$accessory_booking) {
                        $arr['success'] = false;
                        $arr['error'] = 'Sorry, but your time slot has been taken by another customer.  Please return to the <a href="' . route('accessories.multi', [encode_url($item['partNo'])]) . '">accessory page</a> and try another time slot.';
                        return response()->json($arr);
                    }
                }
            }
        }

        // Make a stripe charge
        if (!empty($request->id)) {
            return $this->create_stripe_order($request, $cart, $checkout);
        } elseif (!empty($request->paymentID)) {
            // Charge PayPal and cretae an order.
            return $this->create_paypal_order($request, $cart, $checkout);
        } else {
            // General error message
            $arr['success'] = false;
            $arr['error'] = 'There was a problem processing your payment.  You have not been charged.  Please try again or contact us directly.';
            return response()->json($arr);
        }
    }

    /**
     * Charge Stripe
     *
     * @return Response
     */
    public function create_stripe_order($request, $cart, $checkout)
    {
        // Add the charge request to the database.  We do this in case it fails, so we have a log
        // of all successful and failed payments.  Later we just need to update the row.
        $payment = new StripePayment;
        $payment->stripe_id = $request->id;
        $payment->card_id = $request->card['id'];
        $payment->address_city = $request->card['address_city'];
        $payment->address_country = $request->card['address_country'];
        $payment->address_line1 = $request->card['address_line1'];
        $payment->address_line1_check = $request->card['address_line1_check'];
        $payment->address_line2 = $request->card['address_line2'];
        $payment->address_state = $request->card['address_state'];
        $payment->address_zip = $request->card['address_zip'];
        $payment->address_zip_check = $request->card['address_zip_check'];
        $payment->brand = $request->card['brand'];
        $payment->country = $request->card['country'];
        $payment->cvc_check = $request->card['cvc_check'];
        $payment->dynamic_last4 = $request->card['dynamic_last4'];
        $payment->exp_month = $request->card['exp_month'];
        $payment->exp_year = $request->card['exp_year'];
        $payment->funding = $request->card['funding'];
        $payment->last4 = $request->card['last4'];
        $payment->name = $request->card['name'];
        $payment->tokenization_method = $request->card['tokenization_method'];
        $payment->client_ip = $request->client_ip;
        $payment->created = $request->created;
        $payment->type = $request->type;
        $payment->livemode = ($request->livemode == true ? 1 : 0);
        $payment->process = 0; // Set this to 1 after payment taken successfully

        $payment->save();

        \Stripe\Stripe::setApiKey(config('settings.stripe_secret'));

        // Token is created using Stripe.js or Checkout!
        // Get the payment token submitted by the form:
        $token = $request->id;

        try {
            // Charge the user's card:
            $charge = \Stripe\Charge::create(array(
                "amount" => (int) ($cart['grand_total'] * 100), // Stripe accepts an int, no decimals: 1000 = 10.00
                "currency" => "gbp",
                "description" => "Witter Towbars",
                "source" => $token,
            ));

            // Save the charge data
            $payment->charge_id = $charge->id;
            $payment->amount = $charge->amount;
            $payment->amount_refunded = $charge->amount_refunded;
            $payment->application = $charge->application;
            $payment->application = $charge->application;
            $payment->application_fee = $charge->application_fee;
            $payment->balance_transaction = $charge->balance_transaction;
            $payment->captured = $charge->captured;
            $payment->currency = $charge->currency;
            $payment->customer = $charge->customer;
            $payment->description = $charge->description;
            $payment->destination = $charge->destination;
            $payment->dispute = $charge->dispute;
            $payment->failure_code = $charge->failure_code;
            $payment->failure_message = $charge->failure_message;
            $payment->fraud_details = json_encode($charge->fraud_details);
            $payment->invoice = $charge->invoice;
            $payment->on_behalf_of = $charge->on_behalf_of;
            $payment->order = $charge->order;
            $payment->outcome_network_status = $charge->outcome->network_status;
            $payment->outcome_reason = $charge->outcome->reason;
            $payment->outcome_risk_level = $charge->outcome->risk_level;
            $payment->outcome_seller_message = $charge->outcome->seller_message;
            $payment->outcome_type = $charge->outcome->type;
            $payment->paid = $charge->paid;
            $payment->receipt_email = $charge->receipt_email;
            $payment->receipt_number = $charge->receipt_number;
            $payment->review = $charge->review;
            $payment->shipping = $charge->shipping;
            $payment->source_transfer = $charge->source_transfer;
            $payment->statement_descriptor = $charge->statement_descriptor;
            $payment->status = $charge->status;
            $payment->transfer_group = $charge->transfer_group;

            $payment->save();
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err = $body['error'];

            $arr['success'] = false;
            $arr['error'] = $err['message'];

            return response()->json($arr);
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $arr['success'] = false;
            $arr['error'] = 'Too many requests were made too quickly to our payment provider.  You have not been charged.  Please try again.';

            // Email devs
            Mail::send('emails.stripe_error', ['error' => $arr['error']], function ($m) {
                $m->from('no-reply@witter-towbars.co.uk', 'Witter App');
                $m->to(config('witter.developer_email'), 'Reckless')->subject('Witter - Stripe - Too Many Requests');
            });

            return response()->json($arr);
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $arr['success'] = false;
            $arr['error'] = 'There was a problem making your payment and you have not been charged.  Please try again or contact us directly.';

            // Email devs
            Mail::send('emails.stripe_error', ['error' => $arr['error']], function ($m) {
                $m->from('no-reply@witter-towbars.co.uk', 'Witter App');
                $m->to(config('witter.developer_email'), 'Reckless')->subject('Witter - Stripe - Invalid Parameters');
            });

            return response()->json($arr);
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $arr['success'] = false;
            $arr['error'] = 'There was a problem contacting our payment provider.  You have not been charged.  Please try again or contact us directly.';

            // Email devs
            Mail::send('emails.stripe_error', ['error' => $arr['error']], function ($m) {
                $m->from('no-reply@witter-towbars.co.uk', 'Witter App');
                $m->to(config('witter.developer_email'), 'Reckless')->subject('Witter - Stripe - Unable to Auth');
            });

            return response()->json($arr);
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $arr['success'] = false;
            $arr['error'] = 'There was a problem contacting our payment provider.  You have not been charged.  Please try again or contact us directly.';

            // Email devs
            Mail::send('emails.stripe_error', ['error' => $arr['error']], function ($m) {
                $m->from('no-reply@witter-towbars.co.uk', 'Witter App');
                $m->to(config('witter.developer_email'), 'Reckless')->subject('Witter - Stripe - API Connection Failed');
            });

            return response()->json($arr);
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $arr['success'] = false;
            $arr['error'] = 'There was a problem processing your payment.  You have not been charged.  Please try again or contact us directly.';

            // Email devs
            Mail::send('emails.stripe_error', ['error' => $arr['error']], function ($m) {
                $m->from('no-reply@witter-towbars.co.uk', 'Witter App');
                $m->to(config('witter.developer_email'), 'Reckless')->subject('Witter - Stripe - Base Error');
            });

            return response()->json($arr);
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $arr['success'] = false;
            $arr['error'] = 'There was a problem processing your payment.  Please try again or contact us directly.';

            // Email devs
            Mail::send('emails.stripe_error', ['error' => $arr['error'], 'exceptmsg' => $e->getMessage()], function ($m) {
                $m->from('no-reply@witter-towbars.co.uk', 'Witter App');
                $m->to(config('witter.developer_email'), 'Reckless')->subject('Witter - Stripe - Generic Error');
            });

            return response()->json($arr);
        }

        $payment->process = 1;
        $payment->save();

        $arr = $this->create_order($request, $cart, $checkout, $payment->charge_id, $phone_order = false, $paid = true, $payment_type = 'Stripe');

        return response()->json($arr);
    }

    public function provisional_appointment($cart, $checkout, $is_mobile, $accessory = false, $item = null)
    {
        if (!$accessory) {
            $booking = Appointment::create_provisional_booking([
                'vtId' => $cart['towbar']['variantTowbarId'],
                'timeslotId' => $cart['towbar']['appointmentId'],
                'bookingTime' => $cart['towbar']['appointmentTime'],
                'appointmentDate' => $cart['towbar']['appointmentDate'],
                'customerEmail' => $checkout['section1']['email'],
                'isMobile' => $is_mobile,
            ]);
        } else {
            $booking = Appointment::create_provisional_booking([
                'partNo' => $item['partNo'],
                'timeslotId' => $item['appointmentId'],
                'bookingTime' => $item['appointmentTime'],
                'appointmentDate' => $item['appointmentDate'],
                'customerEmail' => $checkout['section1']['email'],
                'isMobile' => $is_mobile,
            ]);
        }

        if ($booking->success == false) {
            Log::error('provisional_appointment() accessory = ' . ($accessory ? 'yes' : 'no') . ' error: ' . json_encode($booking));

            return false;
        }

        Log::debug('provisional_appointment() accessory = ' . ($accessory ? 'yes' : 'no') . ' success: ' . json_encode($booking));

        return $booking;
    }

    /**
     * Charge Paypal
     *
     * @return Response
     */
    public function create_paypal_order($request, $cart, $checkout)
    {
        // Use the paypal service class to exicute the payment.
        $paypal = new Paypal;
        $payment = $paypal->charge_payment($request->paymentID, $request->payerID, $cart['grand_total']);

        // Check if there were any errors while charging PayPal
        if (is_array($payment) && $payment['success'] === false) {
            // Return the error array.
            return response()->json($payment);
        } else {
            // Create an order if the charge was a success.
            $arr = $this->create_order($request, $cart, $checkout, $payment->id, $phone_order = false, $paid = true, $payment_type = 'PayPal');

            return response()->json($arr);
        }
    }

    /**
     * Checkout completed
     *
     * @return Response
     */
    public function complete()
    {
        $this->data['checkout_step'] = 4;

        // Get the cart and payment info, then destroy it
        $this->data['cart'] = build_cart();
        $this->data['checkout'] = Session::get('checkout');
        $this->data['payment'] = Session::get('payment');
        $this->data['revenue_amt'] = price($this->data['payment']["amount"] - $this->data['payment']["vat_total"]);

        $order = Order::where('witter_order_id', '=', $this->data['payment']['order_id'])
            ->first();

        if ($order->has_tracked == 0) {
            $this->data['run_tracking'] = true;
            $order->has_tracked = 1;
            $order->save();

            if ($order->agreed_contact || $order->agreed_market_research) {
                // Use the MailChimp service class to add user to Witter list.
                $mailchimp = new MailChimp;

                // Add the user to the general offers contact list
                if ($order->agreed_contact) {
                    $mailchimp->add_user($order, config('mailchimp.contact_list'));
                }

                // Add the user to the market research contact list
                if ($order->agreed_market_research) {
                    $mailchimp->add_user($order, config('mailchimp.market_research_list'));
                }
            }
        } else {
            $this->data['run_tracking'] = false;
        }

        Session::forget('cart');
        Session::forget('checkout');
        Session::forget('payment');

        // Check the fitting type - needed for the delivery address / location
        $this->data['fitting_type'] = $this->getFittingType();

        return view('checkout.complete', $this->data);
    }

    /**
     * Create an order.  This handles all order logic for phone and credit card orders
     *
     * @param $cart
     *    Cart object
     * @param $checkout
     *    Checkout object
     * @param $reference
     *    Payment reference - Stripe ID or Request Code for phone orders
     * @param $phone_order
     *    Is it a phone order?
     * @return array
     *    success = bool; error = string; order_id = int;
     */
    public function create_order($request, $cart, $checkout, $reference, $phone_order = false, $paid = true, $payment_type = null)
    {
        $arr = [];

        // Build an accessories array
        $accessories_arr = [];
        $accessories_fitted_arr = [];
        $reg_and_colour_arr = [];
        $mobileFitCost = 0;

        // Check if t's and c's have been selected
        $agreed_terms = empty($request->get('agreed_terms')) ? 0 : 1;
        $agreed_contact = empty($request->get('agreed_contact')) ? 0 : 1;
        $agreed_market_research = empty($request->get('agreed_market_research')) ? 0 : 1;

        if (!empty($cart['contents'])) {
            foreach ($cart['contents'] as $accessory) {
                // If the item is to be fitted then send appointment details.
                if ($accessory['type'] == 'accessory-fitted') {
                    $accessories_fitted_arr[] = [
                        'partNo' => $accessory['partNo'],
                        'Quantity' => 1,
                        'TimeslotId' => $accessory['appointmentId'],
                        'AppointmentDate' => $accessory['appointmentDate'],
                        // 'removalOption' => !is_null($accessory['removal_option']) && $accessory['removal_option'] === "true" ? 'Y' : 'N',
                        // 'removalCost' => !is_null($accessory['removal_cost']) ? $accessory['removal_cost'] : 0,
                        'IncludeRemoval' => !is_null($accessory['removal_option']) && $accessory['removal_option'] === "true" ? 1 : 0,
                    ];

                    if (!empty($accessory['fitterFee'])) {
                        $mobileFitCost = $mobileFitCost + $accessory['fitterFee'];
                    }
                } else {
                    // Non fitted items
                    $accessories_arr[] = [
                        'partNo' => $accessory['partNo'],
                        'quantity' => 1,
                    ];
                }

                // As we are looping items in the cart. Check if any have had vehicle details added.
                if (!empty($checkout['sectionVehicle'][$accessory['cart_item_id']])) {
                    $reg_and_colour_arr[] = [
                        'partNo' => $accessory['partNo'],
                        'RegistrationNo' => $checkout['sectionVehicle'][$accessory['cart_item_id']]['registration_no'],
                        'VehicleColour' => $checkout['sectionVehicle'][$accessory['cart_item_id']]['vehicle_colour'],
                    ];
                }
            }
        }

        // Towbar details
        $towbar_arr = [];
        if (!empty($cart['towbar'])) {
            if ($cart['towbar']['fitterType'] == 'self_fitted') {
                $accessories_arr[] = [
                    'AdditionalInfo' => $cart['towbar']['variantTowbarId'],
                    'partNo' => $cart['towbar']['partNo'],
                    'quantity' => 1,
                ];

                // If the towball_option cost money then add it to the array
                if (!empty($cart['towbar']['towball_option_price'])) {
                    $accessories_arr[] = [
                        'partNo' => $cart['towbar']['towball_option'],
                        'quantity' => 1,
                    ];
                }

                if (!empty($cart['towbar']['electric_option'])) {
                    $accessories_arr[] = [
                        'partNo' => $cart['towbar']['electric_option'],
                        'quantity' => 1,
                    ];
                }
            } else {
                // Normal fitted towbar
                $towbar_arr = [
                    'vtId' => $cart['towbar']['variantTowbarId'],
                    'timeslotId' => $cart['towbar']['appointmentId'],
                    'appointmentDate' => $cart['towbar']['appointmentDate'],
                    'towballCode' => $cart['towbar']['towball_option'],
                    'electricKitCode' => $cart['towbar']['electric_option'],
                    'recodeRequested' => $cart['towbar']['softwareUpgrade'],
                ];

                if (!empty($cart['towbar']['fitterFee'])) {
                    $mobileFitCost = $mobileFitCost + $cart['towbar']['fitterFee'];
                }
            }
        }

        $promo_code = '';
        if (!empty($cart['promotion'])) {
            if (!empty($cart['promotion']['promotionCode'])) {
                $promo_code = $cart['promotion']['promotionCode'];
            }
        }

        // If the order came from a quote add the quote id.
        if(!empty($cart['quote_ref'])){
            $quote_id = $cart['quote_ref'];
        } else {
            $quote_id = null;
        }

        // Create an order with Witter
        $order_arr = [
            'customerFirstName' => $checkout['section1']['first_name'],
            'customerSurname' => $checkout['section1']['surname'],
            'customerTelephone' => $checkout['section1']['telephone'],
            'customerMobile' => $checkout['section1']['mobile'],
            'customerEmail' => $checkout['section1']['email'],
            'customerAddressLine1' => $checkout['section1']['billing_address1'],
            'customerAddressLine2' => $checkout['section1']['billing_address2'],
            'customerAddressTownCity' => $checkout['section1']['billing_towncity'],
            'customerAddressCounty' => $checkout['section1']['billing_county'],
            'customerAddressPostcode' => $checkout['section1']['billing_postcode'],
            'customerAddressCountryCode' => $checkout['section1']['billing_country'],
            'deliveryAddressLine1' => $checkout['section2']['delivery_address1'],
            'deliveryAddressLine2' => $checkout['section2']['delivery_address2'],
            'deliveryAddressTownCity' => $checkout['section2']['delivery_towncity'],
            'deliveryAddressCounty' => $checkout['section2']['delivery_county'],
            'deliveryAddressPostcode' => $checkout['section2']['delivery_postcode'],
            'deliveryAddressCountryCode' => $checkout['section2']['delivery_country'],
            'orderItems' => $accessories_arr,
            'webfitDetails' => $towbar_arr,
            'promotionCode' => $promo_code,
            'totalCost' => $cart['grand_total'],
            'isPhoneOrder' => $phone_order,
            'paymentReference' => $reference,
            'AccessoryFittings' => $accessories_fitted_arr,
            'Trackers' => $reg_and_colour_arr,
            'MobileFitCost' => $mobileFitCost,
            'QuoteID' => $quote_id
        ];

        // If the product is a Towbar then we send over the registration plate data.
        if (!empty($cart['towbar'])) {
            $order_arr['RegistrationNo'] = empty(Session::get('registration_no')) ? '' : Session::get('registration_no');
        }

        // Has the order been paid for yet? (This is in reverse, defaults to 0 if not provided)
        $order_arr['NotPaid'] = $paid ? 0 : 1;

        // What type of payment method was used?
        $order_arr['PaymentType'] = $payment_type;

        // If it's a phone order, change the PaymentType to 'Phone'.
        if ($phone_order) {
            $order_arr['PaymentType'] = 'Phone';
        }

        $witter_order = WitterOrder::create_order($order_arr);

        // Is order successful?
        if ($witter_order->success == true) {
            Session::put('payment', [
                'amount' => $cart['grand_total'],
                'order_id' => $witter_order->orderId,
                'order_total' => $witter_order->orderTotal,
                'vat_total' => $witter_order->vatTotal,
            ]);

            // Create an internal order
            $internal_order = new Order;
            $internal_order->witter_order_id = $witter_order->orderId;
            $internal_order->payment_reference = $reference;
            $internal_order->payment_type = $payment_type;
            $internal_order->paid = $paid;
            $internal_order->customer_first_name = $checkout['section1']['first_name'];
            $internal_order->customer_surname = $checkout['section1']['surname'];
            $internal_order->customer_telephone = $checkout['section1']['telephone'];
            $internal_order->customer_email = $checkout['section1']['email'];
            $internal_order->customer_address1 = $checkout['section1']['billing_address1'];
            $internal_order->customer_address2 = $checkout['section1']['billing_address2'];
            $internal_order->customer_towncity = $checkout['section1']['billing_towncity'];
            $internal_order->customer_county = $checkout['section1']['billing_county'];
            $internal_order->customer_postcode = $checkout['section1']['billing_postcode'];
            $internal_order->customer_country = $checkout['section1']['billing_country'];
            $internal_order->delivery_address1 = $checkout['section2']['delivery_address1'];
            $internal_order->delivery_address2 = $checkout['section2']['delivery_address2'];
            $internal_order->delivery_towncity = $checkout['section2']['delivery_towncity'];
            $internal_order->delivery_county = $checkout['section2']['delivery_county'];
            $internal_order->delivery_postcode = $checkout['section2']['delivery_postcode'];
            $internal_order->delivery_country = $checkout['section2']['delivery_country'];
            $internal_order->sub_total = $cart['sub_total'];
            $internal_order->promotion_code = $promo_code;
            $internal_order->discount_amount = (empty($cart['promotion']['discountValue']) ? null : $cart['promotion']['discountValue']);
            $internal_order->grand_total = $cart['grand_total'];
            $internal_order->is_phone_order = $phone_order;
            $internal_order->agreed_terms = $agreed_terms;
            $internal_order->agreed_contact = $agreed_contact;
            $internal_order->agreed_market_research = $agreed_market_research;
            $internal_order->save();

            // Create items associated with order
            if (!empty($cart['contents'])) {
                foreach ($cart['contents'] as $accessory) {
                    $internal_order_item = new OrderItem;
                    $internal_order_item->item_type = 'accessory';
                    $internal_order_item->order_id = $internal_order->id;
                    $internal_order_item->accessory_part_no = $accessory['partNo'];
                    $internal_order_item->accessory_qty = 1;
                    $internal_order_item->price = $accessory['details']->price;
                    $internal_order_item->save();
                }
            }

            // Create towbar associated with order
            if (!empty($cart['towbar'])) {
                $internal_order_item = new OrderItem;
                $internal_order_item->item_type = 'towbar';
                $internal_order_item->order_id = $internal_order->id;
                $internal_order_item->towbar_vtid = $cart['towbar']['variantTowbarId'];
                $internal_order_item->towbar_appointment_id = $cart['towbar']['appointmentId'];
                $internal_order_item->towbar_appointment_date = $cart['towbar']['appointmentDate'];
                $internal_order_item->towbar_appointment_time = $cart['towbar']['appointmentTime'];
                $internal_order_item->towbar_partner_id = $cart['towbar']['partnerId'];
                $internal_order_item->towbar_partner_address = $cart['towbar']['address'];
                $internal_order_item->towbar_registration_date = $cart['towbar']['registrationDate'];
                $internal_order_item->towbar_towball_part_no = $cart['towbar']['towball_option'];
                $internal_order_item->towbar_towball_price = empty($cart['towbar']['towball_option_price']) ? null : $cart['towbar']['towball_option_price'];
                $internal_order_item->towbar_electric_option_part_no = $cart['towbar']['electric_option'];
                $internal_order_item->towbar_electric_option_price = empty($cart['towbar']['electric_option_price']) ? null : $cart['towbar']['electric_option_price'];
                $internal_order_item->price = $cart['towbar']['final_price'];
                $internal_order_item->save();
            }

            $arr['success'] = true;
            $arr['order_id'] = $witter_order->orderId;
        } else {
            $arr['success'] = false;
            $arr['error'] = 'There was a problem with your order. Please contact us on 01244 284555, and quote ' . $reference;

            // Rather than passing the error message from the API to the front-end we simply log the error message
            if (!empty($witter_order->reasonForUnsuccessful)) {
                Log::debug("Creating an order failed : Info from API " . $witter_order->reasonForUnsuccessful);
            }
            Log::debug("Creating an order failed. 'WitterOrder::create_order'. Payment ref: " . $reference);
        }
        return $arr;
    }

    /**
     * Check if an order has had its analytics fired already.  This is called
     * by AJAX on the checkout success page.  Used to prevent duplicate Analytics
     * events from firing.  The page is locked to session, which is cleared, but
     * some browsers can cache the entire response, especially when tabs are left
     * open and computers put to sleep/awoken again.
     *
     * We do an AJAX check to see if the order has already had the Analytics fired.
     * If so, return true.  If not, update the field to true so it can't fire again
     * and return false.
     */
    public function has_analytics_fired($order_id)
    {
        // Grab order
        $order = Order::where('witter_order_id', '=', $order_id)->first();

        if ($order->analytics_fired == true) {
            return ['shouldFire' => false];
        } else {
            $order->analytics_fired = true;
            $order->save();
            return ['shouldFire' => true];
        }
    }

    /**
     * Prepare a phone request
     *
     * @return Response
     */
    public function phone_prepare(Request $request)
    {
        // Grab the session details
        $cart = build_cart();
        $checkout = Session::get('checkout');

        $arr = [];

        if (empty($cart) || empty($checkout)) {
            $arr['success'] = false;
            $arr['error'] = 'Sorry, there was a problem.  Your session may have timed out.  Please try again and if the problem persists please visit your cart again.';
            return response()->json($arr);
        }

        if (empty($cart['contents']) && empty($cart['towbar'])) {
            $arr['success'] = false;
            $arr['error'] = 'Sorry, there was a problem.  Your session may have timed out.  Please try again and if the problem persists please visit your cart again.';
            return response()->json($arr);
        }

        $amount = $cart['grand_total'];
        $first_name = $checkout['section1']['first_name'];
        $surname = $checkout['section1']['surname'];
        $telephone = $checkout['section1']['telephone'];

        $data = [
            'firstName' => $first_name,
            'surname' => $surname,
            'telephone' => $telephone,
            'totalCost' => $amount,
        ];

        // Need to check if the fitter is mobile or not and pass it beck to the API.
        if (!empty($cart['towbar']['fitterType']) && $cart['towbar']['fitterType'] == 'mobile') {
            $is_mobile = "Y";
        } else {
            $is_mobile = "N";
        }

        // Make a provisional booking for the webfit
        if (!empty($cart['towbar'])) {
            // Create a provisional booking
            $booking = Appointment::create_provisional_booking([
                'vtId' => $cart['towbar']['variantTowbarId'],
                'timeslotId' => $cart['towbar']['appointmentId'],
                'bookingTime' => $cart['towbar']['appointmentTime'],
                'appointmentDate' => $cart['towbar']['appointmentDate'],
                'customerEmail' => $checkout['section1']['email'],
                'isMobile' => $is_mobile,
            ]);

            if ($booking->success == false) {
                $arr['success'] = false;
                $arr['error'] = 'Sorry, but your time slot has been taken by another customer.  Please return to <a href="' . route('towbars') . '">towbars</a> and try another time slot.';
                return response()->json($arr);
            }
        }

        // Create a provisional booking for a fitted accessory
        if (!empty($cart['contents'])) {
            foreach ($cart['contents'] as $item) {
                if ($item['type'] == 'accessory-fitted') {
                    // Need to check if the fitter is mobile or not and pass it beck to the API.
                    if (!empty($item['fitterType']) && $item['fitterType'] == 'mobile') {
                        $is_mobile = "Y";
                    } else {
                        $is_mobile = "N";
                    }

                    $accessory_booking = Appointment::create_provisional_booking([
                        'partNo' => $item['partNo'],
                        'timeslotId' => $item['appointmentId'],
                        'bookingTime' => $item['appointmentTime'],
                        'appointmentDate' => $item['appointmentDate'],
                        'customerEmail' => $checkout['section1']['email'],
                        'isMobile' => $is_mobile,
                    ]);

                    if ($accessory_booking->success == false) {
                        $arr['success'] = false;
                        $arr['error'] = 'Sorry, but your time slot has been taken by another customer.  Please return to the <a href="' . route('accessory.view', [encode_url($item['partNo'])]) . '">accessory page</a> and try another time slot.';
                        return response()->json($arr);
                    }
                }
            }
        }

        // Commence phone order
        $prepare = WitterOrder::phone_request_prepare($data);

        // It failed
        if ($prepare->success != true) {
            $arr['success'] = false;
            $arr['error'] = $prepare->reasonForUnsuccessful;

            return response()->json($arr);
        }

        // Success, return
        $arr['success'] = true;
        $arr['request_code'] = $prepare->requestCode;

        return response()->json($arr);
    }

    /**
     * Process a request
     *
     * @return Response
     */
    public function phone_request(Request $request)
    {
        // Grab the session details
        $cart = build_cart();
        $checkout = Session::get('checkout');
        $request_code = $request->requestCode;
        $response_code = $request->responseCode;

        $arr = [];

        if (empty($cart) || empty($checkout)) {
            $arr['success'] = false;
            $arr['error'] = 'Sorry, there was a problem.  Your session may have timed out.  Please try again and if the problem persists please visit your cart again.';
            return response()->json($arr);
        }

        if (empty($cart['contents']) && empty($cart['towbar'])) {
            $arr['success'] = false;
            $arr['error'] = 'Sorry, there was a problem.  Your session may have timed out.  Please try again and if the problem persists please visit your cart again.';
            return response()->json($arr);
        }

        if (empty($request_code)) {
            $arr['success'] = false;
            $arr['error'] = 'Please enter the code given to you by the call centre when you place your order.';
            return response()->json($arr);
        }

        if (empty($response_code)) {
            $arr['success'] = false;
            $arr['error'] = 'There was a problem with the response code from the call centre, please try again.';
            return response()->json($arr);
        }

        $check = WitterOrder::phone_request([
            'requestCode' => $request_code,
            'responseCode' => $response_code,
        ]);

        if (empty($check) || $check->success != true) {
            $arr['success'] = false;
            $arr['error'] = 'Sorry, that code is not valid.  Please ensure you are entering the correct code from the call centre.';
            return response()->json($arr);
        }

        // Create order
        $arr = $this->create_order($request, $cart, $checkout, $request_code, $phone_order = true, $paid = true, $payment_type = 'Phone');

        // Success, return
        return response()->json($arr);
    }

    /**
     * Process a request
     *
     * @return Response
     */
    private function getFittingType()
    {
        // Based on the fitting type we know where the products are delivered.
        // Check if the user has a fitting appointment.
        $fitting_type = null;

        // Check if there is a towbar in the cart
        if (!empty($this->data['cart']['towbar']))   {

            $fitting_type = $this->data['cart']['towbar']['fitterType'];
        }

        // Check if there are fitted accessoriess in the cart
        if (!empty($this->data['cart']['contents'])) {

            foreach ($this->data['cart']['contents'] as $item) {
                // Check if the accessory fitted and has an appointment
                if ($item['type'] == 'accessory-fitted' && !empty($item['appointmentId'])) {

                    $fitting_type = $item['fitterType'];
                    break;
                }
            }
        }

        return $fitting_type;
    }
}
