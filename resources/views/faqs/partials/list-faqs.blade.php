

	<div class="faqs-panel panel-group">
	    <div class="panel panel-default">

	    	@foreach($faqs as $faq)
				<a data-toggle="collapse" href="#collapse_{!! $faq->id !!}" class="collapsed">
				    <div class="panel-heading">
				        <h4 class="panel-title title-text">
				        	{{ $faq->faq }}
				        </h4>
				    </div>
			    </a>
			    <div id="collapse_{!! $faq->id !!}" class="panel-collapse collapse">
			    	<div class="panel-footer">
			    		{!! $faq->answer !!}
			    	</div>
			    </div>
			@endforeach
	    </div>
	</div>