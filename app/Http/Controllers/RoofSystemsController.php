<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Api\Commercial\CommercialType;
use App\Models\Api\Commercial\CommercialVehicleManufacturer;
use App\Models\Api\Commercial\CommercialVehicleModel;
use App\Models\Api\Commercial\CommercialVehicle;
use App\Models\Api\VehicleManufacturer;
use App\Models\Api\VehicleModel;
use App\Models\Api\Accessory;
use App\Models\Api\PartClass;

class RoofSystemsController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Witter Roof Systems', route('roof-systems'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['types'] = CommercialType::get_all();
        $this->data['manufacturers'] = CommercialVehicleManufacturer::get_all();

        return view('roof-systems.index', $this->data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function models($manufacturer_slug)
    {
        $this->data['manufacturer_slug'] = $manufacturer_slug;
        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        $this->data['models'] = CommercialVehicleModel::get_where(['makeSlug' => $manufacturer_slug]);

        $this->data['breadcrumbs']->addCrumb($manufacturer->manufacturerName, route('roof-systems.models', $manufacturer_slug));

        return view('roof-systems.bymanufacturer.models', $this->data);
    }

    /**
     * Display a list of vehicles
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicles($manufacturer_slug, $model_slug)
    {
        $this->data['manufacturer_slug'] = $manufacturer_slug;
        $this->data['model_slug'] = $model_slug;

        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);
        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        $this->data['vehicles'] = CommercialVehicle::get_where([
            'makeSlug' => $manufacturer_slug,
            'modelSlug' => $model_slug
        ]);

        // If there are no vehicles to display get the reg details for our message and form.
        if(empty($this->data['vehicles'])){

            $this->data['back_link'] = route('roof-systems.models.vehicles', [$manufacturer_slug, $model_slug]);
        }

        $this->data['breadcrumbs']->addCrumb($manufacturer->manufacturerName, route('roof-systems.models', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($model->modelName, route('roof-systems.models.vehicles', [$manufacturer_slug, $model_slug]));

        return view('roof-systems.bymanufacturer.vehicles', $this->data);
    }

    /**
     * Display a list of products
     *
     * @return \Illuminate\Http\Response
     */
    public function products($manufacturer_slug, $model_slug, $vehicle_slug)
    {
        $this->data['manufacturer_slug'] = $manufacturer_slug;
        $this->data['model_slug'] = $model_slug;
        $this->data['model_slug'] = $vehicle_slug;

        $this->data['manufacturer'] = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);
        $this->data['model'] = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        $this->data['roofbars'] = CommercialVehicle::get_roofbars_by_slug($vehicle_slug);
        $this->data['roofdecks'] = CommercialVehicle::get_roofdecks_by_slug($vehicle_slug);
        $this->data['accessories'] = CommercialVehicle::get_accessories_by_slug($vehicle_slug);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('roof-systems.models', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('roof-systems.models.vehicles', [$manufacturer_slug, $model_slug]));
        $this->data['breadcrumbs']->addCrumb('Accessories', route('roof-systems.models.vehicles.products', [$manufacturer_slug, $model_slug, $vehicle_slug]));

        $this->data['has_products'] = false;
        if(!empty($this->data['roofbars'] || !empty($this->data['roofdecks']) || !empty($this->data['accessories']))) {
            $this->data['has_products'] = true;
        }

        return view('roof-systems.bymanufacturer.products', $this->data);
    }

    /**
     * Display a list of accessory categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function accessories()
    {
        $this->data['categories'] = CommercialType::get_sub_categories(3);

        return view('roof-systems.roofaccessories.category', $this->data);
    }

    /**
     * Display a list of accessory categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function accessories_category($slug)
    {
        $this->data['category'] = PartClass::get_subcategory_by_slug('accessories', $slug);
        $this->data['accessories'] = Accessory::get_accessories_by_slug('accessories', $slug);

        return view('roof-systems.roofaccessories.subcategory', $this->data);
    }

}