@extends('layouts.frontend')

@section('title', 'Help and Advice')

@section('heading')
	HELP &amp; ADVICE
@stop

@section('meta_description')
	Witter Towbars HELP &amp; ADVICE
@stop

@section('content')
	@include('help_articles.loop')
@stop
