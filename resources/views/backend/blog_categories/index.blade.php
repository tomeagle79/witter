@extends('layouts.backend')

@section('actions')
	<a href="{!! URL::backend('blog_categories/create') !!}" class="pull-right btn btn-success">Add Blog Category</a>
@stop

@section('title')
	Blog Posts
@stop

@section('scripts')
<script>
$(document).ready(function(){
	$('.datatable').DataTable( {
        "ajax": "{!! URL::backend('blog_categories/index_datatables') !!}",
        "processing": true,
        "serverSide": true,
        "columns": [
            null,
            null,
            null,
            null,
            null,
            {"searchable": false, "orderable": false},
            {"searchable": false, "orderable": false},
        ]
    } );
});
</script>
@stop

@section('content')

<table class="table table-hover table-striped table-bordered datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>URL Slug</th>
			<th>Created</th>
			<th>Updated</th>
			<th width="50"></th>
			<th width="50"></th>
		</tr>
	</thead>
	<tbody></tbody>
</table>

@stop


