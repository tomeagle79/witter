<?php

namespace App\Http\Middleware;

use Closure;

use Session;

class Reckless
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $allowed_ips = ['127.0.0.1', '109.238.64.145'];

        if(!in_array($request->ip(), $allowed_ips)) {
            return redirect('/');
        }

        return $next($request);
    }
}
