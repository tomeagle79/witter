<h2 id="types">THE DIFFERENT TYPES OF CAR BIKE RACKS</h2>
<p>Whether you’re exploring country-cycle paths with the kids or throwing yourself down a mountain trail, a bike rack is a key tool in getting you to the adventure. </p>
<div class="types clearfix">
    <div class="row">
        <div class="tile clearfix">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="image-wrapper">
                       <img src="https://api.witter-towbars.co.uk:8443/api/live/en/pictures/P/1983/" alt="Towball mounted bike rack" >
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h4 class="tile-title">Towball Mounted</h4>
                    <p class="tile-description">
                        One of the easiest and safest ways to transport your bikes, a <a href="/bike-racks/bolt-on-towball-mounted-2-bike-carrier">towball mounted rack</a> is one of the most popular bike rack variations on the market. 
                        <br><br>
                        Attached directly to your towball, there are two variations of a towball mounted bike rack available. These are: 
                        <br><br>
                        <ul>
                          <li>Towball Bolt-on Bike Rack</li>
                          <li>Towball Clamp-on Bike Rack</li>
                        </ul>
                        <br>
                        The significant differences between these two bike racks are how they are attached to your towball. 
                        <br><br>
                        The bolt-on bike rack is physically bolted on to your vehicle’s towball using an adjuster screw. 
                        <br><br>
                        The clamp-on bike rack uses a clamp feature that secures itself around your vehicle’s towball. 
                    </p>
                </div>
            </div>

            <a href="#towball-mounted-bike-rack" class="btn btn-grey more-info smoothscroll">Read More</a>
        </div>
    </div>
    <div class="row">
        <div class="tile clearfix">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="image-wrapper">
                       <img src="https://api.witter-towbars.co.uk:8443/api/live/en/pictures/P/2029/" alt="Towbar mounted bike rack" >
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h4 class="tile-title">Towbar Mounted</h4>
                    <p class="tile-description">
                            Not to be confused with a <strong>towball</strong> mounted bike rack, the <a href="/bike-racks">towbar mounted bike rack</a> version is fitted to the mounting plate of a flange type towbar. 
                            <br>
                            Fitted and removed in seconds, the towbar bike rack can carry up to 4 bikes and tow a caravan or trailer at the same time. 
                    </p>
                </div>
            </div>

            <a href="#towbar-mounted-bike-rack" class="btn btn-grey more-info smoothscroll">Read More</a>
        </div>
    </div>
    <div class="row">
        <div class="tile clearfix">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="image-wrapper">

                        <img src="https://api.witter-towbars.co.uk:8443/api/live/en/pictures/P/2211/" alt="Rear mounted bike rack" >
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h4 class="tile-title">Rear Mounted</h4>
                    <p class="tile-description">
                      Often one of the cheapest bike rack options available, <a href="/accessory/350046600001">rear mounted bike racks</a> are attached to the rear of your vehicle using a variety of straps. 
                        <br>
                        If you’re looking to purchase a rear mounted bike rack, make sure you carefully read the manufacturer's installation instructions - as some bike racks are only designed to fit specific vehicles.
                </div>
            </div>

            <a href="#rear-mounted-bike-rack" class="btn btn-grey more-info smoothscroll">Read More</a>
        </div>
    </div>
    <div class="row">
        <div class="tile clearfix">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="image-wrapper">

                         <img src="https://api.witter-towbars.co.uk:8443/api/live/en/pictures/P/2219/" alt="Roof mounted bike rack" >
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h4 class="tile-title">Roof Mounted</h4>
                    <p class="tile-description">
                        As the name suggests, a <a href="/bike-racks">roof-mounted bike rack</a> is attached to the roof of your vehicle.
                        <br>
                        One thing to be aware of when purchasing this type of rack is that you will need a set of <a href="/roof-systems">roof bars</a> installed on your vehicle.
                    </p>
                </div>
            </div>

            <a href="#roof-mounted-bike-rack" class="btn btn-grey more-info smoothscroll">Read More</a>
        </div>
    </div>
</div>

<hr>

<h2 id="advantages" class="border">What are the advantages and disadvantages of using each rack?</h2>
<div class="row">
    <div class="col-xs-12">
        <p>When purchasing a bike rack, there’s quite a lot that you need to consider:</p>
        <ul>
            <li>How many bikes are you looking to carry?</li>
            <li>Do you have a towbar?</li>
            <li>Are you planning to carry bikes and tow a caravan or trailer?</li>
            <li>Do you need easy access to the boot of your car?</li>
            <li>How big or small is your vehicle?</li>
        </ul>
        <p>If you have the answers to these questions in mind, you can then find the perfect towbar for your vehicle by looking at the advantages and disadvantages below.</p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <h5 class="tile-title" id="towball-mounted-bike-rack">Towball Mounted Bike Rack</h5>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title green">Advantages</p>
        <ul>
            <li>Carry 2, 3 or 4 bikes</li>
            <li>Mounted directly to your towball making it easy to lift bikes onto the rack</li>
            <li>Tilts outwards allowing easy access to the boot</li>
            <li>Allows you to lock the bikes to the rack securely</li>
            <li>Comes with a lighting board</li>
            <li>Fits securely to most vehicles with a towbar</li>
            <li>Very secure and bike friendly option</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title red">Disadvantages</p>
        <ul>
            <li>Needs an extra number plate</li>
            <li>Can’t be removed easily</li>
            <li>Tend to be a more expensive bike rack option </li>
        </ul>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-xs-12">
        <h5 class="tile-title" id="towbar-mounted-bike-rack">Towbar Bike Rack</h5>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title green">Advantages</p>
        <ul>
            <li>Carry 2, 3 or 4 bikes</li>
            <li>Mounted directly to your towball making it easy to lift bikes onto the rack</li>
            <li>Simple and easy to use</li>
            <li>Allows you to carry bikes and tow a caravan or trailer</li>
            <li>Cheaper bike rack option</li>
            <li>Fits securely to most vehicles with a towbar</li>

        </ul>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title red">Disadvantages</p>
        <ul>
            <li>Carry 2, 3 or 4 bikes</li>
            <li>Bikes cannot be securely locked to your car</li>
            <li>Doesn’t allow easy access to the boot of your car</li>
            <li>Needs an extra number plate</li>
        </ul>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-xs-12">
        <h5 class="tile-title" id="rear-mounted-bike-rack">Rear Mounted Bike Rack</h5>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title green">Advantages</p>
        <ul>
            <li>Carry up to 3 bikes</li>
            <li>Cheapest bike rack option </li>
            <li>Don’t need to purchase a towbar or roof bars</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title red">Disadvantages</p>
        <ul>
            <li>This bike rack is usually vehicle specific </li>
            <li>Bikes can not be securely locked to your car </li>
            <li>Needs an extra number plate</li>
            <li>Needs an extra lighting board</li>
        </ul>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-xs-12">
        <h5 class="tile-title" id="roof-mounted-bike-rack">Roof Mounted Bike Rack</h5>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title green">Advantages</p>
        <ul>
            <li>Carry up to 4 bikes depending on the model</li>
            <li>Bikes are mounted to the roof allowing easy access to the rear of the vehicle</li>
            <li>Bikes can be securely locked to the vehicle</li>
            <li>No need to purchase extra number plates or lighting boards</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title red">Disadvantages</p>
        <ul>
            <li>Mounted to the roof making it difficult to lift bikes onto the rack</li>
            <li>Roof carrying limit may limit how much you can carry </li>
            <li>Need to purchase and install roof bars</li>
        </ul>
    </div>
</div>
