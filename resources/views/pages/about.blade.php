@extends('layouts.frontend')

@section('title', 'About')

@section('heading')
	About Witter Towbars
@stop

@section('meta_description')
	About Witter Towbars
@stop

@section('content')

	<div class="container-fluid about-banner">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2>Towbars <span class="small">to Trust</span></h2>

					<p>Since it was founded over half a century ago, Witter Towbars has consistently set the standard for quality, reliability, and value for money.</p>

					<p>The company was formed in 1950 by Colin Witter, who saw an opportunity to meet the growing demand for easy-to-fit towing brackets from caravan and trailer owners who were benefiting from the increased leisure time enjoyed by post-war Britain.</p>

					<p>It was his vision that led to the creation of Witter Towbars, the UK's leading towbar manufacturer. Today, thanks to continuous investment, the Witter name is synonymous with the highest standards of design, quality, safety and security for motorists and road users. From its modern headquarters on Deeside, North Wales, the company supplies a complete range of towbars and accessories through a helpful nationwide network of specialist stockists and fitters.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="timeline">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8 text-center timeline-wrapper">
					<div class="time-line">
						<div class="circle-base blue">Start</div>
						<div class="circle-base red active" data-year="1950">1950</div>
						<div class="circle-base pink" data-year="1958">1958</div>
						<div class="circle-base pink" data-year="1997">1997</div>
						<div class="circle-base pink" data-year="1998">1998</div>
						<div class="circle-base pink" data-year="2007">2007</div>
						<div class="circle-base pink" data-year="2015">2015</div>
					</div>
					<div class="timeline-item" data-text="1950">
						<h2 class="time-line-title" style="font-size:44px;" >The company is formed by Colin Witter</h2>
						<p>
							In 1950 the company Witter Towbars was founded by the late Colin Witter operating from a converted cottage in Chester. Colin saw an opportunity to meet the growing demand for easy to fit towing brackets for caravan and trailer owners who benefited from the increased leisure time in post-war Britain.
						</p>
					</div>
					<div class="timeline-item" data-text="1958" style="display: none;">
						<h2 class="time-line-title" style="font-size:44px;" data-text="1958">The company moved to Canal Side in Chester </h2>
						<p>
							By now the Witter Trademark had been registered and a reputation had been established that would sustain the company for the next 60+ years. The company had out grown its original premises and moved to an  industrial buildings on the canal side within Chester, gradually expanding to adjacent buildings over the following years.
						</p>
					</div>
					<div class="timeline-item" data-text="1997" style="display: none;">
						<h2 class="time-line-title" style="font-size:44px;" data-text="1997">Move from Canal Side to existing premises in Deeside</h2>
						<p>
							In 1997 after 40 years in business the company moved from the Canal side in Chester to the large modern industrial premises located on Deeside Industrial Park. The move allowed for a big expansion of the factory so to towbars could be manufactured on a larger scale and more efficiently.
						</p>
					</div>
					<div class="timeline-item" data-text="1998" style="display: none;">
						<h2 class="time-line-title" style="font-size:44px;" data-text="1998">Type Approval of Towbars was introduced</h2>
						<p>
							In 1998 Type approvals were first introduced meaning all towbars manufactured now had to be approved by the VCA for quality purposes. However, Witter introduced the first type approved UK Towbar in 1995 for SsangYong Musso showing how Witter Towbars has always been at the forefront of the industry.
						</p>
					</div>
					<div class="timeline-item" data-text="2007" style="display: none;">
						<h2 class="time-line-title" style="font-size:44px;" data-text="2007">WebFit programme was released</h2>
						<p>
							Before 2007 Witter Towbars had only dealt with trade stockists and saw an opportunity to be able to deal with the end user as well. The WebFit online towbar selling system, allowed Witter to sell their towbars for a fully fitted price to the general public and as a fully guaranteed product. The WebFit programme continued to grow and Witter now have a nationwide array of Witter recommended Fitters who can fit their towbars.
						</p>
					</div>
					<div class="timeline-item" data-text="2015" style="display: none;">
						<h2 class="time-line-title" style="font-size:44px;" data-text="2015">Introduction of 3 axis Carlos test rig 2015</h2>
						<p>
							As part of continuous efforts to drive innovation and development Witter Towbars installed and commissioned a state of the art 3 Axis test machine in 2015 at their premises in Deeside. The new test rig is capable of multi axial tow bar tests using 3 load directions, with simultaneously introduced forces, as defined by the Carlos TC loading sequence.  Each axis, is capable of being driven independently to allow standard Regulation 55.01 testing and also other unspecified forms of static and dynamic load testing.
						</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="clearfix">
						<h2 class="time-line-title-header">If you were having a towbar fitted around 1950 the chances are you would have visited your local blacksmith!</h2>
						<div class="">
							<p>However, with the passing of the chassis in car construction and the rapidly increasing interest in caravanning and trailers, the towing bracket had to come of age.</p>
							<p>It was Colin Witter, the company's founding father, who identified the growing need for a well designed and engineered product that could be made available to the general towing public.</p>
							<p>His vision led to the creation of Britain's largest manufacturer of towbars with over six million distributed, through a nationwide network of over 250 Witter specialists.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- /.timeline -->

	<div class="clearfix"></div>

	<div class="container-fluid about-banners red">
		<div class="container">
			<div class="row">

				<div class="col-md-12 text-center">
					<h2>Witter Engineering</h2>
				</div>

				<div class="col-md-6 text-right">
					<p>We employ a team of fully qualified engineers who are responsible for the design of your towbar, from first concept through to the issue of production drawings.</p>
					<p>The first stage is to view the new vehicle and develop a design concept which will integrate seamlessly with the vehicle manufacturer’s specified towbar mounting points. </p>
					<p>This includes the need to obtain European Type Approval, efficiency of manufacture, ease of installation and, most importantly, the aesthetics of the finished product on the vehicle.</p>
				</div>

				<div class="col-md-6 text-left">
					<p>All towbars are designed using the latest 3D Computer Aided Design. This gives us great flexibility during the design process. Once a prototype has been developed it is then tested to the European Type Approval requirements of Directive 94/20EC.</p>
					<p>Testing is carried out on our in house servo hydraulic test rigs, and is independently witnessed and authorised by the UK Vehicle Certification Agency (VCA). Only when the VCA are satisfied that we have met all of the criteria of the EC regulations, will they then grant us EC Type Approval for that particular towbar.</p>
				</div>
			</div>

		</div>
	</div>
{{--
	<div class="container meet-the-team">
		<div class="row">

			<div class="col-md-12">
				<h2 class="time-line-title-header">Meet <span class="small">the team</span></h2>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>

		<div class="row about-the-team">
			<div class="col-md-3 col-sm-6 col-xs-12 team-member">
				<img src="/assets/img/meet-the-team-1.jpg" alt="Meet the team">
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 team-member">
				<img src="/assets/img/meet-the-team-1.jpg" alt="Meet the team">
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 team-member">
				<img src="/assets/img/meet-the-team-1.jpg" alt="Meet the team">
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 team-member">
				<img src="/assets/img/meet-the-team-1.jpg" alt="Meet the team">
			</div>
		</div>
	</div>
--}}
	<div class="container-fluid about-banners blue">
		<div class="container">
			<div class="row careers">

				<div class="col-md-12 text-center">
					<h2 >Careers</h2>
				</div>

				@foreach ($careers as $career)

					<div class="col-md-3 col-sm-6 col-xs-12 careers-item">

						<h3 class="careers-item-title">{{ $career->title }}</h3>

						<div>
							@if(isset($career->contract))
								<strong>Contract:</strong> {!! $career->contract !!}
							@endif
							@if(isset($career->salary))
								&#124; <strong>Salary:</strong> {!! $career->salary !!}
							@endif
						</div>

						<div>
							{!! substr($career->description, 0, 100) !!}...
						</div>


						<a href="{!! route('careers_dynamic_route', $career->url_slug) !!}" class="btn btn-secondary">Find Out More</a>
					</div>
				@endforeach

			</div>
		</div>
	</div>

@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $('.circle-base').on('click', function(event) {
				event.preventDefault();

				var yearClicked = $(this).data("year");
				if (! $(this).hasClass('blue')) { // Makesure it's not Start being clicked
					$('.circle-base').removeClass('active');
					$(this).addClass('active');

					$('.timeline-item').css({ 'display': 'none' });

					$(".timeline-item[data-text='" + yearClicked +"']").css({
						'display': 'block'
					});
				}
			});
        });
    </script>
@stop
