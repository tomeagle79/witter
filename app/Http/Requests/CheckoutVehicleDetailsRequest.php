<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutVehicleDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        // Check that each of the vehicle details are valid.
        $vehicles = $this->request->get('vehicle');

        if (count($vehicles) > 0) {
            foreach ($vehicles as $key => $value) {

                $rules['vehicle.'.$key.'.registration_no'] = ['required', 'max:255'];
                $rules['vehicle.'.$key.'.vehicle_colour'] = ['required', 'max:255'];
            }
        }

        return $rules;
    }

    /**
     * Add some custom validation messages.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [];

        $request = $this->instance()->all();

        // Send a message back to the user
        $vehicles = $request['vehicle'];
        if (count($vehicles) > 0) {
            foreach ($vehicles as $key => $value) {
                $messages['vehicle.'.$key.'.registration_no.required'] = 'Please enter the registration number of the vehicle the product is to be fixed to.';
                $messages['vehicle.'.$key.'.vehicle_colour.required'] = 'Please enter the colour of the vehicle the product is to be fixed to.';
            }
        }

        return $messages;
    }

}
