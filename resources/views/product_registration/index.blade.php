@extends('layouts.frontend')

@section('title', 'Product Registration')

@section('heading')
	Product Registration
@stop

@section('meta_description')
	Register your product with Witter Towbars
@stop

@section('content')
	
<div class="container product-registration">
	<div class="row">
		<div class="hidden-xs hidden-sm col-md-3">
			
			@include('partials.sidebar-search')

			@include('partials.sidebar-findlink-secondary')

		</div> <!-- .col-md-3 -->

		<div class="col-sm-12 col-md-9">

            <h2 class="border-bottom no-top-margin">Product Registration</h2>

            <div class="row"> 
                <div class="col-sm-6">

                    <a href="{{ route('product_registration.towbar') }}" class="towbar-reg product-to-reg">
                        <h3>Towbar Registration</h3>

                        <div class="content">
                            <h4>Register your Towbar</h4>

                            <p>
                                Registering your Witter towbar with us will help us to provide you with a better service and to be able to easily ascertain whether you are entitled to the benefits of our lifetime guarantee.
                            </p>
                            <p>
                                <button class="btn btn-white">Register your<br>Towbar</button>
                            </p>
                        </div>
                    </a>
                    
                </div> <!-- .col -->
                <div class="col-sm-6">

                    <a href="{{ route('product_registration.cycle_carrier') }}" class="cycle-reg product-to-reg">
                        <h3>Cycle Carrier Registration</h3>

                        <div class="content">
                            <h4>Register your Cycle Carrier</h4>

                            <p>
                                Registering your Witter Cycle carrier with us will help us to provide you with a better service and to be able to easily ascertain whether you are entitled to the benefits of our lifetime guarantee.
                            </p>
                            <p>
                                <button class="btn btn-white">Register your<br>Cycle Carrier</button>
                            </p>
                        </div>
                    </a>
                    
                </div> <!-- .col -->
            </div> <!-- .row -->

		</div> <!-- .col-md-9 -->
	</div> <!-- .row -->
</div> <!-- .container -->

@stop
