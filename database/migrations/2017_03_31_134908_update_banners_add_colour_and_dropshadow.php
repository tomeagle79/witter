<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBannersAddColourAndDropshadow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('banners', function (Blueprint $table) {
            $table->string('main_color')->default('#da3934')->after('link');
            $table->string('sub_color')->default('#014789')->after('main_color');
            $table->boolean('drop_shadow')->default(0)->after('sub_color');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn(['main_color', 'sub_color', 'drop_shadow']);
        });
    }
}
