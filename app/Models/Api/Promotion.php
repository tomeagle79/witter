<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class Promotion extends ApiModel
{
	const URI = "promotions";

	/**
	 * Send promotion
	 */
	static function apply_promotion($promotion) {
        $url = config('witter.api_base') . self::URI;
        $obj = self::post_request($url, $promotion);
        
		return $obj;
	}
}
