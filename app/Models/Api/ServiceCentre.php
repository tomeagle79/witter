<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class ServiceCentre extends ApiModel
{
    const URI = "servicecentres";

    /**
     * Get with postcode parameter
     */
    static function get_where(array $args) {

        $url = config('witter.api_base') . self::URI .'?' . http_build_query($args);
        $obj = self::get_request($url);

        return $obj;
    }

    /**
     * Get by slug name
     */
    static function get_by_slug(string $slug) {

        $url = config('witter.api_base') . self::URI .'/slug?' . http_build_query(['centreSlug' => $slug]);
        $obj = self::get_request($url);

        return $obj;
    }

    /**
     * Get by region slug name
     */
    static function get_by_region_slug(string $slug) {

        $url = config('witter.api_base') . self::URI .'/regions/slug?' . http_build_query(['regionSlug' => $slug]);
        $obj = self::get_request($url);

        return $obj;
    }

    /**
     * Get a collection of the approved centres
     */
    static function get_approved() {

        $url = config('witter.api_base') . self::URI .'/approved';
        $obj = self::get_request($url);

        return $obj;
    }
}
