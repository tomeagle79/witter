<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddToCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        $product_type = $this->request->get("type");

        // IF we are buying a 'accessory-fitted' then an appointment is required.
        if($product_type == 'accessory-fitted'){

            $rules['appointmentDate'] = 'required';
        }

        return $rules;
    }

    /**
     * Add some custom validation messages.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [];

        // Send a relevant message about there needing to be an appointment selected.
        if($this->request->get("type") == 'accessory-fitted'){

            $messages['appointmentDate.required'] = 'Please enter a postcode and select an appointment date and time.';
        }

        return $messages;
    }

}
