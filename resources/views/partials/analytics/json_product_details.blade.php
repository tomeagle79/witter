<script>
    if (!window.hasOwnProperty('product_json')) {
        window.product_json = {};
    }

    @if($category == 'Towbars')
    window.product_json['{{ $product->towbarPartNo }}'] = {
        'name': '{{ str_replace("'", '', $product->title) }}',
        'id': '{{ $product->towbarPartNo }}',
        'price': '{{ $product->price }}',
        @if(!empty($product->brandName))
        'brand': '{{ $product->brandName }}',
        @endif
        @if(!empty($category))
        'category': '{{ $category }}',
        @endif
        'list_position': '{{ $position }}',
        'list_name': '{{ $list_name }}'
    };
    @endif

    @if($category == 'Trackers')
    window.product_json['{{ $product->partNo }}'] = {
        'name': '{{ str_replace("'", '', $product->title) }}',
        'id': '{{ $product->partNo }}',
        'price': '{{ $product->displayPrice }}',
        @if(!empty($product->brandName))
        'brand': '{{ $product->brandName }}',
        @endif
        @if(!empty($category))
        'category': '{{ $category }}',
        @endif
        'list_position': '{{ $position }}',
        'list_name': '{{ $list_name }}'
    };
    @endif


    @php 
        $arr = [
            'Roof Systems/Accessories',
            'Roof Systems/By Manufacturer',
            'Roof Systems/Roof Bars',
            'Roof Systems/Roof Racks',
            'Electrical Kits',
            'Bike Racks',
            'Accessories'
        ];
    @endphp

    @if(in_array($category, $arr))
    window.product_json['{{ $product->partNo }}'] = {
        'name': '{{ str_replace("'", '', $product->title) }}',
        'id': '{{ $product->partNo }}',
        'price': '{{ $product->price }}',
        @if(!empty($product->brandName))
        'brand': '{{ $product->brandName }}',
        @endif
        @if(!empty($category))
        'category': '{{ $category }}',
        @endif
        'list_position': '{{ $position }}',
        'list_name': '{{ $list_name }}'
    };
    @endif

    
</script>