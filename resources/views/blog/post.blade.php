@extends('layouts.frontend', ['heading_h2' => true])

@section('title')
	{!! !empty($post->meta_title) ? $post->meta_title : $post->title !!}
@stop

@section('heading')
	Witter Blog
@stop

@section('meta_description')
	{!! $post->meta_description !!}
@stop

@section('content')

	<div class="container">

		<nav class="row blog-catmenu">
			@foreach($categories as $cat)
				<div class="col-md-2">
					<a href="{!! route('blog_dynamic_route', $cat->url_slug) !!}" class="{{ (!empty($category) && $cat->id == $category->id)? 'active' : '' }}">
						{!! $cat->title !!}
					</a>
				</div>
			@endforeach

			<div class="col-md-2"><a href="{!! route('blog') !!}" class="{{ empty($category)? 'active' : '' }}">View All</a></div>
		</nav>

		<div class="row">

			<div class="col-sm-8 col-md-9">
				<article class="blog-article" id="blog-article">

					@if(!is_null($post->image))

						<img src="{{URL('imagecache', ['template' => 'original', 'filename' => $post->image->filename])}}" alt="" />

					@endif

					<h1 class="blog-single-header">{!! $post->title !!}</h1>

					<div class="blog-categories">
						@foreach($post->categories as $blog_categories)
							{{ $blog_categories->title }}<br />
						@endforeach
					</div>

					<div class="blog-copy">
						{!! $post->content !!}
					</div>

				</article>

				<ul class="share-buttons">
					<li><a href="https://www.facebook.com/sharer/sharer.php?u=&t=" target="_blank" title="Share on Facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;"><i class="fa fa-facebook-square" aria-hidden="true"></i><span class="sr-only">Share on Facebook</span></a></li>
					<li><a href="https://twitter.com/intent/tweet?source=&text=:%20&via=witter_towbars" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ':%20' + encodeURIComponent(document.URL)); return false;"><i class="fa fa-twitter-square" aria-hidden="true"></i><span class="sr-only">Tweet</span></a></li>
					<li><a href="https://plus.google.com/share?url=" target="_blank" title="Share on Google+" onclick="window.open('https://plus.google.com/share?url=' + encodeURIComponent(document.URL)); return false;"><i class="fa fa-google-plus-square" aria-hidden="true"></i><span class="sr-only">Share on Google+</span></a></li>
					<li><a href="http://www.tumblr.com/share?v=3&u=&t=&s=" target="_blank" title="Post to Tumblr" onclick="window.open('http://www.tumblr.com/share?v=3&u=' + encodeURIComponent(document.URL) + '&t=' +  encodeURIComponent(document.title)); return false;"><i class="fa fa-tumblr-square" aria-hidden="true"></i><span class="sr-only">Post to Tumblr</span></a></li>
					<li><a href="http://pinterest.com/pin/create/button/?url=&description=" target="_blank" title="Pin it" onclick="window.open('http://pinterest.com/pin/create/button/?url=' + encodeURIComponent(document.URL) + '&description=' +  encodeURIComponent(document.title)); return false;"><i class="fa fa-pinterest-square" aria-hidden="true"></i><span class="sr-only">Pin it</span></a></li>
					<li><a href="http://www.reddit.com/submit?url=&title=" target="_blank" title="Submit to Reddit" onclick="window.open('http://www.reddit.com/submit?url=' + encodeURIComponent(document.URL) + '&title=' +  encodeURIComponent(document.title)); return false;"><i class="fa fa-reddit-square" aria-hidden="true"></i><span class="sr-only">Submit to Reddit</span></a></li>
					<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=" target="_blank" title="Share on LinkedIn" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(document.URL) + '&title=' +  encodeURIComponent(document.title)); return false;"><i class="fa fa-linkedin-square" aria-hidden="true"></i><span class="sr-only">Share on LinkedIn</span></a></li>
					<li><a href="http://wordpress.com/press-this.php?u=&t=&s=" target="_blank" title="Publish on WordPress" onclick="window.open('http://wordpress.com/press-this.php?u=' + encodeURIComponent(document.URL) + '&t=' +  encodeURIComponent(document.title)); return false;"><i class="fa fa-wordpress" aria-hidden="true"></i><span class="sr-only">Publish on WordPress</span></a></li>
					<li><a href="mailto:?subject=&body=:%20" target="_blank" title="Send email" onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' +  encodeURIComponent(document.URL)); return false;"><i class="fa fa-envelope-square" aria-hidden="true"></i><span class="sr-only">Send email</span></a></li>
				</ul>

			</div> <!-- col-md-9 -->

			<div class="col-sm-4 col-md-3">

				@include('partials.sidebar-search')

				@include('partials.sidebar-blog-article')

			</div> <!-- .col-md-3 -->

		</div> <!-- .row -->
	</div> <!-- .container -->
@stop


@section('scripts')
<script>
    $(document).ready(function () {
        var $blogArticle = $('#blog-article');
       
        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
            scrollTop: $blogArticle.offset().top
        }, 800);
    })
</script>

@stop

