$(document).ready(function(){
	$(document).on('click', '[data-confirm]', function(e){
		if(confirm($(this).attr('data-confirm'))){
			return true;
		} else {
			e.preventDefault();
			return false;
		}
	});

	function convertToSlug(Text)
	{
	    return Text
	    	.toString()
	        .toLowerCase()
	        .replace(/ /g,'-')
	        .replace(/[^\w-]+/g,'');
	}

	$(".title-for-slug").change(function() {
		$(".slug-generated").val(convertToSlug($(".title-for-slug" ).val()));
	});
});
