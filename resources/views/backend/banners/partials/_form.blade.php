<div class="form-group required">
	<label for="title" class="col-md-2 control-label">Title</label>
	<div class="col-md-4">
		{!! Form::text('title', ( ($type == 'edit') ? $banner->title : null), ['class' => 'form-control', 'id' => 'title', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group">
	<label for="sub_title" class="col-md-2 control-label">Sub-title</label>
	<div class="col-md-4">
		{!! Form::text('sub_title', ( ($type == 'edit') ? $banner->sub_title : null), ['class' => 'form-control', 'id' => 'sub_title']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="link" class="col-md-2 control-label">Link</label>
	<div class="col-md-4">
		{!! Form::text('link', ( ($type == 'edit') ? $banner->link : null), ['class' => 'form-control', 'id' => 'link', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="link" class="col-md-2 control-label">Button text</label>
	<div class="col-md-4">
		{!! Form::text('button_text', ( ($type == 'edit') ? $banner->button_text : null), ['class' => 'form-control', 'id' => 'button_text']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="link" class="col-md-2 control-label">Main Title Colour</label>
	<div class="col-md-4">
		{!! Form::select('main_color', $colors, ( ($type == 'edit') ? $banner->main_color : '#da3934'), ['class' => 'form-control', 'id' => 'link', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="link" class="col-md-2 control-label">Sub Title Colour</label>
	<div class="col-md-4">
		{!! Form::select('sub_color', $colors, ( ($type == 'edit') ? $banner->sub_color : '#014789'), ['class' => 'form-control', 'id' => 'link', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="link" class="col-md-2 control-label">Drop Shadow?</label>
	<div class="col-md-4">
		{!! Form::checkbox('drop_shadow', true, isset($banner) ? $banner->drop_shadow : false, ['class' => '']) !!}
	</div>
</div>

<div class="form-group">
	<label for="link" class="col-md-2 control-label">Discount Code?</label>
	<div class="col-md-4">
		{!! Form::checkbox('discount_code_check', true, isset($banner) ? $banner->discount_code_check : false, ['class' => '']) !!}
	</div>
</div>

<div class="form-group">
	<label for="discount_code" class="col-md-2 control-label">Discount Code</label>
	<div class="col-md-4">
		{!! Form::text('discount_code', ( ($type == 'edit') ? $banner->discount_code : null), ['class' => 'form-control', 'id' => 'discount_code']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="sort_order" class="col-md-2 control-label">Sort Order</label>
	<div class="col-md-1">
		{!! Form::number('sort_order', ( ($type == 'edit') ? $banner->sort_order : 0), ['class' => 'form-control', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group">
	<label for="active" class="col-sm-3 col-md-2 control-label">Active</label>
	<div class="col-sm-9 col-md-6">
		{!! Form::checkbox('active', true, isset($banner) ? $banner->active : true, ['class' => '']) !!}
		<p class="help-block">Display on frontend?</p>
	</div>
</div>

<div class="form-group">
	<label for="active" class="col-sm-3 col-md-2 control-label">Vehicle of the week</label>
	<div class="col-sm-9 col-md-6">
		{!! Form::checkbox('vehicle_of_week', true, isset($banner) ? $banner->vehicle_of_week : true, ['class' => '']) !!}
	</div>
</div>

<div class="form-group">
	<label for="active" class="col-sm-3 col-md-2 control-label">Manufacturer of the week</label>
	<div class="col-sm-9 col-md-6">
		{!! Form::checkbox('manufacturer_of_week', true, isset($banner) ? $banner->manufacturer_of_week : true, ['class' => '']) !!}
	</div>
</div>

@if($type=='edit' && !is_null($banner->image))
	<div class="form-group">
		<label for="logo" class="control-label col-xs-12 col-sm-3 col-md-2 col-lg-2">Current Image</label>
		<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">

			<a href="{{URL('imagecache', ['template' => 'original', 'filename' => $banner->image->filename])}}" class="fancybox image">
				<img src="{{URL('imagecache', ['template' => 'small', 'filename' => $banner->image->filename])}}" alt="" />
			</a>
		</div>
	</div>
@endif

<div class="form-group">
	<label for="logo" class="control-label col-xs-12 col-sm-3 col-md-2 col-lg-2">Image</label>
	<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
		<div class="fileinput fileinput-new" data-provides="fileinput">
			<div class="input-group input-large">
				<div class="form-control uneditable-input" data-trigger="fileinput">
					<i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename">
					</span>
				</div>
				<span class="input-group-addon btn default btn-file">
				<span class="fileinput-new">
				Select file </span>
				<span class="fileinput-exists">
				Change </span>
				<input type="file" name="file">
				</span>
				<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">
				Remove </a>
			</div>
		</div>
	</div>
</div>

<div class="form-group required">
	<label for="active_start_at" class="col-md-2 control-label">Start Banner Date</label>
	<div class="col-md-4">
		{!! Form::text('active_start_at', ( ($type == 'edit') ? $banner->active_start_at : null), ['class' => 'form-control datepicker', 'id' => 'active_start_at']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="active_end_at" class="col-md-2 control-label">End Banner Date</label>
	<div class="col-md-4">
		{!! Form::text('active_end_at', ( ($type == 'edit') ? $banner->active_end_at : null), ['class' => 'form-control datepicker', 'id' => 'active_end_at']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-md-offset-2 col-md-4">
		<button type="submit" class="btn btn green">{{ $submit_text }}</button>
	</div>
</div>

@section('scripts')
	@parent
	<script type="text/javascript" src="{!! URL::asset('assets/js/redactor.js') !!}"></script>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD HH:mm'
            });

			$('textarea.wysiwyg').redactor({
				minHeight: 300,
				plugins: ['table', 'video', 'imagemanager'],
				buttonSource: true,
				imageUpload: '{!! URL::to('wtadmin/redactor/image_upload') !!}?_token=' + '{{ csrf_token() }}',
				imageManagerJson: '{!! URL::to('wtadmin/redactor/get_image_json') !!}',
			});
		});

	</script>
@overwrite
