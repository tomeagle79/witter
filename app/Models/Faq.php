<?php

namespace App\Models;

use App\Traits\Published;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{

    use Published;

    /**
     * Relationship to categories
     */
    public function categories()
    {
        return $this->belongsTo('App\Models\FaqCategory', 'category_id');
    }
}
