var $ = jQuery.noConflict();

/*
    * Call the avalibale appointments for the fitter.
    *
*/
function callAppointments(appointments_url, $element){

    $.ajax({
        type: 'POST',
        url: appointments_url,
        data: {
            'address': $element.attr('data-address'),
            'postcode': $element.attr('data-postcode'),
            'partNo': $element.attr('data-partNo'),
            'variantTowbarId': $element.attr('data-variantTowbarId'),
            'vehicleDesc': $element.attr('data-vehicleDesc'),
            'partnerId': $element.attr('data-fitter'),
            'fitterType': $element.attr('data-fitterType'),
            'electric': $element.attr('data-electric')
        },
        success: function(result){
            if(result.success == false) {
                $('#error p').html(result.errorMessage);
                $('#error').fadeIn('slow');
            } else {
                $('#modal .modal-content .modal-body').html(result.modal);
            }
        },
        error: function(){
            alert('Sorry, something went wrong');
        }
    });
}

/*
    * Submit the user's postcode to find fitters
    *
*/
function findFitters(fitters_url, postcode){

    var partNo = $('input[name=partNo]').val();
    var variantTowbarId = $('input[name=variantTowbarId]').val();
    var towball_option = $('input[name=towball_option]').val();
    var electric_option = $('input[name=electric_option]').val();
    var requires_coding = $('input[name="software_upgrade"]').val();

    $('#no-postcode, #no-kit, #loading, #error').hide();

    $("#modal .loading-modal-body").show();

    if(!postcode.length) {
        $('#no-postcode').fadeIn('slow');
    } else {
        $('#loading').fadeIn('slow');
        $.ajax({
            type: 'POST',
            url: fitters_url,
            data: {
                'postcode': postcode,
                'partNo': partNo,
                'variantTowbarId': variantTowbarId,
                'towball_option': towball_option,
                'electric_option': electric_option,
                'requires_coding': requires_coding,
            },
            success: function(result){
                $('#loading').fadeOut('slow');

                if(result.success == false) {
                    $('#error p').html(result.errorMessage);
                    $('#error').fadeIn('slow');
                } else {
                    /*
                    if(fitting_type == 'mobile') {
                        $('.standard-row').hide();
                    } else if(fitting_type == 'workshop') {
                        $('.mobile-row').hide();
                    }
                    */
                    $('#reload').fadeIn('slow');

                    $('#modal .modal-title').html( 'Book your fitting' );
                    $('#modal .modal-content .modal-body').html(result.modal);
                    $('#modal').modal('show');

                    // Save the postcode to entry
                    $('#postcode').val(postcode.toUpperCase());
                }

                $("#modal .loading-modal-body").hide();
            },
            error: function(){

                $("#modal .loading-modal-body").hide();
            }
        });
    }
}

/*
    * Submit the user's postcode to find accessory fitters
    *
*/
function findAccessoryFitters(accessory_fitters_url, postcode){

    console.log("findAccessoryFitters ");

    var partNo = $('input[name=partNo]').val();

    $('#no-postcode, #no-kit, #loading, #error').hide();

    $("#modal .loading-modal-body").show();

    if(!postcode.length) {
        $('#no-postcode').fadeIn('slow');
    } else {
        $('#loading').fadeIn('slow');

        $.ajax({
            type: 'POST',
            url: accessory_fitters_url,
            data: {
                'postcode': postcode,
                'partNo': partNo,

            },
            success: function(result){
                $('#loading').fadeOut('slow');

                console.log(result);

                if(result.success == false) {
                    $('#error p').html(result.errorMessage);
                    $('#error').fadeIn('slow');
                } else {


                    /*
                    if(fitting_type == 'mobile') {
                        $('.standard-row').hide();
                    } else if(fitting_type == 'workshop') {
                        $('.mobile-row').hide();
                    }
                    */
                    $('#reload').fadeIn('slow');

                    $('#modal .modal-title').html( 'Book your fitting' );
                    $('#modal .modal-content .modal-body').html(result.modal);
                    $('#modal').modal('show');

                    // Save the postcode to entry
                    $('#postcode').val(postcode.toUpperCase());
                }

                $("#modal .loading-modal-body").hide();
            },
            error: function(){

                console.log("Do we have an error.");

                $("#modal .loading-modal-body").hide();
            }
        });
    }
}
