<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CareerStoreRequest;
use App\Models\Career;
use App\Models\Image as ImageModel;
use App\Events\Admin\ImageStored;
use Auth;
use Datatables;
use Hash;
use View;
use DB;
use URL;
use Input;

class CareerController extends Controller
{
    /**
     * Instantiate a new instance.
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.careers.index');
    }

    /**
     * Return the datatable for careers
     *
     * @return Datatables
     */
    public function indexDatatable()
    {
        $careers = Career::select(['id', 'title', DB::raw('IF(published=1, "Yes", "No") as published'), 'created_at', 'updated_at'])->get();

        return Datatables::of($careers)
            ->addColumn('edit', '<a href="{{ URL::backend(\'careers/edit/\'. $id) }}" class="btn btn-success">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'careers/delete/\'. $id) }}" class="btn btn-danger" data-confirm="Are you sure you want to delete this user?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.careers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CareerStoreRequest $request)
    {
        // create the career
        $career = new Career();
        $career->title = $request->title;
        $career->url_slug = str_slug($request->url_slug,'-');
        $career->description = $request->description;
        $career->contract = $request->contract;
        $career->salary = $request->salary;
        $career->email = $request->email;
        $career->published = (empty($request->published) ? 0 : 1);

        // wrap in transaction, for graceful fail
        DB::transaction(function() use ($career, $request) {

            // save the career
            $career->save();
        });

        return redirect(URL::backend('careers'))->with('success', '<strong>Success!</strong> Career created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $career = Career::findOrFail($id);
        return View::make('backend.careers.edit', compact('career'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CareerStoreRequest $request, $id)
    {
        // save the career
        $career = Career::findOrFail($id);
        $career->title = $request->title;
        $career->url_slug = str_slug($request->url_slug,'-');
        $career->description = $request->description;
        $career->contract = $request->contract;
        $career->salary = $request->salary;
        $career->email = $request->email;
        $career->published = (empty($request->published) ? 0 : 1);

        // Save the career
        $career->save();

        return redirect(URL::backend('careers'))->with('success', '<strong>Success!</strong> Career updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $career = Career::findOrFail($id);
        $career->delete();

        return redirect(URL::backend('careers'))->with('message', '<strong>Success!</strong> Career deleted.');
    }

    /**
     * Restore the specified resource
     */
    public function restore($id)
    {
        $career = Career::withTrashed()->findOrFail($id);
        $career->restore();

        return redirect(URL::backend('careers'))->with('success', '<strong>Success!</strong> Career restored.');
    }
}
