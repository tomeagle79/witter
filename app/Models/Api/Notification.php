<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class Notification extends ApiModel
{
	const URI = "notifications";

	/**
	 * Add a new notification.  Towbars only
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function new_notification($notification)
	{
		$url = config('witter.api_base') . self::URI . '?' . http_build_query($notification);
		$obj = self::get_request($url);
		
		return $obj;
	}
}
