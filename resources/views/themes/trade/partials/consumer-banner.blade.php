<div class="trade-header">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <p>Welcome to the Witter trade site</p>
            </div>
            <div class="col-sm-6 col-xs-12 text-right">
                <p>Looking for the consumer site? <a href="{{ $redirect_url . request()->path() }}">Take me there</a></p>
            </div>
        </div>
    </div>
</div>