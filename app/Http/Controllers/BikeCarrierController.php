<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Api\PartClass;
use App\Models\Api\Accessory;
use Mail;

class BikeCarrierController extends FrontendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $this->data['filtersArray'] = $filtersArray = $request->input();

        // Set our slugs to call the API.
        $category_slug = 'cycle-carriers';

        if(count($filtersArray) == 0) { // On main page, get all carriers
            $this->data['category'] = PartClass::get_category_by_slug($category_slug);
            $this->data['check'] = PartClass::get_subcategories_by_slug($category_slug);

            $check = Accessory::get_accessories_by_class_slug($category_slug);

            $this->data['carriers'] = $check;
        } else if (count($filtersArray) > 0 || count($filtersArray) <= 3 ) { // Full Valid Search
            $amountOfBikes = empty($filtersArray['amount']) ? 'any' : $filtersArray['amount'];
            $connectionType =  empty($filtersArray['type']) ? 'any' : $filtersArray['type'];
            $carriers = [];

            if(count($filtersArray) == 3) {
                $ebikeOrNot = $filtersArray['ebike'];
            }

            if($connectionType == 'towbar') {
                $connectionType = 'Towbar Mounted';
            } else if ($connectionType == 'roof') {
                $connectionType = 'Roof Mounted';
            } else if ($connectionType == 'rear') {
                $connectionType = 'High Rear Mounted';
            }

            $this->data['category'] = PartClass::get_category_by_slug($category_slug);

            $check = Accessory::get_accessories_by_class_slug($category_slug);

            foreach($check as $checked) {
                $tempAmountKey;
                $tempTypeKey;
                $ebikeKey;

                foreach($checked->attributes as $key => $attribute) {
                    if($attribute->attributeName == 'Number of bikes') {
                        $tempAmountKey = $key;
                    } else if ($attribute->attributeName == 'Mounting Type') {
                        $tempTypeKey = $key;
                    } else if ($attribute->attributeName == 'Suitable for eBikes') {
                        $ebikeKey = $key;
                    }
                }

                /**
                 * Check if $checked->attributes is not empty.
                 */
                if(empty($checked->attributes)) {
                    continue;
                }

                if($amountOfBikes == 'any' && $connectionType == 'any') {
                    if(count($filtersArray) == 3) {
                        if($checked->attributes[$ebikeKey]->attributeValue == $ebikeOrNot) {
                            array_push($carriers, $checked);
                        }
                    } else {
                        array_push($carriers, $checked);
                    }
                } else if ($amountOfBikes != 'any' && $connectionType == 'any') {
                    if($checked->attributes[$tempAmountKey]->attributeValue == $amountOfBikes) {
                        if(count($filtersArray) == 3) {
                            if($checked->attributes[$ebikeKey]->attributeValue == $ebikeOrNot) {
                                array_push($carriers, $checked);
                            }
                        } else {
                            array_push($carriers, $checked);
                        }
                    }
                } else if ($amountOfBikes == 'any' && $connectionType != 'any') {
                    if(!empty($tempTypeKey) && $checked->attributes[$tempTypeKey]->attributeValue == $connectionType) {
                        if(count($filtersArray) == 3) {
                            if($checked->attributes[$ebikeKey]->attributeValue == $ebikeOrNot) {
                                array_push($carriers, $checked);
                            }
                        } else {
                            array_push($carriers, $checked);
                        }
                    }
                } else {
                    if(!empty($tempTypeKey) && $checked->attributes[$tempAmountKey]->attributeValue == $amountOfBikes && $checked->attributes[$tempTypeKey]->attributeValue == $connectionType) {
                        if(count($filtersArray) == 3) {
                            if($checked->attributes[$ebikeKey]->attributeValue == $ebikeOrNot) {
                                array_push($carriers, $checked);
                            }
                        } else {
                            array_push($carriers, $checked);
                        }
                    }
                }
            }

            $this->data['carriers'] = $carriers;
        } else { // Redirect to main page
            header("Location: https://" . $_SERVER['HTTP_HOST'] . "/bike-carriers");

            exit();
        }

        $filters = [];

        $this->data['breadcrumbs']->addCrumb('Bike Racks');

        return view('bike-carriers.index', $this->data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function email_notify(Request $request)
    {

        \Mail::send('emails.product_notify',
            array(
                'email' => $request->get('email'),
                'product_id' => $request->get('product_id'),
                'product_name' => $request->get('product_name'),
                'product_url' => $request->get('product_url'),
                'checkbox_for_subscription' => $request->get('checkbox_for_subscription'),
            ), function($message)
        {
            $message->from(config('witter.noreply_email'));
            $message->to(config('witter.admin_email'), 'Admin')->subject('Product notify request');
            //$message->to('kieran.rigby@reckless.agency', 'Admin')->subject('Product notify request');
        });

        return redirect($request->get('product_url'))->with('success', 'Thanks for your details, we will keep you updated when new stock arrives!');
    }

    /**
     * Display the accessory.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($accessory_slug)
    {
        $this->data['accessory'] = $accessory = Accessory::get_single_by_slug($accessory_slug);

        if($accessory->partNo == null) {
            return redirect(route('bike-racks'));
        }

        $this->data['category'] = PartClass::get_partclass_by_id($this->data['accessory']->partClassId);
        $this->data['subcategories'] = PartClass::get_categories_by_partclass_id($this->data['accessory']->partClassId);
        $this->data['subcategory'] = PartClass::get_category_by_category_id($this->data['accessory']->categoryId);

        $this->data['breadcrumbs']->addCrumb('Bike Racks', route('bike-racks'));
        $this->data['breadcrumbs']->addCrumb($this->data['accessory']->title, route('accessories.multi', [encode_url($this->data['accessory']->partNo)]));

        return view('accessories.view_updated', $this->data);
    }
}