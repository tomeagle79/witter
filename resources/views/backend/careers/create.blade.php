@extends('layouts.backend')

@section('title')
	Create Career
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/careers/partials/_form', ['submit_text' => 'Create Career', 'type' => 'add'])
	</form>

@stop

