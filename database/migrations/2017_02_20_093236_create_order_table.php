<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('witter_order_id')->nullable();
            $table->string('payment_reference')->nullable();
            $table->string('customer_first_name')->nullable();
            $table->string('customer_surname')->nullable();
            $table->string('customer_telephone')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('customer_address1')->nullable();
            $table->string('customer_address2')->nullable();
            $table->string('customer_towncity')->nullable();
            $table->string('customer_county')->nullable();
            $table->string('customer_postcode')->nullable();
            $table->string('customer_country')->nullable();
            $table->string('delivery_address1')->nullable();
            $table->string('delivery_address2')->nullable();
            $table->string('delivery_towncity')->nullable();
            $table->string('delivery_county')->nullable();
            $table->string('delivery_postcode')->nullable();
            $table->string('delivery_country')->nullable();
            $table->string('sub_total')->comments('Total of all products before deductions')->nullable();
            $table->string('promotion_code')->comments('Can only have one discount applied')->nullable();
            $table->string('discount_amount')->nullable();
            $table->string('grand_total')->comments('Sub total minus any discounts')->nullable();
            $table->boolean('is_phone_order')->default(false);
            $table->timestamps();
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->enum('item_type', ['accessory', 'towbar'])->nullable();
            $table->string('accessory_part_no')->nullable();
            $table->string('accessory_qty')->nullable();
            $table->string('towbar_vtid')->comments('Variable towbar ID.  Combination of the car and towbar')->nullable();
            $table->string('towbar_appointment_id')->nullable();
            $table->string('towbar_appointment_date')->nullable();
            $table->string('towbar_appointment_time')->nullable();
            $table->string('towbar_partner_id')->nullable();
            $table->string('towbar_partner_address')->nullable();
            $table->string('towbar_registration_date')->nullable();
            $table->string('towbar_towball_part_no')->comments('Towbar towball choice.  Optional')->nullable();
            $table->string('towbar_towball_price')->comments('Additional amount for the towball')->nullable();
            $table->string('towbar_electric_option_part_no')->comments('Towbar electical kit choice.  Optional')->nullable();
            $table->string('towbar_electric_option_price')->comments('Additional amount for the kit')->nullable();
            $table->decimal('price', 8, 2)->comments('Total including options added')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_items');
    }
}
