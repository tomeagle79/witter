@extends('layouts.frontend')

@section('title', 'Light Commercial Vehicles - Roof Bars - Models')

@section('heading')
	Light Commercial Vehicles - Roof Bars - Models
@stop

@section('meta_description')
	Witter Light Commercial Vehicles - Roof Bars - Models
@stop

@section('content')


<div class="container towbars-listing">

	<div class="row">
		<div class="col-md-12">

			@if( !empty($model->photoUrl) )
                <div class="top-img-wrapper">
					<img src="{{ $model->photoUrl }}" alt="" class="top-img">
                    <p class="text-center">Images are representative only</p>
                </div> <!-- .wrapper -->
			@endif

			<h2>{{ $manufacturer->manufacturerName }}</h2>
			<h3>{{ $model->modelName }}</h3>

			<div class="item-list">
                <?php
                // Count
                $count = count($vehicles);
                ?>
				@foreach ($vehicles as $key => $vehicle)

					<?php $key++ ?>

					@if($key%4 === 1)
						<div class="row">
					@endif

					<div class="col-md-3">
                        <div class="item">
                            <div class="manufacturer-logo">
                                <a href="{{ route('roof-systems.roof-bars.models.vehicles.products', [$manufacturer_slug, $model_slug, $vehicle->slug]) }}">
                                    <img src="{{ image_load($vehicle->photoUrl) }}" alt="Roof Bars for {{ $vehicle->vehicleName }}" >
                                </a>
                                 <a href="{{ route('roof-systems.roof-bars.models.vehicles.products', [$manufacturer_slug, $model_slug, $vehicle->slug]) }}">
                                    <h3>{{ $vehicle->vehicleName }}</h3>
                                    <h4>
                                        @if(!empty($vehicle->fromMonth))
                                            {{ date('M', mktime(0, 0, 0, $vehicle->fromMonth, 1, $vehicle->fromYear)) }}
                                        @endif
                                        {{ $vehicle->fromYear }}
                                        -
										@if(empty($vehicle->toYear))
											Onwards
										@else
											@if(!empty($vehicle->toMonth))
												{{ date('M', mktime(0, 0, 0, $vehicle->toMonth, 1, $vehicle->toMonth)) }}
											@endif
											{{ $vehicle->toYear }}
										@endif
                                    </h4>
                                </a>
                            </div>
                        </div> <!-- .item -->
					</div>

					@if($key%4 === 0 && $key > 0 || $key == $count)
						</div><!-- .row -->
					@endif
				@endforeach

			</div> <!-- .item-list -->

		</div>
	</div>
</div>
@stop
