
{!! Form::open(['route' => [$redirect_slug . '.index_submit'], 'id' => 'search-dropdown-form', 'onsubmit' => "
    gtag('event', 'Success', {
        event_category: 'Model select',
        event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
    });
"]) !!}
    <div class="form-group">
        <label for="manufacturer" class="sr-only">Choose Make</label>
        <div class="car-dropdowns-select">
            <select name="manufacturer" id="manufacturer" class="" data-style="btn-select-box">
                <option>Make</option>
                @if(!empty($manufacturers_dropdown))
                    @foreach($manufacturers_dropdown as $mdrop)
                        <option value="{{ $mdrop->manufacturerId }}" data-slug="{{ $mdrop->slug }}"
                            @if(isset($vehicle_dropdown['manufacturer_id']) && $vehicle_dropdown['manufacturer_id'] == $mdrop->manufacturerId)
                                selected="selected"
                                @php $manufacturer_slug = $mdrop->slug @endphp
                            @endif >{{ $mdrop->manufacturerName }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="model" class="sr-only">Choose Model</label>
        <div class="car-dropdowns-select">
            <select name="model" id="model" class="" data-style="btn-select-box">
                <option>Model</option>
                @if(!empty($models_dropdown))
                    @foreach($models_dropdown as $mdrop)
                        <option value="{{ $mdrop->modelId }}" data-slug="{{ $mdrop->slug }}"
                            @if(isset($vehicle_dropdown['model_id']) && $vehicle_dropdown['model_id'] == $mdrop->modelId)
                                selected="selected"
                                @php $model_slug = $mdrop->slug @endphp
                            @endif >{{ $mdrop->modelName }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="body" class="sr-only">Choose Body Type</label>
        <div class="car-dropdowns-select">
            <select name="body" id="body" class="" data-style="btn-select-box">
                <option>Body</option>
                @if(!empty($bodies_dropdown))
                    @foreach($bodies_dropdown as $mdrop)
                        <option value="{{ $mdrop->bodyId }}" data-slug="{{ $mdrop->slug }}"
                            @if(isset($vehicle_dropdown['body_id']) && $vehicle_dropdown['body_id'] == $mdrop->bodyId)
                                selected="selected"
                                @php $body_slug = $mdrop->slug @endphp
                            @endif >{{ $mdrop->bodyName }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="registration" class="sr-only">Choose Registration</label>
        <div class="car-dropdowns-select">
            <select name="registration" id="registration" class="" data-style="btn-select-box">
                <option>Registration</option>
                @if(!empty($registrations_dropdown))
                    @foreach($registrations_dropdown as $mdrop)
                        <option value="{{ $mdrop->registrationId }}" data-slug="{{ $mdrop->registrationId }}"
                            @if(isset($vehicle_dropdown['registration_id']) && $vehicle_dropdown['registration_id'] == $mdrop->registrationId)
                                selected="selected"
                                @php $registration_slug = $mdrop->registrationId @endphp
                            @endif >{{ $mdrop->registrationText }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>

    {{ Form::hidden('manufacturerSlug', empty($manufacturer_slug) ? '' : $manufacturer_slug, ['id' => 'manufacturerSlug']) }}
    {{ Form::hidden('modelSlug', empty($model_slug) ? '' : $model_slug, ['id' => 'modelSlug']) }}
    {{ Form::hidden('bodySlug', empty($body_slug) ? '' : $body_slug, ['id' => 'bodySlug']) }}
    {{ Form::hidden('registrationId', empty($registration_slug) ? '' : $registration_slug, ['id' => 'registrationId']) }}

    <button type="submit" class="btn btn-primary pull-right">{{ $form_button_text }}</button>

{!! Form::close() !!}
<div class="car-dropdowns-loading"></div>


@section('scripts')
    @parent
    <script type="text/javascript">

        // Set up the dropdowns
        $(function() {

            // Send the full url of where our vehicle ajax endpoints are
            setUpVehicleDropdowns( "{{ URL('vehicles') }}" );

            // Send the starting/first slug to build the url and redirect the user.
            setUpVehicleRedirect("/{{ $redirect_slug }}/");
        });

    </script>
@stop

