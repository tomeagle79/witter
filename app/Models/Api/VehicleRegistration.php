<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class VehicleRegistration extends ApiModel
{
	const URI = "registrations";
	
	/**
	 * Get all models based on body ID
	 *
	 * @param Int
	 * @return Object
	 */
	static public function get_all_from_body_id($body_id)
	{
		$url = config('witter.api_base') . 'bodies/'. $body_id .'/registrations';
		$obj = self::get_request($url);
		
		return $obj;
	}
}
