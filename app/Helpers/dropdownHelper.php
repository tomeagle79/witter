<?php

/**
 * Builds an array in the session of the users selected vehicle
 */
function build_dropdown($manufacturer_id = null, $model_id = null, $body_id = null, $registration_id = null, $session_name = 'vehicle_dropdown')
{

    // Get the vehicle_dropdown array
    $vehicle_dropdown = Session::get($session_name, []);

    // If the manufacturer_id has been passed in the add to the array
    if (isset($manufacturer_id)) {

        // If this manufacturer_id has not been set or does not equal the value passed into the build.
        if (empty($vehicle_dropdown['manufacturer_id']) || $vehicle_dropdown['manufacturer_id'] != $manufacturer_id) {

            $vehicle_dropdown = [];

            $vehicle_dropdown['manufacturer_id'] = $manufacturer_id;
        }
    }

    // If the model_id has been passed in the add to the array
    if (isset($model_id)) {
        // If the model id is empty set or is different
        if (empty($vehicle_dropdown['model_id']) || $vehicle_dropdown['model_id'] != $model_id) {

            $vehicle_dropdown['model_id'] = $model_id;

            // Unset child options if set.
            unset($vehicle_dropdown['body_id'], $vehicle_dropdown['registration_id']);
        }
    }

    // If the body_id has been passed in the add to the array
    if (isset($body_id)) {

        // If the body_id is empty set or is different
        if (empty($vehicle_dropdown['body_id']) || $vehicle_dropdown['body_id'] != $body_id) {

            $vehicle_dropdown['body_id'] = $body_id;

            // Unset child options if set.
            unset($vehicle_dropdown['registration_id']);
        }
    }

    // If the registration_id has been passed in the add to the array
    if (isset($registration_id)) {
        $vehicle_dropdown['registration_id'] = $registration_id;

        // If the registration_id is empty set or is different
        if (empty($vehicle_dropdown['registration_id']) || $vehicle_dropdown['registration_id'] != $registration_id) {

            $vehicle_dropdown['registration_id'] = $registration_id;
        }
    }

    Session::put($session_name, $vehicle_dropdown);

    return $vehicle_dropdown;
}

/**
 * Builds an array in the session of the users selected air suspension vehicles
 */
function build_suspension_dropdown($manufacturer_id = null, $model_id = null, $body_id = null, $registration_id = null, $session_name = 'suspension_vehicle_dropdown')
{
    // Simply return the 'build_dropdown' but ensure that the 'session_name' is set.
    return build_dropdown($manufacturer_id, $model_id, $body_id, $registration_id, $session_name);
}
