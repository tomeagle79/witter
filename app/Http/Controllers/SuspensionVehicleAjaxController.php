<?php

namespace App\Http\Controllers;

use App\Models\Api\Suspension\SuspensionVehicle;
use App\Models\Api\Suspension\SuspensionVehicleManufacturer;
use App\Models\Api\Suspension\SuspensionVehicleModel;
use App\Models\Api\VehicleManufacturer;
use App\Models\Api\VehicleModel;
use Illuminate\Http\Request;

class SuspensionVehicleAjaxController extends Controller
{
    /**
     * Display a json response of the manufacturers collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_manufacturers()
    {
        $manufacturers = SuspensionVehicleManufacturer::get_all();

        $data = [];

        foreach ($manufacturers as $manufacturer) {
            $data[] = [
                'index' => $manufacturer->manufacturerId,
                'slug' => $manufacturer->slug,
                'name' => $manufacturer->manufacturerName,
            ];
        }

        return response()->json($data);
    }

    /**
     * Display a json response of the models collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_models($id)
    {

        $models = SuspensionVehicleModel::get_all_from_manufacturer_id($id);

        $data = [];

        foreach ($models as $model) {
            $data[] = [
                'index' => $model->modelId,
                'slug' => $model->slug,
                'name' => $model->modelName,
            ];
        }

        $vehicle_dropdown = build_suspension_dropdown($id);

        return response()->json($data);
    }

    /**
     * Display a json response of the bodies collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_bodies($id)
    {
        $vehicles = SuspensionVehicle::get_all_from_model_id($id);

        $data = [];

        foreach ($vehicles as $vehicle) {
            $data[] = [
                'index' => $vehicle->vehicleId,
                'slug' => $vehicle->slug,
                'name' => 'From ' . $vehicle->fromYear . ' to ' . $vehicle->toYear . ' - ' . $vehicle->chassisDesignation,
            ];
        }

        $vehicle_dropdown = build_suspension_dropdown($vehicle->manufacturerId, $id);

        return response()->json($data);
    }

    /**
     * Redirect the user if they post to the index dropdowns
     *
     * @return redirect
     */
    public function index_submit(Request $request)
    {
        $redirect_to = $request->segment(1);

        // If user posts an id redirect
        if (!empty(preg_replace('/\D/', '', $request->manufacturer))) {

            $manufacturer = VehicleManufacturer::get_by_id($request->manufacturer);

            return redirect(route($redirect_to . '.model_list', $manufacturer->slug));
        } else {

            return redirect(route($redirect_to));
        }
    }

    /**
     * Redirect the user if they post to the index dropdowns
     *
     * @return redirect
     */
    public function model_list_submit(Request $request, $manufacturer_slug)
    {
        $redirect_to = $request->segment(1);

        // If user posts an id redirect
        if (!empty(preg_replace('/\D/', '', $request->model))) {

            $model = VehicleModel::get_by_id($request->model);

            return redirect(route($redirect_to . '.models.vehicle_list', [$manufacturer_slug, $model->slug]));

        } else {

            return redirect(route($redirect_to . '.model_list', $manufacturer_slug));
        }
    }

    /**
     * Redirect the user if they post to the index dropdowns
     *
     * @return redirect
     */
    public function body_registration_submit(Request $request, $manufacturer_slug, $model_slug)
    {
        $redirect_to = $request->segment(1);

        // If user posts an id redirect
        if (!empty(preg_replace('/\D/', '', $request->body))) {

            $vehicles = collect(SuspensionVehicle::get_all_by_slug(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]));

            $vehicle = $vehicles->firstWhere('vehicleId', $request->body);

            return redirect(route($redirect_to . '.models.vehicles.products', [$manufacturer_slug, $model_slug, $vehicle->slug]));
        } else {

            return redirect(route($redirect_to . '.models.vehicle_list', [$manufacturer_slug, $model_slug]));
        }
    }
}
