<?php

namespace App\Services;

use App\Models\Api\Appointment;
use Log;

class ProvisionalAppointmentFactory
{
    /**
     * Make the provisial booking basde on the info in the cart.
     *
     * @param  array  $cart
     * @param  string  $customer_email
     * @return obj $appointment
     */
    public static function create($cart, $customer_email)
    {
        $appointment = false;

        // Cretae a towbar provisional appointment if towbar in the cart.
        if (!empty($cart['towbar']) && $cart['towbar']['fitterType'] != 'self_fitted') {

            $appointment = self::towbar_provisional_appointment($cart, $customer_email);
        }

        // Create a provisional booking for a fitted accessory
        if (!empty($cart['contents'])) {
            foreach ($cart['contents'] as $item) {
                if ($item['type'] == 'accessory-fitted') {

                    $appointment = self::accessory_provisional_appointment($cart, $customer_email, $item);
                }
            }
        }

        return $appointment;
    }

    /**
     * Make a provisial booking for a towbar
     *
     * @param  array  $cart
     * @param  string  $customer_email
     * @return obj $appointment
     */
    public static function towbar_provisional_appointment($cart, $customer_email)
    {
        // Need to check if the fitter is mobile or not and pass it beck to the API.
        if (!empty($cart['towbar']['fitterType']) && $cart['towbar']['fitterType'] == 'mobile') {
            $is_mobile = "Y";
        } else {
            $is_mobile = "N";
        }

        // Create a provisional booking
        return self::provisional_appointment($cart, $customer_email, $is_mobile, $accessory = false);
    }

    /**
     * Make a provisial booking for an accessory
     *
     * @param  array  $cart
     * @param  string  $customer_email
     * @return obj $appointment
     */
    public static function accessory_provisional_appointment($cart, $customer_email, $item)
    {
        // Need to check if the fitter is mobile or not and pass it beck to the API.
        if (!empty($item['fitterType']) && $item['fitterType'] == 'mobile') {
            $is_mobile = "Y";
        } else {
            $is_mobile = "N";
        }

        return self::provisional_appointment($cart, $customer_email, $is_mobile, $accessory = true, $item);
    }

    /**
     * Make a provisional_appointment
     *
     * @return obj $appointment
     */
    public static function provisional_appointment($cart, $customer_email, $is_mobile, $accessory = false, $item = null)
    {
        // If not and accessory create provisional booking for Towbar.
        if (!$accessory) {
            $appointment = Appointment::create_provisional_booking([
                'vtId' => $cart['towbar']['variantTowbarId'],
                'timeslotId' => $cart['towbar']['appointmentId'],
                'bookingTime' => $cart['towbar']['appointmentTime'],
                'appointmentDate' => $cart['towbar']['appointmentDate'],
                'customerEmail' => $customer_email,
                'isMobile' => $is_mobile,
            ]);

            // Set paramter product type towbar
            $appointment->product_type = 'towbar';

        } else {
            $appointment = Appointment::create_provisional_booking([
                'partNo' => $item['partNo'],
                'timeslotId' => $item['appointmentId'],
                'bookingTime' => $item['appointmentTime'],
                'appointmentDate' => $item['appointmentDate'],
                'customerEmail' => $customer_email,
                'isMobile' => $is_mobile,
            ]);

            // Set paramter product type
            $appointment->product_type = 'accessory-fitted';
        }

        // Log as an error is appointment not available.
        if ($appointment->success == false) {
            Log::error('error provisional_appointment() accessory = ' . ($accessory ? 'yes' : 'no') . ' error: ' . json_encode($appointment));

            return $appointment;
        }

        Log::debug('success provisional_appointment() accessory = ' . ($accessory ? 'yes' : 'no') . ' success: ' . json_encode($appointment));

        return $appointment;
    }
}
