@extends('layouts.frontend')

@section('title', 'Air Suspension')

@section('heading')
	Air Suspension Kits
@stop

@section('body-class', 'suspension-kits')

@section('meta_description')
	Air Suspension Kits
@stop

@section('content')

<div class="container-fluid light-grey-background">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="mt-0">Improve stability, safety and comfort</h2>
				</div><!-- /.col-xs-12 -->
			</div><!-- /.row -->

			<div class="row suspension-flex-container">
				<div class="col-xs-12">
					<p>Air suspension can improve the stability, safety and comfort levels of your journey by correcting ride height and improving weight distribution. If you frequently drive on uneven roads or carry heavy or variable loads, an auxiliary air suspension kit could transform your journeys.</p>

					<p>VB-Airsuspension produce air suspension systems for Light Commercial Vehicles between 1.5t and 7.5t.</p>
				</div><!-- /.col-xs-12 -->

				<div class="col-xs-12">
					@include('partials.search-dropdowns-suspensions', ['redirect_slug' => 'air-suspension-kits',
					'display_models' => true,
					'display_manufacturers' => true,
					'vehicle_dropdown_url' => route('suspensions') ])
				</div><!-- /.col-xs-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.row -->
</div><!-- /.container-fluid -->
<div class="what-is">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>What is air suspension?</h2>

				<p>VB-Airsuspension systems are aftermarket accessories which can be fitted to light commercial vehicles chassis to improve safety, stability and comfort.</p>

				<p>Auxiliary air suspension (SemiAir) works with the existing leaf springs and is installed between the rear axle and chassis, giving the opportunity to raise and lower the vehicle within the limits of the leaf spring suspension.</p>

				<p>Depending on the type of auxiliary air suspension kit this can be done manually or automatically.</p>

				<p>SemiAir is often fitted together with reinforced front coil springs to give a solution for both front and rear of the vehicle.</p>

				<p>Fully automatic air suspension (FullAir) replaces the original equipment suspension completely so that the vehicle has full air suspension at the rear only, or on all 4 corners.  Fully automatic air suspension uses an ECU and height sensors to automatically adjust the suspension, ensuring that it is level and at the correct ride height regardless of load/load distribution*</p>
			</div>
		</div>
	</div>
	<img src="/assets/img/suspension/man-and-van.png" class="man-van">
</div>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<hr class="suspension-separator suspension-separator-top">
			<h2 class="hidden-xs hidden-sm">Why buy a SemiAir air suspension kit?</h2>
			<h2 class="hidden-md hidden-lg suspension-toggle"><a href="#semiair-air-suspension" class="suspension-toggle-link collapsed" data-toggle="collapse">Why buy a SemiAir air suspension kit?</a></h2>
		</div>
	</div>
	<div class="row collapse" id="semiair-air-suspension">
		<div class="col-xs-12">
			<p>SemiAir is well suited to both commercial applications such as goods transport or trade use, and to leisure applications as an entry point for air suspension for motorhomes.</p>
			<p>SemiAir is ideal if you are frequently towing or hauling heavy or often variable loads, or if you require greater ground clearance for the rear of your vehicle, or more stability while driving under load.</p>
			<p>It is a relatively inexpensive solution which is fast to install and helps to protect your vehicle and load from damage.</p>
			<p>The functionality allows the height of your vehicle (within the limits of the leaf spring suspension) to be adjusted by increasing/decreasing the air pressure in the air springs. This keeps your vehicle at a steady height above the road surface and reduces the appearance of heavy loads.</p>
		</div><!-- /.col-xs-12 -->

		<div class="col-xs-12 col-sm-4">
			<h3>Comfort</h3>
			<p>This can be increased significantly with auxiliary air suspension because the pressure in the air spring helps to restore the intended ride height and keep your vehicle level throughout the journey. It also restores the original intended spring travel, and allows the leaf spring and shock absorbers to move and function as intended</p>
		</div>
		<div class="col-xs-12 col-sm-4">
			<h3>Stability</h3>
			<p>The improved weight distribution and level ride height helps to ensure that dynamic inputs such as steering, braking and acceleration all operate as intended. The dual chamber aspect of the system helps to also stabilise off-centre loads.</p>
		</div>
		<div class="col-xs-12 col-sm-4">
			<h3>Adjustable</h3>
			<p>You will be able to adjust the height of your vehicle by adding or removing pressure from the airbags as appropriate. This gives you a variable force spring which can be adjusted to meet the requirements of your load*, and raise the rear of the vehicle, e.g. to hitch to a tow bar.</p>
		</div>
	</div>
</div>

<div class="container why-buy ">
	<div class="row">
		<div class="col-xs-12">
			<hr class="suspension-separator">
			<h2 class="hidden-xs hidden-sm">Why buy a FullAir system?</h2>
			<h2 class="hidden-md hidden-lg suspension-toggle"><a href="#fullair-air-suspension" class="suspension-toggle-link collapsed" data-toggle="collapse">Why buy a FullAir system?</a></h2>
		</div>
	</div>
	<div class="row collapse" id="fullair-air-suspension">
		<div class="col-xs-12">
			<p>FullAir offers the ultimate in comfort and functionality for commercial and leisure applications.</p>
		</div><!-- /.col-xs-12 -->

		<div class="col-xs-12 col-sm-4">
			<h3>Comfort</h3>
			<p>Removing the original suspension and replacing it with air springs means that the vehicle is fully cushioned by the air springs, reducing high frequency vibrations and unwanted noise.</p>
			<p>This makes the vehicle quieter, smoother and more pleasant to travel in, and for the transportation of fragile items helps to protect the load from damage in transit (e.g. computer equipment, fine art/antiques etc.)</p>
		</div>
		<div class="col-xs-12 col-sm-4">
			<h3>Stability</h3>
			<p>The default function of FullAir systems is to maintain a level and correct ride height regardless of load and load distribution*. As weight is added or removed from the vehicle the height sensor/s will recognise the change and feedback to the ECU which in turn will activate the compressor unit to adjust the pressure in the air spring/s appropriately.</p>
		</div>
		<div class="col-xs-12 col-sm-4">
			<h3>Functionality</h3>
			<p>The VB-ASCU control unit can be selected with different software options to give specific application functionality.</p>
			<p>The vehicle can be raised for greater ground clearance or lowered to assist loading/access to the vehicle or car park/garage access. It can also be tilted F-R/R-F to assist with ramp clearances or L-R/R-L to give better side access.</p>
			<p>There is an additional AutoLevel module for 4 Corner systems which automatically levels the vehicle when stationary.</p>
		</div>
	</div>
</div>

<div class="consider ">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<hr class="suspension-separator">
				<h2 class="hidden-xs hidden-sm">What do I need to consider?</h2>
				<h2 class="hidden-md hidden-lg suspension-toggle"><a href="#consider-air-suspension" class="suspension-toggle-link collapsed" data-toggle="collapse">What do I need to consider?</a></h2>
			</div>
		</div>
		<div class="row collapse" id="consider-air-suspension">
			<div class="col-xs-12">
				<P>The two main types of air suspension are SemiAir or FullAir. The best kit for you will depend on how you use your vehicle and what you wish to achieve from it, and what you would wish to spend.</P>
				<P>Semi or full air suspension: FullAir systems require getting your full suspension replaced by a professional. The systems comprise of many more components and therefore are considerably more expensive than SemiAir kits.</P>
				<P>With a SemiAir kit you can quickly and relatively cheaply add auxiliary air bags to your rear suspension. This fits between the rear axle and chassis of your vehicle.</P>
				<P>Cars with air suspension: Some cars will already have air suspension built in. Alternatively, you can buy a kit if you will be carrying heavy loads or want to increase your vehicle stability. The airbag suspension is like an enhanced shock absorber and can prevent movement around the corners and make journeys more comfortable on uneven roads.</P>
				<P>Motorhome air suspension: Motorhomes operate at a maximum weight capacity almost all of the time, so the regular suspension can't work as efficiently as you might like it to. You can replace the bump stops or auxiliary springs with an air suspension spring to take away some of the banging and increase stability when you are driving.</P>
			</div><!-- /.col-xs-12 -->

			<div class="col-xs-12 col-sm-6">
				<h3>Installation</h3>
				<p>We always recommend having any modifications to your vehicle being completed by a professional. It is possible to fit a SemiAir suspension kit yourself, but you should only do this if you have the correct equipment and the mechanical expertise.</p>
				<p>A FullAir suspension system can only be supplied and fitted by a VB factory accredited installer. Suspension systems are safety critical parts and require specialist tools and knowledge to work with. Using accredited installers ensures the requirements of manufacturer approvals are met and the system is installed and calibrated properly.</p>
			</div>
			<div class="col-xs-12 col-sm-6">
				<h3>Range</h3>
				<p>The VB-Airsuspension range includes coil springs (front and rear), SemiAir auxiliary air suspension (rear only) and FullAir fully automatic air suspension (4 corner and 2 corner rear).</p>
				<p>VB also offer specialist shock absorbers and stabiliser bars for certain vehicles.</p>
				<p>*Within the limitations of the maximum GVW/Axle Weight of the vehicle). In some cases an air suspension kit can be used to increase the Gross Vehicle Weight (GVW) of a commercial vehicle.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<hr class="suspension-separator suspension-separator-bottom">
			</div><!-- /.col-xs-12 -->
		</div><!-- /.row -->
	</div>
</div>

<div class="browse">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2>Browse by manufacturer</h2>
				<p>Choose the relevant vehicle manufacturer to find out which air suspension kits are suitable.</p>
			</div>
		</div>
		<div class="row ">
			<div class="col-xs-12 towbars-by-manufacturer towbars-listing">
				<?php
					// Count
					$count = count($manufacturers);
				?>
				@foreach ($manufacturers as $key => $manufacturer)
					<?php $key++ ?>

					@if($key%12 === 1)
						<div class="row lazy-load-images item-list flex-items">
					@endif

					<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
						<div class="item">
							<div class="item-body">
								<div class="manufacturer-logo">
									<a href="{{ route('suspensions.model_list', $manufacturer->slug) }}">
										<img src="" data-src="{{ image_load($manufacturer->logoUrl) }}" alt="Air Suspension Kits for {{ $manufacturer->manufacturerName }}" >
										<h3 class="title-small">{{ $manufacturer->manufacturerName }}</h3>
										<p class="small">Air Suspension Kits</p>
									</a>
								</div>
							</div><!-- /.item-body -->
						</div><!-- /.item -->
					</div>

					@if($key%12 === 0 && $key > 0 || $key == $count)
						</div><!-- .row -->
					@endif
				@endforeach
			</div><!-- /.col-xs-12 -->
		</div><!-- /.row -->
	</div>
</div>
@stop
