@extends('layouts.backend')

@section('title')
	Create Page 
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/pages/partials/_form', ['submit_text' => 'Create Page', 'type' => 'add'])
	</form>

@stop

