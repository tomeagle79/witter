<?php

namespace App\Models\Api\Commercial;

use  App\Models\Api\ApiModel;

class CommercialVehicle extends ApiModel
{
	const URI = "commercials/vehicles";

	/**
	 * Get a vehicle list
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_where(array $args)
	{
		$url = config('witter.api_base') . 'commercials/models/slug/vehicles?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a vehicle list by the product type
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_by_type(array $args)
	{
		$url = config('witter.api_base') . 'commercials/types/slug/models/slug/vehicles?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Gets roof bars based on vehicle slug
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_roofbars_by_slug($vehicle_slug){
		$url = config('witter.api_base') . 'commercials/vehicles/slug/roofbars?vehicleSlug='. $vehicle_slug;
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Gets roof decks based on vehicle slug
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_roofdecks_by_slug($vehicle_slug){
		$url = config('witter.api_base') . 'commercials/vehicles/slug/roofdecks?vehicleSlug='. $vehicle_slug;
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Gets accessories based on vehicle slug
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_accessories_by_slug($vehicle_slug){
		$url = config('witter.api_base') . 'commercials/vehicles/slug/accessories?vehicleSlug='. $vehicle_slug;
		$obj = self::get_request($url);

		return $obj;
	}
}
