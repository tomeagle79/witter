@extends('layouts.frontend')

@section('title', $towbar->title)

@section('heading')
    {{ $towbar->title }}
@stop

@section('meta_description')
    {{ $towbar->title }}
@stop

@section('body-class', 'body_towbar_listing')

@section('content')

<div class="container towbars-product-view">

    <div class="row">

        <div class="col-md-9" itemscope itemtype="http://schema.org/Product">
            <div class="towbars-product-listing">

                <div class="row headlines-container">

                    <div class="col-md-4 hidden-xs hidden-sm">
                        @if(!empty($towbar->imageUrls))
                            @if(!empty($towbar->imageUrls[0]))
                                <img src="{{ image_load($towbar->imageUrls[0]) }}" alt="" itemprop="image">
                            @endif
                        @endif
                    </div> <!-- .col-md-4 -->

                    <div class="col-md-8">
                        <div class="product-headlines">
                            <h2 itemprop="name">{{ $towbar->title }}</h2>
                            <h3>
                                For {{ $vehicle }}.
                                <a href="{{ route('towbars') }}">Change Vehicle</a>
                            </h3>

                            <div class="hidden-lg hidden-md towbar-image">
                                @if(!empty($towbar->imageUrls))
                                    @if(!empty($towbar->imageUrls[0]))
                                        <img src="{{ image_load($towbar->imageUrls[0]) }}" alt="" itemprop="image" class="hidden-lg hidden-md">
                                    @endif
                                @endif
                            </div> <!-- .col-md-4 -->

                            <div class="description hidden-xs hidden-sm" itemprop="description">
                                <p>
                                    {{ $towbar->description }}
                                </p>
                            </div> <!-- .description -->

                            <ul class="row towbar-includes">
                                @if(towbar_in_stock($towbar) && ($towbar->stockLevel > 5 || $towbar->stockLevel == 0))
                                    <li class="col-sm-6 col-xs-12 stock in-stock">
                                        <div class="include-item">In Stock</div>
                                    </li> <!-- col-sm-6 -->
                                @elseif(towbar_in_stock($towbar) && $towbar->stockLevel <= 5)
                                    <li class="col-sm-6 col-xs-12 stock low-stock">
                                        <div class="include-item">Hurry, only {{ $towbar->stockLevel }} left!</div>
                                    </li> <!-- col-sm-6 -->
                                @else
                                    <li class="col-sm-6 col-xs-12 stock out-of-stock">
                                        <div class="include-item">Out of Stock</div>
                                    </li> <!-- col-sm-6 -->
                                @endif

                                <li class="col-sm-6 col-xs-12 tick-li">
                                    <div class="include-item">Free UK Delivery</div>
                                </li> <!-- col-sm-6 -->

                                <li class="col-xs-12 tick-li">
                                    <div class="include-item">Mobile and Workshop Fitting Available</div>
                                </li> <!-- .col-md-4 col-sm-6 -->
                            </ul> <!-- .row -->
                            <div class="row mobile-pricing hidden-lg hidden-md hidden-sm">
                                <div class="col-xs-12">
                                    <p>Starting at <span>&pound;{{ price($towbar->price)}}</span></p>
                                    <a href="#order-form" class="btn btn-block btn-primary smoothscroll">Configure now</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <a href="#tech" class="view-features smoothscroll">View More Features</a>
                                </div>
                                <div class="col-md-6 col-sm-6 hidden-xs hidden-sm text-right">

                                    @if(strtolower($towbar->brandName) == 'witter')
                                        <img src="/assets/img/towbars/witter-logo.png" alt="{{ $towbar->brandName }}" width="70">
                                    @elseif(strtolower($towbar->brandName) == 'westfalia')
                                        <img src="/assets/img/towbars/westfalia-logo.png" alt="{{ $towbar->brandName }}" width="150">
                                    @endif

                                </div>
                            </div>
                        </div> <!-- .product-headlines -->
                    </div> <!-- .col-md-8 -->

                    <div class="col-xs-12 hidden-md hidden-lg">
                        <div class="product-headlines">
                            <div class="description" itemprop="description">
                                <p>
                                    {{ $towbar->description }}
                                </p>
                            </div> <!-- .description -->
                        </div> <!-- .product-headlines -->
                    </div> <!-- .col-md-8 -->

                </div> <!-- .row -->

                @if(!towbar_in_stock($towbar) && !$towbar->isMto)

                    <div class="process">
                        <div class="bordered">
                            <h3>Out of Stock</h3>

                            <p>
                                Sorry, but this product is currently out of stock.
                            </p>
                            <p>
                                We can alert you when this product comes back in stock, just your
                                details below.
                            </p>
                            <p>
                                <strong>All fields are mandatory</strong>
                            </p>

                            <form action="{{ route('stock_notifications') }}" method="post" id="oos" onsubmit="
                                gtag('event', 'Out of stock', {
                                    event_category: 'Form Submission',
                                    event_label: '{{ $towbar->title }}'
                                });
                            ">
                                {{ csrf_field() }}
                                <input type="hidden" name="vtid" value="{{ $variantTowbarId }}">
                                <input type="hidden" name="towbar" value="{{ $towbar->towbarPartNo }}">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" class="form-control" placeholder="First Name" required="required">
                                    </div> <!-- .col-sm-12 -->
                                </div> <!-- .row -->

                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" name="surname" id="surname" value="{{ old('surname') }}" class="form-control" placeholder="Surname" required="required">
                                    </div> <!-- .col-sm-12 -->
                                </div> <!-- .row -->

                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control" placeholder="Email" required="required">
                                    </div> <!-- .col-sm-12 -->
                                </div> <!-- .row -->

                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" name="mobile" id="mobile" value="{{ old('mobile') }}" class="form-control" placeholder="Mobile Number" required="required">
                                    </div> <!-- .col-sm-12 -->
                                </div> <!-- .row -->

                                <div class="row">
                                    <div class="col-lg-8 col-md-7 col-sm-6">
                                        <input type="text" name="postcode" id="postcode" value="{{ old('postcode') }}" class="form-control" placeholder="Postcode" required="required">
                                    </div> <!-- .col-md-9 -->
                                    <div class="col-lg-4 col-md-5 col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block" id="btn-oos">Receive Updates</button>
                                    </div> <!-- .col-md-3 -->
                                </div> <!-- .row -->
                            </form>
                        </div> <!-- .bordered -->
                    </div> <!-- .process -->

                @else
                    <form action="{{ route('add_to_cart') }}" method="post" id="order-form" data-price="{{ price($towbar->price) }}">
                        {{ csrf_field() }}

                        <div class="panel-group">
                            <div class="panel-towbar-options towball @if($towball_included) selected @endif" id="towball-options">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#towball">1. <span>Choose a towball</span></a>
                                        <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                        title="
                                            <h4>Choose a towball?</h4>
                                            <hr />
                                            <p>The towball is the circular piece of metal that sits at the top of your towbar. When you hitch up a caravan or trailer, the towball is the part of the towbar that the hitch ‘couples’ to. The towball is sometimes welded onto the towbar as one solid piece or it  could be removable, depending on which towbar you have chosen.</p>
                                        "></i>
                                    </h4>

                                    <div class="panel-title-icons">
                                        <div class="unselected">
                                            <a data-toggle="collapse" href="#towball" class="unselected-plus"><i class="icon-plus-circle"></i></a>
                                        </div><!-- /.unselected -->

                                        <div class="selected">
                                            <a data-toggle="collapse" href="#towball" class="selected-pen"><i class="icon-pen"></i></a>
                                            <a data-toggle="collapse" href="#towball" class="selected-tick"><i class="icon-green-tick"></i></a>
                                        </div><!-- /.selected -->
                                    </div><!-- /.panel-title-icons -->
                                </div>
                                <div id="towball" class="panel-collapse in">
                                    <div class="panel-body">

                                        @if($towball_included)
                                            <ul class="towbar-option-list">
                                                <li class="towbar-option">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <h5>Towball Included</h5>
                                                        </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->

                                                        <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                                                            <input id="towball_option" type="radio" name="towball_option" value="" data-name="Towball Included" data-price="0" data-towballOnlyPrice="0" checked="checked" checked>
                                                            <label for="towball_option"><span class="btn btn-grey"><span class="sr-only">Select</span></span></label>
                                                        </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->
                                                    </div>
                                                </li>
                                            </ul><!-- /.towbar-option -->
                                        @else
                                            <ul class="towbar-option-list">
                                            @foreach($towbar->towballOptions as $towball_option)
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <h5>{{ $towball_option->title }}</h5>

                                                        </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->
                                                        <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                                                            <input type="radio" name="towball_option" value="{{ $towball_option->partNo }}" data-name="{{ $towball_option->title }}" data-price="{{ price($towball_option->priceAdjustment) }}" data-towballOnlyPrice="{{ price($towball_option->towballOnlyPrice) }}" data-name="{{ $towball_option->title }}" id="towball_option_{{ $loop->iteration }}">

                                                            <label for="towball_option_{{ $loop->iteration }}"><span class="btn btn-grey"><span class="sr-only">Select</span></span></label>

                                                        </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->
                                                    </div><!-- /.row -->
                                                </li>

                                            @endforeach
                                            </ul><!-- /.towbar-option-list -->

                                        @endif
                                    </div><!-- /.panel-body -->
                                </div><!-- /.panel-collapse -->
                            </div><!-- /.panel-towbar-options -->

                            <div class="panel-towbar-options" id="electric-options">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#electric-kit">2. <span>Choose an electric kit</span></a>
                                        <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                        title="
                                            <h4>What is an electrical kit?</h4>
                                            <hr />
                                            <p>An electrical kit connects your vehicle electrics with the electrics on your trailer or caravan when you are towing. This is a legal requirement so that other road users can still clearly see your light signals and indicators. The electrical kit includes a plug which will be located near your towbar and attaches to the plug on your trailer or caravan to synchronise the electrics.</p>
                                        "></i>
                                    </h4>
                                    <div class="panel-title-icons">

                                        <div class="unselected">
                                            <a data-toggle="collapse" href="#electric-kit" class="unselected-plus"><i class="icon-plus-circle"></i></a>
                                        </div><!-- /.unselected -->

                                        <div class="selected">
                                            <a data-toggle="collapse" href="#electric-kit" class="selected-pen"><i class="icon-pen"></i></a>
                                            <a data-toggle="collapse" href="#electric-kit" class="selected-tick"><i class="icon-green-tick"></i></a>
                                        </div><!-- /.selected -->

                                    </div><!-- /.panel-title-icons -->
                                </div>

                                <div id="electric-kit" class="panel-collapse collapse @if($towball_included) in @endif">
                                    <div class="panel-body">

                                        <div class="alert alert-danger" role="alert" id="no-kit" style="display: none;">
                                            <p>Please choose an electric kit before booking your fitting</p>
                                        </div>

                                        <div class="alert alert-danger" role="alert" id="purchase-no-kit" style="display: none;">
                                            <p>Please choose an electric kit before adding to your basket</p>
                                        </div>

                                        <ul class="towbar-option-list">
                                            @foreach($towbar->electricOptions as $electric_option)
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                                            <h5>{{ $electric_option->title }}</h5>

                                                        </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->

                                                        <div class="col-md-6 col-xs-12 text-right">
                                                            <a class="benefits-toggle-link collapsed" data-toggle="collapse" href="#electrical_kit_info_{{ $loop->iteration }}">More information</a>

                                                            <input id="electrical_kit_{{ $loop->iteration }}" type="radio" name="electric_option" value="{{ $electric_option->partNo }}" data-price="{{ price($electric_option->priceAdjustment) }}" data-name="{{ $electric_option->title }}" data-kitOnlyPrice="{{ $electric_option->kitOnlyPrice }}" @if($electric_option->inStock == false) disabled="disabled" @endif data-can_code="{{ $electric_option->mayNeedRecoding }}" data-dealer_coding="{{ empty($electric_option->dealerRecodingRequired) ? 0 : 1 }}"
                                                            {{-- Software update title. Check if codeing not required/possible  --}}
                                                            @if(empty($electric_option->mayNeedRecoding))
                                                                data-software_title="Software Update not required"
                                                            @elseif(!empty($electric_option->mayNeedRecoding && !empty($electric_option->dealerRecodingRequired)))
                                                                data-software_title="Software Update available from manufacturer"
                                                            @else
                                                                data-software_title="Software Update"
                                                            @endif
                                                            >
                                                            <label for="electrical_kit_{{ $loop->iteration }}" @if($electric_option->inStock == false) class="outofstock" @endif>@if($electric_option->inStock == false)<p class="text-danger">Sorry, this kit is out of stock.</p>@else<span class="btn btn-grey"><span class="sr-only">Select</span></span>@endif</label>
                                                        </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->

                                                    </div><!-- /.row -->
                                                    <div class="row">
                                                        <div class="col-md-12">

                                                            <div id="electrical_kit_info_{{ $loop->iteration }}"  class="collapse">

                                                                @if($electric_option->kitIdentifier == 'S13')

                                                                    @include('towbars.partials.electrical_kit_13_universal')

                                                                @elseif($electric_option->kitIdentifier == 'S7' )

                                                                    @include('towbars.partials.electrical_kit_7_universal')

                                                                @elseif($electric_option->kitIdentifier == 'D7')

                                                                    @include('towbars.partials.electrical_kit_7')

                                                                @elseif($electric_option->kitIdentifier == 'D13' )

                                                                    @include('towbars.partials.electrical_kit_13')

                                                                @else
                                                                    <p>
                                                                        {{ $electric_option->description }}
                                                                    </p>
                                                                @endif

                                                            </div>
                                                        </div><!-- /.col-md-6 -->

                                                    </div><!-- /.row -->

                                                </li>
                                            @endforeach

                                            @if(!$electric_kit_included)
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-6">

                                                            <h5>I have my own electrical kit or integral kit</h5>

                                                        </div><!-- /.col-md-6 -->

                                                        <div class="col-md-6 text-right">

                                                            <input id="no_electrical_kit" type="radio" name="electric_option" value="None" data-price="0" data-name="I have my own electrical kit or integral kit" data-can_code="0" data-dealer_coding="0">
                                                            <label for="no_electrical_kit">
                                                                <span class="btn btn-grey"><span class="sr-only">Select</span></span>
                                                            </label>
                                                        </div><!-- /.col-md-6 -->

                                                    </div><!-- /.row -->
                                                    <div class="row">
                                                        <div class="col-xs-12" id="fitting-option-warning" style="display: none;">
                                                            <div class="col-xs-12 alert alert-danger" role="alert">
                                                                <div class="col-xs-12 col-md-9">
                                                                    <p>Unfortunately our Witter Fitters cannot fit a Towbar without an electrical kit. Do you have your own fitter to fit this Towbar?</p>
                                                                </div><!-- /.col-xs-12 .alert .alert-warning -->
                                                                <div class="col-xs-12 col-md-3">
                                                                    <a class="btn btn-primary pull-right" id="confirm-own-fitter" href="#">Yes</a>
                                                                </div><!-- /.col-xs-12 .alert .alert-warning -->
                                                            </div><!-- /.col-xs-12 -->
                                                        </div><!-- /.col-xs-12 -->
                                                    </div><!-- /.row -->
                                                </li>

                                            @endif

                                        </ul>
                                    </div><!-- /.panel-body -->
                                </div><!-- /.panel-collapse -->
                            </div><!-- /.panel-towbar-options -->

                            <div class="panel-towbar-options" id="software-upgrade">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#software-upgrade-tab">3. <span>Software Update</span></a>
                                        <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                        title="
                                            <h4>What is a software upgrade?</h4>
                                            <hr />
                                            <p>Software Updates activate additional benefits and safety features.</p>
                                        "></i>
                                    </h4>
                                    <div class="panel-title-icons">

                                        <div class="unselected">
                                            <a data-toggle="collapse" href="#software-upgrade-tab" class="unselected-plus"><i class="icon-plus-circle"></i></a>
                                        </div><!-- /.unselected -->

                                        <div class="selected">
                                            <a data-toggle="collapse" href="#software-upgrade-tab" class="selected-pen"><i class="icon-pen"></i></a>
                                            <a data-toggle="collapse" href="#software-upgrade-tab" class="selected-tick"><i class="icon-green-tick"></i></a>
                                        </div><!-- /.selected -->

                                    </div><!-- /.panel-title-icons -->
                                </div>

                                <div id="software-upgrade-tab" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <p class="can-code">Our records show that your vehicle requires a software update to activate all additional benefits and safety features. We can offer this coding upgrade as part of the fitting for an additional fee of &pound;{{ $software_upgrade_price }}.</p>

                                        <p class="cannot-code" style="display: none;">A software update is not needed for this setup.</p>
                                        <a data-toggle="collapse" href="#towbar-fitting" class="btn btn-primary cannot-code" style="display: none;">Next Step</a>

                                        <p class="dealer-code">Please note that your vehicle requires coding. However, this can currently only be done by a dealer of your vehicle's manufacturer.</p>

                                        <p class="dealer-code">Please check <strong>before</strong> booking whether they are able to do this. If you have any questions please give us a call on 01244 284 555.</p>

                                        <a data-toggle="collapse" href="#towbar-fitting" class="btn btn-primary dealer-code" style="display: none;">Next Step</a>

                                        <ul class="towbar-option-list">
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h5>I would like a software update</h5>
                                                    </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->

                                                    <div class="col-md-6 col-xs-12 text-right">
                                                    <input type="radio" name="software_upgrade" id="software_upgrade_true" value="true" data-price="{{ $software_upgrade_price }}" data-name="Software Update included">
                                                        <label for="software_upgrade_true">
                                                            <span class="btn btn-grey"><span class="sr-only">Select</span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h5>I do not want a software update</h5>
                                                    </div><!-- /.col-md-6 col-sm-6 col-xs-12 -->

                                                    <div class="col-md-6 col-xs-12 text-right">
                                                        <input type="radio" name="software_upgrade" id="software_upgrade_false" value="false" data-price="0" data-name="Software Update excluded">
                                                        <label for="software_upgrade_false">
                                                            <span class="btn btn-grey"><span class="sr-only">Select</span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                    </div><!-- /.panel-body -->
                                </div><!-- /.panel-collapse -->
                            </div><!-- /.panel-towbar-options -->

                            <div class="panel-towbar-options" id="fitting-options">
                                <div class="panel-heading">

                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#towbar-fitting">4. <span>Book Your Fitting</span></a>
                                        <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                        title="
                                            <h4>Book your fitting?</h4>
                                            <hr />
                                            <p>To book your towbar fitting, enter your postcode to find your nearest fitting centre or select a mobile fitter who can come to you. When you book your towbar fitting through our website, we will guarantee the labour if you have any problems after the towbar has been fitted. If you only purchase the towbar from us and take it to your own fitter, the labour will not be covered by the guarantee. To book a fitter, just select a date and time that is best for you and we will take care of the rest.</p>
                                        "></i>
                                    </h4>

                                    <div class="panel-title-icons">

                                        <div class="unselected">

                                            <a data-toggle="collapse" href="#towbar-fitting" class="unselected-plus"><i class="icon-plus-circle"></i></a>
                                        </div><!-- /.unselected -->

                                        <div class="selected">
                                            <a data-toggle="collapse" href="#towbar-fitting" class="selected-pen"><i class="icon-pen"></i></a>
                                            <a data-toggle="collapse" href="#towbar-fitting" class="selected-tick"><i class="icon-green-tick"></i></a>
                                        </div><!-- /.selected -->

                                    </div><!-- /.panel-title-icons -->
                                </div>

                                <div id="towbar-fitting" class="panel-collapse collapse">

                                    <div class="panel-body">

                                        <div class="alert alert-danger" role="alert" id="purchase-no-fitting" style="display: none;">
                                            <p>Please choose a fitting before adding to your basket</p>
                                        </div>

                                        {{--
                                        <div class="find-fitter-form">
                                            <p class="uppercase">
                                                Enter your postcode to find your nearest fitter, and view available booking dates.
                                            </p> <!-- .uppercase -->

                                            <div class="alert alert-danger" role="alert" id="no-postcode" style="display: none;">
                                                <p>Please enter a postcode</p>
                                            </div>

                                            <div class="alert alert-danger" role="alert" id="no-kit" style="display: none;">
                                                <p>Please choose an electric kit before booking your fitting</p>
                                            </div>

                                            <div class="alert alert-danger" role="alert" id="error" style="display: none;">
                                                <p></p>
                                            </div>

                                            <div class="alert alert-info" role="alert" id="loading" style="display: none;">
                                                <p>Loading...</p>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="text" name="postcode" id="postcode" value="" class="form-control" placeholder="Postcode">
                                                </div> <!-- .col-md-9 -->
                                            </div> <!-- .row -->

                                            <div class="alert alert-info" role="alert" id="loading" style="display: none;">
                                                <p>Loading...</p>
                                            </div>

                                            <div class="alert alert-info" role="alert" id="reload" style="display: none;">
                                                <p>
                                                    Once you have started your order of a towbar you are unable to change the Electrical Kit
                                                    or Towball choice.  If you selected the wrong Electrical Kit or Towball you can
                                                    <a href="">start the process again</a>.
                                                </p>
                                            </div>

                                            <div id="selected-fitter"></div>
                                        </div> <!-- .find-fitter-form -->
                                        --}}

                                        <div class="type_of_fitting">
                                            <div class="panel-heading-fitting panel-heading-fitting-garage" role="tab" id="headingOne">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="fitting-title fitting-title-garage">

                                                            <h4 class="panel-title">
                                                                I would like to take my vehicle to a workshop
                                                                <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                                                title="
                                                                    <h4>Fitting Centre</h4>
                                                                    <hr />
                                                                    <p>We have a network of approved fitters who can install your towbar, and if you book the fitting through our website then the labour will be covered as part of your guarantee. Just enter your postcode to find your nearest fitting centre. Book your towbar fitting for a date and time to suit you and arrive at the fitting centre in plenty of time for your scheduled appointment. Your towbar fitting will take around 3-4 hours.</p>
                                                                "></i>
                                                            </h4>

                                                        </div><!-- /.fitting-title-mobile -->
                                                    </div><!-- /.col-md-9 -->

                                                    <div class="col-md-3 text-right title-buttons">

                                                        <input id="fitting_option_workshop" type="radio" name="fitting_option" value="workshop" data-name="Workshop fitting" data-price="0">
                                                        <label for="fitting_option_workshop"><span class="btn btn-grey select-fitter-option" data-type="workshop" data-for="fitting_option_workshop"><span class="sr-only">Select</span></span></label>
                                                    </div><!-- /.col-md-3 -->

                                                    <div class="col-xs-12">
                                                        <div class="selected-fitter" id="selected-fitter"></div>
                                                    </div><!-- /.col-xs-12 -->

                                                </div><!-- /.row -->
                                            </div>
                                            <div class="panel-heading-fitting panel-heading-fitting-mobile" role="tab" id="headingTwo">

                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="fitting-title fitting-title-mobile">
                                                            <h4 class="panel-title">
                                                                I would like a mobile fitter to come to me
                                                                <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                                                title="
                                                                    <h4>Mobile Fitter</h4>
                                                                    <hr />
                                                                    <p>Choosing a mobile fitter couldn’t be simpler or more convenient. We have already sourced reliable fitters, so you don’t have to, plus the labour will be guaranteed when you book your towbar fitting through us. Fitters can come to your home or work address, so simply enter the postcode for where you would like the fitting to take place. Just make sure that your chosen location has safe and secure off road parking for your car and the fitter’s vehicle. The fitter will need full access to your car and installation takes around 3-4 hours.</p>
                                                                "></i>
                                                            </h4>
                                                        </div><!-- /.fitting-title-mobile -->
                                                    </div><!-- /.col-md-9 -->

                                                    <div class="col-md-3 text-right title-buttons">
                                                        <input id="fitting_option_mobile" type="radio" name="fitting_option" value="mobile" data-name="Mobile fitting" data-price="25">
                                                        <label for="fitting_option_mobile"><span class="btn btn-grey select-fitter-option" data-type="mobile" data-for="fitting_option_mobile"><span class="sr-only">Select</span></span></label>
                                                    </div><!-- /.col-md-3 -->

                                                    <div class="col-xs-12">
                                                        <div class="selected-fitter" id="selected-fitter-mobile"></div>
                                                    </div><!-- /.col-xs-12 -->
                                                </div><!-- /.row -->

                                            </div>

                                            <div id="own-fitter-software-upgrade-error" style="display: none; margin-top: 20px;">
                                                <div class="alert alert-danger">
                                                    <p>You cannot use your own garage if opting for a software upgrade. Please select either workshop or mobile fitting, or deselect the software upgrade option.</p>
                                                </div>
                                            </div>

                                            <div class="panel-heading-fitting" role="tab" id="headingThree">

                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="fitting-title fitting-title-self">
                                                            <h4 class="panel-title">

                                                                I have my own fitter
                                                                <i class="icon-question-mark tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom" data-container="body"
                                                                title="
                                                                    <h4>I have my own fitter</h4>
                                                                    <hr />
                                                                    <p>Finding your own fitter can be a risk. If a fitter has not been approved by us, and something goes wrong, our guarantee does not cover the labour. Our guarantee, however, will still cover the parts you bought from us.</p>
                                                                "></i>
                                                            </h4>
                                                        </div><!-- /.fitting-title-self -->

                                                    </div><!-- /.col-md-9 -->

                                                    <div class="col-md-3 text-right title-buttons">
                                                        <input id="fitting_option_self_fitted" type="radio" name="fitting_option" value="self_fitted" data-price="0" data-name="I have my own fitter">
                                                        <label for="fitting_option_self_fitted"><span class="btn btn-grey select-fitter-option"  data-type="self_fitted" data-for="fitting_option_self_fitted"><span class="sr-only">Select</span></span></label>
                                                    </div><!-- /.col-md-3 -->
                                                </div><!-- /.row -->
                                            </div>
                                        </div>
                                    </div><!-- /.panel-body -->
                                </div><!-- /.panel-collapse -->
                            </div><!-- /.panel-towbar-options -->

                            <div class="price-bar">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <p class="type">Your customised price:</p>
                                    </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->
                                    <div class="col-md-4 col-sm-4 col-xs-12 price-wrapper">
                                        &pound;<span class="final-price">0</span><span class="vat">Inc. VAT</span>
                                    </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="btn-table">
                                            <button type="submit" class="btn btn-primary btn-block order-btn submit-towbar-form">Add to Basket</button>
                                        </div> <!-- .btn-table -->
                                    </div> <!-- .col-md-6 col-sm-4 col-xs-12 -->
                                </div> <!-- .row -->
                            </div> <!-- .price-bar -->
                        </div><!-- /.panel-group -->

                        <input type="hidden" name="type" id="fld_type" value="towbar">
                        <input type="hidden" name="slug" id="fld_slug" value="{{ $towbar->slug }}">
                        <input type="hidden" name="registrationDate" id="fld_registration_date" value="{{ $registration_date or '' }}">
                        <input type="hidden" name="variantTowbarId" id="fld_variantTowbarId" value="">
                        <input type="hidden" name="vehicleDesc" id="fld_vehicleDesc" value="">
                        <input type="hidden" name="partNo" id="fld_partNo" value="">
                        <input type="hidden" name="appointmentDate" id="fld_appointmentDate" value="">
                        <input type="hidden" name="appointmentTime" id="fld_appointmentTime" value="">
                        <input type="hidden" name="appointmentId" id="fld_appointmentId" value="">
                        <input type="hidden" name="partnerId" id="fld_partnerId" value="">
                        <input type="hidden" name="address" id="fld_address" value="">
                        <input type="hidden" name="fitterType" id="fld_fitterType" value="">
                        <input type="hidden" name="fitterFee" id="fld_fitterFee" value="0">

                        {{--  Just buying a towbar price --}}
                        <input type="hidden" name="towbarOnlyPrice" id="towbarOnlyPrice" value="{{ $towbar->towbarOnlyPrice }}">
                        <input type="hidden" name="postcode" id="postcode" value="">
                    </form>
                @endif
            </div><!-- /.towbars-product-listing -->

        </div> <!-- .col-md-6 -->

        <div class="col-md-3 hidden-xs hidden-sm">

            @include('partials.sidebar-webfit')

            <div class="all-in-one">
                <h4>You have currently customised your Towbar as follows:</h4>

                <ul class="all_in_one_items">
                    @if($towball_included)
                        <li class="towball_option">Towball included</li>
                    @endif
                </ul>

                <p class="price">&pound;<span class="final-price">{{ price($towbar->price)}}</span> <span class="inc">Inc. VAT</span></p>

                {{-- <a href="#" class="btn btn-block btn-primary">Add to basket</a> --}}
                <button type="submit" class="btn btn-primary btn-block order-btn submit-towbar-form">Add to Basket</button>

            </div>
            <div class="benefits-of-wiring">
                <div class="panel-group" id="benefitsAccordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <a role="button" data-toggle="collapse" data-parent="#benefitsAccordion" href="#benefitsAccordionOne" aria-expanded="true" aria-controls="benefitsAccordionOne">
                            <div class="panel-heading" role="tab" id="benefitsAccordionHeadingOne">
                                <h4 class="panel-title">
                                    The benefits of a dedicated wiring kit?
                                </h4>
                            </div>
                        </a>
                        <div id="benefitsAccordionOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="benefitsAccordionHeadingOne">
                            <div class="panel-body">
                                <ul class="benefits">
                                    <li>Helps optimise your car fuel efficiency for trailer operation</li>
                                    <li>Failure messages in the cockpit alerting you to dangers</li>
                                    <li>Integrates with your vehicle manufacturer’s safety features and retains any manufacturer’s warranty</li>
                                    <li>Offers EPS trailer stabilisation and reduces the chances of ‘snaking’</li>
                                    <li>Shuts off parking sensors when in towing mode eliminating false feedback</li>
                                    <li>Lane assist to aid navigation on busy motorways</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- .col-md-3 -->
    </div> <!-- .row -->

    <div class="row details_tech" id="tech">
        @if(!empty($towbar->productDetails))
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="panel-group" id="techAccordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-product-info">
                        <div class="panel-heading active">
                            <a data-toggle="collapse" data-parent="#techAccordion" href="#techDetails"><h4 class="panel-title">Technical Details</h4></a>
                        </div><!-- /.panel-heading -->
                        <div id="techDetails" class="panel-collapse collapse in">
                            <div class="product-info-rows">
                                @foreach($towbar->productDetails as $attribute)
                                    <div class="row">
                                        <div class="col-xs-6">
                                            {{ $attribute->prefix }}
                                        </div><!-- /.col-xs-6 -->

                                        <div class="col-xs-6 text-right">
                                            {{ $attribute->text }}
                                        </div><!-- /.col-xs-6 -->
                                    </div><!-- /.row -->
                                @endforeach
                            </div><!-- /.info-list -->
                        </div><!-- /.panel-collapse -->
                    </div><!-- /.panel -->
                </div>
            </div>
        @endif
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="panel-group" id="infoAccordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-product-info">
                    <div class="panel-heading active">
                        <a data-toggle="collapse" data-parent="#infoAccordion" href="#delivery"><h4 class="panel-title">Delivery</h4></a>
                    </div><!-- /.panel-heading -->
                    <div id="delivery" class="panel-collapse collapse in">
                        <div class="product-info-rows">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>
                                        When you choose to get your Towbar fitted at one of our approved fitting centres or with one of our mobile fitters, your Towbar will be delivered to your fitter ready for installation.
                                    </p>
                                    <p>
                                        However, if you want to choose your own fitter, your towbar and electrics will be delivered to your chosen address on the next working day you place your order before 2:30pm.
                                    </p>
                                </div><!-- /.col-xs-6 -->
                            </div><!-- /.row -->
                        </div><!-- /.info-list -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->
                <div class="panel panel-product-info">
                    <div class="panel-heading active">
                        <a data-toggle="collapse" data-parent="#infoAccordion" href="#fitting"><h4 class="panel-title">Fitting</h4></a>
                    </div><!-- /.panel-heading -->
                    <div id="fitting" class="panel-collapse collapse">
                        <div class="product-info-rows">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>
                                        It is essential that you should get your towbar fitted by a professional fitter to protect your vehicle from unnecessary damage and to protect your guarantee. We have saved you the trouble of finding a fitter, so all you need to do is enter your postcode and we will find your nearest fitting centre or mobile fitter.
                                    </p>
                                </div><!-- /.col-xs-6 -->
                            </div><!-- /.row -->
                        </div><!-- /.info-list -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->
            </div>
        </div>
    </div>

    @if($towbar->recommendedAccessories)
    <div class="row recommended">
        <div class="col-xs-12">
            <h5>You may also be interested in…</h5>
        </div>
        @foreach($towbar->recommendedAccessories as $accessory)
            <div class="xol-xs-12 col-sm-6 col-md-3">
                <div class="item">
                    <a href="{!! buildLinkToAccessoryView($accessory) !!}" ><img src="{{ $accessory->imageUrl }}" alt=""></a>
                    <a href="{!! buildLinkToAccessoryView($accessory) !!}" ><h3 class="list-title">{!! $accessory->title !!}</h3></a>

                    <hr>

                    <div class="row">
                        <div class="col-xs-12">
                            <p class="price">&pound;{{ price($accessory->displayPrice) }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <p class="price-rrp">RRP &pound;{{ price($accessory->displayPrice) }}</p>
                        </div>
                    </div>

                    @if($accessory->inStock)
                        <div class="row stock">
                            <div class="col-xs-12">
                                <i class="icon-check"></i>
                                <p>In Stock</p>
                            </div>
                        </div>
                    @else
                        <div class="row stock">
                            <div class="col-xs-12">
                                <i class="icon-cancel-1"></i>
                                <p>Out of stock</p>
                            </div>
                        </div>
                    @endif

                    @if($accessory->inStock)
                        <div class="row">
                            <div class="col-xs-7">
                                <a href="{!! buildLinkToAccessoryView($accessory) !!}" class="btn btn-primary">View Now</a>
                            </div><!-- /.col-xs-7 -->
                            <div class="col-xs-5">
                                <a href="{{ route('add_to_cart', [
                                    'type' => 'accessory',
                                    'partNo' => $accessory->partNo
                                ]) }}" class="btn btn-primary btn-green-plus"> <img src="/assets/img/basket.svg" height="20" width="20" alt="Add to basket"></a>
                            </div>
                        </div><!-- /.row -->
                    @else
                        <div class="row">
                            <div class="col-xs-12">
                                <a diabled class="btn btn-primary btn-oos-model">Notify me when stock arrives</a>
                            </div>
                        </div><!-- /.row -->
                    @endif

                    @if($accessory->inStock == false)
                        <div class="out-of-stock-modal">
                            <div class="close-modal"><i class="icon-cancel-1"></i></div>
                            <img src="{{URL('assets/img/icons', ['filename' => 'basket.svg'])}}" alt="">
                            <p>We’re sorry we have no stock</p>
                            <h5>We’d like to let you know when it’s back…</h5>
                            <p>Fill in your email address here and as soon as it comes back into stock we’ll send you an update:</p>

                            {!! Form::open(['route' => 'product.email_notify', 'onsubmit' => "
                                gtag('event', '". ( (isset($accessory->isSellable) && $accessory->isSellable == true) ? 'Out of stock' : 'Not for sale') ."', {
                                    event_category: 'Form Submission',
                                    event_label: '" . encode_url($accessory->title)  . "'
                                });
                            "]) !!}
                                {{ Form::email('email', null, ['id' => 'email', 'placeholder' => 'Email Address']) }}
                                {{ Form::hidden('product_id', $accessory->partNo, ['id' => 'product_id']) }}
                                {{ Form::hidden('product_name', $accessory->title, ['id' => 'product_name']) }}
                                {{ Form::hidden('product_url', route('accessories.multi', [encode_url($accessory->partNo)]), ['id' => 'product_url']) }}
                                {{ Form::hidden('checkbox_for_subscription', false, ['id' => 'checkbox_for_subscription']) }}

                                <button type="submit" class="btn btn-primary btn-green-basic">Notify Me</button>
                                <input type="checkbox" name="placeholder_for_checkbox" id="placeholder_for_checkbox">
                                <label for="placeholder_for_checkbox">Would you also like to subscribe to news and offers from Witter?</label>
                            {!! Form::close() !!}
                        </div>
                    @endif
                </div>
            </div>
            @break($loop->index == 1)
        @endforeach
    </div>
    @endif

</div> <!-- .container -->

<div class="modal fade find-your-fitter" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="loading-modal-body"></div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
                <h4 class="modal-title">Select a fitting location</h4>
            </div> <!-- .modal-header -->
            <div class="modal-body">
                <div class="fitters">
                    <div class="form">
                        <label>Your Postcode</label>
                        <input type="text" name="postcode" id="postcode_update" value="" class="form-control">
                        <button type="button" id="update" class="btn btn-primary btn-primary-double-line">Find<br>Fitters</button>
                    </div> <!-- .form -->
                </div>
            </div> <!-- .modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section('scripts')
    <script>
        @include('partials.analytics.product-view', [
            'product' => $towbar,
            'category' => 'Towbars'
        ])


    /*
     * Submit the user's postcode to find fitters
     *
     */
    function findFitters(partNo, vehicleDesc, variantTowbarId, postcode){

        var towball_option = $('input[name=towball_option]:checked').val();
        var electric_option = $('input[name=electric_option]:checked').val();
        var fitting_type = $( '#fld_fitterType' ).val();
        var requires_coding = $('input[name="software_upgrade"]:checked').val();

        $('#no-postcode, #no-kit, #loading, #error').hide();

        $("#modal .loading-modal-body").show();

        if(!postcode.length) {
            $('#no-postcode').fadeIn('slow');
        } else {
            $('#loading').fadeIn('slow');
            $.ajax({
                type: 'POST',
                url: "{{ route('appointments.ajax_list_fitters')}}",
                data: {
                    'postcode': postcode,
                    'partNo': partNo,
                    'variantTowbarId': variantTowbarId,
                    'vehicleDesc': vehicleDesc,
                    'towball_option': towball_option,
                    'electric_option': electric_option,
                    'fitting_type' : fitting_type,
                    'requires_coding': requires_coding,
                },
                success: function(result){
                    $('#loading').fadeOut('slow');

                    if(result.success == false) {
                        $('#error p').html(result.errorMessage);
                        $('#error').fadeIn('slow');
                    } else {

                        if(fitting_type == 'mobile') {
                            $('.standard-row').hide();
                        } else if(fitting_type == 'workshop') {
                            $('.mobile-row').hide();
                        }
                        // Fitting process has begun so readonly everything - client request
                        // $('input[type=radio]:not(:checked)').prop('disabled', true);

                        $('#reload').fadeIn('slow');

                        $('#modal .modal-title').html( 'Book your fitting' );
                        $('#modal .modal-content .modal-body').html(result.modal);
                        $('#modal').modal('show');

                        // Save the postcode to entry
                        $('#postcode').val(postcode.toUpperCase());
                    }

                    $("#modal .loading-modal-body").hide();
                },
                error: function(){
                    alert('Sorry, something went wrong');

                    $("#modal .loading-modal-body").hide();
                }
            });
        }
    }

    $(document).ready(function(){
        var $root = $('html, body');
        var partNo = '{{ $towbar->towbarPartNo }}';
        var vehicleDesc = '{{ addslashes($vehicle) }}';
        var variantTowbarId = '{{ $variantTowbarId }}';

        calculatePrice();

        function resetFitting() {
            $('#fld_partNo').val('');
            $('#fld_variantTowbarId').val('');
            $('#fld_vehicleDesc').val('');
            $('#fld_appointmentTime').val('');
            $('#fld_appointmentId').val('');
            $('#fld_appointmentDate').val('');
            $('#fld_partnerId').val('');
            $('#fld_fitterType').val('');
            $('#fld_fitterFee').val('');
            $('#fld_address').val('');

            $('#towbar-fitting input').prop('checked', false);

            $('#fitting-option-warning').hide();

            return true;
        }

        function setSelfFittedValues(fitting_type) {

            var fitting_type = 'self_fitted';

            // Set the various order fields
            $('#fld_partNo').val(partNo);
            $('#fld_variantTowbarId').val(variantTowbarId);
            $('#fld_vehicleDesc').val(vehicleDesc);
            $('#fld_fitterType').val(fitting_type);
            $('#fld_appointmentTime').val('');
            $('#fld_appointmentId').val('');
            $('#fld_appointmentDate').val('');
            $('#fld_partnerId').val('');
            $('#fld_fitterFee').val('');
            $('#fld_address').val('');

            // Tidy the display
            $('#selected-fitter').css('display', 'none');
            $('#selected-fitter-mobile').css('display', 'none');
            $('#fitting_option_self_fitted').prop("checked", true);

            // This needs to be last as the '#fitting_option_self_fitted' needs to be checked.
            calculatePrice();

            return true;
        }

        $('#confirm-own-fitter').on('click', function(e) {

            e.preventDefault();

            resetFitting();
            // Select the no electrical kit radio button for the user.
            $('#no_electrical_kit').prop('checked', true);
            var panel_selected = $('#no_electrical_kit').closest('.panel-towbar-options');
            var panel_name = $('#no_electrical_kit').data('name');
            var id_clicked = $('#no_electrical_kit').attr('name');

            // Add styles to the panel heading.
            panel_selected.addClass('selected');
            panel_selected.find('.panel-heading h4 span').text(panel_name);

            if($(".all_in_one_items").find('.' + id_clicked)['length'] > 0) {
                $(".all_in_one_items").find('.' + id_clicked).remove();
                $(".all_in_one_items").append('<li class="' + id_clicked + '">' + panel_name +'</li>');
            } else {
                $(".all_in_one_items").append('<li class="' + id_clicked + '">' + panel_name +'</li>');
            }

            // Open the fitter panel
            $('#towball').collapse('hide');
            $('#electric-kit').collapse('hide');
            $('#software-upgrade').collapse('hide');
            $('#towbar-fitting').collapse('show');

            // Set software-upgrade as sleected
            $('#software-upgrade').addClass('selected');

            // Change the software-upgrade title
            $('#software-upgrade .panel-heading h4 span').text("Software Update not required");

            // Select the self fitted option
            setSelfFittedValues();

            // Get the radio button
            $('#fitting_option_self_fitted').prop("checked", true);
            var self_fitted_radio_button = $('#fitting_option_self_fitted');

            self_fitted_radio_button.prop('checked', true);
            var panel_selected = self_fitted_radio_button.closest('.panel-towbar-options');
            var panel_name = self_fitted_radio_button.data('name');
            var id_clicked = self_fitted_radio_button.attr('name');

            // Add styles to the panel heading.
            panel_selected.addClass('selected');
            panel_selected.find('.panel-heading h4 span').text(panel_name);

            if($(".all_in_one_items").find('.' + id_clicked)['length'] > 0) {
                $(".all_in_one_items").find('.' + id_clicked).remove();
                $(".all_in_one_items").append('<li class="' + id_clicked + '">' + panel_name +'</li>');
            } else {
                $(".all_in_one_items").append('<li class="' + id_clicked + '">' + panel_name +'</li>');
            }

            // If the user does not want an electrical kit we can't fit the towbar.
            $('.panel-heading-fitting-mobile').hide();
            $('.panel-heading-fitting-garage').hide();

            // Set the coding setup to not available.
            // Values canCode = 0, dealerCoding = 0
			codingSetup(0, 0);

            calculatePrice();
        });

        // Catch enter press
        $('#postcode').on('keypress', function(e) {
            var code = e.keyCode || e.which;
            if(code == 13) { //Enter keycode
                $('#find-fitter').click();
                return false;
            }
        });

        $('#order-form').on('click', 'input[type=radio]', function(){
            $('#order-form input[type=radio]').each(function(){
                if($(this).is(':checked')) {
                    $(this).parents('label').addClass('active');
                } else {
                    $(this).parents('label').removeClass('active');
                }
            });

            // Calculate total price
            calculatePrice();
        });

        /**
         * Confirm dealer-code click
         *
         */
        $('.dealer-code').on('click', function(e) {

			// Tick the software update panel.
            $('#software-upgrade').addClass('selected');

            // Close the software-upgrade panel.
            $('#software-upgrade-tab').collapse('hide');
        });

        /**
         * submit-towbar-form
         *
         * Before submitting the form check if the selections have been made.
         */
        $('.submit-towbar-form').on('click', function(e) {

            e.preventDefault();

            if (!$('input[name="electric_option"]').is(':checked')) {

                   $('#purchase-no-kit').fadeIn('slow').focus();

                    $root.animate({

                        scrollTop: $('#electric-options').offset().top
                    }, 500);

                   return false;

            } else if (!$('input[name="fitting_option"]').is(':checked')) {

                   $('#purchase-no-fitting').fadeIn('slow').focus();

                    $root.animate({

                        scrollTop: $('#fitting-options').offset().top
                    }, 500);

                   return false;
            }

            $('#order-form').submit();

        });

        function buildModalContent() {
            $('#modal .modal-body').html(
                '<div class="fitters">'+
                    '<div class="form">'+
                        '<label>Your Postcode</label>'+
                        '<input type="text" name="postcode" id="postcode_update" value="" class="form-control">'+
                        '<button type="button" id="update" class="btn btn-primary btn-primary-double-line">Find<br>Fitters</button>'+
                    '</div> <!-- .form -->'+
                '</div>');

            $('#modal .modal-title').html( 'Book your fitting' );

            return true;
        }

        /**
         * Select fitting type
         *
         * Need to open modal on mobile and garage fitting
         */
        $('.select-fitter-option').on('click', function(e) {

            e.preventDefault();

            // Check if user has selected an electrical kit.
            var electric_option = $('input[name=electric_option]:checked').val();

            if( !electric_option ) {
                $('#electric-kit').collapse('show');

                $('#no-kit').fadeIn('slow').focus();

                $root.animate({

                    scrollTop: $('#electric-kit').offset().top
                }, 500);

            }else{

                var fitting_type = $(this).data("type");

                $( '#fld_fitterType' ).val( fitting_type ).change();

                // Hide any errors that may change now the fitting type is changing.
                $('#own-fitter-software-upgrade-error').hide();

                // If self fitted just update the values and change the styles.
                if(fitting_type == 'self_fitted') {

                    // Make sure not opted for software upgrade.
                    if($('input[name="software_upgrade"]:checked').val() == 'true') {
                        $('#own-fitter-software-upgrade-error').show();

                        return false;
                    }

                    setSelfFittedValues();

                    updatePanelsByRadioName('fitting_option');

                    $('p.type').html("Your customised price:");

                } else {

                    // Check if there is an appointment already set.
                    var appointment = $('#fld_appointmentId').val();

                    if(appointment != ''){

                        var postcode = $('#postcode').val();
                        if(postcode != '') {
                            findFitters(partNo, vehicleDesc, variantTowbarId, postcode);
                        }
                    }

                    // Open the modal
                    buildModalContent();
                    $('#modal').modal('show');
                }
            }
        });

        $('#modal').on('click', '#update', function() {

            var postcode = $('#postcode_update').val();
            if(postcode != '') {
                findFitters(partNo, vehicleDesc, variantTowbarId, postcode);
            }
        });

    });

    /**
     * calculatePrice
     * Depending on users selections update the price.
     */
    function calculatePrice()
    {
        // Check user's selected options
        // Check if user does not want fitting (if no electrical kit automatically means they will self fit)
        if( $('#fitting_option_self_fitted').is(':checked') || $('#no_electrical_kit').is(':checked') ) {

            // Add the base price to the electric_option kitOnlyPrice
            var total = $('#towbarOnlyPrice').val().replace(',', '');

            total = parseFloat(total) + parseFloat($('input[name=towball_option]:checked').attr('data-towballonlyprice').replace(',', ''));

            // Check if an electrical kit has been selected
            if(!$('#no_electrical_kit').is(':checked')) {

                total = parseFloat(total) + parseFloat($('input[name=electric_option]:checked').attr('data-kitOnlyPrice').replace(',', ''));
            }

        // Check if the user does not want an electrical kit
        } else {

            // Normal purchase
            var base = $('#order-form').attr('data-price');

            var total = parseFloat(base.replace(',', ''));

            $('input[type=radio]:checked').each(function(i){

                total = parseFloat(total) + parseFloat($(this).attr('data-price').replace(',', ''));

            });
        }

        // Display the total on the page.
        total = total.toFixed(2);

        // Re enter the comma to display to the user
        var formatted_total =  Number(total).toLocaleString('en', { minimumFractionDigits: 2});

        $('.final-price').html(formatted_total);
    }

    </script>
@stop
