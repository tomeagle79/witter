<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TowbarRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'max:255'],
            'surname' => ['required', 'max:255'],
            'company_name' => ['max:255'],
            'postcode' => ['required', 'max:255'],
            'address1' => ['required', 'max:255'],
            'address2' => ['max:255'],
            'address3' => ['max:255'],
            'towncity' => ['required', 'max:255'],
            'telephone' => ['required', 'max:255'],
            'fax' => ['max:255'],
            'email' => ['required', 'email', 'max:255'],
            'date_fitted_dd' => ['required', 'max:255'],
            'date_fitted_mm' => ['required', 'max:255'],
            'date_fitted_yyyy' => ['required', 'max:255'],
            'supplier_name' => ['required', 'max:255'],
            'supplier_postcode' => ['required', 'max:255'],
            'supplier_address1' => ['required', 'max:255'],
            'supplier_address2' => ['max:255'],
            'supplier_address3' => ['max:255'],
            'supplier_towncity' => ['required', 'max:255'],
            'supplier_telephone' => ['max:255'],
            'vehicle_reg' => ['required', 'max:255'],
            'towbar_serial_no' => ['required', 'max:255'],
            'towbar_part_no' => ['required', 'max:255'],
            'neck_serial_no' => ['max:255'],
        ];
    }
}
