@extends('layouts.frontend')

@section('title', 'Find Your Vehicle')

@section('heading')
	Find Your Vehicle
@stop

@section('meta_description')
	Witter Towbars Find Your Vehicle
@stop

@section('content')
<div class="container vehicle-select">
	<div class="row">
		<div class="col-md-12">

            <div class="alert alert-success" role="alert">

                <p>
                    We have found a vehicle matching your registration number.
                </p>
                <p>
                    Please confirm this is your vehicle.
                </p>

            </div>

            @if( !empty($reg->photoUrl) )
                <img src="{{ $reg->photoUrl }}" alt="" class="model-img">
            @endif

            <p>
                @foreach($reg->vehicleDetails as $detail)
                    {{ $detail->prefix }} <strong>{{ $detail->text }}</strong><br>
                @endforeach
            </p>

            <p>
                <a href="{{ route('towbars.vehicle_towbars_by_reg', [$reg->vehicleId, $reg->registrationDate]) }}" class="btn btn-primary">This is my car</a>
            </p>

		</div>
	</div>
</div>
@stop
