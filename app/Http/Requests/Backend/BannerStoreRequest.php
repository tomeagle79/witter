<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class BannerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'link' => 'required|max:255',
            'button_text' => 'max:255',
            'vehicle_of_week' => 'max:1',
            'manufacturer_of_week' => 'max:1',
            'active_start_at' => 'date',
            'active_end_at' => 'date',
        ];
    }
}
