<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutSection2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $cart = build_cart();

        // Set the fitter delivery as false to start with
        $fitter_delivery = false;

        // Check if we have any fitted accessories.
        if (isset($cart['contents'])) {

            foreach ($cart['contents'] as $item) {

                if ($item['type'] == 'accessory-fitted' && isset($item['appointmentId'])) {

                    $fitter_delivery = true;
                }
            }
        }

        // If we haev a towbar check if it will be fitted.
        if (isset($cart['towbar'])) {
            if (isset($cart['towbar']['appointmentId'])) {

                $fitter_delivery = true;
            }
        }

        // Check if the products will be fitted. If so we deliver to the fitter.
        if ($fitter_delivery) {

            return [];
        } else {

            // No fitting means we send the products to teh customer.
            return [
                'delivery_telephone' => ['required', 'max:255'],
                'delivery_postcode' => ['required', 'max:10'],
                'delivery_address1' => ['required', 'max:255'],
                'delivery_address2' => ['max:255'],
                'delivery_towncity' => ['required', 'max:255'],
                'delivery_county' => ['required', 'max:255'],
                'delivery_country' => ['required', 'max:255'],
            ];
        }
    }
}
