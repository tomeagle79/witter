<?php

namespace App\Providers;

use App\Contracts\TenantLocaleContext;
use Illuminate\Support\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(TenantLocaleContext $local)
    {
        // Get the website
        $website = $local->getWebsite();

        // Check if a theme has been set up for this website
        if (!empty($website->theme)) {

            // Load the website theme to the views.
            $views = [
                resource_path("views/themes/" . $website->theme),
                resource_path("views"),
            ];

            $this->loadViewsFrom($views, 'theme');

        } else {

            // If no theme / defualt teme we still need to set up the 'theme' namespace.
            $view = resource_path("views");

            $this->loadViewsFrom($view, 'theme');
        }
    }
}
