<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;
use Log;

class VehicleRegistrationLookup extends ApiModel
{
	const URI = "vehicles/registration";

	/**
	 * Get all registrations based on reg
	 *
	 * @param Int
	 * @return Object
	 */
	static public function get_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'?' . http_build_query($args);
		$obj = self::get_request($url);

		Log::info("The search url = " . $url);

		Log::info("The search url = " . json_encode($obj));

		return $obj;
	}
}
