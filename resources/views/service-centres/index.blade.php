@extends('layouts.frontend')

@section('title', 'Towbar Fitting Service Centres | Professional Fitters')

@section('heading')
	Service centres
@stop

@section('meta_description')
	Looking for towbar fitting near you? Find a towbar from Witter Towbars and book your fitting at a WebFit service centre.
@stop

@section('content')

<div class="container service-centres">

	<div class="row">

		<div class="col-sm-7 centres-form">

			@if(empty($postcode))
				<h2>Search by postcode</h2>
			@else
				<h2>Search results for '{{ strtoupper($postcode) }}'</h2>
			@endif

			@include('service-centres.partials.postcode-form')

			@if(empty($nearest_centre) && !empty($postcode))

				<p>oops, we could not find this postcode. Please make sure you've entered your postcode in the correct format or try our regional map instead?</p>

			@elseif(empty($nearest_centre))

				<p>With over 450 service centres across the UK, including 'Witter Approved' service centres, you can be sure that you will receive the highest level of service no matter where you are located.</p>
			@endif

			@if(!empty($approved_centers_list))

				<div class="card-clear">

					<div class="card-clear-header">
						<div class="row">
							<div class="col-sm-4 text-center">

								<h2 class="approved-title">APPROVED SERVICE CENTRES</h2>

							</div><!-- /.col-sm-4 -->
							<div class="col-sm-8">
								With our Approved Service Centre scheme you will benefit from high-quality products that are fitted to the highest possible standard. The Service Centres below comply with our quality standards and requirements when it comes to installing our products. Find your nearest centre today and drive away with peace of mind.
							</div><!-- /.col-sm-8 -->
						</div><!-- /.row -->

					</div><!-- /.card-clear-header -->


						@foreach($approved_centers_list->chunk(2) as $centres_chunk)

							<div class="card-clear-grid">
								<div class="row">
							        @foreach ($centres_chunk as $regional_centre)
							        	<div class="col-xs-12 col-md-6">
							        		@include('service-centres.partials.regional-centre-card-basic', ['centre' => $regional_centre])
							        	</div><!-- /.col-sm-6 -->

							        @endforeach
								</div><!-- /.row -->
							</div><!-- /.card-clear-grid -->

						@endforeach

				</div><!-- /.card-clear -->
			@endif

			@if(!empty($nearest_centre))

				<h2>Your nearest service centre</h2>

				<div class="nearest-centre">

					<div class="row">

						<div class="col-sm-8">
							<h4 class="title"><a href="{{ route('service-centres.show', $nearest_centre->centreSlug) }}">{{ $nearest_centre->centreName }}</a></h4>
						</div><!-- /.col-sm-8 -->

						<div class="col-sm-4">
							<a href="{{ route('service-centres.show', $nearest_centre->centreSlug) }}" class="btn btn-primary btn-primary-small pull-right">Full Details</a>
						</div><!-- /.col-sm-4 -->

					</div><!-- /.row -->

					<hr>

					@if($nearest_centre->isApprovedCentre)
						<div class="centre-approved-banner">
							<h2>WITTER APPROVED SERVICE CENTRE</h2>
						</div>
					@endif

					<hr>

					@if($nearest_centre->isApprovedCentre)
						<div class="center approved-centre">
					@else
						<div class="centre">
					@endif

						<div class="row">

							<div class="col-sm-6">
								<p>
								@if(!empty($nearest_centre->address1)){{ $nearest_centre->address1 }}<br>@endif
								@if(!empty($nearest_centre->address2)){{ $nearest_centre->address2 }}<br>@endif
								@if(!empty($nearest_centre->address3)){{ $nearest_centre->address3 }}<br>@endif
								@if(!empty($nearest_centre->city)){{ $nearest_centre->city }}<br>@endif
								@if(!empty($nearest_centre->county)){{ $nearest_centre->county }}<br>@endif
								@if(!empty($nearest_centre->postcode)){{ $nearest_centre->postcode }}@endif
								</p>
							</div><!-- /.col-sm-6 -->

							<div class="col-sm-6">

								@foreach($nearest_centre->openingHours as $day => $hours)

									<div class="opening-hours @if(strtolower($day) == strtolower(date('D'))) today @endif "><span class="day">{{ date('l', strtotime($day)) }}</span> {{ $hours }}</div>

								@endforeach
							</div><!-- /.col-sm-6 -->
						</div><!-- /.row -->

						<hr>

						<div id="map" class="contact-map"></div>

					</div><!-- /.item -->

				</div><!-- /.other-centres -->

			@endif


		</div><!-- /.col-sm-7 -->

		<div class="col-sm-5">

			@if(empty($nearest_centre) && empty($approved_centers) && empty($centres))

				<h2>Search by region</h2>

				@include('service-centres.partials.map-card-clear')

			@endif

			@if(!empty($approved_centers))

				<div class="other-centres">

					<h2 class="other-centres-title">Your nearest 'Approved Service Centre'</h2>

					@foreach($approved_centers as $centre)


						@include('service-centres.partials.centre-card-approved', ['centre' => $centre])

					@endforeach

				</div><!-- /.other-centres -->

			@endif

			@if(!empty($centres))

				<div class="other-centres">

					<h2>Other nearby centres</h2>

					@foreach($centres as $centre)


						@include('service-centres.partials.centre-card-basic', ['centre' => $centre])

					@endforeach

				</div><!-- /.other-centres -->

			@endif

		</div><!-- /.col-sm-5 -->

	</div><!-- /.row -->

</div><!-- /.container -->


@stop


@section('scripts')


	@if(!empty($nearest_centre->postcode) && !empty($nearest_centre->address1))


	    <script type="text/javascript">
			function initMap() {
				var witter = {lat: {{ $geocode['lat'] }}, lng: {{ $geocode['lng'] }}};
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 12,
					center: witter
				});

				var marker = new google.maps.Marker({
				  	position: witter,
				  	map: map,
				 	title: ''
				});
			}

	    </script>

	    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-VmDhNlxlKQoxC7F7YXoQvmS2APd6Qw0&callback=initMap">
	    </script>

	@endif


@stop


