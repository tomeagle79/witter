<div class="container-fluid checkout-container">
    <div class="checkout-header-gradient"></div><!-- /.checkout-header-gradient -->
    <div class="container  checkout-container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="checkout-steps-wrapper">
                        <ul class="checkout-steps-2 clearfix">

                            <!-- STEP 1 ////////////// -->
                            <li class="step @if($checkout_step == 1) step-active @endif @if($checkout_step > 1) step-completed @endif">

                                @if($checkout_step > 1 && $checkout_step != 4)
                                <a href="{{ route('checkout') }}" title="Return to Your Details">
                                @endif
                                    <div class="step-icon">
                                        <i class="icon-person"></i><!-- /.icon-person -->
                                    </div><!-- /.step-icon -->

                                    <div class="step-text">
                                        <h3 class="step-text-title">
                                            <span>Step</span> 1
                                        </h3><!-- /.step-text-title -->
                                        <p class="step-copy">
                                            <span class="hidden-sm hidden-xs">Your&nbsp;</span><!-- /.hidden-sm .hidden-xs -->Details
                                        </p><!-- /.step-copy -->
                                    </div><!-- /.step-text -->
                                @if($checkout_step > 1 && $checkout_step != 4)
                                </a>
                                @endif
                            </li><!-- /.step -->

                            <!-- STEP 2 ////////////// -->
                            <li class="step @if($checkout_step == 2) step-active @endif @if($checkout_step > 2) step-completed @endif">
                                    @if($checkout_step > 2 && $checkout_step != 4)
                                    <a href="{{ route('checkout.section2') }}" title="Return to Delivery Details">
                                    @endif
                                        <div class="step-icon">
                                            <i class="icon-van"></i><!-- /.icon-person -->
                                        </div><!-- /.step-icon -->
                                        <div class="step-text">
                                            <h3 class="step-text-title">
                                                <span>Step</span> 2
                                            </h3><!-- /.step-text-title -->
                                            <p class="step-copy">
                                                @if(!empty($fitting_type) && $fitting_type == 'mobile')
                                                    Fitting<span class="hidden-sm hidden-xs">&nbsp;Address</span>
                                                @else
                                                    Delivery<span class="hidden-sm hidden-xs">&nbsp;Details</span>
                                                @endif
                                            </p><!-- /.step-copy -->
                                        </div><!-- /.step-text -->
                                    @if($checkout_step > 1 && $checkout_step != 4)
                                    </a>
                                    @endif
                            </li><!-- /.step -->

                            <!-- STEP 3 ////////////// -->
                            <li class="step  @if($checkout_step == 3) step-active @endif @if($checkout_step > 3) step-completed @endif">
                                    <div class="step-icon">
                                        <i class="icon-credit-card"></i><!-- /.icon-credit-card -->
                                    </div><!-- /.step-icon -->
                                    <div class="step-text">
                                        <h3 class="step-text-title">
                                            <span>Step</span> 3
                                        </h3><!-- /.step-text-title -->
                                        <p class="step-copy">
                                            <span class="hidden-sm hidden-xs">Summary&nbsp;&AMP;&nbsp;</span>Payment
                                        </p><!-- /.step-copy -->
                                    </div><!-- /.step-text -->
                            </li><!-- /.step -->

                            <!-- STEP 4 ////////////// -->
                            <li class="step @if($checkout_step == 4) step-completed-last @endif ">
                                    <div class="step-icon">
                                        <i class="icon-circled-tick"></i><!-- /.icon-circled-tick -->
                                    </div><!-- /.step-icon -->
                                    <div class="step-text">
                                        <h3 class="step-text-title">
                                            <span>Step</span> 4
                                        </h3><!-- /.step-text-title -->
                                        <p class="step-copy">
                                            <span class="hidden-sm hidden-xs">Order&nbsp;</span>Complete
                                        </p><!-- /.step-copy -->
                                    </div><!-- /.step-text -->
                            </li><!-- /.step -->

                        </ul><!-- /.checkout-steps -->

                    </div><!-- /.checkout-steps-wrapper -->
                </div><!-- /.col-xs-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.container-fluid checkout-container-->