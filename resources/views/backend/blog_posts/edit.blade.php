@extends('layouts.backend')

@section('title')
	Edit Blog Post
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/blog_posts/partials/_form', ['submit_text' => 'Edit Blog Post', 'type' => 'edit'])
	</form>

@stop
