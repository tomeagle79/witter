@extends('layouts.frontend')

@section('title', 'Light Commercial Vehicles - Roof Bars')

@section('heading')
	Light Commercial Vehicles - Roof Bars
@stop

@section('meta_description')
	Witter Light Commercial Vehicles - Roof Bars
@stop

@section('content')


<div class="container">

	<div class="row">
		<div class="col-md-12">
			<div class="towbars-by-manufacturer">
				<h2 class="towbars-by-manufacturer-title">Select your <span class="slim">Manufacturer</span></h2>

				<p>Designed and manufactured in the UK, our extensive range of Roof Accessories offer the chance to add versatility and value to your vehicle and your business.</p>

				<p>With Steel and Aluminium bars available you can choose the right ones for you.</p>
                <?php
                // Count
                $count = count($manufacturers);
                ?>
				@foreach ($manufacturers as $key => $manufacturer)

					<?php $key++ ?>

					@if($key%6 === 1)
						<div class="row">
					@endif

					<div class="col-md-2 col-xs-6">
						<div class="manufacturer-logo">
							<a href="{{ route('roof-systems.accessories.models', $manufacturer->slug) }}">
								<img src="{{ image_load($manufacturer->logoUrl) }}" alt="Towbars for {{ $manufacturer->manufacturerName }}" >
							</a>
						</div>
					</div>

					@if($key%6 === 0 && $key > 0 || $key == $count)
						</div><!-- .row -->
					@endif
				@endforeach
			</div>

		</div>
	</div>
</div>
@stop
