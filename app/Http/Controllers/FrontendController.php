<?php 
namespace App\Http\Controllers;

use Session;
use App\Models\TwitterCache;
use Twitter;
use App\Models\Api\PartClass;
use App\Models\Api\VehicleManufacturer;

class FrontendController extends Controller {

	/**
	 * Any code that should be run on every front end page
	 */
	public function __construct()
	{	
		// Set breadcrumb
    	$this->data['breadcrumbs'] = new \Creitive\Breadcrumbs\Breadcrumbs;
    	$this->data['breadcrumbs']->setDivider('');
    	$this->data['breadcrumbs']->setCssClasses('breadcrumb'); 
    	$this->data['breadcrumbs']->addCrumb('Home', '/');
        $this->data['partClasses'] = PartClass::get_all();
		$this->data['manufacturers_dropdown'] = VehicleManufacturer::get_all();
	}
}
