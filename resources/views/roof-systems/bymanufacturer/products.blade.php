@extends('layouts.frontend')

@section('title', 'Light Commercial Vehicles - Roof Bars - Models')

@section('heading')
	Light Commercial Vehicles - Roof Bars - Models
@stop

@section('meta_description')
	Witter Light Commercial Vehicles - Roof Bars - Models
@stop

@section('content')


<div class="container towbars-product-listing ">
	<div class="row">

		<div class="col-md-12">

            <p class="pre-title">We have the following roof bars to fit your</p>

            <h2 class="nomargin">{{ $manufacturer->manufacturerName }} - {{ $model->modelName }}</h2>

            <div class="button-area">
                <a href="{{ route('roof-systems.roof-bars') }}" class="btn btn-primary">Not your Vehicle?</a>
            </div>
        </div> <!-- .col-md-6 -->

    </div> <!-- .row -->

    <div class="row">
        <div class="col-md-12">

           @if($has_products)
                <div class="product-listing">

                @php $iteration = 1; @endphp

                @if(!empty($roofbars))
                    @foreach($roofbars as $roofbar)

                        @include('partials.analytics.json_product_details', [
                            'product' => $roofbar,
                            'list_name' => 'Category Results',
                            'category' => 'Roof Systems/By Manufacturer',
                            'position' => $iteration
                        ])
                        @php $iteration++; @endphp

                        <div class="product-listing-item">

                            <div class="row">
                                <div class="col-md-3">
                                    <a href="{{ route('roof-systems.roof-bars.view', encode_url($roofbar->partNo)) }}"><img src="{{ image_load($roofbar->imageUrl) }}" alt=""></a>
                                </div> <!-- .col-md-3 -->
                                <div class="col-md-6">
                                    <a href="{{ route('roof-systems.roof-bars.view', encode_url($roofbar->partNo)) }}"><h3>{{ $roofbar->title }}</h3></a>
                                    <p>{{ $roofbar->details }}</p>
                                    <div class="price">
                                        Price From
                                        <span>&pound;{{ price($roofbar->price) }} Inc. VAT</span>
                                    </div> <!-- .price -->
                                </div> <!-- .col-md-6 -->
                                <div class="col-md-3">
                                    <p>
                                        <a href="{{ route('roof-systems.roof-bars.view', encode_url($roofbar->partNo)) }}" class="btn btn-secondary btn-block">More<br>Details</a>
                                    </p>
                                </div> <!-- .col-md-6 -->
                            </div> <!-- .row -->
                        </div> <!-- .towbar-listing -->
                    @endforeach
                @endif

                @if(!empty($roofdecks))
                    @foreach($roofdecks as $roofdeck)

                        @include('partials.analytics.json_product_details', [
                            'product' => $roofdeck,
                            'list_name' => 'Category Results',
                            'category' => 'Roof Systems/By Manufacturer',
                            'position' => $iteration
                        ])
                        @php $iteration++; @endphp

                        <div class="product-listing-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="{{ route('roof-systems.roof-decks.view', encode_url($roofdeck->partNo)) }}"><img src="{{ image_load($roofdeck->imageUrl) }}" alt=""></a>
                                </div> <!-- .col-md-3 -->
                                <div class="col-md-6">
                                    <a href="{{ route('roof-systems.roof-decks.view', encode_url($roofdeck->partNo)) }}"><h3>{{ $roofdeck->title }}</h3></a>
                                    <p>{{ $roofdeck->details }}</p>
                                    <div class="price">
                                        Price From
                                        <span>&pound;{{ price($roofdeck->price) }} Inc. VAT</span>
                                    </div> <!-- .price -->
                                </div> <!-- .col-md-6 -->
                                <div class="col-md-3">
                                    <p>
                                        <a href="{{ route('roof-systems.roof-decks.view', encode_url($roofdeck->partNo)) }}" class="btn btn-secondary btn-block">More<br>Details</a>
                                    </p>
                                </div> <!-- .col-md-6 -->
                            </div> <!-- .row -->
                        </div> <!-- .towbar-listing -->
                    @endforeach
                @endif

                @if(!empty($accessories))
                    @foreach($accessories as $accessory)

                        @include('partials.analytics.json_product_details', [
                            'product' => $accessory,
                            'list_name' => 'Category Results',
                            'category' => 'Roof Systems/By Manufacturer',
                            'position' => $iteration
                        ])
                        @php $iteration++; @endphp
                        
                        <div class="product-listing-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="{{ route('roof-systems.accessories.view', encode_url($accessory->partNo)) }}"><img src="{{ image_load($accessory->imageUrl) }}" alt=""></a>
                                </div> <!-- .col-md-3 -->
                                <div class="col-md-6">
                                    <a href="{{ route('roof-systems.accessories.view', encode_url($accessory->partNo)) }}"><h3>{{ $accessory->title }}</h3></a>
                                    <p>{{ $accessory->details }}</p>
                                    <div class="price">
                                        Price From
                                        <span>&pound;{{ price($accessory->price) }} Inc. VAT</span>
                                    </div> <!-- .price -->
                                </div> <!-- .col-md-6 -->
                                <div class="col-md-3">
                                    <p>
                                        <a href="{{ route('roof-systems.accessories.view', encode_url($accessory->partNo)) }}" class="btn btn-secondary btn-block">More<br>Details</a>
                                    </p>
                                </div> <!-- .col-md-6 -->
                            </div> <!-- .row -->
                        </div> <!-- .towbar-listing -->
                    @endforeach
                @endif

                </div> <!-- .item-list -->

            @else
                <p>Sorry, we don't have any roof bars that are compatible with your vehicle.</p>
            @endif

        </div> <!-- .col-md-12 -->
    </div> <!-- .row -->
</div> <!-- .container -->
@stop

@section('scripts')
	<script>
        @include('partials.analytics.list', [
            'products' => array_merge($accessories, $roofdecks, $roofbars),
            'page' => 1,
            'per_page' => 999,
            'list_name' => 'Category Results',
            'category' => 'Roof Systems/By Manufacturer'
        ])
	</script>
@stop