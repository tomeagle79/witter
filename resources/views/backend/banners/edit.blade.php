@extends('layouts.backend')

@section('title')
	Edit Banner
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/banners/partials/_form', ['submit_text' => 'Edit Banner', 'type' => 'edit'])
	</form>

@stop
