@extends('layouts.frontend')

@section('title', 'Towbar Electrics | Tow Bar Wiring Kits')

@section('heading')
	Towbar Electric Kits
@stop

@section('meta_description')
	Towbar electrics by Witter Towbars. We supply a full range of towbar wiring kits designed to complement our towbars. Buy online today.
@stop

@section('content')

<div class="container">
	<div class="row">

		<div class="col-md-9 col-md-push-3">
			<div class="row">
				<div class="col-md-8">
					@include('partials.search-dropdowns', ['redirect_slug' => 'electrical-kits', 'form_button_text' => 'Find your kit'])
				</div>
				<div class="col-md-4">
					@include('partials.sidebar-findlink-primary')
				</div>
			</div>
			<div class="towbars-listing">
				<h2 class="towbars-by-manufacturer-title">Find by <span class="slim">Manufacturer</span></h2>

				<p>Witter Towbars supply a full range of high quality pre-boxed dedicated electrical kits that have been designed to complement their range of towbars. All of our dedicated electric range is manufactured to the highest standards and designed around your vehicle to ensure good life and performance in the most arduous of towing conditions.</p>
                <?php
                // Count
                $count = count($manufacturers);
                ?>
				@foreach ($manufacturers as $key => $manufacturer)

					<?php $key++ ?>

					@if($key%12 === 1)
						<div class="row lazy-load-images item-list flex-items">
					@endif

					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">


						<div class="item">
							<div class="item-body">

								<div class="manufacturer-logo">
									<a href="{{ route('electrical-kits.model_list', $manufacturer->slug) }}">
										<img src="" data-src="{{ image_load($manufacturer->logoUrl) }}" alt="Towbars Electric Kits for {{ $manufacturer->manufacturerName }}" >

		                            	<h3 class="title-small">{{ $manufacturer->manufacturerName }} Towbar Electric Kits</h3>
									</a>
								</div>
							</div><!-- /.item-body -->
						</div><!-- /.item -->


					</div>

					@if($key%12 === 0 && $key > 0 || $key == $count)
						</div><!-- .row -->
					@endif
				@endforeach
			</div>

		</div> <!-- .col-md-9 -->

		<div class="col-md-3 col-md-pull-9">

			@include('partials.electrics.sidebar-search')

			@include('partials.sidebar-info-menu')

		</div> <!-- .col-md-3 -->
	</div>
</div>
@stop
