<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BlogCategoryStoreRequest;
use App\Models\BlogCategory;
use Auth;
use Datatables;
use Hash;
use View;
use DB;
use URL;


class BlogCategoryController extends Controller
{
    /**
     * Instantiate a new instance.
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.blog_categories.index');
    }

    /**
     * Return the datatable for blog_categories
     *
     * @return Datatables
     */
    public function indexDatatable()
    {
        $blog_categories = BlogCategory::select(['id', 'title', 'url_slug', 'created_at', 'updated_at'])->get();

        return Datatables::of($blog_categories)
            ->addColumn('edit', '<a href="{{ URL::backend(\'blog_categories/edit/\'. $id) }}" class="btn green">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'blog_categories/delete/\'. $id) }}" class="btn red" data-confirm="Are you sure you want to delete this blog category?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.blog_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BlogCategoryStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogCategoryStoreRequest $request)
    {
        // save the blog_category
        $blog_category = new BlogCategory();
        $blog_category->title = $request->title;
        $blog_category->url_slug = str_slug($request->url_slug,'-');
        $blog_category->save();

        return redirect(URL::backend('blog_categories'))->with('success', '<strong>Success!</strong> Blog category created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['blog_category'] = BlogCategory::find($id);

        return view('backend.blog_categories.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BlogCategoryStoreRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogCategoryStoreRequest $request, $id)
    {
        // save the blog_category
        $blog_category = BlogCategory::find($id);
        $blog_category->title = $request->title;
        $blog_category->url_slug = str_slug($request->url_slug,'-');
        $blog_category->save();

        return redirect(URL::backend('blog_categories'))->with('success', '<strong>Success!</strong> Blog category updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog_category = BlogCategory::find($id);
        $blog_category->delete();

        return redirect(URL::backend('blog_categories'))->with('message', '<strong>Success!</strong> Blog category deleted.');
    }

    /**
     * Restore the specified resource
     */
    public function restore($id)
    {
        $blog_category = BlogCategory::withTrashed()->find($id);
        $blog_category->restore();

        return redirect(URL::backend('blog_categories'))->with('success', '<div class="alert alert-success"><strong>Success!</strong> Blog category restored.</div>');
    }
}
