@extends('layouts.frontend')

@section('title', 'Light Commercial Vehicles - Roof Racks - Models')

@section('heading')
	Light Commercial Vehicles - Roof Racks - Models
@stop

@section('meta_description')
	{{ $manufacturer->metaDescription }}
@stop

@section('content')


<div class="container towbars-listing">

	<div class="row">
		<div class="col-md-12">
			<h2>{{ $manufacturer->manufacturerName }}</h2>

			@if( !empty($manufacturer->logoUrl) )
                <div class="top-img-wrapper">
					<img src="{{ $manufacturer->logoUrl }}" alt="{{ $manufacturer->manufacturerName }}" class="top-img">
                    <p class="text-center">Images are representative only</p>
                </div> <!-- .wrapper -->
			@endif

			<div class="description">
				<p>{!! nl2br($manufacturer->manufacturerDescription) !!}</p>
			</div>

			<div class="item-list">
                <?php
                // Count
                $count = count($models);
                ?>
				@foreach ($models as $key => $model)

					<?php $key++ ?>

					@if($key%4 === 1)
						<div class="row">
					@endif

					<div class="col-md-3">
                        <div class="item">
                            <div class="manufacturer-logo">
                                <a href="{{ route('roof-systems.roof-decks.models.vehicles', [$manufacturer_slug, $model->slug]) }}">
                                    <img src="{{ image_load($model->photoUrl) }}" alt="Roof Racks for {{ $model->modelName }}" >
                                </a>
                                <a href="{{ route('roof-systems.roof-decks.models.vehicles', [$manufacturer_slug, $model->slug]) }}">
                                    <h3>{{ $model->modelName }}</h3>
                                </a>
                            </div>
                        </div> <!-- .item -->
					</div>

					@if($key%4 === 0 && $key > 0 || $key == $count)
						</div><!-- .row -->
					@endif
				@endforeach

			</div> <!-- .item-list-->

		</div>
	</div>
</div>
@stop
