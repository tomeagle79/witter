<?php

namespace App\Console\Commands;

use App\Models\Banner;
use Illuminate\Console\Command;

class UpdateBanners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'banners:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the banners based on the active start and end dates.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Activate banners
        $this->activate_banners();

        // Deactivate banners
        $this->deactivate_banners();
    }

    /**
     * Activate banners no longer in their date period.
     *
     * @return bool
     */
    public function activate_banners()
    {
        $this->info('//////////////////////////');
        $this->info('Activate banners based on their dates.');
        $this->info('//////////////////////////');

        $now = date('Y-m-d H:i:s');

        $banners = Banner::where('active', '=', 0)
            ->where('active_start_at', '<', $now)
            ->where('active_end_at', '>', $now)
            ->get();

        if (!empty($banners)) {
            foreach ($banners as $banner) {

                $banner->active = 1;
                $banner->save();
            }
        }

        $this->info('A total of ' . $banners->count() . ' banners have been made active.');
        $this->info('//////////////////////////');

        return true;
    }

    /**
     * Deactivate banners no longer in their date period.
     *
     * @return bool
     */
    public function deactivate_banners()
    {
        $this->info('--------------------------');
        $this->info('//////////////////////////');
        $this->info('Deactivate banners based on their dates.');
        $this->info('//////////////////////////');

        $now = date('Y-m-d H:i:s');

        $banners = Banner::where('active', '=', 1)
            ->where('active_end_at', '<', $now)
            ->get();

        if (!empty($banners)) {
            foreach ($banners as $banner) {

                $banner->active = 0;
                $banner->save();
            }
        }

        $this->info('A total of ' . $banners->count() . ' banners have been deactivated.');
        $this->info('//////////////////////////');

        return true;
    }
}
