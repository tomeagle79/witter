@extends('layouts.backend')

@section('title')
	Create Help &amp; Advice Category 
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/help_categories/partials/_form', ['submit_text' => 'Create Help Category', 'type' => 'add'])
	</form>

@stop

