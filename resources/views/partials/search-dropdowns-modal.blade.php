<div class="vehicle-search">

    {!! Form::open(['id' => 'search-dropdown-modal', 'class' => 'vehicle-select', 'onsubmit' => "
        gtag('event', 'Success', {
            event_category: 'Model select',
            event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
        });
    "]) !!}

    <div class="row">
        <div class="col-xs-12 vehicle-search-message">
            <p>Please select your vehicle</p>
        </div>
    </div>

    <div class="row bg">
        <div class="col-xs-12 col-sm-5 modal-body-dropdown">
            <div class="form-group ">
                <label for="body" class="sr-only">Choose Body Type</label>
                <div class="car-dropdowns-select">
                    <select name="body" class="body" data-style="btn-select-box">
                        <option>Choose body</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4 modal-registration-dropdown">
            <div class="form-group">
                <label for="body" class="sr-only">Choose year</label>
                <div class="car-dropdowns-select">
                    <select name="registration" class="registration" data-style="btn-select-box">
                        <option>Choose year</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-3">
            <button type="submit" class="btn btn-primary">Find</button>
        </div><!-- /.col-sm-6 -->

    </div>

        {{ Form::hidden('manufacturerSlug') }}
        {{ Form::hidden('modelSlug') }}
        {{ Form::hidden('bodySlug') }}
        {{ Form::hidden('registrationId') }}
        {{ Form::hidden('topTenId') }}

    <div class="row">

        <div class="col-xs-12">
            <div id="dropdownError"></div><!-- /#dropdownError -->
        </div><!-- /.col-xs-12 -->

    </div><!-- /.row -->

    {!! Form::close() !!}
    <div class="car-dropdowns-loading"></div>

    <p class="splitter">Or</p>

    {!! Form::open(['route' => 'search.numberplate', 'class' => 'form vehicle-plate-search', 'method' => 'get', 'onsubmit' => "
        gtag('event', 'Success', {
            event_category: 'Reg lookup',
            event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
        });
    "]) !!}
    <div class="row bg">

        <div class="col-xs-12 col-sm-5">

            <p>Search by number plate</p>
        </div>

        <div class="col-xs-12 col-sm-7">

            {!! Form::text('search', null, ['required', 'class' => 'form-control', 'placeholder' => 'Registration Number']) !!}

            <button type="submit" class="btn"><span>Search</span></button>
        </div>
    </div>
    {!! Form::close() !!}

</div>

@section('scripts')
    @parent
    <script type="text/javascript">

        /*
         * Set up the dynamic vehicle dropdowns form so when submitted call the correct functions
         * With Google event tracking
         */
        function setUpVehicleRedirectFormWithTracking(slug, form_id, validate){

            if(typeof validate === 'undefined'){
                validate = false;
            }

            $( form_id ).on('submit', function(e){
                e.preventDefault();

                // Get the dropdown values.
                var manufacturer    = $( form_id + " input[name='manufacturerSlug']" ).val();
                var model           = $( form_id + " input[name='modelSlug']" ).val();
                var body            = $( form_id + " input[name='bodySlug']" ).val();
                var registration    = $( form_id + " input[name='registrationId']" ).val();
                var top_ten_no      = $( form_id + " input[name='topTenId']" ).val();

                // Only validate the last page before the car types
                if(validate){
                   if(!dropdownValidate(body, registration)){
                        return false;
                    }
                }

                // Build a url from the slug values and redirect the user
                var url = dropdownRedirectsURL(slug, manufacturer, model, body, registration);

                var redirect = "{{ url("") }}" + url;

                gtag('event', 'Select vehicle', {
                    event_category: 'Top Ten',
                    event_label: redirect,
                    value: top_ten_no
                });

                window.location = url;
            });
        }

        $(function() {

            var vehicle_url_call = "{{ URL('vehicles') }}";

            var form_id = '#search-dropdown-modal';

            setUpVehicleDropdownsForm(vehicle_url_call, form_id);

            var validate = true;

            // Send the starting slug to build the url and redirect the user.
            setUpVehicleRedirectFormWithTracking("/towbars/", form_id, validate);

            $( '.vehicle-plate-search' ).on('submit', function(){

                var manufacturer    = $( form_id + " input[name='manufacturerSlug']" ).val();
                var model           = $( form_id + " input[name='modelSlug']" ).val();
                var top_ten_no      = $( form_id + " input[name='topTenId']" ).val();

                gtag('event', 'Registration search', {
                    event_category: 'Top Ten',
                    event_label: top_ten_no + " " + manufacturer + " " + model,
                    value: top_ten_no
                });
            });
        });

    </script>
@stop
