<div class="col-xs-12 col-sm-3 col-md-3 col-sm-pull-9 col-md-pull-9">
        <div class="sidebar">
            <div class="sidebar-heading">Towbar Overview</div>
            <ul>
                <a class="smoothscroll" href="#types" data-open-panel="collapseOne"><li>The Different Types of Towbars</li></a>
                <a class="smoothscroll" href="#advantages" data-open-panel="collapseOne"><li>The Advantages and Disadvantages of Using Each Towbar Type</li></a>
            </ul>
    
            <div class="sidebar-heading">Bike Rack FAQ</div>
            <ul>
                <a class="smoothscroll" href="#best-rack" data-open-panel="collapseTwo"><li>Which is the Best Bike Rack?</li></a>
                <a class="smoothscroll" href="#safe-rack" data-open-panel="collapseThree"><li>Are Bike Racks Safe?</li></a>
                <a class="smoothscroll" href="#which-rack" data-open-panel="collapseFour"><li>Which Bike Racks Will Fit my Vehicle?</li></a>
                
                <a class="smoothscroll" href="#can-i-fit" data-open-panel="collapseFive"><li>Can I Fit a Bike Rack Myself?</li></a>
                <a class="smoothscroll" href="#what-else" data-open-panel="collapseSix"><li>What Else Should I Consider Before Buying a Bike Rack?</li></a>
                <a class="smoothscroll" href="#insurance" data-open-panel="collapseSeven"><li>Can Fitting a Bike Rack Affect my Insurance?</li></a>
            </ul>
        </div>
    
        <a href="#top" class="btn btn-primary back-to-top hidden-xs smoothscroll" >Back to top</a>
    
    </div>
    