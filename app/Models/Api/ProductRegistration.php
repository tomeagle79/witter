<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class ProductRegistration extends ApiModel
{
	const URI = "registrations";

	/**
	 * Register a towbar
	 *
	 * @params Array
	 * @return Object
	 */
	static public function towbar($data)
    {
        $url = config('witter.api_base') . self::URI . '/registertowbar';

        $obj = self::post_request($url, $data);

		return $obj;
	}

    /**
     * Register a cycle carrier
     *
     * @params Array
     * @return Object
     */
    static public function cycle_carrier($data)
    {
        $url = config('witter.api_base') . self::URI . '/registercyclecarrier';

        $obj = self::post_request($url, $data);

        return $obj;
    }

}
