@extends('layouts.frontend')

@section('title', 'How to Choose a Towbar for Your Car')

@section('meta_description')
    Need help choosing the right towbar for your vehicle? Read our easy to follow towbar guide. All you need to know about buying and fitting a towbar.
@stop

@section('body-class', 'complete-guide')

@section('heading')
    HOW TO CHOOSE A TOWBAR FOR YOUR CAR
@stop

@section('content')
    <a name="#top" id="top"></a>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <p>If you’re new to the world of towing then <a href="/towbars">choosing the right towbar</a> for your vehicle can be a bit of a daunting experience.</p>
                <p>Do you go for a flange or swan neck towbar? Is a fixed or detachable towbar right for your vehicle? And what is the difference between a towbar and a towball? All questions that will no doubt overwhelm a towbar beginner.</p>
                <p>That's why we’ve decided to put together this simple, step by step guide to understanding the different types of towbars. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-sm-push-3 col-md-push-3 page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading active" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <div class="pill-title">Towbar Overview</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        @include('partials.guide.towbar.towbar-overview')
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="collapsed" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                            <div class="pill-title">Towbar Electrics</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        @include('partials.guide.towbar.towbar-electrics')
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" class="collapsed" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                            <div class="pill-title">Towbar Fitting</div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        @include('partials.guide.towbar.towbar-fitting')
                                    </div>
                                </div>
                            </div>
                        </div>

                        @include('partials.guide.towbar.ideal-towbar')
                    </div>
                </div>
            </div>
            
            @include('partials.sidebar-complete-guide')
        </div>
    </div>
@stop


@section('scripts')
    <script type="text/javascript">
		$(document).ready(function() {
            var $root = $('html, body');

            $('a.smoothscroll').click(function () {

                // Just open the panel if data-open-panel is set
                if( $( this )[0].hasAttribute( 'data-open-panel' ) ) {

                    $( '#' + $.attr( this, 'data-open-panel' ) ).collapse('show');
                }

                $root.animate({

                    scrollTop: $( $.attr(this, 'href') ).offset().top
                }, 500);

                return true;
            });
		});
	</script>
@stop
