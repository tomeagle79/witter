@extends('layouts.frontend')

@section('title', $product->title)

@section('heading')
    {{ $product->title }}
@stop

@section('meta_description')
    Witter Towbars, {{ $product->title }}
@stop

@section('content')
	<div class="container witter-product view-accessory">
		<div class="row">

			<div class="col-sm-12 accessory-listing" itemscope itemtype="http://schema.org/Product">

				<h2 class="border-bottom no-top-margin" itemprop="name">{{ $product->title }}</h2>

				<div class="row">
                    <div class="col-md-4">
                        <div class="bordered img-center">
                            <img src="{{ image_load($product->imageUrl) }}" alt="" itemprop="image">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="details" itemprop="description">{!! nl2br($product->details) !!}</div> <!-- .details -->

                        <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            @php
                                $save = empty($product->retailPrice) ? 0 : $product->retailPrice - $product->price;
                            @endphp

                            @if($save > 0)

                                <div class="retail-price">RRP &pound;{{ price($product->retailPrice) }}</div>

                                Buy now for &pound;<span itemprop="price">{{ price($product->price) }}</span>

                                <div class="retail-price">Save &pound;{{ price($save) }}</div>

                            @else

                                &pound;<span itemprop="price">{{ price($product->price) }}</span>

                            @endif

                            <meta itemprop="priceCurrency" content="GBP" />
                            <meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
                            @if($product->isSellable && $product->inStock)

                                <link itemprop="availability" href="http://schema.org/InStock"/>
                            @else
                                <link itemprop="availability" href="http://schema.org/OutOfStock"/>
                            @endif
                        </div> <!-- .price -->

                        @if($product->isSellable && $product->inStock)
                            <a href="{{ route('add_to_cart', [
                                'type' => 'accessory',
                                'partNo' => $product->partNo
                            ]) }}" class="btn btn-primary">Add to Cart</a>
                        @else
                            <h4>Out of stock</h4>
                            <p>This item is currently out of stock</p>
                        @endif
                    </div> <!-- .col-md-9 -->
                </div> <!-- .row -->

                <h3>Additional Information</h3>

                <div class="bordered">
                    {!! nl2br($product->moreDetails) !!}
                </div> <!-- .bordered -->

			</div> <!-- .col-md-12 -->
		</div> <!-- .row -->
	</div> <!-- .container -->
@stop

@section('scripts')
<script>
        @include('partials.analytics.product-view', [
            'product' => $product,
            'category' => 'Roof Systems/Accessories'
        ])
</script>
@stop