// Toggle towbar search form on homepage
$('.towbar-search .btn-show').on('click', function (e) {
  e.preventDefault();
  $(this).parents('.towbar-search').addClass('open');
})