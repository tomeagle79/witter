<?php

namespace App\Models\Api\Suspension;

use App\Models\Api\ApiModel;

class SuspensionVehicleManufacturer extends ApiModel
{
    const URI = "suspensions/manufacturers";

    /**
     * Get all manufacturers
     *
     * @return Object
     */
    public static function get_all()
    {
        $url = config('witter.api_base') . self::URI;
        $obj = self::get_request($url);

        return $obj;
    }
}
