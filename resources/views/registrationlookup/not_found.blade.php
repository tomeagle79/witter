@extends('layouts.frontend')

@section('title', 'Registration Number Not Found')

@section('heading')
	Find Your Towbar
@stop

@section('meta_description')
	Witter Towbars Registration Not Found
@stop

@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">

            <div class="alert alert-danger" role="alert">

                <p>
                    Sorry, but we can't find that registration number.
                    Please try searching again or using one of the other
                    methods below.
                </p>

            </div>

			<div class="row">
				<div class="col-md-6 col-md-push-3">
					@include('partials.search-dropdowns')
				</div>
				<div class="col-md-3 col-md-pull-6">
					@include('partials.sidebar-search')
				</div>
				<div class="col-md-3 hidden-sm hidden-xs">
					@include('partials.sidebar-findlink-primary')

					@include('partials.sidebar-price')
				</div>
			</div>
			<div class="towbars-by-manufacturer">
				<h2 class="towbars-by-manufacturer-title">Find by <span class="slim">Manufacturer</span></h2>

				<p>You can find your towbar by selecting your manufacturer from the list below.</p>
                <?php
                // Count
                $count = count($manufacturers);
                ?>
				@foreach ($manufacturers as $key => $manufacturer)

					<?php $key++ ?>

					@if($key%12 === 1)
						<div class="row lazy-load-images">
					@endif

					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="manufacturer-logo">
							<a href="{{ route('towbars.model_list', $manufacturer->slug) }}">
								<img src="" data-src="{{ image_load($manufacturer->logoUrl) }}" alt="Towbars for {{ $manufacturer->manufacturerName }}" >
							</a>
						</div>
					</div>

					@if($key%12 === 0 && $key > 0 || $key == $count)
						</div><!-- .row -->
					@endif
				@endforeach
			</div>

		</div>
	</div>
</div>
@stop
