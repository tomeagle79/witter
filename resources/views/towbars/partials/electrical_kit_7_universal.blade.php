
    <h4>Universal Electrics</h4>

    <p>
        Universal electrics are compatible with all vehicles and provide legal lighting requirements, but the functionality is limited. Because newer cars have sophisticated electrical systems, a universal wiring kit cannot support complex safety functions, and you would also need a towbar wiring bypass relay. However, if you have an old car with limited electrical functions, a universal wiring kit would work fine.
    </p>

    <h4>What can a 7-pin electric socket power?</h4>

    <div class="powered">
        <ul class="row towbar-includes">
            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Left indicator</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Right indicator</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Taillights</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Fog lights</div>
            </li> <!-- col-sm-6 -->

            <li class="col-md-3 col-sm-4 col-xs-6 tick-li">
                <div class="include-item">Brake lights</div>
            </li> <!-- col-sm-6 -->

        </ul>
    </div><!-- /.powered -->
