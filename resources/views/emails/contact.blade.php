@extends('layouts.email')

@section('content')

    You received a message from Witter:

    <p>
    Name: {{ $name }}
    </p>

    <p>
    Phone: {{ $phone }}
    </p>

    <p>
    Postcode: {{ $postcode }}
    </p>

    <p>
    {{ $email }}
    </p>

    <p>
    {{ $user_message }}
    </p>

@stop