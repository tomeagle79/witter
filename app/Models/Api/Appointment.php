<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class Appointment extends ApiModel
{
	const URI = "appointments";

	/**
	 * Get a fitter list
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_fitters_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'?' . http_build_query($args);
		$obj = self::get_request($url);
		
		return $obj;
	}

	/**
	 * Get an appointment list
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_dates_where($partner_id, array $args)
	{
		$url = config('witter.api_base') . self::URI .'/'. $partner_id .'?' . http_build_query($args);
		$obj = self::get_request($url);
		
		return $obj;
	}

	/**
	 * Create a provisional booking
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function create_provisional_booking(array $args)
	{
		$url = config('witter.api_base') . self::URI .'/provisionalbooking' .'?' . http_build_query($args);
		$obj = self::get_request($url);
		
		return $obj;
	}
}
