<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use Session;

class HasCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $cart = Session::get('cart');
        if (!$request->ajax()) {
            if(empty($cart)) {
                return redirect(route('cart'));
            } else {
                if(empty($cart['contents']) && empty($cart['towbar'])) {
                    return redirect(route('cart'));
                }
            }
        }

        return $next($request);
    }
}
