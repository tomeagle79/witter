<?php

namespace App\Http\Middleware;

use App\Contracts\TenantLocaleContext;
use Closure;

class Frontend
{
    private $locale;

    private $website;

    public function __construct(TenantLocaleContext $locale)
    {
        $this->locale = $locale->getLocale();
        $this->website = $locale->getWebsite();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
