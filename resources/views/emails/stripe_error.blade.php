@extends('layouts.email')

@section('content')
<p>There was an error processing payment with Stripe on the Witter website</p>
<p>This <b>should not</b> be ignored.  Attempt a payment.</p>

<p>
    <strong>Error</strong>: {{ $error }}
</p>

@if(!empty($exceptmsg))
<p>
    <strong>Message</strong>: {{ print_r($exceptmsg, true) }}
</p>
@endif

@stop