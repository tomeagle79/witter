<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddToCartRequest;
use App\Models\Api\Accessory;
use App\Models\Api\Promotion;
use App\Models\Api\Settings;
use App\Models\Api\Towbar;
use Illuminate\Http\Request;
use Session;

class CartController extends Controller
{
    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        $this->data['include_main_banner'] = true;
    }

    /**
     * Add items to basket
     *
     * @return Response
     */
    public function add(AddToCartRequest $request)
    {
        if (empty($request->type)) {
            return redirect()
                ->route('cart')
                ->with('error', 'Something went wrong, there was nothing to add to your basket');
        } else {
            $product_added = [];

            switch ($request->type) {
                ###
                // Adding an accessory
                case 'accessory':
                    if (empty($request->partNo)) {
                        return redirect()
                            ->route('cart')
                            ->with('error', 'Sorry, we do not recognise that product type');
                    } else {
                        $cart = Session::get('cart');

                        if (empty($cart)) {
                            $cart = create_cart();
                        }

                        $cart['contents'][] = [
                            'cart_item_id' => uniqid(),
                            'type' => 'accessory',
                            'partNo' => $request->partNo,
                        ];

                        Session::put('cart', $cart);

                        $accessory = Accessory::get_single_where(['partNo' => $request->partNo]);

                        $product_added = [
                            'name' => $accessory->title,
                            'id' => $accessory->partNo,
                            'price' => $accessory->displayPrice,
                            'quantity' => 1,
                            'brand' => $accessory->brandName,
                            'category' => 'Accessories',
                        ];
                    }
                    break;

                // Adding an accessory
                case 'accessory-fitted':

                    if (empty($request->partNo)) {
                        return redirect()
                            ->route('cart')
                            ->with('error', 'Sorry, we do not recognise that product type.');
                    } else {
                        // Get the accessories data from the API
                        $accessory = Accessory::get_single_where(['partNo' => $request->partNo]);

                        if ($this->has_appointment()) {

                            $request->session()->flash('ga_event', [
                                'event_category' => 'Add to basket',
                                'event_action' => 'Double booking attempt',
                                'event_label' => $accessory->title,
                                'value' => $accessory->displayPrice,
                            ]);

                            return redirect()
                                ->back()
                                ->with('error', 'Sorry, you can only have one fitted product per order.');
                        }

                        $cart = Session::get('cart');

                        if (empty($cart)) {
                            $cart = create_cart();
                        }

                        $cart['contents'][] = [
                            'cart_item_id' => uniqid(),
                            'type' => 'accessory-fitted',
                            'partNo' => $request->partNo,
                            'slug' => $request->slug,
                            'appointmentDate' => $request->appointmentDate,
                            'appointmentTime' => $request->appointmentTime,
                            'appointmentId' => $request->appointmentId,
                            'partnerId' => $request->partnerId,
                            'fitterType' => $request->fitterType,
                            'address' => $request->address,
                            'customer_postcode' => $request->postcode,
                            'registrationDate' => $request->registrationDate,
                            'fitterFee' => $request->fitterFee,
                            'removal_option' => $request->removal_option,
                            'removal_cost' => $request->removal_option_cost,
                        ];

                        // Add flag to the cart to say we have a webfit item
                        if (!empty($request->appointmentId)) {

                            $cart['webfit'] = true;
                        }

                        Session::put('cart', $cart);

                        $price = $accessory->price;

                        // Calculate removal cost if required.
                        if (!is_null($request->removal_option)) {
                            $price += $request->removal_option_cost;
                        }

                        // Calculate the fitting cost.
                        if (!is_null($request->fittingPrice)) {
                            $price += $request->fittingPrice;
                        }

                        // Mobile fitting fee.
                        if (!is_null($request->fitterFee)) {
                            $price += $request->fitterFee;
                        }

                        $product_added = [
                            'name' => $accessory->title,
                            'id' => $accessory->partNo,
                            'price' => $price,
                            'quantity' => 1,
                            'brand' => $accessory->brandName,
                            'category' => 'Accessories',
                        ];
                    }
                    break;

                ###
                // Adding an electrical kit
                case 'electrical-kit':
                    if (empty($request->partNo)) {
                        return redirect()
                            ->route('cart')
                            ->with('error', 'Sorry, we do not recognise that product type.');
                    } else {
                        $cart = Session::get('cart');

                        if (empty($cart)) {
                            $cart = create_cart();
                        }

                        $cart['contents'][] = [
                            'cart_item_id' => uniqid(),
                            'type' => 'electrical-kit',
                            'partNo' => $request->partNo,
                        ];

                        Session::put('cart', $cart);

                        $accessory = Accessory::get_single_where(['partNo' => $request->partNo]);

                        $product_added = [
                            'name' => $accessory->title,
                            'id' => $accessory->partNo,
                            'price' => $accessory->displayPrice,
                            'quantity' => 1,
                            'brand' => $accessory->brandName,
                            'category' => 'Electrical Kits',
                        ];
                    }
                    break;

                ###
                // Adding a towbar
                case 'towbar':

                    if (empty($request->partNo)) {
                        return redirect()
                            ->route('cart')
                            ->with('error', 'Sorry, we do not recognise that product type.');
                    } else {

                        $towbar = Towbar::get_single_where(['vehicleTowbarSlug' => $request->slug]);

                        if ($request->fitterType !== "self_fitted" && $this->has_appointment()) {

                            $request->session()->flash('ga_event', [
                                'event_category' => 'Add to basket',
                                'event_action' => 'Double booking attempt',
                                'event_label' => $towbar->title,
                                'value' => $towbar->itemPrice,
                            ]);

                            return redirect()
                                ->back()
                                ->with('error', 'Sorry, you can only have one fitted product per order.');
                        }

                        $cart = Session::get('cart');

                        if (empty($cart)) {
                            $cart = create_cart();
                        }

                        $cart['towbar'] = [
                            'cart_item_id' => uniqid(),
                            'type' => 'towbar',
                            'partNo' => $request->partNo,
                            'variantTowbarId' => $request->variantTowbarId,
                            'vehicleDesc' => $request->vehicleDesc,
                            'slug' => $request->slug,
                            'towball_option' => $request->towball_option,
                            'electric_option' => $request->electric_option,
                            'appointmentDate' => $request->appointmentDate,
                            'appointmentTime' => $request->appointmentTime,
                            'appointmentId' => $request->appointmentId,
                            'partnerId' => $request->partnerId,
                            'fitterType' => $request->fitterType,
                            'address' => $request->address,
                            'customer_postcode' => $request->postcode,
                            'registrationDate' => $request->registrationDate,
                            'fitterFee' => $request->fitterFee,
                            'softwareUpgrade' => $request->software_upgrade === 'true' ? true : false,
                            'softwareUpgradePrice' => Settings::get_recode_premium(),
                        ];

                        // Add flag to the cart to say we have a webfit item
                        if ($request->fitterType !== "self_fitted") {
                            $cart['webfit'] = true;
                        }

                        Session::put('cart', $cart);

                        $product_added = [
                            'name' => $towbar->title,
                            'id' => $towbar->towbarPartNo,
                            'price' => $towbar->itemPrice,
                            'quantity' => 1,
                            'brand' => $towbar->brandName,
                            'category' => 'Accessories',
                        ];
                    }
                    break;
            }

            // Apply the cart discounts. If they have been set
            if (!empty($cart['promotion'])) {
                apply_promo_to_cart($cart['promotion']['promotionCode']);
            } elseif (!empty($cart['webfit']) && !empty($cart['contents'])) {
                // If the cart has webfit items add WebFit discount code
                apply_promo_to_cart(config('witter.webfit_discount_code'));
            }

            return redirect(route('cart'))
                ->with('product_added', $product_added);
        }
    }

    /**
     * Remove standard item from cart
     */
    public function remove(Request $request)
    {
        if (isset($request->id)) {
            $cart = Session::get('cart', []);

            $i = 0;
            if (!empty($cart['contents'])) {
                foreach ($cart['contents'] as $item) {
                    if ($item['cart_item_id'] == $request->id) {

                        // Remove the webfit flag if the accesssory has an appointment ID / means its fitted
                        if (!empty($item['appointmentId']) && !empty($cart['webfit'])) {
                            unset($cart['webfit']);
                        }

                        // Remove item from the contents array
                        unset($cart['contents'][$i]);
                    }
                    $i++;
                }
                $cart['contents'] = array_values($cart['contents']); // Reset array keys
            }

            Session::put('cart', $cart);

            if (!empty($cart['promotion'])) {
                apply_promo_to_cart($cart['promotion']['promotionCode']);
            }
        }
        return redirect()
            ->route('cart');
    }

    /**
     * Remove towbar from cart
     */
    public function remove_towbar(Request $request)
    {
        $cart = Session::get('cart', []);

        // If the cart had a webfit flag then remove.
        if (!empty($cart['webfit']) && !empty($cart['towbar']['appointmentId'])) {

            unset($cart['webfit']);
        }

        $cart['towbar'] = [];

        Session::put('cart', $cart);

        if (!empty($cart['promotion'])) {
            apply_promo_to_cart($cart['promotion']['promotionCode']);
        }

        return redirect()
            ->route('cart');
    }

    /**
     * Show the cart
     *
     * @return Response
     */
    public function cart()
    {
        $cart = build_cart();

        if (empty($cart['contents']) && empty($cart['towbar'])) {

            $this->data['alsobought'] = Accessory::get_also_bought();

            return view('cart.empty', $this->data);
        }

        $this->data['cart'] = $cart;

        $this->data['alsobought'] = Accessory::get_also_bought();

        // Check if the quote_ref has been set. If so allow the user to change their appointment date.
        if (!empty($cart['webfit']) && !empty($cart['quote_ref'])) {

            $this->data['allow_date_change'] = true;
        }

        return response()
            ->view('cart.index', $this->data)
            ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    }

    /**
     * Apply a promotion to the cart
     *
     * @return Response
     */
    public function apply_promo(Request $request)
    {
        if (empty($request->discount_code)) {
            return redirect('/cart');
        } else {
            $promo = apply_promo_to_cart($request->discount_code);

            if (!empty($promo['error'])) {

                $cart = Session::get('cart', []);

                if (!empty($cart['webfit'])) {
                    // If discount code is incorrect but user still has a webfit discount.
                    apply_promo_to_cart(config('witter.webfit_discount_code'));
                }

                // Return to cart with error message
                return redirect('/cart')
                    ->with('error', $promo['error']);
            } else {
                return redirect('/cart')
                    ->with('success', '<i class="icon-tag"></i>Woohoo! Your discount code has been applied to your basket');
            }
        }
    }

    /**
     * Remove a promotion from the cart
     *
     * @return Response
     */
    public function remove_promo(Request $request)
    {
        $cart = Session::get('cart', []);

        if (empty($cart['webfit'])) {

            $cart['promotion'] = null;

            Session::put('cart', $cart);

            return redirect('/cart')
                ->with('error', 'Discount code removed');

        } else {
            apply_promo_to_cart(config('witter.webfit_discount_code'));

            return redirect('/cart')
                ->with('message', 'Discount code removed, Webfit discount still applies');
        }
    }

    /**
     * Check if there's already an appointment in the session.
     * This is as we only allow a single fitted 'thing' per order.
     * @return boolean - false = no appointment
     */
    private function has_appointment()
    {
        $cart = build_cart();

        // If no items in the cart return false
        if (empty($cart['contents']) && empty($cart['towbar'])) {
            return false;
        }

        // Check if there is an appointment for fitted accessoires.
        if (isset($cart['contents'])) {
            foreach ($cart['contents'] as $item) {
                if (isset($item['appointmentId'])) {
                    return true;
                }
            }
        }

        // Check if the towbar has a fitting appointment
        if (isset($cart['towbar'])) {
            if (isset($cart['towbar']['appointmentId'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Update the customer's fittering appointment
     *
     * @return Response
     */
    public function adjustAppointment(Request $request)
    {
        $cart = Session::get('cart');

        // If no items in the cart return false
        if (empty($cart['contents']) && empty($cart['towbar'])) {
            return false;
        }

        // Check if the towbar has a fitting appointment
        if (!empty($cart['towbar'])) {

            if (!empty($cart['towbar']['appointmentId'])) {
                $cart['towbar']['appointmentId'] = $request->input('appointment_id');
                $cart['towbar']['appointmentTime'] = $request->input('appointmentTime');
                $cart['towbar']['appointmentDate'] = $request->input('appointment_date');
                $cart['towbar']['fitterType'] = $request->input('fitterType');
                $cart['towbar']['partnerId'] = $request->input('partnerId');
                $cart['towbar']['address'] = $request->input('address');
                $cart['towbar']['fitterFee'] = $request->input('fitterFee');
            }
        } elseif (!empty($cart['contents'])) {
            foreach ($cart['contents'] as $key => $item) {
                if (!empty($item['appointmentId'])) {
                    $cart['contents'][$key]['appointmentId'] = $request->input('appointment_id');
                    $cart['contents'][$key]['appointmentTime'] = $request->input('appointmentTime');
                    $cart['contents'][$key]['appointmentDate'] = $request->input('appointment_date');
                    $cart['contents'][$key]['fitterType'] = $request->input('fitterType');
                    $cart['contents'][$key]['partnerId'] = $request->input('partnerId');
                    $cart['contents'][$key]['address'] = $request->input('address');
                    $cart['contents'][$key]['fitterFee'] = $request->input('fitterFee');
                }
            }
        }

        $cart['new_appointment_required'] = false;

        Session::put('cart', $cart);

        return redirect()
            ->route('cart')
            ->with('success', 'Your appointment has been updated.');
    }
}
