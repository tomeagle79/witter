<div class="form-group required">
	<label for="title" class="col-md-2 control-label">Title</label>
	<div class="col-md-4">
		{!! Form::text('title', ( ($type == 'edit') ? $blog_post->title : null), ['class' => 'form-control title-for-slug', 'id' => 'title', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group">
	<label for="published" class="col-sm-3 col-md-2 control-label">Published</label>
	<div class="col-sm-9 col-md-6">
		{!! Form::checkbox('published', true, isset($blog_post) ? $blog_post->published : true, ['class' => '']) !!}
		<p class="help-block">Display on website?</p>
	</div>
</div>

<div class="form-group required">
	<label for="url_slug" class="col-md-2 control-label">URL Slug</label>
	<div class="col-md-4">
		{!! Form::text('url_slug', ( ($type == 'edit') ? $blog_post->url_slug : null), ['class' => 'form-control slug-generated', 'id' => 'slug']) !!}
	</div>
</div>

<div class="form-group">
	<label for="meta_title" class="col-md-2 control-label">Meta Title</label>
	<div class="col-md-4">
		{!! Form::text('meta_title', ( ($type == 'edit') ? $blog_post->meta_title : null), ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<label for="meta_description" class="col-md-2 control-label">Meta Description</label>
	<div class="col-md-4">
		{!! Form::textarea('meta_description', ( ($type == 'edit') ? $blog_post->meta_description : null), ['class' => 'form-control', 'rows' => 3]) !!}
	</div>
</div>

@if($type=='edit' && !is_null($blog_post->image))
	<div class="form-group">
		<label for="logo" class="control-label col-xs-12 col-sm-3 col-md-2 col-lg-2">Current Image</label>
		<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">

			<a href="{{URL('imagecache', ['template' => 'original', 'filename' => $blog_post->image->filename])}}" class="fancybox image">
				<img src="{{URL('imagecache', ['template' => 'small', 'filename' => $blog_post->image->filename])}}" alt="" />
			</a>
		</div>
	</div>
@endif

<div class="form-group">
	<label for="logo" class="control-label col-xs-12 col-sm-3 col-md-2 col-lg-2">Image</label>
	<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
		<div class="fileinput fileinput-new" data-provides="fileinput">
			<div class="input-group input-large">
				<div class="form-control uneditable-input" data-trigger="fileinput">
					<i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename">
					</span>
				</div>
				<span class="input-group-addon btn default btn-file">
				<span class="fileinput-new">
				Select file </span>
				<span class="fileinput-exists">
				Change </span>
				<input type="file" name="file">
				</span>
				<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">
				Remove </a>
			</div>
		</div>
	</div>
</div>

<div class="form-group required">
	<label for="content" class="control-label col-xs-12 col-sm-3 col-md-2 col-lg-2">Content</label>
	<div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
		{!! Form::textarea('content', ( ($type == 'edit') ? $blog_post->content : null), ['class' => 'form-control wysiwyg']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="category_ids[]" class="control-label col-xs-12 col-sm-3 col-md-2 col-lg-2">Categories</label>
	<div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
	@foreach($categories as $category)
	<div class="checkbox">
		<label>{!! Form::checkbox('category_ids[]', $category->id, $type == 'edit' && in_array($category->id, $blog_post->categories()->pluck('id')->all()) ? true : false, array( 'class' => '' )) !!} {!! $category->title !!}</label>
	</div>
	@endforeach
	</div>
</div>

<div class="form-group">
	<div class="col-md-offset-2 col-md-4">
		<button type="submit" class="btn btn green">{{ $submit_text }}</button>
	</div>
</div>


@section('scripts')
	@parent
	<script type="text/javascript" src="{!! URL::asset('assets/js/redactor.js') !!}"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('textarea.wysiwyg').redactor({
				minHeight: 300,
				plugins: ['table', 'video', 'imagemanager'],
				buttonSource: true,
				imageUpload: '{!! URL::to('wtadmin/redactor/image_upload') !!}?_token=' + '{{ csrf_token() }}',
				imageManagerJson: '{!! URL::to('wtadmin/redactor/get_image_json') !!}',
			});
		});

	</script>
@overwrite

