
<div class="sidebar-blog-article">

	<article class="blog-article">

		@if(!is_null($blog->image))
			<img src="{{URL('imagecache', ['template' => 'small-thumbnail', 'filename' => $blog->image->filename])}}" alt="{!! $blog->title !!}" />
		@endif

		<a href="{!! route('blog_dynamic_route', $blog->url_slug) !!}" class="blog-title"><h3 class="blog-list-header">{!! $blog->title !!}</h3></a>

		<div class="blog-copy">
			{!! substr($blog->content, 0, 300) !!}...
		</div> <!-- .excerpt -->

	</article>
</div>
