@extends('layouts.frontend')

@section('title', 'Roof Accessories')

@section('heading')
	Witter Towbars - Roof Accessories
@stop

@section('meta_description')
	Witter Towbars - Roof Accessories
@stop

@section('content')
	<div class="container witter-listing">
		<div class="row">

			<div class="col-md-12 accessory-listing">

				<h2 class="no-top-margin">Witter Towbars <span class="slim">Roof Accessories</span></h2>

				<div class="row item-list is-flex">

                    @foreach($categories as $category)

                        <div class="col-sm-4 col-md-4">
                            <div class="item text-center">
                                <a href="{!! route('roof-systems.roof-accessories.category', [$category->slug]) !!}" ><img src="{{ image_load($category->imageUrl) }}" alt="" /></a>

                                <a href="{!! route('roof-systems.roof-accessories.category', [$category->slug]) !!}" ><h3 class="list-title">{!! $category->categoryName !!}</h3></a>
                            </div>
                        </div>

                    @endforeach				

				</div> <!-- .row -->
					

			</div> <!-- .col-md-12 -->
		</div> <!-- .row -->
	</div> <!-- .container -->
@stop

