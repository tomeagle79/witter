<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\BuildShoppingFeed::class,
        Commands\BuildSitemap::class,
        Commands\PayPalProfile::class,
        Commands\UpdateBanners::class,
        Commands\LowercaseImages::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('twitter:refresh')
            ->hourly()
            ->unlessBetween('23:00', '4:00');

        $schedule->command('build:shopping_feed')
            ->dailyAt('01:00');

        $schedule->command('build:sitemap')
            ->dailyAt('02:00');

        // Run the banners update command every hour.
        $schedule->command('banners:update')
            ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
