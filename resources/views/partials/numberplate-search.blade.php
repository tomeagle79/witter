
<div class="numberplate-wrapper">

    {!! Form::open(['route' => 'search.numberplate', 'class' => 'form numberplate-form float-right', 'method' => 'get', 'onsubmit' => "
        gtag('event', 'Success', {
            event_category: 'Reg lookup',
            event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
        });
    "]) !!}

        <div class="row">

            <div class="col-xs-12">

                <div class="num-plate @if($numberplate) to-edit @endif">
                    <a href="#" class="edit-btn"><i class="icon-pen"></i></a>
                    {!! Form::text('search', $numberplate, ['required', 'class' => 'form-control pull-left', 'placeholder' => 'Reg Number', 'onfocus' => "this.placeholder = ''", 'onblur' => "this.placeholder = 'Reg Number'"]) !!}
                    <button type="submit" class="submit-btn"><i class="icon-angle-right"></i></button>
                </div>

                <div class="incorrect_vehicle-wrapper">

                    <a href="{{ route('towbars') }}" class="incorrect_vehicle">Not your Vehicle?</a>

                </div><!-- /.pull-left -->

            </div><!-- /.col-xs-8 -->

        </div><!-- /.row -->

    {!! Form::close() !!}

</div><!-- /.numberplate-wrapper -->

<script>
    jQuery(document).ready(function($){
        $('.edit-btn').on('click', function(e) {
            e.preventDefault();
            var value = $(this).val();
            $('.num-plate input').focus();
            if(! $('.num-plate').hasClass('to-edit')) {
                $('.num-plate').addClass('to-edit');
            } else {
                $('.num-plate').removeClass('to-edit');
            }
        });

        $('.num-plate input').on('input', function(e) {
            e.preventDefault();
            var value = $(this).val();
            if(value.length >= 1) {
                $('.num-plate').removeClass('to-edit');
            } else {
                $('.num-plate').addClass('to-edit');
            }
        });
    });
</script>
