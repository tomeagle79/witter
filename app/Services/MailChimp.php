<?php

namespace App\Services;

use Log;

class MailChimp
{
    private $api_key;
    private $list_id;

    public function __construct()
    {
        /**
         * Just get the config info etc.
         */
        $this->api_key = config('mailchimp.api_key');

        $this->list_id = config('mailchimp.list_id');
    }

    /**
     * Method to add user to list.
     * Based on this tutorial.
     * https://www.codexworld.com/add-subscriber-to-list-mailchimp-api-php/
     */
    public function add_user($user_data, $list_id = null)
    {
        // If the list ID has been passed in use it.
        if(!is_null($list_id)){

            $this->list_id = $list_id;
        }

        // Check we have the required data to proceed.
        if(empty($this->api_key) || empty($this->list_id)){

            Log::error('Mailchimp Settings are not correct please check.');
            return true;
        }

        try {

            // MailChimp API URL
            $memberID = md5(strtolower($user_data->customer_email));
            $dataCenter = substr($this->api_key,strpos($this->api_key,'-')+1);
            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $this->list_id . '/members/' . $memberID;

            // member information
            $json = json_encode([
                'email_address' => $user_data->customer_email,
                'status'        => 'subscribed',
                'merge_fields'  => [
                    'FNAME'     => $user_data->customer_first_name,
                    'LNAME'     => $user_data->customer_surname
                ]
            ]);

            // send a HTTP POST request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $this->api_key);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // Log the status message based on response code
            if ($httpCode != 200) {
                switch ($httpCode) {
                    case 214:
                        Log::warning('Mailchimp 214 Warning. User already subscribed to Mailchimp');
                        break;
                    default:
                        Log::error('Mailchimp - Some problem occurred.');
                        Log::error(json_encode($result) . ' httpCode '. $httpCode);
                        break;
                }
            }

        } catch(Exception $e) {

            Log::error('Mailchimp Error Message: ' . $e->getMessage());
        }

        return true;
    }
}
