@extends('layouts.frontend')

@section('title', 'Service centres')

@section('heading')
	Service centres
@stop

@section('meta_description')
	Service centres
@stop

@section('JSON-LD')
<script type='application/ld+json'>
{
 	"@context": "http://www.schema.org",
 	"@type": "AutomotiveBusiness",
	@if(!empty($centre->centreName))"name": "{{ $centre->centreName }}", @endif
	"url": "{{ url()->current() }}",
 	"logo": "https://www.witter-towbars.co.uk/assets/img/logo-white-curve.png",
  	"image": "https://www.witter-towbars.co.uk/imagecache/original/UnCP32Hqek1q6PDEWOB1CCNqTo4niFOSgV8bPy0B.jpg",
  	"description": "Witter Towbars fitted by {{ $centre->centreName }}. Our all-in-one price includes towbar, electric kit and fitting. Buy your towbar and arrange a fitting at {{ $centre->centreName }} online today.",
  	"address": {
	    "@type": "PostalAddress",
	    @if(!empty($centre->address1))"streetAddress": "{{ $centre->address1 }}", @endif
	    @if(!empty($centre->city))"addressLocality": "{{ $centre->city }}", @endif
	    @if(!empty($centre->city))"addressRegion": "{{ $centre->county }}", @endif
	    @if(!empty($centre->postcode))"postalCode": "{{ $centre->postcode }}", @endif
	    "addressCountry": "UK"
  	},
  	@if(!empty($ld_json_hours))"openingHours": "{{ $ld_json_hours }}", @endif
  	"contactPoint": {
	    "@type": "ContactPoint",
	    "contactType": "customer service",
	    "telephone": "+44(0)1244 284555"
  	},
  	"telephone": "+44(0)1244 284555"
}
 </script>
@stop

@section('content')

<div class="container service-centres">

	<div class="row">

		<div class="col-sm-7 centres-form">

			@if(!empty($centre))

				<div class="nearest-centre">

					<div class="row">

						@if($centre->isApprovedCentre || !empty($centre->canMobileFit))

							<div class="col-xs-12 col-md-9">
								<h4>{{ $centre->centreName }}</h4>
							</div><!-- /.col-sm-9 -->

							<div class="col-xs-12 col-md-3">
								<div class="centre-icons">
					                @if($centre->isApprovedCentre)
					                    <i class="icon-tick" title="Approved centre"></i>
					                @endif

					                @if(!empty($centre->canMobileFit))
					                    <i class="icon-mobile-fitter" title="Mobile fitter"></i>
					                @endif
								</div><!-- /.centre-icons -->
							</div><!-- /.col-sm-3 -->
						@else

							<div class="col-xs-12">
								<h4>{{ $centre->centreName }}</h4>
							</div><!-- /.col-xs-12 -->
						@endif

					</div><!-- /.row -->

					<hr>

					@if($centre->isApprovedCentre)
						<div class="centre-approved-container">
							<h2>THIS IS A WITTER APPROVED SERVICE CENTRE</h2>
							<hr>
							<p>Our Approved Service Centre scheme certifies that a centre meets Witter Towbars' quality standards and requirements. When you choose to buy a high-quality product, make sure it's fitted to the highest standard. Choose an approved Service Centre that is qualified to install our products.</p>
						</div>

						<hr>
					@endif

					@if($centre->isApprovedCentre)
						<div class="center approved-centre">
					@else
						<div class="centre">
					@endif

						<div class="row">

							<div class="col-sm-6">
								@if(!empty($centre->address1)){{ $centre->address1 }}<br>@endif
								@if(!empty($centre->address2)){{ $centre->address2 }}<br>@endif
								@if(!empty($centre->address3)){{ $centre->address3 }}<br>@endif
								@if(!empty($centre->city)){{ $centre->city }}<br>@endif
								@if(!empty($centre->county)){{ $centre->county }}<br>@endif
								@if(!empty($centre->postcode)){{ $centre->postcode }}<br>@endif
							</div><!-- /.col-sm-6 -->

							<div class="col-sm-6">
								@if(!empty($centre->openingHours))
									@foreach($centre->openingHours as $day => $hours)
									<div class="opening-hours @if(strtolower($day) == strtolower(date('D'))) today @endif "><span class="day">{{ date('l', strtotime($day)) }}</span> {{ $hours }}</div>
									@endforeach
								@endif
							</div><!-- /.col-sm-6 -->
						</div><!-- /.row -->

						<hr>

						<div id="map" class="contact-map"></div>

					</div><!-- /.item -->

				</div><!-- /.other-centres -->

			@endif


		</div><!-- /.col-sm-7 -->

		<div class="col-sm-5">

			<div class="card-bg-grey col-xs-12">
			 	<div class="card-bg-grey-body">
			    	<h2 class="card-blue-header">Would you like your Towbar fitted at this service centre? Find your ideal Towbar...</h2>

					<div class="row">
						<div class="col-xs-8 col-xs-offset-2">
							<a href="{{ route('towbars') }}" class="btn btn-green btn-green-large">Browse and book now</a>
						</div><!-- /.col-xs-8 -->
					</div><!-- /.row -->
			  	</div>

			 	<div class="card-bg-grey-footer">
					<div class="row">
						<div class="col-xs-12">

							<h3 class="footer-title">Any Questions? Call us on 01244 284 555</h3>

						</div><!-- /.col-xs-12 -->
					</div><!-- /.row -->
				</div><!-- /.card-bg-grey-footer -->
			</div><!-- /.card-bg-grey -->

			@if(!empty($centre->services))

				<div class="card-basic col-xs-12">
					<h2 class="card-basic-title">Services Offered</h2>
					<hr>
					<ul>
					@foreach($centre->services as $service)
						<li class="col-sm-6">{{ $service->serviceDescription}}</li>
					@endforeach
					</ul>
				</div><!-- /.card-bg-grey -->
			@endif

			@if(!empty($centre->facilities))

				<div class="card-basic col-xs-12">
					<h2 class="card-basic-title">Customer Facilities</h2>
					<hr>
					<ul>
					@foreach($centre->facilities as $facility)
						<li class="col-sm-6">{{ $facility->facilityDescription}}</li>
					@endforeach
					</ul>
				</div><!-- /.card-bg-grey -->
			@endif

			@if(!empty($centres))

				<div class="other-centres">

					<h2 class="other-centres-title">Other nearby centres</h2>

					@foreach($centres as $nearby_centre)

						@if($centre->isApprovedCentre)
							@include('service-centres.partials.centre-card-approved', ['centre' => $nearby_centre])
						@else
							@include('service-centres.partials.centre-card-basic', ['centre' => $nearby_centre])
						@endif

					@endforeach
				</div><!-- /.other-centres -->
			@endif

		</div><!-- /.col-sm-5 -->

	</div><!-- /.row -->

</div><!-- /.container -->

@stop

@section('scripts')

	@if(!empty($geocode['lat']) && !empty($geocode['lng']))

	    <script type="text/javascript">
			function initMap() {
				var witter = {lat: {{ $geocode['lat'] }}, lng: {{ $geocode['lng'] }}};
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 12,
					center: witter
				});

				var marker = new google.maps.Marker({
				  	position: witter,
				  	map: map,
				 	title: ''
				});
			}

	    </script>

	    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-VmDhNlxlKQoxC7F7YXoQvmS2APd6Qw0&callback=initMap">
	    </script>
	@endif


@stop


