<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReturnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_number' => ['required', 'max:255'],
            'first_name' => ['required', 'max:255'],
            'surname' => ['required', 'max:255'],
            'company_name' => ['max:255'],
            'postcode' => ['required', 'max:255'],
            'address1' => ['required', 'max:255'],
            'address2' => ['max:255'],
            'address3' => ['max:255'],
            'towncity' => ['required', 'max:255'],
            'telephone' => ['required', 'max:255'],
            'fax' => ['max:255'],
            'email' => ['required', 'email', 'max:255'],
            'date_purchased_dd' => ['required', 'max:255'],
            'date_purchased_mm' => ['required', 'max:255'],
            'date_purchased_yyyy' => ['required', 'max:255'],
            'product_serial_no' => ['max:255'],
            'product_name' => ['required', 'max:255'],
            'product_part_no' => ['max:255'],
            'return_reason' => ['required', 'max:255'],
        ];
    }
}
