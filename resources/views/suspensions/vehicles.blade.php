@extends('layouts.frontend')

@section('title', $manufacturer->manufacturerName .' Air Suspension Kit | '. $manufacturer->manufacturerName)

@section('heading')
Air Suspension Kits for {{ $manufacturer->manufacturerName }} {{ $model->modelName }}
@stop

@section('meta_description')
	Buy an {{ $manufacturer->manufacturerName }} Air Suspension Kit direct from Witter Towbars.
@stop

@section('content')


<div class="container towbars-listing">

	<div class="row">

        <div class="col-xs-12">
            <h2>{{ $manufacturer->manufacturerName }} {{ $model->modelName }}</h3>
        </div><!-- /.col-xs-12 -->

        <div class="col-xs-12 col-sm-9">

            @include('partials.search-dropdowns-suspensions', ['redirect_slug' => 'air-suspension-kits',
                'form_button_text' => 'Find your suspension kit',
                'display_models' => false,
                'display_manufacturers' => false,
                'vehicle_dropdown_url' => route('suspensions')])

        </div><!-- /.col-xs-9 -->

        <div class="hidden-xs col-sm-3 col-md-3">
            @if( !empty($model->photoUrl) )
                <div class="top-img-wrapper">
                    <img src="{{ $model->photoUrl }}" alt="" class="top-img">
                    <p class="text-center">Images are representative only</p>
                </div> <!-- .wrapper -->
            @elseif(!empty($manufacturer->logoUrl))
                <div class="top-img-wrapper">
                    <img src="{{ $manufacturer->logoUrl }}" alt="{{ $manufacturer->manufacturerName }}" class="top-img">
                    <p class="text-center">Images are representative only</p>
                </div> <!-- .wrapper -->
            @endif

        </div><!-- /.col-md-3 -->

    </div><!-- /.row -->

    <div class="row">
		<div class="col-md-12  towbars-listing">
            <hr >
            @if(count($vehicles) > 0)
                <h2 class="title-margin-bottom">Find an Air Suspension Kit for {{ $manufacturer->manufacturerName }} {{ $model->modelName }} vehicles</h2>
            @endif

            <div class="row item-list flex-items">

                @foreach ($vehicles as $key => $vehicle)

					<div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="item">
                            <div class="manufacturer-logo">
                                @if(!empty($vehicle->photoUrl))
                                    <a href="{{ route('suspensions.models.vehicles.products', [$manufacturer_slug, $model_slug, $vehicle->slug]) }}">
                                        <img src="{{ image_load($vehicle->photoUrl) }}" alt="Air Suspension Kits for {{ $vehicle->vehicleName }}" >
                                    </a>
                                @endif
                                    <a href="{{ route('suspensions.models.vehicles.products', [$manufacturer_slug, $model_slug, $vehicle->slug]) }}">
                                    @if(!empty($vehicle->chassisDesignation))
                                        <h3>Chassis {{ $vehicle->chassisDesignation }}</h3>
                                    @endif
                                    <h4>
                                        @if(!empty($vehicle->fromMonth))
                                            {{ date('M', mktime(0, 0, 0, $vehicle->fromMonth, 1, $vehicle->fromYear)) }}
                                        @endif
                                        {{ $vehicle->fromYear }}
                                        -
                                        @if(empty($vehicle->toYear))
                                            Onwards
                                        @else
                                            @if(!empty($vehicle->toMonth))
                                                {{ date('M', mktime(0, 0, 0, $vehicle->toMonth, 1, $vehicle->toMonth)) }}
                                            @endif
                                            {{ $vehicle->toYear }}
                                        @endif
                                    </h4>
                                </a>
                            </div>
                        </div> <!-- .item -->
                    </div> <!-- .col-xs-12 .col-sm-4 .col-md-3 -->
				@endforeach

			</div><!-- .row -->

		</div>
	</div>
</div>
@stop
