@extends('layouts.frontend')

@section('title', 'Car &amp; Motorhome Towbars | UK Tow Bars')

@section('heading')
	Car &amp; Motorhome Towbars
@stop

@section('meta_description')
	Car and motorhome towbars by Witter Towbars. Our all-in-one price includes towbar, electric kit and fitting. Buy and arrange a fitting online today.
@stop

@section('content')

<div class="towbars-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="section-title">Our Towbar brands</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p>We have an extensive range of 5000 towbars listed online. We design our products to conform to different vehicle specifications. So whether you are looking for a towbar to pull a trailer or a horsebox, we have a product to suit you.</p>
                <p>When it comes to quality we go beyond the limits. Our manufacturing process uses materials that are robust and built to last a lifetime. We also test our products beyond the minimum standards. When you buy from Witter Towbars, you can rest assured that the towbar, electrical kit and the fitting work is of unrivalled quality.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <img src="/assets/img/towbars/witter-logo.png" alt="" >
                <img src="/assets/img/towbars/westfalia-logo.png" alt="" >
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="reg-search">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <p><strong>Let's find your vehicle</strong></p>
                            <a href="/towbars">Not got your registration number?</a>
                        </div>
                        {!! Form::open(['route' => 'search.numberplate', 'class' => 'form numberplate-search float-right', 'method' => 'get', 'onsubmit' => "
                            gtag('event', 'Success', {
                                event_category: 'Reg lookup',
                                event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                            });
                        "]) !!}
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                {!! Form::text('search', null, ['required', 'class' => 'form-control', 'placeholder' => 'Reg Number']) !!}
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <button type="submit"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container which-towbar">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="section-title">Which Towbar is right for you?</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="towbar-image-wrapper">
                <div class="towbar-image" style="background-image: url('https://picsum.photos/260/?random');"></div>
            </div>

            <p class="towbar-title">Fixed Flange</p>

            <p>The flange towbar refers to the actual shape and design of the towbar. This towbar is… <strong>more</strong></p>

            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Bumper shields can be fitted</div>
            </div>
            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Compatible with AL-KO towballs</div>
            </div>
            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Accessories and tow couplings can be fitted</div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="towbar-image-wrapper">
                <div class="towbar-image" style="background-image: url('https://picsum.photos/260/?random');"></div>
            </div>

            <p class="towbar-title">Fixed Flange</p>

            <p>The flange towbar refers to the actual shape and design of the towbar. This towbar is… <strong>more</strong></p>

            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Carry bikes whilst towing</div>
            </div>
            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Compatible with AL-KO towballs</div>
            </div>
            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Reversing sensors won’t be set off when not towing</div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="towbar-image-wrapper">
                <div class="towbar-image" style="background-image: url('https://picsum.photos/260/?random');"></div>
            </div>

            <p class="towbar-title">Fixed Flange</p>

            <p>The flange towbar refers to the actual shape and design of the towbar. This towbar is… <strong>more</strong></p>

            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Compatible with AL-KO towballs</div>
            </div>
            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Not as likely to trigger reversing sensors</div>
            </div>
            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">More aesthetically pleasing than flange towbars</div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="towbar-image-wrapper">
                <div class="towbar-image" style="background-image: url('https://picsum.photos/260/?random');"></div>
            </div>

            <p class="towbar-title">Fixed Flange</p>

            <p>The flange towbar refers to the actual shape and design of the towbar. This towbar is… <strong>more</strong></p>

            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Easy to remove when not in use</div>
            </div>
            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Reversing sensors won’t be set off when not towing</div>
            </div>
            <div class="row benefit">
                <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                <div class="col-md-10 col-sm-10 col-xs-10">Compatible with AL-KO towballs</div>
            </div>
        </div>
    </div>

    <div class="row">
            @include('partials.search-dropdowns-horizontal')
    </div>
</div>

<div class="what-type">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2>What type of fitting is right for me?</h2>
            </div>
        </div>
        <div class="row margin-bottom">
            <div class="col-xs-12">
                <p>We have an extensive range of 5000 towbars listed online. We design our products to conform to different vehicle specifications. So whether you are looking for a towbar to pull a trailer or a horsebox, we have a product to suit you.</p>
                <p>When it comes to quality we go beyond the limits. Our manufacturing process uses materials that are robust and built to last a lifetime. We also test our products beyond the minimum standards. When you buy from Witter Towbars, you can rest assured that the towbar, electrical kit and the fitting work is of unrivalled quality.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        Icon
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p><strong>Workshop Fitting</strong></p>
                        <div class="row benefit">
                            <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                            <div class="col-md-10 col-sm-10 col-xs-10">A benefit of workshop fitting</div>
                        </div>
                        <div class="row benefit">
                            <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                            <div class="col-md-10 col-sm-10 col-xs-10">A benefit of workshop fitting</div>
                        </div>
                        <div class="row benefit">
                            <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                            <div class="col-md-10 col-sm-10 col-xs-10">A benefit of workshop fitting</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p><strong>Mobile Fitting</strong></p>
                        <div class="row benefit">
                            <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                            <div class="col-md-10 col-sm-10 col-xs-10">A benefit of mobile fitting</div>
                        </div>
                        <div class="row benefit">
                            <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                            <div class="col-md-10 col-sm-10 col-xs-10">A benefit of mobile fitting</div>
                        </div>
                        <div class="row benefit">
                            <div class="col-md-2 col-sm-2 col-xs-2"><i class="icon-simple-tick"></i></div>
                            <div class="col-md-10 col-sm-10 col-xs-10">A benefit of mobile fitting</div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        Icon
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="how-it-works">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="title">How does it work?</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus elementum eget felis sed tempus. Donec vitae imperdiet tellus. Maecenas lectus ipsum, facilisis ut sapien in, varius rutrum mauris. Duis vulputate iaculis justo, in auctor ante semper et.</p>
            </div>
        </div>
        <div class="row">
            <div class="how col-md-3 col-sm-3 col-xs-12">
                <div class="circle"><span>1</span></div>
                <p class="how-title">Find your towbar</p>
                <p>Search for your vehicle and we’ll help you to find the best towbar for your purpose</p>
            </div>
            <div class="how col-md-3 col-sm-3 col-xs-12">
                <div class="circle"><span>2</span></div>
                <p class="how-title">Customise your towbar</p>
                <p>Select from a wide range of optional extras to ensure your towbar is right for you</p>
            </div>
            <div class="how col-md-3 col-sm-3 col-xs-12">
                <div class="circle"><span>3</span></div>
                <p class="how-title">Book your fitting</p>
                <p>Choose between workshop fitting or the convenience of a mobile fitter coming to you*</p>
            </div>
            <div class="how col-md-3 col-sm-3 col-xs-12">
                <div class="circle"><span>4</span></div>
                <p class="how-title">Your towbar fitting</p>
                <p>One of our approved fitters will fit your towbar and start your Witter Guarantee</p>
            </div>
        </div>
    </div>
</div>

<div class="manufacturers">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="title">Browse by manufacturer</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p>Choose the relevant vehicle manufacturer to find out which towbars are suitable. We make towbars to fit the following car manufacturers:</p>
            </div>
        </div>
        <div class="row">
            @php
                // Count
                $count = count($manufacturers);
            @endphp
            @foreach ($manufacturers as $key => $manufacturer)

                <?php $key++ ?>

                @if($key%12 === 1)
                    <div class="row lazy-load-images item-list is-flex">
                @endif

                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4 item-wrapper" data-number="{{ $key }}">

                    <div class="item">
                        <div class="item-body">

                            <div class="manufacturer-logo">
                                <a href="{{ route('towbars.model_list', $manufacturer->slug) }}">
                                    <img src="" data-src="{{ image_load($manufacturer->logoUrl) }}" alt="Towbars for {{ $manufacturer->manufacturerName }}" >

                                    <h3 class="title-small">{{ $manufacturer->manufacturerName }}</h3>
                                    <p>Towbars</p>
                                </a>
                            </div>
                        </div><!-- /.item-body -->
                    </div><!-- /.item -->
                </div>

                @if($key%12 === 0 && $key > 0 || $key == $count)
                    </div><!-- .row -->
                @endif
            @endforeach
        </div>
    </div>
</div>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            var windowWidth = $(window).width();
            if(windowWidth <= 768) {
                $('.item-wrapper').each(function(index) {
                    if($(this).data('number') >= 7) {
                        $(this).css('display', 'none');
                    }
                });
            }

            $(window).on('resize', function() {
                windowWidth = $(window).width();

                if(windowWidth <= 768) {
                    $('.item-wrapper').each(function(index) {
                        if($(this).data('number') >= 7) {
                            $(this).css('display', 'none');
                        }
                    });
                } else {
                    $('.item-wrapper').each(function(index) {
                        $(this).css('display', 'flex');
                    });
                }
            });
        });
    </script>
@endsection
