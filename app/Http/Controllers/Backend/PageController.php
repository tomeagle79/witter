<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PageStoreRequest;
use App\Models\Page;
use App\Models\Image as ImageModel;
use App\Models\UrlSlug;
use App\Events\Admin\ImageStored;
use Auth;
use Datatables;
use Hash;
use View;
use DB;
use URL;
use Input;

class PageController extends Controller
{
    /**
     * Instantiate a new instance.
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.index');
    }

    /**
     * Return the datatable for pages
     *
     * @return Datatables
     */
    public function indexDatatable()
    {
        $pages = Page::select(['id', 'title', DB::raw('IF(published=1, "Yes", "No") as published'), 'url_slug_id', 'created_at', 'updated_at'])->get();

        return Datatables::of($pages)
            ->addColumn('edit', '<a href="{{ URL::backend(\'pages/edit/\'. $id) }}" class="btn btn-success">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'pages/delete/\'. $id) }}" class="btn btn-danger" data-confirm="Are you sure you want to delete this user?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PageStoreRequest $request)
    {
        // create page slug
        $url_slug = new UrlSlug;
        $url_slug->url_slug = str_slug($request->url_slug,'-');
        $url_slug->url_type = 'page';
        $url_slug->save();

        // save the page
        $page = new Page();
        $page->title = $request->title;
        $page->meta_title = $request->meta_title;
        $page->meta_description = $request->meta_description;
        $page->url_slug_id = $url_slug->id;
        $page->content = $request->content;
        $page->published = (empty($request->published) ? 0 : 1);

        // store the image
        $file = Input::file('file');

        if(!is_null($file)) {
            $filename = strtolower(str_random(40)).'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/img/pages/', $filename);

            // save the image
            $image = new ImageModel();
            $image->path = '/img/pages/';
            $image->filename = $filename;
            $image->save();

            $page->image_id = $image->id;
        }

        // wrap in transaction, for graceful fail
        DB::transaction(function() use ($page, $request) {

            // save the page
            $page->save();
        });

        return redirect(URL::backend('pages'))->with('success', '<strong>Success!</strong> Page created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);

        return View::make('backend.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageStoreRequest $request, $id)
    {
        // save the page
        $page = Page::findOrFail($id);
        $page->title = $request->title;
        $page->meta_title = $request->meta_title;
        $page->meta_description = $request->meta_description;
        $page->content = $request->get('content');
        $page->published = (empty($request->published) ? 0 : 1);

        $file = $request->file('file');

        // if there is an image
        if(!is_null($file)) {

            // delete the old image from rackspace and the database
            if(!is_null($page->image)) {
                $page->image->forceDelete();
            }

            // store the new image
            $filename = strtolower(str_random(40)).'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/img/pages/', $filename);

            // save the image
            $image = new ImageModel();
            $image->path = '/img/pages/';
            $image->filename = $filename;
            $image->save();

            // update the page
            $page->image_id = $image->id;

        }
        // Save the page
        $page->save();

        // update page slug
        $url_slug = UrlSlug::findorFail($page->url_slug_id);
        $url_slug->url_slug = str_slug($request->url_slug,'-');
        $url_slug->save();

        return redirect(URL::backend('pages'))->with('success', '<strong>Success!</strong> Page updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        $page->delete();

        return redirect(URL::backend('pages'))->with('message', '<strong>Success!</strong> Page deleted.');
    }

    /**
     * Restore the specified resource
     */
    public function restore($id)
    {
        $page = Page::withTrashed()->findOrFail($id);
        $page->restore();

        return redirect(URL::backend('pages'))->with('success', '<strong>Success!</strong> Page restored.');
    }
}
