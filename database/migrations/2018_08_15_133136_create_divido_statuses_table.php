<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDividoStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divido_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_reference')->nullable()->index();
            $table->string('status')->index();
            $table->string('proposal_id')->nullable()->index();
            $table->string('application_id')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divido_statuses');
    }
}
