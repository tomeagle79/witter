@extends('layouts.frontend')

@section('title')
	{!! !empty($career->meta_title) ? $career->meta_title : $career->title !!}
@stop

@section('heading')
	Careers
@stop

@section('meta_description')
	{!! $career->meta_description !!}
@stop

@section('content')

	<div class="container">
		<div class="row">

			<div class="col-sm-8 col-md-9">
				<article>
					@if(!is_null($career->image))

						<img src="{{URL('imagecache', ['template' => 'original', 'filename' => $career->image->filename])}}" alt="" />

					@endif

					<h1 class="no-top-margin">{!! $career->title !!}</h1>

					<p>
						@if(isset($career->contract))
							<strong>Contract:</strong> {!! $career->contract !!}
						@endif
						@if(isset($career->salary))
							&#124; <strong>Salary:</strong> {!! $career->salary !!}
						@endif
					</p>
					
					<div class="blog-copy">
						{!! $career->description !!}
					</div>
					
				</article>
			</div> <!-- col-md-9 -->

			<div class="col-sm-4 col-md-3">

				@include('partials.sidebar-search')

				@include('partials.sidebar-findlink-secondary')

			</div> <!-- .col-md-3 -->

		</div> <!-- .row -->
	</div> <!-- .container -->
@stop
