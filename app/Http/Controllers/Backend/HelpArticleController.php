<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\HelpArticleStoreRequest;
use App\Models\HelpArticle;
use App\Models\HelpCategory;
use App\Models\Image as ImageModel;
use Datatables;
use DB;
use Illuminate\Http\Request;
use URL;
use View;

class HelpArticleController extends Controller
{
    /**
     * Instantiate a new instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.help_articles.index');
    }

    /**
     * Return the datatable for help_articles
     *
     * @return Datatables
     */
    public function indexDatatable()
    {
        $help_articles = HelpArticle::select(['id', 'title', DB::raw('IF(featured=1, "Yes", "No") as featured, IF(published=1, "Yes", "No") as published'), 'created_at', 'updated_at'])->get();

        return Datatables::of($help_articles)
            ->addColumn('edit', '<a href="{{ URL::backend(\'help_articles/edit/\'. $id) }}" class="btn btn-success">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'help_articles/delete/\'. $id) }}" class="btn btn-danger" data-confirm="Are you sure you want to delete this user?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(HelpCategory $blogCategory)
    {
        $categories = $blogCategory->get();
        return view('backend.help_articles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(HelpArticleStoreRequest $request)
    {
        // create the blog post
        $help = new HelpArticle();
        $help->title = $request->title;
        $help->meta_title = $request->meta_title;
        $help->meta_description = $request->meta_description;
        $help->url_slug = str_slug($request->url_slug, '-');
        $help->content = $request->content;
        $help->template_name = $request->template_name;
        $help->published = (empty($request->published) ? 0 : 1);

        // store the image
        $file = $request->file('file');

        if (!is_null($file)) {
            $filename = strtolower(str_random(40)) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/img/help-articles/', $filename);

            // save the image
            $image = new ImageModel();
            $image->path = '/img/help-articles/';
            $image->filename = $filename;
            $image->save();

            // update the help
            $help->image_id = $image->id;
        }

        // wrap in transaction, for graceful fail
        DB::transaction(function () use ($help, $request) {

            // save the blog post
            $help->save();

            // categories
            $help->categories_sync($request->category_ids);
        });

        return redirect(URL::backend('help_articles'))->with('success', '<strong>Success!</strong> Add Help &amp; Advice Article created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $help = HelpArticle::findOrFail($id);
        $categories = HelpCategory::get();
        return View::make('backend.help_articles.edit', compact('help', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HelpArticleStoreRequest $request, $id)
    {
        // save the help
        $help = HelpArticle::findOrFail($id);
        $help->title = $request->title;
        $help->meta_title = $request->meta_title;
        $help->meta_description = $request->meta_description;
        $help->url_slug = str_slug($request->url_slug, '-');
        $help->content = $request->get('content');
        $help->template_name = $request->template_name;
        $help->published = (empty($request->published) ? 0 : 1);

        // update categories
        $help->categories_sync($request->category_ids);

        $file = $request->file('file');

        // if there is an image
        if (!is_null($file)) {

            // delete the old image from rackspace and the database
            if (!is_null($help->image)) {
                $help->image->forceDelete();
            }

            // store the new image
            $filename = strtolower(str_random(40)) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/img/help-articles/', $filename);

            // save the image
            $image = new ImageModel();
            $image->path = '/img/help-articles/';
            $image->filename = $filename;
            $image->save();

            // update the help
            $help->image_id = $image->id;
        }

        // Save the blog
        $help->save();

        return redirect(URL::backend('help_articles'))->with('success', '<strong>Success!</strong> Add Help &amp; Advice Article updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $help = HelpArticle::findOrFail($id);
        $help->delete();

        return redirect(URL::backend('help_articles'))->with('message', '<strong>Success!</strong> Add Help &amp; Advice Article deleted.');
    }

    /**
     * Restore the specified resource
     */
    public function restore($id)
    {
        $help = HelpArticle::withTrashed()->findOrFail($id);
        $help->restore();

        return redirect(URL::backend('help_articles'))->with('success', '<strong>Success!</strong> Add Help &amp; Advice Article restored.');
    }
}
