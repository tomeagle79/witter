
<div class="container">
	<div class="row">
		<div class="col-sm-4 col-md-3">

		    @include('partials.sidebar-faqs-menu')
			
			@include('partials.sidebar-search')

			@include('partials.sidebar-findlink-secondary')

		</div> <!-- .col-md-3 -->
		<div class="col-sm-8 col-md-9">

			<div class="row">

				<h2 class="border-bottom">
					{{ empty($faq_category->title)? 'All frequently asked questions' : $faq_category->title }}
				</h2>

				@include('faqs.partials.list-faqs')

			</div> <!-- .row -->
		</div> <!-- .col-md-9 -->
	</div> <!-- .row -->
</div> <!-- .container -->
