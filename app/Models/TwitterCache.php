<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterCache extends Model
{
    //
    protected $table = 'twitter_cache';
}
