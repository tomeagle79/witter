@if(is_array($products) && !empty($products) || is_object($products) && $products->count() > 0)
gtag('event', 'view_item_list', {
    'items': [
    @foreach($products as $product)
    {
        @if($category == 'Towbars')
        'name': '{{ str_replace("'", '', $product->title) }}',
        'id': '{{ $product->towbarPartNo }}',
        'price': '{{ $product->price }}',
        @if(!empty($product->brandName))
        'brand': '{{ $product->brandName }}',
        @endif
        @if(!empty($category))
        'category': '{{ $category }}',
        @endif
        'list_name': '{{ $list_name }}',
        'list_position': {{ $loop->iteration + (($page - 1) * $per_page)  }}
        @endif


        @if($category == 'Trackers')
        'name': '{{ str_replace("'", '', $product->title) }}', 
        'id': '{{ $product->partNo }}',
        'price': '{{ $product->displayPrice }}',
        @if(!empty($product->brandName))
        'brand': '{{ $product->brandName }}',
        @endif
        @if(!empty($category))
        'category': '{{ $category }}',
        @endif
        'list_name': '{{ $list_name }}',
        'list_position': {{ $loop->iteration + (($page - 1) * $per_page)  }}
        @endif    


        @php 
            $arr = [
                'Roof Systems/Accessories',
                'Roof Systems/By Manufacturer',
                'Roof Systems/Roof Bars',
                'Roof Systems/Roof Racks',
                'Electrical Kits',
                'Bike Racks',
                'Accessories'
            ];
        @endphp

        @if(in_array($category, $arr))
        'name': '{{ str_replace("'", '', $product->title) }}',
        'id': '{{ $product->partNo }}',
        'price': '{{ $product->price }}',
        @if(!empty($product->brandName))
        'brand': '{{ $product->brandName }}',
        @endif
        @if(!empty($category))
        'category': '{{ $category }}',
        @endif
        'list_name': '{{ $list_name }}',
        'list_position': {{ $loop->iteration + (($page - 1) * $per_page)  }}
        @endif    
    }@if(!$loop->last), @endif
    @endforeach
    ]
});
@endif