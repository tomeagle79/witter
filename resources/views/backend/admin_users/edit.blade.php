@extends('layouts.backend')

@section('title')
	Edit Admin User
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/admin_users/partials/_form', ['submit_text' => 'Edit User', 'type' => 'edit'])
	</form>

@stop
