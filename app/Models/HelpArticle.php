<?php

namespace App\Models;

use App\Traits\Published;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpArticle extends Model
{
    use SoftDeletes;

    use Published;

    /**
     * Relationship to image
     */
    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    /**
     * Relationship to categories
     */
    public function categories($trashed = false)
    {
        if ($trashed == 'withTrashed') {
            return $this->belongsToMany('App\Models\HelpCategory', 'help_category_help_articles')->withTimestamps();
        } elseif ($trashed == 'onlyTrashed') {
            return $this->belongsToMany('App\Models\HelpCategory', 'help_category_help_articles')->whereNotNull('help_category_help_articles.deleted_at')->withTimestamps();
        } else {
            return $this->belongsToMany('App\Models\HelpCategory', 'help_category_help_articles')->whereNull('help_category_help_articles.deleted_at')->withTimestamps();
        }
    }

    /**
     * Update relationship to categories
     */
    public function categories_sync($category_ids)
    {
        // categories
        $existing_category_ids = $this->categories()->pluck('help_category_id')->all();
        $trashed_category_ids = $this->categories('onlyTrashed')->pluck('help_category_id')->all();

        if (is_array($category_ids)) {

            foreach ($category_ids as $category_id) {

                if (in_array($category_id, $trashed_category_ids)) {
                    $this->categories()->updateExistingPivot($category_id, ['deleted_at' => null]);
                } elseif (!in_array($category_id, $existing_category_ids)) {
                    $this->categories()->attach($category_id);
                }
            }

            foreach ($existing_category_ids as $category_id) {
                if (!in_array($category_id, $category_ids)) {
                    $this->categories()->updateExistingPivot($category_id, ['deleted_at' => date('YmdHis')]);
                }
            }
        } else {
            foreach ($existing_category_ids as $category_id) {
                $this->categories()->updateExistingPivot($category_id, ['deleted_at' => date('YmdHis')]);
            }
        }
    }

    /**
     * Scope for latest help article
     */
    public function scopeLatest($query)
    {
        return $query->orderBy('created_at', 'desc')->first();
    }
}
