@extends('layouts.frontend')

@section('title', 'Checkout')

@section('heading')
    Checkout
@stop

@section('meta_description')
    Checkout
@stop

@section('content')

<div class="container checkout">

    <div class="row">
        <div class="col-sm-8 col-md-9">

            <div class="section">
                <a href="{{ route('checkout') }}"><h2>Cardholder <span class="slim">Details</span></h2></a>
            </div> <!-- .section -->

            <div class="section">
                <a href="{{ route('checkout.section2') }}"><h2>Delivery <span class="slim">Details</span></h2></a>
            </div> <!-- .section -->

            <div class="section">

                <h2>Vehicle <span class="slim">Details</span></h2>

                {!! Form::open(['route' => 'checkout.sectionVehicleSubmit', 'class' => 'form-horizontal']) !!}

                   @if(!empty($cart['contents']))
                        @foreach($cart['contents'] as $item)

                            @if($item['details']->regAndColourRequired == true)

                                <input type="hidden" name="{{ 'vehicle[' . $item['cart_item_id'] .  '][partNo]' }}" value="{{ $item['partNo'] }}">

                                <p><strong>{{ $item['details']->title }}</strong> will be fitted to the following vehicle</p>

                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Registration <br>number *</label>
                                    <div class="col-sm-10">
                                        {!! Form::text(("vehicle[" . $item['cart_item_id'] . "][registration_no]"),
                                            (empty($checkout['sectionVehicle'][$item['cart_item_id']]['registration_no']) ? null : $checkout['sectionVehicle'][$item['cart_item_id']]['registration_no']),
                                            ['required', 'class'=>'form-control']) !!}
                                    </div> <!-- .col -->
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Vehicle <br>colour *</label>
                                    <div class="col-sm-10">
                                        {!! Form::text( 'vehicle[' . $item['cart_item_id'] .  '][vehicle_colour]',
                                            (empty($checkout['sectionVehicle'][$item['cart_item_id']]['vehicle_colour']) ? null : $checkout['sectionVehicle'][$item['cart_item_id']]['vehicle_colour']),
                                            ['required', 'class'=>'form-control']) !!}
                                    </div> <!-- .col -->
                                </div>

                            @endif

                        @endforeach
                    @endif

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right">Continue</button>
                    </div>

                {!! Form::close() !!}

            </div> <!-- .section -->

            <div class="section">
                <h2>Payment <span class="slim">Options</span></h2>
            </div> <!-- .section -->

            <div class="section">
                <h2>Order <span class="slim">Complete</span></h2>
            </div> <!-- .section -->

        </div> <!-- .col -->
        <div class="col-sm-4 col-md-3">

            @include('partials.sidebar-findlink-secondary')

        </div> <!-- .col-md-3 -->
    </div>
</div>
@stop
