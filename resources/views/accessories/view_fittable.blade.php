@extends('layouts.frontend')

@section('title', $accessory->title)

@section('body-class', 'accessory_fitted body_bg_box')

@section('heading')
    {{ $accessory->title }}
@stop

@section('meta_description')
    Witter Towbars, {{ $accessory->title }}
@stop

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css">
@stop

@section('content')
<div class="grey-block-background">
	<div class="container" itemscope itemtype="http://schema.org/Product">
		<div class="row">

            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="main-image">
                    <img src="{{ image_load($accessory->imageUrl) }}" alt="{{ $accessory->title }}" itemprop="image">
                </div> <!-- .main-image -->

            </div><!-- /.col-xs-12 visible-xs-sm -->

			<div class="col-sm-6 col-sm-push-6">

                @if($accessory->isFittable)
                    @include('partials.sidebar-webfit')
                @endif
                <div class="main-details">
                    <form action="{{ route('add_to_cart') }}" method="post" id="order-form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="main-details--title">
                            <h2 itemprop="name">{{ $accessory->title }}</h2>
                        </div><!-- /.main-details--title -->

                        @if(!$accessory->fitOnly)
                            <div class="main-details--price">
                                &pound;<span>{{ price($accessory->price) }}</span>
                            </div><!-- /.main-details--price -->
                        @endif

                        <div class="main-details--fitting">

                            @if($accessory->fitOnly)
                                <p class="blue-text">This product must be professionally installed</p>
                            @else
                                <p>Do you require fitting?</p>
                            @endif

                            <ul class="row">
                                <li class="col-md-6">Fitted at a time and place to suit you</li>
                                <li class="col-md-6">All labour is guaranteed for a minimum of 1 year</li>
                            </ul>

                        </div><!-- /.main-details--fitting -->

                        <div class="fitting">

                            <div class="fitting--true">
                                <input type="radio" name="type" value="accessory-fitted" id="fitting_yes" @if($accessory->fitOnly) checked="checked" @endif>
                                <label for="fitting_yes" class="row">
                                    @if($accessory->fitOnly)
                                    <span class="text col-md-12">
                                    @else
                                    <span class="text col-xs-12 col-md-8">
                                    @endif
                                        @if($accessory->fitOnly)
                                            <strong>FULLY FITTED BY AN APPROVED PARTNER</strong><br>
                                            (I agree to Witter's <a href="/terms-and-conditions" target="_blank">Terms &amp; Conditions</a>)
                                        @else

                                            <strong>Yes Please</strong> <br>Fully fitted by one of our approved partners<br>
                                            (I agree to Witter's <a href="/terms-and-conditions" target="_blank">Terms &amp; Conditions</a>)
                                        @endif
                                    </span>
                                    @if(!$accessory->fitOnly)
                                        <span class="fitting-cost col-xs-12 col-md-4 text-right">+£{{ price($accessory->fittingPrice) }}</span>
                                    @endif
                                </label>
                            </div><!-- /.fitting--true-->

                            <div class="fitting--postcode">

                                <p>
                                    Enter your postcode to find your nearest fitter, and view available booking dates.
                                </p>

                                <div class="row">
                                    <div class="col-lg-8 col-md-7 col-sm-6">
                                        <input type="text" name="postcode" id="postcode" value="" class="form-control" placeholder="Postcode">
                                    </div> <!-- .col-md-9 -->
                                    <div class="col-lg-4 col-md-5 col-sm-6">
                                        <button type="button" class="btn btn-primary btn-block btn-primary-double-line" id="find-fitter">Find Your<br>Fitter</button>
                                    </div> <!-- .col-md-3 -->
                                </div> <!-- .row -->

                                <div class="fitting--postcode-messages">
                                    @if(empty($appointment_exists))

                                        <div class="alert alert-danger" role="alert" id="no-postcode" style="display: none;">
                                            <p>Please enter a postcode</p>
                                        </div>

                                        <div class="alert alert-danger" role="alert" id="error" style="display: none;">
                                            <p></p>
                                        </div>

                                        <div class="alert alert-info" role="alert" id="loading" style="display: none;">
                                            <p>Loading...</p>
                                        </div>

                                        <div class="alert alert-info" role="alert" id="loading" style="display: none;">
                                            <p>Loading...</p>
                                        </div>

                                        <div class="alert alert-info" role="alert" id="reload" style="display: none;">
                                            <p>
                                                If you selected the wrong dates <a href="">start the process again</a>.
                                            </p>
                                        </div>

                                    @else
                                        <p>
                                            We simply add to the fitting to your existing appointment
                                        </p> <!-- .uppercase -->

                                        <small>
                                            @if(!empty($appointment_exists['fitterType']) &&  $appointment_exists['fitterType'] == 'mobile')
                                                <strong>Mobile fitting</strong> (they come to you)<br>
                                                <strong>Company</strong> {{ $appointment_exists['address'] }}<br>
                                            @else
                                                Fitting at <strong>{{ $appointment_exists['address'] }}</strong><br>
                                            @endif

                                            Fitting on <strong>{{ date('jS F', strtotime($appointment_exists['appointmentDate'])) }}</strong> at <strong>{{ $appointment_exists['appointmentTime'] }}</strong>
                                        </small>
                                    @endif

                                    <div id="selected-fitter"></div>

                                </div> <!-- .fitting--postcode-messages -->
                            </div><!-- /.fitting--postcode -->

                            @if(empty($accessory->fitOnly))
                                <div class="fitting--false">
                                    <input type="radio" name="type" value="accessory" id="fitting_no"><label for="fitting_no"><strong>No Thanks</strong></label>
                                </div><!-- /.fitting--false -->
                            @endif
                        </div><!-- /.fitting -->

                        <div class="purchase" itemprop="offers" itemscope itemtype="http://schema.org/Offer"  id="book-scrollto">
                            <div class="row">
                                <div class="col-md-8">

                                    <meta itemprop="priceCurrency" content="GBP" />
                                    <meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition"
                                    @if($accessory->isSellable)
                                        <link itemprop="availability" href="http://schema.org/InStock"/>
                                        <span class="availability availability-true">Available</span>
                                    @else
                                        <link itemprop="availability" href="http://schema.org/OutOfStock"/>
                                        <span class="availability">Out of stock</span>
                                    @endif

                                </div><!-- /.col-md-8 -->
                                <div class="col-md-4">
                                    <span class="total_prefix">Total Price Inc. VAT</span>
                                    <span class="total">&pound;<span itemprop="price">{{ price($accessory->price) }}</span></span>
                                </div><!-- /.col-md-4 -->
                            </div><!-- /.row -->

                            @if($accessory->isSellable)
                                <div class="row">

                                    <div class="col-xs-12">

                                        <div class="alert alert-danger" role="alert" id="fitting_required" style="display: none;">
                                            <p>Please select if you require fitting</p>
                                        </div><!-- /.alert alert-danger -->

                                        <div class="alert alert-danger" role="alert" id="fitting_appointment_required" style="display: none;">
                                            <p>Please enter your postcode above and select an appointment for your fitting</p>
                                        </div><!-- /.alert alert-danger -->

                                    </div><!-- /.col-xs-12 -->

                                    <div class="col-md-4">
                                        {{--
                                            Just allow the user to select one item at a time for now.
                                            Form::select('quantity', array_combine(array_merge(range(1, 10), range(10, 100, 10)), array_merge(range(1, 10), range(10, 100, 10))), null, ['class' => 'form-control', 'id' => 'quantity'])  --}}

                                    </div><!-- /.col-md-8 -->
                                    <div class="col-md-8">
                                        <button type="submit" class="btn btn-green btn-block btn-lg" id="order-btn">ADD TO BASKET</button>
                                    </div><!-- /.col-md-4 -->
                                </div><!-- /.row -->
                            @else
                                <h4>Out of stock</h4>
                                <p>This item is currently out of stock</p>
                            @endif

                            <input type="hidden" name="price" value="{{ $accessory->price }}">
                            <input type="hidden" name="fittingPrice" value="{{ $accessory->fittingPrice }}">

                            <input type="hidden" name="slug" id="fld_slug" value="{{ $accessory->partNo }}">
                            <input type="hidden" name="registrationDate" id="fld_registration_date" value="{{ $registration_date ?? '' }}">
                             <input type="hidden" name="fitterFee" id="fld_fitterFee" value="0">

                            @if(empty($appointment_exists))
                                <input type="hidden" name="partNo" id="fld_partNo" value="{{ $accessory->partNo }}">
                                <input type="hidden" name="appointmentDate" id="fld_appointmentDate" value="">
                                <input type="hidden" name="appointmentTime" id="fld_appointmentTime" value="">
                                <input type="hidden" name="appointmentId" id="fld_appointmentId" value="">
                                <input type="hidden" name="partnerId" id="fld_partnerId" value="">
                                <input type="hidden" name="address" id="fld_address" value="">
                                <input type="hidden" name="fitterType" id="fld_fitterType" value="">
                            @else
                                <input type="hidden" name="partNo" id="fld_partNo" value="{{ $accessory->partNo }}">
                                <input type="hidden" name="appointmentDate" id="fld_appointmentDate" value="{{ $appointment_exists['appointmentDate'] }}">
                                <input type="hidden" name="appointmentTime" id="fld_appointmentTime" value="{{ $appointment_exists['appointmentTime'] }}">
                                <input type="hidden" name="appointmentId" id="fld_appointmentId" value="{{ $appointment_exists['appointmentId'] }}">
                                <input type="hidden" name="partnerId" id="fld_partnerId" value="{{ $appointment_exists['partnerId'] }}">
                                <input type="hidden" name="address" id="fld_address" value="{{ $appointment_exists['address'] }}">
                                <input type="hidden" name="fitterType" id="fld_fitterType" value="{{ $appointment_exists['fitterType'] }}">
                                <input type="hidden" name="postcode" id="fld_postcode" value="{{ $appointment_exists['customer_postcode'] }}">
                            @endif


                        </div><!-- /.purchase -->
                    </form>
                </div><!-- /.main-details -->

                @if(!empty($accessory->keyFeatures))

                    <h3>KEY FEATURES</h3>

                    <ul class="info-list">

                    @foreach($accessory->keyFeatures as $feature)

                        <li class="col-sm-6">{{ $feature->featureText }}</li>

                    @endforeach

                    </ul>
                @endif

                @if(isset($accessory->videoId) && $accessory->videoId !== "")
                    <div class="row hidden-xs hidden-sm">
                        <div class="col-xs-12">
                            <div class="youtube-video">
                                <h4>Watch the video</h4>
                                <a data-fancybox target="_blank" href="https://www.youtube.com/embed/{{ $accessory->videoId }}" class="clearfix">
                                    <div class="play-wrapper">
                                        <img src="https://img.youtube.com/vi/{{ $accessory->videoId }}/maxresdefault.jpg" alt="Watch the video">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif

            </div> <!-- .col-md-6 -->

            <div class="col-sm-6 col-sm-pull-6 hidden-xs">

                <div class="main-image">
                    <img src="{{ image_load($accessory->imageUrl) }}" alt="{{ $accessory->title }}" itemprop="image">
                </div> <!-- .main-image -->

            </div><!-- /.col-sm-6 col-sm-pull-6 hidden-xs -->

            <div class="col-sm-6 col-sm-pull-6">

                @if(!empty($accessory->attributes))

                    <h3>PRODUCT SPECIFICATION</h3>

                    <div class="panel panel-product-info">
                        <div class="panel-heading active">

                                <a data-toggle="collapse" data-parent="#accordion" href="#attributes"><h4 class="panel-title">KEY INFORMATION</h4></a>

                        </div><!-- /.panel-heading -->
                        <div id="attributes" class="panel-collapse collapse in">

                            <div class="product-info-rows">
                                @foreach($accessory->attributes as $attribute)

                                    <div class="row">
                                        <div class="col-xs-6">
                                            {{ $attribute->attributeName }}
                                        </div><!-- /.col-xs-6 -->

                                        <div class="col-xs-6 text-right">
                                            {{ $attribute->attributeValue }}
                                        </div><!-- /.col-xs-6 -->
                                    </div><!-- /.row -->

                                @endforeach
                            </div><!-- /.info-list -->
                        </div><!-- /.panel-collapse -->
                    </div><!-- /.panel -->

                @endif


                <h3>PRODUCT OVERVIEW</h3>

                <div class="details" itemprop="description">{!! nl2br($accessory->details) !!}</div> <!-- .details -->

                <h3>Additional Information</h3>

                {!! nl2br($accessory->moreDetails) !!}

            </div><!-- /.col-sm-6 col-sm-pull-6 -->


		</div> <!-- .row -->

        {{--
        <div class="row">
            <div class="col-xs-12">
                <h2>YOU MAY ALSO BE INTERESTED IN</h2>

            </div><!-- /.col-xs-12 -->

        </div><!-- /.row -->
        --}}

	</div> <!-- .container -->
</div><!-- /.grey-block-background -->

<div class="modal fade find-your-fitter" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
                <h4 class="modal-title">Select a fitting location</h4>
            </div> <!-- .modal-header -->
            <div class="modal-body">

            </div> <!-- .modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@stop

@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js"></script>

    <script>
        @include('partials.analytics.product-view', [
            'product' => $accessory,
            'category' => 'Accessories'
        ])

        $(document).ready(function(){
            var partNo = '{{ $accessory->partNo }}';

            calculatePrice();

            $("input[name='type']").change(function(){

                $( '#fitting_required' ).fadeOut('slow');

                calculatePrice();
            });

            // On submit form check if they
            $( ".order-btn" ).click( function( event ) {

                var accessory_type = $( 'input[name=type]:checked' ).val();

                if (typeof accessory_type === 'undefined'){

                    event.preventDefault();

                    $( '#fitting_required' ).fadeIn('slow');
                }

                // Check if the product is an 'accessory-fitted' we need an appointment selecting.
                if (accessory_type === 'accessory-fitted' && $( '#fld_appointmentDate' ).val() == ''){

                    event.preventDefault();

                    $( '#fitting_appointment_required' ).fadeIn('slow');
                }
            });


            // Catch enter press
            $('#postcode').on('keypress', function(e) {
                var code = e.keyCode || e.which;
                if(code == 13) { //Enter keycode
                    $('#find-fitter').click();
                    return false;
                }
            });

            $('#find-fitter').on('click', function() {
                var postcode = $('#postcode').val();


                $('#no-postcode, #no-kit, #loading, #error').hide();

                if(!postcode.length) {
                    $('#no-postcode').fadeIn('slow');
                } else {
                    $('#loading').fadeIn('slow');

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('appointments.ajax_list_accessory_fitters')}}",
                        data: {
                            'postcode': postcode,
                            'partNo': partNo,

                        },
                        success: function(result){
                            $('#loading').fadeOut('slow');

                            if(result.success == false) {
                                $('#error p').html(result.errorMessage);
                                $('#error').fadeIn('slow');
                            } else {
                                // Fitting process has begun so readonly everything - client request
                                $('input[type=radio]:not(:checked)').prop('disabled', true);
                                $('#reload').fadeIn('slow');

                                $('#modal .modal-content .modal-body').html(result.modal);
                                $('#modal').modal('show');

                                // If the yes radio button has not been check then select it.
                                if(!$('#fitting_yes').is(':checked')) {
                                    $("#fitting_yes").prop("checked", true);
                                }

                                calculatePrice();
                            }
                        },
                        error: function(){
                            alert('Sorry, something went wrong');
                        }
                    });
                }
            });
        });



        function calculatePrice()
        {

            var base = parseFloat({{ $accessory->price }});

            // Check if the fitting radio button has been selected.
            if($('#fitting_yes').is(':checked')) {

                // Add the fitting cost.
                var total = base + parseFloat({{ $accessory->fittingPrice }});

            }else{

                var total = base;
            }

            total = total + parseFloat($('#fld_fitterFee').val());

            total = total.toFixed(2);

            $('.purchase .total span').html(total);
        }
    </script>
@stop


