@extends('layouts.email')

@section('content')

    You received a product enquiry from Witter:

    <p>
    Product: {{ $enquiry_regarding }}
    </p>

    <p>
    Email send from website URL: {{ $website_url }}
    </p>

    <p>
    Name: {{ $name }}
    </p>

    <p>
    Phone: {{ $phone }}
    </p>

    <p>
    Postcode: {{ $postcode }}
    </p>

    <p>
    {{ $email }}
    </p>

    <p>
    {{ $user_message }}
    </p>

@stop
