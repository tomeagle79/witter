<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Email</title>
    <style>
		body {
			font-family: Arial, Helvetica, sans-serif;
			font-size: 14px;
			color: #666666;
		}

		h1, h2, h3, h4, h5 {
			color: #008372;
			font-weight: normal;
		}

		a {
			color: #C01984;
		}

		table.border {
			width: 100%;
			border: solid 1px #cccccc;
			border-collapse: collapse;
		}
		table.border td, table.border th {
			border: solid 1px #cccccc;
			padding: 5px;
		}
    </style>
</head>

<body>
<center>
	<table width="600" cellpadding="10" cellspacing="0" align="center">
		<thead align="left" style="text-align: left: font-size: 13px;">
			<tr>
				<td width="1">&nbsp;</td>

				<td>
    				<a href="{!! route('home') !!}" font-color="white" style="color: #ffffff;"><img src="{!! asset('assets/img/logo-white-curve.png') !!}" alt="Witter Towbars" style="border: 0;" /></a>
    			</td>

    			<td width="1">&nbsp;</td>
			</tr>
			<tr>
				<td width="1">&nbsp;</td>

				<td>

    			</td>

    			<td width="1">&nbsp;</td>
			</tr>
		</thead>
		<tbody align="left" style="text-align: left; font-size: 14px;">
			<tr>
				<td width="1">&nbsp;</td>

				<td font-color="#666666" style="color: #666666;">
    				@yield('content')
    			</td>

    			<td width="1">&nbsp;</td>
    		</tr>
    	</tbody>
    	<tfoot>
			<tr>
				<td colspan="3" style="font-size: 10px; text-align:center; color:#999;">Witter Towbars, Drome Road, Deeside Industrial Estate, Deeside Flintshire CH5 2NY</td>
			</tr>
    	</tfoot>
	</table>
</center>
</body>
</html>
