<div class="cart-item">

    <div class="row">
        <div class="col-xs-10">
            <h3>{{ $item['details']->title }}</h3>
        </div><!-- /.col-xs-12 -->
        <div class="col-xs-2">
            @if($edit)
                @if($item['type'] == 'towbar')
                    <a href="{{ route('cart.remove_towbar') }}" class="pull-right remove-link"><i class="icon-cross"></i></a>
                @else
                    <a href="{{ route('cart.remove', ['id' => $item['cart_item_id']]) }}" class="pull-right remove-link"><i class="icon-cross"></i></a>
                @endif
            @endif
        </div><!-- /.col -xs-2 -->
    </div><!-- /.row -->

    <div class="row">

        <div class="col-sm-2 hidden-xs">
            {{-- Display towbar image --}}
            @if(!empty($item['details']->imageUrls))
                @if(!empty($item['details']->imageUrls[0]))
                    <img src="{{ image_load($item['details']->imageUrls[0]) }}" alt="" width="100%">
                @endif
            @endif
            {{-- Display accessory image --}}
            @if(!empty($item['details']->imageUrl))
                <img src="{{ image_load($item['details']->imageUrl) }}" width="100%" alt="">
            @endif
        </div><!-- /.col-md-2 -->

        <div class="col-sm-10 col-xs-12">
            <hr class="dotted">
            <div class="row">
                @if($item['type'] == 'accessory-fitted' && isset($item['appointmentId']))

                    <div class="col-xs-12 product-details">

                        <div class="row">
                            <div class="col-md-8 col-xs-12 ">
                                <p @if(!empty($cart['new_appointment_required'])) class="strikethrough"@endif>
                                    <span>
                                    @if($item['fitterType'] == 'mobile')
                                        @if(empty($cart['new_appointment_required'])) <i class="icon-mobile-fitter"></i> @endif  Mobile fitting on <strong>{{ date('jS F', strtotime($item['appointmentDate'])) }}</strong> at <strong>{{ $item['appointmentTime'] }}</strong>
                                    @else
                                        @if(empty($cart['new_appointment_required'])) <i class="icon-garage-fitting"></i> @endif   Fitting on <strong>{{ date('jS F', strtotime($item['appointmentDate'])) }}</strong> at <strong>{{ $item['appointmentTime'] }}</strong>
                                    @endif
                                    </span>
                                </p>

                                @if(!empty($cart['new_appointment_required']))
                                    <p class="availability-message">Sorry, this date/time is no longer available</p>
                                @endif
                            </div><!-- /.col-md-8 -->

                            <div class="col-md-4 appointment-btn-wrapper">
                                @if(!empty($allow_date_change))
                                    <p>
                                        <a href="javascript:void(0);" id="new_appointment" class="btn btn-primary btn-primary-slim"
                                            data-address="{{ $item['address'] }}"
                                            data-partNo="{{ $item['partNo'] }}"
                                            data-fitter="{{ $item['partnerId'] }}"
                                            data-fitterType="{{ $item['fitterType'] }}"
                                            >Select new date</a>
                                        <input type="hidden" name="appointment_type" id="appointment_type" value="fitted_accessory" >
                                        <input type="hidden" name="partNo" value="{{ $item['partNo'] }}" >
                                        <input type="hidden" name="address" value="{{ $item['address']}}" >
                                        <input type="hidden" name="customer_postcode" value="{{ $item['customer_postcode'] }}" >
                                        <input type="hidden" name="removal_option" value="{{ $item['removal_option']}}" >
                                        <input type="hidden" name="removal_cost" value="{{ $item['removal_cost'] }}" >
                                    </p>
                                @endif
                            </div><!-- /.col-md-4 -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div id="appointment_not_changed" class="alert alert-help alert-help-appointment" style="display: none;"><p>The appointment slot you previously chose is no longer available.</p></div>
                        </div>
                        <hr class="dotted">

                        @if($item['fitterType'] != 'mobile')
                            <p>
                                Fitting at <strong>{{ $item['address'] }}</strong>
                            </p>
                            <hr class="dotted">
                        @endif

                        @if(isset($item['removal_option']) && $item['removal_option'] === 'true')
                            <p>
                                Existing caravan mover removal <strong>included</strong>
                            </p>
                            <hr class="dotted">
                        @endif

                    </div><!-- /.col-xs-12 -->
                @endif

                @if($item['type'] == 'towbar')
                    <div class="col-xs-12 product-details">

                        @if(!empty($item['vehicleDesc']))
                            <p>for {{ $item['vehicleDesc'] }}</p>
                            <hr class="dotted">
                        @endif

                        @if(!empty($item['towball_option_title']))
                            <p>+ {{ $item['towball_option_title'] }} (+&pound;{{ price($item['towball_option_price']) }})</p>
                        <hr class="dotted">
                        @endif

                        @if(!empty($item['electric_option_title']))
                            <p>
                                {{ $item['electric_option_title'] }} (+&pound;{{ price($item['electric_option_price']) }})

                            {{--@if($item['electric_option_kit_identifier'] == 'D13' || $item['electric_option_kit_identifier'] == 'D7')
                            <i class="icon-alert tooltip-icon" data-toggle="tooltip" data-html="true" data-placement="bottom"
                                title="<p>You might need a software update to activate additional benefits and safety features. You may need to pay an additional fee for the software update.</p>
                                "></i>
                            @endif--}}

                                @if($item['softwareUpgrade'])
                                <br>Software upgrade (+&pound;{{ number_format($item['softwareUpgradePrice'], 2) }})
                                @endif
                            </p>
                            <hr class="dotted">
                        @endif

                        @if($item['fitterType'] == 'self_fitted')

                            <p>
                                I have my own fitter.
                                <hr class="dotted">
                            </p>

                        @else

                        <div class="row">
                            <div class="col-md-8 col-xs-12 ">
                                <p @if(!empty($cart['new_appointment_required'])) class="strikethrough"@endif>
                                    <span>
                                    @if($item['fitterType'] == 'mobile')
                                        @if(empty($cart['new_appointment_required'])) <i class="icon-mobile-fitter"></i> @endif  Mobile fitting on <strong>{{ date('jS F', strtotime($item['appointmentDate'])) }}</strong> at <strong>{{ $item['appointmentTime'] }}</strong>
                                    @else
                                        @if(empty($cart['new_appointment_required'])) <i class="icon-garage-fitting"></i> @endif   Fitting on <strong>{{ date('jS F', strtotime($item['appointmentDate'])) }}</strong> at <strong>{{ $item['appointmentTime'] }}</strong>
                                    @endif
                                    </span>
                                </p>

                                @if(!empty($cart['new_appointment_required']))
                                    <p class="availability-message">Sorry, this date/time is no longer available</p>
                                @endif
                            </div><!-- /.col-md-8 -->
                            <div class="col-md-4 appointment-btn-wrapper">
                                @if(!empty($allow_date_change))
                                    <p>
                                        <a href="javascript:void(0);" id="new_appointment" class="btn btn-primary btn-primary-slim"
                                            data-address="{{ $cart['towbar']['address'] }}"
                                            data-partNo="{{ $cart['towbar']['partNo'] }}"
                                            data-variantTowbarId="{{ $cart['towbar']['variantTowbarId'] }}"
                                            data-vehicleDesc="{{ $cart['towbar']['vehicleDesc'] }}"
                                            data-fitter="{{ $cart['towbar']['partnerId'] }}"
                                            data-fitterType="{{ $cart['towbar']['fitterType'] }}"
                                            data-electric="{{ $cart['towbar']['electric_option'] }}"
                                            >Select new date</a>
                                        <input type="hidden" name="appointment_type" id="appointment_type" value="towbar" >
                                        <input type="hidden" name="partNo" value="{{ $cart['towbar']['partNo'] }}" >
                                        <input type="hidden" name="variantTowbarId" value="{{ $cart['towbar']['variantTowbarId'] }}" >
                                        <input type="hidden" name="towball_option" value="{{ $cart['towbar']['towball_option'] }}" >
                                        <input type="hidden" name="electric_option" value="{{ $cart['towbar']['electric_option'] }}" >
                                        <input type="hidden" name="softwareUpgrade" value="{{ $cart['towbar']['softwareUpgrade'] }}" >
                                        <input type="hidden" name="softwareUpgradePrice" value="{{ $cart['towbar']['softwareUpgradePrice'] }}" >
                                        <input type="hidden" name="address" value="{{ $cart['towbar']['address']}}" >
                                        <input type="hidden" name="customer_postcode" value="{{ $cart['towbar']['customer_postcode'] }}" >
                                    </p>
                                @endif
                            </div><!-- /.col-md-4 -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div id="appointment_not_changed" class="alert alert-help alert-help-appointment" style="display: none;"><p>The appointment slot you previously chose is no longer available.</p></div>
                        </div>
                        <hr class="dotted">

                        @endif

                        @if($item['fitterType'] != 'mobile' && $item['fitterType'] != 'self_fitted')
                            <p>
                                Fitting at <strong>{{ $item['address'] }}</strong>
                            </p>
                            <hr class="dotted">
                        @endif

                    </div><!-- /.col-xs-12 -->
                @endif

                <div class="col-xs-12">
                    <span class="price">
                        @if($item['type'] == 'towbar')
                            &pound;{{ price($item['final_price']) }}
                        @else
                            @if(empty($item['totalCost']))
                                &pound;{{ price($item['details']->price) }}
                            @else
                                &pound;{{ price($item['totalCost']) }}
                            @endif
                        @endif
                    </span><!-- /.pull-right -->
                </div><!-- /.col-xs-12 -->
            </div><!-- /.row -->

        </div><!-- /.col-sm-10 -->

    </div><!-- /.row -->

</div><!-- /.cart-item -->
