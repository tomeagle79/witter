@extends('layouts.frontend')

@section('title', $subcategory->categoryName ?? '')

@section('heading')
	{{ $subcategory->categoryName ?? '' }}
@stop

@section('meta_description')
	{{ $subcategory->metaDescription ?? '' }}
@stop

@section('content')
	<div class="container witter-listing">
		<div class="row">

			<div class="col-sm-8 col-md-9 col-md-push-3 accessory-listing">

				<h2 class="border-bottom no-top-margin">{{ $subcategory->categoryName ?? '' }}</h2>

                {{-- Check if Caravan Movers --}}
                @if($subcategory->slug == 'caravan-movers')
                    <div class="description">

                        <p>Caravan motor movers have revolutionised caravanning making it even more accessible to the public. Whether you don't have space to easily manoeuvre your caravan, or it's simply just too heavy, a Caravan mover can help you get on the road in no time.</p>
                    </div>
                @else
                    <div class="description">
                        <p>{!! nl2br((empty($subcategory->description) ? '' : $subcategory->description)) !!}</p>
                    </div>

                @endif

				<div class="row item-list flex-items">

                    @if(empty($accessories))
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            New products coming soon.
                        </div>
                    @else
                        @foreach($accessories as $accessory)

                            @include('partials.analytics.json_product_details', [
                                'product' => $accessory,
                                'list_name' => 'Category Results',
                                'category' => 'Accessories',
                                'position' => $loop->iteration
                            ])

                            <div class="col-xs-12 col-sm-6 col-md-4" itemscope itemtype="http://schema.org/Product">
                                <div class="item text-center">
                                    <div class="item-body">
                                        <a href="{!! route('accessories.multi', [encode_url($accessory->slug)]) !!}" class="product-link" data-product-id="{{ $accessory->partNo }}"><img itemprop="image" src="{{ image_load($accessory->imageUrl) }}" alt="" /></a>

                                        <a href="{!! route('accessories.multi', [encode_url($accessory->slug)]) !!}" class="product-link" data-product-id="{{ $accessory->partNo }}">
                                            <h3 class="list-title" itemprop="name">{!! $accessory->title !!}</h3>
                                        </a>
                                        <div itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
                                            <a href="{!! route('accessories.multi', [encode_url($accessory->slug)]) !!}" class="product-link" data-product-id="{{ $accessory->partNo }}">

                                                @php
                                                    $save = empty($accessory->retailPrice) ? 0 : $accessory->retailPrice - $accessory->displayPrice;
                                                @endphp

                                                @if($save > 0)

                                                    <span class="retail-price">RRP &pound;{{ price($accessory->retailPrice) }}</span>

                                                    <h3 class="price">Buy now for &pound;<span itemprop="lowPrice">{!! price($accessory->displayPrice) !!}</span></h3>

                                                    <span class="retail-price">Save &pound;{{ price($save) }}</span>
                                                @else
                                                    <h3 class="price">&pound;<span itemprop="lowPrice">{!! price($accessory->displayPrice) !!}</span></h3>
                                                @endif

                                            </a>
                                            <meta itemprop="priceCurrency" content="GBP" />
                                        </div>
                                    </div><!-- /.item-body -->
                                </div>
                            </div>

                        @endforeach
                    @endif

				</div> <!-- .row -->

                {{-- Check if Caravan Movers --}}
                @if($subcategory->slug == 'caravan-movers')
                    <div class="description">

                        <h4 class="grey-title">What is a Caravan Mover?</h4>
                        <p>The caravan mover bolts to the caravan chassis and drives its wheels by 12-volt power. They come with a small remote control that easily directs the caravan into position without breaking a sweat.</p>

                        <h4 class="grey-title">Why buy a Caravan Mover?</h4>

                        <ul><li>Ease: A caravan mover makes your life so much easier. Simply use the remote to effortlessly manoeuvre the caravan directly up to your hitch.</li>

                        <li>Restricted Space: If you only have limited space at home or on your pitch, the caravan mover makes it easy to manoeuvre the caravan into tight spaces.</li>

                        <li>Accessibility: The caravan mover takes all the stress and physical labour out of hitching up your caravan. Stand back and watch as the mover takes on all the hard work and your caravan glides into place.</li></ul>

                        <h4 class="grey-title">What do I need to consider? </h4>
                        <ul><li>Twin or Single Axle:  Consider if the caravan mover is designed to move a single or twin axle caravan. Our caravan movers are designed to be interchangeable between single and twin axle caravans to make things easier.</li>

                        <li>Weight: Weight comes into consideration in two aspects, the first is the weight of the caravan itself. Caravan mover manufacturers will stipulate the amount of weight a given mover can cope with moving, so make sure your chosen mover can take the weight of your caravan.</li>

                        <li>The second instance where weight must be considered is in the weight of the mover itself. The weight of the mover needs to be added to your payload so that your vehicle is not towing over its maximum towing capacity. Our range of caravan movers is the lightest on the market by 10kg allowing you to carry more of the things you really need for your holiday.</li>
                        </ul>

                        <h4 class="grey-title">Can I fit a caravan mover myself?</h4>
                        <p>We recommend having your Caravan mover fitted by a professional, that's why we offer a fully fitted price fitted by our approved UK Nationwide Fitting centre. Simply choose the caravan mover best suited for you, enter your postcode and choose your preferred fitting date. We will try our best to accommodate your chosen date and your mover can usually be fitted within 7 days of placing an order. Click here to learn more and find the perfect mover for you.</p>

                        <h4 class="grey-title">Caravan Movers Range</h4>
                        <p>The e-go caravan mover range is powered by e-go's exclusive Quattro® technology. Its intelligent modular system provides soft-start for precision moving in tight spaces. The e-go range can be used on single, twin and AWD installations and even allows retro-fit upgrades so a twin system can be adapted to an All-Wheel Drive at any point. This exclusive intelligent technology gives you ultimate flexibility and peace of mind.<p>
                    </div>
                @endif
			</div> <!-- .col-md-9 -->

			<div class="col-sm-4 col-md-3 col-md-pull-9">

				@include('partials.sidebar-partclass-menu')

				@include('partials.sidebar-search')

				@include('partials.sidebar-findlink-secondary')

			</div> <!-- .col-md-3 -->

		</div> <!-- .row -->
	</div> <!-- .container -->
@stop

@section('scripts')
	<script>
        @include('partials.analytics.list', [
            'products' => $accessories,
            'page' => 1,
            'per_page' => 999,
            'list_name' => 'Category Results',
            'category' => 'Accessories'
        ])
	</script>
@stop