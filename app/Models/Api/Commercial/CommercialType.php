<?php

namespace App\Models\Api\Commercial;

use App\Models\Api\ApiModel;

class CommercialType extends ApiModel
{
	const URI = "commercials/types";

	/**
	 * Get all types/categories
	 *
	 * @return Object
	 */
	static public function get_all()
	{
		$url = config('witter.api_base') . self::URI;
		$obj = self::get_request($url);
		
		return $obj;
	}

	/**
	 * Get all sub-types/categories
	 *
	 * @return Object
	 */
	static public function get_sub_categories($commercialTypeId)
	{
		$url = config('witter.api_base') . self::URI . '/'. $commercialTypeId .'/partcategories';
		$obj = self::get_request($url);
		
		return $obj;
	}
}