<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\FaqStoreRequest;
use App\Models\Faq;
use App\Models\FaqCategory;
use Datatables;
use DB;
use Illuminate\Http\Request;
use Input;
use URL;
use View;

class FaqController extends Controller
{
    /**
     * Instantiate a new instance.
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.faqs.index');
    }

    /**
     * Return the datatable for faqs
     *
     * @return Datatables
     */
    public function indexDatatable()
    {
        $faqs = Faq::select(['id', 'faq', DB::raw('IF(published=1, "Yes", "No") as published'), 'created_at', 'updated_at'])->get();

        return Datatables::of($faqs)
            ->addColumn('edit', '<a href="{{ URL::backend(\'faqs/edit/\'. $id) }}" class="btn btn-success">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'faqs/delete/\'. $id) }}" class="btn btn-danger" data-confirm="Are you sure you want to delete this user?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(FaqCategory $faqCategory)
    {
        $categories = FaqCategory::pluck('title', 'id');

        $category_id = null;

        return view('backend.faqs.create', compact('categories', 'category_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(FaqStoreRequest $request)
    {
        // create the faq
        $faq = new Faq();
        $faq->faq = $request->faq;
        $faq->meta_title = $request->meta_title;
        $faq->meta_description = $request->meta_description;
        $faq->url_slug = str_slug($request->url_slug, '-');
        $faq->answer = $request->answer;
        $faq->category_id = $request->category_id;
        $faq->published = (empty($request->published) ? 0 : 1);

        // store the image
        $file = Input::file('file');

        if (!is_null($file)) {
            $filename = strtolower(str_random(40)) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/img/faqs/', $filename);

            // save the image
            $image = new ImageModel();
            $image->path = '/img/faqs/';
            $image->filename = $filename;
            $image->save();

            $faq->image_id = $image->id;
        }

        // wrap in transaction, for graceful fail
        DB::transaction(function () use ($faq, $request) {

            // save the faq
            $faq->save();
        });

        return redirect(URL::backend('faqs'))->with('success', '<strong>Success!</strong> FAQ created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faq::findOrFail($id);

        $categories = FaqCategory::pluck('title', 'id');

        $category_id = $faq->category_id;

        return View::make('backend.faqs.edit', compact('faq', 'categories', 'category_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqStoreRequest $request, $id)
    {
        // save the faq
        $faq = Faq::findOrFail($id);
        $faq->faq = $request->faq;
        $faq->meta_title = $request->meta_title;
        $faq->meta_description = $request->meta_description;
        $faq->url_slug = str_slug($request->url_slug, '-');
        $faq->answer = $request->get('answer');
        $faq->category_id = $request->category_id;
        $faq->published = (empty($request->published) ? 0 : 1);
        $faq->save();

        $file = $request->file('file');

        // if there is an image
        if (!is_null($file)) {

            // delete the old image from rackspace and the database
            if (!is_null($faq->image)) {
                $faq->image->forceDelete();
            }

            // store the new image
            $filename = strtolower(str_random(40)) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/img/faqs/', $filename);

            // save the image
            $image = new ImageModel();
            $image->filename = $filename;
            $image->save();

            // update the faq
            $faq->image_id = $image->id;
            $faq->save();
        }

        return redirect(URL::backend('faqs'))->with('success', '<strong>Success!</strong> FAQ updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faq::findOrFail($id);
        $faq->delete();

        return redirect(URL::backend('faqs'))->with('message', '<strong>Success!</strong> FAQ deleted.');
    }

    /**
     * Restore the specified resource
     */
    public function restore($id)
    {
        $faq = Faq::withTrashed()->findOrFail($id);
        $faq->restore();

        return redirect(URL::backend('faqs'))->with('success', '<strong>Success!</strong> FAQ restored.');
    }
}
