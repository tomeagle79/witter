<?php

namespace App\Models;

use App\Traits\Published;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    use Published;

    /**
     * Relationship to image
     */
    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    /*
     * Get the page url slug
     */
    public function url_slug()
    {
        return $this->belongsTo('App\Models\UrlSlug');
    }

    /**
     * Model events
     */
    protected static function boot()
    {
        parent::boot();

        /**
         * Logic to run before delete
         */
        static::deleting(function ($page) {
            $page->url_slug()->delete();
        });

        /**
         * Logic to run before restore
         */
        static::restoring(function ($page) {
            $page->url_slug()->restore();
        });
    }
}
