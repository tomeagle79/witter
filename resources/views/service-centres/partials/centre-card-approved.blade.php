
<div class="card-approved">

    <h2 class="card-approved-title"><a href="{{ route('service-centres.show', $centre->centreSlug) }}">{!! $centre->centreName !!}</a></h2>
    <hr>

    @if(!empty($centre->address1)){{ $centre->address1 }}<br>@endif
    @if(!empty($centre->address2)){{ $centre->address2 }}<br>@endif
    @if(!empty($centre->address3)){{ $centre->address3 }}<br>@endif
    @if(!empty($centre->city)){{ $centre->city }}<br>@endif
    @if(!empty($centre->county)){{ $centre->county }}<br>@endif
    @if(!empty($centre->postcode)){{ $centre->postcode }}<br>@endif

    <a href="{{ route('service-centres.show', $centre->centreSlug) }}" class="more">View full details</a>
</div><!-- /.card-basic -->
