<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogCategory extends Model
{
	use SoftDeletes;

    /**
     * Relationship to posts
     */
    public function posts()
    {
        return $this->belongsToMany('App\Models\BlogPost', 'blog_category_blog_posts');
    }
}
