@extends('layouts.frontend')

@section('title', 'Order successful')

@section('body-class', 'body_bg_checkout')

@section('heading')
	Quote Sent
@stop

@section('meta_description')
    Quote Sent
@stop

@section('content')

<div class="container checkout-process">
    <div class="row">
        <div class="col-xs-12">
            <h1>Quote Sent</h1>
            <h3 class="tag-line">
                Thanks for your request, a quote has been sent to your email address.
            </h3><!-- /.tag-line -->
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->

    <div class="row">

        <div class="col-xs-12 col-md-9">

            <div class="section">
                <div class="delivery-notice">
                    @if(empty($fitting_type) || (!empty($fitting_type) && $fitting_type == 'self_fitted'))
                        <i class="icon-van"></i><span>The following item(s) are in your quote</span>
                    @else
                        @if($fitting_type == 'mobile')
                            <i class="icon-mobile-fitter"></i>
                        @else
                            <i class="icon-garage-fitting"></i>
                        @endif
                        <span>The following item(s) will be delivered to your fitter:</span>
                    @endif
                </div><!-- /.delivery-notice -->

                <div class="mobile-box">
                    @if(!empty($cart['towbar']))
                        @include('cart.partials.cart_item', ['edit' => false, 'item' => $cart['towbar']])
                        @if(!empty($cart['contents']))
                            <hr>
                        @endif
                    @endif

                    @if(!empty($cart['contents']))
                        @foreach($cart['contents'] as $item )
                            @include('cart.partials.cart_item', ['edit' => false, 'item' => $item])
                            @if(!$loop->last)
                                <hr>
                            @endif
                        @endforeach
                    @endif
                </div><!-- /.mobile-box -->

                @include('checkout.partials.checkout_subtotal')

            </div> <!-- .section -->


		</div> <!-- .col -->
        <div class="hidden-xs hidden-sm col-md-3">

            @include('checkout.partials.sidebar-checkout')

        </div> <!-- .col-md-3 -->
	</div>
</div>
@stop

@section('scripts')




@stop
