@extends('layouts.frontend')

@section('title')
    {!! !empty($article->meta_title) ? $article->meta_title : $article->title !!}
@stop

 @section('body-class', 'guide_page')

@section('heading')
    {{ $article->title }}
@stop

@section('meta_description')
    {!! $article->meta_description !!}
@stop

@section('content')

<div class="container help-guide">

    <div class="row">
        <div class="col-md-9">
            <div class="banner-title" style="background-image: url('{{ URL('imagecache', ['template' => 'original', 'filename' => $article->image->filename]) }}')" alt="{{ $article->title }}">

                <h2 class="title">Professional Towbar Fitting at your home or work</h2>

            </div><!-- /.banner-title -->

        </div><!-- /.col-md-9 -->

        <div class="col-md-3 hidden-xs  hidden-sm">
            <a href="/how-to-guide-towbars" class="block-link block-link-towbars">
                <span>The Complete Guide to Buying a Towbar</span>
            </a><!-- /.block-link -->

            <a href="/how-to-guide-towbars#electrics" class="block-link block-link-electrics">
                <span>The Beginner’s Guide to Towbar Electrics</span>
            </a><!-- /.block-link -->
        </div><!-- /.col-md-3 -->

    </div><!-- /.row -->

    <div class="row">
        <div class="col-sm-12">

            {!! $article->content !!}

        </div><!-- /.col-sm-12 -->

    </div><!-- /.row -->

    <div class="row">
        <div class="col-sm-12">

            <h2 class="subheader">How does it work exactly?</h2>

        </div><!-- /.col-sm-12 -->

        <div class="col-sm-12 steps-col">

            <div class="swiper-container">
                <div class="swiper-wrapper steps">

                    <div class="swiper-slide step-wrapper">
                        <div class="step">
                            <div class="step-body">
                                <div class="step-title step-title-active">
                                    <span class="number">1</span>
                                    <h3 class="title">Order your Witter <br>towbar online</h3>
                                    <i class="icon-click-icon"></i>
                                </div><!-- /.step-title -->
                                <div class="step-copy">
                                    <p>Find the perfect towbar for your vehicle using our registration lookup service</p>
                                </div><!-- /.step-copy -->
                            </div><!-- /.step-body -->
                        </div><!-- /.step -->
                    </div><!-- /.swiper-slide -->

                    <div class="swiper-slide step-wrapper">
                        <div class="step">
                            <div class="step-body">
                                <div class="step-title">
                                    <span class="number">2</span>
                                    <h3 class="title">Select a <br>date and time</h3>
                                    <i class="icon-calendar"></i>
                                </div><!-- /.step-title -->
                                <div class="step-copy">
                                    <p>Choose a day and time for fitting that is convenient for you</p>
                                </div><!-- /.step-copy -->
                            </div><!-- /.step-body -->
                        </div><!-- /.step -->
                    </div><!-- /.swiper-slide -->

                    <div class="swiper-slide step-wrapper">
                        <div class="step">
                            <div class="step-body">
                                <div class="step-title">
                                    <span class="number">3</span>
                                    <h3 class="title">Your new towbar gets delivered</h3>
                                    <i class="icon-package-icon"></i>
                                </div><!-- /.step-title -->
                                <div class="step-copy">
                                    <p>We deliver everything you need for fitting straight to your door</p>
                                </div><!-- /.step-copy -->
                            </div><!-- /.step-body -->
                        </div><!-- /.step -->
                    </div><!-- /.swiper-slide -->

                    <div class="swiper-slide step-wrapper">
                        <div class="step">
                            <div class="step-body">
                                <div class="step-title">
                                    <span class="number">4</span>
                                    <h3 class="title">Your mobile fitter <br>arrives</h3>
                                    <i class="icon-white-spanner"></i>
                                </div><!-- /.step-title -->
                                <div class="step-copy">
                                    <p>One of our trusted, professional towbar fitters comes to your work or home and fits the towbar to your vehicle</p>
                                </div><!-- /.step-copy -->
                            </div><!-- /.step-body -->
                        </div><!-- /.step -->
                    </div><!-- /.swiper-slide -->

                    <div class="swiper-slide step-wrapper">
                        <div class="step">
                            <div class="step-body">
                                <div class="step-title">
                                    <span class="number">5</span>
                                    <h3 class="title">Towbar <br>Guarantee</h3>
                                    <i class="icon-thumb-icon"></i>
                                </div><!-- /.step-title -->
                                <div class="step-copy">
                                    <p>All fitting work is guaranteed by Witter Towbars direct</p>
                                </div><!-- /.step-copy -->
                            </div><!-- /.step-body -->
                        </div><!-- /.step -->
                    </div><!-- /.swiper-slide -->

                </div><!-- /.swiper-wrapper -->

                <div class="steps-pagination hidden-md hidden-lg">
                    <!-- Add Pagination -->
                    <div class="swiper-pagination "></div>

                    <i class="icon-page-scroll"></i>

                </div><!-- /.steps-pagination -->

            </div><!-- /.swiper-container -->

        </div><!-- /.col-sm-12 -->

    </div><!-- /.row -->


    <div class="row">

        <div class="col-xs-12 col-sm-6 hidden-md  hidden-lg">
            <a href="/how-to-guide-towbars" class="block-link block-link-towbars">
                <span>The Complete Guide to Buying a Towbar</span>
            </a><!-- /.block-link -->
        </div><!-- /.col-xs-12 -->

        <div class="col-xs-12 col-sm-6 hidden-md  hidden-lg">
            <a href="/how-to-guide-towbars#electrics" class="block-link block-link-electrics">
                <span>The Beginner’s Guide to Towbar Electrics</span>
            </a><!-- /.block-link -->
        </div><!-- /.col-xs-12 -->

    </div><!-- /.row -->

    {{--
    <div class="row">
        <div class="col-sm-12">

            <h2 class="subheader">Let’s start helping you to find a mobile fitter near you...</h2>

            @include('partials.search-dropdowns-horizontal')

        </div><!-- /.col-sm-12 -->

    </div><!-- /.row -->

     --}}

</div> <!-- .container -->

@stop

@section('scripts')


    <script type="text/javascript">

        function setSwiper() {

            var viewport = $(window).width();

            if ( viewport <= 1040 ) {
                var swiper = new Swiper('.swiper-container', {

                    slidesPerView: 'auto',
                    CSSWidthAndHeight: true,
                    freeMode: true,
                    pagination: {
                         el: '.swiper-pagination',
                            clickable: true,
                    },
                });
            }
        }

        $(function() {
            // Trigger the swiper
            setSwiper();

            // "step" On hover keep the step active.
            $( ".step" ).hover(function() {

                $( ".step-title" ).removeClass( "step-title-active" );

                $( this ).find( ".step-title" ).addClass( "step-title-active" );
            });

        });

        $( window ).resize(function() {
            setSwiper();
        });

  </script>

@stop
