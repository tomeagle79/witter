<a href="{{ route('help_articles') }}" class="no-hover">
    <div class="sidebar-price">

        <span class="pound"><img src="/assets/img/pound-sign.png" alt="Pound sign" /></span>

        <h2 class="sidebar-price-title">All-in-one <span class="slim block">price</span></h2>

        <p>No hidden costs...<br /> Fully fitted by our experts</p>

        <span class="sr-only">Click here to find out what your car can pull</span>

    </div>
</a>
