<?php
namespace App\Http\Controllers;

use App\Models\Api\Accessory;
use App\Models\Api\PartClass;
use App\Models\Api\VehicleManufacturer;
use App\Models\Api\VehicleModel;
use App\Models\Banner;
use App\Models\BlogPost;
use App\Show;

class WelcomeController extends FrontendController
{

    public function __construct()
    {
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        $this->data['partClasses'] = PartClass::get_all();
        $this->data['manufacturers_dropdown'] = VehicleManufacturer::get_all();
        $this->data['bannerType'] = 'Normal';
        $this->data['banners'] = Banner::active()->orderBy('sort_order', 'ASC')->get();
        $this->data['popular_products'] = Accessory::get_popular_products();
        $this->data['top_ten'] = VehicleModel::get_popular();
        $this->data['blog_posts'] = BlogPost::published()->orderBy('created_at', 'desc')
            ->limit(4)
            ->get();
        return view('pages.home', $this->data);
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function indexTwo()
    {
        $this->data['partClasses'] = PartClass::get_all();
        $this->data['manufacturers_dropdown'] = VehicleManufacturer::get_all();
        $this->data['bannerType'] = 'AB';
        $this->data['banners'] = Banner::active()->orderBy('sort_order', 'ASC')->get();
        $this->data['noindex_page'] = true;
        $this->data['canonical'] = 'https://www.witter-towbars.co.uk';

        $this->data['top_ten'] = VehicleModel::get_popular();

        return view('pages.home', $this->data);
    }
}
