<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>@yield('title') | {{ config('settings.site.name') }}</title>
        <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet" />

        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content">
        <div class="page-header navbar navbar-fixed-top">
            <div class="page-header-inner">
                <div class="page-logo">
                    <a href="">
                        <img src="{{ asset('assets/img/logo.png') }}" alt="{{ config('settings.site.name') }}" width="70">
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                    </div>
                </div>
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right navbar-right">
                        <li class="dropdown dropdown-user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username"><i class="fa fa-user"></i> {{ Auth::user()->name }}</span>
                                <i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <ul class="page-sidebar-menu " data-auto-scroll="true" data-slide-speed="200">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler">
                            </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>


                        <li{!! strpos(Request::url(), config('settings.admin.slug').'/dashboard') ? ' class="active"' : '' !!}>
                            <a href="{!! URL::backend('dashboard') !!}">
                                <i class="fa fa-fw fa-dashboard"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>

                        <li{!! Request::is('*/admin_users*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('admin_users') }}">
                                <i class="fa fa-fw fa-users"></i>
                                <span class="title">Admin Users</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/admin_users') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('admin_users') !!}">
                                    <i class="fa fa-list"></i>
                                    Manage</a>
                                </li>
                                <li{!! Request::is('*/admin_users/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('admin_users/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add User</a>
                                </li>
                            </ul>
                        </li>

                        <li{!! Request::is('*/blog_posts*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('blog_posts') }}">
                                <i class="fa fa-fw fa-newspaper-o"></i>
                                <span class="title">Blog Posts</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/blog_posts') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('blog_posts') !!}">
                                    <i class="fa fa-list"></i>
                                    Manage</a>
                                </li>
                                <li{!! Request::is('*/blog_posts/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('blog_posts/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add Blog Posts</a>
                                </li>
                            </ul>
                        </li>

                        <li{!! Request::is('*/blog_categories*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('blog_categories') }}">
                                <i class="fa fa-fw fa-newspaper-o"></i>
                                <span class="title">Blog Categories</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/blog_categories') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('blog_categories') !!}">
                                    <i class="fa fa-list"></i>
                                    Manage</a>
                                </li>
                                <li{!! Request::is('*/blog_categories/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('blog_categories/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add Blog Categories</a>
                                </li>
                            </ul>
                        </li>

                        <li{!! Request::is('*/help_articles*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('help_articles') }}">
                                <i class="fa fa-fw fa-question-circle"></i>
                                <span class="title">Help &amp; Advice Articles</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/help_articles') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('help_articles') !!}">
                                    <i class="fa fa-list"></i>Manage</a>
                                </li>
                                <li{!! Request::is('*/help_articles/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('help_articles/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add Help &amp; Advice</a>
                                </li>
                            </ul>
                        </li>

                        <li{!! Request::is('*/help_categories*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('help_categories') }}">
                                <i class="fa fa-fw fa-question-circle"></i>
                                <span class="title">Help &amp; Advice Categories</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/help_categories') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('help_categories') !!}">
                                    <i class="fa fa-list"></i>Manage</a>
                                </li>
                                <li{!! Request::is('*/help_categories/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('help_categories/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add Help &amp; Advice Category</a>
                                </li>
                            </ul>
                        </li>

                        <li{!! Request::is('*/faqs*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('faqs') }}">
                                <i class="fa fa-fw fa-question-circle"></i>
                                <span class="title">FAQS</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/faqs') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('faqs') !!}">
                                    <i class="fa fa-list"></i>
                                    Manage</a>
                                </li>
                                <li{!! Request::is('*/faqs/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('faqs/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add FAQ</a>
                                </li>
                            </ul>
                        </li>

                        <li{!! Request::is('*/faq_categories*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('faq_categories') }}">
                                <i class="fa fa-fw fa-question-circle"></i>
                                <span class="title">FAQ Categories</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/faq_categories') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('faq_categories') !!}">
                                    <i class="fa fa-list"></i>
                                    Manage</a>
                                </li>
                                <li{!! Request::is('*/faq_categories/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('faq_categories/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add FAQ Category</a>
                                </li>
                            </ul>
                        </li>

                        <li{!! Request::is('*/pages*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('pages') }}">
                                <i class="fa fa-fw fa-file"></i>
                                <span class="title">Pages</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/pages') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('pages') !!}">
                                    <i class="fa fa-list"></i>
                                    Manage</a>
                                </li>
                                <li{!! Request::is('*/pages/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('pages/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add Page</a>
                                </li>
                            </ul>
                        </li>

                        <li{!! Request::is('*/banners*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('banners') }}">
                                <i class="fa fa-fw fa-map-signs"></i>
                                <span class="title">Banners</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/banners') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('banners') !!}">
                                    <i class="fa fa-list"></i>
                                    Manage</a>
                                </li>
                                <li{!! Request::is('*/banners/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('banners/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add Banner</a>
                                </li>
                            </ul>
                        </li>

                        <li{!! Request::is('*/careers*') ? ' class="active"' : '' !!}>
                            <a href="{{ URL::backend('careers') }}">
                                <i class="fa fa-fw fa-briefcase"></i>
                                <span class="title">Careers</span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li{!! Request::is('*/careers') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('careers') !!}">
                                    <i class="fa fa-list"></i>
                                    Manage</a>
                                </li>
                                <li{!! Request::is('*/careers/create') ? ' class="active"' : '' !!}>
                                    <a href="{!! URL::backend('careers/create') !!}">
                                    <i class="fa fa-plus"></i>
                                    Add Career</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
		        </div>
            </div>
            <div class="page-content-wrapper">
                <div class="page-content">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

                    @if (session('message'))
                        <div class="note note-danger noprint">
                            <p>{!! session('message') !!}</p>
                        </div>
                    @endif

                    @if (session()->has('error'))
                        <div class="note note-danger noprint">
                            <p>{!! session('error') !!}</p>
                        </div>
                    @endif
                    
                    @if (session()->has('success'))
                        <div class="note note-success noprint">
                            <p>{!! session('success') !!}</p>
                        </div>
                    @endif

                    @yield('actions')
                    <h3 class="page-title">
                        @yield('title')
                    </h3>

                    @yield('content')
                </div>
            </div>
        </div>

        <div class="page-footer">
            <div class="page-footer-inner">
                &copy; <?php echo date('Y'); ?> {{ config('settings.site.name') }}.  Created by <a href="{{ config('settings.developer.url') }}" target="_blank" title="{{ config('settings.developer.name') }}">{{ config('settings.developer.name') }}</a>.
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>

        <script src="{{ asset('assets/js/admin.js') }}"></script>

        @yield('scripts')
        
<script>
    $(document).ready(function() {

    Metronic.init(); // init metronic core
    Layout.init(); // init layout

   $.ajaxPrefilter(function(options, originalOptions, xhr) {
        var token = '{!! csrf_token() !!}';

        if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
        }
    });

   $('.fancybox.image').fancybox({
        padding: 5
   });
});
</script>

    </body>
</html>
