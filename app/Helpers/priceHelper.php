<?php

function price($price)
{
    if (empty($price)) {
        return 0.00;
    } else {
        return number_format($price, 2, '.', ',');
    }
}
