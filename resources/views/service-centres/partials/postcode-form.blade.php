
<div class="centres-form-container">

    <p>Search over 450 locations, simply enter your postcode</p>

    <div class="row">
        {!! Form::open(['route' => 'service-centres', 'class' => '', 'id' => 'service_centres_form', 'method' => 'get']) !!}

        <div class="col-lg-7">
            {!! Form::text('postcode', null, array('class'=>'form-control', 'placeholder'=>'Postcode')) !!}
        </div><!-- /.col-sm-7 -->

        <div class="col-lg-5">
            <button type="submit" class="btn btn-primary btn-primary-large ">Find my service centre</button>
        </div><!-- /.col-sm-7 -->

        {!! Form::close() !!}
    </div><!-- /.row -->

</div><!-- /.centres-form -->
