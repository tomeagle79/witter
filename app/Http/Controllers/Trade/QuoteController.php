<?php

namespace App\Http\Controllers\Trade;

use App\Contracts\TenantLocaleContext;
use App\Http\Controllers\Controller;
use App\Models\Quote;
use App\Services\ProvisionalAppointmentFactory;
use App\Services\WitterQuoteFactory;
use Illuminate\Http\Request;
use Session;

class QuoteController extends Controller
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cart = build_cart();

        // If the cart has no data just return to the cart page.
        if (empty($cart)) {
            return redirect(route('cart'));
        }

        $this->data['cart'] = $cart;
        $this->data['checkout_layout'] = true;
        $this->data['checkout_layout_steps_hide'] = true;

        return view('theme::cart.quote_create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WitterQuoteFactory $witter_quote_factory)
    {
        $cart = build_cart();

        // If the cart has no data just return to the cart page.
        if (empty($cart)) {
            return redirect(route('cart'));
        }

        // Cretae a long random string to reference in the url
        $quote_link_ref = md5(uniqid(rand(), true));

        // Encode the cart to json
        $quote_data = json_encode($cart);

        // Get the customers user data.
        $customer_arr = $request->input();

        // Json encode the customers data.
        $customer_data = json_encode($customer_arr);

        // If registration plate data is there then pass it in the quote data.
        $registration_no = empty(Session::get('registration_no')) ? null : Session::get('registration_no');

        // Create the quote to the database.
        $data = compact('quote_link_ref', 'quote_data', 'customer_data', 'registration_no');

        $quote = Quote::create($data);

        // Set a quote ref for users.
        $quote->quote_ref = 'Q' . str_pad($quote->id, 5, '0', STR_PAD_LEFT);

        $quote->save();

        // Create witter quote
        $witter_quote = $witter_quote_factory->create($quote);

        Session::put('quote_id', $quote->id);

        // If creating a quote errors then add a message to say so.
        if (empty($witter_quote->success)) {

            return redirect(route('quote.show'))->with('error', 'Quote failed to send.');
        } else {
            // Successful quote normal response.
            return redirect(route('quote.show'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(TenantLocaleContext $tenant)
    {
        // Get the quote id from the session.
        $quote_id = Session::get('quote_id');

        // Get the full quote from the database.
        $quote = Quote::find($quote_id);

        // Get product info from the cart.
        $cart = build_cart();

        // If the cart has no data just return to the cart page.
        if (empty($cart)) {
            return redirect(route('cart'));
        }

        // Set the template values.
        $this->data['quote_link'] = $tenant->getWebsite()->redirect_url . 'my-quote/' . $quote->quote_link_ref;
        $this->data['cart'] = $cart;
        $this->data['quote'] = $quote;
        $this->data['checkout_layout'] = true;
        $this->data['checkout_layout_steps_hide'] = true;

        // Forget items in the session.
        Session::forget('cart');
        Session::forget('quote_id');

        return view('theme::cart.quote_show', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $quote_ref
     * @return \Illuminate\Http\Response
     */
    public function retrieve($quote_link_ref)
    {
        // Clean the quote_link_ref value.
        $quote_link_ref = preg_replace('/[^A-Za-z0-9?!]/', '', $quote_link_ref);

        // Select the quote data
        $quote = Quote::where('quote_link_ref', '=', $quote_link_ref)->first();

        // Select the customer data
        $customer = json_decode($quote->customer_data, true);

        $cart = json_decode($quote->quote_data, true);

        // If the quote is webfit then create provisional appointment
        if (!empty($cart['webfit'])) {

            // Check the appointment date is over 2 days from now.
            $appointmentDate = null;

            if (!empty($cart['towbar']['appointmentDate'])) {

                $appointmentDate = $cart['towbar']['appointmentDate'];

            } elseif (!empty($cart['contents'])) {

                foreach ($cart['contents'] as $item) {
                    if (!empty($item['appointmentDate'])) {
                        $appointmentDate = $item['appointmentDate'];
                    }
                }
            }

            // Min appointment date
            $min_date = date('Ymd', strtotime(' + 2 days'));

            if ($min_date > $appointmentDate) {

                $appointment = false;
            } else {
                // Try and create an appointment.
                $appointment = ProvisionalAppointmentFactory::create($cart, $customer['email']);
            }

            // If the provisional appointment is unsuccessful
            if ($appointment == false || empty($appointment->success)) {
                // Flag that a new appointment is required
                $cart['new_appointment_required'] = true;
            }
        }

        // Set the quote_id in the session.
        $cart['quote_ref'] = $quote->quote_ref;

        // Update the cart with the retrieved cart data.
        Session::put('cart', $cart);

        // If a reg number has been used then set in the user's session.
        if(!empty($quote->registration_no)) {

            Session::put('registration_no', $quote->registration_no);
        }

        return redirect(route('cart'));
    }
}
