<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class VehicleModel extends ApiModel
{
	const URI = "models";

	/**
	 * Get all models based on manufacturer ID
	 *
	 * @param Int
	 * @return Object
	 */
	static public function get_all_from_manufacturer_id($manufacturer_id)
	{
		$url = config('witter.api_base') . 'manufacturers/'. $manufacturer_id .'/models';
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a single model
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_single_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'/slug?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a single model by id
	 *
	 * @params string
	 * @return Object
	 */
	static public function get_by_id(string $id)
	{
		$url = config('witter.api_base') . self::URI . '/' . $id;
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a collection of the most popular models
	 *
	 * @return Object
	 */
	static public function get_popular()
	{
		$url = config('witter.api_base') . self::URI . '/popular';
		$obj = self::get_request($url);

		return $obj;
	}
}
