<?php

namespace App\Http\Controllers\Roofsystems;

use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Api\Commercial\CommercialType;
use App\Models\Api\Commercial\CommercialVehicleManufacturer;
use App\Models\Api\Commercial\CommercialVehicleModel;
use App\Models\Api\Commercial\CommercialVehicle;
use App\Models\Api\Commercial\CommercialRoofdecks;

use App\Models\Api\VehicleManufacturer;
use App\Models\Api\VehicleModel;
use App\Models\Api\VehicleBody;
use App\Models\Api\VehicleRegistration;

class RoofDecksController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Witter Roof Systems', route('roof-systems'));
        $this->data['breadcrumbs']->addCrumb('Roof Racks', route('roof-systems.roof-decks'));
    }

    /**
     * Display a 'home page' of roof decks.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['manufacturers'] = CommercialVehicleManufacturer::get_by_type(['typeSlug' => 'roof-decks']);

        return view('roof-systems.roofdecks.index', $this->data);
    }

    /**
     * Display a list of models
     *
     * @return \Illuminate\Http\Response
     */
    public function models($manufacturer_slug)
    {
        $this->data['manufacturer_slug'] = $manufacturer_slug;
        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['typeSlug' => 'roof-decks', 'makeSlug' => $manufacturer_slug]);

        $this->data['models'] = CommercialVehicleModel::get_by_type(['typeSlug' => 'roof-decks', 'makeSlug' => $manufacturer_slug]);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('roof-systems.roof-decks.models', $manufacturer_slug));

        return view('roof-systems.roofdecks.models', $this->data);
    }

    /**
     * Display a list of vehicles
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicles($manufacturer_slug, $model_slug)
    {
        $this->data['manufacturer_slug'] = $manufacturer_slug;
        $this->data['model_slug'] = $model_slug;

        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);
        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('roof-systems.roof-decks.models', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('roof-systems.roof-bars.models.vehicles', [$manufacturer_slug, $model_slug]));

        $this->data['vehicles'] = CommercialVehicle::get_by_type([
            'typeSlug' => 'roof-decks',
            'makeSlug' => $manufacturer_slug,
            'modelSlug' => $model_slug
        ]);

        return view('roof-systems.roofdecks.vehicles', $this->data);
    }

    /**
     * Display a list of product
     *
     * @return \Illuminate\Http\Response
     */
    public function products($manufacturer_slug, $model_slug, $vehicle_slug)
    {
        $this->data['manufacturer_slug'] = $manufacturer_slug;
        $this->data['model_slug'] = $model_slug;
        $this->data['model_slug'] = $vehicle_slug;

        $this->data['manufacturer'] = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);
        $this->data['model'] = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);
        $this->data['products'] = CommercialVehicle::get_roofdecks_by_slug($vehicle_slug);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('roof-systems.roof-decks.models', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('roof-systems.roof-decks.models.vehicles', [$manufacturer_slug, $model_slug]));
        $this->data['breadcrumbs']->addCrumb('Products', route('roof-systems.roof-decks.models.vehicles', [$manufacturer_slug, $model_slug, $vehicle_slug]));

        return view('roof-systems.roofdecks.products', $this->data);
    }

    /**
     * Display a product
     *
     * @return \Illuminate\Http\Response
     */
    public function view($partNo)
    {
        $this->data['product'] = CommercialRoofdecks::get_single_where(['partNo' => decode_url($partNo)]);

        return view('roof-systems.roofdecks.view', $this->data);
    }
}
