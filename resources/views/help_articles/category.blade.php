@extends('layouts.frontend')

@section('title')
	{!! $category->name !!} - Blog
@stop

@section('heading')
	{!! $category->name !!} 
@stop

@section('meta_description')
	{!! $category->name !!}. The Craft Channel blog, news, articles and inspiration
@stop

@section('content')
	@include('blog.loop', ['blog_posts' => $blog_posts])
@stop