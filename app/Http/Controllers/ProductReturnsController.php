<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ReturnRequest;

use Mail;

class ProductReturnsController extends FrontendController
{
    /**
     * Product returns form
     *
     * @return \Illuminate\Http\Response
     */
    public function returns()
    {
        $this->data['breadcrumbs']->addCrumb('Product Returns');
        return view('returns.index', $this->data);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function returns_process(ReturnRequest $request)
    {
        Mail::send('emails.returns', (array)$request->all(), function($message)
        {
            $message->from(config('witter.noreply_email'));
            $message->to(config('witter.returns_email'), 'Admin')->subject('Witter Product Returns');
        });

        return redirect(route('returns'))->with('success', 'Thank you.  We will be in touch about your return.');
    }
}
