<?php

namespace App\Http\Controllers;

use App\Models\Career;
use App\Models\Page;

class CareerController extends FrontendController
{
    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('About', route('about'));
    }

    /**
     * Show the Career's page
     *
     * @param  string $slug
     * @param  id     $career_id
     * @return Response
     */
    public function show($slug, $career_id)
    {

        $this->data['career'] = $career = Career::published()->find($career_id);

        if (empty($career)) {

            return redirect('about');
        }

        $this->data['breadcrumbs']->addCrumb($career->title);

        return view('careers.show', $this->data);
    }

}
