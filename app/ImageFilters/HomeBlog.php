<?php

namespace App\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class HomeBlog implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(680, 480);
    }
}