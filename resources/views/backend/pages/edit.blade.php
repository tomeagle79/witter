@extends('layouts.backend')

@section('title')
	Edit Pages
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/pages/partials/_form', ['submit_text' => 'Edit Page', 'type' => 'edit'])
	</form>

@stop
