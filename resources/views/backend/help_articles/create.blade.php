@extends('layouts.backend')

@section('title')
	Create Help &amp; Advice Article  
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/help_articles/partials/_form', ['submit_text' => 'Create Help &amp; Advice Article', 'type' => 'add'])
	</form>

@stop

