<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stripe_id');
            $table->string('card_id');
            $table->string('address_city');
            $table->string('address_country');
            $table->string('address_line1');
            $table->string('address_line1_check');
            $table->string('address_line2');
            $table->string('address_state');
            $table->string('address_zip');
            $table->string('address_zip_check');
            $table->string('brand');
            $table->string('country');
            $table->string('cvc_check');
            $table->string('dynamic_last4');
            $table->string('exp_month');
            $table->string('exp_year');
            $table->string('funding');
            $table->string('last4');
            $table->string('name');
            $table->string('tokenization_method');
            $table->string('client_ip');
            $table->string('created');
            $table->string('type');
            $table->boolean('livemode');

            $table->string('charge_id')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('amount_refunded')->nullable();
            $table->string('application')->nullable();
            $table->string('application_fee')->nullable();
            $table->string('balance_transaction')->nullable();
            $table->boolean('captured')->nullable();
            $table->string('currency')->nullable();
            $table->string('customer')->nullable();
            $table->string('description')->nullable();
            $table->string('destination')->nullable();
            $table->string('dispute')->nullable();
            $table->string('failure_code')->nullable();
            $table->string('failure_message')->nullable();
            $table->string('fraud_details')->nullable();
            $table->string('invoice')->nullable();
            $table->string('on_behalf_of')->nullable();
            $table->string('order')->nullable();
            $table->string('outcome_network_status')->nullable();
            $table->string('outcome_reason')->nullable();
            $table->string('outcome_risk_level')->nullable();
            $table->string('outcome_seller_message')->nullable();
            $table->string('outcome_type')->nullable();
            $table->boolean('paid')->default(0);
            $table->string('receipt_email')->nullable();
            $table->string('receipt_number')->nullable();
            $table->string('review')->nullable();
            $table->string('shipping')->nullable();
            $table->string('source_transfer')->nullable();
            $table->string('statement_descriptor')->nullable();
            $table->string('status')->nullable();
            $table->string('transfer_group')->nullable();

            $table->boolean('process')->default(0)->comment('Defaults to 0, changes if payment is successful');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripe_payments');
    }
}
