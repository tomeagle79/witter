<?php

return [

    'credentials' => [
        'client_id' => env('PAYPAL_CLIENT_ID'),
        'client_secret' => env('PAYPAL_CLIENT_SECRET'),
    ],
    'experience_profile_id' => env('PAYPAL_EXPERIENCE_PROFILE_ID', false),
    'paypal_sandbox' => env('PAYPAL_SANDBOX', false),
    'log_api_calls' => env('PAYPAL_LOG_API', false),
    'sanitised_errors' => [
        'GENERAL' => 'There was a problem taking payment via PayPal.  You have not been charged.  Please try again or contact us directly.',
        'AUTHORIZATION_AMOUNT_LIMIT_EXCEEDED' => 'There was a problem taking payment via PayPal. The payment exceeded the PayPal authorised limit. You have not been charged. Please try payment using a credit or debit card.',
        'CREDIT_CARD_CVV_CHECK_FAILED' => 'There was a problem taking payment via PayPal. Your CVV number was incorrect, please try again or pay by credit/debit card.',
        'CREDIT_CARD_REFUSED' => 'There was a problem taking payment via PayPal. Your credit card was refused, please try again or use another card.',
        'CREDIT_PAYMENT_NOT_ALLOWED' => 'There was a problem taking payment via PayPal.  Your credit card was not allowed, please try again or use another card.',
        'INSTRUMENT_DECLINED' => 'There was a problem taking payment via PayPal. Please try again or use another card.',
        'CREDIT_PAYMENT_NOT_ALLOWED' => 'There was a problem taking payment via PayPal. Your credit payment was not allowed, please try again or use another card.',
        'MAX_NUMBER_OF_PAYMENT_ATTEMPTS_EXCEEDED' =>  'There was a problem taking payment via PayPal. You have exceeded the maximum number of payment. Please try payment using a credit or debit card.',
        'NEED_CREDIT_CARD' => 'There was a problem taking payment via PayPal. You need to set up a credit or debit card in your PayPal account. Please try payment using a credit or debit card.',
        'NEED_CREDIT_CARD_OR_BANK_ACCOUNT' => 'There was a problem taking payment via PayPal. You need to set up a credit or debit card in your PayPal account. Please try payment using a credit or debit card.',
        'PAYEE_ACCOUNT_INVALID' => 'There was a problem taking payment via PayPal. Your PayPal account has not been set up correctly. Please try payment using a credit or debit card.',
        'PAYEE_ACCOUNT_LOCKED_OR_CLOSED' => 'There was a problem taking payment via PayPal. Your PayPal account has been locked or closed. Please try payment using a credit or debit card.',
        'PAYEE_ACCOUNT_NO_CONFIRMED_EMAIL' => 'There was a problem taking payment via PayPal. Your PayPal account needs email confirmed. Please try payment using a credit or debit card.',
        'PAYEE_ACCOUNT_RESTRICTED' => 'There was a problem taking payment via PayPal. Your PayPal account has restricted. Please try payment using a credit or debit card.',
        'PAYMENT_DENIED' => 'There was a problem taking payment via PayPal. Your PayPal payment was denied. Please try payment using a credit or debit card.',
        'PHONE_NUMBER_REQUIRED' => 'There was a problem taking payment via PayPal. Your PayPal account needs a phone number setting up. Please try payment using a credit or debit card.',
        'REDIRECT_PAYER_FOR_ALTERNATE_FUNDING' => 'There was a problem taking payment via PayPal. Please use an alternative payment type, please try payment using a credit or debit card.',
        'TRANSACTION_REFUSED' => 'There was a problem taking payment via PayPal. Please use an alternative payment type, please try payment using a credit or debit card.',
        'TRANSACTION_REFUSED_BY_PAYPAL_RISK' => 'There was a problem taking payment via PayPal. Please use an alternative payment type, please try payment using a credit or debit card.',
        'TRANSACTION_REFUSED_PAYEE_PREFERENCE' => 'There was a problem taking payment via PayPal. Please use an alternative payment type, please try payment using a credit or debit card.',
     ]
];
