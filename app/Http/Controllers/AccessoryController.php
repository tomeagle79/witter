<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Api\PartClass;
use App\Models\Api\Accessory;

class AccessoryController extends FrontendController
{
    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
         parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Accessories', route('accessories'));
    }

    /**
     * Display a listing of all accessory categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('accessories.index', $this->data);
        
    }

    /**
     * Display a listing of all accessory categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function category($category_slug)
    {
        $this->data['category'] = PartClass::get_category_by_slug($category_slug);
        $this->data['subcategories'] = PartClass::get_subcategories_by_slug($category_slug);

        $this->data['breadcrumbs']->addCrumb($this->data['category']->title, route('accessories.multi', $category_slug));

        return view('accessories.category', $this->data);
    }

    /**
     * Display a listing of all accessory categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function subcategory($category_slug, $subcategory_slug)
    {
        $this->data['category'] = PartClass::get_category_by_slug($category_slug);
        $this->data['subcategories'] = PartClass::get_subcategories_by_slug($category_slug);
        $this->data['subcategory'] = PartClass::get_subcategory_by_slug($category_slug, $subcategory_slug);

        if (is_null($this->data['subcategory'])) {
            return abort(404);
        }

        $this->data['accessories'] = Accessory::get_accessories_by_slug($category_slug, $subcategory_slug);

        $this->data['breadcrumbs']->addCrumb($this->data['category']->title, route('accessories.multi', $category_slug));

        if (!empty($this->data['subcategory'])) {
            $this->data['breadcrumbs']->addCrumb($this->data['subcategory']->categoryName, route('accessories.subcategory', [$category_slug, $subcategory_slug]));
        }

        return view('accessories.subcategory', $this->data);
    }

    /**
     * Display the accessory.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($accessory_slug)
    {
        $this->data['accessory'] = Accessory::get_single_by_slug($accessory_slug);

        $this->data['category'] = PartClass::get_partclass_by_id($this->data['accessory']->partClassId);
        $this->data['subcategories'] = PartClass::get_categories_by_partclass_id($this->data['accessory']->partClassId);
        $this->data['subcategory'] = PartClass::get_category_by_category_id($this->data['accessory']->categoryId);

        $this->data['breadcrumbs']->addCrumb($this->data['category']->title, route('accessories.multi', $this->data['category']->slug));
        $this->data['breadcrumbs']->addCrumb($this->data['subcategory']->categoryName, route('accessories.subcategory', [$this->data['category']->slug, $this->data['subcategory']->slug]));
        $this->data['breadcrumbs']->addCrumb($this->data['accessory']->title, route('accessories.multi', [encode_url($this->data['accessory']->partNo)]));


        // If the accessory is "fittable" then we need to check if an appointment already exists.
        if($this->data['accessory']->isFittable){

            // Check if we have 'accessory-fitted' items in the cart
            $cart = build_cart();

            if(isset($cart['contents'])){

                foreach($cart['contents'] as $item){

                    // If we do have an 'accessory-fitted' item then add it to the 'appointment-exists' value.
                    if($item['type'] == 'accessory-fitted') {
                        $this->data['appointment_exists'] = $item;
                    }
                }
            }
        }

        // If fitable use view_fittable template
        if($this->data['accessory']->isFittable){
            return view('accessories.view_fittable', $this->data);

        }else{
            return view('accessories.view', $this->data);
        }
    }

}