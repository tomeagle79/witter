@extends('layouts.frontend')

@section('title', 'Bike Racks and Carriers')

@section('heading')
    Bike Racks and Carriers
@stop

@section('body-class')cycle-carriers-page @stop

@section('meta_description')Witter Towbars stock a variety of platform style bike racks that can hold from 2 to 4 bikes. All models benefit from the tilt forward function to allow easy access to the boot. Let us help you find the right bike rack for you.@stop

@section('content')
    @php
        if(isset($filtersArray['amount']) || isset($filtersArray['type']) || isset($filtersArray['ebike'])) {
            $scroll = true;
        } else {
            $scroll = 0;
        }
    @endphp
	<div class="container cycle-carriers" data-scroll="{{ $scroll }}">
        <div class="row content">
            <div class="col-xs-12">
                <p>Witter Towbars stock a range of platform style bike racks that can hold from 2 to 4 bikes. All models benefit from the tilt forward function to allow easy access to the boot. Our bike racks vary in price to suit your budget and needs so you can be sure you're getting the right rack for you.</p>
            </div>
        </div>
		<div class="row">
			<div class="col-sm-12 col-md-12">
                {!! Form::open(array('route' => 'bike-racks', 'class' => 'form', 'id' => 'bike_carriers_submit')) !!}
                    <div class="filter-wrapper">
                        <div class="title-part">
                            <h3><strong>Unsure? </strong> Let us help you find the right bike rack for you</h3>
                        </div>
                        <div class="row">
                            <div class="filter-box clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="filter">
                                        <p>Number of bikes to carry?</p>
                                        <div class="select-wrapper">
                                            <select name="amount_of_bikes" id="amount_of_bikes" data-default="<?php echo isset($filtersArray['amount']) ? $filtersArray['amount'] : 'any'; ?>" required>
                                                <option value="any">Any</option>
                                                <option value="1">1 bike</option>
                                                <option value="2">2 bikes</option>
                                                <option value="3">3 bikes</option>
                                                <option value="4">4 bikes</option>
                                            </select>
                                        </div>
                                    </div><!-- /.filter -->
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="filter">
                                        <p>Your connection type?</p>
                                        <div class="select-wrapper">
                                            <select name="type_of_connection" id="type_of_connection" data-default="<?php echo isset($filtersArray['type']) ? $filtersArray['type'] : 'any'; ?>" required>
                                                <option value="any">Any</option>
                                                <option value="towbar">Towbar</option>
                                                <option value="rear">Rear</option>
                                                <option value="roof">Roof</option>
                                            </select>
                                        </div>
                                    </div><!-- /.filter -->
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="filter">
                                        <p>Do you need to carry e-bikes?</p>
                                        <div class="select-wrapper">
                                            <select name="e_bike" id="e_bike" data-default="<?php echo isset($filtersArray['ebike']) ? $filtersArray['ebike'] : 'No'; ?>">
                                                <option value="No">Please Select</option>
                                                <option value="No">No</option>
                                                <option value="Yes">Yes</option>
                                            </select>
                                        </div>
                                    </div><!-- /.filter -->
                                </div>

                                <input type="hidden" name="redirect_url" id="redirect_url">
                            </div>
                        </div>
                    </div><!-- /.filter-box -->
                {!! Form::close() !!}
			</div> <!-- .col-md-9 -->
        </div> <!-- .row -->

        <div class="row">
            <div class="col-xs-12">
                <div class="filters-selection">

                    @if(!empty($filters))
                        <span>YOUR FILTERS</span>
                    @endif

                    @if(isset($filters['brand']))

                        @if(isset($filters['price']))
                            <a href="{{ route('trackers.index', ['price' => $filters['price']]) }}"
                        @else
                            <a href="{{ route('trackers.index') }}"
                        @endif
                            class="btn btn-remove">
                                <span>{{ $filters['brand'] }}</span>
                            </a>
                    @endif

                    @if(isset($filters['price']))

                        @if(isset($filters['brand']))
                            <a href="{{ route('trackers.index', ['brand' => $filters['brand']]) }}"
                        @else
                            <a href="{{ route('trackers.index') }}"
                        @endif
                            class="btn btn-remove">
                                <span>{!! $price_filters[$filters['price']] !!}</span>
                            </a>
                    @endif

                </div><!-- /.filter-selection -->
            </div><!-- /.col-sm-12 -->
        </div><!-- /.row -->

        <div class="row item-list is-flex lazy-load-images" id="prod_results">
            @if($carriers)
                @foreach($carriers as $carrier)

                    @include('partials.analytics.json_product_details', [
                        'product' => $carrier,
                        'list_name' => 'Category Results',
                        'category' => 'Bike Racks',
                        'position' => $loop->iteration
                    ])

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="item">
                            <a href="{!! route('bike_rack.view', [encode_url($carrier->slug)]) !!}" class="product-link " data-product-id="{{ $carrier->partNo }}">
                                @if($loop->index < 4)
                                    <img  src="{{ image_load($carrier->imageUrl) }}" class="img-center" alt="{!! $carrier->title !!}" />
                                @else
                                    <img  src="/assets/img/ajax-loader.gif"  data-src="{{ image_load($carrier->imageUrl) }}" class="img-center" alt="{!! $carrier->title !!}" />
                                @endif
                            </a>
                            <a href="{!! route('bike_rack.view', [encode_url($carrier->slug)]) !!}" class="product-link" data-product-id="{{ $carrier->partNo }}"><h3 class="list-title">{!! $carrier->title !!}</h3></a>

                            <hr>

                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="price">&pound;{{ price($carrier->price) }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="price-rrp">@if($carrier->retailPrice > $carrier->price)RRP &pound;{{ price($carrier->retailPrice) }}@endif</p>
                                </div>
                            </div>

                            @if($carrier->isSellable && $carrier->inStock)
                                <div class="row stock">
                                    <div class="col-xs-12">
                                        <i class="icon-check"></i>
                                        <p>In Stock</p>
                                    </div>
                                </div>
                            @else
                                <div class="row stock">
                                    <div class="col-xs-12">
                                        <i class="icon-cancel-1"></i>
                                        <p>Out of stock</p>
                                    </div>
                                </div>
                            @endif

                            @if($carrier->isSellable && $carrier->inStock)
                                <div class="row">
                                    <div class="col-xs-7">
                                        <a href="{!! route('bike_rack.view', [encode_url($carrier->slug)]) !!}" class="btn btn-primary product-link" data-product-id="{{ $carrier->partNo }}">View Now</a>
                                    </div><!-- /.col-xs-7 -->
                                    <div class="col-xs-5">
                                        <a href="{{ route('add_to_cart', [
                                            'type' => 'accessory',
                                            'partNo' => $carrier->partNo
                                        ]) }}" class="btn btn-primary btn-green-plus"> <img src="/assets/img/basket.svg" height="20" width="20" alt="Add to basket"></a>
                                    </div>
                                </div><!-- /.row -->
                            @else
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a diabled class="btn btn-primary btn-oos-model">Notify me when stock arrives</a>
                                    </div>
                                </div><!-- /.row -->
                            @endif

                            @if($carrier->inStock == false)
                                <div class="out-of-stock-modal">
                                    <div class="close-modal"><i class="icon-cancel-1"></i></div>
                                    <img src="{{URL('assets/img/icons', ['filename' => 'basket_80px.svg'])}}" alt="">
                                    <p>We’re sorry we have no stock</p>
                                    <h5>We’d like to let you know when it’s back…</h5>
                                    <p>Fill in your email address here and as soon as it comes back into stock we’ll send you an update:</p>

                                    {!! Form::open(['route' => 'product.email_notify', 'onsubmit' => "
                                        gtag('event', '". ($carrier->isSellable ? 'Out of stock' : 'Not for sale') ."', {
                                            event_category: 'Form Submission',
                                            event_label: '" . encode_url($carrier->title)  . "'
                                        });
                                    "]) !!}
                                        {{ Form::email('email', null, ['id' => 'email', 'placeholder' => 'Email Address']) }}
                                        {{ Form::hidden('product_id', $carrier->partNo, ['id' => 'product_id']) }}
                                        {{ Form::hidden('product_name', $carrier->title, ['id' => 'product_name']) }}
                                        {{ Form::hidden('product_url', route('bike_rack.view', [encode_url($carrier->slug)]), ['id' => 'product_url']) }}
                                        {{ Form::hidden('checkbox_for_subscription', false, ['id' => 'checkbox_for_subscription']) }}

                                        <button type="submit" class="btn btn-primary btn-green-basic">Notify Me</button>
                                        <input type="checkbox" name="placeholder_for_checkbox" id="placeholder_for_checkbox">
                                        <label for="placeholder_for_checkbox">Would you also like to subscribe to news and offers from Witter?</label>
                                    {!! Form::close() !!}
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            @else
                <p class="none-found">Oops, we can't find a bike rack that meets your needs. Have a go at refining your requirements and we'll try again.</p>
            @endif
        </div> <!-- .row -->
	</div> <!-- .container -->
@stop

@section('scripts')
    <script>
        @include('partials.analytics.list', [
            'products' => $carriers,
            'page' => 1,
            'per_page' => 9999,
            'list_name' => 'Category Results',
            'category' => 'Bike Racks'
        ])

        $(document).ready(function() {
            var to_scroll = $('.cycle-carriers').data('scroll');

            if(to_scroll == 1) {
                $('html, body').animate({
                    scrollTop: $("#bike_carriers_submit").offset().top
                }, 1000);
            }

            $('.btn-oos-model').on('click', function(e) {
                e.preventDefault();
                $(this).parent().parent().next('.out-of-stock-modal').addClass('active');
            });

            $('.close-modal').on('click', function(e) {
                e.preventDefault();
                $(this).parent().removeClass('active');
            });

            $('#placeholder_for_checkbox').on('click', function(e) {
                e.preventDefault();
                if($(this).hasClass('true')) {
                    $(this).removeClass('true');
                    $('#checkbox_for_subscription').val(false);
                } else {
                    $(this).addClass('true');
                    $('#checkbox_for_subscription').val(true);
                }
            });

            $('select').on('change', function() {
                $('#bike_carriers_submit').submit();
            });

            $('#bike_carriers_submit').submit(function(e) {
                var amount_of_bikes     = $('#amount_of_bikes').val(),
                    type_of_connection  = $('#type_of_connection').val(),
                    e_bike              = $('#e_bike').val(),
                    url                 = window.location.pathname;

                url = url.substr(0, url.indexOf('/'));

                e.preventDefault();

                if (e_bike == 'Yes') {
                    window.location.href = url + '/bike-racks?amount=' + amount_of_bikes + '&type=' + type_of_connection + '&ebike=' + e_bike;
                } else if (amount_of_bikes && type_of_connection) {
                    window.location.href = url + '/bike-racks?amount=' + amount_of_bikes + '&type=' + type_of_connection;
                }
            });

            var window_width = $(window).width();
            if(window_width < 992){
                $('#amount_of_bikes option').first().text('How many bikes?');
                $('#type_of_connection option').first().text('What type of connection?');
                $('#e_bike option').first().text('Do you need to carry e-bikes?');
            }

            var amountDefault = $('#amount_of_bikes').data('default');
            var typeDefault = $('#type_of_connection').data('default');
            var ebikeDefault = $('#e_bike').data('default');

            if(amountDefault != 'any') {
                $('#amount_of_bikes').val(amountDefault);
            }

            if(typeDefault != 'any') {
                $('#type_of_connection').val(typeDefault);
            }

            if(ebikeDefault != 'No') {
                $('#e_bike').val(ebikeDefault);
            }
        });
    </script>
@stop
