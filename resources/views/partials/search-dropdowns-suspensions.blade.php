

<div class="towbar-horizontal-search suspension">
    <div class="manufacturer-dropdowns search-dropdown-wrapper">
        <p>Find your suspension kit</p>
        <div class="form-wrapper">
            {!! Form::open(['id' => 'search-dropdown-form', 'onsubmit' => "
                gtag('event', 'Success', {
                    event_category: 'Model select',
                    event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                });
            "]) !!}

            @if($display_manufacturers)
                <div class="form-group">
                    <label for="manufacturer" class="sr-only">Choose Make</label>
                    <div class="car-dropdowns-select">
                        <select name="manufacturer" id="manufacturer" class="" data-style="btn-select-box">
                            <option>Make</option>
                            @if(!empty($manufacturers_dropdown))
                                @foreach($manufacturers_dropdown as $mdrop)
                                    <option value="{{ $mdrop->manufacturerId }}" data-slug="{{ $mdrop->slug }}"
                                        @if(isset($vehicle_dropdown['manufacturer_id']) && $vehicle_dropdown['manufacturer_id'] == $mdrop->manufacturerId)
                                            selected="selected"
                                            @php $manufacturer_slug = $mdrop->slug @endphp
                                        @endif >{{ $mdrop->manufacturerName }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            @endif

            @if($display_models)

                <div class="form-group">
                    <label for="model" class="sr-only">Choose Model</label>
                    <div class="car-dropdowns-select">
                        <select name="model" id="model" class="" data-style="btn-select-box">
                            <option>Model</option>
                            @if(!empty($models_dropdown))
                                @foreach($models_dropdown as $mdrop)
                                    <option value="{{ $mdrop->modelId }}" data-slug="{{ $mdrop->slug }}"
                                        @if(isset($vehicle_dropdown['model_id']) && $vehicle_dropdown['model_id'] == $mdrop->modelId)
                                            selected="selected"
                                            @php $model_slug = $mdrop->slug @endphp
                                        @endif >{{ $mdrop->modelName }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            @endif

            <div class="form-group">
                <label for="body" class="sr-only">Choose vehicle</label>
                <div class="car-dropdowns-select">
                    <select name="body" id="body" class="" data-style="btn-select-box">
                        <option>Body</option>
                        @if(!empty($vehicles_dropdown))
                            @foreach($vehicles_dropdown as $mdrop)
                                <option value="{{ $mdrop->vehicleId }}" data-slug="{{ $mdrop->slug }}"
                                    @if(isset($vehicle_dropdown['body_id']) && $vehicle_dropdown['body_id'] == $mdrop->slug)
                                        selected="selected"
                                        @php $body_slug = $mdrop->slug @endphp
                                    @endif >
                                        {{ $mdrop->full_name }}
                                    </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div><!-- /.form-group -->

            {{ Form::hidden('manufacturerSlug', empty($manufacturer_slug) ? '' : $manufacturer_slug, ['id' => 'manufacturerSlug']) }}
            {{ Form::hidden('modelSlug', empty($model_slug) ? '' : $model_slug, ['id' => 'modelSlug']) }}
            {{ Form::hidden('bodySlug', empty($body_slug) ? '' : $body_slug, ['id' => 'bodySlug']) }}
            {{ Form::hidden('registrationId', '', ['id' => 'registrationId']) }}

            <div class="form-group">
                <button type="submit" class="btn btn-primary pull-right"></button>
            </div>

            {!! Form::close() !!}
            <div class="car-dropdowns-loading"></div>
        </div>
    </div><!-- /.manufacturer-dropdowns -->
</div><!-- /.towbar-horizontal-search -->

@section('scripts')
    @parent
    <script type="text/javascript">

        // Set up the dropdowns
        $(function() {

            // Send the full url of where our vehicle ajax endpoints are
            setUpVehicleDropdowns( "{{ $vehicle_dropdown_url ?? URL('vehicles') }}" );

            // Send the starting/first slug to build the url and redirect the user.
            setUpVehicleRedirect("/{{ $redirect_slug }}/");
        });

    </script>
@stop

