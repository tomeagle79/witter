@extends('layouts.frontend')

@section('title', $kit->title)

@section('heading')
    {{ $kit->title }}
@stop

@section('meta_description')
    Witter Towbars, {{ $kit->title }}
@stop

@section('content')
	<div class="container witter-product view-accessory">
		<div class="row">
			<div class="col-md-12 accessory-listing" itemscope itemtype="http://schema.org/Product">

				<h2 class="border-bottom no-top-margin" itemprop="name">{{ $kit->title }}</h2>

				<div class="row">
                    <div class="col-md-4">
                        <div class="bordered img-center">
                            <img src="{{ image_load($kit->imageUrl) }}" alt="" itemprop="image">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="details" itemprop="description">{!! nl2br($kit->details) !!}</div> <!-- .details -->

                        <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            &pound;<span itemprop="price">{{ price($kit->price) }}</span>
                            <meta itemprop="priceCurrency" content="GBP" />
                            <link itemprop="availability" href="http://schema.org/InStock"/>
                        </div> <!-- .price -->
                        <a href="{{ route('add_to_cart', [
                            'type' => 'electrical-kit',
                            'partNo' => $kit->partNo
                        ]) }}" class="btn btn-primary">Add to Cart</a>
                    </div> <!-- .col-md-9 -->
                </div> <!-- .row -->

                <h3>Additional Information</h3>

                <div class="bordered">
                    {!! nl2br($kit->moreDetails) !!}
                </div> <!-- .bordered -->

			</div> <!-- .col-md-9 -->
		</div> <!-- .row -->
	</div> <!-- .container -->
@stop

@section('scripts')
    <script>
        @include('partials.analytics.product-view', [
            'product' => $kit,
            'category' => 'Electrical Kits'
        ])
    </script>
@stop