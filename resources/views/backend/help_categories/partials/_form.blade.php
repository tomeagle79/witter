<div class="form-group required">
	<label for="first_name" class="col-md-2 control-label">Title</label>
	<div class="col-md-4">
		{!! Form::text('title', ( ($type == 'edit') ? $blog_category->title : null), ['class' => 'form-control title-for-slug', 'id' => 'title', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="url_slug" class="col-md-2 control-label">URL Slug</label>
	<div class="col-md-4">
		{!! Form::text('url_slug', ( isset($blog_category) ? $blog_category->url_slug : null), ['class' => 'form-control slug-generated', 'id' => 'slug']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-md-offset-2 col-md-4">
		<button type="submit" class="btn btn green">{{ $submit_text }}</button>
	</div>
</div>