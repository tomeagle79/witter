<?php

namespace App\Models\Api\Commercial;

use  App\Models\Api\ApiModel;

class CommercialRoofdecks extends ApiModel
{
	const URI = "roofdecks";

	/**
	 * Get a list
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_single_where(array $args)
	{
		$url = config('witter.api_base') . self::URI . '?' . http_build_query($args);
		$obj = self::get_request($url);
		
		return $obj;
	}
}
