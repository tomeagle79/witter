@extends('layouts.backend')

@section('title')
	Edit FAQ 
@stop

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="" autocomplete="no" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@include('backend/faqs/partials/_form', ['submit_text' => 'Edit FAQ', 'type' => 'edit'])
	</form>

@stop
