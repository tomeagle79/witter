<?php

namespace App\Http\Controllers;

use Artisan;

class RecklessController extends Controller
{
    // Run migrations
    public function migrate()
    {
        /*
        $base_path = base_path();
        $cmd = 'php '. $base_path .'/artisan migrate';

        echo $cmd;

        echo '<pre>';
        $output = shell_exec(escapeshellcmd($cmd)); // Don't use Artisan::call as it doesn't provide output
        echo $output;
         */
        set_time_limit(0);
        $exitCode = Artisan::call('migrate');

        echo $exitCode;

        exit;
    }

    // Clear cache
    public function clear_cache()
    {
        set_time_limit(0);
        $exitCode = Artisan::call('cache:clear');

        echo $exitCode;

        exit;
    }

    // Refresh Twitter cache
    public function tweets()
    {
        set_time_limit(0);
        $exitCode = Artisan::call('twitter:refresh');

        echo $exitCode;

        exit;
    }
}
