<?php

    // Just set the cart count data here straight from the CartTotalItems middleware.
    $cart_total_items = request()->get('cart_total_items');
?>

  <header>
    <!-- Top Nav -->
    <div class="top-bar">
      <div class="container">
        <div class="row">
          <div class="col-xs-3 col-md-2 col-logo">
            <div class="navbar-brand">
              <a href="/">
                <img src="{{URL::asset('/assets/img/logo.png')}}" alt="Witter Towbars logo">
              </a>
            </div>
          </div>
          <!-- /.col-xs-12 -->
          <div class="col-xs-12 col-buttons">
            <ul class="block-link pull-right menu-buttons">
              <li>
                <a href="tel:01244284555" class="call-us">
                  <p>Call us on</p>
                  <p>
                    <strong>01244 284 555</strong>
                  </p>
                </a>
              </li>
              <li class="hidden-xs hidden-sm">
                <a href="https://tz.witter-towbars.co.uk" class="btn btn-header">Trade</a>
              </li>

              <li class="hidden-xs hidden-sm">
                <a href="/contact-us" class="btn btn-header btn-contact btn-has-icon">Contact</a>
              </li>
              <li class="hidden-xs hidden-sm">
                <a href="/service-centres" class="btn btn-header btn-has-icon btn-approved">Approved <span>Service</span> Centres</a>
              </li>
              <li class="hidden-xs hidden-sm">
                <a href="{{ URL::route('help_articles') }}" class="btn btn-header">Help &amp; Advice</a>
              </li>
              <li class="">
                @if(!empty($cart_total_items))
                <a class="btn btn-basket" href="{{ route('cart') }}">{{ $cart_total_items }}</a>
                @else
                <a class="btn btn-basket btn-basket-empty" href="{{ route('cart') }}">0</a>
                @endif
              </li>
            </ul>
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </div>
    <nav class="navbar navbar-witter" role="navigation">
      <div class="container">
        <div class="wrapper-navbars">

          <ul class="nav navbar-nav navbar-right navbar-main">
            <li {{ Request::is( 'towbars') || Request::is( 'towbars/*') || Request::is( 'towbar/*') ? 'class=active' : '' }}>
              <a href="{{ URL::route('towbars') }}">
                <span class="sr-only">Witter</span> Towbars</a>
            </li>
            <li {{ (Request::is( 'bike-racks') || Request::is( 'bike-racks/*')) ? 'class=active' : '' }}>
              <a href="{{ URL::route('bike-racks') }}">Bike Racks</a>
            </li>
            <li {{ (Request::is( 'caravan-movers') || Request::is( 'caravan-movers/*')) ? 'class=active' : '' }}>
              <a href="{{ URL::route('caravan_movers') }}">Caravan Movers</a>
            </li>

            <li class="hidden-md hidden-lg menu-icon">
              <span></span>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right navbar-secondary">
            <li {{ (Request::is( 'electrical-kits') || Request::is( 'electrical-kits/*')) ? 'class=active' : '' }}>
              <a href="{{ URL::route('electrics') }}">Electrical Kits</a>
            </li>

            <li {{ (Request::is( 'roof-systems') || Request::is( 'roof-systems/*')) ? 'class=active' : '' }}>
              <a href="{{ URL::route('roof-systems') }}">Roof Bars</a>
            </li>

            <li {{ (Request::is( 'vehicle-trackers') || Request::is( 'vehicle-trackers/*')) ? 'class=active' : '' }}>
              <a href="{{ URL::route('trackers.index') }}">GPS Car Trackers</a>
            </li>

            <li class="has-subnav {{ (Request::is( 'accessories') || Request::is( 'accessories/*') || Request::is( 'accessory/*')) ? 'active' : '' }}">
              <span class="icon-down-arrow icon-toggle-subnav"></span>
              <a href="{{ URL::route('accessories') }}">Accessories</a>
              <ul class="subnav">
                <li>
                  <a href="/accessories/couplings">Couplings</a>
                </li>
                <li>
                  <a href="/accessories/trackers-pdcs-alarms">Safety &amp; Security Accessories</a>
                </li>
                <li>
                  <a href="/accessories/covers-shields">Neck Accessories</a>
                </li>
                <li>
                  <a href="/accessories/keys">Keys</a>
                </li>
                <li>
                  <a href="/accessories/electric-kits-standard">Electric Kits Standard</a>
                </li>
                <li>
                  <a href="/accessories/electrical-accessories">Electrical Accessories</a>
                </li>
                <li>
                  <a href="/accessories/cycle-accessories">Cycle Carrier Accessories</a>
                </li>
                <li>
                  <a href="/accessories/towsteps">Towsteps</a>
                </li>
                <li>
                  <a href="/accessories/fixed-neck">Fixed necks</a>
                </li>
                <li>
                  <a href="/accessories/detachable-kits">Detachable Neck Kits</a>
                </li>
                <li>
                  <a href="/accessories/roof-boxes">Roof Boxes</a>
                </li>
                <li>
                  <a href="/accessories/winches">Winches</a>
                </li>
              </ul>
            </li>
            <li {{ Request::is( 'air-suspension-kits') || Request::is( 'air-suspension-kits/*') ? 'class=active' : '' }}>
              <a href="{{ URL::route('suspensions') }}">Air Suspension</a>
            </li>
            <li class="hidden-md hidden-lg">
              <a href="/service-centres">Find an approved centre</a>
              <span class="list-item-icon approved"></span>
            </li>
            <li id="help" class=" hidden-md hidden-lg {{ Request::is( 'help-advice') || Request::is( 'help-advice/*') || Request::is( 'faqs') || Request::is( 'faqs/*') ? 'active' : '' }}">
              <a href="{{ URL::route('help_articles') }}">Help &amp; Advice</a>
              <span class="list-item-icon help"></span>
            </li>
            <li class="hidden-md hidden-lg">
              <a href="/contact-us">Contact</a>
              <span class="list-item-icon contact"></span>
            </li>
            <li class="hidden-md hidden-lg">
              <a href="https://tz.witter-towbars.co.uk">Trade</a>
              <span class="list-item-icon trade"></span>
            </li>
          </ul>
        </div>

      </div>
      <!-- /.container -->
    </nav>

  </header>