
<div class="checkout-widget checkout-widget-horizontal">

    <div class="row">

        <div class="col-xs-4">
            <p class="checkout-widget-copy">Total price:</p><!-- /.copy -->
        </div><!-- /.col-sm-4 -->

        <div class="col-xs-8">
            <div class="row">

                <div class="col-md-7  col-lg-6 @if(!empty($checkout_step) && $checkout_step == 4) col-md-offset-5 col-lg-offset-6 @endif">
                    <div class="price-wrapper {{ (!empty($checkout_step) && $checkout_step == 4) ? 'checkout-complete' : '' }}">
                            <p> &pound;<span class="final-price">{{ price($cart['grand_total']) }}</span><span class="vat">Inc. VAT</span></p>
                    </div> <!-- .price-wrapper  -->
                </div><!-- /.col-md-6 -->

                @if(!empty($checkout_step) && $checkout_step == 3)
                    <div class="col-md-5  col-lg-6 hidden-sm hidden-xs">
                        <button type="submit" class="btn btn-tertiary btn-large  make-payment">Buy now</button>

                        <div class="paypal-button" style="display:none;" id="paypal-button-3"></div>

                    </div><!-- /.col-md-5 -->
                @endif

            </div><!-- /.row -->

        </div><!-- /.col-md-8 -->
    </div><!-- /.row -->
</div>

