<?php

namespace App\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class LargeThumbnail implements FilterInterface
{

    public function applyFilter(Image $image)
    {
        return $image->resize(580, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }
}
