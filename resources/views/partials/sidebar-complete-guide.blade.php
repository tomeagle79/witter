<div class="col-xs-12 col-sm-3 col-md-3 col-sm-pull-9 col-md-pull-9 sidebar-complete-guide">
    <div class="sidebar">
        <div class="sidebar-heading">Towbar Overview</div>
        <ul>
            <a class="smoothscroll" href="#types" data-open-panel="collapseOne"><li>The Different Types of Towbars</li></a>
            <a class="smoothscroll" href="#advantages" data-open-panel="collapseOne"><li>The Advantages and Disadvantages of Using Each Towbar Type</li></a>
        </ul>

        <div class="sidebar-heading">Towbar Electrics</div>
        <ul>
            <a class="smoothscroll" href="#electrics-for-my-towbar" data-open-panel="collapseTwo"><li>Do I Need Electrics for My Towbar?</li></a>
            <a class="smoothscroll" href="#how-towbar-electrics-used" data-open-panel="collapseTwo"><li>What are the Electrics Used For?</li></a>
            <a class="smoothscroll" href="#which-electric-kit" data-open-panel="collapseTwo"><li>Should I Buy a Dedicated or Universal Electric Kit?</li></a>
        </ul>

        <div class="sidebar-heading">Towbar Fitting</div>
        <ul>
            <a class="smoothscroll" href="#cost-to-fit-towbar" data-open-panel="collapseThree"><li>How Much Does it Cost to Fit a Towbar?</li></a>
            <a class="smoothscroll" href="#fit-a-towbar-myself" data-open-panel="collapseThree"><li>Can I Fit a Towbar Myself?</li></a>
            <a class="smoothscroll" href="#towbar-affect-insurance" data-open-panel="collapseThree"><li>Can Fitting a Towbar Affect My Insurance?</li></a>
        </ul>
    </div>

    <a href="#top" class="btn btn-primary back-to-top hidden-xs smoothscroll" >Back to top</a>

</div>
