
<div class="card-basic ">
    <h2 class="card-basic-title-small "><a href="{{ route('service-centres.show', $centre->centreSlug) }}">{!! $centre->centreName !!}</a></h2>
    <hr>

    <div class="row">

        @if(!empty($centre->isApprovedCentre) || !empty($centre->canMobileFit))
            <div class="col-xs-2 col-xs-push-10 approved-centre-icon">

                @if($centre->isApprovedCentre)
                    <p><i class="icon-tick" title="Approved centre"></i></p>
                @endif

                @if(!empty($centre->canMobileFit))
                    <i class="icon-mobile-fitter" title="Mobile fitter"></i>
                @endif
            </div><!-- /.col-sm-3 -->

            <div class="col-xs-10 col-xs-pull-2">
        @else
            <div class="col-xs-12">
        @endif
            <p>
                @if(!empty($centre->address1)){{ $centre->address1 }}<br>@endif
                @if(!empty($centre->address2)){{ $centre->address2 }}<br>@endif
                @if(!empty($centre->address3)){{ $centre->address3 }}<br>@endif
                @if(!empty($centre->city)){{ $centre->city }}<br>@endif
                @if(!empty($centre->county)){{ $centre->county }}<br>@endif
                @if(!empty($centre->postcode)){{ $centre->postcode }}@endif
            </p>
        </div><!-- /.col-sm-9 -->

    </div><!-- /.row -->

    <a href="{{ route('service-centres.show', $centre->centreSlug) }}" class="btn btn-primary btn-primary-small">View full details</a>
</div><!-- /.card-basic -->
