<?php

namespace App\Models\Api\Suspension;

use App\Models\Api\ApiModel;

class SuspensionAccessory extends ApiModel
{
    /**
     * Get all models based on model ID
     *
     * @param Int
     * @return Object
     */
    public static function get_by_slug_where($args)
    {
        $url = config('witter.api_base') . 'suspensions/vehicles/slug/accessories?' . http_build_query($args);
        $obj = self::get_request($url);

        return $obj;
    }
}
