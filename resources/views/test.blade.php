<!DOCTYPE html>
<html lang="en-GB">
    <head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123600959-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-123600959-1');

gtag('event', 'view_item_list', {
    'items': [{
        'name': 'Android Warhol T-Shirt',       // Name or ID is required.
        'id': 'P12345',
        'price': '99.99',
        'brand': 'Google',
        'category': 'T-Shirts',
        'list_name': 'Search Results',
        'position': 1
    }, {
        'name': 'YouTube Organic T-Shirt',       // Name or ID is required.
        'id': 'P67890',
        'price': '199.99',
        'brand': 'YouTube',
        'category': 'T-Shirts',
        'list_name': 'Search Results',
        'position': 2
    }]
});
</script>
</head>
<body>
    Content
</body>
</html>