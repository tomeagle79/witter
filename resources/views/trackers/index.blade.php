@extends('layouts.frontend')

@section('title', 'GPS Trackers for Cars - Car Tracker UK')

@section('heading')
    GPS Car Trackers
@stop

@section('meta_description')
	{{ $category->metaDescription }}
@stop

@section('content')
	<div class="container vehicle-trackers">
		<div class="row">

			<div class="col-sm-8 col-md-9 col-md-push-3">

    			<h2>Introduction to vehicle trackers</h2>

                <p>Easy to install and straightforward to use, buying a GPS tracker is a simple and effective way to defend your vehicle against car theft. Hidden in a discreet location on your vehicle, a GPS tracker is an advanced technological device that can allow you to check the location and status of your vehicle using your smartphone or tablet device. Plus, with the home office reporting vehicle thefts hitting record numbers last year, there’s never been a better time to buy a GPS tracker to protect your vehicle.</p>

                <div class="description">
                    <p>{{ nl2br((empty($subcategory->description) ? '' : $subcategory->description)) }}</p>
                </div>

                <div class="filter-wrapper">
                    @if(!empty($brand_filters))
                        <div class="filter">
                            <h3>Got a particular brand in mind?</h3>
                            <div class="row">
                                @foreach($brand_filters as $key => $value)

                                    <div class="col-md-3">
                                        @if(isset($filters['price']))
                                            <a href="{{ route('trackers.index', ['brand' => $key, 'price' => $filters['price']]) }}"
                                        @else
                                            <a href="{{ route('trackers.index', ['brand' => $key]) }}"
                                        @endif
                                        @if(isset($filters['brand']) && $filters['brand'] == $key)
                                            class="filter-link active"
                                        @else
                                            class="filter-link"
                                        @endif
                                            >
                                                <img src="/img/brands/{{ $key }}-logo.png" alt="{{ $value }}" width="140" />
                                            </a>
                                    </div><!-- /.col-md-3 -->
                                @endforeach
                            </div><!-- /.row -->
                        </div><!-- /.filter -->
                    @endif

                    @if(!empty($price_filters))
                        <div class="filter">
                            <h3>Do you have a budget?</h3>
                            <div class="row">
                                @foreach($price_filters as $key => $value)

                                    <div class="col-md-3">
                                        @if(isset($filters['brand']))
                                            <a href="{{ route('trackers.index', ['brand' => $filters['brand'], 'price' => $key]) }}"
                                        @else
                                            <a href="{{ route('trackers.index', ['price' => $key]) }}"
                                        @endif
                                        @if(isset($filters['price']) && $filters['price'] == $key)
                                            class="filter-link active"
                                        @else
                                            class="filter-link"
                                        @endif
                                            >
                                                <span>{!! $value !!}</span>
                                            </a>
                                    </div><!-- /.col-md-3 -->
                                @endforeach
                            </div><!-- /.row -->
                        </div><!-- /.filter -->
                    @endif

                    <div class="filter-count container">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                {{ $trackers->count() }} Products match your selection
                            </div><!-- /.col-sm-6 -->

                            <div class="col-xs-12 col-sm-6 text-right">
                                <a href="#" class="btn btn-green-basic view-matching-products">VIEW MATCHING PRODUCTS</a>
                            </div><!-- /.col-sm-6 -->

                        </div><!-- /.row -->

                    </div><!-- /.filter-count -->

                </div><!-- /.filter-box -->

                <div class="row">
                    <div class="col-xs-12">
                        <div class="filters-selection">

                            @if(!empty($filters))
                                <span>YOUR FILTERS</span>
                            @endif

                            @if(isset($filters['brand']))

                                @if(isset($filters['price']))
                                    <a href="{{ route('trackers.index', ['price' => $filters['price']]) }}"
                                @else
                                    <a href="{{ route('trackers.index') }}"
                                @endif
                                    class="btn btn-remove">
                                        <span>{{ $filters['brand'] }}</span>
                                    </a>
                            @endif

                            @if(isset($filters['price']))

                                @if(isset($filters['brand']))
                                    <a href="{{ route('trackers.index', ['brand' => $filters['brand']]) }}"
                                @else
                                    <a href="{{ route('trackers.index') }}"
                                @endif
                                    class="btn btn-remove">
                                        <span>{!! $price_filters[$filters['price']] !!}</span>
                                    </a>
                            @endif

                        </div><!-- /.filter-selection -->
                    </div><!-- /.col-sm-12 -->
                </div><!-- /.row -->

                <div class="row item-list is-flex">

                    @foreach($trackers as $accessory)

                        @include('partials.analytics.json_product_details', [
                            'product' => $accessory,
                            'list_name' => 'Category Results',
                            'category' => 'Trackers',
                            'position' => $loop->iteration
                        ])

                        <div class="col-sm-6 col-md-4">
                            <div class="item">
                                <a href="{!! route('accessories.multi', [encode_url($accessory->slug)]) !!}" class="product-link ribbon-conatiner" data-product-id="{{ $accessory->slug }}">

                                    @if($accessory->isFittable)
                                        @include('partials.ribbon.webfit')
                                    @endif
                                    <img src="{{ image_load($accessory->imageUrl) }}" alt="" /></a>

                                <a href="{!! route('accessories.multi', [encode_url($accessory->slug)]) !!}" class="product-link" data-product-id="{{ $accessory->slug }}"><h3 class="list-title">{!! $accessory->title !!}</h3></a>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <span class="available available-green-tick">Available</span>
                                    </div><!-- /.col-xs-6 -->

                                    <div class="col-xs-6 text-right">
                                        <span class="total">
                                            &pound;{{ price($accessory->displayPrice) }}
                                        </span>
                                    </div><!-- /.col-xs-6 -->

                                </div><!-- /.row -->

                                <div class="row">
                                    <div class="col-xs-6">

                                    </div><!-- /.col-xs-6 -->

                                    <div class="col-xs-6 text-right">
                                        <a href="{!! route('accessories.multi', [encode_url($accessory->slug)]) !!}" class="btn btn-green-basic product-link" data-product-id="{{ $accessory->partNo }}">View product</a>
                                    </div><!-- /.col-xs-6 -->

                                </div><!-- /.row -->
                                <hr>
                            </div>
                        </div>

                    @endforeach

                </div> <!-- .row -->

                <h2>What is a GPS Tracker?</h2>

                <p>A GPS tracker is a small electronic device that is installed on your vehicle. Usually located on the underside of your car, the device uses GPS technology with movement detection sensors to monitor the status and security of your car. In the event of a car theft, the tracking device will be able to provide you and the police with speed, direction and location details. Advanced tracking devices will also be able to give you tamper alerts, towing alerts and low vehicle battery alerts.</p>

                <p>You should also note that only high quality devices like the Scorpion GPS Tracker devices provide an international security command centre that helps monitor the status of your vehicle.</p>


                <h2>Why buy a GPS Tracker?</h2>

                <p>As car and van prices continue to rise, the unfortunate downside is that your vehicle becomes a more lucrative target for car thieves.</p>

                <p>Often stolen overnight while you sleep, vehicles are frequently whisked away to foreign countries or dismantling factories before you even wake.</p>

                <p>Fortunately, a car thief taking your car doesn’t have to be the end for your vehicle.</p>

                <p>Using a GPS tracker not only allows you to check the status and location of your vehicle, but, if you buy a Scorpion or Bulldog Tracker, your tracker can actually notify you of any possible theft.</p>

                <p>Some devices like the Scorpion GPS Tracker devices also provide international security command centre coverage. The command centre actively monitors the status of your vehicle.</p>



                <h2>How do I install a GPS tracker?</h2>

                <p>There are three ways to install a GPS tracker. </p>

                <h4>1. Magnetic Installation</h4>

                <p>Some GPS trackers are magnetic and simply attach to anything metal in or outside of the car.</p>

                <h4>2. Plug-In Installation</h4>

                <p>Some GPS trackers simply plug-in to the on board diagnostic socket.</p>

                <h4>3. Wired Installation</h4>

                <p>The third and least visible option is a wired-in solution which requires professional installation. This involves correctly fitting movement detection sensors to ensure your vehicle is fully secure. Due to the technical difficulties of the job, our fully fitted prices include the fitting service provided by an approved partner organisation, with the purchase of one of our trackers. Our fully inclusive fitting services offer:</p>

                <ul>
                    <li>Professional tracker installation</li>
                    <li>A fitting at a time and place to suit you</li>
                    <li>All labour is guaranteed for a minimum of 1 year</li>
                </ul>

                <h2>Which GPS tracker should I buy?</h2>

                <p>All trackers sold here at Witter actively protect your vehicle against theft using unauthorised movement and GPS technology.</p>

                <p>Saying that, some of the trackers we sell offer a range of different features and benefits.</p>

                <p>Find the main features for each tracker brand below.</p>

                <h2>Scorpion Tracker</h2>

                <ul>
                    <li>GPS Accurate to within 10 metres</li>
                    <li>Tow-away alert</li>
                    <li>Tamper alert</li>
                    <li>Low vehicle battery text alert</li>
                    <li>Location on demand via the internet or smartphone</li>
                    <li>Speed, location, and signal strength notifications</li>
                    <li>Command centre protection</li>
                </ul>

                <h2>Bulldog Tracker</h2>

                <ul>
                    <li>GPS Accurate to within 10 metres</li>
                    <li>Tow-away alert</li>
                    <li>Tamper alert</li>
                    <li>Remote engine immobilisation (must be fitted with a immobiliser kit)</li>
                    <li>View history, speed and tracking reports</li>
                    <li>Location on demand via the internet or smartphone</li>
                    <li>Speed, location, and signal strength notifications</li>
                    <li>Unlike many other tracker providers, this tracker does not limit how many people can access the tracker via the app</li>
                </ul>

			</div> <!-- .col-md-9 -->
			<div class="col-sm-4 col-md-3 col-md-pull-9">

				@include('partials.sidebar-trackers')

                @include('partials.sidebar-trackers-fleet')

			</div> <!-- .col-md-3 -->
		</div> <!-- .row -->
	</div> <!-- .container -->
@stop

@section('scripts')
    <script>
        @include('partials.analytics.list', [
            'products' => $trackers,
            'page' => 1,
            'per_page' => 999,
            'list_name' => 'Category Results',
            'category' => 'Trackers'
        ])
    </script>
@stop

