<?php

namespace App\Http\Controllers;

use App\Models\Api\Settings;
use App\Models\Api\Towbar;
use App\Models\Api\Vehicle;
use App\Models\Api\VehicleBody;
use App\Models\Api\VehicleManufacturer;
use App\Models\Api\VehicleModel;
use App\Models\Api\VehicleRegistration;
use Illuminate\Http\Request;
use Session;

class TowbarController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Witter Towbars', route('towbars'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['vehicle_dropdown'] = $vehicle_dropdown = Session::get('vehicle_dropdown', []);

        // Get all the manufacturers
        $this->data['manufacturers'] = $this->data['manufacturers_dropdown'] = $data = VehicleManufacturer::get_all();

        // Get all the manufacturer's models if the manufacturer_id is set
        if (isset($vehicle_dropdown['manufacturer_id'])) {
            $this->data['models_dropdown'] = VehicleModel::get_all_from_manufacturer_id($vehicle_dropdown['manufacturer_id']);
        }

        // Get all the model's bodies if the model_id is set
        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['bodies_dropdown'] = VehicleBody::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        // Get all the body registrations if the body_id is set
        if (isset($vehicle_dropdown['body_id'])) {
            $this->data['registrations_dropdown'] = VehicleRegistration::get_all_from_body_id($vehicle_dropdown['body_id']);
        }

        return view('towbars.index', $this->data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index2()
    {
        $this->data['vehicle_dropdown'] = $vehicle_dropdown = Session::get('vehicle_dropdown', []);

        // Get all the manufacturers
        $this->data['manufacturers'] = $this->data['manufacturers_dropdown'] = $data = VehicleManufacturer::get_all();

        // Get all the manufacturer's models if the manufacturer_id is set
        if (isset($vehicle_dropdown['manufacturer_id'])) {
            $this->data['models_dropdown'] = VehicleModel::get_all_from_manufacturer_id($vehicle_dropdown['manufacturer_id']);
        }

        // Get all the model's bodies if the model_id is set
        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['bodies_dropdown'] = VehicleBody::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        // Get all the body registrations if the body_id is set
        if (isset($vehicle_dropdown['body_id'])) {
            $this->data['registrations_dropdown'] = VehicleRegistration::get_all_from_body_id($vehicle_dropdown['body_id']);
        }

        return view('towbars.index2', $this->data);
    }

    /**
     * Display a listing models.
     *
     * @return \Illuminate\Http\Response
     */
    public function model_list($manufacturer_slug)
    {
        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        if (empty($manufacturer)) {
            return redirect()->route('towbars')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['models'] = $models = VehicleModel::get_all_from_manufacturer_id($manufacturer->manufacturerId);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('towbars.model_list', $manufacturer_slug));

        // Build the vehicle_dropdown session array
        $this->data['vehicle_dropdown'] = $vehicle_dropdown = build_dropdown($manufacturer->manufacturerId);

        // If the manufacturer_id set then get the models for the dropdown
        if (isset($vehicle_dropdown['manufacturer_id'])) {
            $this->data['models_dropdown'] = VehicleModel::get_all_from_manufacturer_id($vehicle_dropdown['manufacturer_id']);
        }

        // If the model_id set then get the bodies for the body dropdown
        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['bodies_dropdown'] = VehicleBody::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        // If the body_id set then get the registrations for the dropdown
        if (isset($vehicle_dropdown['body_id'])) {
            $this->data['registrations_dropdown'] = VehicleRegistration::get_all_from_body_id($vehicle_dropdown['body_id']);
        }

        return view('towbars.models', $this->data);
    }

    /**
     * Display a listing body types.
     *
     * @return \Illuminate\Http\Response
     */
    public function body_list($manufacturer_slug, $model_slug)
    {
        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        // First check if the manufacturer exists
        if (empty($manufacturer)) {
            return redirect()->route('towbars')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        // Check if the model exists
        if (empty($model)) {
            return redirect()->route('towbars.model_list', $manufacturer_slug)->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $this->data['bodies'] = VehicleBody::get_all_from_model_id($model->modelId);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('towbars.model_list', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('towbars.model_list.body_list', [$manufacturer_slug, $model_slug]));

        // Build the vehicle_dropdown session array
        $vehicle_dropdown = build_dropdown($manufacturer->manufacturerId, $model->modelId);

        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['bodies_dropdown'] = VehicleBody::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        if (isset($vehicle_dropdown['body_id'])) {
            $this->data['registrations_dropdown'] = VehicleRegistration::get_all_from_body_id($vehicle_dropdown['body_id']);
        }

        $this->data['vehicle_dropdown'] = $vehicle_dropdown;

        $this->data['model_slug'] = $model_slug;

        return view('towbars.bodies', $this->data);
    }

    /**
     * List of the vehicle bodies.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registration_list($manufacturer_slug, $model_slug, $body_slug)
    {
        // Change the canonical link in the page to the parent model page.
        $this->data['canonical'] = route('towbars.model_list.body_list', [$manufacturer_slug, $model_slug]);

        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        // First check if the manufacturer exists
        if (empty($manufacturer)) {
            return redirect()->route('towbars')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        // Check if the model exists
        if (empty($model)) {
            return redirect()->route('towbars.model_list', $manufacturer_slug)->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $this->data['body'] = $body = VehicleBody::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug, 'bodySlug' => $body_slug]);

        // Check if the model exists
        if (empty($body)) {
            return redirect()->route('towbars.model_list.body_list', [$manufacturer_slug, $model_slug])->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $vehicle_dropdown = build_dropdown($body->manufacturerId, $body->modelId, $body->bodyId);

        $this->data['bodies'] = VehicleBody::get_all_from_model_id($model->modelId);

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('towbars.model_list', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('towbars.model_list.body_list', [$manufacturer_slug, $model_slug]));

        // Build the vehicle_dropdown session array
        $vehicle_dropdown = build_dropdown($manufacturer->manufacturerId, $model->modelId);

        if (isset($vehicle_dropdown['model_id'])) {
            $this->data['bodies_dropdown'] = VehicleBody::get_all_from_model_id($vehicle_dropdown['model_id']);
        }

        if (isset($vehicle_dropdown['body_id'])) {
            $this->data['registrations_dropdown'] = VehicleRegistration::get_all_from_body_id($vehicle_dropdown['body_id']);
        }

        $this->data['breadcrumbs']->addCrumb($this->data['body']->bodyName, route('towbars.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug]));

        $this->data['vehicle_dropdown'] = $vehicle_dropdown;

        $this->data['model_slug'] = $model_slug;

        $this->data['noindex_follow_page'] = 'true';

        return view('towbars.bodies', $this->data);
    }

    /**
     * Display a listing vehicles.
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicle_list($manufacturer_slug, $model_slug, $body_slug, $registration_id)
    {
        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        $this->data['noindex_follow_page'] = true;

        // First check if the manufacturer exists
        if (empty($manufacturer)) {
            return redirect()->route('towbars')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        // Check if the model exists
        if (empty($model)) {
            return redirect()->route('towbars.model_list', $manufacturer_slug)->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $this->data['body'] = $body = VehicleBody::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug, 'bodySlug' => $body_slug]);

        if (empty($body)) {
            return redirect()->route('towbars.model_list.body_list', [$manufacturer_slug, $model_slug])->with('message', 'Sorry, we could not find that body type. Please select a body type from below.');
        }

        $vehicles = Vehicle::get_where(['bodyId' => $body->bodyId, 'registrationId' => $registration_id]);

        if (empty($vehicles)) {
            return redirect()->route('towbars.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug])->with('message', 'Sorry, we could not find that year. Please select a year from below.');
        }

        // Try and sort the vehicles by most year
        $this->data['vehicles'] = collect($vehicles)->sortByDesc(function ($item, $key) {
            return preg_replace('/\D/', '', ($item->period . $item->vehicleId));
        })->values()->all();

        // Check if there is only one vehicle if so redirect to towbars.
        if (count($this->data['vehicles']) == 1) {
            return redirect()->to(route('towbars.by_selection', [$manufacturer_slug, $model_slug, $body_slug, $registration_id, $this->data['vehicles'][0]->slug]));
        }

        $this->data['registration_id'] = $registration_id;

        // Set the dropdown values as the user has navigated to this car type.
        $vehicle_dropdown = build_dropdown($manufacturer->manufacturerId, $model->modelId, $body->bodyId, $registration_id);

        // If there are no vehicles to display get the reg details for our message and form.
        if (empty($this->data['vehicles'])) {
            $registrations = VehicleRegistration::get_all_from_body_id($body->bodyId);

            foreach ($registrations as $value) {
                if ($value->registrationId == $registration_id) {
                    $this->data['registration'] = $value;
                    break;
                }
            }

            // Check if the registration_id has a valid id if not redirect back to the body
            if (empty($this->data['registration'])) {
                return redirect()->route('towbars.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug])->with('message', 'Sorry, we could not find that year. Please select a year from below.');
            }

            $this->data['back_link'] = route('towbars.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug]);
        }

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('towbars.model_list', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('towbars.model_list.body_list', [$manufacturer_slug, $model_slug]));
        $this->data['breadcrumbs']->addCrumb($this->data['body']->bodyName, route('towbars.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug]));
        $this->data['breadcrumbs']->addCrumb('Registration', route('towbars.model_list.body_list.registration_list.vehicle_list', [$manufacturer_slug, $model_slug, $body_slug, $registration_id]));

        return view('towbars.vehicles', $this->data);
    }

    /**
     * List towbars
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicle_towbars(Request $request, $manufacturer_slug, $model_slug, $body_slug, $registration_id, $vehicle_slug)
    {
        $this->data['manufacturer'] = $manufacturer = VehicleManufacturer::get_single_where(['makeSlug' => $manufacturer_slug]);

        $this->data['noindex_follow_page'] = true;

        // First check if the manufacturer exists
        if (empty($manufacturer)) {
            return redirect()->route('towbars')->with('message', 'Sorry, we could not find that manufacturer. Please select your manufacturer from below.');
        }

        $this->data['model'] = $model = VehicleModel::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug]);

        // Check if the model exists
        if (empty($model)) {
            return redirect()->route('towbars.model_list', $manufacturer_slug)->with('message', 'Sorry, we could not find that model. Please select model from below.');
        }

        $this->data['body'] = $body = VehicleBody::get_single_where(['makeSlug' => $manufacturer_slug, 'modelSlug' => $model_slug, 'bodySlug' => $body_slug]);

        // Redirect if body does not exist
        if (empty($body)) {
            return redirect()->route('towbars.model_list.body_list', [$manufacturer_slug, $model_slug])->with('message', 'Sorry, we could not find that body type. Please select a body type from below.');
        }

        $vehicles = collect(Vehicle::get_where(['bodyId' => $body->bodyId, 'registrationId' => $registration_id]));

        // Redirect if vehicles do not exist
        if (empty($vehicles)) {
            return redirect()->route('towbars.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug])->with('message', 'Sorry, we could not find that year. Please select a year from below.');
        }

        // Get a list of the available towbars using this vehicle slug
        $this->data['towbars'] = $towbars = Towbar::get_where_by_slug(['vehicleSlug' => $vehicle_slug]);

        // Set the 'ga_out_of_stock_event' to false before we loop through out towbar stock.
        $this->data['ga_some_out_of_stock'] = false;
        $this->data['ga_all_out_of_stock'] = false;

        $this->data['towbar_list'] = [];

        // If no towbars ensure we set GA as out of stock.
        if (empty($towbars->towbarTypes)) {
            $this->data['ga_all_out_of_stock'] = true;

            // Just get the age in years for our error messages
            $this->data['towbars']->age = now()->year - $towbars->startDate;

        } else {
            $this->data['towbar_list'] = $towbar_list = collect($towbars->towbarTypes);

            // Loop through the list of towbars and if there are any our of stock then create a 'ga_out_of_stock_event'
            $out_of_stock_count = 0;

            foreach ($towbar_list as $key => $towbar) {
                if (!towbar_in_stock($towbar)) {
                    $this->data['ga_some_out_of_stock'] = true;

                    $out_of_stock_count++;
                }

                if (!empty($towbar->inDevelopment)) {
                    $towbar_list->forget($key);

                    $this->data['towbar_in_development'] = true;
                }
            }

            // Check if the total number of out of stock items is all the items.
            if ($towbar_list->count() == $out_of_stock_count) {
                $this->data['ga_all_out_of_stock'] = true;
            }

            $pagination = collection_paginate($towbar_list, config('settings.pagination.towbars'));
            $pagination->withPath(request()->url());

            // $sorted = $towbar_list->sortBy('price')->sortByDesc('inStock');
            $this->data['towbar_list'] = $pagination;
        }

        $this->data['breadcrumbs']->addCrumb($this->data['manufacturer']->manufacturerName, route('towbars.model_list', $manufacturer_slug));
        $this->data['breadcrumbs']->addCrumb($this->data['model']->modelName, route('towbars.model_list.body_list', [$manufacturer_slug, $model_slug]));
        $this->data['breadcrumbs']->addCrumb($this->data['body']->bodyName, route('towbars.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body_slug]));
        $this->data['breadcrumbs']->addCrumb('Registration', route('towbars.model_list.body_list.registration_list.vehicle_list', [$manufacturer_slug, $model_slug, $body_slug, $registration_id]));
        $this->data['breadcrumbs']->addCrumb('Towbars list', route('towbars.by_selection', [$manufacturer_slug, $model_slug, $body_slug, $registration_id, $vehicle_slug]));

        // Set the breadcrumb in a flash session so can be used on the Towbar purchase page
        request()->session()->flash('breadcrumbs', $this->data['breadcrumbs']);

        return view('towbars.towbar_list', $this->data);
    }

    /**
     * List towbars
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicle_towbars_by_reg($vehicle_id, $registration_date)
    {
        // Get the users number plate if it has been set.
        if (request()->session()->has('registration_no')) {
            $this->data['numberplate'] = request()->session()->get('registration_no');
        }

        // Get a list of the available towbars using this vehicle slug
        $this->data['towbars'] = $towbars = Towbar::get_towbars_by_id_and_date($vehicle_id, $registration_date);
        $this->data['towbar_list'] = [];

        // If no photo get manufacturer logo
        if (empty($towbars->photoUrl)) {
            $this->data['manufacturer'] = VehicleManufacturer::get_by_id($towbars->manufacturerId);
        }

        // If no towbars ensure we set GA as out of stock.
        if (empty($towbars->towbarTypes)) {
            $this->data['ga_all_out_of_stock'] = true;

            // Just get the age in years for our error messages
            $this->data['towbars']->age = now()->year - $towbars->startDate;

        } else {
            $this->data['towbar_list'] = $towbar_list = collect($towbars->towbarTypes);

            // Loop through the list of towbars and if there are any our of stock then create a 'ga_out_of_stock_event'
            $out_of_stock_count = 0;

            foreach ($towbar_list as $key => $towbar) {
                if (!towbar_in_stock($towbar)) {
                    $this->data['ga_some_out_of_stock'] = true;
                    $out_of_stock_count++;
                }

                if (!empty($towbar->inDevelopment)) {
                    $towbar_list->forget($key);
                    $this->data['towbar_in_development'] = true;
                }
            }

            // Check if the total number of out of stock items is all the items.
            if ($towbar_list->count() == $out_of_stock_count) {
                $this->data['ga_all_out_of_stock'] = true;
            }

            $pagination = collection_paginate($towbar_list, config('settings.pagination.towbars'));
            $pagination->withPath(request()->url());

            // $sorted = $towbar_list->sortBy('price')->sortByDesc('inStock');
            $this->data['towbar_list'] = $pagination;
        }

        $this->data['registration_date'] = $registration_date;

        // Add vehicle to the breadcrumb
        $breadcrumb_text = trim($towbars->manufacturerName . ' ' . $towbars->modelName . ' ' . $towbars->bodyName);
        $this->data['breadcrumbs']->addCrumb($breadcrumb_text, route('towbars.vehicle_towbars_by_reg', [$vehicle_id, $registration_date]));

        // Set the breadcrumb in a flash session so can be used on the Towbar purchase page
        request()->session()->flash('breadcrumbs', $this->data['breadcrumbs']);

        return view('towbars.towbar_list', $this->data);
    }

    /**
     * View individual towbar - product page
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Request $request, $towbar_slug, $registration_date = null)
    {
        // Do we have a variant ID?
        if (!$request->input('variantTowbarId') || !$request->input('variant')) {
            return redirect(route('towbars'))->with('error', 'Please select your vehicle first');
        }

        // Get the vehicle
        $this->data['vehicle'] = base64_decode($request->input('variant'));
        $this->data['variantTowbarId'] = $request->input('variantTowbarId');

        if ($this->data['vehicle'] == false) {
            // Valid?
            return redirect(route('towbars'))->with('error', 'Please select your vehicle first');
        }

        // Get the towbar
        $this->data['towbar'] = $towbar = Towbar::get_single_where(['vehicleTowbarSlug' => $towbar_slug]);
        $this->data['towbar']->slug = $towbar_slug;
        $this->data['registration_date'] = $registration_date;

        /* This page is specific to the car model and needs the extra parameters in the URL query string
        So noindex. */
        $this->data['noindex_page'] = true;

        // If coming from the vehicles pages use the breadcrumb in the session
        $breadcrumbs = request()->session()->get('breadcrumbs');

        if (!empty($breadcrumbs)) {
            $this->data['breadcrumbs'] = $breadcrumbs;
        }
        $this->data['breadcrumbs']->addCrumb($towbar->title);

        // Get the users number plate if it has been set.
        if (request()->session()->has('registration_no')) {
            $this->data['numberplate'] = request()->session()->get('registration_no');
        } else {
            $this->data['numberplate'] = '';
        }

        $this->data['reg_search'] = true;

        // This is a flag to see if we need to include an option to allow the user to buy without an electric kit
        $this->data['electric_kit_included'] = false;

        // Check if the Towball is included. Add flag if true.
        // Set this as true. Giving them time to decide on how much to charge.
        $this->data['towball_included'] = false;

        if (empty($towbar->towballOptions)) {
            $this->data['towball_included'] = true;
        }

        // Get the software upgrade (coding) price.
        $this->data['software_upgrade_price'] = Settings::get_recode_premium();

        return view('towbars.view', $this->data);
    }
}
