<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutSection1Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'first_name' => ['required', 'max:255'],
            'surname' => ['required', 'max:255'],
            'telephone' => ['required', 'max:255'],
            'mobile' => ['max:255'],
            'email' => ['required', 'email', 'max:255'],

            'billing_postcode' => ['required', 'max:10'],
            'billing_address1' => ['required', 'max:255'],
            'billing_address2' => ['max:255'],
            'billing_towncity' => ['required', 'max:255'],
            'billing_county' => ['required', 'max:255'],
            'billing_country' => ['required', 'max:255'],
        ];
    }
}
