<h2 id="types">THE DIFFERENT TYPES OF TOWBARS</h2>

<div class="types clearfix">
    <div class="row">
        <div class="tile clearfix">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="image-wrapper">
                       <img src="/img/complete-guide/flange_neck_grey.jpg" alt="Flange neck towbar" >
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h4 class="tile-title">What is a Flange Towbar?</h4>
                    <p class="tile-description">
                         This towbar is bolted with 2 or 4 bolts to a flange plate. The flange plate is then fitted to the underside of the vehicle. This is considered to be an ugly design because of the visible bolts and components on display.
                    </p>
                </div>
            </div>

            <a href="#fixed-flange-ball-towbar" class="btn btn-grey more-info smoothscroll">Read More</a>
        </div>
    </div>
    <div class="row">
        <div class="tile clearfix">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="image-wrapper">

                        <img src="/img/complete-guide/swan_neck.jpg" alt="Swan neck towbar" >
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h4 class="tile-title">What is a Swan Neck Towbar?</h4>
                    <p class="tile-description">
                      The swan neck towbar refers to the actual shape and design of the towbar. This towbar is fixed directly to the underside of your vehicle and uses a ‘swan neck’ design to offer an unobtrusive appearance.
                    </p>
                </div>
            </div>

            <a href="#detachable-flange-towbar" class="btn btn-grey more-info smoothscroll">Read More</a>
        </div>
    </div>
    <div class="row">
        <div class="tile clearfix">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="image-wrapper">

                         <img src="/img/complete-guide/fixed_flange_grey.jpg" alt="Fixed flange towbar" >
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h4 class="tile-title">What is a Fixed Towbar?</h4>
                    <p class="tile-description">
                        As the name suggests, a fixed towbar cannot be removed easily from your vehicle. Fixed towbars are available in both swan neck and flange design.
                    </p>
                </div>
            </div>

            <a href="#fixed-flange-ball-towbar" class="btn btn-grey more-info smoothscroll">Read More</a>
        </div>
    </div>
    <div class="row">
        <div class="tile">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="image-wrapper">
                        <img src="/img/complete-guide/swan_neck_grey.jpg" alt="Swan neck towbar" >
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h4 class="tile-title">What is a Detachable Towbar?</h4>
                    <p class="tile-description">
                        Again, as the name suggests, a detachable towbar can be removed easily from your vehicle. It uses a quick release handle to allow quick and straightforward detachment.
                    </p>
                </div>
            </div>

            <a href="#detachable-flange-towbar" class="btn btn-grey more-info smoothscroll">Read More</a>
        </div>
    </div>
</div>

<hr>

<h2 id="advantages" class="border">THE ADVANTAGES AND DISADVANTAGES OF USING EACH TOWBAR TYPE</h2>

<div class="row">
    <div class="col-xs-12">
        <h5 class="tile-title" id="fixed-flange-ball-towbar">Fixed Flanged Ball Towbar</h5>
        <p>The Witter fixed flange towbar is used for all forms of towing and is generally the cheapest towbar option on the market. The design involves a 50mm towball bolted to a faceplate. This towbar design allows the use of accessories such as bumper protector plates, stabiliser car plates and towbar mounted cycle carriers.</p>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title green">Advantages</p>
        <ul>
            <li>Tow and carry cycles at the same time (subject to nose load limits)</li>
            <li>Bumper shields can be fitted</li>
            <li>AL-KO towballs can be fitted</li>
            <li>The towing height can be adjusted on commercial and 4x4 four-hole plate towbars</li>
            <li>A choice of towing couplings and accessories can be fitted</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title red">Disadvantages</p>
        <ul>
            <li>The towbar and electrics are visible</li>
            <li>The towbar has a tendency to get in the way</li>
            <li>Can damage ‘shins’</li>
            <li>May trigger reversing sensors when towing</li>
        </ul>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-xs-12">
        <h5 class="tile-title" id="detachable-flange-towbar">Detachable Flange Towbar</h5>
        <p>The Witter detachable flange towbar offers you all the versatility of the traditional flange version while only a small part of the towbar remains visible when detached. This towbar can be easily detached when not towing.</p>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title green">Advantages</p>
        <ul>
            <li>Tow and carry cycles at the same time</li>
            <li>The towball is removed when not in use preserving 'shins'</li>
            <li>AL-KO towballs can be fitted</li>
            <li>Won't trigger reversing sensors when not towing</li>
            <li>A choice of towing couplings and accessories can be fitted</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title red">Disadvantages</p>
        <ul>
            <li>Generally one of the most expensive types of towbars</li>
            <li>Some people believe the design of this towbar to be quite ugly</li>
        </ul>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-xs-12">
        <h5 class="tile-title" id="fixed-swan-neck-towbar">Fixed Swan Neck Towbar</h5>
        <p>The Witter Fixed Swan towbar is ideal for many towing applications and offers a slim, unobtrusive appearance.</p>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title green">Advantages</p>
        <ul>
            <li>Compatible with AL-KO stabilisers</li>
            <li>Less likely to trigger reversing sensors</li>
            <li>More aesthetically pleasing than a flange towbar</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title red">Disadvantages</p>
        <ul>
            <li>Towball height cannot be altered</li>
            <li>Slightly more expensive than the flange towbar</li>
            <li>Cannot be fitted with a bumper shield</li>
        </ul>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-xs-12">
        <h5 class="tile-title" id="detachable-swan-neck-towbar">Detachable Swan Neck Towbar</h5>
        <p>The Witter detachable swan neck towbar is slim, secure and simple to use. When detached only a small part of the towbar remains visible.</p>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title green">Advantages</p>
        <ul>
            <li>Compatible with AL-KO stabilisers</li>
            <li>Less likely to trigger reversing sensors</li>
            <li>More aesthetically pleasing than a flange towbar</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="tile-title red">Disadvantages</p>
        <ul>
            <li>Towball height cannot be altered</li>
            <li>Slightly more expensive than the flange towbar</li>
            <li>Cannot be fitted with a bumper shield</li>
        </ul>
    </div>
</div>
