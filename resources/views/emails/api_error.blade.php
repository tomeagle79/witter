@extends('layouts.email')

@section('content')
<p>There was an error on the Witter API</p>

<p>
    <strong>Error</strong>: {{ $error->error_text }}
</p>
<p>
    <strong>API URL</strong>: <a href="{{ $error->api_url }}">{{ $error->api_url }}</a>
</p>
<p>
    <strong>URL</strong>: <a href="{{ $error->url }}">{{ $error->url }}</a>
</p>
<p>
    <strong>IP</strong>: {{ $error->ip_address }}
</p>
<p>
    <strong>User Agent</strong>: {{ $error->user_agent }}
</p>
@stop
