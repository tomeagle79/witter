<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Api\VehicleManufacturer;
use App\Models\Api\VehicleModel;
use App\Models\Api\VehicleBody;
use App\Models\Api\VehicleRegistration;

class VehicleAjaxController extends Controller
{
    /**
     * Display a json response of the manufacturers collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_manufacturers()
    {
        $manufacturers = VehicleManufacturer::get_all();

        $data = [];

        foreach ($manufacturers as $manufacturer){
            $data[] = [
                'index' => $manufacturer->manufacturerId,
                'slug' => $manufacturer->slug,
                'name' => $manufacturer->manufacturerName
            ];
        }

        return response()->json($data);
    }

    /**
     * Display a json response of the models collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_models($id)
    {
        $models = VehicleModel::get_all_from_manufacturer_id($id);

        $data = [];

        foreach ($models as $model){
            $data[] = [
                'index' => $model->modelId,
                'slug' => $model->slug,
                'name' => $model->modelName
            ];
        }

        $vehicle_dropdown = build_dropdown($id);

        return response()->json($data);
    }

    /**
     * Display a json response of the bodies collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_bodies($id)
    {
        $bodies = VehicleBody::get_all_from_model_id($id);

        $data = [];

        foreach ($bodies as $body){
            $data[] = [
                'index' => $body->bodyId,
                'slug' => $body->slug,
                'name' => $body->bodyName
            ];
        }

        $vehicle_dropdown = build_dropdown($body->manufacturerId, $id);

        return response()->json($data);
    }

    /**
     * Display a json response of the registrations collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_registrations($id)
    {
        $data = [];
        
        if($id === null) {
            return response()->json($data);
        }

        $registrations = VehicleRegistration::get_all_from_body_id($id);

        foreach ($registrations as $registration){
            $data[] = [
                'index' => $registration->registrationId,
                'slug' => $registration->registrationId,
                'name' => $registration->registrationText
            ];
        }

        $vehicle_dropdown = build_dropdown($registration->manufacturerId, $registration->modelId, $id);

        return response()->json($data);
    }

    /**
     * Redirect the user if they post to the index dropdowns
     *
     * @return redirect
     */
    public function index_submit(Request $request)
    {
        $redirect_to = $request->segment(1);

        // If user posts an id redirect
        if(!empty(preg_replace('/\D/', '', $request->manufacturer))){

            $manufacturer = VehicleManufacturer::get_by_id($request->manufacturer);

            return redirect(route($redirect_to . '.model_list', $manufacturer->slug));
        }else{

            return redirect(route($redirect_to));
        }
    }

    /**
     * Redirect the user if they post to the index dropdowns
     *
     * @return redirect
     */
    public function model_list_submit(Request $request, $manufacturer_slug)
    {
        $redirect_to = $request->segment(1);

        // If user posts an id redirect
        if(!empty(preg_replace('/\D/', '', $request->model))){

            $model = VehicleModel::get_by_id($request->model);

            return redirect(route($redirect_to . '.model_list.body_list', [$manufacturer_slug, $model->slug]));

        }else{

            return redirect(route($redirect_to . '.model_list', $manufacturer_slug));
        }
    }

    /**
     * Redirect the user if they post to the index dropdowns
     *
     * @return redirect
     */
    public function body_registration_submit(Request $request, $manufacturer_slug, $model_slug)
    {
        $redirect_to = $request->segment(1);

        // If user posts an id redirect
        if(!empty(preg_replace('/\D/', '', $request->body)) && !empty(preg_replace('/\D/', '', $request->registration))){

            $body = VehicleBody::get_by_id($request->body);

          return redirect(route($redirect_to . '.model_list.body_list.registration_list.vehicle_list', [$manufacturer_slug, $model_slug, $body->slug, $request->registration]));

        }elseif(!empty(preg_replace('/\D/', '', $request->body))){

            $body = VehicleBody::get_by_id($request->body);

            return redirect(route($redirect_to . '.model_list.body_list.registration_list', [$manufacturer_slug, $model_slug, $body->slug]));
        }else{

            return redirect(route($redirect_to . '.model_list.body_list', [$manufacturer_slug, $model_slug]));
        }
    }
}
