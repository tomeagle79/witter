<?php
return [
    'admin' => [
        'loginurl' => 'wtlogin',
        'slug' => 'wtadmin',
        'dashboard' => '/wtadmin/dashboard',
    ],
    'bounce_url' => 'http://www.witter-towbars.co.uk',
    'developer' => [
        'name' => 'Reckless',
        'url' => 'https://www.reckless.agency/',
        'dev_email' => env('RECKLESS_DEV_EMAIL', 'dev@reckless.agency'),
    ],
    'google_analytics_code' => env('GOOGLE_ANALYTICS_CODE', 'UA-19846752-1'),
    'api' => [
        'user' => '',
        'pass' => '',
    ],
    'pagination' => [
        'blog' => 10,
        'search' => 10,
        'towbars' => 4,
    ],
    'phone_booking_centre_ids' => [39152, 42403],
    # Allow the Reckless Office and Witter office view the pay by phone option.
    'enable_phone_payments_by_ip' => ['109.238.64.145', '84.19.49.2', '84.19.49.4', '82.15.47.224'],
    'stripe_publishable' => env('STRIPE_PUBLISHABLE', 'pk_live_f6bbtct9EO1jafKVwEA3vH53'),
    'stripe_secret' => env('STRIPE_SECRET', 'sk_live_lA6iJAKJdl3Bpd2fvUj3OW9Y'),
    'google_maps_api_key' => env('GOOGLE_MAPS_API_KEY', 'AIzaSyA-VmDhNlxlKQoxC7F7YXoQvmS2APd6Qw0'),
    'fitting_centre' => [
        'regions_by_slug' => [
            'north-east' => [
                'name' => 'North East',
                'meta_description' => 'Looking for towbars in the North East of England? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'north-west' => [
                'name' => 'North West',
                'meta_description' => 'Looking for towbars in the North West of England? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'yorkshire-and-the-humber' => [
                'name' => 'Yorkshire and the Humber',
                'meta_description' => 'Looking for towbars in Yorkshire and the Humber? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'east-midlands' => [
                'name' => 'East Midlands',
                'meta_description' => 'Looking for towbars in the East Midlands? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'west-midlands' => [
                'name' => 'West Midlands',
                'meta_description' => 'Looking for towbars in the West Midlands? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'east-of-england' => [
                'name' => 'East of England',
                'meta_description' => 'Looking for towbars in the East of England? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'greater-london' => [
                'name' => 'Greater London',
                'meta_description' => 'Looking for towbars in Greater London? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'south-east' => [
                'name' => 'South East',
                'meta_description' => 'Looking for towbars in the South East of England? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'south-west' => [
                'name' => 'South West',
                'meta_description' => 'Looking for towbars in the South West of England? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'wales' => [
                'name' => 'Wales',
                'meta_description' => 'Looking for towbars in Wales? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'scotland' => [
                'name' => 'Scotland',
                'meta_description' => 'Looking for towbars in Scotland? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'northern-ireland' => [
                'name' => 'Northern Ireland',
                'meta_description' => 'Looking for towbars in Northern Ireland? Find a towbar and book your fitting at a WebFit service centre.',
            ],
            'isle-of-man' => [
                'name' => 'Isle of Man',
                'meta_description' => 'Looking for towbars in Isle of Man? Find a towbar and book your fitting at a WebFit service centre.',
            ],
        ],
        'regions' => [
            'ne' => 'north-east',
            'nw' => 'north-west',
            'yh' => 'yorkshire-and-the-humber',
            'em' => 'east-midlands',
            'wm' => 'west-midlands',
            'ee' => 'east-of-england',
            'gl' => 'greater-london',
            'se' => 'south-east',
            'sw' => 'south-west',
            'wa' => 'wales',
            'sc' => 'scotland',
            'ni' => 'northern-ireland',
            'im' => 'isle-of-man',
        ],
        'region_names' => [
            'ne' => 'North East',
            'nw' => 'North West',
            'yh' => 'Yorkshire and the Humber',
            'em' => 'East Midlands',
            'wm' => 'West Midlands',
            'ee' => 'East of England',
            'gl' => 'Greater London',
            'se' => 'South East',
            'sw' => 'South West',
            'wa' => 'Wales',
            'sc' => 'Scotland',
            'ni' => 'Northern Ireland',
            'im' => 'Isle of Man',
        ],
    ],
    'site' => [
        'adminname' => 'Witter Towbars Admin Area',
        'name' => 'Witter Towbars',
        'opening_hours' => [
            [
                'day' => 'Monday',
                'times' => '9:00am-5:30pm',
            ],
            [
                'day' => 'Tuesday',
                'times' => '9:00am-5:30pm',
            ],
            [
                'day' => 'Wednesday',
                'times' => '9:00am-5:30pm',
            ],
            [
                'day' => 'Thursday',
                'times' => '9:00am-5:30pm',
            ],
            [
                'day' => 'Friday',
                'times' => '9:00am-4:00pm',
            ],
            [
                'day' => 'Saturday',
                'times' => 'CLOSED',
            ],
            [
                'day' => 'Sunday',
                'times' => 'CLOSED',
            ],
        ],
    ],
    'appointments_arr' => [
        '08:00',
        '09:00',
        '10:00',
        '11:00',
        '12:00',
        '13:00',
        '14:00',
    ],
    'assets_cache_arr' => [
        'assets/css/app.css' => 'assets/css/app.css',
        'assets/css/ie/app_1.css' => 'assets/css/ie/app_1.css',
        'assets/css/ie/app_2.css' => 'assets/css/ie/app_2.css',
        'assets/css/ie/app_3.css' => 'assets/css/ie/app_3.css',
        'assets/js/cart_appointments.js' => 'assets/js/cart_appointments.js',
        'assets/js/app.js' => 'assets/js/app.js',
    ],
];
