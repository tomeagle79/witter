<div class="form-group required">
	<label for="title" class="col-md-2 control-label">Title</label>
	<div class="col-md-4">
		{!! Form::text('title', ( ($type == 'edit') ? $career->title : null), ['class' => 'form-control title-for-slug', 'id' => 'title', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group">
	<label for="published" class="col-sm-3 col-md-2 control-label">Published</label>
	<div class="col-sm-9 col-md-6">
		{!! Form::checkbox('published', true, isset($career) ? $career->published : true, ['class' => '']) !!}
		<p class="help-block">Display on website?</p>
	</div>
</div>

<div class="form-group required">
	<label for="url_slug" class="col-md-2 control-label">URL Slug</label>
	<div class="col-md-4">
		{!! Form::text('url_slug', ( ($type == 'edit') ? $career->url_slug : null), ['class' => 'form-control slug-generated', 'id' => 'slug']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="email" class="col-md-2 control-label">Email</label>
	<div class="col-md-4">
		{!! Form::email('email', ( ($type == 'edit') ? $career->email : null), ['class' => 'form-control', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="contract" class="col-md-2 control-label">Contract</label>
	<div class="col-md-4">
		{!! Form::text('contract', ( ($type == 'edit') ? $career->contract : null), ['class' => 'form-control', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="salary" class="col-md-2 control-label">Salary</label>
	<div class="col-md-4">
		{!! Form::text('salary', ( ($type == 'edit') ? $career->salary : null), ['class' => 'form-control', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="description" class="control-label col-xs-12 col-sm-3 col-md-2 col-lg-2">Description</label>
	<div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
		{!! Form::textarea('description', ( ($type == 'edit') ? $career->description : null), ['class' => 'form-control wysiwyg']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-md-offset-2 col-md-4">
		<button type="submit" class="btn btn green">{{ $submit_text }}</button>
	</div>
</div>


@section('scripts')
	@parent
	<script type="text/javascript" src="{!! URL::asset('assets/js/redactor.js') !!}"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('textarea.wysiwyg').redactor({
				minHeight: 300,
				plugins: ['table', 'video', 'imagemanager'],
				buttonSource: true,
				imageUpload: '{!! URL::to('wtadmin/redactor/image_upload') !!}?_token=' + '{{ csrf_token() }}',
				imageManagerJson: '{!! URL::to('wtadmin/redactor/get_image_json') !!}',
			});
		});

	</script>
@overwrite

