<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Api\VehicleRegistrationLookup;
use App\Models\Api\VehicleManufacturer;
use App\Http\Requests\TowbarSearchFormRequest;
use Session;
use App\Models\Api\Towbar;

class RegistrationLookupController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Witter Towbars', route('electrics'));
    }

    /**
     * Search for a towbar
     *
     * @return \Illuminate\Http\Response
     */
    public function towbar_search(TowbarSearchFormRequest $request)
    {
        // Get the towbars from the API
        $this->data['reg'] = $reg = VehicleRegistrationLookup::get_where(['registrationNo' => $request->input('search')]);

        if(empty($reg)) {

            $this->data['manufacturers'] = $this->data['manufacturers_dropdown'] = $data = VehicleManufacturer::get_all();

            return view('towbars.registrationlookup.not_found', $this->data);
        } else {

            if(!$reg->isRecognised) {

                $this->data['manufacturers'] = $this->data['manufacturers_dropdown'] = $data = VehicleManufacturer::get_all();

                return view('towbars.registrationlookup.not_found', $this->data);
            } else {

                // Add the reg number to the session so that we can send it later
                Session::put('registration_no', $request->input('search'));

                if(empty($reg->vehicleChoices)){

                    // Found but no choices so redirect to towbars 
                    return redirect( route('towbars.vehicle_towbars_by_reg', [$reg->vehicleId, $reg->registrationDate]) );

                } else {

                    $choice_arr = $reg->vehicleChoices;

                    // Loop through the choices
                    foreach($choice_arr as $key => $value) {

                        $towbars = Towbar::get_towbars_by_id_and_date($value->vehicleId, $reg->registrationDate);

                        if(!empty($towbars->towbarTypes)){

                            $towbar_collection = collect($towbars->towbarTypes); 

                            // Filter out the out of stock items.
                            $filtered = $towbar_collection->where('inStock', true);

                            // Add the count for each of the options. 
                            $choice_arr[$key]->count = $filtered->count();
                        }
                    }

                    $choice = collect($choice_arr);

                    // Sort by count and use top item
                    $sorted = $choice->sortByDesc('count')->values()->all();

                    return redirect( route('towbars.vehicle_towbars_by_reg', [$sorted[0]->vehicleId, $reg->registrationDate]) );
                }
            }
        }
    }

    /**
     * Search for a towbar
     *
     * @return \Illuminate\Http\Response
     */
    public function electrics_search(TowbarSearchFormRequest $request)
    {
        // Get the towbars from the API
        $this->data['reg'] = $reg = VehicleRegistrationLookup::get_where(['registrationNo' => $request->input('search')]);

        $this->data['manufacturers'] = $this->data['manufacturers_dropdown'] = $data = VehicleManufacturer::get_all();

        if(empty($reg)) {
            return view('electrics.registrationlookup.not_found', $this->data);
        } else {

            if(!$reg->isRecognised) {
                return view('electrics.registrationlookup.not_found', $this->data);
            } else {

                // Add the reg number to the session so that we can send it later
                Session::put('registration_no', $request->input('search'));

                if(empty($reg->vehicleChoices)){
                    // Found but no choices available, so show confirmation
                    return view('electrics.registrationlookup.vehicle_confirm', $this->data);
                } else {
                    return view('electrics.registrationlookup.vehicle_select', $this->data);
                }
            }
        }
    }

}
