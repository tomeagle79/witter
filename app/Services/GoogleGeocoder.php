<?php

namespace App\Services;

class GoogleGeocoder
{
    /**
     * Construct a centre address string ready to be passed to Google
     *
     * @return array - Geocode result
     */
    function geocode_centre($centre)
    {
        $look_up = empty($centre->address1) ? '' : $centre->address1 . ', ';
        $look_up .= empty($centre->address2) ? '' : $centre->address2 . ', ';
        $look_up .= empty($centre->address3) ? '' : $centre->address3 . ', ';
        $look_up .= empty($centre->city) ? '' : $centre->city . ', ';
        $look_up .= empty($centre->postcode) ? '' : $centre->postcode . ', ';
        $look_up .= " UK";

        $result = $this->lookup($look_up);

        // no result try just the postcode
        if(empty($result)) {
            return $this->lookup($centre->postcode . ' UK');
        } else {
            return $result;
        }
    }

    /**
     * Pass the address over to geocode API
     *
     * @return array - Geocode result
     */
    function lookup($address)
    {
        $client = new \GuzzleHttp\Client();

        $request = $client->get('https://maps.googleapis.com/maps/api/geocode/json', [
            'query' => [
                'address' => $address,
                'key' => config('settings.google_maps_api_key')
            ],
            'verify' => false,
            'region' => 'UK'
        ]);

        $result = json_decode($request->getBody()->getContents());

        // got a match
        if($result->status=='OK') {
            return ['lat' => $result->results[0]->geometry->location->lat, 'lng' => $result->results[0]->geometry->location->lng];
        } else {
            return false;
        }
    }
}
