<?php

function towbar_in_stock($towbar)
{
    /**
     * Always in stock?
     */
    if($towbar->alwaysInStock) {
        return true;
    }

    /**
     * Has any stock left?
     */
    if($towbar->inStock && $towbar->stockLevel > 0) {
        return true;
    }

    return false;
}