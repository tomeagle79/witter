<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddButtonTextToBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->string('button_text')->nullable()->after('link');
            $table->tinyInteger('vehicle_of_week')->default(0)->after('button_text');
            $table->string('manufacturer_logo')->nullable()->after('vehicle_of_week');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn('button_text');
            $table->dropColumn('vehicle_of_week');
            $table->dropColumn('manufacturer_logo');
        });
    }
}
