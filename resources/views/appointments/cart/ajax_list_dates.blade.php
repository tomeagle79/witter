<p>When is best for you?</p>

<div class="appointments cart-appointments">
    <form class="form-horizontal" action="{{ route('cart.adjust_appointment') }}" method="post" id="book-form">

        @csrf

        <input type="hidden" name="partnerId" value="{{ $appointments->partnerId }}" >
        <input type="hidden" name="address" value="{{ $appointments->address }}" >
        <input type="hidden" name="partNo" value="{{ $appointments->partNo }}" >
        <input type="hidden" name="variantTowbarId" value="{{ $appointments->variantTowbarId }}" >
        <input type="hidden" name="vehicleDesc" value="{{ $appointments->vehicleDesc }}" >
        <input type="hidden" name="fitterType" value="{{ $appointments->fitterType }}" >
        <input type="hidden" name="electric" value="{{ $appointments->electric }}" >
        <input type="hidden" name="appointmentTime" value="" >

        @if($appointments->fitterType == 'mobile')
            <input type="hidden" name="fitterFee" value="{{ $appointments->mobileFitCost }}" >
        @else
            <input type="hidden" name="fitterFee" value="0" >
        @endif

        <div class="strip">

            <div class="row">
                <div class="col-md-6">
                    <div id="datepicker"></div>

                </div> <!-- .col-md-6 -->
                <div class="col-md-6">
                    <div class="bordered">

                        <input type="hidden" name="appointment_date" id="appointment_date" value="">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label">Fitter's address</label>
                            <div class="col-sm-12">
                                <p class="form-control-static">

                                    {{ $appointments->address }}

                                    @if($appointments->fitterType == 'mobile')
                                        <span class="control-label">Mobile Fitter (They come to you, with an additional charge of &pound;25)</span>
                                    @endif
                                </p>
                            </div>
                        </div> <!-- .form-group -->

                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label">Selected date</label>
                            <div class="col-sm-12">
                                <p class="form-control-static" id="date-text">

                                </p>
                            </div>
                        </div> <!-- .form-group -->

                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label">Select a time</label>
                            <div class="col-sm-12">
                                <select name="appointment_id" class="form-control" id="appointment_id">
                                    <option>Select a time</option>
                                </select>
                            </div>
                        </div> <!-- .form-group -->

                    </div> <!-- .bordered -->
                </div> <!-- .col-md-6 -->
            </div> <!-- .row -->
        </div><!-- /.strip -->

        <div class="row" id="appointment_error" style="display: none;">
            <div class="col-xs-12">
                <div class="alert alert-help"><p>Please select a date and time before continuing.</p></div>
            </div><!-- /.col-md-12 -->
        </div> <!-- .row -->

        <div class="row cart-appointments--buttons">
            <div class="col-md-6">
                <div class="form-group">
                    <a class="btn btn-grey btn-large " id="list_fitters">Edit fitter location</a>
                </div> <!-- .form-group -->
            </div><!-- /.col-md-6 -->

            <div class="col-md-6">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-primary-large">Book Fitting</button>
                </div> <!-- .form-group -->
            </div><!-- /.col-md-6 -->
        </div> <!-- .row -->

        <div class="row">
            <div class="col-xs-12">
                    <p class="tel">Got any questions? Just give us a call <a href="tel:+441244284555">01244 284 555</a></p>
                </div> <!-- .col-md-6 -->
        </div><!-- /.row -->
    </form>
</div> <!-- .appointments -->

<script>
    $(function () {
        var appointments = {!! json_encode($appointments->data) !!};

        var enabled_dates = [
            @foreach ( $appointments->enabled_dates as $key => $value )
                moment('{{ $value }}'),
            @endforeach
        ]

        var partner = {!! json_encode($appointments->partner) !!};
        var partnerId = '{{ $appointments->partnerId }}';
        var address = '{{ $appointments->address }}';
        var partNo = '{{ $appointments->partNo }}';
        var variantTowbarId = '{{ $appointments->variantTowbarId }}';
        var vehicleDesc = '{{ $appointments->vehicleDesc }}';
        var fitterType = '{{ $appointments->fitterType }}';
        var electric = '{{ $appointments->electric }}';

        $('#datepicker').datetimepicker({
            inline: true,
            sideBySide: true,
            format: 'DD/MM/YYYY',
            enabledDates: enabled_dates
        });

        $("#datepicker").on("dp.change", function (e) {
            $('#date-text').html(e.date.format('Do MMMM YYYY'));
            $('#appointment_date').val(e.date.format('YYYYMMDD'));
            $('#appointment_error').fadeOut('slow');

            var dt = e.date.format('YYYYMMDD');

            $('#appointment_id').html();
            var options = '';
            $.each(appointments[dt], function(i,v) {
                options = options + '<option value="' + appointments[dt][i].appointmentId + '">' + appointments[dt][i].appointmentTime + '</option>' + "\n";
            });
            $('#appointment_id').html(options);
        });

        // Appointment chosen
        $(document).on('submit', '#book-form', function(e){

            // Check that an appointment has been selected.
            var appointmentId = $('#appointment_id').val();
            var appointmentTime = $('#appointment_id option:selected').html();
            var appointmentDateFriendly = $('#date-text').html();
            var appointmentDate = $('#appointment_date').val();

            if(appointmentId.length == 0 || appointmentTime.length == 0 || appointmentDateFriendly.length == 0 || appointmentDate.length == 0) {
                e.preventDefault();

                $('#appointment_error').fadeIn('slow');

                // alert('Please select a date and time before continuing.');

                return false;
            }

            var appointmentTime = $('#appointment_id option:selected').html();

            $("#book-form [name='appointmentTime']").val(appointmentTime);

            return true;
        });
    });
</script>
