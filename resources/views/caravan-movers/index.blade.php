@extends('layouts.frontend')

@section('title', 'Caravan Movers')

@section('heading')
Caravan Movers
@stop

@section('body-class')
caravan-movers-page
@stop

<?php //dd($accessories); ?>

@section('meta_description')

@stop

@section('content')
<div class="container caravan-movers" data-scroll="">
  <div class="row content">
    <div class="col-xs-12">
      <div class="description">
        <p>Caravan motor movers have revolutionised caravanning making it even more accessible to the public. Whether you don't have space to easily manoeuvre your caravan, or it's simply just too heavy, a Caravan mover can help you get on the road in no time.</p>
      </div>
    </div>
  </div>

  <div class="row item-list is-flex" id="prod_results">
    @if($accessories)
        @foreach($accessories as $accessory)
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="item">
                    <a href="{!! route('caravan_movers.view', [encode_url($accessory->slug)]) !!}" @if($accessory->isFittable) class="ribbon-conatiner" @endif>
                        @if($accessory->isFittable)
                            @include('partials.ribbon.webfit')
                        @endif
                        <img src="{{ image_load($accessory->imageUrl) }}" alt="" /></a>
                    <a href="{!! route('caravan_movers.view', [encode_url($accessory->slug)]) !!}" ><h3 class="list-title">{!! $accessory->title !!}</h3></a>

                    <hr>

                    <div class="row">
                        <div class="col-xs-12">
                            <p class="price">&pound;{{ price($accessory->displayPrice) }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <p class="price-rrp">@if($accessory->retailPrice > $accessory->price)RRP &pound;{{ price($accessory->retailPrice) }}@endif</p>
                        </div>
                    </div>

                    @if($accessory->isSellable && $accessory->inStock)
                        <div class="row stock">
                            <div class="col-xs-12">
                                <i class="icon-check"></i>
                                <p>In Stock</p>
                            </div>
                        </div>
                    @else
                        <div class="row stock">
                            <div class="col-xs-12">
                                <i class="icon-cancel-1"></i>
                                <p>Out of stock</p>
                            </div>
                        </div>
                    @endif

                    @if($accessory->isSellable && $accessory->inStock)
                        <div class="row">
                            <div class="col-xs-7">
                                <a href="{!! route('caravan_movers.view', [encode_url($accessory->slug)]) !!}" class="btn btn-primary">View Now</a>
                            </div><!-- /.col-xs-7 -->
                            <div class="col-xs-5">
                                @if(!$accessory->fitOnly)
                                <a href="{{ route('add_to_cart', [
                                    'type' => 'accessory',
                                    'partNo' => $accessory->partNo
                                ]) }}" class="btn btn-primary btn-green-plus"> <img src="/assets/img/basket.svg" height="20" width="20" alt="Add to basket"></a>
                                @endif
                            </div>
                        </div><!-- /.row -->
                    @else
                        <div class="row">
                            <div class="col-xs-12">
                                <a diabled class="btn btn-primary btn-oos-model">Notify me when stock arrives</a>
                            </div>
                        </div><!-- /.row -->
                    @endif

                    @if($accessory->inStock == false)
                        <div class="out-of-stock-modal">
                            <div class="close-modal"><i class="icon-cancel-1"></i></div>
                            <img src="{{URL('assets/img/icons', ['filename' => 'basket.svg'])}}" alt="">
                            <p>We’re sorry we have no stock</p>
                            <h5>We’d like to let you know when it’s back…</h5>
                            <p>Fill in your email address here and as soon as it comes back into stock we’ll send you an update:</p>

                            {!! Form::open(['route' => 'product.email_notify']) !!}
                                {{ Form::email('email', null, ['id' => 'email', 'placeholder' => 'Email Address']) }}
                                {{ Form::hidden('product_id', $accessory->partNo, ['id' => 'product_id']) }}
                                {{ Form::hidden('product_name', $accessory->title, ['id' => 'product_name']) }}
                                {{ Form::hidden('product_url', route('caravan_movers.view', [encode_url($accessory->slug)]), ['id' => 'product_url']) }}
                                {{ Form::hidden('checkbox_for_subscription', false, ['id' => 'checkbox_for_subscription']) }}

                                <button type="submit" class="btn btn-primary btn-green-basic">Notify Me</button>
                                <input type="checkbox" name="placeholder_for_checkbox" id="placeholder_for_checkbox">
                                <label for="placeholder_for_checkbox">Would you also like to subscribe to news and offers from Witter?</label>
                            {!! Form::close() !!}
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    @else
        <p class="none-found">Oops, we can't find a bike rack that meets your needs. Have a go at refining your requirements and we'll try again.</p>
    @endif
  </div> <!-- .row -->
  <!-- .row -->
  <div class="row content">
    <div class="col-xs-12">
      <div class="description">

        <h4 class="grey-title">What is a Caravan Mover?</h4>
        <p>The caravan mover bolts to the caravan chassis and drives its wheels by 12-volt power. They come with a small remote control that easily directs the caravan into position without breaking a sweat.</p>

        <h4 class="grey-title">Why buy a Caravan Mover?</h4>

        <ul>
          <li>Ease: A caravan mover makes your life so much easier. Simply use the remote to effortlessly manoeuvre the caravan directly up to your hitch.</li>

          <li>Restricted Space: If you only have limited space at home or on your pitch, the caravan mover makes it easy to manoeuvre the caravan into tight spaces.</li>

          <li>Accessibility: The caravan mover takes all the stress and physical labour out of hitching up your caravan. Stand back and watch as the mover takes on all the hard work and your caravan glides into place.</li>
        </ul>

        <h4 class="grey-title">What do I need to consider? </h4>
        <ul>
          <li>Twin or Single Axle: Consider if the caravan mover is designed to move a single or twin axle caravan. Our caravan movers are designed to be interchangeable between single and twin axle caravans to make things easier.</li>

          <li>Weight: Weight comes into consideration in two aspects, the first is the weight of the caravan itself. Caravan mover manufacturers will stipulate the amount of weight a given mover can cope with moving, so make sure your chosen mover can take the weight of your caravan.</li>

          <li>The second instance where weight must be considered is in the weight of the mover itself. The weight of the mover needs to be added to your payload so that your vehicle is not towing over its maximum towing capacity. Our range of caravan movers is the lightest on the market by 10kg allowing you to carry more of the things you really need for your holiday.</li>
        </ul>

        <h4 class="grey-title">Can I fit a caravan mover myself?</h4>
        <p>We recommend having your Caravan mover fitted by a professional, that's why we offer a fully fitted price fitted by our approved UK Nationwide Fitting centre. Simply choose the caravan mover best suited for you, enter your postcode and choose your preferred fitting date. We will try our best to accommodate your chosen date and your mover can usually be fitted within 7 days of placing an order. Click here to learn more and find the perfect mover for you.</p>

        <h4 class="grey-title">Caravan Movers Range</h4>
        <p>The e-go caravan mover range is powered by e-go's exclusive Quattro® technology. Its intelligent modular system provides soft-start for precision moving in tight spaces. The e-go range can be used on single, twin and AWD installations and even allows retro-fit upgrades so a twin system can be adapted to an All-Wheel Drive at any point. This exclusive intelligent technology gives you ultimate flexibility and peace of mind.
          <p>
      </div>
    </div>
  </div>
</div>
<!-- .container -->
@stop @section('scripts')
<script>
  $(document).ready(function () {
    var to_scroll = $('.caravan-movers').data('scroll');

    $('.btn-oos-model').on('click', function (e) {
      e.preventDefault();
      $(this).parent().parent().next('.out-of-stock-modal').addClass('active');
    });

    $('.close-modal').on('click', function (e) {
      e.preventDefault();
      $(this).parent().removeClass('active');
    });

    $('#placeholder_for_checkbox').on('click', function (e) {
      e.preventDefault();
      if ($(this).hasClass('true')) {
        $(this).removeClass('true');
        $('#checkbox_for_subscription').val(false);
      } else {
        $(this).addClass('true');
        $('#checkbox_for_subscription').val(true);
      }
    });

    var window_width = $(window).width();

    if (typeDefault != 'any') {
      $('#type_of_connection').val(typeDefault);
    }
  });
</script>
@stop