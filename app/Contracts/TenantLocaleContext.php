<?php

namespace App\Contracts;

interface TenantLocaleContext
{
    /*
     * Return current website object
     */
    public function getWebsite();

    /*
     * Return current website locale object
     */
    public function getLocale();
}
