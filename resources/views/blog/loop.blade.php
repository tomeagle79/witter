<div class="container ">
	<nav class="row blog-catmenu hidden-sm hidden-xs">
		@foreach($categories as $cat)
			<div class="col-md-2">
				<a href="{!! route('blog_dynamic_route', $cat->url_slug) !!}" class="{{ (!empty($category) && $cat->id == $category->id)? 'active' : '' }}">
					{!! $cat->title !!}
				</a>
			</div>
		@endforeach

		<div class="col-md-2"><a href="{!! route('blog') !!}" class="{{ empty($category)? 'active' : '' }}">View All</a></div>
	</nav>

		@foreach($blog_posts as $key => $blog)

			@if($key === 0 || $key === 2 || $key === 4)
				<div class="row">
			@endif

				@if($key < 2)
					<div class="col-sm-6 col-md-6 blog-article">

						@if(!is_null($blog->image))
							<a href="{!! route('blog_dynamic_route', $blog->url_slug) !!}" class="blog-title"><img src="{{URL('imagecache', ['template' => 'large-thumbnail', 'filename' => $blog->image->filename])}}" alt="" /></a>
						@endif

						<a href="{!! route('blog_dynamic_route', $blog->url_slug) !!}" class="blog-title"><h3 class="blog-list-header">{!! $blog->title !!}</h3></a>

						<div class="blog-categories">
							@foreach($blog->categories as $blog_categories)
								{{ $blog_categories->title }}<br />
							@endforeach
						</div>

						<div class="blog-copy">
							{!! substr(strip_tags($blog->content), 0, 300) !!}...
						</div> <!-- .blog-copy -->
					</div> <!-- .blog-article -->
				@elseif($key < 5)

					<div class="col-sm-4 col-md-4 blog-article">

						@if(!is_null($blog->image))
							<a href="{!! route('blog_dynamic_route', $blog->url_slug) !!}" class="blog-title"><img src="{{URL('imagecache', ['template' => 'medium-thumbnail', 'filename' => $blog->image->filename])}}" alt="" /></a>
						@endif

						<a href="{!! route('blog_dynamic_route', $blog->url_slug) !!}" class="blog-title"><h3 class="blog-list-header">{!! $blog->title !!}</h3></a>

						<div class="blog-categories">
							@foreach($blog->categories as $blog_categories)
								{{ $blog_categories->title }}<br />
							@endforeach
						</div>

						<div class="blog-copy">
							{!! substr(strip_tags($blog->content), 0, 300) !!}...
						</div> <!-- .blog-copy -->
					</div> <!-- .blog-article -->

				@elseif($key < 9)

					<div class="col-sm-3 col-md-3 blog-article">

						@if(!is_null($blog->image))
							<a href="{!! route('blog_dynamic_route', $blog->url_slug) !!}" class="blog-title"><img src="{{URL('imagecache', ['template' => 'small-thumbnail', 'filename' => $blog->image->filename])}}" alt="" /></a>
						@endif

						<a href="{!! route('blog_dynamic_route', $blog->url_slug) !!}" class="blog-title"><h3 class="blog-list-header">{!! $blog->title !!}</h3></a>

						<div class="blog-categories">
							@foreach($blog->categories as $blog_categories)
								{{ $blog_categories->title }}<br />
							@endforeach
						</div>

						<div class="blog-copy">
							{!! substr(strip_tags($blog->content), 0, 300) !!}...
						</div> <!-- .blog-copy -->
					</div> <!-- .blog-article -->
				@endif

			@if($key === 1 || $key === 4 || $blog->id === $blog_posts->last()->id)
				</div> <!-- .row -->
			@endif

		@endforeach

	<nav class="row blog-catmenu hidden-lg hidden-md">
		@foreach($categories as $cat)
			<div class="col-md-2">
				<a href="{!! route('blog_dynamic_route', $cat->url_slug) !!}" class="{{ (!empty($category) && $cat->id == $category->id)? 'active' : '' }}">
					{!! $cat->title !!}
				</a>
			</div>
		@endforeach

		<div class="col-md-2"><a href="{!! route('blog') !!}" class="{{ empty($category)? 'active' : '' }}">View All</a></div>
	</nav>

	<div class="row text-center">
		{!! $blog_posts->render() !!}
	</div>

</div> <!-- .container -->


