@extends('layouts.frontend')

@section('title', 'Find Your Vehicle')

@section('heading')
	Find Your Vehicle
@stop

@section('meta_description')
	Witter Towbars Find Your Vehicle
@stop

@section('content')


<div class="container vehicle-select">
	<div class="row">
		<div class="col-md-12">

            <div class="alert alert-success" role="alert">

                <p>
                    We have found a vehicle matching your registration number.
                </p>
                <p>
                    Please confirm this is your vehicle.
                </p>

            </div>

            <div class="row">

                <div class="col-md-3 col-md-offset-3">

                    <p>
                        @foreach($reg->vehicleDetails as $detail)
                            {{ $detail->prefix }}
                            @if($detail->prefix == 'Registration:')
                                <strong>{{ strtoupper($detail->text) }}</strong><br>
                            @else
                                <strong>{{ $detail->text }}</strong><br>
                            @endif
                        @endforeach
                    </p>

                    <p>
                        <a href="{{ route('electrics.vehicle_towbars_by_reg', [$reg->vehicleId, $reg->registrationDate]) }}" class="btn btn-primary">This is my car</a>
                    </p>

                </div>

                @if( !empty($reg->photoUrl) )
                <div class="col-md-3">
                    <img src="{{ $reg->photoUrl }}" alt="" class="top-img">
                    <p class="text-center">Images are representative only</p>
                </div>
                @endif

            </div>

		</div>
	</div>
</div>
@stop
