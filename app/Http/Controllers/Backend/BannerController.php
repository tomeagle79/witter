<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BannerStoreRequest;
use App\Models\Banner;
use App\Models\Image as ImageModel;
use Datatables;
use DB;
use Illuminate\Http\Request;
use URL;
use View;

class BannerController extends Controller
{
    /**
     * Instantiate a new AdminUserController instance.
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.banners.index');
    }

    /**
     * Return the datatable for banners
     *
     * @return Datatables
     */
    public function indexDatatable()
    {

        $banners = Banner::select(['id', 'title', 'active_start_at', 'active_end_at', 'active', 'sort_order', 'created_at', 'updated_at'])->get();

        return Datatables::of($banners)
            ->addColumn('edit', '<a href="{{ URL::backend(\'banners/edit/\'. $id) }}" class="btn btn-success">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'banners/delete/\'. $id) }}" class="btn btn-danger" data-confirm="Are you sure you want to delete this banner?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colors = [
            '#014789' => 'Blue',
            '#da3934' => 'Red',
            '#ffffff' => 'White',
            '#000000' => 'Black',
        ];
        return view('backend.banners.create', ['colors' => $colors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(BannerStoreRequest $request)
    {
        // create the banner
        $banner = new Banner();
        $banner->title = $request->title;
        $banner->sub_title = $request->sub_title;
        $banner->link = $request->link;
        $banner->main_color = $request->main_color;
        $banner->sub_color = $request->sub_color;
        $banner->discount_code_check = $request->discount_code_check;
        $banner->discount_code = $request->discount_code;
        $banner->drop_shadow = (empty($request->drop_shadow) ? 0 : 1);
        $banner->sort_order = $request->sort_order;
        $banner->active = (empty($request->active) ? 0 : 1);
        $banner->button_text = (empty($request->button_text) ? null : $request->button_text);
        $banner->vehicle_of_week = (empty($request->vehicle_of_week) ? 0 : 1);
        $banner->manufacturer_of_week = (empty($request->manufacturer_of_week) ? 0 : 1);
        $banner->active_start_at = (empty($request->active_start_at) ? null : $request->active_start_at);
        $banner->active_end_at = (empty($request->active_end_at) ? null : $request->active_end_at);

        // store the image
        $file = $request->file('file');

        if (!is_null($file)) {
            $filename = strtolower(str_random(40)) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/img/banners/', $filename);

            // save the image
            $image = new ImageModel();
            $image->path = '/img/banners/';
            $image->filename = $filename;
            $image->save();

            $banner->image_id = $image->id;
        }

        // wrap in transaction, for graceful fail
        DB::transaction(function () use ($banner, $request) {

            // save the banner
            $banner->save();
        });

        return redirect(URL::backend('banners'))->with('success', '<strong>Success!</strong> Banner created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        $colors = [
            '#014789' => 'Blue',
            '#da3934' => 'Red',
            '#ffffff' => 'White',
            '#000000' => 'Black',
        ];
        return View::make('backend.banners.edit', ['banner' => $banner, 'colors' => $colors]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannerStoreRequest $request, $id)
    {

        $banner = Banner::findOrFail($id);
        $banner->title = $request->title;
        $banner->sub_title = $request->sub_title;
        $banner->link = $request->link;
        $banner->main_color = $request->main_color;
        $banner->sub_color = $request->sub_color;
        $banner->discount_code_check = $request->discount_code_check;
        $banner->discount_code = $request->discount_code;
        $banner->drop_shadow = (empty($request->drop_shadow) ? 0 : 1);
        $banner->sort_order = $request->sort_order;
        $banner->active = (empty($request->active) ? 0 : 1);
        $banner->button_text = (empty($request->button_text) ? null : $request->button_text);
        $banner->vehicle_of_week = (empty($request->vehicle_of_week) ? 0 : 1);
        $banner->manufacturer_of_week = (empty($request->manufacturer_of_week) ? 0 : 1);
        $banner->active_start_at = (empty($request->active_start_at) ? null : $request->active_start_at);
        $banner->active_end_at = (empty($request->active_end_at) ? null : $request->active_end_at);

        $file = $request->file('file');

        // if there is an image
        if (!is_null($file)) {

            // delete the old image from rackspace and the database
            if (!is_null($banner->image)) {
                $banner->image->forceDelete();
            }

            // store the new image
            $filename = strtolower(str_random(40)) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/img/banners/', $filename);

            // save the image
            $image = new ImageModel();
            $image->path = '/img/banners/';
            $image->filename = $filename;
            $image->save();

            // update the banner
            $banner->image_id = $image->id;
        }
        // save the banner
        $banner->save();

        return redirect(URL::backend('banners'))->with('success', '<strong>Success!</strong> Banner updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        $banner->delete();

        return redirect(URL::backend('banners'))->with('message', '<strong>Success!</strong> Banner deleted.');
    }

    /**
     * Restore the specified resource
     */
    public function restore($id)
    {
        $banner = Banner::withTrashed()->findOrFail($id);
        $banner->restore();

        return redirect(URL::backend('banners'))->with('success', '<strong>Success!</strong> Banner restored.');
    }
}
