<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TowbarStockNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vtid' => ['required', 'max:255'],
            'towbar' => ['required', 'max:255'],
            'first_name' => ['required', 'max:255'],
            'surname' => ['required', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'mobile' => ['required', 'max:255'],
            'postcode' => ['required', 'max:10'],
        ];
    }
}
