<div class="form-group required">
	<label for="first_name" class="col-md-2 control-label">Name</label>
	<div class="col-md-4">
		{!! Form::text('name', ( ($type == 'edit') ? $user->name : null), ['class' => 'form-control', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group required">
	<label for="email" class="col-md-2 control-label">Email</label>
	<div class="col-md-4">
		{!! Form::email('email', ( ($type == 'edit') ? $user->email : null), ['class' => 'form-control', 'required' => 'required']) !!}
	</div>
</div>

<div class="form-group{!! $type == 'edit' ? '' : ' required' !!}">
	<label for="password" class="col-md-2 control-label">Password</label>
	<div class="col-md-4">
		{!! Form::password('password', ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-md-offset-2 col-md-4">
		<button type="submit" class="btn green">{{ $submit_text }}</button>
	</div>
</div>
