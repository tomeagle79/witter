@extends('layouts.frontend')

@section('title', 'FAQs')

@section('heading')
	FAQs
@stop

@section('meta_description')
	Witter Frequently asked questions
@stop

@section('content')
	@include('faqs.loop')
@stop
