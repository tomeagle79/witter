<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class TenantLocaleServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // register Tenant Locale Service
        $this->app->call([$this, 'registerTenantLocaleService']);
    }

    /**
     * Registers the required Tenant Locale service, based on specific criteria
     */
    public function registerTenantLocaleService(Request $request)
    {
        /*
        // running in console
        if ($this->app->runningInConsole()) {
        $service = 'Console';
        } else {
        if (is_null($request->route())) {
        $service = 'Frontend';
        } else {
        // get middleware
        $middleware = $request->route()->middleware();

        // backend request
        if (in_array('backend', $middleware)) {
        $service = 'Backend';
        }
        // api request
        elseif (in_array('api', $middleware)) {
        $service = 'API';
        }
        // frontend request
        else {
        $service = 'Frontend';
        }
        }
        }
         */
        $service = 'Frontend';

        // bind the implementation to the interface
        $this->app->singleton('App\Contracts\TenantLocaleContext', 'App\Services\TenantLocale\\' . $service . 'Service');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['App\Contracts\TenantLocaleContext'];
    }
}
