<?php

namespace App\Models\Api;

use App\Models\Api\ApiModel;

class Settings extends ApiModel
{
    const URI = "settings";

    /**
     * Get a fitter list
     *
     * @params Array
     * @return Object
     */
    public static function get_recode_premium()
    {
        $url = config('witter.api_base') . self::URI . '/recodepremium';
        $obj = self::get_request($url);
        
        return price($obj->value);
    }
}
