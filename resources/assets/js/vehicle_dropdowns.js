/*
 * Dynamic Dropdowns call a url to get the next dropdowns list of options
 *
 */
$.fn.dynamicDropdowns = function( $url ) {

    $selectbox = $(this);

    $selectbox.find("option:gt(0)").remove();

    $('.car-dropdowns-loading').show();

    $.getJSON( $url, function( data ) {
        $.each( data, function( key, val ) {

            $selectbox.append("<option value='" + val.index + "' data-slug='" + val.slug + "'>" + val.name + "</option>");
        });
    }).always(function() {
        $(".car-dropdowns-loading").hide();
    });
};

/*
 * Clear the dropdowns.
 * So if the user selects back there will be no issues with empty or untidy dropdowns.
 *
 */
function clearDropdownSelection(select){

    var selectedValue = select.find('option[selected]').val();

    if ( selectedValue ) {
        select.val( selectedValue );
    } else {
        select.prop( 'selectedIndex', 0 );

    }
}

/*
 * Set up the dynamic vehicle dropdowns on the page
 *
 */
function setUpVehicleDropdowns(vehicle_url_call){

    // When manufacturer dropdown changed update the other dropdowns
    if ( $( '#manufacturer' ).length ) {

        // If the manufacturer dropdown has no data then call it via ajax
        if( $("#manufacturer option").length <= 1){

            $("#manufacturer").dynamicDropdowns( vehicle_url_call + "/ajax_manufacturers" );
        }

        $( '#manufacturer' ).change(function() {

            // Check if its not the first default empty option.
            if($( this ).prop('selectedIndex') == 0){

                // Clear the model dropdowns if no data to call.
                $( '#model' ).find('option:gt(0)').remove();

                $('#manufacturerSlug').val("");
            } else {
                // Call the data.
                $( '#model' ).dynamicDropdowns( vehicle_url_call + '/ajax_models/' + $(this).val());

                $( '#manufacturerSlug' ).val($('option:selected', this).attr('data-slug'));
            }

            // Clear the body dropdowns
            $( '#body' ).find('option:gt(0)').remove();

            // If the registration dropdown exists then clear dropdown
            if ( $( "#registration" ).length ) {
                $( '#registration' ).find('option:gt(0)').remove();
            }

            // Clear the slug values
            $('#modelSlug').val("");
            $('#bodySlug').val("");

            // If the registrationId hidden filed exists the clear
            if ( $( "#registrationId" ).length ) {
                $('#registrationId').val("");
            }
        });

        clearDropdownSelection( $( '#manufacturer' ) );
    }

    // When model dropdown changed update the next bodies dropdown
    if ( $( '#model' ).length ) {
        $('#model').change(function() {

            // Check if its not the first default empty option.
            if($( this ).prop('selectedIndex') == 0){

                // Clear the model dropdowns if no data to call.
                $( '#body' ).find('option:gt(0)').remove();

                $('#modelSlug').val("");
            } else {
                // Call the data.
                $('#body').dynamicDropdowns( vehicle_url_call + '/ajax_bodies/' + $(this).val());

                $('#modelSlug').val($('option:selected', this).attr('data-slug'));
            }

            // If the registration dropdown exists then clear dropdown
            if ( $( "#registration" ).length ) {
                $( '#registration' ).find('option:gt(0)').remove();
            }

            // Clear the slug values
            $('#bodySlug').val("");

            // If the registrationId hidden filed exists the clear
            if ( $( "#registrationId" ).length ) {
                $('#registrationId').val("");
            }
        });

        clearDropdownSelection( $( '#model' ) );
    }

    // When body dropdown changed update the next registrations dropdown
    $('#body').change(function() {

        // Check if its not the first default empty option.
        if($( this ).prop('selectedIndex') == 0){

            // Clear the model dropdowns if no data to call.
            if ( $( "#registration" ).length ) {
                $( '#registration' ).find('option:gt(0)').remove();
            }

            $('#bodySlug').val("");

        } else {
            // Call the data.
            if ( $( "#registration" ).length ) {
                $('#registration').dynamicDropdowns( vehicle_url_call + '/ajax_registrations/' + $(this).val());
            }

            $('#bodySlug').val($('option:selected', this).attr('data-slug'));
        }

        // If the registrationId hidden filed exists the clear
        if ( $( "#registrationId" ).length ) {
            $('#registrationId').val("");
        }
    });
    if ( $( '#registration' ).length ) {
        // When registration dropdown selected record the slug value.
        $('#registration').change(function() {

            // Check if its not the first default empty option.
            if($( this ).prop('selectedIndex') != 0){

                $('#registrationId').val($('option:selected', this).attr('data-slug'));
            } else {
                $('#registrationId').val("");
            }
        });
    }
    clearDropdownSelection( $( '#body' ) );

    // If the registration exists the clear
    if ( $( "#registration" ).length ) {
        clearDropdownSelection( $( '#registration' ) );
    }


}

/*
 * Set up the dynamic vehicle dropdowns form so when submitted call the correct functions
 *
 */
function setUpVehicleRedirect(slug, validate){

    if(typeof validate === 'undefined'){
        validate = false;
    }

    $('#search-dropdown-form').on('submit', function(e){
        e.preventDefault();

        // Get the dropdown values.
        var manufacturer = $('#manufacturerSlug').val();
        var model = $('#modelSlug').val();
        var body = $('#bodySlug').val();
        var registration = $('#registrationId').val();

        // Only validate the last page before the car types
        if(validate){
           if(!dropdownValidate(body, registration)){
                return false;
            }
        }

        // Build a url from the slug values and redirect the user
        dropdownRedirects(slug, manufacturer, model, body, registration);
    });
}

/*
 * Validate that the required vehicle dropdowns have been selected.
 *
 */
function dropdownValidate(body, registration){

    // If body not set then request it from the user
    if(!body.length) {
        $( '#dropdownError' ).html('<p class="alert alert-danger" role="alert">Please select a body from above.</p>');
        $( '#dropdownError' ).fadeIn('slow');
        return false;x
    }

    // If body not set then request it from the user
    if(!registration.length) {
        $( '#dropdownError' ).html('<p class="alert alert-danger" role="alert">Please select a registration from above.</p>');
        $( '#dropdownError' ).fadeIn('slow');
        return false;
    }

    return true;
}

/*
 * When dynamicDropdowns have been submitted build the url they are being redirected to
 * and redirect them to that url.
 */
function dropdownRedirects(url, manufacturer, model, body, registration){

    // Set the default url starting slug as "towbars"
    url = (typeof url !== 'undefined') ?  url : '/towbars/';

    if(manufacturer.length) {
        url = url + manufacturer + '/';

        if(model.length) {
            url = url + model + '/';

            if(body.length) {

                url = url + body + '/';

                if(registration.length) {
                    $( '#dropdownError' ).fadeOut('slow');

                    url = url + registration + '/';
                }
            }
        }
    }

    window.location = url;
}

/*
 * Set up the dynamic vehicle dropdowns on the page
 *
 */
function setUpVehicleDropdownsForm(vehicle_url_call, form_id){

    var manufacturer_dropdown = form_id + ' .manufacturer';
    var model_dropdown = form_id + ' .model';
    var body_dropdown = form_id + ' .body';
    var registration_dropdown = form_id + ' .registration';

    var manufacturer_slug = form_id + " input[name='manufacturerSlug']";
    var model_slug = form_id + " input[name='modelSlug']";
    var body_slug = form_id + " input[name='bodySlug']";
    var registration_id = form_id + " input[name='registrationId']";

    // When manufacturer dropdown changed update the other dropdowns
    if ( $( manufacturer_dropdown ).length ) {

        // If the manufacturer dropdown has no data then call it via ajax
        if( $( manufacturer_dropdown + " option").length <= 1){

            $( manufacturer_dropdown ).dynamicDropdowns( vehicle_url_call + "/ajax_manufacturers" );
        }

        $( manufacturer_dropdown ).change(function() {

            // Check if its not the first default empty option.
            if($( this ).prop('selectedIndex') == 0){

                // Clear the model dropdowns if no data to call.
                $( model_dropdown ).find('option:gt(0)').remove();

                $( manufacturer_slug ).val("");
            } else {
                // Call the data.
                $( model_dropdown ).dynamicDropdowns( vehicle_url_call + '/ajax_models/' + $(this).val());

                $( manufacturer_slug ).val($('option:selected', this).attr('data-slug'));
            }

            // Clear the dropdowns
            $( body_dropdown ).find('option:gt(0)').remove();
            $( registration_dropdown ).find('option:gt(0)').remove();

            // Clear the slug values
            $(model_slug).val("");
            $(body_slug).val("");
            $(registration_id).val("");
        });

        clearDropdownSelection( $( manufacturer_dropdown ) );
    }

    // When model dropdown changed update the next bodies dropdown
    if ( $( model_dropdown ).length ) {
        $(model_dropdown).change(function() {

            // Check if its not the first default empty option.
            if($( this ).prop('selectedIndex') == 0){

                // Clear the model dropdowns if no data to call.
                $( body_dropdown ).find('option:gt(0)').remove();

                $(model_slug).val("");
            } else {
                // Call the data.
                $(body_dropdown).dynamicDropdowns( vehicle_url_call + '/ajax_bodies/' + $(this).val());

                $(model_slug).val($('option:selected', this).attr('data-slug'));
            }

            // Clear any dropdowns
            $(registration_dropdown).find('option:gt(0)').remove();

            // Clear the slug values
            $(body_slug).val("");
            $(registration_id).val("");
        });

        clearDropdownSelection( $( model_dropdown ) );
    }

    // When body dropdown changed update the next registrations dropdown
    $(body_dropdown).change(function() {

        console.log("test here");

        // Check if its not the first default empty option.
        if($( this ).prop('selectedIndex') == 0){

            // Clear the model dropdowns if no data to call.
            $( registration_dropdown ).find('option:gt(0)').remove();
            $(body_slug).val("");

        } else {
            // Call the data.
            $(registration_dropdown).dynamicDropdowns( vehicle_url_call + '/ajax_registrations/' + $(this).val());

            $(body_slug).val($('option:selected', this).attr('data-slug'));
        }

        // Clear the slug values
        $(registration_id).val("");
    });

    // When registration dropdown selected record the slug value.
    $( registration_dropdown ).change(function() {

        // Check if its not the first default empty option.
        if($( this ).prop('selectedIndex') != 0){

            $( registration_id ).val($('option:selected', this).attr('data-slug'));
        } else {
            $( registration_id ).val("");
        }
    });

    clearDropdownSelection( $( body_dropdown ) );
    clearDropdownSelection( $( registration_dropdown ) );
}

/*
 * Set up the dynamic vehicle dropdowns form so when submitted call the correct functions
 *
 */
function setUpVehicleRedirectForm(slug, form_id, validate){

    var manufacturer_slug = form_id + " input[name='manufacturerSlug']";
    var model_slug = form_id + " input[name='modelSlug']";
    var body_slug = form_id + " input[name='bodySlug']";
    var registration_id = form_id + " input[name='registrationId']";

    if(typeof validate === 'undefined'){
        validate = false;
    }

    $( form_id ).on('submit', function(e){
        e.preventDefault();

        // Get the dropdown values.
        var manufacturer = $(manufacturer_slug).val();
        var model = $(model_slug).val();
        var body = $(body_slug).val();
        var registration = $(registration_id).val();

        // Only validate the last page before the car types
        if(validate){
           if(!dropdownValidate(body, registration)){
                return false;
            }
        }

        // Build a url from the slug values and redirect the user
        dropdownRedirects(slug, manufacturer, model, body, registration);
    });
}

/**
 * When dynamicDropdowns have been submitted build the url they are being redirected to
 * @param  string url
 * @param  string manufacturer
 * @param  string model
 * @param  string body
 * @param  string registration
 * @return string url
 */
function dropdownRedirectsURL(url, manufacturer, model, body, registration){

    // Set the default url starting slug as "towbars"
    url = (typeof url !== 'undefined') ?  url : '/towbars/';

    if(manufacturer.length) {
        url = url + manufacturer + '/';

        if(model.length) {
            url = url + model + '/';

            if(body.length) {

                url = url + body + '/';

                if(registration.length) {
                    $( '#dropdownError' ).fadeOut('slow');

                    url = url + registration + '/';
                }
            }
        }
    }

    return url;
}
