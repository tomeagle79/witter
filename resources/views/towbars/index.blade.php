@extends('layouts.frontend')

@section('title', 'Car & Motorhome Towbars | UK Tow Bars')

@section('heading')
	Towbars
@stop

@section('meta_description')
	Car and motorhome towbars by Witter Towbars. Our all-in-one price includes towbar, electric kit and fitting. Buy and arrange a fitting online today.
@stop

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-9 col-md-push-3">
			<div class="row">
				<div class="col-md-8">

					@include('partials.search-dropdowns', ['redirect_slug' => 'towbars', 'form_button_text' => 'Find your towbar'])

				</div>
				<div class="col-md-4 hidden-sm hidden-xs">
					@include('partials.sidebar-findlink-primary')

					@include('partials.sidebar-price')
				</div>
			</div>
			<div class="towbars-by-manufacturer towbars-listing">
				<h2 class="towbars-by-manufacturer-title">Buy a towbar and <span class="slim">book a fitting online</span></h2>

				<p>Purchasing and fitting a towbar can add versatility to your car, van or motorhome.</p>

				<p>We have an extensive range of 5000 towbars listed online. We design our products to conform to different vehicle specifications. So whether you are looking for a towbar to pull a trailer or a horsebox, we have a product to suit you.</p>

				<p>When it comes to quality we go beyond the limits. Our manufacturing process uses materials that are robust and built to last a lifetime. We also test our products beyond the minimum standards. When you buy from Witter Towbars, you can rest assured that the towbar, <a href="/electrical-kits">electrical kit</a> and the fitting work is of unrivalled quality.</p>

				<p>Choose the relevant vehicle manufacturer to find out which towbars are suitable. We make towbars to fit the following car manufacturers.</p>

                <?php
                // Count
                $count = count($manufacturers);
                ?>
				@foreach ($manufacturers as $key => $manufacturer)

					<?php $key++ ?>

					@if($key%12 === 1)
						<div class="row lazy-load-images item-list flex-items">
					@endif

					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

						<div class="item">
							<div class="item-body">

								<div class="manufacturer-logo">
									<a href="{{ route('towbars.model_list', $manufacturer->slug) }}">
										<img src="" data-src="{{ image_load($manufacturer->logoUrl) }}" alt="Towbars for {{ $manufacturer->manufacturerName }}" >

		                            	<h3 class="title-small">{{ $manufacturer->manufacturerName }} Towbars</h3>
									</a>
								</div>
							</div><!-- /.item-body -->
						</div><!-- /.item -->
					</div>

					@if($key%12 === 0 && $key > 0 || $key == $count)
						</div><!-- .row -->
					@endif
				@endforeach
			</div>
		</div> <!-- .col-md-9 -->

		<div class="col-md-3 col-md-pull-9">
			@include('partials.sidebar-search')

			@include('partials.sidebar-info-menu')

		</div> <!-- .col-md-3 -->
	</div>
</div>
@stop
