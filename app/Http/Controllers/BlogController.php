<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use App\Models\BlogPost;

class BlogController extends FrontendController
{

    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {
        parent::__construct();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb('Blog', route('blog'));
    }

    /**
     * Show all blog posts
     *
     * @return Response
     */
    public function index()
    {
        // Get all categories
        $this->data['categories'] = BlogCategory::get();

        // Get and paginate blog posts
        $this->data['blog_posts'] = BlogPost::published()->orderBy('created_at', 'desc')->paginate(config('settings.pagination.blog'));

        return view('blog.index', $this->data);
    }

    /**
     * Show a list of posts in a category
     *
     * @param  string $slug
     * @return Response
     */
    public function viewCategory($slug, $category_id)
    {
        // Get all categories
        $this->data['categories'] = BlogCategory::get();
        $this->data['category'] = $category = BlogCategory::find($category_id);

        // Breadcrumbs::setCurrentRoute('blog.category', $this->data['category']);

        // Get and paginate blog posts
        $this->data['blog_posts'] = $category->posts()->paginate(config('settings.pagination.blog'));

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb($category->title);

        return view('blog.index', $this->data);
    }

    /**
     * Show a blog post
     *
     * @param  string $slug
     * @return Response
     */
    public function viewPost($slug, $post_id)
    {
        // Get all categories
        $this->data['categories'] = BlogCategory::get();

        $this->data['post'] = $post = BlogPost::published()->find($post_id);

        if (empty($post)) {

            return redirect('blog');
        }

        $this->data['category'] = $category = $post->categories->first();

        // Get a random related blog post in the same category.
        $this->data['blog'] = $category->posts()->orderBy(\DB::raw('RAND()'))->first();

        // Set breadcrumb
        $this->data['breadcrumbs']->addCrumb($post->title);

        return view('blog.post', $this->data);
    }
}
