<?php

namespace App\Models\Api;

use  App\Models\Api\ApiModel;

class Electric extends ApiModel
{
	const URI = "electrics";

	/**
	 * Get a single
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'/slug?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a single
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_kits_by_id($vehicle_id, $registrationDate = null)
	{
        $url = config('witter.api_base') . self::URI .'/'. $vehicle_id . (empty($registrationDate) ? '' : '?registrationDate='. $registrationDate);
		$obj = self::get_request($url);
		
		return $obj;
	}

	/**
	 * Get a single accessory
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_single_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'?' . http_build_query($args);
		$obj = self::get_request($url);
		
		return $obj;
	}
}
