<?php

namespace App\Models\Api\Commercial;

use App\Models\Api\ApiModel;


class CommercialVehicleManufacturer extends ApiModel
{
	const URI = "commercials/manufacturers";

	/**
	 * Get all manufacturers
	 *
	 * @return Object
	 */
	static public function get_all()
	{
		$url = config('witter.api_base') . self::URI;
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get all manufacturers based restrictions
	 *
	 * @param Int
	 * @return Object
	 */
	static public function get_by_type(array $args)
	{
		$url = config('witter.api_base') . 'commercials/types/slug/manufacturers?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}
}
