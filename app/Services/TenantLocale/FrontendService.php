<?php

namespace App\Services\TenantLocale;

use App\Contracts\TenantLocaleContext;
use Illuminate\Http\Request;

class FrontendService implements TenantLocaleContext
{
    protected $locale;
    protected $website;
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Gets the website for the current request
     */
    public function getWebsite()
    {
        // If the website data has already been set.
        if (!empty($this->webiste)) {

            return $this->website;
        }

        // Get the domain
        $domain = parse_url($this->request->url(), PHP_URL_HOST);

        if (array_key_exists($domain, config('locale.domains'))) {

            $this->website = (object) config('locale.domains')[$domain];

        } else {
            // Just and empty object
            $this->website = (object) [];
        }

        // If this is a trade site set some trade values.
        if (!empty($this->website->type) && $this->website->type == 'trade') {

            view()->share('noindex_nofollow_page', true);

            // redirect_url used to return users to the main Witter site.
            view()->share('redirect_url', $this->website->redirect_url);
        }

        // We will have different koop scripts per domain so set script location here.
        if(!empty($this->website->koop_script)) {

            view()->share('koop_script', $this->website->koop_script);
        }

        return $this->website;
    }

    /**
     * Gets the locale for the current request
     */
    public function getLocale()
    {
        // do we need to get locale?
        if (empty($this->locale)) {

            $this->locale = null;
        }

        return $this->locale;
    }
}
