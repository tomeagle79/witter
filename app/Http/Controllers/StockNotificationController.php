<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests; 
use App\Models\Api\Notification;

use App\Http\Requests\TowbarStockNotificationRequest;

use Session;

class StockNotificationController extends FrontendController
{
    /**
     * Any code that should be run on every front end page
     */
    public function __construct()
    {   
         parent::__construct();
    }

    /**
     * Add items to basket
     *
     * @return Response
     */
    public function add_notification(TowbarStockNotificationRequest $request)
    {
        // Create a notification 
        $notification = [
            'firstName' => $request->first_name,
            'surname' => $request->surname,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'postcode' => $request->postcode,
            'vtId' => $request->vtid,
            'towbar' => $request->towbar,
        ];

        $response = Notification::new_notification($notification);

        if($response->success == true) {
            return redirect()->back()->with('success', 'Thank you, you will now receive stock notifications for this item.');
        } else {
            return redirect()->back()->with('error', 'Sorry but it looks like something went wrong with creating stock notifications.  Please try again.');
        }
    }
}
