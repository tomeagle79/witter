// File: elixir-extensions.js

var gulp = require('gulp');
var shell = require('gulp-shell');
var Elixir = require('laravel-elixir');
var gulpSakugawa = require('gulp-sakugawa');

var Task = Elixir.Task;

Elixir.extend('ie_split_css', function() {

    new Task('ie_split_css', function() {

        return gulp.src('public_html/assets/css/app.css')
            .pipe(gulpSakugawa({
                maxSelectors: 4000,
                mediaQueries: 'separate',
                suffix: '_'
            }))
            .pipe(gulp.dest('public_html/assets/css/ie/.'));
    });
});
