<?php

namespace App\Models\Api\Commercial;

use  App\Models\Api\ApiModel;

class CommercialRoofbars extends ApiModel
{
	const URI = "roofbars";

	/**
	 * Get a vehicle list
	 *
	 * @params Array 
	 * @return Object
	 */
	static public function get_single_where(array $args)
	{
		$url = config('witter.api_base') . self::URI . '?' . http_build_query($args);
		$obj = self::get_request($url);
		
		return $obj;
	}
}
