@extends('layouts.frontend')

@section('title', $towbars->manufacturerName .' '. $towbars->modelName .' '. $towbars->bodyName .' towbars')

@section('heading')
	Towbars for {{ $towbars->manufacturerName }} {{ $towbars->modelName }} {{ $towbars->bodyName }}
@stop

@section('meta_description')
	Towbars for {{ $towbars->manufacturerName }} {{ $towbars->modelName }} {{ $towbars->bodyName }}
@stop

@section('body-class', 'body_towbar_listing')

@section('content')

<div class="container">
    <div class="towbars-product-listing">

        @if(!empty($towbar_list))

            <div class="row">

        		<div class="col-xs-12 col-sm-9">

                    <div class="row">

                        <div class="col-xs-12 numberplate">

                            @if(!empty($numberplate))

                                @include('partials.numberplate-search')

                            @endif

                        </div><!-- /.col-xs-12 -->

                        <div class="col-xs-12 towbars-listing-title">

                            <p class="pre-title">We have the following towbars to fit your</p>

                            <h2>{{ $towbars->fullVariantName }}</h2>

                        </div><!-- /.col-xs-12 col-xs-push-12 col-sm-12 col-xs-push-0 -->

                    </div><!-- /.row -->
                </div> <!-- .col-sm-9 -->

                <div class="col-sm-3">
                    <div class="top-img-wrapper">
                        @if(empty($towbars->photoUrl))
                            @if(!empty($manufacturer))
                                <img src="<?php echo $manufacturer->logoUrl; ?>" alt="<?php echo $towbars->fullVariantName; ?>" class="top-img"/>
                            @endif
                        @else
                            <img src="<?php echo $towbars->photoUrl; ?>" alt="<?php echo $towbars->fullVariantName; ?>" class="top-img"/>
                            <p class="text-center">Images are representative only</p>
                        @endif
                    </div> <!-- .wrapper -->
                </div> <!-- .col-sm-3 -->

            </div> <!-- .row -->

            <div class="row">
                <div class="col-md-12 item-list">

                    @foreach($towbar_list as $towbar)

                        @include('partials.analytics.json_product_details', [
                            'product' => $towbar,
                            'list_name' => 'Category Results',
                            'category' => 'Towbars',
                            'position' => $loop->iteration + (($towbar_list->currentPage() - 1) * $towbar_list->perPage())
                        ])

                        <div class="towbar-listing">

                            <div class="row towbar-listing-content">

                                <div class="col-md-3 towbar-image">
                                    @if(!empty($registration_date))
                                        <a href="{{ route('towbars.view', ['towbar_slug' => $towbar->slug, 'registration_date' => $registration_date]) }}?variantTowbarId={{ $towbar->variantTowbarId }}&variant={{  base64_encode($towbars->fullVariantName) }}" class="product-link ribbon-conatiner" data-product-id="{{ $towbar->towbarPartNo }}">
                                            @include('partials.ribbon.webfit')
                                            <img src="{{ image_load($towbar->imageUrls[1]) }}" alt="">
                                        </a>
                                    @else
                                        <a href="{{ route('towbars.view', ['towbar_slug' => $towbar->slug]) }}?variantTowbarId={{ $towbar->variantTowbarId }}&variant={{ base64_encode($towbars->fullVariantName) }}" class="product-link ribbon-conatiner" data-product-id="{{ $towbar->towbarPartNo }}">
                                            @include('partials.ribbon.webfit')
                                            <img src="{{ image_load($towbar->imageUrls[1]) }}" alt="">
                                        </a>
                                    @endif
                                </div> <!-- .col-md-3 -->
                                <div class="col-md-6">
                                    @if(!empty($registration_date))
                                        <h3><a href="{{ route('towbars.view', ['towbar_slug' => $towbar->slug, 'registration_date' => $registration_date]) }}?variantTowbarId={{ $towbar->variantTowbarId }}&variant={{ base64_encode($towbars->fullVariantName) }}" class="product-link" data-product-id="{{ $towbar->towbarPartNo }}">{{ $towbar->title }}</a></h3>
                                    @else
                                        <h3><a href="{{ route('towbars.view', ['towbar_slug' => $towbar->slug]) }}?variantTowbarId={{ $towbar->variantTowbarId }}&variant={{ base64_encode($towbars->fullVariantName) }}" class="product-link" data-product-id="{{ $towbar->towbarPartNo }}">{{ $towbar->title }}</a></h3>
                                    @endif

                                    <p class="hidden-xs">{{ $towbar->description }}</p>

                                    <ul class="row towbar-includes">
                                        @if(towbar_in_stock($towbar) && ($towbar->stockLevel > 5 || $towbar->stockLevel == 0))

                                            <li class="col-sm-6 col-xs-12 stock in-stock">
                                                <div class="include-item">In Stock</div>
                                            </li> <!-- col-sm-6 -->
                                        @elseif(towbar_in_stock($towbar) && $towbar->stockLevel <= 5)
                                            <li class="col-sm-6 col-xs-12 stock low-stock">
                                                <div class="include-item">Hurry, only {{ $towbar->stockLevel }} left!</div>
                                            </li> <!-- col-sm-6 -->
                                        @else
                                            <li class="col-sm-6 col-xs-12 stock out-of-stock">
                                                <div class="include-item">Out of Stock</div>
                                            </li> <!-- col-sm-6 -->
                                        @endif

                                        <li class="col-sm-6 col-xs-12 tick-li">
                                            <div class="include-item">Free UK Delivery</div>
                                        </li> <!-- col-sm-6 -->

                                        <li class="col-xs-12 tick-li">
                                            <div class="include-item">Mobile and Workshop Fitting Available</div>
                                        </li> <!-- .col-md-4 col-sm-6 -->
                                    </ul> <!-- .row -->

                                <div class="benefits-toggle hidden-xs hidden-sm">
                                    <a href="#{{$towbar->variantTowbarId}}" data-toggle="collapse" class="benefits-toggle-link collapsed">View all benefits</a>
                                </div> <!-- .benefits-toggle -->

                                </div> <!-- .col-md-6 -->

                                <div class="col-xs-12 col-md-3 text-right">

                                    <div class="row price-wrapper">
                                        <div class="col-xs-push-6 col-xs-6 col-md-12 col-md-push-0 towbar-brand-logo">

                                            @if(strtolower($towbar->brandName) == 'witter')
                                                <img src="/assets/img/towbars/witter-logo.png" alt="{{ $towbar->brandName }}" width="80">
                                            @elseif(strtolower($towbar->brandName) == 'westfalia')
                                                <img src="/assets/img/towbars/westfalia-logo.png" alt="{{ $towbar->brandName }}" width="160">
                                            @endif

                                        </div><!-- /.col-md-12 -->

                                        <div class="col-xs-pull-6 col-xs-6 col-md-12 col-md-pull-0">

                                            <div class="price">
                                                Fully fitted price from
                                                <span>&pound;{{ price($towbar->price) }}</span>
                                            </div> <!-- .price -->
                                        </div><!-- /.col-md-12 -->

                                    </div><!-- /.row -->

                                    <div class="row">
                                        <div class="col-xs-12 btn-links">

                                            @if($towbar->inStock)

                                                @if(!empty($registration_date))
                                                    <a href="{{ route('towbars.view', ['towbar_slug' => $towbar->slug, 'registration_date' => $registration_date]) }}?variantTowbarId={{ $towbar->variantTowbarId }}&variant={{ base64_encode($towbars->fullVariantName) }}" class="btn btn-primary btn-block product-link" data-product-id="{{ $towbar->towbarPartNo }}">View Details</a>
                                                @else
                                                    <a href="{{ route('towbars.view', ['towbar_slug' => $towbar->slug]) }}?variantTowbarId={{ $towbar->variantTowbarId }}&variant={{ base64_encode($towbars->fullVariantName) }}" class="btn btn-primary btn-block product-link" data-product-id="{{ $towbar->towbarPartNo }}">View Details</a>
                                                @endif

                                            @else
                                                <a href="{{ route('towbars.view', ['towbar_slug' => $towbar->slug]) }}?variantTowbarId={{ $towbar->variantTowbarId }}&variant={{ base64_encode($towbars->manufacturerName . ' ' . $towbars->modelName . ' ' . $towbars->bodyName) }}" class="btn btn-primary btn-oos-model">Notify me when in stock</a>
                                            @endif

                                        </div><!-- /.col-xs-12 -->

                                    </div><!-- /.row -->

                                </div> <!-- .col-md-3 -->
                            </div> <!-- .row -->

                            <div class="row collapse" id="{{$towbar->variantTowbarId}}">

                                <div class="col-xs-12 col-md-9 col-md-offset-3">

                                    <div class="benefits hidden-xs hidden-sm">

                                        @foreach($towbar->benefits as $benefit)

                                            @if($loop->iteration%2 === 1)
                                                <ul class="row towbar-includes">
                                            @endif

                                                <li class="col-xs-12 col-md-6 tick-li">{{ $benefit }}</li>

                                            @if($loop->iteration%2 === 0 || $loop->last)
                                                </ul>
                                            @endif

                                        @endforeach

                                    </div><!-- /.benefits -->
                                </div><!-- /.col-md-9 -->

                            </div><!-- /.row -->

                        </div> <!-- .towbar-listing -->
                    @endforeach

                    <hr>

                </div> <!-- .col-md-12 -->
            </div> <!-- .row -->

            <div class="row">
                {{ $towbar_list->links() }}
            </div>

        @else
            {{-- Out of stock  --}}
            <div class="row">

                <div class="col-md-8">

                    <h2 class="nomargin">{{ $towbars->fullVariantName }}</h2>

                    <div class="button-area">
                        <a href="{{ route('towbars') }}" class="btn btn-primary">Not your Vehicle?</a>
                    </div>

                    <div class="row">

                        <div class="col-xs-12">

                            @if($towbars->cannotTow)

                                <h3>We don't have a towbar for this vehicle</h3>

                                <p>Sorry, it looks like we don't have a towbar to fit your vehicle. It appears your vehicle does not have the capacity to tow so we will not produce a towbar to fit it. Find out more by reading our frequently asked questions below.</p>

                                <h3>How do I know if my vehicle can't tow?</h3>

                                <p>The weight that your vehicle can tow depends on its towing capacity. If we don't have a towbar available, it could mean that your vehicle's towing capacity is 0, and therefore unable to tow anything. You can find your vehicle's towing capacity by locating the VIN plate in your car and following our guide to work out what you can tow: <a href="{{ route('what_can_i_tow') }}">What Can My Car Tow?</a></p>

                                <h3>What does 'not homologated to tow' mean?</h3>

                                <p>If your vehicle is not homologated to tow, this simply means that it has not been approved to tow by the vehicle manufacturer. This means your vehicle manufacturer has decided that the model of your vehicle is not suitable or safe for towing any loads.</p>

                                <h3>What if I only want a towbar bike rack?</h3>

                                <p>Unfortunately, if your vehicle has not been approved for towing, we are unable to supply a towbar - even if you only intend on using a towbar bike rack. Your safety is important to us so we would not create or sell you a product if your vehicle is not fit for purpose. This means that if your vehicle cannot tow a trailer or caravan, it can't have a towbar bike rack attached either.</p>

                                <h3>Are there any legal reasons why my vehicle can't tow?</h3>

                                <p>Aside from putting your safety at risk, there are a number of other reasons you should not have a towbar installed if your vehicle isn't suitable for towing. If you install a towbar on a vehicle that has not been approved for towing or the towbar has not been developed for your specific vehicle, it could void your insurance. You may face other driving penalties such as a fine, points on your licence and a driving ban for using your vehicle in a dangerous condition. That is why we do not design or supply towbars for vehicles that are not suitable for towing.</p>

                                <h3>Can I buy a different towbar if you don't have one for my vehicle?</h3>

                                <p>No. All of our towbars have been designed to the exact specifications of each vehicle. This means that they are the correct size for the vehicle they have been designed for and would be unsafe if installed onto another vehicle.</p>

                                <h3>Are there any other reasons why there is no towbar available for my car?</h3>

                                <p>If the demand is too small to create a towbar for your vehicle, we may not have one available. If you know your vehicle has the capacity to tow, contact us so we can measure your vehicle and we may be able to create a towbar for you. If your vehicle has no towing capacity or is too old, then you will never be able to have a towbar as it is deemed unsafe.</p>

                            @else

                                @if($towbars->age < 3)
                                {{-- New vehicle message --}}
                                <h3>We don't have a towbar for this vehicle</h3>

                                <p>Sorry, it looks like we don't have a towbar to fit your vehicle. If you have a brand new vehicle, we might already have a towbar in development and it's not quite finished. However, it could also mean that we've not had the chance to measure the model of your car or van and start creating a specific towbar yet.</p>

                                <p>If you've checked the towing capacity of your new car or van and you're sure it can tow, get in touch with us to see if we have a towbar in development. Fill in your personal details and vehicle details below and we will let you know if there is a towbar in development already. Or, we will arrange to measure your vehicle and start producing your towbar.</p>

                                <h3>Want to see if we're developing a towbar? Contact us below.</h3>

                                @elseif($towbars->age >= 10)
                                {{-- Old vehicle message --}}

                                <h3>We don't have a towbar for this vehicle</h3>

                                <p>Sorry, it looks like we don't have a towbar to fit your vehicle. It appears you have an older car and we will not be producing towbars for your make and model anymore. Find out more by reading our frequently asked questions below.</p>

                                <h3>Can I have a towbar if I have an older car?</h3>

                                <p>If you have an older make or model that your vehicle manufacturer no longer produces, it is likely that we have stopped production of the towbar too. We are therefore unable to supply you with a towbar for your vehicle.</p>

                                <h3>Can I buy a different towbar if you don't have one for my vehicle?</h3>

                                <p>No. All of our towbars have been designed to the exact specifications of each vehicle. This means that they are the correct size for the vehicle they have been designed for and would be unsafe if installed onto another vehicle.</p>

                                <h3>What if I only want a towbar bike rack?</h3>

                                <p>Unfortunately, if we no longer produce a compatible towbar for your vehicle, you will not be able to install a towbar, even if you only want to use a towbar bike rack. Your safety is important to us, so we would not create or sell you a product if your vehicle is not fit for purpose. This means that if we don't have a towbar that is suitable, you can't have a towbar bike rack attached either.</p>

                                <h3>Are there any legal reasons why my vehicle can't tow?</h3>

                                <p>Aside from putting your safety at risk, there are a number of other reasons you should not have a towbar installed if your vehicle isn't suitable for towing. If you install a towbar on a vehicle that has not been approved for towing, it could void your insurance. You may face other driving penalties such as a fine, points on your licence and a driving ban for using your vehicle in a dangerous condition. That is why we do not design or supply towbars for vehicles that are not suitable for towing.</p>

                                @else
                                {{-- General no towbar message --}}
                                <h3>We don't have a towbar for this vehicle</h3>

                                <p>Sorry, it looks like we don't have a towbar to fit your vehicle. This could be due to a number of reasons.</p>

                                <p>Check the towing capacity of your vehicle and if you're certain it can tow, please fill out the contact form below and we will be in touch to discuss your options. There may already be a towbar in production, but it looks like we haven't got a towbar available for you at the moment.</p>

                                <h3>Want to see if we're developing a towbar? Contact us below.</h3>

                                @endif

                            @endif
                        </div>
                    </div>

                </div> <!-- .col-md-8 -->

                <div class="col-md-4">
                    <div class="top-img-wrapper">
                        @if(empty($towbars->photoUrl))
                            @if(!empty($manufacturer))
                                <img src="<?php echo $manufacturer->logoUrl; ?>" alt="<?php echo $towbars->fullVariantName; ?>" class="top-img"/>
                            @endif
                        @else
                            <img src="<?php echo $towbars->photoUrl; ?>" alt="<?php echo $towbars->fullVariantName; ?>" class="top-img"/>
                            <p class="text-center">Images are representative only</p>
                        @endif
                    </div> <!-- .wrapper -->
                </div> <!-- .col-md-6 -->
            </div> <!-- .row -->

            @if(empty($towbars->cannotTow) && $towbars->age < 10)

            <div class="row contact">
                <div class="col-md-3 get-in-touch">
                    <h2 class="border-bottom">Contact us</h2>
                    <p>
                        Call us on<br>
                        <a href="tel:+4401244284555"><span class="color-secondary">01244 284555</span></a><br><br>
                        Send us an email<br>
                        <span class="color-secondary"><a href="mailto:ecommerce@witter-towbars.co.uk" class="email-font">ecommerce@witter-towbars.co.uk</a></span>
                    </p>
                </div>

                <div class="col-md-6 get-in-touch">
                    <h2 class="border-bottom">Email us</h2>

                    {!! Form::open(array('route' => 'contact.submit_product_enquiry', 'class' => 'form', 'id' => 'contact_form', 'onsubmit' => "
                                gtag('event', 'Out of stock', {
                                    event_category: 'Form Submission',
                                    event_label: '". $towbars->fullVariantName ."'
                                });
                            ")) !!}

                        <input type="hidden" name="enquiry_regarding" value="Towbar for {{ $towbars->fullVariantName }}">
                        <input type="hidden" name="website_url" value="{{ url()->current() }}?nofire=true">

                        <div class="form-group">

                            {!! Form::label('name', 'Name', array('class' => 'sr-only')) !!}

                            {!! Form::text('name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                        </div>

                        <div class="form-group">

                            {!! Form::label('email', 'Email Address', array('class' => 'sr-only')) !!}

                            {!! Form::text('email', null,
                                array('required', 'class'=>'form-control', 'placeholder'=>'Email Address')) !!}
                        </div>

                        <div class="form-group">

                            {!! Form::label('phone', 'Telephone', array('class' => 'sr-only')) !!}

                            {!! Form::text('phone', null,
                                array('required', 'class'=>'form-control', 'placeholder'=>'Telephone')) !!}
                        </div>

                        <div class="form-group">

                            {!! Form::label('postcode', 'Postcode', array('class' => 'sr-only')) !!}

                            {!! Form::text('postcode', null,
                                array('class'=>'form-control', 'placeholder'=>'Postcode')) !!}
                        </div>

                        <div class="form-group">

                            {!! Form::label('enquiry', 'Enquiry', array('class' => 'sr-only')) !!}

                            {!! Form::textarea('enquiry', null,
                                array('required', 'class'=>'form-control', 'placeholder'=>'Enquiry')) !!}
                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary">Submit Enquiry </button>

                        </div>
                    {!! Form::close() !!}
                </div>

            </div>
            @endif

        @endif
    </div><!-- /.card-clear -->
</div> <!-- .container -->
@stop

@section('scripts')
    <script type="text/javascript">
        {{-- It is an array when there are no towbars in the list --}}
        @if(!is_array($towbar_list))
            @include('partials.analytics.list', [
                'products' => $towbar_list,
                'page' => $towbar_list->currentPage(),
                'per_page' => $towbar_list->perPage(),
                'list_name' => 'Category Results',
                'category' => 'Towbars'
            ])
        @endif

        @if(empty($towbars->towbarTypes))
            $(document).ready(function(){
                $("form#contact_form").submit(function() {
                    $('button.btn').attr('disabled', 'disabled');
                    return true;
                });
            });
        @endif

        {{--
            Create the out stock ga event
            eventAction is the page title
            eventLabel is the url
        --}}

        @if(!request()->nofire)
            @if(!empty($ga_all_out_of_stock))

                gtag('event', '{{ $towbars->manufacturerName .' '. $towbars->modelName .' '. $towbars->bodyName .' towbars' }}', {
                    event_category: 'Out of stock',
                    event_label: '{{ url()->current() }}'
                });

            @elseif(!empty($ga_some_out_of_stock))

                gtag('event', '{{ $towbars->manufacturerName .' '. $towbars->modelName .' '. $towbars->bodyName .' towbars' }}', {
                    event_category: 'Some in stock',
                    event_label: '{{ url()->current() }}'
                });

            @else

                gtag('event', '{{ $towbars->manufacturerName .' '. $towbars->modelName .' '. $towbars->bodyName .' towbars' }}', {
                    event_category: 'In stock',
                    event_label: '{{ url()->current() }}'
                });

            @endif
        @endif
    </script>


@stop
