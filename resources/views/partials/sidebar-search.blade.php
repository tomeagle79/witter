<div class="sidebar-primary">

	<h2>Find your towbar</h2>

	<p>Enter your registration number</p>

	{!! Form::open(['route' => 'search.numberplate', 'class' => 'form numberplate-search', 'method' => 'get', 'onsubmit' => "
		gtag('event', 'Success', {
			event_category: 'Reg lookup',
			event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
		});
	"]) !!}
		{!! Form::text('search', null, ['required', 'class' => 'form-control', 'placeholder' => 'Reg Num']) !!}
		<button type="submit" class="btn btn-primary">Find Your<br>Towbar</button>
	{!! Form::close() !!}

	<p><a href="{{ route('towbars') }}" class="smaller">NOT GOT YOUR REGISTRATION NUMBER?</a></p>

</div>