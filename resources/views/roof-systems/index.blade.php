@extends('layouts.frontend')

@section('title', 'Roof Bars &amp; Racks | Van Roof Systems | Commercial Van Racks ')

@section('heading')
	Roof Bars &amp; Racks
@stop

@section('meta_description')
	Van roof systems by Witter Towbars. We supply a full range of commercial van roof racks and van roof bars. Buy online today.
@stop

@section('content')

<div class="container roof-systems">

    <div class="row">
        @foreach($types as $type)
        <div class="col-md-4 text-center">
            <a href="{{ route('roof-systems.'.$type->slug) }}" class="roof-type {{ $type->slug }}">
				@if($type->commercialTypeName == 'Roof Decks')
					<h2>Roof Racks</h2>
				@else
					<h2>{{ $type->commercialTypeName }}</h2>
				@endif
            </a>
        </div> <!-- .col-md-4 -->
        @endforeach
    </div> <!-- .row -->

	<div class="row">
		<div class="col-md-12">
			<div class="towbars-by-manufacturer">
				<h2 class="towbars-by-manufacturer-title">Find by <span class="slim">Manufacturer</span></h2>

				<p>Designed and manufactured in the UK, our extensive range of Van accessories offer the chance to add versatility and value to your vehicle and your business. From roof bars to bulkhead and internal racking we’ve got you covered for all of your LCV needs.</p>
                <?php
                // Count
                $count = count($manufacturers);
                ?>
				@foreach ($manufacturers as $key => $manufacturer)

					<?php $key++ ?>

					@if($key%6 === 1)
						<div class="row">
					@endif

					<div class="col-md-2 col-xs-6">
						<div class="manufacturer-logo">
							<a href="{{ route('roof-systems.models', $manufacturer->slug) }}">
								<img src="{{ image_load($manufacturer->logoUrl) }}" alt="Towbars for {{ $manufacturer->manufacturerName }}" >
							</a>
						</div>
					</div>

					@if($key%6 === 0 && $key > 0 || $key == $count)
						</div><!-- .row -->
					@endif
				@endforeach
			</div>
		</div>
	</div>
</div>
@stop
