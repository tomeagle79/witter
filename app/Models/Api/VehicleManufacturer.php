<?php

namespace App\Models\Api;

use App\Models\Api\ApiModel;


class VehicleManufacturer extends ApiModel
{
	const URI = "manufacturers";

	/**
	 * Get all manufacturers
	 *
	 * @return Object
	 */
	static public function get_all()
	{
		$url = config('witter.api_base') . self::URI;
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a single manufacturer
	 *
	 * @params Array
	 * @return Object
	 */
	static public function get_single_where(array $args)
	{
		$url = config('witter.api_base') . self::URI .'/slug?' . http_build_query($args);
		$obj = self::get_request($url);

		return $obj;
	}

	/**
	 * Get a single manufacturer by id 
	 *
	 * @params string
	 * @return Object
	 */
	static public function get_by_id(string $id)
	{
		$url = config('witter.api_base') . self::URI . '/' . $id;
		$obj = self::get_request($url);

		return $obj;
	}
}
