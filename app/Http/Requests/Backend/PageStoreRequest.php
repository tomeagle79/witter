<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Page; 

class PageStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:255',
            'url_slug' => ['required', 'max:255', 'regex:/^[a-z0-9-_]+$/'],
            'meta_title' => 'max:255',
            'meta_description' => 'max:255',
            'content' => 'required',
        ];

        if($this->route('id')){
            $page = Page::findOrFail($this->route('id'));
        }

        if(!empty($page)){
            // check unique url slug
            $rules['url_slug'][] = 'unique:url_slugs,url_slug,'.$page->url_slug_id.',id,deleted_at,NULL';
        }
        else {
            // check unique url slug
            $rules['url_slug'][] = 'unique:url_slugs,url_slug,NULL,id,deleted_at,NULL';
        }

        return $rules;
    }
}
