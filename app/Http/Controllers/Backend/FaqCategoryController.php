<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\FaqCategoryStoreRequest;
use App\Models\FaqCategory;
use Auth;
use Datatables;
use Hash;
use View;
use DB;
use URL;

class FaqCategoryController extends Controller
{
    /**
     * Instantiate a new instance.
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.faq_categories.index');
    }

    /**
     * Return the datatable for faq_categories
     *
     * @return Datatables
     */
    public function indexDatatable()
    {
        $faq_categories = FaqCategory::select(['id', 'title', 'url_slug', 'created_at', 'updated_at'])->get();

        return Datatables::of($faq_categories)
            ->addColumn('edit', '<a href="{{ URL::backend(\'faq_categories/edit/\'. $id) }}" class="btn green">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'faq_categories/delete/\'. $id) }}" class="btn red" data-confirm="Are you sure you want to delete this faq category?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = FaqCategory::pluck('title', 'id');

        $parent_id = null;

        return view('backend.faq_categories.create', compact('categories', 'parent_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  FaqCategoryStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqCategoryStoreRequest $request)
    {
        // save the faq_category
        $faq_category = new FaqCategory();
        $faq_category->title = $request->title;
        $faq_category->url_slug = str_slug($request->url_slug,'-');
        $faq_category->parent_id = !empty($request->parent_id) ? $request->parent_id : null;
        $faq_category->save();

        return redirect(URL::backend('faq_categories'))->with('success', '<strong>Success!</strong> FAQ Category created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq_category = FaqCategory::find($id);

        $categories = FaqCategory::where('id', '<>', $id)
            ->pluck('title', 'id');

        $parent_id = $faq_category->parent_id;

        return view('backend.faq_categories.edit', compact('faq_category', 'categories', 'parent_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  FaqCategoryStoreRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqCategoryStoreRequest $request, $id)
    {
        // save the faq_category
        $faq_category = FaqCategory::find($id);
        $faq_category->title = $request->title;
        $faq_category->url_slug = str_slug($request->url_slug,'-');
        $faq_category->parent_id = isset($request->parent_id)? $request->parent_id : null; 
        $faq_category->save();

        return redirect(URL::backend('faq_categories'))->with('success', '<strong>Success!</strong> FAQ Category updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq_category = FaqCategory::find($id);
        $faq_category->delete();

        return redirect(URL::backend('faq_categories'))->with('message', '<strong>Success!</strong> FAQ Category deleted.');
    }

    /**
     * Restore the specified resource
     */
    public function restore($id)
    {
        $faq_category = FaqCategory::withTrashed()->find($id);
        $faq_category->restore();

        return redirect(URL::backend('faq_categories'))->with('success', '<div class="alert alert-success"><strong>Success!</strong> FAQ Category restored.</div>');
    }
}
