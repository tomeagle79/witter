@extends('layouts.frontend')

@section('title', 'Find Your Vehicle')

@section('heading')
	Find Your Vehicle
@stop

@section('meta_description')
	Witter Towbars Find Your Vehicle
@stop

@section('content')


<div class="container vehicle-select">
	<div class="row">		
		<div class="col-md-12">

            <div class="alert alert-success" role="alert">

                <p>
                    We have found vehicles matching your registration number. 
                </p>
                <p>
                    Please select your vehicle.
                </p>

            </div>

            @if( !empty($reg->photoUrl) )
                <img src="{{ $reg->photoUrl }}" alt="" class="model-img">
            @endif

            <ul>
            @foreach( $reg->vehicleChoices as $vehicle )
                <li><a href="{{ route('towbars.vehicle_towbars_by_reg', [$vehicle->vehicleId, $reg->registrationDate]) }}">{{ $vehicle->vehicleDescription }}</a></li>
            @endforeach
            </ul>

		</div>
	</div>
</div>
@stop