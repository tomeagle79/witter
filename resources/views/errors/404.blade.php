@extends('layouts.frontend')

@section('title')
	Error - Page Not Found
@stop

@section('heading')
	Error - Page Not Found
@stop

@section('meta_description')
	Error - Page Not Found
@stop

@section('content')

	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<article>

                    <h1>404 - Page Not Found</h1>

					<p>
                        <strong>Sorry</strong>
                        we don't have the page you're looking for.
                    </p>

                    <p>
                        Please try again, and if the problem persists please contact us.
                        <br>
                        <br>
                    </p>

				</article>
			</div> <!-- col-md-12 -->

            <div class="col-md-9 col-md-push-3">
                <div class="row">
                    <div class="col-md-8">

                        @include('partials.search-dropdowns', ['redirect_slug' => 'towbars',
                            'form_button_text' => 'Find your towbar',
                            'title' => "Let's find A Towbar<span class='slim block'>for your car</span>"])
                    </div>
                    <div class="col-md-4 hidden-sm hidden-xs">
                        @include('partials.sidebar-findlink-primary')

                        @include('partials.sidebar-price')
                    </div>
                </div>



            </div> <!-- .col-md-9 -->

            <div class="col-md-3 col-md-pull-9">
                @include('partials.sidebar-search')

                @include('partials.sidebar-info-menu')

            </div> <!-- .col-md-3 -->



		</div> <!-- .row -->
	</div> <!-- .container -->
@stop
