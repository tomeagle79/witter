@extends('layouts.frontend')

@section('title', 'Product Returns')

@section('heading')
	Product Returns
@stop

@section('meta_description')
	Product Returns
@stop

@section('content')

<div class="container">
	<div class="row">
		<div class="col-sm-4 col-md-3">

			@include('partials.sidebar-search')

			@include('partials.sidebar-findlink-secondary')

		</div> <!-- .col-md-3 -->

		<div class="col-sm-8 col-md-9">

            <h2 class="border-bottom">Product Returns</h2>

            <p> Should you have any issues with your Witter product or no longer require it, you have up to 14 days to apply for a return of the item and one of our sales team will be in contact to organise a refund and collection of the item. It helps us if you are able to give as much detail about the reason for return so we can get it organised for you as soon as possible. Please be aware for large items there could be an £18 collection charge. </p>

            {!! Form::open(['route' => 'returns.process', 'method' => 'post', 'class' => 'form-horizontal']) !!}

                <h3>Customer Details</h3>

                <div class="form-group{{ $errors->has('order_number') ? ' has-error' : '' }}">
                    <label for="order_number" class="col-sm-3 control-label">Order number *</label>
                    <div class="col-sm-9">
                        {!! Form::text('order_number', null, ['id' => 'order_number', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    <label for="first_name" class="col-sm-3 control-label">First Name *</label>
                    <div class="col-sm-9">
                        {!! Form::text('first_name', null, ['id' => 'first_name', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                    <label for="surname" class="col-sm-3 control-label">Surname *</label>
                    <div class="col-sm-9">
                        {!! Form::text('surname', null, ['id' => 'surname', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                    <label for="company_name" class="col-sm-3 control-label">Company Name</label>
                    <div class="col-sm-9">
                        {!! Form::text('company_name', null, ['id' => 'company_name', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="address-group">
                    <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                        <label for="postcode" class="col-xs-12 col-sm-3 control-label">Postcode *</label>
                        <div class="col-xs-9 col-sm-7 col-md-5">
                            <div class="input-group">
                                {!! Form::text('postcode',
                                    null,
                                    ['required', 'class'=>'form-control']) !!}
                                <span class="input-group-btn">
                                    <button class="btn btn-default address-lookup" type="button">Address Lookup</button>
                                </span>
                            </div> <!-- .input-group -->

                            <div class="alert alert-danger postcode-alert" style="display:none;"><p></p></div>
                            <div class="alert alert-info loading" style="display:none;"><p>Loading...</p></div>
                            {!! Form::select('address_dropdown',
                                [],
                                null,
                                ['class' => 'form-control address_dropdown']) !!}
                        </div> <!-- .col-sm-9 -->
                    </div> <!-- .form-group -->

                    <div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
                        <label for="address1" class="col-sm-3 control-label">Address 1 *</label>
                        <div class="col-sm-9">
                            {!! Form::text('address1', null, ['id' => 'address1', 'class' => 'form-control address1']) !!}
                        </div> <!-- .col-sm-9 -->
                    </div> <!-- .form-group -->

                    <div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
                        <label for="address2" class="col-sm-3 control-label">Address 2</label>
                        <div class="col-sm-9">
                            {!! Form::text('address2', null, ['id' => 'address2', 'class' => 'form-control address2']) !!}
                        </div> <!-- .col-sm-9 -->
                    </div> <!-- .form-group -->

                    <div class="form-group{{ $errors->has('address3') ? ' has-error' : '' }}">
                        <label for="address3" class="col-sm-3 control-label">Address 3</label>
                        <div class="col-sm-9">
                            {!! Form::text('address3', null, ['id' => 'address3', 'class' => 'form-control address3']) !!}
                        </div> <!-- .col-sm-9 -->
                    </div> <!-- .form-group -->

                    <div class="form-group{{ $errors->has('towncity') ? ' has-error' : '' }}">
                        <label for="towncity" class="col-sm-3 control-label">Town / City *</label>
                        <div class="col-sm-9">
                            {!! Form::text('towncity', null, ['id' => 'towncity', 'class' => 'form-control towncity']) !!}
                        </div> <!-- .col-sm-9 -->
                    </div> <!-- .form-group -->
                </div> <!-- .address-group -->

                <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                    <label for="telephone" class="col-sm-3 control-label">Telephone *</label>
                    <div class="col-sm-9">
                        {!! Form::text('telephone', null, ['id' => 'telephone', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
                    <label for="fax" class="col-sm-3 control-label">Fax</label>
                    <div class="col-sm-9">
                        {!! Form::text('fax', null, ['id' => 'fax', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-sm-3 control-label">Email Address *</label>
                    <div class="col-sm-9">
                        {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->


                <h3>Product Details</h3>

                <div class="form-group">
                    <label for="date_fitted" class="col-sm-3 control-label">Date Fitted *</label>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <div class="col-xs-3">
                                {!! Form::number('date_purchased_dd', null, ['id' => 'date_purchased_dd', 'class' => 'form-control text-center', 'placeholder' => 'DD']) !!}
                            </div>
                            <div class="col-xs-3">
                                {!! Form::number('date_purchased_mm', null, ['id' => 'date_purchased_mm', 'class' => 'form-control text-center', 'placeholder' => 'MM']) !!}
                            </div>
                            <div class="col-xs-3">
                                {!! Form::number('date_purchased_yyyy', null, ['id' => 'date_purchased_yyyy', 'class' => 'form-control text-center', 'placeholder' => 'YYYY']) !!}
                            </div>
                        </div> <!-- .input-group -->
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="form-group{{ $errors->has('product_serial_no') ? ' has-error' : '' }}">
                    <label for="product_serial_no" class="col-sm-3 control-label">Product Serial No  (if known)</label>
                    <div class="col-sm-9">
                        {!! Form::text('product_serial_no', null, ['id' => 'product_serial_no', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="form-group{{ $errors->has('product_part_no') ? ' has-error' : '' }}">
                    <label for="product_part_no" class="col-sm-3 control-label">Product Part No (if known)</label>
                    <div class="col-sm-9">
                        {!! Form::text('product_part_no', null, ['id' => 'product_part_no', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
                    <label for="product_name" class="col-sm-3 control-label">Product Name *</label>
                    <div class="col-sm-9">
                        {!! Form::text('product_name', null, ['id' => 'product_name', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->

                <div class="form-group{{ $errors->has('return_reason') ? ' has-error' : '' }}">
                    <label for="return_reason" class="col-sm-3 control-label">Return Reason *</label>
                    <div class="col-sm-9">
                        {!! Form::textarea('return_reason', null, ['id' => 'return_reason', 'class' => 'form-control']) !!}
                    </div> <!-- .col-sm-9 -->
                </div> <!-- .form-group -->


				<div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
				        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
				</div>
            {!! Form::close() !!}

		</div> <!-- .col-md-9 -->
	</div> <!-- .row -->
</div> <!-- .container -->

@stop


@section('scripts')
    <script>
        $(document).ready(function(){

            // Address lookup button
            $('.address-lookup').on('click', function(){
                var $postcode_alert = $(this).parent().parent().parent().find('.postcode-alert');
                var $loading = $(this).parent().parent().parent().find('.loading');
                var $address_dropdown = $(this).parent().parent().parent().find('.address_dropdown');

                $postcode_alert.hide();
                $loading.hide();
                $address_dropdown.hide();

                var $postcode = $(this).parent().prev();

                if($postcode.val().length == 0) {
                    $('p', $postcode_alert).html('Please enter a postcode');
                    $postcode_alert.fadeIn('slow');
                } else {
                    $loading.fadeIn('slow');

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('postcode.lookup')}}",
                        data: {
                            'postcode': $postcode.val()
                        },
                        success: function(result){
                            $loading.fadeOut('slow');

                            if(result.success == false) {
                                $('p', $postcode_alert).html(result.errorMessage);
                                $postcode_alert.fadeIn('slow');
                            } else {
                                // Success!
                                var $options = $address_dropdown;
                                $options.html('<option>Please select</option>');
                                $.each(result.addressList, function(){
                                    $options.append($("<option />").val(this.key).text(this.value));
                                });
                                $address_dropdown.fadeIn('slow');
                            }
                        },
                        error: function(){
                            $('p', $postcode_alert).html('Sorry, something went wrong');
                            $postcode_alert.fadeIn('slow');
                        }
                    });
                }
            });

            // Address lookup dropdown selection
            $(document).on('change', '.address_dropdown', function() {

                var $loading = $(this).parent().find('.loading');
                var $postcode_alert = $(this).parent().find('.postcode-alert');
                var $this = $(this);

                if($(this).val().length !== 0) {
                    // Look up the full address
                    var key = $(this).val();

                    $(this).fadeOut('slow');
                    $loading.fadeIn('slow');

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('postcode.get_addresses')}}",
                        data: {
                            'key': key
                        },
                        success: function(result){
                            $loading.fadeOut('slow');

                            if(result.success == false) {
                                $('p', $postcode_alert).html(result.errorMessage);
                                $postcode_alert.fadeIn('slow');
                            } else {
                                // Success!
                                $this.parents('.address-group').find('.address1').val( (result.organisationName.length == 0 ? '' : result.organisationName + ', ') + result.line1 );
                                $this.parents('.address-group').find('.address2').val( result.line2 );
                                $this.parents('.address-group').find('.address3').val( result.line3 );
                                $this.parents('.address-group').find('.towncity').val( result.postTown );
                            }
                        },
                        error: function(){
                            $('p', $postcode_alert).html('Sorry, something went wrong');
                            $postcode_alert.fadeIn('slow');
                        }
                    });

                }

            });

        });
    </script>
@stop
