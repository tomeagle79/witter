@extends('layouts.email')

@section('content')

    You received a product notification request from a customer:

    <p>
    Email: {{ $email }}
    </p>

    <p>
    Do they want to be subscribed?: {{ $checkbox_for_subscription }}
    </p>

    
    <p>Product Details</p>

    <p>
    Product: {{ $product_name }}
    </p>

    <p>
    Product ID: {{ $product_id }}
    </p>

    <p>
    Product URL: {{ $product_url }}
    </p>

@stop
