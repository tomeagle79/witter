@extends('layouts.frontend')

@section('title', '')

@section('heading')
	{{-- $category->categoryName --}}
@stop

@section('meta_description')
	Witter Towbars Light Commercial Vehicles Roof Accessories
@stop

@section('content')
	<div class="container witter-listing">
		<div class="row">

			<div class="col-md-12 accessory-listing">

				<h2 class="border-bottom no-top-margin">{{-- $category->categoryName --}}</h2>

				<div class="row item-list is-flex">

                    @foreach($accessories as $accessory)

                        @include('partials.analytics.json_product_details', [
                            'product' => $accessory,
                            'list_name' => 'Category Results',
                            'category' => 'Roof Systems/Accessories',
                            'position' => $loop->iteration
                        ])

                        <div class="col-sm-4 col-md-4">
                            <div class="item text-center">
                                <a href="{!! route('accessories.multi', [$accessory->partNo]) !!}" class="product-link" data-product-id="{{ $accessory->partNo }}"><img src="{{ image_load($accessory->imageUrl) }}" alt="" /></a>

                                <a href="{!! route('accessories.multi', [$accessory->partNo]) !!}" class="product-link" data-product-id="{{ $accessory->partNo }}">
                                    <h3 class="list-title">{!! $accessory->title !!}</h3>
                                </a>
                            </div>
                        </div>

                    @endforeach				

				</div> <!-- .row -->
					

			</div> <!-- .col-md-9 -->
		</div> <!-- .row -->
	</div> <!-- .container -->
@stop

@section('scripts')
	<script>
        @include('partials.analytics.list', [
            'products' => $accessories,
            'page' => 1,
            'per_page' => 999,
            'list_name' => 'Category Results',
            'category' => 'Roof Systems/Accessories'
        ])
	</script>
@stop