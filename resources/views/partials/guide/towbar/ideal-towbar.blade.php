<div class="ideal-towbar clearfix">
    <p>Ready to find your ideal towbar?</p>
    <a href="{!! route('towbars') !!}" class="btn btn-primary">Find my towbar</a>
</div>
