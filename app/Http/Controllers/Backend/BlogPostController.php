<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BlogPostStoreRequest;
use App\Models\BlogCategory;
use App\Models\BlogPost;
use App\Models\Image as ImageModel;
use Datatables;
use DB;
use Illuminate\Http\Request;
use URL;
use View;

class BlogPostController extends Controller
{
    /**
     * Instantiate a new instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.blog_posts.index');
    }

    /**
     * Return the datatable for blog_posts
     *
     * @return Datatables
     */
    public function indexDatatable()
    {
        $blog_posts = BlogPost::select(['id', 'title', DB::raw('IF(featured=1, "Yes", "No") as featured, IF(published=1, "Yes", "No") as published'), 'created_at', 'updated_at'])->get();

        return Datatables::of($blog_posts)
            ->addColumn('edit', '<a href="{{ URL::backend(\'blog_posts/edit/\'. $id) }}" class="btn btn-success">Edit</a>')
            ->addColumn('delete', '<a href="{{ URL::backend(\'blog_posts/delete/\'. $id) }}" class="btn btn-danger" data-confirm="Are you sure you want to delete this user?  There is no going back">Delete</a>')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(BlogCategory $blogCategory)
    {
        $categories = $blogCategory->get();
        return view('backend.blog_posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(BlogPostStoreRequest $request)
    {
        // create the blog post
        $blog_post = new BlogPost();
        $blog_post->title = $request->title;
        $blog_post->meta_title = $request->meta_title;
        $blog_post->meta_description = $request->meta_description;
        $blog_post->url_slug = str_slug($request->url_slug, '-');
        $blog_post->content = $request->content;
        $blog_post->published = (empty($request->published) ? 0 : 1);

        // store the image
        $file = $request->file('file');

        if (!is_null($file)) {
            $filename = strtolower(str_random(40)) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/img/blog_posts/', $filename);

            // save the image
            $image = new ImageModel();
            $image->path = '/img/blog_posts/';
            $image->filename = $filename;
            $image->save();

            // update the blog_post
            $blog_post->image_id = $image->id;
        }

        // wrap in transaction, for graceful fail
        DB::transaction(function () use ($blog_post, $request) {

            // save the blog post
            $blog_post->save();

            // categories
            $blog_post->categories_sync($request->category_ids);
        });

        return redirect(URL::backend('blog_posts'))->with('success', '<strong>Success!</strong> Blog post created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog_post = BlogPost::findOrFail($id);
        $categories = BlogCategory::get();
        return View::make('backend.blog_posts.edit', compact('blog_post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogPostStoreRequest $request, $id)
    {
        // save the blog_post
        $blog_post = BlogPost::findOrFail($id);
        $blog_post->title = $request->title;
        $blog_post->meta_title = $request->meta_title;
        $blog_post->meta_description = $request->meta_description;
        $blog_post->url_slug = str_slug($request->url_slug, '-');
        $blog_post->content = $request->get('content');
        $blog_post->published = (empty($request->published) ? 0 : 1);

        // update categories
        $blog_post->categories_sync($request->category_ids);

        $file = $request->file('file');

        // if there is an image
        if (!is_null($file)) {

            // delete the old image from rackspace and the database
            if (!is_null($blog_post->image)) {
                $blog_post->image->forceDelete();
            }

            // store the new image
            $filename = strtolower(str_random(40)) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/img/blog_posts/', $filename);

            // save the image
            $image = new ImageModel();
            $image->path = '/img/blog_posts/';
            $image->filename = $filename;
            $image->save();

            // update the blog_post
            $blog_post->image_id = $image->id;
        }

        // Save the blog
        $blog_post->save();

        return redirect(URL::backend('blog_posts'))->with('success', '<strong>Success!</strong> Blog post updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog_post = BlogPost::findOrFail($id);
        $blog_post->delete();

        return redirect(URL::backend('blog_posts'))->with('message', '<strong>Success!</strong> Blog post deleted.');
    }

    /**
     * Restore the specified resource
     */
    public function restore($id)
    {
        $blog_post = BlogPost::withTrashed()->findOrFail($id);
        $blog_post->restore();

        return redirect(URL::backend('blog_posts'))->with('success', '<strong>Success!</strong> Blog post restored.');
    }
}
