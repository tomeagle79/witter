const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

require('./elixir-extensions');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// Change our root directort to public_html
elixir.config.publicPath = 'public_html/assets';

/*elixir(function (mix) {
    mix.browserSync({
        proxy: 'https://www.local.witter.com/'
    });
});*/

elixir.config.css.autoprefix = {
    enabled: true, //default, this is only here so you know how to disable
    options: {
        cascade: true,
        browsers: ['last 20 versions', '> 1%']
    }
};

elixir(mix => {
    mix.sass([
        'app.scss',
        '../../../node_modules/swiper/dist/css/swiper.min.css',
        '../../../node_modules/animate.css/animate.min.css',
    ]);

    // Frontend scripts
    mix.scripts([
        '../../../node_modules/jquery/dist/jquery.min.js',
        '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        '../../../node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
        '../../../node_modules/bootstrap-validator/dist/validator.min.js',
        '../../../node_modules/moment/moment.js',
        '../../../node_modules/swiper/dist/js/swiper.min.js',
        '../../../node_modules/slick-carousel/slick/slick.min.js',
        'scroll_effects.js',
        'slick_init.js',
        'towbar_search_toggle.js',
        '../packages/bootstrap-datepicker/js/bootstrap-datetimepicker.js',
        'js.cookie.js',
        'custom.js',
        'toggle.js',
        'vehicle_dropdowns.js',
        'cart_appointments.js',
    ], 'public_html/assets/js/app.js');


    // Cart appointments frontend script
    mix.scripts([
        'cart_appointments.js',
    ], 'public_html/assets/js/cart_appointments.js');

    // Login CSS
    mix.sass([
        'font-awesome/scss/font-awesome.scss',
        'login.scss',
    ], 'public_html/assets/css/login.css');

    // Login Scripts
    mix.scripts([
        "jquery-2.2.4.min.js",
        "bootstrap.min.js",
        "login.js"
    ], 'public_html/assets/js/login.js');

    // Backend / Admin  CSS
    mix.sass([
        'backend.scss',
        'font-awesome/scss/font-awesome.scss',
        "../packages/datatables/dataTables.bootstrap.min.css",
        '../packages/redactor/redactor.min.css',
        '../packages/metronic/global/css/components.css',
        '../packages/metronic/global/css/plugins.css',
        '../packages/metronic/admin/layout/css/layout.css',
        '../packages/metronic/admin/layout/css/themes/default.css',
        '../packages/metronic/admin/layout/css/custom.css',
        '../packages/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        '../packages/fancybox/jquery.fancybox.css',
    ], 'public_html/assets/css/admin.css');

    // Backend / Admin Scripts
    mix.scripts([
        "jquery-2.2.4.min.js",
        "bootstrap.min.js",
        '../../../node_modules/moment/moment.js',
        "datatables/jquery.dataTables.min.js",
        "datatables/dataTables.bootstrap.min.js",
        '../packages/metronic/global/plugins/jquery.blockui.min.js',
        '../packages/metronic/global/plugins/jquery.cokie.min.js',
        '../packages/metronic/global/plugins/uniform/jquery.uniform.min.js',
        '../packages/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        '../packages/metronic/global/scripts/metronic.js',
        '../packages/metronic/global/scripts/datatable.js',
        '../packages/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
        '../packages/metronic/admin/layout/scripts/layout.js',
        '../packages/fancybox/jquery.fancybox.pack.js',
        '../packages/bootstrap-datepicker/js/bootstrap-datetimepicker.js',
        "validator.min.js",
        "admin.js",
    ], 'public_html/assets/js/admin.js');

    // Redactor Scripts
    mix.scripts([
        '../packages/redactor/redactor.js',
        '../packages/redactor/table.js',
        '../packages/redactor/imagemanager.js',
        '../packages/redactor/video.js'
    ], 'public_html/assets/js/redactor.js');

    mix.ie_split_css();
});