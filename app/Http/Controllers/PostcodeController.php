<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Api\Postcode;

use Session;

class PostcodeController extends FrontendController
{
    /**
     * Lookup a postcode
     *
     * @return Response
     */
    public function lookup(Request $request)
    {
        if(empty($request->postcode)) {
            $arr = [
                'success' => false,
                'errorMessage' => 'Please enter a postcode' 
            ];

            return response()->json($arr);
        }

        $result = Postcode::lookup($request->postcode);

        return response()->json($result);
    }

    /**
     * Get a list of addresses
     *
     * @return Response
     */
    public function get_addresses(Request $request)
    {
        if(empty($request->key)) {
            $arr = [
                'success' => false,
                'errorMessage' => 'Please send a key' 
            ];

            return response()->json($arr);
        }

        $result = Postcode::get_addresses($request->key);

        return response()->json($result);
    }

}
