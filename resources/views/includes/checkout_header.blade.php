<?php

    // Just set the cart count data here straight from the CartTotalItems middleware.
    $cart_total_items = request()->get('cart_total_items');
?>
<header class="checkout_header">
    <!-- Top Nav -->
    <div class="top-bar">
      <div class="container">
        <div class="row">
          <div class="col-xs-3 col-md-2 col-logo">
            <div class="navbar-brand">
              <a href="/">
                <img src="{{URL::asset('/assets/img/logo.png')}}" alt="Witter Towbars logo">
              </a>
            </div>
          </div>
          <!-- /.col-xs-12 -->
          <div class="col-xs-12 col-buttons">
            <ul class="block-link pull-right menu-buttons">
              <li class="checkout">
                @if(!empty($cart_total_items))
                Checkout<br><a href="{{ route('cart') }}">{{ $cart_total_items }} items</a>
                @else
                Checkout<br><a href="{{ route('cart') }}">0 items</a>
                @endif
              </li>
            </ul>
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </div>
</header>

@if(empty($checkout_layout_steps_hide))
    @includeIf('partials.checkout-steps')
@endif