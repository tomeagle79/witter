<div class="towbar-search">

  <div class="row">
    <div class="col-xs-12">
      <p>Let's find your vehicle</p>

            {!! Form::open(['route' => 'search.numberplate', 'class' => 'form numberplate-search float-right', 'method' => 'get', 'onsubmit' => "
                gtag('event', 'Success', {
                    event_category: 'Reg lookup',
                    event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
                });
            "]) !!}
                <div class="reg-form">
                    {!! Form::text('search', null, ['required', 'class' => 'form-control', 'placeholder' => 'Registration no.']) !!}
                    <button type="submit" class="btn btn-primary btn-special"></button>
                </div>
            {!! Form::close() !!}

      <p class="splitter">or</p>

      <a class="btn-show bg-arrow">I don't know my vehicle registration</a>

      @include('partials.search-dropdowns-minimal', ['redirect_slug' => 'towbars', 'form_button_text' => 'Find my vehicle'])

    </div><!-- /.col-xs-12 -->

  </div><!-- /.row -->


</div><!-- /.towbar-search -->

