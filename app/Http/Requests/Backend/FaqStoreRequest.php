<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class FaqStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'faq' => 'required|max:255',
            'url_slug' => ['required', 'max:255', 'regex:/^[a-z0-9-_]+$/'],
            'meta_title' => 'max:255',
            'meta_description' => 'max:255',
            'answer' => 'required',
        ];
        
        $id = $this->route('id');

        if($id){
            $rules['url_slug'][] = 'unique:faqs,url_slug,'.$id.',id,deleted_at,NULL';
        } else {
            $rules['url_slug'][] = 'unique:faqs,url_slug,NULL,id,deleted_at,NULL';
        }

        return $rules; 
    }
}
