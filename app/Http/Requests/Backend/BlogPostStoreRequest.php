<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class BlogPostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:255',
            'url_slug' => ['required', 'max:255', 'regex:/^[a-z0-9-_]+$/'],
            'meta_title' => 'max:255',
            'meta_description' => 'max:255',
            'content' => 'required',
        ];

        // categories
        $categories = $this->input('category_ids');
        if(is_array($categories)) {
            foreach($categories as $idx => $unused) {
                $rules['category_ids.'.$idx] = 'required:integer|exists:blog_categories,id';
            }
        } else {
            $rules['category_ids.0'] = 'required:integer|exists:blog_categories,id';
        }
        
        $id = $this->route('id');

        if($id){
            $rules['url_slug'][] = 'unique:blog_posts,url_slug,'.$id.',id,deleted_at,NULL';
        } else {
            $rules['url_slug'][] = 'unique:blog_posts,url_slug,NULL,id,deleted_at,NULL';
        }

        return $rules;
    }

    public function attributes() {
        return [
            'category_ids.0' => 'Categories'
        ];
    }
}
