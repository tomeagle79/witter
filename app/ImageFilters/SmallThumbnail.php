<?php

namespace App\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class SmallThumbnail implements FilterInterface
{

    public function applyFilter(Image $image)
    {
        return $image->resize(280, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }
}