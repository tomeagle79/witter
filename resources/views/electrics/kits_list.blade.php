@extends('layouts.frontend')

@section('title', 'Electrical Kits')

@section('heading')
	Electrical Kits for {{ $kits->fullVariantName }}
@stop

@section('meta_description')
	Electrical Kits for {{ $kits->fullVariantName }}
@stop

@section('content')

<div class="container product-listing">
	<div class="row">

		<div class="col-md-12">

            <p class="pre-title">We have the following electrical kits to fit your</p>
            <h2 class="nomargin">{{ $kits->fullVariantName }}</h2>

            <div class="button-area">
                <a href="{{ route('electrics') }}" class="btn btn-primary">Not your Vehicle?</a>
            </div>
        </div> <!-- .col-md-6 -->
        <?php /*
        <div class="col-md-6">
            <!-- image placeholder - not available in api at present -->
        </div> <!-- .col-md-6 -->
        */ ?>

    </div> <!-- .row -->

    <div class="row">
        <div class="col-md-12">

            @if(!empty($kits->electricKits))
                <div class="item-list">
                    @foreach($kits->electricKits as $kit)

                        @include('partials.analytics.json_product_details', [
                            'product' => $kit,
                            'list_name' => 'Category Results',
                            'category' => 'Electrical Kits',
                            'position' => $loop->iteration
                        ])

                        <div class="product-listing-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="{{ route('electrics.view', encode_url($kit->partNo)) }}" class="product-link" data-product-id="{{ $kit->partNo }}"><img src="{{ image_load($kit->imageUrl) }}" alt="{{ $kit->title }}"></a>
                                </div> <!-- .col-md-3 -->
                                <div class="col-md-6">
                                    <a href="{{ route('electrics.view', encode_url($kit->partNo)) }}" class="product-link" data-product-id="{{ $kit->partNo }}"><h3>{{ $kit->title }}</h3></a>
                                    <p>{{ $kit->details }}</p>
                                    <div class="price">
                                        Price From
                                        <span>&pound;{{ price($kit->price) }} Inc. VAT</span>
                                    </div> <!-- .price -->
                                </div> <!-- .col-md-6 -->

                                <div class="col-md-3">
                                    <a href="{{ route('electrics.view', encode_url($kit->partNo)) }}" class="btn btn-secondary btn-block product-link" data-product-id="{{ $kit->partNo }}">More<br>Details</a>
                                </div> <!-- .col-md-3 -->
                            </div> <!-- .row -->
                        </div> <!-- .towbar-listing -->
                    @endforeach
                </div> <!-- .item-list -->
            @else
                <p>Sorry, we don't have any electrical kits that are compatible with your vehicle.</p>
            @endif

        </div> <!-- .col-md-12 -->
    </div> <!-- .row -->
</div> <!-- .container -->
@stop

@section('scripts')
<script>
    @include('partials.analytics.list', [
        'products' => $kits->electricKits,
        'page' => 1,
        'per_page' => 9999,
        'list_name' => 'Category Results',
        'category' => 'Electrical Kits'
    ])
</script>
@stop