<?php

namespace App\Http\ViewComposers;

use Cache;
use Illuminate\View\View;
use App;

class CacheBustingViewComposer
{
    /** @var  View */
    protected $view;

    /** @var  array */
    protected $assets = [];

    /**
     * @param View $view
     */
    public function compose($view)
    {
        $this->view = $view;

        if (!App::environment('production')) {

            $this->assets = config('settings.assets_cache_arr');

            $this->view->with('assets', $this->assets);

            return;
        }

        if (Cache::has('assets')) {
            $this->view->with('assets', Cache::get('assets'));

            return;
        }

        $this->assets = config('settings.assets_cache_arr');

        $this->assets = $this->createFileHashes($this->assets);

        Cache::forever('assets', $this->assets);

        $this->view->with('assets', $this->assets);
    }

    /**
     * Create a short file hash for each asset.
     *
     * @param $assets
     */
    protected function createFileHashes($assets)
    {
        foreach ($assets as $key => $asset) {
            $path = public_path().'/'.$key;

            if (!file_exists($path)) {
                continue;
            }

            $hash = hash_file('crc32', $path);
            $dot  = strripos($asset, '.');

            $assets[$key] = substr($asset, 0, $dot + 1).$hash.substr($asset, $dot);
        }

        return $assets;
    }
}
