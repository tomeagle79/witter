<?php

namespace App\Models\Api;

use App\Models\Api\ApiModel;

class WitterQuote extends ApiModel
{
    const URI = "quotes";

    /**
     * Create a quote
     *
     * @params Array
     * @return Object
     */
    public static function create_quote($quote)
    {
        $url = config('witter.api_base') . self::URI;

        $obj = self::post_request($url, $quote);

        return $obj;
    }
}
