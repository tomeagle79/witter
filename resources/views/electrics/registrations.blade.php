@extends('layouts.frontend')

@section('title', $manufacturer->manufacturerName .' '. $model->modelName .' '. $body->bodyName .' towbars')

@section('heading')
	Vehicles for {{ $manufacturer->manufacturerName }} {{ $model->modelName }} {{ $body->bodyName }}
@stop

@section('meta_description')
	Towbars Vehicles for {{ $manufacturer->manufacturerName }} {{ $model->modelName }} {{ $body->bodyName }}
@stop

@section('content')


<div class="container">
	<div class="row">

		<div class="col-md-9 col-md-push-3 towbars-listing">
            @if( !empty($model->photoUrl) )
                <div class="top-img-wrapper">
                    <img src="{{ $model->photoUrl }}" alt="{{ $body->bodyName }}" class="top-img">
                    <p class="text-center">Images are representative only</p>
                </div> <!-- .wrapper -->
            @endif

            <h2>{{ $manufacturer->manufacturerName }}</h2>
            <h3>{{ $model->modelName }}</h3>
            <h4>{{ $body->bodyName }}</h4>


            <p>Please select the year of manufacture</p>

            <div class="item-list">
            <?php
            // Count
            $count = count($registrations);
            ?>
            @foreach ($registrations as $key => $registration)
                <?php $key++; ?>

                @if($key%3 === 1)
                    <div class="row">
                @endif

                <div class="col-md-4">
                    <div class="item">
                        <a href="{{ route('electrical-kits.model_list.body_list.registration_list.vehicle_list', [$manufacturer->slug, $model->slug, $body->slug, $registration->registrationId]) }}">
                            <h3>
                                {{ $registration->registrationText }}
                            </h3>
                        </a>
                    </div> <!-- .body -->
                </div>

                @if($key%3 === 0 && $key > 0 || $key == $count)
                    </div><!-- .row -->
                @endif
            @endforeach
            </div> <!-- .bodies -->
		</div>

        <div class="col-md-3 col-md-pull-9">
            @include('partials.electrics.sidebar-search')

            @include('partials.sidebar-info-menu')

        </div> <!-- .col-md-3 -->
	</div>
</div>
@stop
