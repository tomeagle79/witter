<?php

use App\Models\HelpArticle;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPublishedToHelpArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('help_articles', function (Blueprint $table) {
            $table->tinyInteger('published')->index()->default(0)->after('title');
        });

        // Update the help articles to ba published. So existing posts display.
        $articles = HelpArticle::get();

        foreach ($articles as $article) {

            $article->published = 1;
            $article->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('help_articles', function (Blueprint $table) {
            $table->dropColumn('published');
        });
    }
}
