@extends('layouts.frontend')

@section('title', 'Towbar Accessories | Towing Accessories')

@section('heading')
	Towbar Accessories
@stop

@section('meta_description')
	Towbar accessories by Witter Towbars. Our extensive range of towing accessories cater for a wide variety of vehicles and towbars. Buy online today.
@stop

@section('content')
	<div class="container">
		<div class="row">

			<div class="col-sm-8 col-md-9 col-md-push-3 accessory-listing">

				<h2 class="border-bottom no-top-margin">Witter Towbars Accessories</h2>

				<div class="row item-list flex-items">

					{{-- {{ dd($partClasses) }} --}}
					@foreach($partClasses as $class)
						@if($class->partClassId !== 25)
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="item text-center">
									<div class="item-body">
										<a href="{!! route('accessories.multi', $class->slug) !!}" ><img src="{{ image_load($class->imageUrl) }}" alt="" /></a>

										<a href="{!! route('accessories.multi', $class->slug) !!}" ><h3 class="accessories-list-title">{!! $class->title !!}</h3></a>
									</div><!-- /.item-body -->
								</div>
							</div>
						@endif
					@endforeach

				</div> <!-- .row -->


			</div> <!-- .col-md-9 -->

			<div class="col-sm-4 col-md-3 col-md-pull-9">

				@include('partials.sidebar-partclass-menu')

				@include('partials.sidebar-search')

				@include('partials.sidebar-findlink-secondary')

			</div> <!-- .col-md-3 -->
		</div> <!-- .row -->
	</div> <!-- .container -->
@stop

