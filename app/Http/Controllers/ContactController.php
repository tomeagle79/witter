<?php
namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;

use Illuminate\Http\Request;

use App\Http\Requests;

class ContactController extends FrontendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['breadcrumbs']->addCrumb('Contact us');

        $this->data['openingHours'] = config('settings.site.opening_hours');

        return view('pages.contact', $this->data);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function submit(ContactFormRequest $request)
    {
        \Mail::send('emails.contact',
            array(
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'postcode' => $request->get('postcode'),
                'user_message' => $request->get('enquiry')
            ), function($message)
        {
            $message->from(config('witter.noreply_email'));
            $message->to(config('witter.admin_email'), 'Admin')->subject('Witter Contact Form');
        });

        return \Redirect::route('contact')->with('message', 'Thank-you for your enquiry, one of our team will respond within 48 hours');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function product_enquiry(ContactFormRequest $request)
    {
        $enquiry_regarding = $request->get('enquiry_regarding');

        \Mail::send('emails.product_enquiry',
            array(
                'website_url' => $request->get('website_url'),
                'enquiry_regarding' => $enquiry_regarding,
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'postcode' => $request->get('postcode'),
                'user_message' => $request->get('enquiry')
            ), function($message) use ($enquiry_regarding)
        {
            $message->from(config('witter.noreply_email'));
            $message->to(config('witter.admin_email'), 'Admin')->subject('Product enquiry - ' . $enquiry_regarding);
        });

        $msg = 'Thank-you for your enquiry, one of our team will respond within 48 hours';
        if(!empty($request->get('website_url'))) {
            return redirect($request->get('website_url'))->with('message', $msg);
        }
        return redirect()->back()->with('message', $msg);
    }

}
