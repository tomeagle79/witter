<?php
namespace App\Facades;

class Redirect extends \Illuminate\Support\Facades\Redirect {
    public static function backend($path) {
        return self::to(config('settings.admin.slug').'/'.$path);
    }
}