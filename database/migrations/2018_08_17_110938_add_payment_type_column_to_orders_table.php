<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddPaymentTypeColumnToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('payment_type')->nullable()->after('payment_reference');
        });

        DB::table('orders')
            ->where('payment_reference', 'LIKE', 'ch_%')
            ->update(['payment_type' => 'Stripe']);

        DB::table('orders')
            ->where('payment_reference', 'LIKE', 'PAY%')
            ->update(['payment_type' => 'PayPal']);

        DB::table('orders')
            ->where('payment_reference', 'LIKE', 'divido-%')
            ->update(['payment_type' => 'Divido']);

        DB::table('orders')
            ->where('is_phone_order', '=', 1)
            ->update(['payment_type' => 'Phone']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('payment_type');
        });
    }
}
