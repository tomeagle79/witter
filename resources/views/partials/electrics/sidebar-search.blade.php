<div class="sidebar-primary">

	<h2>Find your electrical kit</h2>

	<p>Enter your registration number</p>

	{!! Form::open(['route' => 'electrics.search.numberplate', 'class' => 'form numberplate-search', 'method' => 'get', 'onsubmit' => "
		gtag('event', 'Success', {
			event_category: 'Reg lookup',
			event_label: '". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ."'
		});
	"]) !!}
		{!! Form::text('search', null, ['required', 'class' => 'form-control', 'placeholder' => 'Reg Num']) !!}
		<button type="submit" class="btn btn-primary">Find<br>Your Kit</button>
	{!! Form::close() !!}

</div>