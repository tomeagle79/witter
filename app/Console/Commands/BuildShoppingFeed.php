<?php

namespace App\Console\Commands;

use App\Models\Api\Accessory;
use App\Models\Api\PartClass;
use App\Models\ShoppingFeedOverride;
use Illuminate\Console\Command;

class BuildShoppingFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:shopping_feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build the shopping feed for Google, in XML format';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);

        /*
        For reference, Witter's internal naming convention is:
        - PartClass = Categories
        - PartClass Subcategory = Sub-Category
        - Accessory - Product
         */
        $this->info('Starting feed build...');

        // Open the file for writing
        $fp = fopen(public_path('google_feed.xml'), "w");

        $this->info('File opened for writing.');

        $xml = '<?xml version="1.0"?>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
    <channel>
        <title>Towbar Fitting | UK Tow Bar Fitters | Witter Towbars</title>
        <link>https://www.witter-towbars.co.uk</link>
        <description>Towbar fitting by Witter Towbars. Our all-in-one price includes towbar, electric kit and fitting. Buy your towbar and arrange a fitting online today.</description>
        ';
        // Write it to stream
        fwrite($fp, $xml);

        $this->info('Initial write done.');

        // Get roof box category id
        $roof_box_category = PartClass::get_category_by_slug("roof-boxes");

        // Grab all categories
        $categories = PartClass::get_all();

        // Need to call cycle_carriers seperatly and then add them to the array.
        $cycle_carriers = PartClass::get_category_by_slug('cycle-carriers');

        // Add the cycle_carriers to the first element in the array of categories.
        array_unshift($categories, $cycle_carriers);

        $i = 0;
        $f = 0;
        $missing_titles = [];
        $westfalia_count = 0;
        $witter_count = 0;
        foreach ($categories as $category) {
            $this->info('Building category ' . $category->title);

            // Loop through sub categories
            $subCategories = PartClass::get_subcategories_by_slug($category->slug);

            foreach ($subCategories as $subCategory) {
                $this->info('- Building sub-category ' . $subCategory->categoryName);

                // Loop through all accessories in a sub category
                $accessories = Accessory::get_accessories_by_slug($category->slug, $subCategory->slug);

                foreach ($accessories as $accessory) {
                    if (strlen(trim($accessory->title)) > 0) {
                        if (strlen(trim($accessory->details)) > 0 && strlen(trim($accessory->details)) > 0) {
                            $this->info('-- Building product ' . $accessory->title . ' (' . $accessory->partNo . ')');

                            $description = $accessory->details . "\n\n" . $accessory->moreDetails;
                            $stock = $accessory->isSellable ? 'in stock' : 'out of stock';

                            // Handle categories and links
                            if ($category->slug == 'cycle-carriers') {
                                $google_category = 'Vehicles &amp; Parts &gt; Vehicle Parts &amp; Accessories &gt; Vehicle Storage &amp; Cargo &gt; Motor Vehicle Carrying Racks &gt; Vehicle Bicycle Rack';

                                $link = route('bike_rack.view', [encode_url($accessory->slug)]);

                            } elseif ($category->slug == 'caravan-movers') {
                                $google_category = 'Vehicles &amp; Parts &gt; Vehicle Parts &amp; Accessories';

                                $link = route('caravan_movers.view', [encode_url($accessory->slug)]);
                            } else {
                                $google_category = 'Vehicles &amp; Parts &gt; Vehicle Parts &amp; Accessories';

                                $link = route('accessories.multi', encode_url($accessory->slug));
                            }

                            // Handle brands
                            // List of westfalia part numbers
                            $westfalia = [
                                '300025300113',
                                '300028300113',
                                '335251300183',
                                '350002600001',
                                '350004600001',
                                '350008600001',
                                '350055600001',
                            ];
                            // Is this a westfalia product?
                            if (in_array($accessory->partNo, $westfalia)) {
                                $brand = 'Westfalia';
                                $westfalia_count++;
                            } else {
                                $brand = 'Witter';
                                $witter_count++;
                            }

                            // Set fields. We'll override these if we have a valid override in the database
                            $title = $accessory->title;
                            $description = $description;
                            $image = $accessory->imageUrl;
                            $google_category = $google_category;

                            $override = ShoppingFeedOverride::where('part_no', $accessory->partNo)->first();

                            if (!empty($override)) {
                                $this->info('-- Overrides found for ' . $accessory->title . '. Applying and continuing build.');

                                $title = (empty($override->product_title) ? $title : $override->product_title);
                                $description = (empty($override->product_description) ? $description : $override->product_description);
                                $image = (empty($override->product_image) ? $image : $override->product_image);
                                $google_category = (empty($override->google_category) ? $google_category : $override->google_category);
                            }

                            // Just trim the title to 70 characters or less.
                            $title = strlen($title) > 70 ? substr($title, 0, 67) . "..." : $title;

                            // Check if there is a display price. If so use it rather than the price.
                            $display_price = empty($accessory->displayPrice) ? price($accessory->price) : price($accessory->displayPrice);

                            $xml = '
        <item>
            <!-- The following attributes are always required -->
            <g:id>' . $accessory->partNo . '</g:id>
            <g:title>
                <![CDATA[' . $title . ']]>
            </g:title>
            <g:description>
                <![CDATA[' . $description . ']]>
            </g:description>
            <g:link>
                <![CDATA[' . $link . ']]>
            </g:link>
            <g:image_link>
                <![CDATA[' . $image . ']]>
            </g:image_link>
            <g:condition>new</g:condition>
            <g:availability>in stock</g:availability>
            <g:price>' . $display_price . ' GBP</g:price>
            <g:brand>' . $brand . '</g:brand>
            <g:mpn>' . $accessory->partNo . '</g:mpn>
            <g:google_product_category>' . $google_category . '</g:google_product_category>';

                            // Set shipping label if a roof box.
                            if ($roof_box_category->partClassId == $accessory->partClassId) {

                                $xml .= '
            <g:delivery_label>roofbox</g:delivery_label>';
                            }

                            // Set up some custom labels
                            if (!empty($category->title)) {
                                $xml .= '
            <g:custom_label_0>
                <![CDATA[' . $category->title . ']]>
            </g:custom_label_0>';
                            }

                            // Set up some custom labels
                            if (!empty($subCategory->categoryName)) {
                                $xml .= '
            <g:custom_label_1>
                <![CDATA[' . $subCategory->categoryName . ']]>
            </g:custom_label_1>';
                            }

                            $xml .= '
        </item>';

                            // Write it to stream
                            fwrite($fp, $xml);

                            $i++;
                        } else {
                            $this->error('-- Product ' . $accessory->partNo . ' has no description so skipping');
                            $missing_titles[] = $accessory->partNo;
                            $f++;
                        }
                    } else {
                        $this->error('-- Product ' . $accessory->partNo . ' has no title so skipping');
                        $missing_titles[] = $accessory->partNo;
                        $f++;
                    }
                }
            }
        }

        $this->info("\n ----- \n");

        $this->info($i . ' products processed successfully');
        $this->info($f . ' products were skipped due to missing titles or descriptions');
        if ($f > 0) {
            $this->info('Skipped product codes are: ' . implode(', ', $missing_titles));
        }

        $this->info("\n ----- \n");

        $this->info($witter_count . ' products were Witter products');
        $this->info($westfalia_count . ' products were Westfalia products');

        $xml = '
    </channel>
</rss>';

// Write it to stream
        fwrite($fp, $xml);

        fclose($fp);

    }
}
