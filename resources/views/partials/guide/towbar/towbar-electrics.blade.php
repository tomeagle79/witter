<div class="row">
    <div class="col-xs-12">
        <p>Once you've found the right towbar, then it is a case of deciding which electrics are best for your car and your towing needs. In this section we will look at dedicated kits, universal kits, 7-pin electrics, 7-pin twin electrics and 13-pin electrics.</p>

        <h2 class="small" id="electrics-for-my-towbar">Do I need electrics for my towbar?</h2>
        <p>Yes, you will need electrics for your towbar. Anybody who fits a towbar to your vehicle without an <a href="/electrical-kits">electric kit</a> is breaking the law. Here at Witter Towbars, we would never fit a towbar without an electric kit.</p>

        <h2 class="small" id="how-towbar-electrics-used">What are the electrics used for?</h2>
        <p>They connect the electric system in your car to the electric system on either a caravan, trailer or cycle carrier. As stated above, it is a requirement by law to have an electrical kit fitted with your towbar. The reason this is a legal requirement is because the electric kit syncs the safety features in your car with the electrics in a caravan, trailer or cycle carrier.</p>

        <h2 class="small" id="which-electric-kit">Should I buy a dedicated or universal electric kit?</h2>
        <p>It is possible that there will be a universal or a dedicated unit available to fit to your vehicle. Due to modern vehicles having such complex wiring systems, we always suggest that you go with a dedicated kit. Dedicated kits are specifically designed to sync the safety features in your car with the electrics in your caravan. This could be a lifesaving piece of equipment.  Although they are more expensive than the universal kits, buying a dedicated kit allows you to avoid all the hassle of a universal kit.</p>
        <p>You then have the choice between 7-pin, 7-pin twin or 13-pin electrics; again the choice is down to your towing needs, so read the guide below to help you pick which electrics are best suited to your vehicle.</p>
    </div>
</div>

<div class="row electrics-buttons">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <a href="#seven-pin-electrics" class="btn btn-primary btn-block smoothscroll">7-pin</a>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <a href="#seven-pin-twin-electrics" class="btn btn-primary btn-block smoothscroll">7-pin Twin</a>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <a href="#thirteen-pin-electrics" class="btn btn-primary btn-block smoothscroll">13-pin</a>
    </div>
</div>

<hr>

<div class="row" id="best">
    <div class="col-xs-12">
        <h2 class="small" id="seven-pin-electrics">7-Pin Electrics</h2>
        <p>This is the most common and cheapest connector type currently used in the UK. It will fulfil all normal towing requirements under UK law and it can be used with lighting boards, cycle carriers, old caravans, garden trailers, plant trailers and horseboxes. In fact, almost anything you can connect to a towbar. A 7-pin electric kit will not power the inside of your caravan. </p>

        <p class="tile-title">What Can You Power with 7-Pin Electrics?</p>
        <p>Lights on a caravan, trailer or cycle carrier (indicator, fog lights, brake lights)</p>

        <p class="tile-title">What Cannot be Powered with 7-Pin Electrics?</p>
        <p>Interior appliances of a caravan (fridge, caravan battery) Reverse lights (caravan, cycle carrier, trailer)</p>
    </div>
    <div class="col-xs-12">
        <img src="/img/complete-guide/7-pin.jpg">
    </div>
</div>

<hr>

<div class="row">
    <div class="col-xs-12">
        <h2 class="small" id="seven-pin-twin-electrics">7-Pin Twin Electrics</h2>
        <p>These sockets are usually fitted in addition to the single 7-pin electrics. Providing electric to both the exterior and interior of a caravan, this is the ideal electrical socket when you wish to charge the caravan battery or power the fridge in a caravan built before 2008.</p>

        <p class="tile-title">What Can You Power with 7-Pin Twin Electrics?</p>
        <p>Lights on a caravan, trailer or cycle carrier (indicator, fog lights, brake lights) Interior appliances of a caravan (fridge, caravan battery) Reverse lights (caravan, cycle carrier, trailer)</p>

        <p class="tile-title">What Cannot be Powered with 7-Pin Twin Electrics?</p>
        <p>N/A</p>
    </div>
    <div class="col-xs-12">
        <img src="/img/complete-guide/7-pin-twin-1.jpg">
        <img src="/img/complete-guide/7-pin-twin-2.jpg">
    </div>
</div>

<hr>

<div class="row">
    <div class="col-xs-12">
        <h2 class="small" id="thirteen-pin-electrics">13-Pin Electrics</h2>
        <p>The modern socket type on the market, the 13-pin socket is ideal for all your towing needs. Powering the lights on a caravan, trailer or cycle carrier, a 13-pin socket can also provide power to the inside of your caravan. All UK caravans manufactured after 2008 will have a 13-pin connector as standard.</p>

        <p class="tile-title">What Can You Power with 13-Pin Twin Electrics?</p>
        <p>Lights on a caravan, trailer or cycle carrier (indicator, fog lights, brake lights) Interior appliances of a caravan (fridge, caravan battery) Reverse lights (caravan, cycle carrier, trailer)</p>

        <p class="tile-title">What Cannot be Powered with 13-Pin Twin Electrics?</p>
        <p>N/A</p>
    </div>
    <div class="col-xs-12">
        <img src="/img/complete-guide/13-pin.jpg">
    </div>
</div>
